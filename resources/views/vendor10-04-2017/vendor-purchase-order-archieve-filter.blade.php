<div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Sr. No</th>
                                                <th class="text-center">PO. No</th>
                                                <th class="text-center">PO. Date</th>
                                                <th class="text-center">Due. Date</th>
                                                <th class="text-center">Item Code</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Width/Size</th>
                                                 <th class="text-center">Cat 6</th>
                                                <th class="text-center">Price</th>
                                                <th class="text-center">Order Qty</th>
                                                <th class="text-center">Received Qty</th>
                                                <th class="text-center">Cancelled Qty</th>
                                                <th class="text-center">GIT Qty</th>
                                                <th class="text-center">Adhoc Qty</th>
                                                <th class="text-center">Pending Qty</th>
                                                <!-- <th class="last-width text-center">Action</th> -->
                                            </tr>
                                        </thead>
                                    <tbody  id="search_result_body">
                                    <?php $inc=$purchaseOrderList->perPage() * ($purchaseOrderList->currentPage()-1); ?>
                                    @foreach($purchaseOrderList as $eachOrder)
                                    <?php $inc++; 
                                    $pending = ($eachOrder->ORDQTY - ($eachOrder->RCQTY+$eachOrder->CNLQTY + $eachOrder->git_qty));
                                    $adhoc_qty=0;
                                    if($pending<=0)
                                    {
                                    //echo $pndg_qty=str_replace("-","Adhoc:",$pndg_qty);
                                    $adhoc_qty=str_replace("-"," ",$pending);
                                    $pending=0;
                                    
                                    }
                                    ?>
                                    <tr>
                                        <td class="text-center">{{$inc}}</td>
                                        <td class="text-center po-no<?php echo $inc;?>">{{$eachOrder->PONO}}</td>
                                        <td class="text-center po-date<?php echo $inc;?>">{{date('d/m/Y',strtotime($eachOrder->ORDDT))}}</td>
                                        <td class="text-center due-date<?php echo $inc;?>">{{date('d/m/Y',strtotime($eachOrder->DUEDATE))}}</td>
                                        @if(isset($eachOrder->ICODE))
                                        <td class="text-center ICODE<?php echo $inc;?>">{{$eachOrder->ICODE}}</td>
                                        @else
                                        <td class="text-center ICODE<?php echo $inc;?>"></td>
                                        @endif
                                        <td class="text-center cat-1<?php echo $inc;?>">{{$eachOrder->cat1}}</td>
                                        <td class="text-center cat-2<?php echo $inc;?>">{{$eachOrder->cat2}}</td>
                                        <td class="text-center cat-3<?php echo $inc;?>">{{$eachOrder->cat3}}</td>
                                        <td class="text-center cat-4<?php echo $inc;?>">{{$eachOrder->cat4}}</td>
                                        <td class="text-center cat-4<?php echo $inc;?>">{{$eachOrder->CNAME6}}</td>
                                        <td class="text-center price<?php echo $inc;?>">{{$eachOrder->RATE}}</td>
                                        <td class="text-center order-qty<?php echo $inc;?>">{{$eachOrder->ORDQTY}}</td>
                                        <td class="text-center received-qty<?php echo $inc;?>">{{$eachOrder->RCQTY}}</td>
                                        <td class="text-center cancelled-qty<?php echo $inc;?>">{{$eachOrder->CNLQTY}}</td>
                                        <td class="text-center git-qty<?php echo $inc;?>">@if(!empty($eachOrder->git_qty)){{$eachOrder->git_qty}}@else{{0}}@endif</td>
                                        <td class="text-center pending-qty<?php echo $inc;?>">{{$adhoc_qty}}</td>
                                        <td class="text-center pending-qty<?php echo $inc;?>">{{$pending}}</td>
                                        
                                    </tr>
                                    @endforeach
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                              <div class="col-sm-5">
                                <div class="dataTables_info" role="status" aria-live="polite">Showing {{$purchaseOrderList->count()}} of {{$purchaseOrderList->total()}} entries</div>
                              </div>
                              <div class="col-sm-7">
                                    <nav aria-label="Page navigation" class="pull-right">
                                        {{ $purchaseOrderList->links() }}
                                    </nav>
                                  </div>
                              </div>