@include('layouts.vendor_header');
    <!--add user popup Modal Start-->
<div class=" quotation-pop" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Change Password</h4>
          </div>
             @if(isset($errors))
                            @foreach ($errors->all() as $error)

                               <div style="padding-left: 145px;" class="alert alert-success alert-dismissable">{{ $error }}</div>
                            
                            @endforeach
                          @endif

                   <div class="modal-body">  
                   <form action="{{route('updt-password')}}" method="post">  
                       <div class="row shorting-area pricechange">
                    {{ csrf_field() }}
                    <input type="hidden"  name="id" value='{{$userid}}' >              
                      <div class="col-sm-12">
                            <h5>Old password :</h5>
                            <input type="password" name="old_password"  class="form-control">
                        </div>

                        <div class="col-sm-12">
                            <h5>New Password :</h5>
                            <input type="password" name="new_password"  class="form-control">
                        </div>
                        <div class="col-sm-12">
                            <h5>Confirm New Password :</h5>
                            <input type="password" name="confirm_new_password"  class="form-control">
                        </div>
                       
                        <div class="clearfix"></div>
                        <div class="col-sm-6">
                        <input type="submit" name="submit" value="Save" class="pup-btn">
                        </div>
                        <div class="col-sm-6">
                        <a href="{{route('ven-dashboard')}}" class="pup-btn">Cancel
                        </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                     </form> 
                
           </div>
        </div>
    </div>
</div>
<!--add user popup  Modal End-->
@include('layouts.footer');
