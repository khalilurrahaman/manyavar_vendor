
      <div class="table-responsive ">
          <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                  <tr>
                      <th class="text-center">Sr. No</th>
                      <th class="text-center">PO. No</th>
                      <th class="text-center">PO. Date</th>
                      <th class="text-center">Due. Date</th>
                      <th class="text-center">Item Code</th>
                      <th class="text-center">Design</th>
                      <th class="text-center">Colour</th>
                      <th class="text-center">Cat 3</th>
                      <th class="text-center">Width/Size</th>
                       <th class="text-center">Cat 6</th>
                      <th class="text-center">Price</th>
                      <th class="text-center">Order Qty</th>
                      <th class="text-center">Received Qty</th>
                      <th class="text-center">Cancelled Qty</th>
                      <th class="text-center">GIT Qty</th>
                      <th class="text-center">Adhoc Qty</th>
                      <th class="text-center">Pending Qty</th>
                      <th class="last-width text-center">Action</th>
                  </tr>
              </thead>
          <tbody id="search_result_body">
          <?php $inc=$purchaseOrderList->perPage() * ($purchaseOrderList->currentPage()-1); ?>
          @if(count($purchaseOrderList)>0)
                @foreach($purchaseOrderList as $eachOrder)
                <?php $inc++; 
                $pending = ($eachOrder->ORDQTY - ($eachOrder->RCQTY+$eachOrder->CNLQTY + $eachOrder->git_qty));
                                    //if($pending<=0)
                                        //$pending=str_replace('-', 'Adhoc:', $pending);
									$adhoc_qty=0;
									if($pending<=0)
									{
									//echo $pndg_qty=str_replace("-","Adhoc:",$pndg_qty);
									$adhoc_qty=str_replace("-"," ",$pending);
									$pending=0;
									
									}
                    ?>
                    <tr>
                        <td class="text-center">{{$inc}}</td>
                        <td class="text-center po-no<?php echo $inc;?>">{{$eachOrder->PONO}}</td>
                        <td class="text-center po-date<?php echo $inc;?>">{{date('d/m/Y',strtotime($eachOrder->ORDDT))}}</td>
                        <td class="text-center due-date<?php echo $inc;?>">{{date('d/m/Y',strtotime($eachOrder->DUEDATE))}}</td>
                        @if(isset($eachOrder->ICODE))
                        <td class="text-center ICODE<?php echo $inc;?>">{{$eachOrder->ICODE}}</td>
                        @else
                        <td class="text-center ICODE<?php echo $inc;?>"></td>
                        @endif
                        <td class="text-center cat-1<?php echo $inc;?>">{{$eachOrder->cat1}}</td>
                        <td class="text-center cat-2<?php echo $inc;?>">{{$eachOrder->cat2}}</td>
                        <td class="text-center cat-3<?php echo $inc;?>">{{$eachOrder->cat3}}</td>
                        <td class="text-center cat-4<?php echo $inc;?>">{{$eachOrder->cat4}}</td>
                        <td class="text-center cat-4<?php echo $inc;?>">{{$eachOrder->CNAME6}}</td>
                        <td class="text-center price<?php echo $inc;?>">{{$eachOrder->RATE}}</td>
                        <td class="text-center order-qty<?php echo $inc;?>">{{$eachOrder->ORDQTY}}</td>
                        <td class="text-center received-qty<?php echo $inc;?>">{{$eachOrder->RCQTY}}</td>
                        <td class="text-center cancelled-qty<?php echo $inc;?>">{{$eachOrder->CNLQTY}}</td>
                        <td class="text-center git-qty<?php echo $inc;?>">@if(!empty($eachOrder->git_qty)){{$eachOrder->git_qty}}@else{{0}}@endif</td>
                        <td class="text-center adhoc-qty<?php echo $inc;?>">{{$adhoc_qty}}</td>
                        <td class="text-center pending-qty<?php echo $inc;?>">{{$pending}}</td>
                        <td class="last-width text-center">
                        <select id="action_type" name="action_type" class="select-drop" onchange="return actionType(this.value,'<?php echo $inc;?>');">
                        <option value="">Select Type</option>
                        <?php
                        $cdisable='';
                        $rdisable='';
                        $wdisable='';
                        $ddisable='';
                        $odisable='';
                        $sdisable='';
                        $candisable='';
                        if(!empty($eachOrder->actiondtls) && count($eachOrder->actiondtls)>0){
                        foreach($eachOrder->actiondtls as $row){
                          if($row->action_type=='cutting' && $row->status=='new' && $row->ICODE==$eachOrder->ICODE) 
                           $cdisable='disabled';
                          if($row->action_type=='rate_change' && ($row->status=='new' || $row->status=='inprocess') && $row->ICODE==$eachOrder->ICODE)
                           $rdisable='disabled';
                          if($row->action_type=='width_change' && ($row->status=='new' || $row->status=='inprocess') && $row->ICODE==$eachOrder->ICODE)
                           $wdisable='disabled';
                          if($row->action_type=='date_ext' && $row->status=='new' && $row->ICODE==$eachOrder->ICODE)
                           $ddisable='disabled';
                          if($row->action_type=='others' && $row->status=='new' && $row->ICODE==$eachOrder->ICODE)
                           $odisable='disabled';
                          if($row->action_type=='sample' && $row->status=='new' && $row->ICODE==$eachOrder->ICODE)
                           $sdisable='disabled';
                          if($row->action_type=='cancel' && $row->status=='new' && $row->ICODE==$eachOrder->ICODE)
                           $candisable='disabled';

                        }
                        }
                         ?>
                          <option value="vendorChangecutting" {{$cdisable}}>REQUEST FOR CUTTING</option>
                          <option value="vendorChangeprice" {{$rdisable}}>REQUEST FOR RATE CHANGE</option>
                          <option value="vendorChangewidth" {{$wdisable}}>REQUEST FOR WIDTH CHANGE</option>
                          <option value="vendorChangedate" {{$ddisable}}>REQUEST FOR DATE EXT</option>
                          <option value="vendorCancellation" {{$candisable}}>REQUEST FOR CANCEL</option>
                          <option value="vendorAddremarks" {{$odisable}}>REQUEST FOR OTHERS</option>
                          <option value="vendorSamplerequest" {{$sdisable}}>REQUEST FOR Sample</option>
                        </select>
            		</td>
                    </tr>
                    @endforeach

                    @else
                        <tr><td style="font-size: 18px;" class="text-center" colspan="18" >Please enter search keyword! </td></tr>

                     @endif

                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                              <div class="col-sm-5">
                                <div class="dataTables_info" role="status" aria-live="polite">Showing {{$purchaseOrderList->count()}} of {{$purchaseOrderList->total()}} entries</div>
                              </div>
                              <div class="col-sm-7">
                                    <nav aria-label="Page navigation" class="pull-right">
                                        {{ $purchaseOrderList->links() }}
                                    </nav>
                                  </div>
                              </div>