@include('layouts.vendor_header')
<style>
.dt-buttons{margin-right:0px !important;}
.dt-buttons>a {padding-left:18px;}
</style>
<script src="{{asset('dataTable/css/jquery.dataTables.min.css')}}"></script>
<script src="{{asset('dataTable/css/buttons.dataTables.min.css')}}"></script>

   
<script src="{{asset('dataTable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('dataTable/js/dataTables.buttons.min.js')}}"></script>   
<script src="{{asset('dataTable/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('dataTable/js/jszip.min.js')}}"></script>   
<script src="{{asset('dataTable/js/pdfmake.min.js')}}"></script>
<script src="{{asset('dataTable/js/vfs_fonts.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.print.min.js ')}}"></script>
   
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                	<div class="col-sm-6"><h3 class="panel-title">
                                    <i class="fa fa-users" aria-hidden="true"></i> Price Listing</h3></div>
                                    <div class="col-sm-6">
                                    
                                    <div class="shorting-area">
                            		
                              
                                <!-- <div class="col-sm-3 pull-right">
                                        <a  data-toggle="modal" class="btn btn-success">
                                        <i class="fa fa-plus" aria-hidden="true"></i> Add New</a>
                                </div> -->
                            </div>
                                    
                                    
                                    </div>
                                </div>
                                
                            @if(isset($errors))
                            @foreach ($errors->all() as $error)
                              <div style="padding-left: 145px;" id="hide-me" class="alert alert-success alert-dismissable">{{ $error }}</div>
                            @endforeach
                            @endif    
                                
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Sr. No</th>
                                                <th class="text-center">Image</th>
                                                <th class="text-center">Item</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Cat3</th>
                                                <th class="text-center">Width/Size</th>
                                                <th class="text-center">Cat6</th>
                                                <th class="text-center">Price</th>
                                                <th class="text-center col-sm-2">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <?php $inc=$data->perPage() * ($data->currentPage()-1);; ?>
                                    @foreach($data as $eachData)
                                     <?php $inc++; ?>
                                     
                                    <tr>
                                        <td class="text-center">{{$inc}}</td>
                                        <?php
                                          $item_image=URL::to('/img').'/pre-order-img.jpg';
                                              if($eachData->image!='' && file_exists('upload/product/'.$eachData->image))
                                                  $item_image=URL::to('/upload/product').'/'.$eachData->image;
                                        ?>
                                        <td class="text-center"> <img src="{{$item_image}}" style="height:60px;width: 60px;"/></td>
                                        <td class="text-center">@if($eachData->ICODE){{($eachData->ICODE)}}@endif</td>
                                        <td class="text-center">{{$eachData->cat1}} </td>
                                        <td class="text-center">{{$eachData->cat2}} </td>
                                        <td class="text-center">{{$eachData->cat3}} </td>
                                        <td class="text-center">{{$eachData->cat4}} </td>
                                        <td class="text-center">{{$eachData->CNAME6}} </td>
                                        <td class="text-center">{{$eachData->RATE}} 
                                        @if($eachData->request_status==2)
                                        (old rate:{{$eachData->old_rate}} updated on {{date("d/m/y",strtotime($eachData->updated_at))}})
                                        @endif
                                        </td>
                                        <td class="text-center">
                                         @if($eachData->request_status==1)  
                                            <a class="btn btn-success">Request Sent</a>
                                         @else
                                            <a href="{{route('request-price-change', $eachData->id)}}" class="btn btn-success">Request For Price Change</a>
                                         @endif
                                        </td>
                                    </tr>
                                    @endforeach                            
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                              <div class="col-sm-6">
                              	<div class="dataTables_info" role="status" aria-live="polite">Showing {{$data->count()}} of {{$data->total()}} entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                        {{ $data->links() }}
                                    </nav>
                                  </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

<script type="text/javascript">


    function ConfirmDelete(id)
        {
            var x = confirm("Are you sure you want to Delete?");
            if (x)
                return true;
            else
                return false;
        }
		$(document).ready(function () {
			
            $('#example').dataTable({
				'order': [[ 0, "asc" ]],//Default Column Attribute
				'pageLength': 500,//Per Page Item
				//'sDom': 't',//Hide All Info From top and bottom
				'aTargets': [-1], //1st one, start by the right/
				"paging":   false,
        		"info":     false,
				"searching": false,
				//'dom': 'Bfrtip',
				//'buttons': ['excel', 'pdf', 'print']//['copy', 'csv', 'excel', 'pdf', 'print']
            });
        });


    setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 2000);
</script>
@include('layouts.footer')