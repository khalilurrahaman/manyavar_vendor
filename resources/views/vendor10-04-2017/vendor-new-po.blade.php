@include('layouts.vendor_header');
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                	<div class="col-sm-6"><h3 class="panel-title">
                                    <i class="fa fa-users" aria-hidden="true"></i>New PO Listing</h3></div>
                                    <div class="col-sm-6">
                                    
                                    <div class="shorting-area">
                            		
                              <!--<div class="col-sm-4 pull-right">
                                        <a class="btn btn-default btn-select">
                                            <input type="hidden" class="btn-select-input" id="" name="" value="" />
                                            <span class="btn-select-value">Select an Item</span>
                                            <span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                                        	<ul>
                                                <li class="selected">Short By</li>
                                                <li>Due Date</li>
                                                <li>Order Date</li>
                                                <li>Pending Qty</li>
                                        	</ul>
                                        </a>
                                </div>-->
                                <div class="col-sm-3 pull-right">
                                        <a href="{{route('ven-dashboard')}}" class="btn btn-success">Back</a>
                                </div>
                                <!-- <div class="col-sm-3 pull-right">
                                        <a href="javascript:void(0)" id="show-btn" class="btn btn-success">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                </div> -->
                            </div>
                                    
                                    
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                            <span id="filterResult">
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Sr. No</th>
                                                <th class="text-center">PO. No</th>
                                                <th class="text-center">PO. Date</th>
                                                <th class="text-center">Due. Date</th>
                                                <th class="text-center">Price</th>
                                                <!-- <th class="text-center">Details</th> -->
                                                <!-- <th class="text-center">Select</th> -->
                                                <th class="last-width text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <?php $inc=0; ?>
                                    @if(!empty($purchaseOrderList) && count($purchaseOrderList)>0)
                                    @foreach($purchaseOrderList as $eachOrder)
                                    <?php $inc++; 
                                    
                                    ?>
                                    <tr>
                                        <td class="text-center">
                                        <input type="checkbox" name="foo" value="{{$eachOrder->poId}}">&nbsp 
                                        {{$inc}}</td>
                                        <td class="text-center po-no<?php echo $inc;?>">{{$eachOrder->PONO}}</td>
                                        <td class="text-center po-date<?php echo $inc;?>">{{date('d/m/Y',strtotime($eachOrder->ORDDT))}}</td>
                                        <?php 
										$ex_date='00/00/0000';
										if($eachOrder->DUEDATE!='0000-00-00' && $eachOrder->DUEDATE!='')
										$ex_date=date('d/m/Y',strtotime($eachOrder->DUEDATE));
										?>
                                        <td class="text-center due-date<?php echo $inc;?>">{{$ex_date}}</td>
                                        <td class="text-center price<?php echo $inc;?>">{{$eachOrder->NETAMT}}</td>
                                       <!--  <td class="text-center">
                                           
                                        </td> -->
                                       <!--  <td class="text-center price<?php echo $inc;?>">
                                            
                                        </td> -->
                                        <td class="text-center">
                                         <a href="javascript:void(0)" onclick="poDetails({{$eachOrder->poId}})" data-toggle="modal" data-target="#details-pop">
                                         <span class="btn btn-primary">Details</span></a>

                                         
                                         <a href="javascript:void(0)" po_id="{{$eachOrder->poId}}" po_no="{{$eachOrder->PONO}}"  data-toggle="modal" data-target="#rejectpop" class="btn btn-danger reject-po">Reject</a>
                                        </td>
                                        
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td class="text-center" colspan="8">New Po Not Available!</td>
                                    </tr>
                                    @endif
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                              @if(!empty($purchaseOrderList) && count($purchaseOrderList)>0)
                              <div class="col-sm-3 pull-right">
                                <div class="dataTables_info" role="status" aria-live="polite">
                                <input type="checkbox" id="select_all" onClick="toggle(this)" /> Select All
                                <a href="javascript:void(0)" class="btn btn-success" onclick="acceptPO()">Accept</a>
                                </div>
                              </div>
                              @endif
                              <div class="col-sm-7">
                                    <nav aria-label="Page navigation" class="pull-right">
                                    </nav>
                                  </div>
                                  <span id="message-succescs"></span>
                              </div>
                              </span>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

        <!--reject popup Modal Start-->
<div id="rejectpop" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Reason for Reject </h4>
      </div>
      <div class="modal-body rejectSuccess not-selected">
        <input type="hidden" name="po_id" id="po_id" value="">
        <div class="row shorting-area pricechange">
            <div class="col-sm-12 text-center"><h4>PO. No : <strong class="po-no"></strong> <br/>
            </h4></div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <h5>Reason :</h5>
            <textarea class="form-control" id="vendorReject_remarks"></textarea>
            </div>
            <div class="clearfix">
            	<span id="rejectfail"></span>
            </div>
            <div class="col-sm-6">
                <a href="javascript:void(0)" onclick="rejectPO()" class="pup-btn">Request Submit</a>
            </div>
            <div class="col-sm-6">
                <a href="javascript:void(0)" data-dismiss="modal" class="pup-btn">Cancel
                </a>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--reject popup  Modal End-->
@include('layouts.footer');
<!-- Details Modal Start-->
<div id="details-pop" class="modal fade quotation-pop details-pop" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title PONO POPPONO"></h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
            <div class="col-sm-3"><h5>PO No. : <strong class=" PONO"></strong></h5></div>
            <div class="col-sm-3"><h5>PO Date : <strong class="po_ORDDT"></strong></h5></div>
            <div class="col-sm-3"><h5>Document No : <strong>----</strong></h5></div>
            <div class="col-sm-3"><h5>ValidIty Period : <strong class="po_TIME"></strong></h5></div>
            <div class="col-sm-3"><h5>To : <strong class="TIME"></strong></h5></div>
             <div class="col-sm-3"><h5>Credit Days : <strong>30</strong></h5></div>
             <!--<div class="col-sm-3"><h5>Form Name : <strong>C Form</strong></h5></div>-->
            <div class="col-sm-3"><h5>Currency : <strong>INR</strong></h5></div>
            <div class="col-sm-3"><h5>Exchange Rate : <strong>---</strong></h5></div>
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
            <div class="custome-table">
            <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">SL No.</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Width/Size</th>
                                                <th class="text-center">Cat 6</th>
                                                <th class="text-center">Due Date</th>
                                                <th class="text-center">Qty</th>
                                                <!--<th class="text-center">UOM</th>-->
                                                <th class="text-center">Rate</th>
                                                <th class="text-center">Amount</th>
                                            </tr>
                                        </thead>
                                    <tbody id = 'tab_data'>
                                   
                                    
                                </tbody>
                               </table>
                              </div>
                <div class="col-sm-2 accept-area">
                    <a href="javascript:void(0)" id="acceptButton"  class="btn btn-success">Accept</a>
                </div>
                <div class="col-sm-2 accept-area">
                    <a href="javascript:void(0)" id="rejectButton" class="btn btn-danger">Reject</a>
                </div>
            </div>  
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>
    <!-- Modal End-->
  </div>
</div>
<!-- Details Modal End--> 
<!--Accept and Reject Success popup Modal Start-->
<div id="updatePO-modal-body" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="updatePO-modal-body">
        
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--Accept and Reject Success popup  Modal End-->
<script language="JavaScript">
 $('.reject-po').click(function(){
    
         var po_no = $(this).attr('po_no');
         var po_id = $(this).attr('po_id');
        $('.po-no').text(po_no);
        $('#po_id').val(po_id);
    });


function toggle(source) {
  checkboxes = document.getElementsByName('foo');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}




 function acceptPO(id='') {
        //var po = confirm('Do you want to accept this PO?');
        // if(po == true){
           var selected = new Array();
           //alert(selected);
          

           $("input:checkbox[name=foo]:checked").each(function() {
                   selected.push($(this).val());
              });

           
            if(selected==''){
            	$('#message-succescs').html('Please Select Any Purchase Order!').css('color', 'red').show();
                setTimeout(function () {
                    $("#message-succescs").hide();
                }, 3000);
                return false;
            }
          // console.log(selected);
            $.ajax({
            type: 'GET', 
            url: '{{route("vendor-new-po-accept")}}',
            data: {selected:selected},
            success:function(data)
            {
                                
                if(data==1)
                {

                    $('#message-succescs').html('PO accepted successfully!').css('color', 'green').show();
                }
                else
                    $('#message-succescs').html('Please Try again!');
                    //$('.updatePO-clearfix').text('Please Try again!');
               
            setTimeout(function () {
                    window.location.reload();
                     }, 1000);
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
        // }else{
        //     window.location.reload();
        // }
    }

    function acceptEachpo(id) {
            $.ajax({
            type: 'GET', 
            url: '{{route("vendor-po-accept")}}',
            data: {id:id},
            success:function(data)
            {
                $('#updatePO-modal-body').modal('show');
                
                if(data==1)
                {

                    $('.updatePO-modal-body').html('PO accepted successfully!');
                }
                else
                    $('.updatePO-modal-body').html('Please Try again!');
               
                setTimeout(function () {
                        window.location.reload();
                }, 1000);
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
        // }else{
        //     window.location.reload();
        // }
    }

     $('#rejectButton').click(function(){
         var po_no = $('.POPPONO').text();
         var po_id = $(this).attr('popPO_id');
        $('.po-no').text(po_no);
        $('#po_id').val(po_id);
        $("#rejectpop").modal('show');
        $("#details-pop").modal('hide');
    })
    function rejectPO() {
        
        var remarks=$("#vendorReject_remarks").val();
        var po_id=$("#po_id").val();
        var ptoken = '{{csrf_token()}}';
        $.ajax({
            type: 'POST', 
            url: '{{route("vendor-po-reject")}}',
            data: {id:po_id,remarks:remarks,_token:ptoken},
            success:function(data)
            {
            	if(data==1)
                {

                    $('.rejectSuccess').html('PO rejected successfully!');
                    setTimeout(function () {
                    window.location.reload();
                     }, 1000);
                }
                else
                	$('#rejectfail').html('Please Try again!');
                    
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
    }

    function poDetails(id) {
        $.ajax({
            type: 'GET',
            url: '{{route("vendor-po-details")}}',
            data: {id:id},
            success:function(data){
                console.log(data);
                var poId = id;
                var PONO = data[0].PONO;
                var ORDDT    = new Date(data[0].ORDDT);
                var disc_rate=0;
                var disc_type='Charge Apply';
                var cst =0;
                var total_amount =data[0].GRSAMT;
                var net_amount=data[0].NETAMT;
                var cst =data[0].CHGAMT;
                var disc_rate=data[0].disc_rate;
                var disc_type=data[0].CHGNAME;      
                yr = ORDDT.getFullYear(),
                month = ORDDT.getMonth() < 9 ? '0' + (ORDDT.getMonth()+1) : (ORDDT.getMonth()+1),
                day = ORDDT.getDate()  < 10 ? '0' + ORDDT.getDate()  : ORDDT.getDate(),
                newORDDTDate = day + '/' + month + '/' + yr;
                console.log(newORDDTDate);
                
                
                
                
                var po_ORDDT = newORDDTDate;
                var TIME    = new Date(data[0].DUEDATE),
                yr = TIME.getFullYear(),
                month = TIME.getMonth() < 9 ? '0' + (TIME.getMonth()+1) : (TIME.getMonth()+1),
                day = TIME.getDate()  < 10 ? '0' + TIME.getDate()  : TIME.getDate(),
                newTIMEDate = day + '/' + month + '/' + yr;
                var po_TIME = newTIMEDate;
                $('.PONO').text(PONO);
                $('.po_ORDDT').text(po_ORDDT);
                $('.po_TIME').text(po_TIME);
                var details_html = '';
                var total_ORDQTY = 0;
                for (i = 0; i <data.length; i++) {
                    
                    var DUEDATE     = new Date(data[i].DUEDATE);
                    yr = DUEDATE.getFullYear(),
                    month = DUEDATE.getMonth() < 9 ? '0' + (DUEDATE.getMonth()+1) : (DUEDATE.getMonth()+1),
                    day = DUEDATE.getDate()  < 10 ? '0' + DUEDATE.getDate()  : DUEDATE.getDate(),
                    newDUEDATEDate = day + '/' + month + '/' + yr;
                    console.log(newDUEDATEDate);
                
                    var total_ORDQTY = parseInt(total_ORDQTY) + parseInt(data[i].ORDQTY);
                    var amount = parseInt(data[i].RATE) * parseInt(data[i].ORDQTY);

                    details_html +='<tr><td class="text-center">'+(i+1)+'</td>';
                    details_html +='<td class="text-center">'+data[i].cat1+'</td>';
                    details_html +='<td class="text-center">'+data[i].cat2+'</td>';
                    details_html +='<td class="text-center">'+data[i].cat3+'</td>';
                    details_html +='<td class="text-center">'+data[i].cat4+'</td>';
					details_html +='<td class="text-center">'+data[i].CNAME6+'</td>';
                    details_html +='<td class="text-center">'+newDUEDATEDate+'</td>';
                    details_html +='<td class="text-center">'+data[i].ORDQTY+'</td>';
                    //details_html +='<td class="text-center">'+"N"+'</td>';
                    details_html +='<td class="text-center">'+data[i].RATE+'</td>';
                    details_html +='<td class="text-center">'+amount+'</td>';
                    details_html +='</tr>';
                }
                
               
                details_html +='<tr><td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+"Total :"+'</strong></td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+total_ORDQTY+'</strong></td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+total_amount+'</strong></td>';
                    details_html +='</tr>';
					if(disc_type!==null){
                    details_html +='<tr><td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+disc_type+'</strong></td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>&nbsp;</strong></td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+disc_rate+" % on "+total_amount+'</strong></td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+cst+'</strong></td>';
					}
                    
                    details_html +='</tr>';
                    details_html +='<tr><td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+"Net Amount"+'</strong></td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+net_amount+'</strong></td>';
                    details_html +='</tr>';
                $("#tab_data").html(details_html);

                $('#acceptButton').attr('onclick','acceptEachpo('+poId+');');
                
                $('#rejectButton').attr('popPO_id',poId);

            }
        });
    }

</script>