@include('layouts.vendor_header');
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <div class="row">
                    <div class="col-lg-9 dash-left">
                    <div class="row">
                    	<div class="col-sm-5">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title pull-left"><i class="fa fa-cubes" aria-hidden="true"></i>{{count($totnewPo)}} New PO Created </h3>
                                    <h5 class="pull-right" style="cursor: pointer;"><a href="{{route('vendor-po-new')}}">View All</a></h5>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="panel-body pre-order panel-scrool">
                                    <ul>@if(count($POList)>0)
                                		@foreach($POList as $eachPOList)
                                    	<li>
                                            <div class="row">
                                                <div class="col-sm-9 pre-details">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="row">
                                                                <div class="col-sm-9 details-dash">
                                                                    <h4>PO No : &nbsp; <a href="javascript:void(0)" onclick="poDetails({{$eachPOList->poId}})" data-toggle="modal" data-target="#details-pop"><strong>{{$eachPOList->PONO}}</strong>
                                                                    <span class="btn btn-primary btn-xs">Details</span></a></h4>
                                                                    <?php 
																	$ex_date='00/00/0000';
																	if($eachPOList->l_time!='0000-00-00'  && $eachPOList->l_time!='')
																	$ex_date=date('d/m/Y',strtotime($eachPOList->l_time));
																	?>
                                                                    <h4>Date : &nbsp; <strong>{{ $ex_date }}</strong></h4>
                                                                    <h4>PO Value : &nbsp; <strong>{{$eachPOList->NETAMT}}/-</strong></h4>
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 accept-area">
                                                <a href="javascript:void(0)" class="btn btn-success" onclick="acceptPO({{$eachPOList->poId}})">Accept</a>
                                                <a href="javascript:void(0)" po_id="{{$eachPOList->poId}}" po_no="{{$eachPOList->PONO}}"  data-toggle="modal" data-target="#rejectpop" class="btn btn-danger reject-po">Reject</a>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </li>
                                        @endforeach
	                                    @else
	                                    <li class="text-center">No Records Found!</li>
	                                    @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-7 paddingL0">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title pull-left"><i class="fa fa-calendar" aria-hidden="true"></i> {{count($acceptedPOList)}} Item to be Expired in 15 Days </h3>
                                    <h5 class="pull-right">Urgent Action Required</h5>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="panel-body pre-order custome-table panel-scrool">
                                    <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" style="margin-bottom:0;">
                                        <thead>
                                            <tr>
                                                <th class="text-center">PO No</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Pending Qty</th>
                                                <th class="text-center dash-last">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <?php $inc=0; ?>
                                    @if(count($acceptedPOList)>0)
                                    @foreach($acceptedPOList as $echAcceptedList)
                                    <?php $inc++; 
                                      $sclass='vendorChangedate';
                                        $targetmodal='vendorChangedate';
                                        if(!empty($echAcceptedList->actiondtls) && count($echAcceptedList->actiondtls)>0){
                                        foreach($echAcceptedList->actiondtls as $row){
                                          
                                          if($row->action_type=='date_ext' && $row->status=='new' && $row->ICODE==$echAcceptedList->ICODE){
                                           $sclass='unablepopup';
                                           $targetmodal='';
                                          
                                        }
                                        }
                                        }
                                      ?>
		                                <tr>
		                                   <td style="display: none;" class="text-center cat-4<?php echo $inc;?>">{{$echAcceptedList->cat4}}</td>
		                                   <td style="display: none;" class="text-center ICODE<?php echo $inc;?>">{{$echAcceptedList->ICODE}}</td>
		                                   <td style="display: none;" class="text-center order-qty<?php echo $inc;?>">{{$echAcceptedList->ORDQTY}}</td>
		                                   <td style="display: none;" class="text-center received-qty<?php echo $inc;?>">{{$echAcceptedList->RCQTY}}</td>
		                                   <td style="display: none;" class="text-center po-date<?php echo $inc;?>">{{date('d/m/Y',strtotime($echAcceptedList->ORDDT))}}</td>
		                                   <td style="display: none;" class="text-center due-date<?php echo $inc;?>">{{date('d/m/Y',strtotime($echAcceptedList->DUEDATE))}}</td>
		                                    
		                                	<td class="text-center po-no<?php echo $inc;?>">{{$echAcceptedList->PONO}}</td>
		                                    <td class="text-center cat-1<?php echo $inc;?>">{{$echAcceptedList->cat1}}</td>
		                                    <td class="text-center cat-2<?php echo $inc;?>">{{$echAcceptedList->cat2}}</td>
		                                    <td class="text-center pending-qty<?php echo $inc;?>">{{$echAcceptedList->ORDQTY - ($echAcceptedList->RCQTY+$echAcceptedList->CNLQTY)}}</td>
		                                    <td class="text-center dash-last">
		                                    	<span data-toggle="modal" data-target="#{{$targetmodal}}"><a hre
		                                    	f="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Request for Date Extension" class="btn btn-success {{$sclass}}" unique-key="<?php echo $inc;?>">Date Ext</a></span>
		                                            <a href="{{route('add-git-vendor')}}" data-toggle="tooltip" data-placement="top" title="Request for Add GIT" class="btn btn-info">Add GIT</a>
		                                            
		                                    </td>
		                                </tr>
                          			@endforeach
                          			@else
                                    <tr>
                                    <td class="text-center" colspan="5">No Records Found!</td>
                                    </tr>
	                                @endif
                                    </tbody>
                               </table>
                              </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>  
                     <style type="text/css">
                                    .blnk_txt {   
                                      animation-duration: 400ms;
                                      animation-name: blink;
                                      animation-iteration-count: infinite;
                                      animation-direction: alternate;
                                    }
                                   
                                    @keyframes blink {
                                      from {
                                        opacity: 1;
                                      }

                                      to {
                                        opacity: .5;
                                      }
                                    }
                                </style>
                    <div class="row">
                    	<div class="col-sm-12">
                    		<div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title pull-left" style="padding-top:6px; padding-bottom:6px;">
                                    <i class="fa fa-list-alt" aria-hidden="true"></i> REQ Summary </h3>
                                    <h5 class="pull-right" style="cursor: pointer;"><a href="{{route('ven-rfq-request')}}">View All</a></h5>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="panel-body pre-order custome-table">
                                    <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" style="margin-bottom:0;">
                                        <thead>
                                            <tr>   
                                                <th class="text-center">Image</th>
                                                <th class="text-center">REQ ID</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Width/Size</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    @if(!empty($rfq_list) && count($rfq_list)>0)
                                     @foreach($rfq_list as $rfq)
                                      <?php 
                                     $pGalList=DB::table('product_gallery')->select('*')->where('product_id','=',$rfq->product_id)->get();
                                     #echo "<pre>";print_r($pGalList);
                                     $data_mouseover=URL::to('/img').'/pre-order-img.jpg';
                                     if($rfq->image!='' && file_exists('upload/product/'.$rfq->image))
                                      $data_mouseover=URL::to('/upload/product').'/'.$rfq->image.'#200';
                                     if(!empty($pGalList) && count($pGalList)>0)
                                     {
                                      $timeoutcount=1;
                                      foreach ($pGalList as $pGal) 
                                      {
                                        $timeout=$timeoutcount*400;
                                        $data_mouseover .=' '.URL::to('/upload/product').'/'.$pGal->product_image.'#'.$timeout.' ';
                                        $timeoutcount++;
                                      }

                                     }
                                     $data_mouseover=rtrim($data_mouseover," ");
                                     ?>
                                    <tr>                                        
                                    	<td class="text-center">
                                            @if($rfq->image!='')
                                            
                                                @if(file_exists('upload/product/'.$rfq->image))
                                                
                                                <img src="{{URL::to('/upload/product').'/'.$rfq->image }}" data-mouseover="{{$data_mouseover}}" style="height:40px;width: 40px;">
                                                @else
                                                
                                                <img src="{{URL::to('/img').'/pre-order-img.jpg' }}" data-mouseover="{{$data_mouseover}}" style="height:40px;width: 40px;">
                                                @endif
                                            @else
                                            <img src="{{URL::to('/img').'/pre-order-img.jpg' }}" data-mouseover="{{$data_mouseover}}" style="height:40px;width: 40px;">
                                            @endif
                                        </td>
                                        <td class="text-center">{{$rfq->req_id}}</td>
                                        <td class="text-center">{{$rfq->cat1}}</td>
                                        <td class="text-center">{{$rfq->cat2}}</td>
                                        <td class="text-center">{{$rfq->cat3}}</td>
                                        <td class="text-center">{{$rfq->cat4}}</td>
                                        <td class="text-center">
                                            <?php
                                            $status_text='Close';
                                            $status_class='danger';
                                            if(!empty($rfq->status) && $rfq->status==1)
                                            {
                                                $status_text='Open';
                                                $status_class='success';
                                            }
                                            ?>
                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#AddQuotation"
                                                        class="btn btn-warning addQuotationClass blnk_txt"  
                                             rfq_id="{{ $rfq->id }}" rfq_details_id="{{ $rfq->details_id }}" rfq_no="{{ $rfq->req_id }}" >Add Quotation</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                    <td class="text-center" colspan="6">No Records Found!</td>
                                    </tr>
                                    @endif
                                </tbody>
                               </table>
                              </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-bell" aria-hidden="true"></i> Notifications (@if(!empty($notificationList) && count($notificationList)>0) {{$notificationList->tot}} 
                                 @else 
                                 0
                                 @endif) </h3>
                            </div>
                           <div class="panel-body">
                                <div class="list-group">
                                	@if(!empty($notificationList) && count($notificationList)>0)
                                    @foreach($notificationList as $notification)
                                 	<a href="javascript:void(0)" onclick="redirectUrl({{$notification->id}},'{{$notification->red_url}}')" class="list-group-item">
                                        <span class="badge" style="padding: 10px;">                                        				<?php 
										$time_one = new DateTime(date('Y-m-d H:i:s')) ;
										$time_two = new DateTime($notification->created_at);
										$difference = $time_one->diff($time_two);
										echo $difference->format('%a days %h hours %i minutes');?> ago
                                        </span>
                                        
                                        <i class="fa fa-fw fa-check"></i>{{$notification->message}}
                                  			
                                    </a>
                                    @endforeach
                                    @else
                                        <i class="fa fa-fw fa-check"></i>Notification Board is Empty!
                                    @endif
                                    
                                </div>
                                <div class="text-right">
                                    <a href="{{route('all-notification')}}">View All Notifications &nbsp; <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@include('layouts.footer'); 

<!-- Modal Start-->
<div id="details-pop" class="modal fade quotation-pop details-pop" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title PONO POPPONO"></h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
        	<div class="col-sm-3"><h5>PO No. : <strong class=" PONO"></strong></h5></div>
            <div class="col-sm-3"><h5>PO Date : <strong class="po_ORDDT"></strong></h5></div>
            <div class="col-sm-3"><h5>Document No : <strong>----</strong></h5></div>
            <div class="col-sm-3"><h5>ValidIty Period : <strong class="po_TIME"></strong></h5></div>
            <div class="col-sm-3"><h5>To : <strong class="TIME"></strong></h5></div>
             <div class="col-sm-3"><h5>Credit Days : <strong>30</strong></h5></div>
             <!--<div class="col-sm-3"><h5>Form Name : <strong>C Form</strong></h5></div>-->
            <div class="col-sm-3"><h5>Currency : <strong>INR</strong></h5></div>
            <div class="col-sm-3"><h5>Exchange Rate : <strong>---</strong></h5></div>
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
        	<div class="custome-table">
            <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                            	<th class="text-center">SL No.</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Width/Size</th>
                                                <th class="text-center">Cat 6</th>
                                                <th class="text-center">Due Date</th>
                                                <th class="text-center">Qty</th>
                                                <!--<th class="text-center">UOM</th>-->
                                                <th class="text-center">Rate</th>
                                                <th class="text-center">Amount</th>
                                            </tr>
                                        </thead>
                                    <tbody id = 'tab_data'>
                                   
                                    
                                </tbody>
                               </table>
                              </div>
            	<div class="col-sm-2 accept-area">
                    <a href="javascript:void(0)" id="acceptButton"  class="btn btn-success">Accept</a>
                </div>
                <div class="col-sm-2 accept-area">
                    <a href="javascript:void(0)" id="rejectButton" class="btn btn-danger">Reject</a>
                </div>
            </div>
            
            
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!-- Modal End-->

<!--reject popup Modal Start-->
<div id="rejectpop" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Reason for Reject </h4>
      </div>
      <div class="modal-body rejectSuccess">
        <input type="hidden" name="po_id" id="po_id" value="">
        <div class="row shorting-area pricechange">
            <div class="col-sm-12 text-center"><h4>PO. No : <strong class="po-no"></strong> <br/>
            </h4></div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <h5>Reason :</h5>
            <textarea class="form-control" id="vendorReject_remarks"></textarea>
            </div>
            <div class="clearfix">
            	<span id="rejectfail"></span>
            </div>
            <div class="col-sm-6">
                <a href="javascript:void(0)" onclick="rejectPO()" class="pup-btn">Request Submit</a>
            </div>
            <div class="col-sm-6">
                <a href="javascript:void(0)" data-dismiss="modal" class="pup-btn">Cancel
                </a>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--reject popup  Modal End-->

<!--Accept and Reject Success popup Modal Start-->
<div id="updatePO-modal-body" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="updatePO-modal-body">
        
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--Accept and Reject Success popup  Modal End-->

<!--reject popup Modal Start-->
<div id="notificationDetails" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Notification Details</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
            <div class="col-sm-12 text-center"><h4>PO. No : <strong class="po-no"></strong> <br/>
            </h4></div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <h5>Reason :</h5>
            <textarea class="form-control" id="vendorReject_remarks"></textarea>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <a href="javascript:void(0)" onclick="rejectPO()" class="pup-btn">Request Submit</a>
            </div>
            <div class="col-sm-6">
                <a href="javascript:void(0)" data-dismiss="modal" class="pup-btn">Cancel
                </a>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--reject popup  Modal End-->

<!-- AddQuotation Modal Start-->
<div id="AddQuotation" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Quotation</h4>
      </div>
      <div class="modal-body">
        <div class="modal-body">
        <div class="row shorting-area pricechange">
        <h4>You are Adding your Quotate to RFQ No:<strong class="rfq_id">#</strong></h4>
        <input type="hidden" name="rfq_id" id="rfq_id" value="" />
        <input type="hidden" name="rfq_details_id" id="rfq_details_id" value="" />
        <div class="clearfix"></div>
            <div class="col-sm-6">
            <!--<h5>Price <small>(In Rupees)</small> :</h5>-->
            <input type="text" placeholder="Rate (In Rupees)" class="form-control emptyValue" id="req_price" name="req_price" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
            <span  id="req_price_message" ></span>
            </div>
            <div class="col-sm-6">
            <!--<h5>Width <small>(In Inch)</small> :</h5>-->
            <input type="text" placeholder="Width (In Inch)" class="form-control emptyValue" id="req_width" name="req_width" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
            <span  id="req_width_message" ></span>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="pup-btn submitQuotation">Submit Quotation</a>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!-- AddQuotation Modal End-->

<!--vendorChangedate Modal Start-->
<div id="vendorChangedate" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close resetmodal" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Due Date extension</h4>
      </div>
      <div class="modal-body vendorChangecutting-modal-body">
        <div class="row shorting-area pricechange">
           <input type="hidden" name="type" id="date_ext_type" value="date_ext" /> 
            
            <div class="col-sm-3"><h5>Design : <strong class="vendorChangedate-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Colour : <strong class="vendorChangedate-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Width/Size : <strong class="vendorChangedate-cat4"><!--45"--></strong></h5></div>
            
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorChangedate-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>I Code : <strong class="vendorChangedate-icode"></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorChangedate-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorChangedate-received-qty"><!--2000--></strong></h5></div>
            
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorChangedate-pending-qty"><!--1000--></strong></h5></div>
            
            <div class="col-sm-4"><h5>Order Date : <strong class="vendorChangedate-order-date"><!--23/09/2016--></strong></h5></div>
            
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
        <div class="col-sm-6">
            <h5>Current Item Due Date :</h5>
            <h4 style="text-align:left"><strong class="vendorChangedate-due-date"><!--30/10/2016--></strong></h4>
            </div>
            <div class="col-sm-6">
            <h5>Extend Due Date :</h5>
            <div class="form-group">
                <div class="input-group date">
                    <input type="text" class="form-control" placeholder="dd/mm/yy" id="vendorChangedate_extended_date" name="vendorChangedate_extended_date" />
                    <!-- <span data-provide="datepicker" class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar" id="vendorChangedate_extended_date"></span>
                    </span>
                     -->
                </div>              
                <div class="clearfix"></div>
                <span id="vendorChangecuttingdate_remarks_message"></span>
            </div>
            </div>
            
            <div class="col-sm-12">
            <h5>Reason for Extension :</h5>
            <textarea class="form-control" id="vendorChangedate_reason" name="vendorChangedate_reason"></textarea>
            <span id="vendorChangecuttingDate_remarks_message"></span>
            </div>
            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorChangedate" id="preventDouble">Submit</a>
            </div>
            <div class="col-sm-6">
            <a href="#" data-dismiss="modal" class="pup-btn resetmodal">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>

<!--vendorChangedate  Modal End-->
<script>

$('#vendorChangedate_extended_date').on('changeDate', function(ev){
    $(this).datepicker('hide');
});

var ptoken='{{csrf_token()}}';
var cutting_url='{{route("purchase-order-request")}}';

$( ".unablepopup" ).click(function() {
    
   alert('Please wait for Admin Response against your previous Request!');
});


$("#preventDouble").on("submit",function(){
    $(this).unbind("submit");
    $(this).on("submit",function(){return false;});
});

//--For Clearing input Data after modal dismiss-----//
    $(".resetmodal").bind("click", function() {
    $("input[type=text], textarea").val("");
    }); 
</script>

<!-- /#Hover Image -->
    <script type="text/javascript">$('img').on('mouseover', function() {
    var self = this,
        i = 0,
        images = $(this).data('mouseover').split(/\s+/);
    
    (function nextImage() {
        var next = images[i++ % images.length].split('#');
        $(self).data('timeout', setTimeout(function() {
            self.src = next[0];
            nextImage();
        }, next[1]));
    })();
    
}).on('mouseout', function() {
    clearTimeout($(this).data('timeout'));
    this.src = $(this).attr('src');
});
</script>
<script>

 function redirectUrl(id,url)
 {
    readNotification(id);
    setTimeout(function () {
        window.location.href=url;
    }, 1000);
    
 }


 function readNotification(id)
 {        
        $.ajax({
            type: 'GET', 
            url: '{{route("read-notification")}}',
            data: {id:id},
            success:function(data)
            {
                                
                if(data==1)
                {
                    $('#read_btn_'+id).remove();

                    var unread=$('#total_unread').text()-1;
                    if(unread!=0)
                      $('#total_unread').text(unread);
                    else
                      $('#total_unread').parent().hide();
                }
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
       
    }



$('.notificationDetails').click(function(){
 var notification_id=$(this).attr('notification_id');
 $('#notificationDetails').modal('show');
});
</script>
<script src="{{asset('js/custom/purchase_order.js')}}"></script>
<script>
    $('.reject-po').click(function(){
    
         var po_no = $(this).attr('po_no');
         var po_id = $(this).attr('po_id');
        $('.po-no').text(po_no);
        $('#po_id').val(po_id);
    });
    function acceptPO(id) {
        //var po = confirm('Do you want to accept this PO?');
        // if(po == true){
            $.ajax({
            type: 'GET', 
            url: '{{route("vendor-po-accept")}}',
            data: {id:id},
            success:function(data)
            {
            	$('#updatePO-modal-body').modal('show');
            	
                if(data==1)
                {

                    $('.updatePO-modal-body').html('PO accepted successfully!');
                }
                else
                	$('.updatePO-modal-body').html('Please Try again!');
                    //$('.updatePO-clearfix').text('Please Try again!');
               
            setTimeout(function () {
                    window.location.reload();
                     }, 1000);
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
        // }else{
        //     window.location.reload();
        // }
    }
    $('#rejectButton').click(function(){
         var po_no = $('.POPPONO').text();
         var po_id = $(this).attr('popPO_id');
        $('.po-no').text(po_no);
        $('#po_id').val(po_id);
        $("#rejectpop").modal('show');
        $("#details-pop").modal('hide');
    })
    function rejectPO() {
        
        var remarks=$("#vendorReject_remarks").val();
        var po_id=$("#po_id").val();
        var ptoken = '{{csrf_token()}}';
        $.ajax({
            type: 'POST', 
            url: '{{route("vendor-po-reject")}}',
            data: {id:po_id,remarks:remarks,_token:ptoken},
            success:function(data)
            {
            	if(data==1)
                {

                    $('.rejectSuccess').html('PO rejected successfully!');
                    setTimeout(function () {
                    window.location.reload();
                     }, 1000);
                }
                else
                	$('#rejectfail').html('Please Try again!');
                    //$('.updatePO-clearfix').text('Please Try again!');
               
            
                // if(data==1)
                // {
                //     $('.updatePO-modal-body').html('PO rejected successfully!');
                //     window.location.reload();
                // }
                // else
                //     $('.updatePO-clearfix').text('Please Try again!');
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
    }

    function poDetails(id) {
        $.ajax({
            type: 'GET',
            url: '{{route("vendor-po-details")}}',
            data: {id:id},
            success:function(data){
                console.log(data);
                var poId = id;
                var PONO = data[0].PONO;
                var ORDDT    = new Date(data[0].ORDDT);
				var disc_rate=0;
				var disc_type='Charge Apply';
				var cst =0;
                var total_amount =data[0].GRSAMT;
				var net_amount=data[0].NETAMT;
				var cst =data[0].CHGAMT;
				var disc_rate=data[0].disc_rate;
				var disc_type=data[0].CHGNAME;		
                yr = ORDDT.getFullYear(),
                month = ORDDT.getMonth() < 9 ? '0' + (ORDDT.getMonth()+1) : (ORDDT.getMonth()+1),
                day = ORDDT.getDate()  < 10 ? '0' + ORDDT.getDate()  : ORDDT.getDate(),
                newORDDTDate = day + '/' + month + '/' + yr;
                console.log(newORDDTDate);
				
				
				
				
                var po_ORDDT = newORDDTDate;
                var TIME    = new Date(data[0].DUEDATE),
                yr = TIME.getFullYear(),
                month = TIME.getMonth() < 9 ? '0' + (TIME.getMonth()+1) : (TIME.getMonth()+1),
                day = TIME.getDate()  < 10 ? '0' + TIME.getDate()  : TIME.getDate(),
                newTIMEDate = day + '/' + month + '/' + yr;
                var po_TIME = newTIMEDate;
                $('.PONO').text(PONO);
                $('.po_ORDDT').text(po_ORDDT);
                $('.po_TIME').text(po_TIME);
                var details_html = '';
                var total_ORDQTY = 0;
                for (i = 0; i <data.length; i++) {
                    
					var DUEDATE     = new Date(data[i].DUEDATE);
					yr = DUEDATE.getFullYear(),
					month = DUEDATE.getMonth() < 9 ? '0' + (DUEDATE.getMonth()+1) : (DUEDATE.getMonth()+1),
					day = DUEDATE.getDate()  < 10 ? '0' + DUEDATE.getDate()  : DUEDATE.getDate(),
					newDUEDATEDate = day + '/' + month + '/' + yr;
					console.log(newDUEDATEDate);
				
                    var total_ORDQTY = parseInt(total_ORDQTY) + parseInt(data[i].ORDQTY);
                    var amount = parseInt(data[i].RATE) * parseInt(data[i].ORDQTY);

                    details_html +='<tr><td class="text-center">'+(i+1)+'</td>';
                    details_html +='<td class="text-center">'+data[i].cat1+'</td>';
                    details_html +='<td class="text-center">'+data[i].cat2+'</td>';
                    details_html +='<td class="text-center">'+data[i].cat3+'</td>';
                    details_html +='<td class="text-center">'+data[i].cat4+'</td>';
					details_html +='<td class="text-center">'+data[i].CNAME6+'</td>';
                    details_html +='<td class="text-center">'+newDUEDATEDate+'</td>';
                    details_html +='<td class="text-center">'+data[i].ORDQTY+'</td>';
                    //details_html +='<td class="text-center">'+"N"+'</td>';
                    details_html +='<td class="text-center">'+data[i].RATE+'</td>';
                    details_html +='<td class="text-center">'+amount+'</td>';
                    details_html +='</tr>';
                }
                
               
                details_html +='<tr><td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+"Total :"+'</strong></td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+total_ORDQTY+'</strong></td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+total_amount+'</strong></td>';
                    details_html +='</tr>';
					if(disc_type!==null){
                    details_html +='<tr><td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+disc_type+'</strong></td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>&nbsp;</strong></td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+disc_rate+" % on "+total_amount+'</strong></td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+cst+'</strong></td>';
					}
					
                    details_html +='</tr>';
                    details_html +='<tr><td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+"Net Amount"+'</strong></td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+net_amount+'</strong></td>';
                    details_html +='</tr>';
                $("#tab_data").html(details_html);

                $('#acceptButton').attr('onclick','acceptPO('+poId+');');
                
                $('#rejectButton').attr('popPO_id',poId);

            }
        });
    }
</script>
<script>
var token='{{csrf_token()}}';
var q_url='{{route("add-rfq-quotation")}}';
</script>
<script src="{{asset('js/custom/rfq.js')}}"></script>
