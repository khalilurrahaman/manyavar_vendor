@include('layouts.vendor_header');
<!-- <link href="{{asset('css/uploadfilemulti.css')}}" rel="stylesheet">
 <script src="{{asset('js/multi-file-upload/jquery-1.8.0.min.js')}}"></script>
<script src="{{asset('js/multi-file-upload/jquery.fileuploadmulti.min.js')}}"></script>-->
    <!--add user popup Modal Start-->
<div class=" quotation-pop" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content none-shadow">
          <div class="modal-header">
            <h4 class="modal-title">Upload Document</h4>
          </div>
            <div class="modal-body">

                <form action="{{route('insert-document')}}" method="post" enctype="multipart/form-data">  
                    <div class="row shorting-area pricechange">
                    {{ csrf_field() }}
                    @if(isset($errors))
                            @foreach ($errors->all() as $error)
                              <div style="padding-left: 145px;" class="alert alert-success alert-dismissable">{{ $error }}</div>
                            @endforeach
                    @endif
                      <div class="col-sm-12">
                            <h5>Document Type :</h5>
                              <select class="select-drop" id="type" name="type" required>
                                <option value="">Select Document Type</option>
                                @foreach($data as $value)
                                  <option value="{{$value->id}}">{{$value->type_name}}</option>
                                @endforeach
                              </select>       
                        </div>

                        <div class="col-sm-12">
                            <h5>Month :</h5>
                            <select class="select-drop" id="month" name="month" required>
                              <option value="">Select Month</option>
                              <option value="{{date('F', mktime(0, 0, 0, date('m')-2))}}">{{date('F', mktime(0, 0, 0, date('m')-2))}}</option>
                              <option value="{{date('F', mktime(0, 0, 0, date('m')-1))}}">{{date('F', mktime(0, 0, 0, date('m')-1))}}</option>
                              <option value="{{date('F')}}">{{date('F')}}</option>
                            </select>
                        </div>
                        <div class="col-sm-12">
                            <h5 id="mulitplefileuploader">Upload</h5>
                            <div id="status"></div>
                        </div>
                        <div id="text" >
                        <div class="col-sm-8">
                              <input name="file[]" type="file" id='fileUpload' class="form-control" required />
                        </div>
                              <!-- <input type='button'  class='remove-btn btn btn-danger' value='Remove' /> -->

                            <!-- This is where the new file field will appear -->
                        </div>
                        <span class="error class_err" id="file_message" ></span>
                        <div id="text" class="col-sm-8">
                          <input type="button" id="add-file-field" name="add" value="Add More" class="btn btn-success"/>
                            <!-- This is where the new file field will appear -->
                        </div>
                        
                         
                        <div class="clearfix"></div>
                        <div class="col-sm-12">
                            <h5>Remarks :</h5>
                            <textarea name="remarks" class="form-control" required > </textarea>      
                        </div>

                        <div class="col-sm-6">
                        <input type="submit"  name="submit" onclick="return Upload()"  value="Submit" class="pup-btn">
                        </div>
                        <div class="col-sm-6">
                        <a href="{{route('document-listing')}}" class="pup-btn">Cancel
                        </a>
                        <div class="clearfix"></div>
                    </div>
                     </form> 
               
           </div>
        </div>
    </div>
</div>
<!-- <script type="text/javascript" src="{{asset('js/jquery-1.6.1.min.js')}}"></script> -->
     <script type='text/javascript'>
          $(document).ready(function(){
                // This will add new input field
               $("#add-file-field").click(function(){
                    $("#text").append("<div class='added-field'><div class='col-sm-8'><input name='file[]' type='file' class='form-control' required/></div><input type='button' class='remove-btn btn btn-danger col-sm-2' value='Remove' /></div>");
               });
               // The live function binds elements which are added to the DOM at later time
               // So the newly added field can be removed too
               //$(".remove-btn").live('click',function() {
               $(document).on('click', '.remove-btn', function(){
                    $(this).parent().remove();
               });
          });
     </script>

     <script type="text/javascript">
   /* function Upload() {
        var fileUpload = document.getElementById("fileUpload");
        var size = parseFloat(fileUpload.files[0].size / 1024).toFixed(2);
        if(size>100)
        {
          $('#file_message').text('File size greater then 3MB !').css('color','red').show();
          return false;
        }
        else 
        {
          return true;
        }   
    }*/
</script>
<!--add user popup  Modal End-->
@include('layouts.footer');
