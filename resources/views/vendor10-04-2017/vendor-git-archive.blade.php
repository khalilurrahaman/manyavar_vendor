@include('layouts.vendor_header');
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <!--<div class="row">
                    <div class="col-lg-12 title-top">
                    <div class="row page-header">
                    	<div class="col-sm-6">
                        	<h1>Vendor <small>View All GIT</small></h1>
                        </div>
                        <div class="col-sm-6">
                        	<a href="vendor-add-git.php" class="btn btn-success">Add GIT</a>
                        </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                            	<div class="row">
                                	<div class="col-sm-6">
                                        <h3 class="panel-title">
                                        <i class="fa fa-users" aria-hidden="true"></i> Vendor Archive GIT</h3>
                                    </div>
                                    <div class="col-sm-6"> 
                                        <div class="shorting-area">
                                             <div class="col-sm-3 purchase-filter pull-right">
                                                    <a href="javascript:void(0)" onclick="getCat1();" id="show-btn" class="btn btn-success">
                                                    <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                            </div>
                                            <!-- <div class="col-sm-3 pull-right">
                                                <a href="{{route('add-git-vendor')}}" class="btn btn-success">Add GIT</a>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                            <div id="mySidenavR" class="sidenavR" style="display:none;">
                                <div class="filter-right">
                            <div class="filter-area">
                            <!--<h5>Vendor List Filter</h5>-->
                                <a href="javascript:void(0)" class="closebtn" id="hide-filter">×</a>
                                   <div class="col-sm-2">                                       
                                        <select class="select-drop" id="cat1" name="category1">
                                            <option value="">Select Design</option>
                                            
                                        </select>
                                    </div>
                                    <div class="col-sm-2">                                       
                                        <select class="select-drop" id="cat2" name="category2">
                                            <option value="">Select Colour</option>   
                                        </select>
                                    </div>
                                    <div class="col-sm-2">                                       
                                        <select class="select-drop" id="cat3" name="category3">
                                            <option value="">Select Cat3</option>
                                            
                                        </select>
                                    </div>
                                    <div class="col-sm-2">                                       
                                        <select class="select-drop" id="cat4" name="category4">
                                            <option value="">Select Width/Size</option>   
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                                <div class="input-group date">
                                                    <input type="text" class="form-control date_picker" placeholder="Date From(dd/mm/yy)"  id="due_from" name="due_from"/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                                <div class="input-group date">
                                                    <input type="text" class="form-control date_picker" placeholder="Date To(dd/mm/yy)"  id="due_to" name="due_to"/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    <div class="col-sm-2">
                                        <a href="javascript:void(0)" class="btn btn-success" onclick="applyFilter();">Apply Filter</a>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-sm-1"> 
                                      <a href="javascript:void(0)" class="btn btn-success dateCheck reset" onclick="">Clear</a>
                                      <div class="clearfix"></div>  
                                     </div>

                                    <div class="clearfix"></div>        
                                </div>
                                <div class="clearfix"></div>
                                </div>    
                            </div>
                                                
                        </div>
                            <div class="clearfix"></div>
                            <span id="filterResult">
                            <div class="panel-body custome-table">
                                <div class="table-responsive view-table">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center col-sm-1">Sl. No</th>
                                                <th class="text-center col-sm-1">GIT No</th>
                                                <th class="text-center col-sm-1">Design</th>
                                                <th class="text-center col-sm-1">Colour</th>
                                                <th class="text-center col-sm-1">Cat 3</th>
                                                <th class="text-center col-sm-1">Width/Size</th>
                                                <th class="text-center col-sm-1">Date</th>
                                                <th class="text-center col-sm-1">GIT Qty</th>
                                                <th class="text-center col-sm-1">Receive Qty</th>
                                                 <th class="text-center col-sm-1">Adhoc Qty</th>
                                                <th class="text-center col-sm-1">Pending GIT Qty</th>
                                                <th class="text-center col-sm-1">Status</th>
                                                <th class="text-center col-sm-1">Last Comments</th>
                                                 <th class="text-center col-sm-1">Mark Complete Remarks</th>
                                                <th class="text-center col-sm-1">Challan No</th>
                                                <th class="text-center col-sm-4">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                                                  
                                </tbody>
                               </table>
                              </div>
                              
                            </div>
                            </span>
                        </div>
                    </div>
                    <!--<div class="col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-bell" aria-hidden="true"></i> Notification</h3>
                            </div>
                           <div class="panel-body">
                                <div class="list-group">
                                    <a href="javascript:void(0)" class="list-group-item">
                                        <span class="badge">just now</span>
                                        <i class="fa fa-fw fa-calendar"></i> Calendar updated
                                    </a>
                                    <a href="javascript:void(0)" class="list-group-item">
                                        <span class="badge">4 minutes ago</span>
                                        <i class="fa fa-fw fa-comment"></i> Commented on a post
                                    </a>
                                    <a href="javascript:void(0)" class="list-group-item">
                                        <span class="badge">23 minutes ago</span>
                                        <i class="fa fa-fw fa-truck"></i> Order 392 shipped
                                    </a>
                                    <a href="javascript:void(0)" class="list-group-item">
                                        <span class="badge">46 minutes ago</span>
                                        <i class="fa fa-fw fa-money"></i> Invoice 653 has been paid
                                    </a>
                                    <a href="javascript:void(0)" class="list-group-item">
                                        <span class="badge">1 hour ago</span>
                                        <i class="fa fa-fw fa-user"></i> A new user has been added
                                    </a>
                                    <a href="javascript:void(0)" class="list-group-item">
                                        <span class="badge">2 hours ago</span>
                                        <i class="fa fa-fw fa-check"></i> Completed task: "pick up dry cleaning"
                                    </a>
                                    <a href="javascript:void(0)" class="list-group-item">
                                        <span class="badge">yesterday</span>
                                        <i class="fa fa-fw fa-globe"></i> Saved the world
                                    </a>
                                    <a href="javascript:void(0)" class="list-group-item">
                                        <span class="badge">two days ago</span>
                                        <i class="fa fa-fw fa-check"></i> Completed task: "fix error on sales"
                                    </a>
                                </div>
                                <div class="text-right">
                                    <a href="javascript:void(0)">View All Notification &nbsp; <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        
                    </div>-->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@include('layouts.footer'); 
 <!--ViewRemarks Modal Start-->
<div id="ViewRemarks" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close ViewRemarksCloseClass" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Resolve Remarks Of GIT:<strong id="git_no_r"></strong></h4>
      </div>
      <div class="modal-body resolveGit-modal-body">
        <div class="row shorting-area pricechange">
            <div class="col-sm-12">
            <h5 id="admin_remarks_r"></h5>
            </div>
            <div class="col-sm-12">
            <h5 id="vendor_remarks_r"></h5>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--ViewRemarks  Modal End-->
<!--changewidth Modal Start-->
<div id="updateGit" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">GIT Update for Challan No:<strong id="ch_no"></strong></h4>
      </div>
      <div class="modal-body updateGit-modal-body">
        <div class="row shorting-area pricechange">
        	<div class="col-sm-6">
            <h5>Courier Name:</h5>
            <input type="text"  id="lorry_no" name="lorry_no" class="form-control"/>
            </div>
            <div class="col-sm-6">
            <h5>Docket No:</h5>
            <input type="text" id="transport_no" name="transport_no" class="form-control"/>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="pup-btn submitupdateGitClass">Request Submit</a>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--changewidth  Modal End-->

<!--ResolveGit Modal Start-->
<form method="post" action="{{route('resolve-git-records')}}" enctype="multipart/form-data" id="form_resolve">
 {{ csrf_field() }}
<div id="ResolveGit" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Resolve GIT:<strong id="git_no"></strong></h4>
      </div>
      <br />
      <div class="itemDetailsTable" style="width:90%; padding-left:10%">
       <!--Append table Here-->
       </div>
       <input type="hidden" id="per_git_id" name="per_git_id"  value=""/>
        <input type="hidden" id="pd_id" name="pd_id"  value=""/>
        <input type="hidden" id="id" name="id"  value=""/>
         <input type="hidden" id="type" name="type"  value=""/>
 
      <div class="modal-body resolveGit-modal-body">
        <div class="row shorting-area pricechange">
            <div class="col-sm-4">
                        <img src="{{asset('img/doc.png')}}" id="previewing" alt="" width="80%">
                        <div class="clearfix">
                         <span  id="item_message"  class="commonMessage"></span>   
                    </div> <br/>
                    <div class="clearfix"></div>
                    <input type="file" id="file" name="file" value=""/>
                    </div>
                  
            <div class="col-sm-8">
            <h5>Remarks:</h5>
            <textarea id="resolve_remarks" name="resolve_remarks" class="form-control"></textarea>
             <span  id="resolve_remarks_message"  class="commonMessage"></span>  
            </div>

            <div class="clearfix resolveGit-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn" onclick="resolveGit()">Submit</a>
            </div>
            <div class="col-sm-6">
             <a href="javascript:void(0);" class="pup-btn" data-dismiss="modal">Cancel</a>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
</form>
<!--ResolveGit  Modal End-->


<!--ResolvedGit Modal Start-->
<div id="ResolvedGit" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Resolve GIT:<strong id="git_no99"></strong></h4>
      </div>
      <br />
      <div class="itemDetailsTable99" style="width:90%; padding-left:10%">
       <!--Append table Here-->
       </div>
      <div class="modal-body resolveGit-modal-body">
        <div class="row shorting-area pricechange">
            <div class="col-sm-4 previewing99" style="padding-left:258px;">
             
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--ResolvedGit  Modal End-->
<script>

    function resolveGit(){
        var urls = "{{route('resolve-git-records')}}";
        var item_image = $("#file").val();
		 var resolve_remarks = $("#resolve_remarks").val();
        var imagefile = item_image.split('.').pop();
        var imgArray= ["jpeg","png","jpg","JPEG","PNG","JPG","PDF","pdf","DOC","doc","DOCX","docx"];
		
		var ret_val=true;
		$(".commonMessage").text('');//Remove previous all message
		if($.inArray(imagefile,imgArray)== -1){
            $("#item_message").text('Please Select a File!').css('color', 'red').show();
          	ret_val=false;
        }
		else
		{
			 $("#item_message").hide();
		}
		 if(resolve_remarks=='')
		 {
			 $("#resolve_remarks_message").text('Please enter remarks!').css('color', 'red');
			 ret_val=false;
		 }
		 else
		{
			 $("#resolve_remarks_message").hide();
		}
		
		 if(ret_val==false)
		 {
			 return false;
		 }
	 
        $.ajax({
            url: urls,
            type: 'POST',
            data:new FormData($("#form_resolve")[0]),
            processData: false,
            contentType: false,
            asynce: false,
            cache: false,
            success: function(data)
            {
               if(data==1)
				{
					$('.resolveGit-modal-body').html('Attachment sent sucessfully!');
					 window.location.reload();
				}
				else
					$('.resolveGit-clearfix').text('Please Try again!').css('color', 'red').show();
	 
            },error: function(data){
                alert(data.error);
            }
            

        });
    };
	
	$(function() {
        $("#file").change(function() {
            var file = this.files[0];
            var imagefile = file.type;
            var match= ["image/jpeg","image/png","image/jpg","application/pdf"];
            /*if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])|| (imagefile==match[3])))
            {
                $('#previewing').attr('src','pre-order-img.jpg');
                $("#item_message").text('Please Select Image!').css('color', 'red').show();
            return false;
            }
            else
            {
                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
            }*/
			var reader = new FileReader();
			reader.onload = imageIsLoaded;
			reader.readAsDataURL(this.files[0]);
        });
    });
    function imageIsLoaded(e) {
        $("#file").css("color","green");
		 $("#item_message").hide();
        $('#previewing').attr('src', e.target.result);
    };


$('.submitupdateGitClass').click(function(){
	
   var ch_no=$('#ch_no').text();
   var lorry_no=$('#lorry_no').val(); 
   var transport_no=$('#transport_no').val();
   
		$.ajax({
			type: 'GET', 
			url: '{{route("update-git-records")}}',
			data: {ch_no:ch_no,lorry_no:lorry_no,transport_no:transport_no},
			success:function(data)
			{
					if(data==1)
					{
        				$('.updateGit-modal-body').html('Git Updated successfully!');
						 window.location.reload();
					}
					else
						$('.updateGit-clearfix').text('Please Try again!');
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});
  
});



function getCat1(val)
    {

        $.ajax({
            type: 'GET',
            url: '{{route("get-vendor-archive-git-cat1")}}',
            data: {value:val},
            success:function(data)
            {

                $("#cat1").html(data['cat1']);
                getCat2();
            }
            /*error:function(error)
            {
                alert('Error Occured Please Try Again');
            }*/

        });

        
    }
    function getCat2(val)
    {
        $.ajax({
            type: 'GET',
            url: '{{route("get-vendor-archive-git-cat2")}}',
            data: {value:val},
            success:function(data)
            {
                $("#cat2").html(data['cat2']);
                getCat3();
            }
            /*error:function(error)
            {
                alert('Error Occured Please Try Again');
            }*/

        });

        
    }
    function getCat3(val)
    {
        $.ajax({
            type: 'GET',
            url: '{{route("get-vendor-archive-git-cat3")}}',
            data: {value:val},
            success:function(data)
            {
                $("#cat3").html(data['cat3']);
                getCat4();
            }
            /*error:function(error)
            {
                alert('Error Occured Please Try Again');
            }*/

        });

        
    }
    function getCat4(val)
    {
        $.ajax({
            type: 'GET',
            url: '{{route("get-vendor-archive-git-cat4")}}',
            data: {value:val},
            success:function(data)
            {
                $("#cat4").html(data['cat4']);
            }
            /*error:function(error)
            {
                alert('Error Occured Please Try Again');
            }*/

        });

        
    }

</script>

<script>
$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        var url = $(this).attr('href'); 
        var page=$(this).attr('href').split('page=')[1];
        
        //var po_number=$("#po_number").val();
        var category1=$("#cat1").val();
        var category2=$("#cat2").val();
        var category3=$("#cat3").val();
        var category4=$("#cat4").val();
        var due_from=$("#due_from").val();
        var due_to=$("#due_to").val();
     
        filter(page,category1,category2,category3,category4,due_from,due_to);
    });    
});


function applyFilter(){
        var category1=$("#cat1").val();
        var category2=$("#cat2").val();
        var category3=$("#cat3").val();
        var category4=$("#cat4").val();
        var due_from=$("#due_from").val();   
        var due_to=$("#due_to").val();   
        filter(1,category1,category2,category3,category4,due_from,due_to);
}

$( "#hide-filter" ).click(function() {
    
    $("#cat1").val('');
    $("#cat2").val('');
    $("#cat3").val('');
    $("#cat4").val('');
    $("#due_from").val('');
    $("#due_to").val('');
    
    filter(); 
});

function filter(page=1,category1='',category2='',category3='',category4='',due_from='',due_to=''){
    //if(po_number!='' || category1!=''  || category2!=''  || due_from!='' due_to!='')  
    $('.loading').show();
    $.ajax({
        type: 'GET', 
        url: '{{route("filter-vendor-git-archive")}}',
        data: {category1:category1,category2:category2,category3:category3,category4:category4,due_from:due_from,due_to:due_to,page:page},
        success:function(data)
        {
            $('#filterResult').html(data);
            $('.loading').hide();
        },
        error: function (error) 
        {
            
            alert('Error Occured Please Try Again');
        }
    })
}
$(document).ready(function () {        
        applyFilter();
});

//--For Clearing Filter Data Elements-----//
    $(".reset").bind("click", function() {
    $("input[type=text], textarea").val("");
    $("#cat1").val("");
    $("#cat2").val("");
    $("#cat3").val("");
    $("#cat4").val("");

    });   
</script> 
  