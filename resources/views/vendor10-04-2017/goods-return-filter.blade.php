<div class="panel-body custome-table">
                            <div class="table-responsive ">
                                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <!-- <th class="text-center">Prodict Id</th> -->
                                            <th class="text-center">Image</th>
                                            <th class="text-center">Vendor Name</th>
                                            <th class="text-center">Design</th>
                                            <th class="text-center">Colour</th>
                                            <th class="text-center">Defective Remarks</th>
                                            <th class="text-center">Quantity</th>
                                            <th class="text-center">PRN No</th>
                                            <th class="text-center">PRN Date</th>
                                            <th class="text-center">Docket No</th>
                                            <th class="text-center">Courier Name</th>
                                            <!-- <th class="text-center">Admin Remarks</th> -->
                                            <th class="text-center">Vendor Remarks</th>
                                            <th class="text-center">Last Comments</th>
                                            <th class="text-center">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($receiveList))
                                        @foreach($receiveList as $eachReturn)
                                         <?php 
                                         $pGalList=DB::table('product_gallery')->select('*')->where('product_id','=',$eachReturn->product_id)->get();
                                         #echo "<pre>";print_r($pGalList);
                                         $data_mouseover=URL::to('/img').'/pre-order-img.jpg';
                                         if($eachReturn->image!='' && file_exists('upload/temp/'.$eachReturn->image))
                                          $data_mouseover=URL::to('/upload/temp').'/'.$eachReturn->image.'#200';
                                         if(!empty($pGalList) && count($pGalList)>0)
                                         {
                                          $timeoutcount=1;
                                          foreach ($pGalList as $pGal) 
                                          {
                                            $timeout=$timeoutcount*400;
                                            $data_mouseover .=' '.URL::to('/upload/product').'/'.$pGal->product_image.'#'.$timeout.' ';
                                            $timeoutcount++;
                                          }

                                         }
                                         $data_mouseover=rtrim($data_mouseover," ");
                                         ?>
                                        <tr>
                                            <td class="text-center">
                                                @if($eachReturn->image!='')
                                                
                                                    @if(file_exists('upload/temp/'.$eachReturn->image))
                                                    
                                                    <img src="{{URL::to('/upload/temp').'/'.$eachReturn->image }}" data-mouseover="{{$data_mouseover}}" style="height:60px;width: 60px;">
                                                    @else
                                                    
                                                    <img src="{{URL::to('/img').'/pre-order-img.jpg' }}" data-mouseover="{{$data_mouseover}}" style="height:60px;width: 60px;">
                                                    @endif
                                                @else
                                                <img src="{{URL::to('/img').'/pre-order-img.jpg' }}" data-mouseover="{{$data_mouseover}}" style="height:60px;width: 60px;">
                                                @endif
                                            </td>
                                            <td class="text-center">{{$eachReturn->name}}</td>
                                            <td class="text-center">{{$eachReturn->category1}}</td>
                                            <td class="text-center">{{$eachReturn->category2}}</td>
                                            <td class="text-center" title="{{$eachReturn->reason}}">
                                            <?php
                                            $i=strlen($eachReturn->reason); 
                                            if(strlen($i>25)) 
                                                echo substr($eachReturn->reason,0,22).'...';
                                            else 
                                                echo $eachReturn->reason;

                                            ?>
                                                
                                            </td>
                                            <td class="text-center">{{$eachReturn->return_quantity}}</td>
                                            <td class="text-center">{{$eachReturn->prn_no}}</td>
                                            <td class="text-center">
                                            @if($eachReturn->prn_date!='0000-00-00')
                                            {{date('d/m/Y',strtotime($eachReturn->prn_date))}}
                                            @endif
                                            </td>
                                            <td class="text-center">{{$eachReturn->docket_no}}</td>
                                            <td class="text-center">{{$eachReturn->courier_name}}</td>
                                            <!-- <td class="text-center" title="{{$eachReturn->comments}}">
                                            <?php
                                            $i=strlen($eachReturn->comments); 
                                            if(strlen($i>25)) 
                                                echo substr($eachReturn->comments,0,22).'...';
                                            else 
                                                echo $eachReturn->comments;

                                            ?>
                                                
                                            </td> -->
                                            <td class="text-center" title="{{$eachReturn->vendor_comments}}">
                                            <?php
                                            $i=strlen($eachReturn->vendor_comments); 
                                            if(strlen($i>25)) 
                                                echo substr($eachReturn->vendor_comments,0,22).'...';
                                            else 
                                                echo $eachReturn->vendor_comments;

                                            ?>
                                            </td>
                                            <td class="text-center">
                                            
                                            @if(!empty($eachReturn->good_comments) && count(($eachReturn->good_comments))>0)
                                            <?php                                       
                                       
                                            if(strlen($eachReturn->good_comments[0]->comments)<=20)                                 
                                             echo ($eachReturn->good_comments[0]->comments);
                                            else
                                             echo substr($eachReturn->good_comments[0]->comments,0,18).'..';  

                                            ?>
                                                @if($eachReturn->status==1)
                                                <a href="{{route('goods-comments', $eachReturn->id)}}" class="btn btn-warning btn-xs pull-right">
                                                <?php echo count($eachReturn->good_comments)?>
                                                Comment(s)
                                                </a>
                                                @else
                                                <a class="btn btn-warning btn-xs pull-right">
                                                <?php echo count($eachReturn->good_comments)?>
                                                Comment(s)
                                                </a>
                                                @endif
                                            @else
                                                @if($eachReturn->status==1)
                                                <a href="{{route('goods-comments', $eachReturn->id)}}" class="btn btn-warning btn-xs pull-right">                     
                                                Comments
                                                </a>
                                                @else
                                                <a class="btn btn-warning btn-xs pull-right">
                                                Comments
                                                </a>
                                                @endif
                                            @endif
                                            </td>
                                            <td class="text-center">
                                            @if($eachReturn->status==1)<button onclick="getRequest('{{$eachReturn->id}}','{{$eachReturn->category1}}','{{$eachReturn->prn_no}}','{{$eachReturn->docket_no}}','{{$eachReturn->courier_name}}');" data-toggle="modal" data-target="#addremark" class="btn btn-success">Accept</button>
                                            @else<button href="javascript:void(0)" class="btn btn-danger">Closed</button>
                                            @endif</td>
                                        </tr> 
                                        @endforeach 
                                        @else
                                        <td valign="top" colspan="14" class="dataTables_empty">No data available in table</td>
                                        @endif                          
                                    </tbody>
                               </table>
                          </div>
                          <div class="row">
                              <div class="col-sm-6">
                                <div class="dataTables_info" role="status" aria-live="polite">Showing {{$receiveList->count()}} of {{$receiveList->total()}} entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                        {{ $receiveList->links() }}
                                    </nav>
                                  </div>
                            </div>

                        </div>
                        <!-- /#Hover Image -->
    <script type="text/javascript">$('img').on('mouseover', function() {
    var self = this,
        i = 0,
        images = $(this).data('mouseover').split(/\s+/);
    
    (function nextImage() {
        var next = images[i++ % images.length].split('#');
        $(self).data('timeout', setTimeout(function() {
            self.src = next[0];
            nextImage();
        }, next[1]));
    })();
    
}).on('mouseout', function() {
    clearTimeout($(this).data('timeout'));
    this.src = $(this).attr('src');
});
</script>