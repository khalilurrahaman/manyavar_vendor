@include('layouts.vendor_header');
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <!--<div class="row">
                    <div class="col-lg-12 title-top">
                    <div class="row page-header">
                    	<div class="col-sm-6">
                        	<h1>Admin <small>Dashboard</small></h1>
                        </div>
                        <div class="col-sm-6">
                        	<a href="javascript:void(0)" class="btn btn-success">Expand</a>
                        </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-lg-9 dash-left">
                    <div class="row">
                    	<div class="col-sm-5">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title pull-left"><i class="fa fa-cubes" aria-hidden="true"></i> New PO Creating </h3>
                                    <h5 class="pull-right">Urgent Action Required</h5>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="panel-body pre-order">
                                    <ul>
                                    <li>
                                            <div class="row">
                                                <div class="col-sm-9 pre-details">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="row">
                                                                <div class="col-sm-9 details-dash">
                                                                    <h4>PO No : &nbsp; <a href="javascript:void(0)" data-toggle="modal" data-target="#details-pop"><strong>PO/004567/16-17</strong>
                                                                    <span class="btn btn-primary btn-xs">Details</span></a></h4>
                                                                    <h4>Date : &nbsp; <strong>12/10/2016</strong></h4>
                                                                    <h4>PO Value : &nbsp; <strong>109.00/-</strong></h4>
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 accept-area">
                                                <a href="javascript:void(0)" class="btn btn-success">Accept</a>
                                                <a href="javascript:void(0)" data-toggle="modal" data-target="#rejectpop" class="btn btn-danger">Reject</a>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </li>
                                    <li>
                                            <div class="row">
                                                <div class="col-sm-9 pre-details">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="row">
                                                                <div class="col-sm-9  details-dash">
                                                                    <h4>PO No : &nbsp; <a href="javascript:void(0)" data-toggle="modal" data-target="#details-pop"><strong>PO/002589/16-17</strong>
                                                                    <span class="btn btn-primary btn-xs">Details</span></a></h4>
                                                                    <h4>Date : &nbsp; <strong>10/10/2016</strong></h4>
                                                                    <h4>PO Value : &nbsp; <strong>96.00/-</strong></h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 accept-area">
                                                <a href="javascript:void(0)" class="btn btn-success">Accept</a>
                                                <a href="javascript:void(0)" data-toggle="modal" data-target="#rejectpop" class="btn btn-danger">Reject</a>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-7 paddingR paddingL0">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title pull-left"><i class="fa fa-calendar" aria-hidden="true"></i> 2 PO Expiry is 15 Days </h3>
                                    <h5 class="pull-right">Urgent Action Required</h5>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="panel-body pre-order custome-table">
                                    <div class="table-responsive">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" style="margin-bottom:0;">
                                        <thead>
                                            <tr>
                                                <th class="text-center">PO No</th>
                                                <th class="text-center">Cat 1</th>
                                                <th class="text-center">Cat 2</th>
                                                <th class="text-center">Pending Qty</th>
                                                <th class="text-center dash-last">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <tr>
                                    	<td class="text-center">PO/003625/16-17</td>
                                        <td class="text-center">106-BLACK</td>
                                        <td class="text-center">44"</td>
                                        <td class="text-center">46.00</td>
                                        <td class="text-center dash-last">
                                        	<span data-toggle="modal" data-target="#changedate"><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" class="btn btn-success" data-original-title="Request for Date Extension">Date Ext</a></span>
                                                <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" class="btn btn-info" data-original-title="Request for Add GIT">Add GIT</a>
                                                <span data-toggle="modal" data-target="#cancellation"><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" class="btn btn-danger" data-original-title="Request for Cancellation">Cancel</a></span>
                                        </td>
                                    </tr>
                                    <tr>
                                    	<td class="text-center">PO/003625/16-17</td>
                                        <td class="text-center">106-BLACK</td>
                                        <td class="text-center">44"</td>
                                        <td class="text-center">46.00</td>
                                        <td class="text-center dash-last">
                                        	<span data-toggle="modal" data-target="#changedate"><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" class="btn btn-success" data-original-title="Request for Date Extension">Date Ext</a></span>
                                                <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" class="btn btn-info" data-original-title="Request for Add GIT">Add GIT</a>
                                                <span data-toggle="modal" data-target="#cancellation"><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" class="btn btn-danger" data-original-title="Request for Cancellation">Cancel</a></span>
                                        </td>
                                    </tr>
                                    <tr>
                                    	<td class="text-center">PO/003625/16-17</td>
                                        <td class="text-center">106-BLACK</td>
                                        <td class="text-center">44"</td>
                                        <td class="text-center">46.00</td>
                                        <td class="text-center dash-last">
                                        	<span data-toggle="modal" data-target="#changedate"><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" class="btn btn-success" data-original-title="Request for Date Extension">Date Ext</a></span>
                                                <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" class="btn btn-info" data-original-title="Request for Add GIT">Add GIT</a>
                                                <span data-toggle="modal" data-target="#cancellation"><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" class="btn btn-danger" data-original-title="Request for Cancellation">Cancel</a></span>
                                        </td>
                                    </tr>
                                    
                                    
                                </tbody>
                               </table>
                              </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>  
                    <div class="row">
                    	<div class="col-sm-12">
                    		<div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title" style="padding-top:6px; padding-bottom:6px;">
                                    <i class="fa fa-list-alt" aria-hidden="true"></i> Summary </h3>
                                </div>
                                <div class="clearfix"></div>
                                <div class="panel-body pre-order custome-table">
                                    <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" style="margin-bottom:0;">
                                        <thead>
                                            <tr>
                                            	<th class="text-center">Image</th>
                                                <th class="text-center">Cat 1</th>
                                                <th class="text-center">Cat 2</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Cat 4</th>
                                                <th class="text-center">Order Qty</th>
                                                <th class="text-center">Received Qty</th>
                                                <th class="text-center">Cancelled Qty</th>
                                                <th class="text-center">GIT Qty</th>
                                                <th class="text-center">Pending Qty</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <tr>
                                    	<td class="text-center">
                                        <div class="picture">
											<a class="small" href="#nogo" title="small image">
												<img src="{{asset('img/pre-order-img.jpg')}}" title="small image" width="56">
												<img class="large" src="{{asset('img/pre-order-img.jpg')}}" title="large image"></a>
											</div>
                                        </td>
                                        <td class="text-center">106-BLACK</td>
                                        <td class="text-center">--</td>
                                        <td class="text-center">44"</td>
                                        <td class="text-center">44"</td>
                                        <td class="text-center">126.00</td>
                                        <td class="text-center">102.00</td>
                                        <td class="text-center">0.00</td>
                                        <td class="text-center">46.00</td>
                                        <td class="text-center">00.00</td>
                                    </tr>
                                    <tr>
                                    	<td class="text-center">
                                        <div class="picture">
											<a class="small" href="#nogo" title="small image">
												<img src="{{asset('img/pre-order-img.jpg')}}" title="small image" width="56">
												<img class="large" src="{{asset('img/pre-order-img.jpg')}}" title="large image"></a>
											</div>
                                        </td>
                                        <td class="text-center">210-BROWN</td>
                                        <td class="text-center">--</td>
                                        <td class="text-center">44"</td>
                                        <td class="text-center">44"</td>
                                        <td class="text-center">188.00</td>
                                        <td class="text-center">119.00</td>
                                        <td class="text-center">0.00</td>
                                        <td class="text-center">51.00</td>
                                        <td class="text-center">00.00</td>
                                    </tr>
                                    <tr>
                                    <td class="text-center">
                                        <div class="picture">
											<a class="small" href="#nogo" title="small image">
												<img src="{{asset('img/pre-order-img.jpg')}}" title="small image" width="56">
												<img class="large" src="{{asset('img/pre-order-img.jpg')}}" title="large image"></a>
											</div>
                                        </td>
                                        <td class="text-center">269-WHITE</td>
                                        <td class="text-center">--</td>
                                        <td class="text-center">44"</td>
                                        <td class="text-center">44"</td>
                                        <td class="text-center">176.00</td>
                                        <td class="text-center">112.00</td>
                                        <td class="text-center">0.00</td>
                                        <td class="text-center">41.00</td>
                                        <td class="text-center">00.00</td>
                                    </tr>
                                    <tr>
                                    	<td class="text-center">
                                        <div class="picture">
											<a class="small" href="#nogo" title="small image">
												<img src="{{asset('img/pre-order-img.jpg')}}" title="small image" width="56">
												<img class="large" src="{{asset('img/pre-order-img.jpg')}}" title="large image"></a>
											</div>
                                        </td>
                                        <td class="text-center">106-PINK</td>
                                        <td class="text-center">--</td>
                                        <td class="text-center">44"</td>
                                        <td class="text-center">44"</td>
                                        <td class="text-center">146.00</td>
                                        <td class="text-center">129.00</td>
                                        <td class="text-center">0.00</td>
                                        <td class="text-center">66.00</td>
                                        <td class="text-center">00.00</td>
                                    </tr>
                                    <tr>
                                    	<td class="text-center">
                                        <div class="picture">
											<a class="small" href="#nogo" title="small image">
												<img src="{{asset('img/pre-order-img.jpg')}}" title="small image" width="56">
												<img class="large" src="{{asset('img/pre-order-img.jpg')}}" title="large image"></a>
											</div>
                                        </td>
                                        <td class="text-center">159-RED</td>
                                        <td class="text-center">--</td>
                                        <td class="text-center">44"</td>
                                        <td class="text-center">44"</td>
                                        <td class="text-center">261.00</td>
                                        <td class="text-center">155.00</td>
                                        <td class="text-center">0.00</td>
                                        <td class="text-center">82.00</td>
                                        <td class="text-center">00.00</td>
                                    </tr>
                                  
                                   
                                    
                                </tbody>
                               </table>
                              </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-bell" aria-hidden="true"></i> Notification</h3>
                            </div>
                           <div class="panel-body">
                                <div class="list-group">
                                    <a href="javascript:void(0)" class="list-group-item">
                                        <span class="badge">just now</span>
                                        <i class="fa fa-fw fa-calendar"></i> Calendar updated
                                    </a>
                                    <a href="javascript:void(0)" class="list-group-item">
                                        <span class="badge">4 minutes ago</span>
                                        <i class="fa fa-fw fa-comment"></i> Commented on a post
                                    </a>
                                    <a href="javascript:void(0)" class="list-group-item">
                                        <span class="badge">23 minutes ago</span>
                                        <i class="fa fa-fw fa-truck"></i> Order 392 shipped
                                    </a>
                                    <a href="javascript:void(0)" class="list-group-item">
                                        <span class="badge">46 minutes ago</span>
                                        <i class="fa fa-fw fa-money"></i> Invoice 653 has been paid
                                    </a>
                                    <a href="javascript:void(0)" class="list-group-item">
                                        <span class="badge">1 hour ago</span>
                                        <i class="fa fa-fw fa-user"></i> A new user has been added
                                    </a>
                                    <a href="javascript:void(0)" class="list-group-item">
                                        <span class="badge">2 hours ago</span>
                                        <i class="fa fa-fw fa-check"></i> Completed task: "pick up dry cleaning"
                                    </a>
                                    <a href="javascript:void(0)" class="list-group-item">
                                        <span class="badge">yesterday</span>
                                        <i class="fa fa-fw fa-globe"></i> Saved the world
                                    </a>
                                    <a href="javascript:void(0)" class="list-group-item">
                                        <span class="badge">two days ago</span>
                                        <i class="fa fa-fw fa-check"></i> Completed task: "fix error on sales"
                                    </a>
                                </div>
                                <div class="text-right">
                                    <a href="javascript:void(0)">View All Notification &nbsp; <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
        <!--Due date extension Modal Start-->
<div id="changedate" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Due Date extension</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
            <div class="col-sm-3"><h5>Cat 1 : <strong>F-954762</strong></h5></div>
            <div class="col-sm-3"><h5>Cat 2 : <strong>310</strong></h5></div>
            <div class="col-sm-3"><h5>Cat 4 : <strong>58"</strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong>324</strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong>3000</strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong>2000</strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong>0</strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong>0</strong></h5></div>
            <div class="col-sm-3"><h5>Current rate : <strong>1000</strong></h5></div>
            <div class="col-sm-4"><h5>Order Date : <strong>23/09/2016</strong></h5></div>
            <div class="col-sm-5"><h5>Current Item Due Date : <strong>30/10/2016</strong></h5></div>
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
            <div class="col-sm-6">
            <h5>Extended Date :</h5>
            <div class="form-group">
                <div class="input-group date">
                    <input type="text" class="form-control" placeholder="14/11/2016"  id="example44"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                <div class="clearfix"></div>
            </div>
            </div>
            <div class="col-sm-6">
            <h5>Reason for Extension :</h5>
            <textarea class="form-control">Slow Production</textarea>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="pup-btn">Submit Changes</a>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--Due date extension  Modal End-->
@include('layouts.footer'); 