@include('layouts.vendor_header')
<!--------------------------------Start Shorting-------------------------->
<style>
.dt-buttons{margin-right:0px !important;}
.dt-buttons>a {padding-left:18px;}
</style>
<script src="{{asset('dataTable/css/jquery.dataTables.min.css')}}"></script>
<script src="{{asset('dataTable/css/buttons.dataTables.min.css')}}"></script>

   
<script src="{{asset('dataTable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('dataTable/js/dataTables.buttons.min.js')}}"></script>   
<script src="{{asset('dataTable/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('dataTable/js/jszip.min.js')}}"></script>   
<script src="{{asset('dataTable/js/pdfmake.min.js')}}"></script>
<script src="{{asset('dataTable/js/vfs_fonts.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.print.min.js ')}}"></script>
<!--------------------------------End Shorting-------------------------->
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <!--<div class="row">
                    <div class="col-lg-12 title-top">
                    <div class="row page-header">
                    	<div class="col-sm-6">
                        	<h1>Vendor <small>Purchase Order Listing</small></h1>
                        </div>
                        <div class="col-sm-6">
                        	<a href="javascript:void(0)" class="btn btn-success">Expand</a>
                        </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-lg-12">
                    @if(isset($errors))
                        @foreach ($errors->all() as $error)

                                        <div class="btn btn-success" id="hide-me" style="width:100%;background-color: green;border: none;">{{ $error }}</div>
                                         @endforeach
                      @endif
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                	<div class="col-sm-6"><h3 class="panel-title">
                                    <i class="fa fa-users" aria-hidden="true"></i> Document Listing</h3></div>
                                    <div class="col-sm-6">
                                    <div class="shorting-area">
                              
                                            <div class="col-sm-3 pull-right">
                                                    <a href="{{route('upload-document')}}" class="btn btn-success">
                                                    <i class="fa fa-plus" aria-hidden="true"></i> Add New</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                            <div class="row shorting-area">
                                <!--<div class="col-sm-6">
                                <div class="dataTables_info" role="status" aria-live="polite">Showing 1 to 9 of 9 entries</div>
                              </div>
                              <div class="col-sm-2 pull-right">
                                        <a class="btn btn-default btn-select">
                                            <input type="hidden" class="btn-select-input" id="" name="" value="" />
                                            <span class="btn-select-value">Select an Item</span>
                                            <span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                                            <ul>
                                                <li class="selected">Short By</li>
                                                <li>Due Date</li>
                                                <li>Order Date</li>
                                                <li>Pending Qty</li>
                                            </ul>
                                        </a>
                                </div>
                            <div class="col-sm-1 pull-right">
                                        <a href="javascript:void(0)" id="show-btn" class="btn btn-success">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                </div>--> <div class="clearfix"></div>
                            <div class="col-sm-12">
                            <div id="mySidenavR" class="sidenavR" style="display:none;">
<div class="filter-right">
<div class="filter-area">
<!--<h5>Vendor List Filter</h5>-->
    <a href="javascript:void(0)" class="closebtn" id="hide-filter">×</a>
        <div class="col-sm-4">
           <input class="form-control" id="name" name="name" placeholder="Vendor Name" />
            <span class=" error class_err" id="name_msg" ></span>
        </div>

        <div class="col-sm-4">
            <input class="form-control" id="email" name="email" placeholder="Email ID" />

        </div>
             
        
        <div class="col-sm-2">
            <a href="javascript:void(0)" class="btn btn-success" onclick="applyFilter();">Apply Filter</a>
            <div class="clearfix"></div>
        </div>
<div class="clearfix"></div>        
    </div>
    <div class="clearfix"></div>
    </div>    
</div>
                            
                            </div>
                            
                            
                            </div>
                            <span id="filterResult">
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <!--<th class="text-center">Vendor Name</th>-->
                                                <th class="text-center">Document Type</th>
                                                <th class="text-center">Month</th>
                                                <th class="text-center">Remarks</th>
                                                <th class="text-center">Date & Time</th>
                                                <th class="text-center">File</th>
                                               <!-- <th class="text-center">Action</th>-->
                                                
                                            </tr>
                                        </thead>
                                    <tbody>
                                    @foreach($document_list as $data)
                                    <tr>
                                        <!--<td class="text-center">{{$data->getDocVendor->name}}</td>-->
                                        <td class="text-center">{{$data->getDocType->type_name}}</td>
                                        <td class="text-center">{{$data->month}}</td>
                                        <td class="text-center">{{$data->remarks}}</td>
                                        <td class="text-center">{{date('d/m/Y g:i A',strtotime($data->created_at))}}</td>
                                        <td class="text-center">
                                        <?php 
										$document_list_view = DB::table('document_files')->select('*')->where('document_id','=',$data->id)->orderBy('id','asc')										->get();								  $inc=0;
										if(!empty($document_list_view)){
											foreach($document_list_view as $docs){
										if($docs->document_file!='')
										{
										   if(file_exists('Document_upload/'.$docs->document_file))
										   { 
										    $inc++;
											$document_file=URL::to('/Document_upload').'/'.$docs->document_file;
											echo '<a href="'.$document_file.'" target="_blank" >Document'.$inc.'</a>'.'<br>';
										   }
										}
										
                                        }}?>

                                        </td>
                                        
                                        <!--<td class="text-center">
                                        
                                        
                                        <a href="{{route('vendor-status-deact',$data->id)}}" onclick="return ConfirmDeactive()" class="btn btn-danger">Inactive</a>
                                        

                                        </td>-->
                                    </tr>
                                    @endforeach
                                      
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                              <div class="col-sm-6">
                                <div class="dataTables_info" role="status" aria-live="polite">Showing {{$document_list->count()}} of {{$document_list->total()}} entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                        {{ $document_list->links() }}
                                    </nav>
                                  </div>
                              </div>
                              
                              </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
        <script type="text/javascript">
	
        function ConfirmActive(id)
        {
            var x = confirm("Are you sure you want to Activate?");
            if (x)
                return true;
            else
                return false;
        }function ConfirmDeactive(id)
        {
            var x = confirm("Are you sure you want to Inactivate?");
            if (x)
                return true;
            else
                return false;
        }


      



setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 2000);
        </script>
@include('layouts.footer')   