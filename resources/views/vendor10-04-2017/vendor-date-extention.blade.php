@include('layouts.vendor_header');
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <!--<div class="row">
                    <div class="col-lg-12 title-top">
                    <div class="row page-header">
                    	<div class="col-sm-6">
                        	<h1>Vendor <small>Request for Date Extension</small></h1>
                        </div>
                        <div class="col-sm-6">
                        	<a href="vendor-all-request.php" style="width:auto;" class="btn btn-warning">View All Request</a>
                        </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                	<div class="col-sm-6">
                                    	<h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i> Vendor Request for Date Extension</h3>
                                    </div>
                                    <div class="col-sm-6 changes-area">
                                    
                                    <div class="shorting-area">
                            		<!-- <div class="col-sm-3 pull-right">
                                        <a href="" style="width:auto;" class="btn btn-warning">View All Request</a>
                                </div> -->
                              <!--<div class="col-sm-4 pull-right">
                                        <a class="btn btn-default btn-select">
                                            <input type="hidden" class="btn-select-input" id="" name="" value="" />
                                            <span class="btn-select-value">Select an Item</span>
                                            <span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                                        	<ul>
                                                <li class="selected">Short By</li>
                                                <li></li>
                                                <li></li>
                                                <li></li>
                                        	</ul>
                                        </a>
                                </div>-->
                                <div class="col-sm-3 pull-right">
                                        <a href="javascript:void(0)" id="show-btn" class="btn btn-success">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                </div>
                            </div>
                                    
                                    
                                    </div>
                                </div>
                                
                                
                                
                                
                                
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                            <div class="row shorting-area">
                            <div class="col-sm-12">
                            <div id="mySidenavR" class="sidenavR" style="display:none;">
<div class="filter-right">
<div class="filter-area">
<!--<h5>Vendor List Filter</h5>-->
    <a href="javascript:void(0)" class="closebtn" id="hide-filter">×</a>
        <div class="col-sm-2">
           <select class="select-drop" id="po_number" name="po_number">
            	<option value="">PO Number</option>
                
            </select>
        </div>
        <div class="col-sm-2">
            <select class="select-drop" id="category1" name="category1">
            	<option value="">Design</option>
                
            </select>
        </div>
        <div class="col-sm-2">
           <select class="select-drop" id="category2" name="category2">
            	<option value="">Colour</option>
                
            </select>
        </div>
        <div class="col-sm-2">
             <select class="select-drop" id="category4" name="category4">
                <option value="">Width/Size</option>
                
            </select>
        </div>
        <!--<div class="col-sm-2">
            <a class="btn btn-default btn-select">
            	<input type="hidden" class="btn-select-input" id="" name="" value="" />
            	<span class="btn-select-value">Select an Item</span>
            	<span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                <ul>
                    <li class="selected">Pending Qty</li>
                    <li>----</li>
                    <li>----</li>
                    <li>----</li>
                </ul>
            </a>
            <div class="clearfix"></div>
        </div>-->
        <div class="col-sm-2">
        	<a href="javascript:void(0)" class="btn btn-success" onclick="applyFilter();">Apply Filter</a>
            <div class="clearfix"></div>
        </div>
         <div class="col-sm-1"> 
            <a href="javascript:void(0)" class="btn btn-success dateCheck reset" onclick="">Clear</a>
             <div class="clearfix"></div>  
        </div>

<div class="clearfix"></div>        
	</div>
    <div class="clearfix"></div>
    </div>    
</div>
                            
                            </div>
                            
                            
                            </div>
                            <span id="filterResult">
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Sr. No</th>
                                                <th class="text-center">PO. No</th>
                                                <th class="text-center">Item Code</th>
                                                <th class="text-center">PO. Date</th>
                                                <th class="text-center">Due. Date</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Width/Size</th>
                                                <th class="text-center">Order Qty</th>
                                                <th class="text-center">Received Qty</th>
                                                <th class="text-center">GIT Qty</th>
                                                <th class="text-center">Adhoc Qty</th>
                                                <th class="text-center">Pending Qty</th>
                                                <th class="text-center">Current Rate</th>
                                                <th class="last-width2 text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    
                                  
                                </tbody>
                               </table>
                              </div>
                              
                              </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@include('layouts.footer'); 
<!--vendorChangedate Modal Start-->
<div id="vendorChangedate" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close resetmodal" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Due Date extension</h4>
      </div>
      <div class="modal-body vendorChangecutting-modal-body">
        <div class="row shorting-area pricechange">
           <input type="hidden" name="type" id="date_ext_type" value="date_ext" /> 
            
            <div class="col-sm-3"><h5>Design : <strong class="vendorChangedate-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Colour : <strong class="vendorChangedate-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Width/Size : <strong class="vendorChangedate-cat4"><!--45"--></strong></h5></div>
            
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorChangedate-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>I Code : <strong class="vendorChangedate-icode"></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorChangedate-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorChangedate-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorChangedate-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorChangedate-pending-qty"><!--1000--></strong></h5></div>
            <div class="col-sm-3"><h5>Current rate : <strong class="vendorChangedate-current-rate"><!--37--></strong></h5></div>
            <div class="col-sm-4"><h5>Order Date : <strong class="vendorChangedate-order-date"><!--23/09/2016--></strong></h5></div>
            
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
        <div class="col-sm-6">
            <h5>Current Item Due Date :</h5>
            <h4 style="text-align:left"><strong class="vendorChangedate-due-date"><!--30/10/2016--></strong></h4>
            </div>
            <div class="col-sm-6">
            <h5>Extend Due Date :</h5>
            <div class="form-group">
                <div class="input-group date">
                    <input type="text" class="form-control" placeholder="dd/mm/yy" id="vendorChangedate_extended_date" name="vendorChangedate_extended_date" />
                    <!-- <span data-provide="datepicker" class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar" id="vendorChangedate_extended_date"></span>
                    </span>
                     -->
                </div>              
                <div class="clearfix"></div>
                <span id="vendorChangecuttingdate_remarks_message"></span>
            </div>
            </div>
            
            <div class="col-sm-12">
            <h5>Reason for Extension :</h5>
            <textarea class="form-control" id="vendorChangedate_reason" name="vendorChangedate_reason"></textarea>
            <span id="vendorChangecuttingDate_remarks_message"></span>
            </div>
            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorChangedate" id="preventDouble">Submit</a>
            </div>
            <div class="col-sm-6">
            <a href="#" data-dismiss="modal" class="pup-btn resetmodal">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>

<!--vendorChangedate  Modal End-->
<script>

$('#vendorChangedate_extended_date').on('changeDate', function(ev){
    $(this).datepicker('hide');
});

var ptoken='{{csrf_token()}}';
var cutting_url='{{route("purchase-order-request")}}';
</script>
<script>
$(document).ready(function () {
        getPoList();
        applyFilter();
    });



    function getPoList()
    {
        $.ajax({
            type: 'GET', 
            url: '{{route("vendor-po-list")}}',
            success:function(data)
            {
                $('#po_number').html(data['po']);
                getDesignList();
            },
            error: function (error) 
            {                
                alert('Error Occured Please Try Again');
            }
        });
    }

    function getDesignList()
    {
        $.ajax({
            type: 'GET', 
            url: '{{route("vendor-design-list")}}',
            success:function(data)
            {
                $('#category1').html(data['design']);
                getColorList();
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
    }

    function getColorList()
    {
        $.ajax({
            type: 'GET', 
            url: '{{route("vendor-color-list")}}',
            success:function(data)
            {
                $('#category2').html(data['color']);
                 getCategory3List();
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
    }

    function getCategory3List()//For fetch Category3 list
    {
        $.ajax({
            type: 'GET', 
            url: '{{route("vendor-category3-list")}}',
            success:function(data)
            {
                $('#category3').html(data['category3']);
                 getWidthSizeList();
            },
            error: function (error) 
            {
                
               // alert('Error Occured Please Try Again');
            }
        });
    }
    
    function getWidthSizeList()//For fetch WidthSize list
    {
        $.ajax({
            type: 'GET', 
            url: '{{route("vendor-width-size-list")}}',
            success:function(data)
            {
                $('#category4').html(data['width_size']);
            },
            error: function (error) 
            {
                
                //alert('Error Occured Please Try Again');
            }
        });
    }




$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        //$('#load a').css('color', '#dfecf6');
        //$('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');

        var url = $(this).attr('href'); 
		var page=$(this).attr('href').split('page=')[1];
		
		var po_number=$("#po_number").val();
		var category1=$("#category1").val();
		var category2=$("#category2").val();
		var category4=$("#category4").val();
	 
        filter(page,po_number,category1,category2,category4);
        //window.history.pushState("", "", url);
    });

    
});

function applyFilter(){
		var po_number=$("#po_number").val();
		var category1=$("#category1").val();
		var category2=$("#category2").val();
		var category4=$("#category4").val();
	 
        filter(1,po_number,category1,category2,category4);
}

$( "#hide-filter" ).click(function() {
	
	$("#po_number").val('');
	$("#category1").val('');
	$("#category2").val('');
	$("#category4").val('');	
	filter(); 
});

function filter(page=1,po_number='',category1='',category2='',category4=''){	
	$('.loading').show();
	$.ajax({
		type: 'GET', 
		url: '{{route("filter-date-extention")}}',
		data: {po_number:po_number,category1:category1,category2:category2,category4:category4,page:page},
		success:function(data)
		{
			$('#filterResult').html(data);
			$('.loading').hide();
		},
		error: function (error) 
		{
			
			alert('Error Occured Please Try Again');
		}
	})
}

$( ".unablepopup" ).click(function() {
    
   alert('Please wait for Admin Response against your previous Request!');
});


$("#preventDouble").on("submit",function(){
    $(this).unbind("submit");
    $(this).on("submit",function(){return false;});
});

//--For Clearing Filter Data Elements-----//
    $(".reset").bind("click", function() {
    $("#po_number").val("");
    $("#category1").val("");
    $("#category2").val("");
    $("#category4").val("");

    });   

//--For Clearing input Data after modal dismiss-----//
    $(".resetmodal").bind("click", function() {
    $("input[type=text], textarea").val("");
    });   
</script>      