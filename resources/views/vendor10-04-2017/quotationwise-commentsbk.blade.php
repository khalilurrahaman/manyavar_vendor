@include('layouts.vendor_header')
  <div id="page-wrapper">
    <div class="container-fluid inner-body-area">
      <div class="row">
        <div class="col-lg-6 margin-center">
          <div class="panel panel-default add-padding">
            <div class="panel-heading">
              <div class="row">
                    <div class="col-sm-8">
                        <h3 class="panel-title">
                        <span class="glyphicon glyphicon-comment"></span> 
                        Comments(<?php if(!empty($comments_list)) echo count($comments_list);?>)
                        </h3>
                    </div>
                    <!--<div class="col-sm-4">
                        <div class="row comments-top">
                            <div class="col-sm-6">
                                <a href="javascript:void(0)" class="btn btn-success" style="width:100%;">Accept</a>
                            </div>
                            <div class="col-sm-6">
                                <a href="javascript:void(0)" class="btn btn-danger" style="width:100%;">Reject</a>
                            </div>
                        </div>
                    </div>-->
            </div>
            </div>
            <div class="clearfix"></div>
            <div class="panel-body custome-table">
              <div class="comments-area">
                <div class="chat_window">
                  <ul class="messages"><!--Comments Append Here--></ul>
                  <div class="bottom_wrapper clearfix">
                    <div class="message_input_wrapper">
                      <input class="message_input" placeholder="Type your message here..." />
                    </div>
                    <div class="send_message">
                      <div class="icon"></div>
                      <div class="text">Send</div>
                    </div>
                  </div>
                </div>
                <div class="message_template">
                  <li class="message">
                    <div class="avatar"></div>
                    <div class="text_wrapper">
                      <div class="text">vvvbvb</div>
                    </div>
                  </li>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row --> 
    </div>
    <!-- /.container-fluid --> 
  </div>
  <input type="hidden" id="sucess_status" name="sucess_status" value="0"/>
  <!-- /#page-wrapper -->
@include('layouts.footer')
<!--Added By Khalilur Rahaman-->
<script>
var token_cmnts='{{csrf_token()}}';
var c_url='{{route("add-quotation-comments")}}';
</script>
<!-------------------------Comments Section Start---------------------->
<script>
(function () {
    var Message;/////////////////Append Html
    Message = function (arg) {
        this.text = arg.text, this.message_side = arg.message_side;
        this.draw = function (_this) {
            return function () {
                var $message;
                $message = $($('.message_template').clone().html());
                $message.addClass(_this.message_side).find('.text').html(_this.text);
                $('.messages').append($message);
                return setTimeout(function () {
                    return $message.addClass('appeared');
                }, 0);
            };
        }(this);
        return this;
    };
	
	
	
    $(function () {
        var getMessageText, message_side, sendMessage;
        message_side = 'right';
		
		
		
		
		
		
		
		
        sendMessage = function (text,allignment='right') {//For Append Comment
            var $messages, message;
            if (text.trim() === '') {
                return;
            }
            $('.message_input').val('');//Set Empty Input Comments Box
            $messages = $('.messages');
            //message_side = message_side === 'left' ? 'right' : 'left';
			message_side=allignment;
            message = new Message({
                text: text,
                message_side: message_side
            });
            message.draw();
            return $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);
        };
		
		
		//For New Comments
        getMessageText = function () {
			//alert(5);
            var $message_input;
            $message_input = $('.message_input');
			var cmnts=$message_input.val();
			var quotation_id='<?php echo $quotation_id;?>';
				
//Insert New Comments into Database Start
			$.ajax({
				type: 'POST', 
				url: c_url,
				data: {quotation_id:quotation_id,comments:cmnts, _token:token_cmnts},
				success:function(data)
				{
					
            				$('#sucess_status').val(data);
				},
				error: function (error) 
				{
					
					alert('Error Occured Please Try Again');
				}
			});
			if($('#sucess_status').val()==1)
			return cmnts;			
//Insert New Comments into Database End
        };
		
        $('.send_message').click(function (e) {
            return sendMessage(getMessageText());
        });
		
		
		
        $('.message_input').keyup(function (e) {
            if (e.which === 13) {
                return sendMessage(getMessageText());
            }
        });
		
		
		///Comments List
		<?php if(!empty($comments_list))
		{
		$inc=0;
		$user_id=Auth::user()->id;
			foreach($comments_list as $comments)
			{
			$comments_text=$comments->comments;
			$cmnts_user_id=$comments->user_id;
			$allignment='left';
			if($user_id==$cmnts_user_id)
			  $allignment='right';
			$timeout=500*$inc;
			$inc++;
		?>
			setTimeout(function () {
				return sendMessage('<?php echo $comments_text;?>','<?php echo $allignment;?>');
			}, <?php echo $timeout;?>);
       <?php 
	   		}
	   }?>
		
		
    });
}.call(this));
</script>
<!-------------------------Comments Section End---------------------->