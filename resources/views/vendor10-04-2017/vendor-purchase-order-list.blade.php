@include('layouts.vendor_header')
<div id="page-wrapper">
<div class="container-fluid inner-body-area">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default add-padding">
                <div class="panel-heading">
                    <div class="row">
                    	<div class="col-sm-6"><h3 class="panel-title">
                        <i class="fa fa-users" aria-hidden="true"></i> Vendor Purchase Order Listing</h3></div>
                        <div class="col-sm-6">
                        
                        <div class="shorting-area">
                		
                </div>
                        
                        
                        </div>
                    </div>
                    
                    
                    
                </div>
                <div class="clearfix"></div>
                <div class="panel-body custome-table">
                <div class="tab-filter">
              <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab1default" data-toggle="tab" onclick="clearPrevData('po');">PO List by PO No & PO Date</a></li>
                    <li><a href="#tab3default" data-toggle="tab" onclick="clearPrevData('oth');">PO List by Design, Color, Width / Size & Cat6</a></li>

                     <li><a href="#tab4default" data-toggle="tab" onclick="clearPrevData('next15');">PO to be expired in next 15 days</a></li>

                    <li><a href="#tab5default" data-toggle="tab" onclick="clearPrevData('prev15');">PO expired in last 15 Days</a></li>
                    
                  </ul>
                </div>
                <div class="panel-body">
                  <div class="tab-content">
                    <div class="tab-pane fade in active" id="tab1default">
                      <div class="row shorting-area">
                        <div class="filter-area">
                         <div class="col-sm-3">
                            <div class="form-group">
                              <div class="input-group date">
                                <input type="text" class="form-control date_picker" placeholder="PO date from(dd/mm/yyyy)"  id="due_from" name="due_from"/>
                               </div>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-group">
                              <div class="input-group date">
                                <input type="text" class="form-control date_picker" placeholder="PO date to(dd/mm/yyyy)"  id="due_to" name="due_to"/>
                                 </div>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                          <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="Enter PO No"  id="po_number" name="po_number"/>
                          </div>
                 
                          <div class="col-sm-2"> <a href="javascript:void(0)" class="btn btn-success dateCheck" onclick="applyFilter();">Apply Filter</a><br>
                           
                            <div class="clearfix"></div>
                          </div>
                         <div class="col-sm-1"> 
                          <a href="javascript:void(0)" class="btn btn-success dateCheck reset" onclick="">Clear</a>
                          <div class="clearfix"></div>  
                         </div>


                          <div class="clearfix"></div>
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane fade" id="tab3default">
                      <div class="row shorting-area">
                        <div class="filter-area">
                          <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="Enter Design"  id="category1" name="category1"/>
                          </div>
                          <div class="col-sm-2">
                             <input type="text" class="form-control" placeholder="Enter Color"  id="category2" name="category2"/> 
                          </div>
                          <div class="col-sm-2">
                             <input type="text" class="form-control" placeholder="Enter Width/Size"  id="category4" name="category4"/> 
                          </div>
                          <div class="col-sm-2">
                             <input type="text" class="form-control" placeholder="Enter Cat6"  id="category6" name="category6"/> 
                          </div>
                          <div class="col-sm-2"> <a href="javascript:void(0)" class="btn btn-success" onclick="applyFilter();">Apply Filter</a>
                            <div class="clearfix"></div>
                          </div>
                          <div class="col-sm-1"> 
                          <a href="javascript:void(0)" class="btn btn-success dateCheck reset" onclick="">Clear</a>
                          <div class="clearfix"></div>  
                         </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                    </div>


                    <div class="tab-pane fade" id="tab4default">
                       <input id="next15" name="next15" type="hidden" value="" >
                    </div>



                    <div class="tab-pane fade" id="tab5default">
                      <input id="prev15" name="prev15" type="hidden" value="">
                    </div>



                  </div>
                 <div class="empty_search_msg"></div>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
 <div class="clearfix"></div>
                  
                            <span id="filterResult">
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Sr. No</th>
                                                <th class="text-center">PO. No</th>
                                                <th class="text-center">PO. Date</th>
                                                <th class="text-center">Due. Date</th>
                                                <th class="text-center">Item Code</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Width/Size</th>
                                                <th class="text-center">Cat 6</th>
                                                <th class="text-center">Price</th>
                                                <th class="text-center">Order Qty</th>
                                                <th class="text-center">Received Qty</th>
                                                <th class="text-center">Cancelled Qty</th>
                                                <th class="text-center">GIT Qty</th>
                                                <th class="text-center">Adhoc Qty</th>
                                                <th class="text-center">Pending Qty</th>
                                                <th class="last-width text-center">Action</th>
                                            </tr>
                                        </thead>
                                          <tr><td style="font-size: 18px;" class="text-center" colspan="17" >Please enter search keyword! </td></tr>
                                        <tbody>
                                        
                                        </tbody>
                                   </table>
                              </div>
                          
                              </span>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@include('layouts.footer') 


<!--add vendorChangecutting Modal Start-->
<div id="vendorChangecutting" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close resetmodal" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Cutting</h4>
      </div>
      <div class="modal-body vendorChangecutting-modal-body">
        <div class="row shorting-area pricechange">
            <input type="hidden" name="type" id="cutting_type" value="cutting" />
        	<div class="col-sm-3"><h5>Design : <strong class="vendorChangecutting-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Colour : <strong class="vendorChangecutting-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Width/Size : <strong class="vendorChangecutting-cat4"><!--45"--></strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorChangecutting-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>I Code : <strong class="vendorChangecutting-icode"></strong></h5></div>
            <div class="col-sm-3"><h5>Date : <strong class="vendorChangecutting-date"><!--12/10/2016--></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorChangecutting-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorChangecutting-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorChangecutting-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorChangecutting-pending-qty"><!--1000--></strong></h5></div>
            <div class="clearfix"></div>
            <hr>
        	<div class="clearfix"></div>
           <!--  <div class="col-sm-6 newpo" id="newpo">
            <h5>New PO :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestcutting_new_po" name="vendorRequestcutting_new_po"/>            
            </div> -->
            <div class="col-sm-12">
            <h5>Remarks :</h5>
            <textarea class="form-control" id="vendorChangecutting_remarks" name="vendorChangecutting_remarks" placeholder="Remarks"></textarea>
            <span id="vendorChangecutting_remarks_message"></span>
            </div>
            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorChangecutting preventDouble" id=""> Submit</a>
            </div>
        <div class="col-sm-6">
            <a href="#" data-dismiss="modal" class="pup-btn resetmodal">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--add vendorChangecutting  Modal End-->


<!--vendorChangeprice Modal Start-->
<div id="vendorChangeprice" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close resetmodal" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Rate Changes</h4>
      </div>
      <div class="modal-body vendorChangecutting-modal-body">
        <div class="row shorting-area pricechange">
        <input type="hidden" name="type" id="rate_change_type" value="rate_change" />            
            <div class="col-sm-3"><h5>Design : <strong class="vendorChangeprice-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Colour : <strong class="vendorChangeprice-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Width/Size : <strong class="vendorChangeprice-cat4"><!--45"--></strong></h5></div>
             <div class="col-sm-3"><h5>Order Date : <strong class="vendorChangeprice-po-date"><!--45"--></strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorChangeprice-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>I Code : <strong class="vendorChangeprice-icode"></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorChangeprice-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorChangeprice-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorChangeprice-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorChangeprice-pending-qty"><!--1000--></strong></h5></div>
            <div class="col-sm-3"><h5>Current rate : <strong class="vendorChangeprice-current-rate"><!--300--></strong></h5></div>
            
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
       <!--  <div class="col-sm-6 newpo" id="newpo">
            <h5>New PO :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestrate_new_po" name="vendorRequestrate_new_po"/>            
            </div> -->
        	<div class="col-sm-6">
            <h5>New Rate :</h5>
            <input type="text" placeholder="" class="form-control" onkeypress="javascript:return isNumber(event)" id="vendorChangecutting_new_requested_rate" name="vendorChangecutting_new_requested_rate"/>
            <span id="vendorChangeRate_remarks_message"></span>
            </div>
            <div class="col-sm-6">
            <h5>Reason for change in rate :</h5>
            <textarea class="form-control" id="vendorChangecutting_reason" name="vendorChangecutting_reason"></textarea>
             <span id="vendorChangeRateReason_remarks_message"></span>
            </div>

            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorChangeprice preventDouble" id="preventDouble">Submit</a>
            </div>
        <div class="col-sm-6">
            <a href="#" data-dismiss="modal" class="pup-btn resetmodal">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorChangeprice  Modal End-->


<!--vendorChangewidth Modal Start-->
<div id="vendorChangewidth" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close resetmodal" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Width Changes</h4>
      </div>
      <div class="modal-body vendorChangecutting-modal-body">
        <div class="row shorting-area pricechange">
        <input type="hidden" name="type" id="width_change_type" value="width_change" />
        	<div class="col-sm-3"><h5>Design : <strong class="vendorChangewidth-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Colour : <strong class="vendorChangewidth-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Width/Size : <strong class="vendorChangewidth-cat4"><!--45"--></strong></h5></div>
             <div class="col-sm-3"><h5>Order Date : <strong class="vendorChangewidth-po-date"><!--45"--></strong></h5></div>
            
             <div class="col-sm-3"><h5>PO. No : <strong class="vendorChangewidth-po-no"><!--234--></strong></h5></div>
              <div class="col-sm-3"><h5>I Code : <strong class="vendorChangewidth-icode"></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorChangewidth-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorChangewidth-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorChangewidth-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorChangewidth-pending-qty"><!--1000--></strong></h5></div>
            <div class="col-sm-3"><h5>Current rate : <strong class="vendorChangewidth-current-rate"><!--37--></strong></h5></div>
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
           <!--  <div class="col-sm-6 newpo" id="newpo">
            <h5>New PO :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestwidth_new_po" name="vendorRequestwidth_new_po"/>            
            </div> -->
        	<div class="col-sm-6">
            <h5>New Rate :</h5>
            <input type="text" placeholder="" onkeypress="javascript:return isNumber(event)" class="form-control" id="vendorChangewidth_new_requested_rate" name="vendorChangewidth_new_requested_rate"/>
            <span id="vendorNewReqrate_remarks_message"></span>
            </div>
            <div class="col-sm-6">
            <h5>New Width :</h5>
            <input type="text" placeholder="" onkeypress="javascript:return isNumber(event)" class="form-control" id="vendorChangewidth_new_width" name="vendorChangewidth_new_width"/>
            <span id="vendorChangecuttingwidth_remarks_message"></span>
            </div><div class="clearfix"></div>
            <div class="col-sm-12">
            <h5>Reason for change in Width :</h5>
            <textarea class="form-control" id="vendorChangewidth_reason" name="vendorChangewidth_reason"></textarea>
            <span id="vendorChangecutting_width_message"></span>
            </div>
            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorChangewidth preventDouble" id="preventDouble">Submit</a>
            </div>
        <div class="col-sm-6">
            <a href="#" data-dismiss="modal" class="pup-btn resetmodal">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorChangewidth  Modal End-->


<!--vendorChangedate Modal Start-->
<div id="vendorChangedate" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close resetmodal" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Due Date extension</h4>
      </div>
      <div class="modal-body vendorChangecutting-modal-body">
        <div class="row shorting-area pricechange">
           <input type="hidden" name="type" id="date_ext_type" value="date_ext" /> 
            
            <div class="col-sm-3"><h5>Design : <strong class="vendorChangedate-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Colour : <strong class="vendorChangedate-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Width/Size : <strong class="vendorChangedate-cat4"><!--45"--></strong></h5></div>
            
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorChangedate-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>I Code : <strong class="vendorChangedate-icode"></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorChangedate-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorChangedate-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorChangedate-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorChangedate-pending-qty"><!--1000--></strong></h5></div>
            <div class="col-sm-3"><h5>Current rate : <strong class="vendorChangedate-current-rate"><!--37--></strong></h5></div>
            <div class="col-sm-4"><h5>Order Date : <strong class="vendorChangedate-order-date"><!--23/09/2016--></strong></h5></div>
            
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
        <!-- <div class="col-sm-6 newpo" id="newpo">
            <h5>New PO :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestdate_new_po" name="vendorRequestdate_new_po"/>            
            </div> -->
        <div class="col-sm-6">
            <h5>Current Item Due Date :</h5>
            <h4 style="text-align:left"><strong class="vendorChangedate-due-date"><!--30/10/2016--></strong></h4>
            </div>
        	<div class="col-sm-6">
            <h5>Extend Due Date :</h5>
            <div class="form-group">
                <div class="input-group date">
                    <input type="text" class="form-control" placeholder="dd/mm/yy" id="vendorChangedate_extended_date" name="vendorChangedate_extended_date" />
                   <!--  <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span> -->
                    
                </div>
                <div class="clearfix"></div>
                <span id="vendorChangecuttingdate_remarks_message"></span>
            </div>
            </div>
            
            <div class="col-sm-12">
            <h5>Reason for Extension :</h5>
            <textarea class="form-control" id="vendorChangedate_reason" name="vendorChangedate_reason"></textarea>
            <span id="vendorChangecuttingDate_remarks_message"></span>
            </div>
            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorChangedate preventDouble" id="preventDouble">Submit</a>
            </div>
        <div class="col-sm-6">
            <a href="#" data-dismiss="modal" class="pup-btn resetmodal">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorChangedate  Modal End-->


<!--vendorCancellation order Modal Start-->
<div id="vendorCancellation" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close resetmodal" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Cancellation</h4>
      </div>
      <div class="modal-body vendorChangecutting-modal-body">
        <div class="row shorting-area pricechange">
        <input type="hidden" name="type" id="cancel_type" value="cancel" />             
            <div class="col-sm-3"><h5>Design : <strong class="vendorCancellation-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Colour : <strong class="vendorCancellation-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Width/Size : <strong class="vendorCancellation-cat4"><!--45"--></strong></h5></div>
             <div class="col-sm-3"><h5>Order Date : <strong class="vendorCancellation-po-date"><!--45"--></strong></h5></div>
             <div class="col-sm-3"><h5>Due Date : <strong class="vendorCancellation-due-date"></strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorCancellation-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>I Code : <strong class="vendorCancellation-icode"></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorCancellation-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorCancellation-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorCancellation-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorCancellation-pending-qty"><!--1000--></strong></h5></div>            
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
        	<div class="col-sm-6">
            <h5>Cancel Qty :</h5>
            <input type="text" placeholder="" onkeypress="javascript:return isNumber(event)" class="form-control" id="vendorCancellation_number" name="vendorCancellation_number"/>
             <span id="vendorChangecuttingcancel_remarks_message"></span>
             <span id="vendorcancelqty_message"></span>
            </div>
            <div class="col-sm-6">
            <h5>Reason for Cancellation :</h5>
            <textarea class="form-control" id="vendorCancellation_reason" name="vendorCancellation_reason"></textarea>
            <span id="vendorChangecuttingCancel_remarks_message"></span>
            </div>
            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorCancellation preventDouble" id="preventDouble">Submit</a>
            </div>
        <div class="col-sm-6">
            <a href="#" data-dismiss="modal" class="pup-btn resetmodal">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorCancellation order  Modal End-->




<!--vendorAddremarks Modal Start-->
<div id="vendorAddremarks" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close resetmodal" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Remarks</h4>
      </div>
      <div class="modal-body vendorAddremarks-modal-body">
        <div class="row shorting-area pricechange">
         <input type="hidden" name="type" id="others_type" value="others" />
        	<div class="col-sm-3"><h5>Design : <strong class="vendorAddremarks-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Colour : <strong class="vendorAddremarks-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Width/Size : <strong class="vendorAddremarks-cat4"><!--45"--></strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorAddremarks-po-no"><!--234--></strong></h5></div>
             <div class="col-sm-3"><h5>I Code : <strong class="vendorAddremarks-icode"></strong></h5></div>
            <div class="col-sm-3"><h5>Date : <strong class="vendorAddremarks-date"><!--12/10/2016--></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorAddremarks-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorAddremarks-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorAddremarks-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorAddremarks-pending-qty"><!--1000--></strong></h5></div> 
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix vendorAddremarks-modal-body-clearfix"></div>
       <!--  <div class="col-sm-6 newpo" id="newpo">
            <h5>New PO :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestothers_new_po" name="vendorRequestothers_new_po"/>            
            </div> -->
            <div class="col-sm-12">
            <h5>Add Remarks :</h5>
            <textarea class="form-control" id="vendorAddremarks_remarks" name="vendorAddremarks_remarks"></textarea>
            <span id="vendorOthers_remarks_message"></span>
            </div>
            <span id="vendorAddremarks_remarks_message"></span>
            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorAddremarks preventDouble" id="preventDouble">Submit</a>
            </div>
            <div class="col-sm-6">
            <a href="#" data-dismiss="modal" class="pup-btn resetmodal">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorAddremarks  Modal End-->  
<!--add vendorSamplerequest Modal Start-->
<div id="vendorSamplerequest" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close resetmodal" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Sample</h4>
      </div>
      <div class="modal-body vendorSamplerequest-modal-body">
        <div class="row shorting-area pricechange">
            <input type="hidden" name="type" id="sample_type" value="sample" />
            <div class="col-sm-3"><h5>Design : <strong class="vendorSamplerequest-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Colour : <strong class="vendorSamplerequest-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Width/Size : <strong class="vendorSamplerequest-cat4"><!--45"--></strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorSamplerequest-po-no"><!--234--></strong></h5></div>
             <div class="col-sm-3"><h5>I Code : <strong class="vendorSamplerequest-icode"></strong></h5></div>
            <div class="col-sm-3"><h5>Date : <strong class="vendorSamplerequest-date"><!--12/10/2016--></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorSamplerequest-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorSamplerequest-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorSamplerequest-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorSamplerequest-pending-qty"><!--1000--></strong></h5></div>
            <div class="clearfix"></div>
            <hr>
          
            <div class="col-sm-6 docet" id="docet">
            <h5>Docket No :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestaccept_new_docket" name="vendorRequestaccept_new_docket"/>
            <span id="vendorRequestaccept_docket-remarks_message"></span>
            </div>
          
            <div class="col-sm-6 courier" id="courier">
            <h5>Courier Name :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestaccept_new_courier" name="vendorRequestaccept_new_courier"/>
            <span id="vendorRequestaccept_courier-remarks_message"></span>
            </div>
            <div class="col-sm-12">
            <h5>Remarks :</h5>
            <textarea class="form-control" id="vendorSamplerequest-remarks" name="vendorSamplerequest-remarks" placeholder="Remarks"></textarea>
            <span id="vendorSample_remarks_message"></span>
            </div>
            <div class="clearfix vendorSamplerequest-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorSamplereq preventDouble" id="preventDouble">Submit</a>
            </div>
            <div class="col-sm-6">
            <a href="#" data-dismiss="modal" class="pup-btn resetmodal">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--add vendorChangecutting  Modal End-->
<script>

  var count=0;
	$(document).ready(function () {   //For default data load          
  		clearPrevData();
    });
    /*function getPoList()
    {
        $('.loading').show();
        $.ajax({
            type: 'GET', 
            url: '{{route("vendor-po-list")}}',
            success:function(data)
            {
                $('#po_number').html(data['po']);
                //getDesignList();
                $('.loading').hide();
                
            },
            error: function (error) 
            {                
                alert('Error Occured Please Try Again');
            }
        });
    }

    function getDesignList()
    {
        $('.loading').show();
        $.ajax({
            type: 'GET', 
            url: '{{route("vendor-design-list")}}',
            success:function(data)
            {
                $('#category1').html(data['design']);
                getColorList();
                
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
    }

    function getColorList()
    {
        $.ajax({
            type: 'GET', 
            url: '{{route("vendor-color-list")}}',
            success:function(data)
            {
                $('#category2').html(data['color']);
				        getWidthSizeList();
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
    }
 
	
	function getWidthSizeList()//For fetch WidthSize list
    {
        $.ajax({
            type: 'GET', 
            url: '{{route("vendor-width-size-list")}}',
            success:function(data)
            {
                $('#category4').html(data['width_size']);
                $('.loading').hide();
            },
            error: function (error) 
            {
                
                //alert('Error Occured Please Try Again');
            }
        });
    }*/

  function clearPrevData(pos='')//For Clear Data of prev search
	{

      if(pos=='oth' && count==0)
      {
        count++;
      }
      

      $("#next15").val('');
      $("#prev15").val('');  
			$("#po_number").val('');
			$("#category1").val('');
			$("#category2").val('');
			$("#category3").val('');
			$("#category4").val('');
			$("#category6").val('');
			$("#due_from").val('');
			$("#due_to").val('');		

      if(pos=='next15')
      {
        $("#next15").val(1);
        $("#prev15").val('');
        applyFilter();
      }
      else if(pos=='prev15')
      {
        
        $("#prev15").val(2);
        $("#next15").val('');
        applyFilter();
      }


      var blank_msg='<tr><td style="font-size: 18px;" class="text-center" colspan="17" >Please enter search keyword!! </td></tr>';

      $("#search_result_body").html(blank_msg); 
			//filter(); 
	}



$('#vendorChangedate_extended_date').on('changeDate', function(ev){
    $(this).datepicker('hide');
});

// $('#vendorRequestaccept_new_prndate').on('changeDate', function(ev){
//     $(this).datepicker('hide');
// });

$('#due_from').on('changeDate', function(ev){
    $(this).datepicker('hide');
	
});

$('#due_to').on('changeDate', function(ev){
    $(this).datepicker('hide');
	
});

/*$(document).on('blur', '#due_from', function(e) {

var from = $("#due_from").val().split("/");; 
var strt = new Date(from[2], from[1] - 1, from[0]);
var end =new Date(strt.getFullYear(),strt.getMonth(),strt.getDate()+15);
alert(end);
$('#due_to').datepicker({endDate: end});

alert(1);	
});
*/




var ptoken='{{csrf_token()}}';
var cutting_url='{{route("purchase-order-request")}}';
</script>
<script src="{{asset('js/custom/purchase_order.js')}}"></script> 

<script>
$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        var url = $(this).attr('href'); 
		var page=$(this).attr('href').split('page=')[1];
		
		var po_number=$("#po_number").val();
		var category1=$("#category1").val();
		var category2=$("#category2").val();
		var category3=$("#category3").val();
		var category4=$("#category4").val();
		var category6=$("#category6").val();
		var due_from=$("#due_from").val();
		var due_to=$("#due_to").val();

    var next15=$("#next15").val();
    var prev15=$("#prev15").val();

	 
        filter(page,po_number,category1,category2,category3,category4,category6,due_from,due_to,next15,prev15);
    });    
});


function applyFilter()
{
		var po_number=$("#po_number").val();
		var category1=$("#category1").val();
		var category2=$("#category2").val();
		var category3=$("#category3").val();
		var category4=$("#category4").val();
		var category6=$("#category6").val();
		var due_from=$("#due_from").val();
		var due_to=$("#due_to").val();

    var next15=$("#next15").val();
    var prev15=$("#prev15").val();

   

		$(".empty_search_msg").hide();
		if(po_number=='' && category1=='' && category2=='' && category4=='' && category6=='' && due_from=='' && due_to=='' && next15=='' && prev15=='')
			$(".empty_search_msg").text('Please Select at least one criteria for filter!').css('color', 'red').show();
		else	 
        filter(1,po_number,category1,category2,category3,category4,category6,due_from,due_to,next15,prev15);
}



function filter(page=1,po_number='',category1='',category2='',category3='',category4='',category6='',due_from='',due_to='',next15='',prev15=''){

	//if(po_number!='' || category1!=''  || category2!=''  || due_from!='' due_to!='')	

	$('.loading').show();
	$.ajax({
		type: 'GET', 
		url: '{{route("filter-purchase-ordering")}}',
		data: {po_number:po_number,category1:category1,category2:category2,category3:category3,category4:category4,category6:category6,due_from:due_from,due_to:due_to,page:page,next15:next15,prev15:prev15},
		success:function(data)
		{
			$('#filterResult').html(data);
			$('.loading').hide();
		},
		error: function (error) 
		{
			
			alert('Error Occured Please Try Again');
		}
	})
}
</script>     
<script>
    // WRITE THE VALIDATION SCRIPT IN THE HEAD TAG.
    function isNumber(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
            return false;

        return true;
    } 

//--For Clearing Filter Data Elements-----//
    $(".reset").bind("click", function() {
    $("input[type=text], textarea").val("");
    $("#po_number").val("");
    $("#category1").val("");
    $("#category2").val("");
    $("#category4").val("");
    $("#category6").val("");

    });   

//--For Clearing input Data after modal dismiss-----//
    $(".resetmodal").bind("click", function() {
    $("input[type=text], textarea").val("");
    });   

//--For preventing double submit-----//
$(".preventDouble").on("submit",function(){
    $(this).unbind("submit");
    $(this).on("submit",function(){return false;});
});

</script>
<style type="text/css">
.tab-filter .filter-area {padding:0;}
.tab-filter .form-group {margin:0; height:30px !important;}
.tab-filter {margin:12px 0;}
.tab-filter .panel-default {border-radius:0; background:#111; padding:15px;}
.tab-filter .nav-tabs>li {margin-bottom:0px;}
.tab-filter .nav-tabs>li>a {margin-right:9px; background:#f1651d; letter-spacing:0.6px; padding:10px 19px;}
.tab-filter .panel-body {background:#fff; padding:26px 15px;}
.tab-filter .panel.with-nav-tabs .panel-heading {padding:0; border-radius:0; background:transparent; color:#fff; border:0;}
.tab-filter .panel.with-nav-tabs .nav-tabs {border-bottom:none;}
.tab-filter .with-nav-tabs.panel-default .nav-tabs > li > a, .tab-filter .with-nav-tabs.panel-default .nav-tabs > li > a:hover,
.tab-filter .with-nav-tabs.panel-default .nav-tabs > li > a:focus {color: #fff; font-family: 'Archivo Narrow', sans-serif; font-size:16px; border-radius:0;}
.tab-filter .with-nav-tabs.panel-default .nav-tabs > .open > a, .tab-filter .with-nav-tabs.panel-default .nav-tabs > .open > a:hover,
.tab-filter .with-nav-tabs.panel-default .nav-tabs > .open > a:focus, .tab-filter .with-nav-tabs.panel-default .nav-tabs > li > a:hover,
.tab-filter .with-nav-tabs.panel-default .nav-tabs > li > a:focus {color:#777; background-color:#ddd; border-color:transparent;}
.tab-filter .with-nav-tabs.panel-default .nav-tabs > li.active > a, .tab-filter .with-nav-tabs.panel-default .nav-tabs > li.active > a:hover,
.tab-filter .with-nav-tabs.panel-default .nav-tabs > li.active > a:focus {color:#555; background-color:#fff; border-color:#ddd; 
border-bottom-color:transparent;}
</style>