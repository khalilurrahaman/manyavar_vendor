@include('layouts.vendor_header');
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <!--<div class="row">
                    <div class="col-lg-12 title-top">
                    <div class="row page-header">
                    	<div class="col-sm-6">
                        	<h1>Vendor <small>View All Request</small></h1>
                        </div>
                        <div class="col-sm-6">
                        	<a href="javascript:void(0)" class="btn btn-success">Expand</a>
                        </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                            	<div class="row">
                                	<div class="col-sm-4"><h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i> Vendor View All Request</h3></div>
                                    <div class="col-sm-8">
                                    
                                    <div class="shorting-area">
                            	
                              <!-- <div class="col-sm-3 pull-right select-full">
                                        <a class="btn btn-default btn-select">
                                            <input type="hidden" class="btn-select-input" id="" name="" value="" />
                                            <span class="btn-select-value">Select an Item</span>
                                            <span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                                        	<ul>
                                                <li class="selected">Division...</li>
                                                <li>---</li>
                                                
                                        	</ul>
                                        </a>
                                </div> -->
                                
                                <div class="col-sm-3 pull-right select-full">
                                        <select id="req_status" name="req_status" class="select-drop" onchange="return filterByType()">
                                           <option value="">Select Status</option>
                                           <option value="new">New</option>
                                           <option value="close">Closed</option>
                                           <option value="accept">Accepted</option>
                                           <option value="reject">Rejected</option>

                                        </select>
                                </div>
                                <div class="col-sm-3 pull-right select-full">
                                        <select id="req_type" name="req_type" class="select-drop" onchange="return filterByType()">
                                            <option value="">Select Type</option>
                                            <option value="cutting">Cutting</option>
                                            <option value="rate_change">Rate Change</option>
                                            <option value="width_change">Width Change</option>
                                            <option value="date_ext">Due Date Extention</option>
                                            <option value="cancel">Cancel</option>
                                            <option value="others">Others</option>
                                            <option value="sample">Inspection</option>
                                        </select>
                                </div>
                                <div class="col-sm-3 pull-right select-full">
                                        <select id="req_po_no" name="req_po_no" class="select-drop" onchange="return filterByType()">
                                           <option value="">Select PO</option>
                                          @if(!empty($po_list))
                                          @foreach($po_list as $req)
                                           <option value="{{$req->po_no}}">{{$req->po_no}}</option>
                                          @endforeach
                                          @endif 
                                        </select>
                                </div>                              
                            </div>                               
                        </div>
                        </div>
                            </div>
                            <div class="clearfix"></div>
                            <span id="filterResult">
                            <div class="panel-body custome-table">
                              
                                <div class="table-responsive view-table">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Sr. No</th>
                                                <th class="text-center">PO No</th>
                                                <th class="text-center">New PO No</th>
                                                <th class="text-center">Date</th>
                                                <th class="text-center">Type of Request</th>                                              
                                                <!-- <th class="text-center">Old Value</th>
                                                <th class="text-center">Requested Vaule</th>
                                                <th class="text-center">Accepted Value</th> -->
                                                <th class="text-center">Status</th>
                                                <th class="text-center">Last Comments</th>
                                               <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody id="req_request">
                                    @if(!empty($request_list))
                                     <?php $inc=$request_list->perPage() * ($request_list->currentPage()-1);;?>
                                    @foreach($request_list as $req)
                                    <?php $inc++; ?>
                                    <tr>
                                      <td class="text-center">{{$inc}}</td>
                                    	<td class="text-center">{{$req->po_no}}</td>
                                      <td class="text-center">@if(!empty($req->new_po)){{$req->new_po}}@endif</td>
                                    	<td class="text-center">{{date("d/m/y",strtotime($req->created_at))}}</td>
                                        <td class="text-center">
                                        <?php 
                                        $type='';
                                        if($req->action_type=='cutting')
                                          $type='Request For Cutting';
                                          elseif($req->action_type=='rate_change') 
                                           $type='Request For Rate Change';
                                          elseif($req->action_type=='width_change') 
                                           $type='Request For Width Change';
                                          elseif($req->action_type=='date_ext') 
                                           $type='Request For Due Date Extention';
                                          elseif($req->action_type=='cancel') 
                                           $type='Request For Cancellation';
                                          elseif($req->action_type=='others') 
                                           $type='Request For Others';
                                          elseif($req->action_type=='sample') 
                                           $type='Request For Inspection';

                                        ?>
                                        {{$type}}
                                        </td>                                      
                                       <!--  <td class="text-center"></td>
                                        <td class="text-center"></td>
                                        <td class="text-center"></td> -->
                                        <td class="text-center">
                                        @if($req->status=='new')
                                       <span class="sm-btn btn-primary btn-xs">New</span>
                                       @elseif($req->status=='close')
                                       <span class="sm-btn btn-primary btn-warning">Closed</span>
                                       @elseif($req->status=='accept')
                                       <span class="sm-btn btn-primary btn-warning">Accepted</span>
                                       @elseif($req->status=='reject')
                                       <span class="sm-btn btn-primary btn-danger">Rejected</span> 
                                       @endif
                                        <td class="text-center">
                                         @if(!empty($req->req_comments) && count(($req->req_comments))>0)
                                        <?php                                       
                                       
                                        if(strlen($req->req_comments[0]->comments)<=20)                                 
                                           echo ($req->req_comments[0]->comments);
                                        else
                                         echo substr($req->req_comments[0]->comments,0,18).'..';  

                                        ?>
                                          @if($req->status!='close')
                                          <a href="{{route('requestwise-comments', $req->id)}}" class="pull-right btn btn-warning btn-xs">
                                          <?php echo count($req->req_comments)?>
                                          Comment(s)</a>
                                          @else
                                          <a class="pull-right btn btn-warning btn-xs">
                                          <?php echo count($req->req_comments)?>
                                          Comment(s)</a>
                                          @endif
                                        @else
                                          @if($req->status!='close')
                                          <a href="{{route('requestwise-comments', $req->id)}}" class="btn btn-warning btn-xs pull-right">
                                          Comments</a>
                                          @else
                                           <a class="btn btn-warning btn-xs pull-right">
                                          Comments</a>
                                          @endif
                                        
                                         @endif
                                        </td>
                                     <td class="text-center">
                                       @if($req->status=='new')
                                       <span class="sm-btn btn-primary btn-xs">Pending</span> 
                                       @elseif($req->status=='close')
                                        <span class="sm-btn btn-primary btn-warning">Closed</span>
                                        @elseif($req->status=='accept')
                                        <a href="{{route('vendor-accept-request', $req->id)}}" onclick="return acceptRequest('{{$req->id}}');" class="btn btn-success btn-xs">Close</a>
                                        @elseif($req->status=='reject') 
                                        <span class="sm-btn btn-primary btn-danger">Rejected</span> 
                                        @endif
                                        <!-- <a href="" onclick="return rejectRequest('{{$req->id}}');" class="btn btn-success btn-danger">Reject</a> -->
                                     </td>
                                      
                                    </tr>
                                    @endforeach
                                    @endif 
                                </tbody>
                               </table>
                              </div>
                              
                              <div class="row">
                              <div class="col-sm-6">
                              	<div class="dataTables_info" role="status" aria-live="polite">Showing {{$request_list->count()}} of {{$request_list->total()}} entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                       {{ $request_list->links() }}  
                                    </nav>
                                  </div>
                              </div>
                            </div>
                            </span>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

                                          
         
@include('layouts.footer');   
<script type="text/javascript">
$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        //$('#load a').css('color', '#dfecf6');
        //$('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');

        var url = $(this).attr('href'); 
        var page=$(this).attr('href').split('page=')[1]; 
    
    
        filterByType(page,2);
        //window.history.pushState("", "", url);
    });

    
});
    function filterByType(page=1,load=1)
    {
        //alert(type);
        if(load==1)
        $('.loading').show();
        var status=$('#req_status').val();
        var type=$('#req_type').val();
        var po_no=$('#req_po_no').val();
        $.ajax({
            type: 'GET', 
            url: "{{route('vendor-filter-action')}}",
            data: {type:type,po_no:po_no,status:status,page:page},
            success:function(data)
            {
                $('.loading').hide();  
                $('#filterResult').html(data);
                 
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
    }
</script>
<script>
    function acceptRequest(req_id){
        var x = confirm("Are you sure you want to Close ! ");
        if (x)
            return true;
        else
            return false;
    }
</script>
<script>
    function rejectRequest(req_id){
        var x = confirm("Are you sure you want to Reject ");
        if (x)
            return true;
        else
            return false;
    }
</script>
