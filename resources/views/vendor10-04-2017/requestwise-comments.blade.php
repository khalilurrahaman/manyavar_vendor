@include('layouts.vendor_header')
  <div id="page-wrapper">
    <div class="container-fluid inner-body-area">

    <div class="row shorting-area pricechange">
    <div class="col-lg-11 margin-center">
          <div class="panel panel-default add-padding">
            <div class="panel-heading">
              <div class="row">
                    <div class="col-sm-8">
                        <h3 class="panel-title">
                        <span class="glyphicon glyphicon-comment"></span> 
                        Details
                        </h3>
                    </div>
                    
            </div>
            </div>
            <div class="clearfix"></div>
            <div class="panel-body custome-table">
              <div class="comments-area">
          @if(!empty($poactiondetails))
          <div class="col-sm-3"><h5>PO NO : <strong class="">{{$poactiondetails->po_no}}</strong></h5></div>@endif

           @if(!empty($poactiondetails->ICODE))
          <div class="col-sm-3"><h5>Item Code : <strong class="">{{$poactiondetails->ICODE}}</strong></h5></div>@endif

          @if(!empty($poactiondetails->cat1))
          <div class="col-sm-3"><h5>Design: <strong class="">{{$poactiondetails->cat1}}</strong></h5></div>@endif

          @if(!empty($poactiondetails->cat2))
          <div class="col-sm-3"><h5>Color: <strong class="">{{$poactiondetails->cat2}}</strong></h5></div>@endif

          @if(!empty($poactiondetails->new_po))
          <div class="col-sm-3"><h5>New PO NO : <strong class="">{{$poactiondetails->new_po}}</strong></h5></div>@endif

          @if(!empty($poactiondetails->action_type))
          <div class="col-sm-3"><h5>Request Type : <strong class="">
          <?php 
              $type='';
              if($poactiondetails->action_type=='cutting')
                $type='Cutting Request';
              elseif($poactiondetails->action_type=='rate_change') 
                $type='Rate Change';
              elseif($poactiondetails->action_type=='width_change') 
                $type='Width Change';
              elseif($poactiondetails->action_type=='date_ext') 
                $type='Due Date Extention';
              elseif($poactiondetails->action_type=='cancel') 
                $type='Request For Cancellation';
              elseif($poactiondetails->action_type=='others') 
                $type='Others Request';
              elseif($poactiondetails->action_type=='sample') 
                $type='Request For Inspection';

          ?>
          {{$type}}
          </strong>
          </h5></div>@endif 
           
          @if(!empty($poactiondetails->created_at))  
          <div class="col-sm-3"><h5>Request Date : <strong class="">{{date("d/m/y",strtotime($poactiondetails->created_at))}}</strong></h5></div>@endif

           @if($poactiondetails->action_type=='width_change' || $poactiondetails->action_type=='rate_change')
          <div class="col-sm-3"><h5>Old Rate : <strong class="">{{$poactiondetails->old_value}}</strong></h5></div>@endif

          @if(!empty($poactiondetails->new_rate))  
          <div class="col-sm-3"><h5>Requested Rate : <strong class="">{{$poactiondetails->new_rate}}</strong></h5></div>@endif 

          @if(!empty($poactiondetails->accepted_rate))  
          <div class="col-sm-3"><h5>Accepted Rate : <strong class="">{{$poactiondetails->accepted_rate}}</strong></h5></div>@endif 
          
          @if($poactiondetails->action_type=='width_change')  
          <div class="col-sm-3"><h5>Old Width : <strong class="">{{$poactiondetails->old_width}}</strong></h5></div>@endif 

          @if(!empty($poactiondetails->new_width))  
          <div class="col-sm-3"><h5>Requested Width : <strong class="">{{$poactiondetails->new_width}}</strong></h5></div>@endif 

          @if(!empty($poactiondetails->ext_date))
          <div class="col-sm-3"><h5> Requested Ext Date : <strong class="">{{date("d/m/y",strtotime($poactiondetails->ext_date))}}</strong></h5></div>@endif

          @if(!empty($poactiondetails->cancel_qty))
          <div class="col-sm-3"><h5>Requested Can Qty : <strong class="">{{$poactiondetails->cancel_qty}}</strong></h5></div>@endif

          @if(!empty($poactiondetails->docket_no))
          <div class="col-sm-3"><h5>Docket No : <strong class="">{{$poactiondetails->docket_no}}</strong></h5></div>@endif

          @if(!empty($poactiondetails->prn_no))
          <div class="col-sm-3"><h5>PRN NO : <strong class="">{{$poactiondetails->prn_no}}</strong></h5></div>@endif

          @if(!empty($poactiondetails->prn_date))
          <div class="col-sm-3"><h5>PRN Date : <strong class="">{{date("d/m/y",strtotime($poactiondetails->prn_date))}}</strong></h5></div>@endif

          @if(!empty($poactiondetails->courier_no))
          <div class="col-sm-3"><h5>Courier Name : <strong class="">{{$poactiondetails->courier_no}}</strong></h5></div>@endif

          @if(!empty($poactiondetails->remarks))
          <div class="col-sm-3"><h5>Vendor Remarks : <strong class="">{{$poactiondetails->remarks}}</strong></h5></div>@endif

          @if(!empty($poactiondetails->admin_remarks))
          <div class="col-sm-3"><h5>Admin Remarks : <strong class="">{{$poactiondetails->admin_remarks}}</strong></h5></div>@endif

          @if(!empty($poactiondetails->Reason))
          <div class="col-sm-3"><h5>Reason : <strong class="">{{$poactiondetails->Reason}}</strong></h5></div>@endif

          @if($poactiondetails->action_type=='date_ext')
          <div class="col-sm-3"><h5>No. Of Extended : <strong class="">{{$extended_no->total_ext}}</strong></h5></div>@endif

          @if(!empty($poactiondetails->status))
          <div class="col-sm-3"><h5>Request Status : <strong class="">
           @if($poactiondetails->status=='new')
            <?php echo 'New';?>
           @elseif($poactiondetails->status=='accept')
            <?php echo 'Accepted';?>
           @elseif($poactiondetails->status=='reject')
            <?php echo 'Rejected';?>
           @elseif($poactiondetails->status=='close')
            <?php echo 'Closed';?>
           @elseif($poactiondetails->status=='inprocess')
            <?php echo 'In Process';?>
           @endif
          </strong></h5></div>@endif 
                
              </div>
            </div>
          </div>
        </div>
    </div>
    
    <div class="clearfix"></div>
    <br/>
    
      <div class="row">
        <div class="col-lg-6 margin-center">
          <div class="panel panel-default add-padding">
            <div class="panel-heading">
              <div class="row">
                    <div class="col-sm-8">
                        <h3 class="panel-title">
                        <span class="glyphicon glyphicon-comment"></span> 
                        Comments(<?php if(!empty($comments_list)) echo count($comments_list);?>)
                        </h3>
                    </div>
                    <div class="col-sm-4">
                      <div class="row comments-top">
                          <div class="col-sm-6 pull-right">
                              <a href="{{ URL::previous() }}" class="btn btn-success" style="width:100%;">Back</a>
                          </div>
                      </div>
                  </div>
            </div>
            </div>
            <div class="clearfix"></div>
            <div class="panel-body custome-table">
              <div class="comments-area">
                <div class="chat_window">
                  <ul class="messages"><!--Comments Append Here--></ul>
                  @if($poactiondetails->status=='new' || $poactiondetails->status=='inprocess')
                  <div class="bottom_wrapper clearfix">
                    <div class="message_input_wrapper">
                      <input class="message_input" placeholder="Type your message here..." />
                    </div>
                    <div class="send_message">
                      <div class="icon"></div>
                      <div class="text">Send</div>                      
                    </div>
                    <span id="message-failure"></span>
                  </div>
                  @endif
                </div>
                <div class="message_template">
                  <li class="message">
                    <div class="avatar" style="text-align: center;font-size: 26px;  padding: 14px;"></div>
                    <div class="text_wrapper">
                      <div class="text">vvvbvb </div>
                      <span class="date-time"></span>
                    </div>
                  </li>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row --> 
    </div>
    <!-- /.container-fluid --> 
  </div>
  <input type="hidden" id="sucess_status" name="sucess_status" value="0"/>
  <!-- /#page-wrapper -->
@include('layouts.footer')
<!--Added By Khalilur Rahaman-->
<script>
var token_cmnts='{{csrf_token()}}';
var c_url='{{route("add-request-comments")}}';
</script>
<!-------------------------Comments Section Start---------------------->
<script>
(function () {
    var Message;
    Message = function (arg) {
        this.text = arg.text, this.message_side = arg.message_side,this.sender = arg.sender,this.comments_time = arg.comments_time;
        this.draw = function (_this) {
            return function () {
                var $message;
                $message = $($('.message_template').clone().html());
                $message.addClass(_this.message_side).find('.text').html(_this.text);
                $message.find('.avatar').html(_this.sender);
                $message.find('.date-time').html(_this.comments_time);
                $('.messages').append($message);
                return setTimeout(function () {
                    return $message.addClass('appeared');
                }, 0);
            };
        }(this);
        return this;
    };
    $(function () {
        var getMessageText, message_side, sendMessage;
        message_side = 'right';
        getMessageText = function () {
            var $message_input;
            $message_input = $('.message_input');
            var cmnts= $message_input.val();
            var action_id='<?php echo $action_id;?>';
        
        if(cmnts=='')
           $("#message-failure").text('Please enter your message!').css('color', 'red').show();
        else 
           $("#message-failure").hide();
        if(cmnts!=''){
        $.ajax({
          type: 'POST', 
          url: c_url,
          data: {action_id:action_id,comments:cmnts, _token:token_cmnts},
          success:function(data)
          {
            
                      $('#sucess_status').val(data);
          },
          error: function (error) 
          {
            
            alert('Error Occured Please Try Again');
          }
      });
      }
      //if($('#sucess_status').val()==1)
      return cmnts;   

        };
        sendMessage = function (text,allignment='right',sender='<?php $name=Auth::user()->name; echo strtoupper($name[0]);?>',comments_time='<?php echo date("Y-m-d h:i:s");?>') {
            var $messages, message;
            if (text.trim() === '') {
                return;
            }
            $('.message_input').val('');
            $messages = $('.messages');
            //message_side = message_side === 'left' ? 'right' : 'left';
            message_side=allignment;
            message = new Message({
                text: text,
                message_side: message_side,
                sender: sender,
                comments_time:comments_time
            });
            message.draw();
            return $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);
        };
        $('.send_message').click(function (e) {
            return sendMessage(getMessageText());
        });
        $('.message_input').keyup(function (e) {
            if (e.which === 13) {
                return sendMessage(getMessageText());
            }
        });
       
        <?php if(!empty($comments_list) && count($comments_list)>0)
        {
          $inc=0;
          $user_id=Auth::user()->id;
            foreach($comments_list as $comments)
            {
            $sender1=DB::table('users')
            ->select('name')        
            ->where('id','=',$comments->user_id)
            ->first(); 
            $sender=strtoupper($sender1->name[0]);
            $comments_text=$comments->comments;
            $comments_time=$comments->created_at;
            $cmnts_user_id=$comments->user_id;
            $allignment='left';
            if($user_id==$cmnts_user_id)
              $allignment='right';
              $timeout=500*$inc;
              $inc++;
        ?>
        setTimeout(function () {
            return sendMessage('<?php echo $comments_text;?>','<?php echo $allignment;?>','<?php echo $sender;?>','<?php echo $comments_time;?>');
        }, <?php echo $timeout;?>);
        <?php 
        }
     }
     
     	?>
    });
}.call(this));
</script>
<!-------------------------Comments Section End---------------------->