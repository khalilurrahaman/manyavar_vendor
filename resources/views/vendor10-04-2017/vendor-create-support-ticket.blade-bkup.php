@include('layouts.vendor_header');
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <!--<div class="row">
                    <div class="col-lg-12 title-top">
                    <div class="row page-header">
                    	<div class="col-sm-6">
                        	<h1>Vendor <small>Create Support Ticket</small></h1>
                        </div>
                        <div class="col-sm-6">
                        	
                        </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-lg-5 margin-center">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i> Vendor Create Support Ticket</h3>
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body create-support">
                            	<div class="shorting-area create-support">
                            	
                              <div class="col-sm-12">
                                         <select class="selectpicker2 custom-picker " data-live-search="true" id="order_id">
                                            <option data-tokens="" value="">Select PO</option>
                                             @if(!empty($po_list) && count($po_list)>0)
                                             @foreach($po_list as $eachPo)
                                             <?php 
                                             $o_value = $eachPo->PONO; 
                                             $data_tokens = $eachPo->PONO;
                                            
                                             ?>
                                             <?php
                                       
                                        $odisable='';
                                        
                                        if(!empty($eachPo->actiondtls) && count($eachPo->actiondtls)>0){
                                        foreach($eachPo->actiondtls as $row){
                                          
                                          if($row->action_type=='others' && $row->status=='new'){
                                           $odisable='disabled';
                                           
                                          
                                        }
                                        }
                                        }
                                         ?>
                                                <option data-tokens="{{$data_tokens}}" value="{{$o_value}}" {{$odisable}}>{{$o_value}}</option>
                                             @endforeach
                                             @endif
                                        </select>
                                </div>
                                <span id="vendor-po-message"></span>
                               <!--  <div class="col-sm-12">
                                        <input type="text" placeholder="Title" class="form-control">
                                </div> -->                             
                                <div class="clearfix"></div>
                                <br/>
                                <div class="col-sm-12">
                                        <textarea id="remks" name="remks" class="form-control" style="height:199px;" placeholder="Remarks"></textarea>
                                </div>
                               
                                <span id="vendor-po-remarks"></span>
                                <div class="col-sm-12">
                                        <a style="cursor: pointer;" class="pup-btn" onclick="addRequest()">Submit Now</a>
                                </div>
                                <div class="clearfix"></div>
                                <div id="message-body" class="alert alert-success" style="display: none;">
  									<strong></strong> 
								</div>
                            
                            </div>
                                
                              
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@include('layouts.footer');  
<script type="text/javascript">
 function addRequest(){
    var urls = "{{route('add-other-request')}}";
    var remks = $("#remks").val(); 
    var order_id = $("#order_id").val();
    var ptoken='{{csrf_token()}}';
    //var actionval = $("#actionval").val();
    //alert(actionval);   
  
    if(order_id=='')
         $("#vendor-po-message").text('Please Select PO!').css('color', 'red').show();
    else 
         $("#vendor-po-message").hide();
    if(remks=='')
         $("#vendor-po-remarks").text('Please Enter Remarks!').css('color', 'red').show();
    else 
         $("#vendor-po-remarks").hide(); 
    // if(actionval==1){
          
    //       alert('Please wait for Admin Response against your previous Request!');
    //       return false;
    // }         
    if(remks!='' && order_id!='')
    {
    $.ajax({
            type: 'POST', 
            url: urls,
            data: {order_id:order_id,remks:remks,_token:ptoken},
            success:function(data)
            {
                    
                    if(data==1)
                    { 
                        $('#message-body').show();
                        $('#message-body').html('Request added successfully!');
                        setTimeout(function ()
                         { 
                          window.location.href='<?php echo route("all-requst-widthsampleothers")?>';

                          }, 2000); //will call the function after 2 secs.
                       
                      
                    }
                    else
                        $('#message-body').text('Please Try again!');
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        }); 
        }    
}
</script> 