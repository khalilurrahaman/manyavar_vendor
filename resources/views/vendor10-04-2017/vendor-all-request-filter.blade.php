
                           
                            <div class="panel-body custome-table">
                              
                                <div class="table-responsive view-table">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Sr. No</th>
                                                <th class="text-center">PO No</th>
                                                <th class="text-center">New PO No</th>
                                                <th class="text-center">Date</th>
                                                <th class="text-center">Type of Request</th>                                              
                                                <!-- <th class="text-center">Old Value</th>
                                                <th class="text-center">Requested Vaule</th>
                                                <th class="text-center">Accepted Value</th> -->
                                                <th class="text-center">Status</th>
                                                <th class="text-center">Last Comments</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody id="req_request">
                                    @if(!empty($request_list))
                                     <?php $inc=$request_list->perPage() * ($request_list->currentPage()-1);;?>
                                    @foreach($request_list as $req)
                                    <?php $inc++; ?>
                                    <tr>
                                        <td class="text-center">{{$inc}}</td>
                                    	<td class="text-center">{{$req->po_no}}</td>
                                      <td class="text-center">@if(!empty($req->new_po)){{$req->new_po}}@endif</td>                                   
                                    	<td class="text-center">{{date("d/m/y",strtotime($req->created_at))}}</td>
                                        <td class="text-center">
                                        <?php 
                                        $type='';
                                        if($req->action_type=='cutting')
                                          $type='Request For Cutting';
                                          elseif($req->action_type=='rate_change') 
                                           $type='Request For Rate Change';
                                          elseif($req->action_type=='width_change') 
                                           $type='Request For Width Change';
                                          elseif($req->action_type=='date_ext') 
                                           $type='Request For Due Date Extention';
                                          elseif($req->action_type=='cancel') 
                                           $type='Request For Cancellation';
                                          elseif($req->action_type=='others') 
                                           $type='Request For Others';
                                         elseif($req->action_type=='sample') 
                                           $type='Request For Inspection';

                                        ?>
                                        {{$type}}
                                        </td>                                     
                                             
                                        <td class="text-center">
                                       @if($req->status=='new')
                                       <span class="sm-btn btn-primary btn-xs">New</span>
                                       @elseif($req->status=='close')
                                       <span class="sm-btn btn-primary btn-warning">Closed</span>
                                       @elseif($req->status=='accept')
                                       <span class="sm-btn btn-primary btn-warning">Accepted</span>
                                       @elseif($req->status=='reject')
                                       <span class="sm-btn btn-primary btn-danger">Rejected</span> 
                                       @endif
                                        </td>
                                        <td class="text-center">@if(!empty($req->req_comments) && count(($req->req_comments))>0)
                                        <?php                                       
                                       
                                        if(strlen($req->req_comments[0]->comments)<=20)                                 
                                           echo ($req->req_comments[0]->comments);
                                        else
                                         echo substr($req->req_comments[0]->comments,0,18).'..';  

                                        ?>
                                       @if($req->status!='close')
                                          <a href="{{route('requestwise-comments', $req->id)}}" class="pull-right btn btn-warning btn-xs">
                                          <?php echo count($req->req_comments)?>
                                          Comment(s)</a>
                                          @else
                                          <a class="pull-right btn btn-warning btn-xs">
                                          <?php echo count($req->req_comments)?>
                                          Comment(s)</a>
                                          @endif
                                        @else
                                          @if($req->status!='close')
                                          <a href="{{route('requestwise-comments', $req->id)}}" class="btn btn-warning btn-xs pull-right">
                                          Comments</a>
                                          @else
                                           <a class="btn btn-warning btn-xs pull-right">
                                          Comments</a>
                                          @endif
                                        
                                         @endif
                                        </td>
                                        <td class="text-center">
                                       @if($req->status=='new')
                                       <span class="sm-btn btn-primary btn-xs">Pending</span> 
                                       @elseif($req->status=='close')
                                        <span class="sm-btn btn-primary btn-warning">Closed</span>
                                        @elseif($req->status=='accept')
                                       <a href="{{route('vendor-accept-request', $req->id)}}" onclick="return acceptRequest('{{$req->id}}');" class="btn btn-success btn-xs">Close</a>
                                        @elseif($req->status=='reject') 
                                        <span class="sm-btn btn-primary btn-danger">Rejected</span> 
                                        @endif
                                        <!-- <a href="" onclick="return rejectRequest('{{$req->id}}');" class="btn btn-success btn-danger">Reject</a> -->
                                     </td>
                                    </tr>
                                    @endforeach
                                    @endif 
                                </tbody>
                               </table>
                              </div>
                              
                              <div class="row">
                              <div class="col-sm-6">
                              	<div class="dataTables_info" role="status" aria-live="polite">Showing {{$request_list->count()}} of {{$request_list->total()}} entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                       {{ $request_list->links() }}  
                                    </nav>
                                  </div>
                              </div>
                            </div>
                           
