@include('layouts.vendor_header');
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <div class="row">
                    <div class="col-sm-10 margin-auto">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                            <div class="row">
                                    <div class="col-sm-6">
                                        <h3 class="panel-title"><i class="fa fa-cloud-download" aria-hidden="true"></i> Concluded Item(s)</h3>
                                    </div>
                                    <div class="col-sm-6">
                                    
                                    <div class="shorting-area">
                                <div class="pull-right">
                                        <a href="{{URL::previous()}}" class="btn btn-info">Back</a></div>
                                </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body pre-order">
                                <ul>
                                @if(!empty($rfq_rejected_list))
                                @foreach($rfq_rejected_list as $rfq)
                                <?php 
                                     $pGalList=DB::table('product_gallery')->select('*')->where('product_id','=',$rfq->product_id)->get();
                                     #echo "<pre>";print_r($pGalList);
                                     $data_mouseover=URL::to('/img').'/pre-order-img.jpg';
                                     if($rfq->product_details->image!='' && file_exists('upload/product/'.$rfq->product_details->image))
                                      $data_mouseover=URL::to('/upload/product').'/'.$rfq->product_details->image.'#200';
                                     if(!empty($pGalList) && count($pGalList)>0)
                                     {
                                      $timeoutcount=1;
                                      foreach ($pGalList as $pGal) 
                                      {
                                        $timeout=$timeoutcount*400;
                                        $data_mouseover .=' '.URL::to('/upload/product').'/'.$pGal->product_image.'#'.$timeout.' ';
                                        $timeoutcount++;
                                      }

                                     }
                                     $data_mouseover=rtrim($data_mouseover," ");
                                     ?>
                                <li>
                                        <div class="row">
                                            <div class="col-sm-3">
                                            <?php
                                            $image4=URL::to('/')."/img/pre-order-img.jpg";
                                            if(!empty($rfq->product_details->image) && 
                                                file_exists(public_path().'/upload/product/'. $rfq->product_details->image))
                                            {
                                               $image4= URL::to('/') .'/upload/product/'. $rfq->product_details->image;
                                            }
                                            ?>
                                                <a href="javascript:void(0)"><img src="<?php echo  $image4; ?>" alt="..." class="img-thumbnail" data-mouseover="{{$data_mouseover}}" style="height:200px;width: 200px;"></a>

                                            </div>
                                            <div class="col-sm-9 pre-details">
                                                <div class="row">
                                                    <div class="col-sm-5 paddLR0">
                                                        <div class="row">
                                                            <div class="col-sm-5">
                                                                <h4>RFQ No </h4>
                                                                <h4>Design </h4>
                                                                <h4>Shipment Provider </h4>
                                                                <h4>Remarks</h4>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <h4>: &nbsp; <strong> {{ $rfq->req_id }} </strong></h4>
                                                                <h4>: &nbsp; <strong>302-BLACK</strong></h4>
                                                                <h4>: &nbsp; <strong>
                                                                @if(!empty($rfq->rfq_details->shipment_provider))
                                                                {{ $rfq->rfq_details->shipment_provider }} 
                                                                @else
                                                                N/A
                                                                @endif
                                                                </strong> <small>Kolkata</small></h4>
                                                                 <h4>: &nbsp; <strong>
                                                                @if(!empty($rfq->rfq_details->indv_remarks))
                                                                {{ $rfq->rfq_details->indv_remarks }} 
                                                                @else
                                                                N/A
                                                                @endif
                                                                </strong></h4>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="col-sm-5 right-deta">
                                                        <div class="row">
                                                            <div class="col-sm-5">
                                                                <h4>RFQ Date </h4>
                                                                <h4>Colour </h4>
                                                                <h4>Docket No </h4>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <h4>: &nbsp; <strong>{{ date('d/m/Y',strtotime($rfq->created_at)) }}</strong></h4>
                                                                <h4>: &nbsp; <strong>44"</strong></h4>
                                                                <h4>: &nbsp; <strong> 
                                                                @if(!empty($rfq->rfq_details->docket_no))
                                                                {{ $rfq->rfq_details->docket_no }} 
                                                                @else
                                                                N/A
                                                                @endif</strong></h4>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>

                                                    <div class="col-sm-2 right-small">
                                                        <?php 
                                                        $status_text='Closed';
                                                        $status_class='danger';
                                                        if(!empty($rfq->status) && $rfq->status==1)
                                                        {
                                                            $status_text='Open';
                                                            $status_class='success';
                                                        }
                                                        ?>
                                                        <span class="sm-btn btn-{{ $status_class }} btn-xs"><i class="fa fa-tags" aria-hidden="true"></i> {{ $status_text }}</span>
                                                        
                                                        <div class="clearfix"></div>
                                                    </div>

                                                   <div class="clearfix"></div>             
                                                </div>

                                                <div class="row">
                                                    <div class="table-responsive">
                                                       
                                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" style="margin-bottom:0;">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center col-sm-3">Date</th>
                                                            <th class="text-center col-sm-3">Rate</th>
                                                            <th class="text-center col-sm-3">Width</th>
                                                            <th class="text-center col-sm-3">Comments</th>
                                                        </tr>
                                                    </thead>    

                                                    <tbody>
                                                    @if(!empty($rfq->rfq_quetes) && count(($rfq->rfq_quetes))>0)
                                                    <?php $old_ven='';?>
                                                    @foreach($rfq->rfq_quetes as $quetes)
                                                    <?php $new_ven=$quetes->rfq_details_id;?>
                                                    <tr>
                                                        <td class="text-center col-sm-3">{{ date('d/m/Y',strtotime($quetes->created_at)) }}</td>
                                                        
                                                        <td class="text-center col-sm-3">{{ $quetes->price}}</td>
                                                        <th class="text-center col-sm-3">{{ $quetes->width}}</th>

                                                    <?php if($old_ven!=$new_ven){?>

                                                        <td class="text-center col-sm-3" rowspan="{{count($rfq->rfq_quetes)}}">
                                                        @if(!empty($rfq->rfq_comments) && count($rfq->rfq_comments)>0)
                                                            <?php                                        
                                                            if(!empty($rfq->rfq_comments) && count($rfq->rfq_comments)>0)
                                                            {   
                                                            if(strlen($rfq->rfq_comments[0]->comments)<=20)                                 
                                                               echo ($rfq->rfq_comments[0]->comments);
                                                            else
                                                                echo substr($rfq->rfq_comments[0]->comments,0,18).'.. ';  

                                                            }
                                                            ?>
                                                                
                                                                <a href="{{route('quotationwise-comments', $quetes->rfq_details_id)}}">                                 
                                                                 <?php echo count($rfq->rfq_comments)?> Comment(s)
                                                                </a>                                                       
                                                            
                                                        @endif
                                                        </td>

                                                    <?php }?>
                                                    </tr>
                                                     <?php  $old_ven=$new_ven;?>
                                                    @endforeach
                                                    
                                                    @elseif($rfq->rfq_details->notify==1)
                                                    <tr>
                                                        <td style="color:#FF0000"; class="text-center" colspan="4"><span class="blnk_txt">Please Add Quatation</span></td>
                                                    </tr>
                                                    @endif
                                                </tbody>
                                               </table>
                                               
                                          
                                           
                                                
                                                  </div>     
                                                  <div class="clearfix"></div>    
                                                </div>



                                            </div>
                                            

                                        <div class="clearfix"></div> 
                                

                                                    
                                        </div>
                                    </li>
                                    @endforeach
                                    @else
                                    <li>No Records Found!</li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>



                    
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@include('layouts.footer'); 
<!--Added By Khalilur Rahaman-->
<!-- /#Hover Image -->
    <script type="text/javascript">$('img').on('mouseover', function() {
    var self = this,
        i = 0,
        images = $(this).data('mouseover').split(/\s+/);
    
    (function nextImage() {
        var next = images[i++ % images.length].split('#');
        $(self).data('timeout', setTimeout(function() {
            self.src = next[0];
            nextImage();
        }, next[1]));
    })();
    
}).on('mouseout', function() {
    clearTimeout($(this).data('timeout'));
    this.src = $(this).attr('src');
});
</script>
  
        
