<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title>Manyavar Vendor Panel</title>

    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <link rel="shortcut icon" href="{{ asset('favicon.png')}}" type="image/x-icon" />

    <link href="{{asset('css/datepicker.css')}}" rel="stylesheet">
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/admin.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="container-fluid">
             	<div class="screen-login">
                	<div class="row">
                   		<div class="login-form">
                            <div class="head">
                                <img src="{{asset('img/admin.jpg')}}" alt="">
                            </div>
                            <form class="card" role="form" method="POST" action="{{ url('/submitResetPassword') }}" autocomplete="off">
                            {{ csrf_field() }}
                            <input type="hidden" name="email" value="{{$user->email}}"  />
                                <div class="input-container">
                                    
                                    <input type="password" name="new_password" autocomplete="off" required="required"  />
                                    <label>New Password</label>
                                    <div class="bar"></div>
                                </div>
                                <div class="input-container">
                                    <input type="password" name="confirm_password" autocomplete="off" required="required"  />
                                    <label>Confirm Password</label>
                                    <div class="bar"></div>
                                </div>
                                
                                @if(!empty($errors->all()) )

                    <div   style="width:100%;color: red">{{ $errors->all()[0] }}</div>
                @endif
                                <div class="row btn-top">
                                <div class="col-sm-12">
                                	<button type="submit" class="btn btn-orange">Submit</button>
                                </div>
                                </div>
                            </form>
							</div>
            				<div class="copy-right">
								<p> © 2016 Manyavar Vendor Panel. All rights reserved</p>
							</div>
                   	</div>
             	</div>

            </div>
        </div>
    </div> 
    <style>
	input:-webkit-autofill {background-color:transparent !important; border:0 !important; }
	</style>
 <script src="js/jquery-2.1.1.js"></script>   
    <script>
	$(document).ready(function(){
    $( document ).on( 'focus', ':input', function(){
        $( this ).attr( 'autocomplete', 'off' );
    });
});


    setInterval(function () { 
       
        window.location.reload();
    },180000);

	</script>