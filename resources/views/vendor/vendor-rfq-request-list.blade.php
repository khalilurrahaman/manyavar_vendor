@include('layouts.vendor_header');
<!-- CSS -->
<link rel="stylesheet" href="{{asset('fancybox/css/fancybox/jquery.fancybox-buttons.css')}}">
<link rel="stylesheet" href="{{asset('fancybox/css/fancybox/jquery.fancybox-thumbs.css')}}">
<link rel="stylesheet" href="{{asset('fancybox/css/fancybox/jquery.fancybox.css')}}">
<link rel="stylesheet" href="{{asset('fancybox/demo/demo.css')}}">
<!-- DELETE -->
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <div class="row">
                    <div class="col-sm-10 margin-auto">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                            <div class="row">
                                    <div class="col-sm-6">
                                        <h3 class="panel-title"><i class="fa fa-cloud-download" aria-hidden="true"></i> New Item Request</h3>
                                    </div>
                                    <div class="col-sm-6">
                                    
                                    <div class="shorting-area">
                                <div class="pull-right">
                                        <a href="{{route('ven-rfq-rejected')}}" class="btn btn-info">Concluded Item</a></div>
                                </div>
                                    </div>
                                </div>
                                <style type="text/css">
                                    #blnk_txt {   
                                      animation-duration: 400ms;
                                      animation-name: blink;
                                      animation-iteration-count: infinite;
                                      animation-direction: alternate;
                                    }
                                    .blnk_txt{   
                                      animation-duration: 400ms;
                                      animation-name: blink;
                                      animation-iteration-count: infinite;
                                      animation-direction: alternate;
                                    }
                                    @keyframes blink {
                                      from {
                                        opacity: 1;
                                      }

                                      to {
                                        opacity: .5;
                                      }
                                    }
                                </style>


                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body pre-order">
                                <ul>
                                 <?php $inc=0;?> 
								@if(!empty($rfq_list))
                                @foreach($rfq_list as $rfq)
                                <?php $inc++;?> 
                                <li>
                                        <div class="row">
                                            <div class="col-sm-3">
                                            <div class="gal"> 
                             <?php 
                             $main_image='';
                               $pGalList=DB::table('product_gallery')->select('*')->where('product_id','=',$rfq->product_id)->get();
                               if($rfq->product_details->image!='' && file_exists('upload/product/'.$rfq->product_details->image))
                               $main_image=URL::to('/upload/product').'/'.$rfq->product_details->image;
                               if(empty($main_image) && !empty($pGalList) && count($pGalList)>0)
                               {
                                  foreach ($pGalList as $pGal) 
                                  { 
                                    if($pGal->product_image!='' && file_exists('upload/product/'.$pGal->product_image))
                                    {
                                      $main_image=URL::to('/upload/product').'/'.$pGal->product_image;
                                    }
                                  }
                               }
                               if(empty($main_image))
                                $main_image=URL::to('/img').'/pre-order-img.jpg';
                              ?>
                               <a class="fancybox" rel="group{{$inc}}" href="{{$main_image}}" >
                                <img src="{{$main_image}}" style="width: 150px;" width="56">
                               </a>
                               <?php 
                               if(!empty($pGalList) && count($pGalList)>0)
                               {
                                foreach ($pGalList as $pGal2) 
                                {
                                    if($pGal2->product_image!='' && file_exists('upload/product/'.$pGal2->product_image))
                                    {
                                      $main_image=URL::to('/upload/product').'/'.$pGal2->product_image;

                                ?>                                               
                               <a class="fancybox" rel="group{{$inc}}" href="{{$main_image}}" style="display:none;"><img src="{{$main_image}}" alt=""></a>
                                <?php 
                                }
                                }
                                }
                               ?> 
                            </div>
                                            </div>
                                            <div class="col-sm-9 pre-details">
                                                <div class="row">
                                                    <div class="col-sm-5 paddLR0">
                                                        <div class="row">
                                                            <div class="col-sm-5">
                                                                <h4>RFQ No </h4>
                                                                <h4>Design </h4>
                                                                <h4>Shipment Provider </h4>
                                                                <h4>Remarks</h4>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <h4>: &nbsp; <strong> {{ $rfq->req_id }} </strong></h4>
                                                                <h4>: &nbsp; <strong>@if(!empty($rfq->product_details->cat1)){{$rfq->product_details->cat1}}@endif</strong></h4>
                                                                <h4>: &nbsp; <strong>
                                                                @if(!empty($rfq->rfq_details->shipment_provider))
                                                                {{ $rfq->rfq_details->shipment_provider }} 
                                                                @else
                                                                N/A
                                                                @endif
                                                                </strong> <small>Kolkata</small></h4>
                                                                <h4>: &nbsp; <strong>
                                                                @if(!empty($rfq->rfq_details->indv_remarks))
                                                                {{ $rfq->rfq_details->indv_remarks }} 
                                                                @else
                                                                N/A
                                                                @endif
                                                                </strong></h4>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="col-sm-5 right-deta">
                                                        <div class="row">
                                                            <div class="col-sm-5">
                                                                <h4>RFQ Date </h4>
                                                                <h4>Colour </h4>
                                                                <h4>Docket No </h4>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <h4>: &nbsp; <strong>{{ date('d/m/Y',strtotime($rfq->created_at)) }}</strong></h4>
                                                                <h4>: &nbsp; <strong>@if(!empty($rfq->product_details->cat2)){{$rfq->product_details->cat2}}@endif</strong></h4>
                                                                <h4>: &nbsp; <strong> 
                                                                @if(!empty($rfq->rfq_details->docket_no))
                                                                {{ $rfq->rfq_details->docket_no }} 
                                                                @else
                                                                N/A
                                                                @endif</strong></h4>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                     <!--<div class="clearfix"></div> 
                                                    <div class="col-sm-12 paddLR0">
                                                                    <h5>0 Quotes Added</h5>
                                                                    <div class="clearfix"></div>     
                                                                </div>-->
                                                                <div class="col-sm-2 right-small">
                                            <?php 
											$status_text='Close';
											$status_class='danger';
											if(!empty($rfq->status) && $rfq->status==1)
											{
												$status_text='Open';
												$status_class='success';
											}
											?>
                                            <span class="sm-btn btn-{{ $status_class }} btn-xs"><i class="fa fa-tags" aria-hidden="true"></i> {{ $status_text }}</span>
                                            <div class="pre-border">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <a href="javascript:void(0)" 
                                                        data-toggle="modal" 
														<?php if($status_text=='Open'){echo 'data-target="#AddQuotation"';}?> 
                                                        class="btn btn-warning <?php if($status_text=='Open'){echo'addQuotationClass';}else{echo 'addQuotationClassClose';}?>" rfq_id="{{ $rfq->id }}" rfq_details_id="{{ $rfq->rfq_details->id }}" rfq_no="{{ $rfq->req_id }}" width_size_required="{{$rfq->product_details->width_size_required}}" id="blnk_txt">Add Quotation</a><div class="clearfix"></div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                            </div>
                                                   <div class="clearfix"></div>             
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="row">
                                                <div class="col-sm-12" style="margin-top:12px; padding-left:0;">         
                                             <div class="table-responsive">
                                       
                                   <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" style="margin-bottom:0;">
                                        <thead>
                                            <tr>
                                                <th class="text-center col-sm-3">Sr No</th>
                                                <th class="text-center col-sm-3">Date</th>
                                                <th class="text-center col-sm-3">Rate</th>
                                                <th class="text-center col-sm-3">Width</th>
                                                <th class="text-center col-sm-3">Comments</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    @if(!empty($rfq->rfq_quetes) && count(($rfq->rfq_quetes))>0)
                                    <?php $old_ven='';$qinc=0;?>
                                    @foreach($rfq->rfq_quetes as $quetes)
                                    <?php $new_ven=$quetes->rfq_details_id;$qinc++;?>
                                    <tr>
                                        <td class="text-center col-sm-3">{{ $qinc}}</td>
                                        <td class="text-center col-sm-3">{{ date('d/m/Y',strtotime($quetes->created_at)) }}</td>
                                        
                                        <td class="text-center col-sm-3">{{ $quetes->price}}</td>
                                        <th class="text-center col-sm-3"><?php if($quetes->width==0){echo 'NA';}else{echo $quetes->width.'"';}?></th>

                                    <?php if($old_ven!=$new_ven){?>

                                        <td class="text-center col-sm-3" rowspan="{{count($rfq->rfq_quetes)}}">
                                        @if(!empty($rfq->rfq_comments) && count($rfq->rfq_comments)>0)
                                        <?php                                        
                                        if(!empty($rfq->rfq_comments) && count($rfq->rfq_comments)>0)
                                        {   
                                        if(strlen($rfq->rfq_comments[0]->comments)<=20)                                 
                                           echo ($rfq->rfq_comments[0]->comments);
                                        else
                                            echo substr($rfq->rfq_comments[0]->comments,0,18).'.. ';  

                                        }
                                        ?>
                                            @if($status_text=='Open')
                                            <a href="{{route('quotationwise-comments', $quetes->rfq_details_id)}}">                                 
                                             <?php echo count($rfq->rfq_comments)?> Comment(s)
                                            </a>                                       
                                            @else
                                                                      
                                             Comments
                                           
                                             @endif
                                        @else
                                            @if($status_text=='Open')
                                            <a href="{{route('quotationwise-comments', $quetes->rfq_details_id)}}">
                                             Comment
                                            </a>
                                            @else
                                                                    
                                            Comment
                                                
                                            @endif
                                        @endif
                                        </td>

                                    <?php }?>
                                    </tr>
                                     <?php  $old_ven=$new_ven;?>
                                    @endforeach
                                    
                                    @elseif($rfq->rfq_details->notify==1)
                                    <tr>
                                        <td style="color:#FF0000"; class="text-center" colspan="4"><span class="blnk_txt">Please Add Quatation</span></td>
                                    </tr>
                                    @endif
                                </tbody>
                               </table>
                               
                              
                               
                                    
                              </div>     <div class="clearfix"></div>    
                                           </div>
                                                </div>
                                                
                                                
                                            </div>
                                            
                                          
                                             <div class="clearfix"></div> 
                                           
                                                    
                                        </div>
                                    </li>
                                    @endforeach
                                    @else
                                    <li>No Records Found!</li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@include('layouts.footer'); 
<!--Added By Khalilur Rahaman-->
  
        
<!-- AddQuotation Modal Start-->
<div id="AddQuotation" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Quotation</h4>
      </div>
      <div class="modal-body">
        <div class="modal-body">
        <div class="row shorting-area pricechange">
        <h4>You are Adding your Quotate to RFQ No:<strong class="rfq_id">#</strong></h4>
        <input type="hidden" name="rfq_id" id="rfq_id" value="" />
        <input type="hidden" name="rfq_details_id" id="rfq_details_id" value="" />
        <div class="clearfix"></div>
        	
            <div class="col-sm-6">
            <!--<h5>Price <small>(In Rupees)</small> :</h5>-->
            <input type="text" placeholder="Rate (In Rupees)" class="form-control emptyValue" id="req_price" name="req_price" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
            <span  id="req_price_message" ></span>
            </div>
            
            <div class="col-sm-6">
            <!--<h5>Width <small>(In Inch)</small> :</h5>-->
            <input type="text" placeholder="Width (In Inch)" class="form-control emptyValue" id="req_width" name="req_width" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
            <span  id="req_width_message" ></span>
            </div>
            <input type="hidden" name="vendorChangewidthisfabric" id="vendorChangewidthisfabric" value="">
            

            <div class="clearfix"></div>
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="pup-btn submitQuotation">Submit Quotation</a>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!-- AddQuotation Modal End-->
<!-- /#Hover Image -->
    <script type="text/javascript">$('img').on('mouseover', function() {
    var self = this,
        i = 0,
        images = $(this).data('mouseover').split(/\s+/);
    
    (function nextImage() {
        var next = images[i++ % images.length].split('#');
        $(self).data('timeout', setTimeout(function() {
            self.src = next[0];
            nextImage();
        }, next[1]));
    })();
    
}).on('mouseout', function() {
    clearTimeout($(this).data('timeout'));
    this.src = $(this).attr('src');
});
</script>
<script>
var token='{{csrf_token()}}';
var q_url='{{route("add-rfq-quotation")}}';
</script>
<script src="{{asset('js/custom/rfq.js')}}"></script>  

<!-- ********************************************** -->
<!-- JavaScript at the bottom for fast page loading -->
<!-- ********************************************** -->

<!-- Grab Google CDN's jQuery, fall back to local if offline -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{{asset('fancybox/js/libs/jquery-1.7.1.min.js')}}"><\/script>')</script>
    
<!-- FancyBox -->
    <script src="{{asset('fancybox/js/fancybox/jquery.fancybox.js')}}"></script>
    <script src="{{asset('fancybox/js/fancybox/jquery.fancybox-buttons.js')}}"></script>
    <script src="{{asset('fancybox/js/fancybox/jquery.fancybox-thumbs.js')}}"></script>
    <script src="{{asset('fancybox/js/fancybox/jquery.easing-1.3.pack.js')}}"></script>
    <script src="{{asset('fancybox/js/fancybox/jquery.mousewheel-3.0.6.pack.js')}}"></script>
    
    <script type="text/javascript">
        var nc=$.noConflict();
        nc(document).ready(function() {
        nc(".fancybox").fancybox();
        });
    </script>

