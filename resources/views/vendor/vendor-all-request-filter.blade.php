 <div class="panel-body custome-table">
                              
                                <div class="table-responsive view-table">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Sr. No</th>
                                                <th class="text-center">Request</th>
                                                <th class="text-center">PO Details</th>
                                                <th class="text-center">Product Info<br>(Design and Color)</th>
                                                <th class="text-center">Old Values<br>(Price and Width)</th>
                                                <th class="text-center">Requested Values<br>(Price and Width)</th>
                                                <th class="text-center">Negotiated Rate</th>
                                                <th class="text-center">Accepted Values<br>(Price and Width)</th>
                                                <th class="text-center">Delivery Date</th>
                                                <th class="text-center">Ext. Date</th>
                                                <th class="text-center">Admin Remarks</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-center">Action</th>
                                                <th class="text-center">Last Comments</th>
                                            </tr>
                                        </thead>
                                    <tbody id="req_request">
                                    @if(!empty($request_list))
                                     <?php $inc=$request_list->perPage() * ($request_list->currentPage()-1);;?>
                                    @foreach($request_list as $req)
                                    <?php $inc++; ?>
                                    <tr>
                                      <td class="text-center">{{$inc}}</td>
                                      <td class="text-center">
                                      {{date("d/m/y",strtotime($req->created_at))}}<br>
                                      <?php 
                                        $type='';
                                        if($req->action_type=='rate_change')
                                          $type='Rate Change';          
                                        elseif($req->action_type=='width_change') 
                                          $type='Width Change';
                                         elseif($req->action_type=='cutting')
                                          $type='Sample';          
                                          elseif($req->action_type=='cancel') 
                                           $type='Cancellation';
                                          elseif($req->action_type=='others') 
                                           $type='Others';
                                          elseif($req->action_type=='sample') 
                                           $type='Sample for Approval';
                                         elseif($req->action_type=='date_ext')
                                          $type='Due Date Extention'; 
                                         

                                        ?>
                                        {{$type}}
                                      </td>  
                                      <td class="text-center">
                                        {{$req->po_no}}<br>
                                        {{date("d/m/y",strtotime($req->order_date))}}

                                      </td>   
                                      <td class="text-center">
                                        {{$req->cat1}}<br>
                                        {{$req->cat2}}
                                      </td>                 
                                        <td class="text-center">
                                        @if(!empty($req->old_value)){{$req->old_value}}@else{{'NA'}}@endif
                                        <br>
                                        @if(!empty($req->old_width)){{$req->old_width}}"@else{{'NA'}}@endif
                                        </td>
                                        <td class="text-center">
                                        @if(!empty($req->new_rate)){{$req->new_rate}}@else{{'NA'}}@endif<br>
                                        @if(!empty($req-> new_width)){{$req->new_width}}"@else{{'NA'}}@endif
                                        </td>
                                        <td class="text-center">
                                        @if($req->new_rate!=$req->negotiated_rate)
                                         {{$req->negotiated_rate}}@else{{'NA'}}@endif
                                        </td>
                                        <td class="text-center">
                                        @if($req->action_type=='rate_change'|| $req->action_type=='width_change')
                                        @if($req->new_rate==$req->negotiated_rate){{$req->negotiated_rate}}@else{{$req->accepted_rate}}@endif<br>
                                        @if(!empty($req->new_width)){{$req->new_width}}"@endif
                                        @else{{'NA'}}@endif
                                        </td>
                                        <td class="text-center">@if(!empty($req->po_date)){{date("d/m/y",strtotime($req->po_date))}}@else{{'NA'}}@endif</td>                                     
                                        <td class="text-center">@if(!empty($req->ext_date)){{date("d/m/y",strtotime($req->ext_date))}}@else{{'NA'}}@endif</td>
                                        <td class="text-center">@if(!empty($req->admin_remarks)){{$req->admin_remarks}}@endif</td>  
                                        <td class="text-center">
                                        @if($req->status=='new')
                                       <span class="sm-btn btn-primary btn-xs">New</span>
                                       @elseif($req->status=='close')
                                       <span class="sm-btn btn-xs btn-warning">Accepted</span>
                                       @elseif($req->status=='inprocess')
                                       <span class="sm-btn btn-primary btn-xs">In Process</span>
                                      @elseif($req->status=='accept')
                                       <span class="sm-btn btn-xs btn-warning">Accepted</span>
                                       @elseif($req->status=='reject')
                                       <span class="sm-btn btn-xs btn-danger">Rejected</span> 
                                       @endif
                                       </td>
                                        
                                     <td class="text-center">
                                       @if($req->status=='new')
                                       <span class="sm-btn btn-primary btn-xs">Pending</span> 
                                       <?php
                                       $time = strtotime($req->created_at);

                                        $curtime = time();

                                        if(($curtime-$time) > 3600 && ($req->status=='new') && ($req->notify==0)) { //seconds(72 hrs)?>   
                                         <a href="{{route('vendor-notify-request', $req->id)}}" onclick="return notifyReq('{{$req->id}}');" class="btn btn-success btn-danger">Notify</a>
                                        <?php } elseif(($curtime-$time) > 3600 && ($req->status=='new') && ($req->notify==1)) {?>
                                       <span class="sm-btn btn-xs btn-danger">Notified</span>
                                         
                                       <?php }
                                       ?> 
                                       @elseif($req->status=='close')
                                        <span class="sm-btn btn-xs btn-warning">Closed</span>
                                        @elseif($req->status=='inprocess')
                                        <a onclick="return acceptRequest('{{$req->id}}','{{$req->negotiated_rate}}');" class="btn btn-success btn-xs">Accept</a>
                                       <a href="{{route('vendor-reject-request',$req->id)}}" onclick="return rejectRequest('{{$req->id}}');" class="btn btn-success btn-danger">Reject</a>
                                       @elseif($req->status=='accept')
                                       <span class="sm-btn btn-xs btn-warning">Closed</span>
                                        @elseif($req->status=='reject') 
                                        <span class="sm-btn btn-xs btn-warning">Closed</span> 
                                        @endif
                                        <!-- <a href="" onclick="return rejectRequest('{{$req->id}}');" class="btn btn-success btn-danger">Reject</a> -->
                                     </td>
                                      
                                      <td class="text-center">
                                         @if(!empty($req->req_comments) && count(($req->req_comments))>0)
                                        <?php                                       
                                       
                                        if(strlen($req->req_comments[0]->comments)<=20)                                 
                                           echo ($req->req_comments[0]->comments);
                                        else
                                         echo substr($req->req_comments[0]->comments,0,18).'..';  

                                        ?>
                                          <a href="{{route('requestwise-comments', $req->id)}}" class="pull-right btn btn-warning btn-xs">
                                          <?php echo count($req->req_comments)?>
                                          Comment(s)</a>

                                        @else
                                          <a href="{{route('requestwise-comments', $req->id)}}" class="btn btn-warning btn-xs pull-right">
                                          Comments</a>
                                         @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif 
                                </tbody>
                               </table>
                              </div>
                              
                              <div class="row">
                              <div class="col-sm-6">
                                <div class="dataTables_info" role="status" aria-live="polite">Showing {{$request_list->count()}} of {{$request_list->total()}} entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                       {{ $request_list->links() }}  
                                    </nav>
                                  </div>
                              </div>
                            </div>