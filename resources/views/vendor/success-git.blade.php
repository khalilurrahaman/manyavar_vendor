@include('layouts.vendor_header');
<div id="page-wrapper">
  <div class="container-fluid inner-body-area">
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i> GIT created sucessfully !</h3>
          </div>
          <div class="clearfix"></div>
          <div class="panel-body report-area">
            <h4 style="color:#F00;">Please print packing slip!</h4>
            <div class="row">
              <div class="col-sm-6">
                <h3>View packing slip:</h3>
                <a href="{{route('view-git-invoice',$per_git_id)}}">View</a> 
              </div>
              <div class="col-sm-6">
                <h3>Download packing slip:</h3>
                <a href="{{route('download-git-invoice',$per_git_id)}}">Download</a> 
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('layouts.footer')