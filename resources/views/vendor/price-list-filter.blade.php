<div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <!-- <th class="text-center">Sr. No</th> -->
                                                <th class="text-center">Image</th>
                                                <th class="text-center">Item</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Cat3</th>
                                                <th class="text-center">Width/Size</th>
                                                <th class="text-center">Cat6</th>
                                                <th class="text-center">Price</th>
                                                <th class="text-center col-sm-2">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <?php $inc=0; ?>
                                    @foreach($data as $eachData)
                                     <?php $inc++; ?>
                                     
                                    <tr>
                                       <!--  <td class="text-center">{{$inc}}</td> -->
                                       
                                        <td class="text-center">
                                        <div class="gal"> 
                             <?php 
                              $item_image=URL::to('/img').'/pre-order-img.jpg';
                               if($eachData->image!='' && file_exists('upload/product/'.$eachData->image))
                               $item_image=URL::to('/upload/product').'/'.$eachData->image;
                                                            ?>
                               <a class="fancybox" rel="group{{$inc}}" href="{{$item_image}}" >
                                <img src="{{$item_image}}" width="150">
                               </a>
                                        
                               <a class="fancybox" rel="group{{$inc}}" href="{{$item_image}}" style="display:none;"><img src="{{$item_image}}" alt=""></a>
                              
                            </div>
                                         </td>
                                        <td class="text-center">@if($eachData->ICODE){{($eachData->ICODE)}}@endif</td>
                                        <td class="text-center">{{$eachData->cat1}} </td>
                                        <td class="text-center">{{$eachData->cat2}} </td>
                                        <td class="text-center">{{$eachData->cat3}} </td>
                                        <td class="text-center">{{$eachData->cat4}} </td>
                                        <td class="text-center">{{$eachData->CNAME6}} </td>
                                        <td class="text-center">{{$eachData->RATE}} 
                                        @if($eachData->request_status==2)
                                        (old rate:{{$eachData->old_rate}} updated on {{date("d/m/y",strtotime($eachData->updated_at))}})
                                        @endif
                                        </td>
                                        <td class="text-center">
                                          @if($eachData->request_status==1)
                                          <span>Requested Price:{{$eachData->requested_rate}}</span><br>  
                                            <a class="btn btn-success">Request Sent</a>
                                         @endif   
                                         @if($eachData->request_status!=3 && $eachData->request_status!=1)
                                         @if($eachData->request_status==4)
                                         <span>Requested Price:{{$eachData->requested_rate}}</span><br>
                                        @if(!empty($eachData->negotiate_rate))<br><span>Admin Negotiated Price:{{$eachData->negotiate_rate}}</span><br>@endif
                                         <span class="btn-danger">Rejected</span><br> 
                                         @endif
                                            <a href="{{route('request-price-change-vendor', $eachData->id)}}" class="btn btn-success">Request For Price Change</a>
                                         @endif
                                          @if($eachData->request_status==3) 
                                            <span class="">Admin Negotiated Price:{{$eachData->negotiate_rate}}</span><br>
                                            <a onclick="return acceptRequest('{{$eachData->id}}','{{$eachData->negotiate_rate}}');" class="btn btn-success">Accept</a>
                                            <a href="{{route('vendor-reject-nego-price', $eachData->id)}}" class="btn btn-danger">Reject</a>
                                         @endif  
                                        </td>
                                    </tr>
                                    @endforeach                            
                                </tbody>
                               </table>
                              </div>
                              