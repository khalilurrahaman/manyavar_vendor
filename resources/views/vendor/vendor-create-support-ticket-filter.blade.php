
<div class="table-responsive ">
  <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
      <tr>
        <th class="text-center">Sr. No</th>
        <th class="text-center">PO. No</th>
        <th class="text-center">Item Code</th>
        <th class="text-center">PO. Date</th>
        <th class="text-center">Due. Date</th>
        <th class="text-center">Design</th>
        <th class="text-center">Colour</th>
        <th class="text-center">Width/Size</th>
        <th class="text-center">Order Qty</th>
        <th class="text-center">Received Qty</th>
        <th class="text-center">GIT Qty</th>
        <th class="text-center">Adhoc Qty</th>
        <th class="text-center">Pending Qty</th>
        <th class="text-center">Current rate</th>
        <th class="last-width2 text-center">Action</th>
      </tr>
    </thead>
    <tbody>
      <?php $inc=$purchaseOrderList->perPage() * ($purchaseOrderList->currentPage()-1); ?>
    @foreach($purchaseOrderList as $eachOrder)
    <?php $inc++; 
                                    $pending = ($eachOrder->ORDQTY - ($eachOrder->RCQTY+$eachOrder->CNLQTY + $eachOrder->git_qty));
                                    // if($pending<=0)
                                    //     $pending=str_replace('-', 'Adhoc:', $pending);
                                    $adhoc_qty=0;
                                    if($pending<=0)
                                    {
                                    //echo $pndg_qty=str_replace("-","Adhoc:",$pndg_qty);
                                    $adhoc_qty=str_replace("-"," ",$pending);
                                    $pending=0;
                                    }

                                    ?>
                                    
                                     <?php
                                       
                                        $sclass='vendorAddremarks';
                                        $targetmodal='vendorAddremarks';
                                        if(!empty($eachOrder->actiondtls) && count($eachOrder->actiondtls)>0){
                                        foreach($eachOrder->actiondtls as $row){
                                          
                                          if($row->action_type=='others' && $row->status=='new' && $row->ICODE==$eachOrder->ICODE){
                                           $sclass='unablepopup';
                                           $targetmodal='';
                                          
                                        }
                                        }
                                        }
                                         ?>
    <tr>
      <td class="text-center">{{$inc}}</td>
      <td class="text-center po-no<?php echo $inc;?>">{{$eachOrder->PONO}}</td>
      <td class="text-center ICODE<?php echo $inc;?>">@if(!empty($eachOrder->ICODE)){{$eachOrder->ICODE}}@endif</td>
      <td class="text-center po-date<?php echo $inc;?>">{{date('d/m/Y',strtotime($eachOrder->ORDDT))}}</td>
      <td class="text-center due-date<?php echo $inc;?>">{{date('d/m/Y',strtotime($eachOrder->DUEDATE))}}</td>
      <td class="text-center cat-1<?php echo $inc;?>">{{$eachOrder->cat1}}</td>
      <td class="text-center cat-2<?php echo $inc;?>">{{$eachOrder->cat2}}</td>
      <td class="text-center cat-4<?php echo $inc;?>">{{$eachOrder->cat4}}</td>
      <td class="text-center order-qty<?php echo $inc;?>">{{$eachOrder->ORDQTY}}</td>
      <td class="text-center received-qty<?php echo $inc;?>">{{$eachOrder->RCQTY}}</td>
      <td class="text-center git-qty<?php echo $inc;?>">@if(!empty($eachOrder->git_qty)){{$eachOrder->git_qty}}@else{{0}}@endif</td>
      <td class="text-center adhoc-qty<?php echo $inc;?>">{{$adhoc_qty}}</td>
      <td class="text-center pending-qty<?php echo $inc;?>">{{$pending}}</td>
      <td class="text-center price<?php echo $inc;?>">{{$eachOrder->RATE}}</td>
      <td class="last-width2 text-center"><span data-toggle="modal" data-target="#{{$targetmodal}}"> <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Request for Others" class="tab-btn btn-full {{$sclass}}" unique-key="<?php echo $inc;?>" >Request for Others</a></span></td>
    </tr>
    @endforeach
      </tbody>
    
  </table>
</div>
<div class="row">
  <div class="col-sm-6">
    <div class="dataTables_info" role="status" aria-live="polite">Showing {{$purchaseOrderList->count()}} of {{$purchaseOrderList->total()}} entries</div>
  </div>
  <div class="col-sm-6">
    <nav aria-label="Page navigation" class="pull-right"> {{ $purchaseOrderList->links() }} </nav>
  </div>
</div>
<script>
$( ".unablepopup" ).click(function() {
    
   alert('Please wait for Admin Response against your previous Request!');
});
</script>
<script src="{{asset('js/custom/purchase_order.js')}}"></script>   

