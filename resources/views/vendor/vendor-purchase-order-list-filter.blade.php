<!-- <?php 
      $totpenqty=0;
      $totpenqty=($perpageTotalorderQty-($perpageTotalrcvQty+$perpageTotalcanQty));

      $totadhok=0;
      if($totpenqty<=0){
        $totadhok=str_replace('-', '', $totpenqty);
        $totpenqty=0;
         }
?> -->
      <div class="table-responsive ">
          <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                  <tr>
                      <!--<th class="text-center">Sr. No</th>-->
                      <th class="text-center">Image</th>
                      <th class="text-center">PO. No</th>
                      <th class="text-center">PO. Date</th>
                      <th class="text-center">Due. Date</th>
                      <th class="text-center">Item Code</th>
                      <th class="text-center">Design</th>
                      <th class="text-center">Colour</th>
                      <th class="text-center">Cat 3</th>
                      <th class="text-center">Width/Size</th>
                       <th class="text-center">Cat 6</th>
                      <th class="text-center">Price</th>
                      <th class="text-center">Order Qty</th>
                      <th class="text-center">Received Qty</th>
                      <th class="text-center">Cancelled Qty</th>
                      <th class="text-center">GIT Qty</th>
                      <th class="text-center">Adhoc Qty</th>
                      <th class="text-center">Pending Qty</th>
                      <th class="last-width text-center">Action</th>
                  </tr>
              </thead>
             <!--  <tfoot>
                <tr>
                 <th colspan="11" style="text-align:right">Total:</th>
               
                 <th>{{$perpageTotalorderQty}}</th>
                 <th>{{$perpageTotalrcvQty}}</th>
                 <th>{{$perpageTotalcanQty}}</th>
                 <th>{{$perpageTotalgitQty}}</th>
                 <th>{{$totadhok}}</th>
                 <th colspan="2">{{$totpenqty}}</th>
                </tr>
            </tfoot> -->
          <tbody id="search_result_body">
          
          @if(isset($purchaseOrderList) && count($purchaseOrderList)>0)
<?php $incmain=0; $inc=0; ?>
                @foreach($purchaseOrderList as $eachOrder)
                <?php $inc++;$incmain++; 
                //$pending = ($eachOrder->ORDQTY - ($eachOrder->RCQTY+$eachOrder->CNLQTY + $eachOrder->git_qty));
                if(empty($eachOrder->p_git_qty))
                   $pending = ($eachOrder->ORDQTY - ($eachOrder->RCQTY+$eachOrder->CNLQTY));
                else if(!empty($eachOrder->p_git_qty) && empty($eachOrder->git_rcv))
                   $pending = $eachOrder->ORDQTY - ($eachOrder->RCQTY+$eachOrder->p_git_qty+$eachOrder->CNLQTY);
                 else if(!empty($eachOrder->p_git_qty) && !empty($eachOrder->git_rcv))
                   $pending = $eachOrder->ORDQTY - ($eachOrder->git_rcv+$eachOrder->p_git_qty+$eachOrder->CNLQTY);


                 $bal_quan=$eachOrder->RCQTY+$eachOrder->CNLQTY;
                                    //if($pending<=0)
                                        //$pending=str_replace('-', 'Adhoc:', $pending);
                  $adhoc_qty=0;
                  if($pending<=0)
                  {
                  //echo $pndg_qty=str_replace("-","Adhoc:",$pndg_qty);
                  $adhoc_qty=str_replace("-"," ",$pending);
                  $pending=0;
                  
                  }
                    ?>
                    <tr>
                        <!--<td class="text-center">{{$inc}}</td>-->


                                    
                      <td class="text-center">
                            <div class="gal"> 
                             <?php 
                               $pGalList=DB::table('product_gallery')->select('*')->where('product_id','=',$eachOrder->product_id)->get();
                               if($eachOrder->image!='' && file_exists('upload/product/'.$eachOrder->image))
                               $main_image=URL::to('/upload/product').'/'.$eachOrder->image;
                               if(empty($main_image) && !empty($pGalList) && count($pGalList)>0)
                               {
                                  foreach ($pGalList as $pGal) 
                                  { 
                                    if($pGal->product_image!='' && file_exists('upload/product/'.$pGal->product_image))
                                    {
                                      $main_image=URL::to('/upload/product').'/'.$pGal->product_image;
                                    }
                                  }
                               }
                               if(empty($main_image))
                                $main_image=URL::to('/img').'/pre-order-img.jpg';
                              ?>
                               <a class="fancybox" rel="group{{$incmain}}" href="{{$main_image}}" >
                                <img src="{{$main_image}}" width="150">
                               </a>
                               <?php 
                               if(!empty($pGalList) && count($pGalList)>0)
                               {
                                foreach ($pGalList as $pGal2) 
                                {
                                    if($pGal2->product_image!='' && file_exists('upload/product/'.$pGal2->product_image))
                                    {
                                      $main_image=URL::to('/upload/product').'/'.$pGal2->product_image;

                                ?>                                               
                               <a class="fancybox" rel="group{{$incmain}}" href="{{$main_image}}" style="display:none;"><img src="{{$main_image}}" alt=""></a>
                                <?php 
                                }
                                }
                                }
                               ?> 
                            </div>
                        </td>
                        <td class="text-center po-no<?php echo $inc;?>">{{$eachOrder->PONO}}</td>
                        <td class="text-center po-date<?php echo $inc;?>">{{date('d/m/Y',strtotime($eachOrder->ORDDT))}}</td>
                        <td class="text-center due-date<?php echo $inc;?>">{{date('d/m/Y',strtotime($eachOrder->DUEDATE))}}</td>
                        @if(isset($eachOrder->ICODE))
                        <td class="text-center ICODE<?php echo $inc;?>">{{$eachOrder->ICODE}}</td>
                        @else
                        <td class="text-center ICODE<?php echo $inc;?>"></td>
                        @endif
                        <td class="text-center cat-1<?php echo $inc;?>">{{$eachOrder->cat1}}</td>
                        <td class="text-center cat-2<?php echo $inc;?>">{{$eachOrder->cat2}}</td>
                        <td class="text-center cat-3<?php echo $inc;?>">{{$eachOrder->cat3}}</td>
                        <td class="text-center cat-4<?php echo $inc;?>">{{$eachOrder->cat4}}</td>
                        <td class="text-center cat-4<?php echo $inc;?>">{{$eachOrder->CNAME6}}</td>
                        <td class="text-center price<?php echo $inc;?>">{{$eachOrder->RATE}}</td>
                        <td class="text-center order-qty<?php echo $inc;?>">{{$eachOrder->ORDQTY}}</td>
                        <td class="text-center received-qty<?php echo $inc;?>">{{$eachOrder->RCQTY}}</td>
                        <td class="text-center cancelled-qty<?php echo $inc;?>">{{$eachOrder->CNLQTY}}</td>
                        <td class="text-center git-qty<?php echo $inc;?>">{{$eachOrder->p_git_qty}}</td>
                        <td class="text-center adhoc-qty<?php echo $inc;?>">{{$adhoc_qty}}</td>
                        <td class="text-center pending-qty<?php echo $inc;?>">{{$pending}}</td>
                        <td class="last-width text-center">
                        <select id="action_type" name="action_type" class="select-drop" onchange="return actionType(this.value,'<?php echo $inc;?>');">
                        <option value="">Select Type</option>
                        <?php
                        $cdisable='';
                        $rdisable='';
                        $wdisable='';
                        $ddisable='';
                        $odisable='';
                        $sdisable='';
                        $candisable='';
                        if(!empty($eachOrder->actiondtls) && count($eachOrder->actiondtls)>0){
                        foreach($eachOrder->actiondtls as $row){
                          if($row->action_type=='cutting' && $row->status=='new' && $row->ICODE==$eachOrder->ICODE) 
                           $cdisable='disabled';
                          if($row->action_type=='rate_change' && ($row->status=='new' || $row->status=='inprocess') && $row->ICODE==$eachOrder->ICODE)
                           $rdisable='disabled';
                          if($row->action_type=='width_change' && ($row->status=='new' || $row->status=='inprocess') && $row->ICODE==$eachOrder->ICODE)
                           $wdisable='disabled';
                          if($row->action_type=='date_ext' && $row->status=='new' && $row->ICODE==$eachOrder->ICODE)
                           $ddisable='disabled';
                          if($row->action_type=='others' && $row->status=='new' && $row->ICODE==$eachOrder->ICODE)
                           $odisable='disabled';
                          if($row->action_type=='sample' && $row->status=='new' && $row->ICODE==$eachOrder->ICODE)
                           $sdisable='disabled';
                          if($row->action_type=='cancel' && $row->status=='new' && $row->ICODE==$eachOrder->ICODE)
                           $candisable='disabled';

                        }
                        }
                         ?>
                          <option value="vendorChangecutting" {{$cdisable}}>REQUEST FOR SAMPLE</option>
                          <option value="vendorChangeprice" {{$rdisable}}>REQUEST FOR RATE CHANGE</option>
                          <option value="vendorChangewidth" {{$wdisable}}>REQUEST FOR WIDTH CHANGE</option>
                          <option value="vendorChangedate" {{$ddisable}}>REQUEST FOR DATE EXT</option>
                          <option value="vendorCancellation" {{$candisable}}>REQUEST FOR CANCEL</option>
                          <option value="vendorAddremarks" {{$odisable}}>REQUEST FOR OTHERS</option>
                          <option value="vendorSamplerequest" {{$sdisable}}>REQUEST FOR Approval</option>
                        </select>
                </td>
                    </tr>
                    @endforeach

                    @else
                        <tr><td style="font-size: 18px;" class="text-center" colspan="18" >Please enter search keyword! </td></tr>

                     @endif

                                </tbody>
                               </table>
                              </div>
@if(isset($purchaseOrderList) && count($purchaseOrderList)>0)
                             
@endif


                              <!--add vendorChangecutting Modal Start-->
<div id="vendorChangecutting" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close resetmodal" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for sample</h4>
      </div>
      <div class="modal-body vendorChangecutting-modal-body">
        <div class="row shorting-area pricechange">
            <input type="hidden" name="type" id="cutting_type" value="cutting" />
          <div class="col-sm-3"><h5>Design : <strong class="vendorChangecutting-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Colour : <strong class="vendorChangecutting-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Width/Size : <strong class="vendorChangecutting-cat4"><!--45"--></strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorChangecutting-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>I Code : <strong class="vendorChangecutting-icode"></strong></h5></div>
            <div class="col-sm-3"><h5>Date : <strong class="vendorChangecutting-date"><!--12/10/2016--></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorChangecutting-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorChangecutting-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorChangecutting-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorChangecutting-pending-qty"><!--1000--></strong></h5></div>
            <div class="clearfix"></div>
            <hr>
          <div class="clearfix"></div>
           <!--  <div class="col-sm-6 newpo" id="newpo">
            <h5>New PO :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestcutting_new_po" name="vendorRequestcutting_new_po"/>            
            </div> -->
            <div class="col-sm-12">
            <h5>Remarks :</h5>
            <textarea class="form-control" id="vendorChangecutting_remarks" name="vendorChangecutting_remarks" placeholder="Remarks"></textarea>
            <span id="vendorChangecutting_remarks_message"></span>
            </div>
            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorChangecutting preventDouble" id=""> Submit</a>
            </div>
        <div class="col-sm-6">
            <a href="#" data-dismiss="modal" class="pup-btn resetmodal">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--add vendorChangecutting  Modal End-->


<!--vendorChangeprice Modal Start-->
<div id="vendorChangeprice" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close resetmodal" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Rate Changes</h4>
      </div>
      <div class="modal-body vendorChangecutting-modal-body">
        <div class="row shorting-area pricechange">
        <input type="hidden" name="type" id="rate_change_type" value="rate_change" />            
            <div class="col-sm-3"><h5>Design : <strong class="vendorChangeprice-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Colour : <strong class="vendorChangeprice-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Width/Size : <strong class="vendorChangeprice-cat4"><!--45"--></strong></h5></div>
             <div class="col-sm-3"><h5>Order Date : <strong class="vendorChangeprice-po-date"><!--45"--></strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorChangeprice-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>I Code : <strong class="vendorChangeprice-icode"></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorChangeprice-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorChangeprice-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorChangeprice-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorChangeprice-pending-qty"><!--1000--></strong></h5></div>
            <div class="col-sm-3"><h5>Current rate : <strong class="vendorChangeprice-current-rate"><!--300--></strong></h5></div>
            
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
       <!--  <div class="col-sm-6 newpo" id="newpo">
            <h5>New PO :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestrate_new_po" name="vendorRequestrate_new_po"/>            
            </div> -->
          <div class="col-sm-6">
            <h5>New Rate :</h5>
            <input type="text" placeholder="" class="form-control" onkeypress="javascript:return isNumber(event)" id="vendorChangecutting_new_requested_rate" name="vendorChangecutting_new_requested_rate"/>
            <span id="vendorChangeRate_remarks_message"></span>
            </div>
            <div class="col-sm-6">
            <h5>Reason for change in rate :</h5>
            <textarea class="form-control" id="vendorChangecutting_reason" name="vendorChangecutting_reason"></textarea>
             <span id="vendorChangeRateReason_remarks_message"></span>
            </div>

            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorChangeprice preventDouble" id="preventDouble">Submit</a>
            </div>
        <div class="col-sm-6">
            <a href="#" data-dismiss="modal" class="pup-btn resetmodal">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorChangeprice  Modal End-->


<!--vendorChangewidth Modal Start-->
<div id="vendorChangewidth" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close resetmodal" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Width Changes</h4>
      </div>
      <div class="modal-body vendorChangecutting-modal-body">
        <div class="row shorting-area pricechange">
        <input type="hidden" name="type" id="width_change_type" value="width_change" />
          <div class="col-sm-3"><h5>Design : <strong class="vendorChangewidth-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Colour : <strong class="vendorChangewidth-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Width/Size : <strong class="vendorChangewidth-cat4"><!--45"--></strong></h5></div>
             <div class="col-sm-3"><h5>Order Date : <strong class="vendorChangewidth-po-date"><!--45"--></strong></h5></div>
            
             <div class="col-sm-3"><h5>PO. No : <strong class="vendorChangewidth-po-no"><!--234--></strong></h5></div>
              <div class="col-sm-3"><h5>I Code : <strong class="vendorChangewidth-icode"></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorChangewidth-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorChangewidth-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorChangewidth-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorChangewidth-pending-qty"><!--1000--></strong></h5></div>
            <div class="col-sm-3"><h5>Current rate : <strong class="vendorChangewidth-current-rate"><!--37--></strong></h5></div>
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
           <!--  <div class="col-sm-6 newpo" id="newpo">
            <h5>New PO :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestwidth_new_po" name="vendorRequestwidth_new_po"/>            
            </div> -->
          <div class="col-sm-6">
            <h5>New Rate :</h5>
            <input type="text" placeholder="" onkeypress="javascript:return isNumber(event)" class="form-control" id="vendorChangewidth_new_requested_rate" name="vendorChangewidth_new_requested_rate"/>
            <span id="vendorNewReqrate_remarks_message"></span>
            </div>
            <div class="col-sm-6">
            <h5>New Width :</h5>
            <input type="text" placeholder="" onkeypress="javascript:return isNumber(event)" class="form-control" id="vendorChangewidth_new_width" name="vendorChangewidth_new_width"/>
            <span id="vendorChangecuttingwidth_remarks_message"></span>
            </div><div class="clearfix"></div>
            <div class="col-sm-12">
            <h5>Reason for change in Width :</h5>
            <textarea class="form-control" id="vendorChangewidth_reason" name="vendorChangewidth_reason"></textarea>
            <span id="vendorChangecutting_width_message"></span>
            </div>
            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorChangewidth preventDouble" id="preventDouble">Submit</a>
            </div>
        <div class="col-sm-6">
            <a href="#" data-dismiss="modal" class="pup-btn resetmodal">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorChangewidth  Modal End-->


<!--vendorChangedate Modal Start-->
<div id="vendorChangedate" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close resetmodal" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Due Date extension</h4>
      </div>
      <div class="modal-body vendorChangecutting-modal-body">
        <div class="row shorting-area pricechange">
           <input type="hidden" name="type" id="date_ext_type" value="date_ext" /> 
            
            <div class="col-sm-3"><h5>Design : <strong class="vendorChangedate-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Colour : <strong class="vendorChangedate-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Width/Size : <strong class="vendorChangedate-cat4"><!--45"--></strong></h5></div>
            
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorChangedate-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>I Code : <strong class="vendorChangedate-icode"></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorChangedate-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorChangedate-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorChangedate-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorChangedate-pending-qty"><!--1000--></strong></h5></div>
            <div class="col-sm-3"><h5>Current rate : <strong class="vendorChangedate-current-rate"><!--37--></strong></h5></div>
            <div class="col-sm-4"><h5>Order Date : <strong class="vendorChangedate-order-date"><!--23/09/2016--></strong></h5></div>
            
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
        <!-- <div class="col-sm-6 newpo" id="newpo">
            <h5>New PO :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestdate_new_po" name="vendorRequestdate_new_po"/>            
            </div> -->
        <div class="col-sm-6">
            <h5>Current Item Due Date :</h5>
            <h4 style="text-align:left"><strong class="vendorChangedate-due-date"><!--30/10/2016--></strong></h4>
            </div>
          <div class="col-sm-6">
            <h5>Extend Due Date :</h5>
            <div class="form-group">
                <div class="input-group date">
                    <input type="text" class="form-control" placeholder="dd/mm/yy" id="vendorChangedate_extended_date" name="vendorChangedate_extended_date" />
                   <!--  <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span> -->
                    
                </div>
                <div class="clearfix"></div>
                <span id="vendorChangecuttingdate_remarks_message"></span>
            </div>
            </div>
            
            <div class="col-sm-12">
            <h5>Reason for Extension :</h5>
            <textarea class="form-control" id="vendorChangedate_reason" name="vendorChangedate_reason"></textarea>
            <span id="vendorChangecuttingDate_remarks_message"></span>
            </div>
            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorChangedate preventDouble" id="preventDouble">Submit</a>
            </div>
        <div class="col-sm-6">
            <a href="#" data-dismiss="modal" class="pup-btn resetmodal">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorChangedate  Modal End-->


<!--vendorCancellation order Modal Start-->
<div id="vendorCancellation" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close resetmodal" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Cancellation</h4>
      </div>
      <div class="modal-body vendorChangecutting-modal-body">
        <div class="row shorting-area pricechange">
        <input type="hidden" name="type" id="cancel_type" value="cancel" />             
            <div class="col-sm-3"><h5>Design : <strong class="vendorCancellation-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Colour : <strong class="vendorCancellation-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Width/Size : <strong class="vendorCancellation-cat4"><!--45"--></strong></h5></div>
             <div class="col-sm-3"><h5>Order Date : <strong class="vendorCancellation-po-date"><!--45"--></strong></h5></div>
             <div class="col-sm-3"><h5>Due Date : <strong class="vendorCancellation-due-date"></strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorCancellation-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>I Code : <strong class="vendorCancellation-icode"></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorCancellation-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorCancellation-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorCancellation-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorCancellation-pending-qty"><!--1000--></strong></h5></div>            
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
          <div class="col-sm-6">
            <h5>Cancel Qty :</h5>
            <input type="text" placeholder="" onkeypress="javascript:return isNumber(event)" class="form-control" id="vendorCancellation_number" name="vendorCancellation_number"/>
             <span id="vendorChangecuttingcancel_remarks_message"></span>
             <span id="vendorcancelqty_message"></span>
            </div>
            <div class="col-sm-6">
            <h5>Reason for Cancellation :</h5>
            <textarea class="form-control" id="vendorCancellation_reason" name="vendorCancellation_reason"></textarea>
            <span id="vendorChangecuttingCancel_remarks_message"></span>
            </div>
            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorCancellation preventDouble" id="preventDouble">Submit</a>
            </div>
        <div class="col-sm-6">
            <a href="#" data-dismiss="modal" class="pup-btn resetmodal">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorCancellation order  Modal End-->




<!--vendorAddremarks Modal Start-->
<div id="vendorAddremarks" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close resetmodal" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Remarks</h4>
      </div>
      <div class="modal-body vendorAddremarks-modal-body">
        <div class="row shorting-area pricechange">
         <input type="hidden" name="type" id="others_type" value="others" />
          <div class="col-sm-3"><h5>Design : <strong class="vendorAddremarks-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Colour : <strong class="vendorAddremarks-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Width/Size : <strong class="vendorAddremarks-cat4"><!--45"--></strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorAddremarks-po-no"><!--234--></strong></h5></div>
             <div class="col-sm-3"><h5>I Code : <strong class="vendorAddremarks-icode"></strong></h5></div>
            <div class="col-sm-3"><h5>Date : <strong class="vendorAddremarks-date"><!--12/10/2016--></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorAddremarks-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorAddremarks-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorAddremarks-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorAddremarks-pending-qty"><!--1000--></strong></h5></div> 
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix vendorAddremarks-modal-body-clearfix"></div>
       <!--  <div class="col-sm-6 newpo" id="newpo">
            <h5>New PO :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestothers_new_po" name="vendorRequestothers_new_po"/>            
            </div> -->
            <div class="col-sm-12">
            <h5>Add Remarks :</h5>
            <textarea class="form-control" id="vendorAddremarks_remarks" name="vendorAddremarks_remarks"></textarea>
            <span id="vendorOthers_remarks_message"></span>
            </div>
            <span id="vendorAddremarks_remarks_message"></span>
            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorAddremarks preventDouble" id="preventDouble">Submit</a>
            </div>
            <div class="col-sm-6">
            <a href="#" data-dismiss="modal" class="pup-btn resetmodal">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorAddremarks  Modal End-->  
<!--add vendorSamplerequest Modal Start-->
<div id="vendorSamplerequest" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close resetmodal" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Approval</h4>
      </div>
      <div class="modal-body vendorSamplerequest-modal-body">
        <div class="row shorting-area pricechange">
            <input type="hidden" name="type" id="sample_type" value="sample" />
            <div class="col-sm-3"><h5>Design : <strong class="vendorSamplerequest-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Colour : <strong class="vendorSamplerequest-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Width/Size : <strong class="vendorSamplerequest-cat4"><!--45"--></strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorSamplerequest-po-no"><!--234--></strong></h5></div>
             <div class="col-sm-3"><h5>I Code : <strong class="vendorSamplerequest-icode"></strong></h5></div>
            <div class="col-sm-3"><h5>Date : <strong class="vendorSamplerequest-date"><!--12/10/2016--></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorSamplerequest-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorSamplerequest-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorSamplerequest-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorSamplerequest-pending-qty"><!--1000--></strong></h5></div>
            <div class="clearfix"></div>
            <hr>
          
            <div class="col-sm-6 docet" id="docet">
            <h5>Docket No :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestaccept_new_docket" name="vendorRequestaccept_new_docket"/>
            <span id="vendorRequestaccept_docket-remarks_message"></span>
            </div>
          
            <div class="col-sm-6 courier" id="courier">
            <h5>Courier Name :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestaccept_new_courier" name="vendorRequestaccept_new_courier"/>
            <span id="vendorRequestaccept_courier-remarks_message"></span>
            </div>
            <div class="col-sm-12">
            <h5>Remarks :</h5>
            <textarea class="form-control" id="vendorSamplerequest-remarks" name="vendorSamplerequest-remarks" placeholder="Remarks"></textarea>
            <span id="vendorSample_remarks_message"></span>
            </div>
            <div class="clearfix vendorSamplerequest-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorSamplereq preventDouble" id="preventDouble">Submit</a>
            </div>
            <div class="col-sm-6">
            <a href="#" data-dismiss="modal" class="pup-btn resetmodal">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--add vendorChangecutting  Modal End-->
<script type="text/javascript">
$(document).ready(function () {   //For default data load          
$('#vendorChangedate_extended_date').on('changeDate', function(ev){
    $(this).datepicker('hide');
});
$('#vendorChangedate_extended_date').datepicker({                    
                    format: "dd/mm/yyyy",
                    autoclose: true,
                    startDate: '+1d',
                    //endDate: '+1m'
                   
                });
});
var ptoken='{{csrf_token()}}';
var cutting_url='{{route("purchase-order-request")}}';
</script>
<script src="{{asset('js/custom/purchase_order.js')}}"></script> 
<?php  if(isset($purchaseOrderList) && !empty($purchaseOrderList) && count($purchaseOrderList)>0){?>
<script>

           $(document).ready(function () {
            
            $('#example').dataTable({
                'order': [[ 2, "asc" ]],//Default Column Attribute
                'pageLength': 30,//Per Page Item
                 "searching": false,
                'dom': 'Bfrtip',//Header Info               
                'buttons': [ {extend: 'excel',exportOptions: {columns: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]},footer: false}],
                'aoColumnDefs': [          
                                   { aTargets: [ 0 ], bSortable: false },
                                   { aTargets: [ 1 ], bSortable: false },
                                   { aTargets: [ 2 ], bSortable: true },
                                   { aTargets: [ 3 ], bSortable: true },
                                   { aTargets: [ 4 ], bSortable: false },
                                   { aTargets: [ 5], bSortable: true },
                                   { aTargets: [ 6 ], bSortable: false },
                                   { aTargets: [ 7 ], bSortable: false },
                                   { aTargets: [ 8 ], bSortable: false },
                                   { aTargets: [ 9 ], bSortable: false },
                                   { aTargets: [ 10 ], bSortable: false },
                                   { aTargets: [ 11 ], bSortable: false },
                                   { aTargets: [ 12 ], bSortable: false },
                                   { aTargets: [ 13 ], bSortable: false },
                                   { aTargets: [ 14 ], bSortable: false },
                                   { aTargets: [ 15 ], bSortable: false },
                                   { aTargets: [ 16 ], bSortable: true },
                                   { aTargets: [ 17 ], bSortable: false }

                                ],
                "oLanguage": {
                                "oPaginate": {
                                                "sNext": '<i class="fa fa-chevron-right" ></i>',
                                                "sPrevious": '<i class="fa fa-chevron-left" ></i>'
                                            }
                            }
            });
        });
         
         
</script>
 <?php }?>
