@include('layouts.vendor_header')
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <!--<div class="row">
                    <div class="col-lg-12 title-top">
                    <div class="row page-header">
                    	<div class="col-sm-6">
                        	<h1>Vendor <small>Purchase Order Listing</small></h1>
                        </div>
                        <div class="col-sm-6">
                        	<a href="javascript:void(0)" class="btn btn-success">Expand</a>
                        </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                	<div class="col-sm-6"><h3 class="panel-title">
                                    <i class="fa fa-users" aria-hidden="true"></i> Vendor Purchase Order Listing</h3></div>
                                    <div class="col-sm-6">
                                    
                                    <div class="shorting-area">
                            		
                              <!--<div class="col-sm-4 pull-right">
                                        <a class="btn btn-default btn-select">
                                            <input type="hidden" class="btn-select-input" id="" name="" value="" />
                                            <span class="btn-select-value">Select an Item</span>
                                            <span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                                        	<ul>
                                                <li class="selected">Short By</li>
                                                <li>Due Date</li>
                                                <li>Order Date</li>
                                                <li>Pending Qty</li>
                                        	</ul>
                                        </a>
                                </div>-->
                                <div class="col-sm-3 pull-right">
                                        <a href="{{route('archieve-purchase-order-listing')}}" class="btn btn-success">Archive</a>
                                </div>
                                <div class="col-sm-3 pull-right">
                                        <a href="javascript:void(0)" id="show-btn" class="btn btn-success">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                </div>
                            </div>
                                    
                                    
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                            <div class="row shorting-area">
                            <div class="col-sm-12">
                            <div id="mySidenavR" class="sidenavR" style="display:none;">
<div class="filter-right">
<div class="filter-area">
<!--<h5>Vendor List Filter</h5>-->
    <a href="javascript:void(0)" class="closebtn" id="hide-filter">×</a>
        <div class="col-sm-2">
           <select class="select-drop" id="po_number" name="po_number">
            	<option value="">PO Number</option>
                @if(!empty($po_number_list) && count($po_number_list)>0)
                @foreach($po_number_list as $po_number)
                	<option value="{{$po_number->PONO}}">{{$po_number->PONO}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="col-sm-2">
        	<select class="select-drop" id="category1" name="category1">
            	<option value="">Design</option>
                @if(!empty($cat1_list) && count($cat1_list)>0)
                @foreach($cat1_list as $cat1)
                	<option value="{{$cat1->cat1}}">{{$cat1->cat1}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="col-sm-2">
        	<select class="select-drop" id="category2" name="category2">
            	<option value="">Colour</option>
                @if(!empty($cat2_list) && count($cat2_list)>0)
                @foreach($cat2_list as $cat2)
                	<option value="{{$cat2->cat2}}">{{$cat2->cat2}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="col-sm-2">
        <div class="form-group">
                <div class="input-group date">
                    <input type="text" class="form-control date_picker" placeholder="Due From(dd/mm/yy)"  id="due_from" name="due_from"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-2">
        <div class="form-group">
                <div class="input-group date">
                    <input type="text" class="form-control date_picker" placeholder="Due To(dd/mm/yy)"  id="due_to" name="due_to"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-2">
        	<a href="javascript:void(0)" class="btn btn-success" onclick="applyFilter();">Apply Filter</a>
            <div class="clearfix"></div>
        </div>
<div class="clearfix"></div>        
	</div>
    <div class="clearfix"></div>
    </div>    
</div>
                            
                            </div>
                            
                            
                            </div>
                            <span id="filterResult">
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Sr. No</th>
                                                <th class="text-center">PO. No</th>
                                                <th class="text-center">PO. Date</th>
                                                <th class="text-center">Due. Date</th>
                                                <th class="text-center">Item Code</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Width/Size</th>
                                                <th class="text-center">Price</th>
                                                <th class="text-center">Order Qty</th>
                                                <th class="text-center">Received Qty</th>
                                                <th class="text-center">Cancelled Qty</th>
                                                <th class="text-center">GIT Qty</th>
                                                <th class="text-center">Pending Qty</th>
                                                <th class="text-center">Adhoc Qty</th>
                                                <th class="last-width text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <?php $inc=$purchaseOrderList->perPage() * ($purchaseOrderList->currentPage()-1); ?>
                                    @foreach($purchaseOrderList as $eachOrder)
                                    <?php $inc++; 
                                    $pending = ($eachOrder->ORDQTY - ($eachOrder->RCQTY+$eachOrder->CNLQTY + $eachOrder->git_qty));
                                    //if($pending<=0)
                                        //$pending=str_replace('-', 'Adhoc:', $pending);
									$adhoc_qty=0;
									if($pending<=0)
									{
									//echo $pndg_qty=str_replace("-","Adhoc:",$pndg_qty);
									$adhoc_qty=str_replace("-"," ",$pending);
									$pending=0;
									
									}
                                    ?>
                                    <tr>
                                        <td class="text-center">{{$inc}}</td>
                                        <td class="text-center po-no<?php echo $inc;?>">{{$eachOrder->PONO}}</td>
                                        <td class="text-center po-date<?php echo $inc;?>">{{date('d/m/Y',strtotime($eachOrder->ORDDT))}}</td>
                                        <td class="text-center due-date<?php echo $inc;?>">{{date('d/m/Y',strtotime($eachOrder->TIME))}}</td>
                                        @if(isset($eachOrder->ICODE))
                                        <td class="text-center ICODE<?php echo $inc;?>">{{$eachOrder->ICODE}}</td>
                                        @else
                                        <td class="text-center ICODE<?php echo $inc;?>"></td>
                                        @endif
                                        <td class="text-center cat-1<?php echo $inc;?>">{{$eachOrder->cat1}}</td>
                                        <td class="text-center cat-2<?php echo $inc;?>">{{$eachOrder->cat2}}</td>
                                        <td class="text-center cat-3<?php echo $inc;?>">{{$eachOrder->cat3}}</td>
                                        <td class="text-center cat-4<?php echo $inc;?>">{{$eachOrder->cat4}}</td>
                                        <td class="text-center price<?php echo $inc;?>">{{$eachOrder->RATE}}</td>
                                        <td class="text-center order-qty<?php echo $inc;?>">{{$eachOrder->ORDQTY}}</td>
                                        <td class="text-center received-qty<?php echo $inc;?>">{{$eachOrder->RCQTY}}</td>
                                        <td class="text-center cancelled-qty<?php echo $inc;?>">{{$eachOrder->CNLQTY}}</td>
                                        <td class="text-center git-qty<?php echo $inc;?>">{{$eachOrder->git_qty}}</td>
                                        <td class="text-center pending-qty<?php echo $inc;?>">{{$pending}}</td>
                                        <td class="text-center pending-qty<?php echo $inc;?>">{{$adhoc_qty}}</td>
                                        <td class="last-width text-center">
                                        <select id="action_type" name="action_type" class="select-drop" onchange="return actionType(this.value,'<?php echo $inc;?>');">
                                        <option value="">Select Type</option>
                                        <?php
                                        $cdisable='';
                                        $rdisable='';
                                        $wdisable='';
                                        $ddisable='';
                                        $odisable='';
                                        $sdisable='';
                                        $candisable='';
                                        if(!empty($eachOrder->actiondtls) && count($eachOrder->actiondtls)>0){
                                        foreach($eachOrder->actiondtls as $row){
                                          if($row->action_type=='cutting' && $row->status=='new') 
                                           $cdisable='disabled';
                                          if($row->action_type=='rate_change' && $row->status=='new')
                                           $rdisable='disabled';
                                          if($row->action_type=='width_change' && $row->status=='new')
                                           $wdisable='disabled';
                                          if($row->action_type=='date_ext' && $row->status=='new')
                                           $ddisable='disabled';
                                          if($row->action_type=='others' && $row->status=='new')
                                           $odisable='disabled';
                                          if($row->action_type=='sample' && $row->status=='new')
                                           $sdisable='disabled';
                                          if($row->action_type=='cancel' && $row->status=='new')
                                           $candisable='disabled';

                                        }
                                        }
                                         ?>
                                        <option value="vendorChangecutting" {{$cdisable}}>REQUEST FOR CUTTING</option>
                                        <option value="vendorChangeprice" {{$rdisable}} >REQUEST FOR RATE CHANGE</option>
                                        <option value="vendorChangewidth" {{$wdisable}}>REQUEST FOR WIDTH CHANGE</option>
                                        <option value="vendorChangedate" {{$ddisable}}>REQUEST FOR DUE DATE EXT</option>
                                        <option value="vendorCancellation" {{$candisable}}>REQUEST FOR CANCEL</option>
                                        <option value="vendorAddremarks" {{$odisable}}>REQUEST FOR OTHERS</option>
                                        <option value="vendorSamplerequest" {{$sdisable}}>REQUEST FOR INSPECTION</option>
                                       
                                        </select>
                            		</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                              <div class="col-sm-5">
                                <div class="dataTables_info" role="status" aria-live="polite">Showing {{$purchaseOrderList->count()}} of {{$purchaseOrderList->total()}} entries</div>
                              </div>
                              <div class="col-sm-7">
                                    <nav aria-label="Page navigation" class="pull-right">
                                        {{ $purchaseOrderList->links() }}
                                    </nav>
                                  </div>
                              </div>
                              </span>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@include('layouts.footer') 
<!--add vendorChangecutting Modal Start-->
<div id="vendorChangecutting" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Cutting</h4>
      </div>
      <div class="modal-body vendorChangecutting-modal-body">
        <div class="row shorting-area pricechange">
            <input type="hidden" name="type" id="cutting_type" value="cutting" />
        	<div class="col-sm-3"><h5>Design : <strong class="vendorChangecutting-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Colour : <strong class="vendorChangecutting-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Width/Size : <strong class="vendorChangecutting-cat4"><!--45"--></strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorChangecutting-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>Date : <strong class="vendorChangecutting-date"><!--12/10/2016--></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorChangecutting-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorChangecutting-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorChangecutting-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorChangecutting-pending-qty"><!--1000--></strong></h5></div>
            <div class="clearfix"></div>
            <hr>
        	<div class="clearfix"></div>
           <!--  <div class="col-sm-6 newpo" id="newpo">
            <h5>New PO :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestcutting_new_po" name="vendorRequestcutting_new_po"/>            
            </div> -->
            <div class="col-sm-12">
            <h5>Remarks :</h5>
            <textarea class="form-control" id="vendorChangecutting_remarks" name="vendorChangecutting_remarks" placeholder="Remarks"></textarea>
            <span id="vendorChangecutting_remarks_message"></span>
            </div>
            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorChangecutting"> Submit</a>
            </div>
        <div class="col-sm-6">
            <a href="#" data-dismiss="modal" class="pup-btn">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--add vendorChangecutting  Modal End-->


<!--vendorChangeprice Modal Start-->
<div id="vendorChangeprice" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Rate Changes</h4>
      </div>
      <div class="modal-body vendorChangecutting-modal-body">
        <div class="row shorting-area pricechange">
        <input type="hidden" name="type" id="rate_change_type" value="rate_change" />            
            <div class="col-sm-3"><h5>Design : <strong class="vendorChangeprice-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Colour : <strong class="vendorChangeprice-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Width/Size : <strong class="vendorChangeprice-cat4"><!--45"--></strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorChangeprice-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorChangeprice-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorChangeprice-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorChangeprice-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorChangeprice-pending-qty"><!--1000--></strong></h5></div>
            <div class="col-sm-3"><h5>Current rate : <strong class="vendorChangeprice-current-rate"><!--300--></strong></h5></div>
            
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
       <!--  <div class="col-sm-6 newpo" id="newpo">
            <h5>New PO :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestrate_new_po" name="vendorRequestrate_new_po"/>            
            </div> -->
        	<div class="col-sm-6">
            <h5>New requested rate :</h5>
            <input type="text" placeholder="" class="form-control" onkeypress="javascript:return isNumber(event)" id="vendorChangecutting_new_requested_rate" name="vendorChangecutting_new_requested_rate"/>
            <span id="vendorChangeRate_remarks_message"></span>
            </div>
            <div class="col-sm-6">
            <h5>Reason for change in rate :</h5>
            <textarea class="form-control" id="vendorChangecutting_reason" name="vendorChangecutting_reason"></textarea>
             <span id="vendorChangeRateReason_remarks_message"></span>
            </div>

            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorChangeprice">Submit</a>
            </div>
        <div class="col-sm-6">
            <a href="#" data-dismiss="modal" class="pup-btn">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorChangeprice  Modal End-->


<!--vendorChangewidth Modal Start-->
<div id="vendorChangewidth" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Width Changes</h4>
      </div>
      <div class="modal-body vendorChangecutting-modal-body">
        <div class="row shorting-area pricechange">
        <input type="hidden" name="type" id="width_change_type" value="width_change" />
        	<div class="col-sm-3"><h5>Design : <strong class="vendorChangewidth-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Colour : <strong class="vendorChangewidth-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Width/Size : <strong class="vendorChangewidth-cat4"><!--45"--></strong></h5></div>
            
            
             <div class="col-sm-3"><h5>PO. No : <strong class="vendorChangewidth-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorChangewidth-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorChangewidth-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorChangewidth-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorChangewidth-pending-qty"><!--1000--></strong></h5></div>
            <div class="col-sm-3"><h5>Current rate : <strong class="vendorChangewidth-current-rate"><!--37--></strong></h5></div>
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
           <!--  <div class="col-sm-6 newpo" id="newpo">
            <h5>New PO :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestwidth_new_po" name="vendorRequestwidth_new_po"/>            
            </div> -->
        	<div class="col-sm-6">
            <h5>New requested rate :</h5>
            <input type="text" placeholder="" onkeypress="javascript:return isNumber(event)" class="form-control" id="vendorChangewidth_new_requested_rate" name="vendorChangewidth_new_requested_rate"/>
            <span id="vendorNewReqrate_remarks_message"></span>
            </div>
            <div class="col-sm-6">
            <h5>New Width :</h5>
            <input type="text" placeholder="" onkeypress="javascript:return isNumber(event)" class="form-control" id="vendorChangewidth_new_width" name="vendorChangewidth_new_width"/>
            <span id="vendorChangecuttingwidth_remarks_message"></span>
            </div><div class="clearfix"></div>
            <div class="col-sm-12">
            <h5>Reason for change in Width :</h5>
            <textarea class="form-control" id="vendorChangewidth_reason" name="vendorChangewidth_reason"></textarea>
            <span id="vendorChangecutting_width_message"></span>
            </div>
            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorChangewidth">Submit</a>
            </div>
        <div class="col-sm-6">
            <a href="#" data-dismiss="modal" class="pup-btn">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorChangewidth  Modal End-->


<!--vendorChangedate Modal Start-->
<div id="vendorChangedate" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Due Date extension</h4>
      </div>
      <div class="modal-body vendorChangecutting-modal-body">
        <div class="row shorting-area pricechange">
           <input type="hidden" name="type" id="date_ext_type" value="date_ext" /> 
            
            <div class="col-sm-3"><h5>Design : <strong class="vendorChangedate-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Colour : <strong class="vendorChangedate-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Width/Size : <strong class="vendorChangedate-cat4"><!--45"--></strong></h5></div>
            
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorChangedate-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorChangedate-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorChangedate-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorChangedate-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorChangedate-pending-qty"><!--1000--></strong></h5></div>
            <div class="col-sm-3"><h5>Current rate : <strong class="vendorChangedate-current-rate"><!--37--></strong></h5></div>
            <div class="col-sm-4"><h5>Order Date : <strong class="vendorChangedate-order-date"><!--23/09/2016--></strong></h5></div>
            
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
        <!-- <div class="col-sm-6 newpo" id="newpo">
            <h5>New PO :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestdate_new_po" name="vendorRequestdate_new_po"/>            
            </div> -->
        <div class="col-sm-6">
            <h5>Current Item Due Date :</h5>
            <h4 style="text-align:left"><strong class="vendorChangedate-due-date"><!--30/10/2016--></strong></h4>
            </div>
        	<div class="col-sm-6">
            <h5>Extended Due Date :</h5>
            <div class="form-group">
                <div class="input-group date">
                    <input type="text" class="form-control datepicker" placeholder="dd/mm/yy" id="vendorChangedate_extended_date" name="vendorChangedate_extended_date" />
                   <!--  <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span> -->
                    
                </div>
                <div class="clearfix"></div>
                <span id="vendorChangecuttingdate_remarks_message"></span>
            </div>
            </div>
            
            <div class="col-sm-12">
            <h5>Reason for Extension :</h5>
            <textarea class="form-control" id="vendorChangedate_reason" name="vendorChangedate_reason"></textarea>
            <span id="vendorChangecuttingDate_remarks_message"></span>
            </div>
            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorChangedate">Submit</a>
            </div>
        <div class="col-sm-6">
            <a href="#" data-dismiss="modal" class="pup-btn">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorChangedate  Modal End-->


<!--vendorCancellation order Modal Start-->
<div id="vendorCancellation" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Cancellation</h4>
      </div>
      <div class="modal-body vendorChangecutting-modal-body">
        <div class="row shorting-area pricechange">
        <input type="hidden" name="type" id="cancel_type" value="cancel" />             
            <div class="col-sm-3"><h5>Design : <strong class="vendorCancellation-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Colour : <strong class="vendorCancellation-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Width/Size : <strong class="vendorCancellation-cat4"><!--45"--></strong></h5></div>
            
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorCancellation-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorCancellation-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorCancellation-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorCancellation-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorCancellation-pending-qty"><!--1000--></strong></h5></div>            
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
        	<div class="col-sm-6">
            <h5>Cancel Qty :</h5>
            <input type="text" placeholder="" onkeypress="javascript:return isNumber(event)" class="form-control" id="vendorCancellation_number" name="vendorCancellation_number"/>
             <span id="vendorChangecuttingcancel_remarks_message"></span>
            </div>
            <div class="col-sm-6">
            <h5>Reason for Cancellation :</h5>
            <textarea class="form-control" id="vendorCancellation_reason" name="vendorCancellation_reason"></textarea>
            <span id="vendorChangecuttingCancel_remarks_message"></span>
            </div>
            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorCancellation">Submit</a>
            </div>
        <div class="col-sm-6">
            <a href="#" data-dismiss="modal" class="pup-btn">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorCancellation order  Modal End-->




<!--vendorAddremarks Modal Start-->
<div id="vendorAddremarks" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Remarks</h4>
      </div>
      <div class="modal-body vendorAddremarks-modal-body">
        <div class="row shorting-area pricechange">
         <input type="hidden" name="type" id="others_type" value="others" />
        	<div class="col-sm-3"><h5>Design : <strong class="vendorAddremarks-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Colour : <strong class="vendorAddremarks-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Width/Size : <strong class="vendorAddremarks-cat4"><!--45"--></strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorAddremarks-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>Date : <strong class="vendorAddremarks-date"><!--12/10/2016--></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorAddremarks-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorAddremarks-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorAddremarks-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorAddremarks-pending-qty"><!--1000--></strong></h5></div> 
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix vendorAddremarks-modal-body-clearfix"></div>
       <!--  <div class="col-sm-6 newpo" id="newpo">
            <h5>New PO :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestothers_new_po" name="vendorRequestothers_new_po"/>            
            </div> -->
            <div class="col-sm-12">
            <h5>Add Remarks :</h5>
            <textarea class="form-control" id="vendorAddremarks_remarks" name="vendorAddremarks_remarks"></textarea>
            <span id="vendorOthers_remarks_message"></span>
            </div>
            <span id="vendorAddremarks_remarks_message"></span>
            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorAddremarks">Submit</a>
            </div>
            <div class="col-sm-6">
            <a href="#" data-dismiss="modal" class="pup-btn">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorAddremarks  Modal End-->  
<!--add vendorSamplerequest Modal Start-->
<div id="vendorSamplerequest" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Inspection</h4>
      </div>
      <div class="modal-body vendorSamplerequest-modal-body">
        <div class="row shorting-area pricechange">
            <input type="hidden" name="type" id="sample_type" value="sample" />
            <div class="col-sm-3"><h5>Design : <strong class="vendorSamplerequest-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Colour : <strong class="vendorSamplerequest-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Width/Size : <strong class="vendorSamplerequest-cat4"><!--45"--></strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorSamplerequest-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>Date : <strong class="vendorSamplerequest-date"><!--12/10/2016--></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorSamplerequest-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorSamplerequest-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorSamplerequest-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorSamplerequest-pending-qty"><!--1000--></strong></h5></div>
            <div class="clearfix"></div>
            <hr>
           <!--  <div class="clearfix"></div> -->
           <!--  <div class="col-sm-6 newpo" id="newpo">
            <h5>New PO :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestaccept_new_po" name="vendorRequestaccept_new_po"/>            
            </div> -->
            <div class="col-sm-6 docet" id="docet">
            <h5>Docket No :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestaccept_new_docket" name="vendorRequestaccept_new_docket"/>
            <span id="vendorRequestaccept_docket-remarks_message"></span>
            </div>
           <!--  <div class="col-sm-6 prnno" id="prnno">
            <h5>PRN No :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestaccept_new_prn" name="vendorRequestaccept_new_prn"/>
            <span id="vendorRequestaccept_prn-remarks_message"></span>
            </div> -->
           <!--  <div class="col-sm-6 prndate" id="prndate">
            <h5>PRN Date :</h5>
           
            <div class="input-group date">
                     <input type="text" placeholder="dd/mm/yy" class="form-control date_picker" id="vendorRequestaccept_new_prndate" name="vendorRequestaccept_new_prndate" />
                   
                    
                </div>
            <span id="vendorRequestaccept_prndate-remarks_message"></span>
            </div> -->
      <!--       <div class="clearfix"></div> -->
            <div class="col-sm-6 courier" id="courier">
            <h5>Courier Name :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestaccept_new_courier" name="vendorRequestaccept_new_courier"/>
            <span id="vendorRequestaccept_courier-remarks_message"></span>
            </div>
            <div class="col-sm-12">
            <h5>Remarks :</h5>
            <textarea class="form-control" id="vendorSamplerequest-remarks" name="vendorSamplerequest-remarks" placeholder="Remarks"></textarea>
            <span id="vendorSample_remarks_message"></span>
            </div>
            <div class="clearfix vendorSamplerequest-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorSamplereq">Submit</a>
            </div>
            <div class="col-sm-6">
            <a href="#" data-dismiss="modal" class="pup-btn">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--add vendorChangecutting  Modal End-->
<script>
$('#vendorChangedate_extended_date').on('changeDate', function(ev){
    $(this).datepicker('hide');
});

// $('#vendorRequestaccept_new_prndate').on('changeDate', function(ev){
//     $(this).datepicker('hide');
// });

$('#due_to').on('changeDate', function(ev){
    $(this).datepicker('hide');
});

$('#due_from').on('changeDate', function(ev){
    $(this).datepicker('hide');
});


var ptoken='{{csrf_token()}}';
var cutting_url='{{route("purchase-order-request")}}';
</script>
<script src="{{asset('js/custom/purchase_order.js')}}"></script> 

<script>
$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        //$('#load a').css('color', '#dfecf6');
        //$('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');

        var url = $(this).attr('href'); 
		var page=$(this).attr('href').split('page=')[1];
		
		var po_number=$("#po_number").val();
		var category1=$("#category1").val();
		var category2=$("#category2").val();
		var due_from=$("#due_from").val();
		var due_to=$("#due_to").val();
	 
        filter(page,po_number,category1,category2,due_from,due_to);
        //window.history.pushState("", "", url);
    });

    
});

function applyFilter(){
		var po_number=$("#po_number").val();
		var category1=$("#category1").val();
		var category2=$("#category2").val();
		var due_from=$("#due_from").val();
		var due_to=$("#due_to").val();
	 
        filter(1,po_number,category1,category2,due_from,due_to);
}

$( "#hide-filter" ).click(function() {
	
	$("#po_number").val('');
	$("#category1").val('');
	$("#category2").val('');
	$("#due_from").val('');
	$("#due_to").val('');
	
	filter(); 
});

function filter(page=1,po_number='',category1='',category2='',due_from='',due_to=''){
	//if(po_number!='' || category1!=''  || category2!=''  || due_from!='' due_to!='')	
	$('.loading').show();
	$.ajax({
		type: 'GET', 
		url: '{{route("filter-purchase-ordering")}}',
		data: {po_number:po_number,category1:category1,category2:category2,due_from:due_from,due_to:due_to,page:page},
		success:function(data)
		{
			$('#filterResult').html(data);
			$('.loading').hide();
		},
		error: function (error) 
		{
			
			alert('Error Occured Please Try Again');
		}
	})
}
</script>     
<script>
    // WRITE THE VALIDATION SCRIPT IN THE HEAD TAG.
    function isNumber(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
            return false;

        return true;
    }    
</script>
