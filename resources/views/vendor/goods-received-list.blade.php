@include('layouts.vendor_header')
<!-- CSS -->
<link rel="stylesheet" href="{{asset('fancybox/css/fancybox/jquery.fancybox-buttons.css')}}">
<link rel="stylesheet" href="{{asset('fancybox/css/fancybox/jquery.fancybox-thumbs.css')}}">
<link rel="stylesheet" href="{{asset('fancybox/css/fancybox/jquery.fancybox.css')}}">
<link rel="stylesheet" href="{{asset('fancybox/demo/demo.css')}}">
<!-- DELETE -->
    <div id="page-wrapper">
        <div class="container-fluid inner-body-area">
            <div class="row">
                <div class="col-lg-12">
                @if(!empty($errors->all()) )

                    <div class="btn btn-success" id="hide-me" style="width:100%;background-color: green;border: none;">{{ $errors->all()[0] }}</div>
                @endif
                    <div class="panel panel-default add-padding">
                        <div class="panel-heading">
                            <div class="row">
                            	<div class="col-sm-6">                                
                                <h3 class="panel-title"><i class="fa fa-cloud-download" aria-hidden="true"></i>
                                 Goods Return List</h3>
                                </div>
                                <div class="col-sm-6"> 
                                    <div class="shorting-area">
                                        <div class="col-sm-3 purchase-filter pull-right">
                                            <a href="javascript:void(0)" onclick="getCat1(1);" id="show-btn" class="btn btn-success">
                                            <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                        </div>
                                        <div class="col-sm-3 pull-right">
                                             <a href="{{route('goods-closed')}}" data-toggle="modal" class="btn btn-danger">
                                             <i  aria-hidden="true"></i>Closed</a>
                                        </div>
                                    </div>
                                </div>
                            </div>  

                        </div>
                        <div class="col-sm-12">
                            <div id="mySidenavR" class="sidenavR" style="display:none;">
                                <div class="filter-right">
                            <div class="filter-area">
                            <!--<h5>Vendor List Filter</h5>-->
                                <a href="javascript:void(0)" class="closebtn" id="hide-filter">×</a>
                                    
                                    
                                    <!-- <div class="col-sm-2">
                                       <select class="select-drop" id="name" name="vendore_name">
                                            <option value="">Vendore Name</option>
                                            @if(!empty($vendor_name_list) && count($vendor_name_list)>0)
                                            @foreach($vendor_name_list as $vendore)
                                                <option value="{{$vendore->id}}">{{$vendore->name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div> -->
                                    
                                    
                                    <div class="col-sm-2">
                                       <!--  <select class="select-drop" id="cat1" name="category1">
                                            <option value="">Design</option>
                                            @if(!empty($cat1_list) && count($cat1_list)>0)
                                            @foreach($cat1_list as $cat1)
                                                <option value="{{$cat1->category1}}">{{$cat1->category1}}</option>
                                            @endforeach
                                            @endif
                                        </select> -->
                                        <select class="select-drop" id="cat1" name="category1">
                                            <option value="">Select Design</option>
                                            
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <!-- <select class="select-drop" id="cat2" name="category2">
                                            <option value="">Colour</option>
                                            @if(!empty($cat2_list) && count($cat2_list)>0)
                                            @foreach($cat2_list as $cat2)
                                                <option value="{{$cat2->category2}}">{{$cat2->category2}}</option>
                                            @endforeach
                                            @endif
                                        </select> -->
                                        <select class="select-drop" id="cat2" name="category2">
                                            <option value="">Select Colour</option>
                                            
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <a href="javascript:void(0)" class="btn btn-success" onclick="applyFilter();">Apply Filter</a>
                                        <div class="clearfix"></div>
                                    </div>
                                     <div class="col-sm-1"> 
                                      <a href="javascript:void(0)" class="btn btn-success dateCheck reset" onclick="">Clear</a>
                                      <div class="clearfix"></div>  
                                     </div>
                                    <div class="clearfix"></div>        
                                </div>
                                <div class="clearfix"></div>
                                </div>    
                            </div>
                                                
                        </div>
                        <div class="clearfix"></div>
                        <span id="filterResult">
                        <div class="panel-body custome-table">
                            <div class="table-responsive ">
                                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <!-- <th class="text-center">Prodict Id</th> -->
                                            <th class="text-center">Image</th>
                                            <th class="text-center">Vendor Name</th>
                                            <th class="text-center">Design</th>
                                            <th class="text-center">Colour</th>
                                            <th class="text-center">Defective Remarks</th>
                                            <th class="text-center">Quantity</th>
                                            <th class="text-center">PRN No</th>
                                            <th class="text-center">PRN Date</th>
                                            <th class="text-center">Docket No</th>
                                            <th class="text-center">Courier Name</th>
                                            <!-- <th class="text-center">Admin Remarks</th> -->
                                            <th class="text-center">Vendor Remarks</th>
                                            <th class="text-center">Last Comments</th>
                                            <th class="text-center">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                                              
                                    </tbody>
                               </table>
                          </div>
                         

                        </div>
                        </span>
                    </div>
                </div>
            </div>
            
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
@include('layouts.footer')

<!--add Remark Modal Start-->
<form method="post" action="{{route('goods-received-add')}}">
    {{ csrf_field() }}
    <div id="addremark" class="modal fade quotation-pop" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Vendor Comment</h4>
          </div>
          <div class="modal-body">
            <div class="row shorting-area pricechange">
                <div class="col-sm-4"><h5>PRN No : <strong><span id="prn1_no"></span></strong></h5></div>
                <div class="col-sm-4"><h5>Docket No : <strong><span id="docket1_no"></span></strong></h5></div>
                <div class="col-sm-4"><h5>Courier Name : <strong><span id="courier1_name"></span></strong></h5></div>
                <div class="clearfix"></div>
                <hr>
                <div class="clearfix"></div>
                <div class="col-sm-12">
                    <h5>Remarks :</h5>
                    <input type="hidden" class="form-control" name="returntId" id="returntId" >
                    <input type="hidden" class="form-control" name="design" id="design" >
                    <textarea class="form-control" name="vendor_comment" id="comment_description"></textarea>
                    <span id="comment_message"></span>
                </div>
                <div class="clearfix"></div>
                <div class="col-sm-6">
                    <button onclick="return validateForm();" class="pup-btn">Submit</button>
                </div>
                <div class="col-sm-6">
                    <button class="pup-btn" data-dismiss="modal">Cancel</button>
                </div>
            <div class="clearfix"></div>
            </div>
          </div>
          <div class="modal-footer"></div>
        </div>

      </div>
    </div>
</form>
<!--add Remark  Modal End-->
<script>
    function validateForm(){    
        
        if($.trim($("#comment_description").val())==''){
            $("#comment_message").text('Please Give some Comment!').css('color', 'red').show();
            return false;
        }
        else{
            $("#comment_message").hide();
            return true;
        }
    }

    function getRequest(id,design,prn_no,docket_no,courier_name){
        
        $('#design').val(design);  
        $('#returntId').val(id);  
        $('#prn1_no').html(prn_no);  
        $('#docket1_no').html(docket_no);  
        $('#courier1_name').html(courier_name);  

    }
</script>
<script type="text/javascript">
     setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 2000);
</script>

<script>

$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        $('#load a').css('color', '#dfecf6');
        //$('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');

        var url = $(this).attr('href'); 
        var page=$(this).attr('href').split('page=')[1];
        //alert(page);
        var cat1=$("#cat1").val();
        var cat2=$("#cat2").val();
       // var name=$("#name").val();
        
        
     
        filter(page,cat1,cat2);
        //window.history.pushState("", "", url);
    });

    
});

$( "#hide-filter" ).click(function() {
    $("#cat1").val('');
    $("#cat2").val('');
    //$("#name").val('');
        
    filter(); 
});

function applyFilter(){ 
    
        var cat1=$("#cat1").val();
        var cat2=$("#cat2").val();
        //var name=$("#name").val();
       
    
        filter(1,cat1,cat2);
}

function filter(page=1,cat1='',cat2=''){ 

    $('.loading').show();
    
    $.ajax({
        type: 'GET', 
        url: '{{route("filter-vendor-goods-return")}}',
        data: {page:page,cat1:cat1,cat2:cat2},
        success:function(data)
        {
            $('#filterResult').html(data);
            $('.loading').hide();
        },
        error: function (error) 
        {
            
            alert('Error Occured Please Try Again');
        }
    })
}

$(document).ready(function () {        
        applyFilter();
    });

    function getCat1(val)
    {

        $.ajax({
            type: 'GET',
            url: '{{route("get-vendor-goods-cat1")}}',
            data: {value:val},
            success:function(data)
            {

                $("#cat1").html(data['cat1']);
                getCat2(1);
            }
            /*error:function(error)
            {
                alert('Error Occured Please Try Again');
            }*/

        });

        
    }
    function getCat2(val)
    {
        $.ajax({
            type: 'GET',
            url: '{{route("get-vendor-goods-cat2")}}',
            data: {value:val},
            success:function(data)
            {
                $("#cat2").html(data['cat2']);
            }
            /*error:function(error)
            {
                alert('Error Occured Please Try Again');
            }*/

        });

        
    }

    
    //--For Clearing Filter Data Elements-----//
    $(".reset").bind("click", function() {
    $("#cat1").val("");
    $("#cat2").val("");
    });   
</script>

<!-- ********************************************** -->
<!-- JavaScript at the bottom for fast page loading -->
<!-- ********************************************** -->

<!-- Grab Google CDN's jQuery, fall back to local if offline -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{{asset('fancybox/js/libs/jquery-1.7.1.min.js')}}"><\/script>')</script>
    
<!-- FancyBox -->
    <script src="{{asset('fancybox/js/fancybox/jquery.fancybox.js')}}"></script>
    <script src="{{asset('fancybox/js/fancybox/jquery.fancybox-buttons.js')}}"></script>
    <script src="{{asset('fancybox/js/fancybox/jquery.fancybox-thumbs.js')}}"></script>
    <script src="{{asset('fancybox/js/fancybox/jquery.easing-1.3.pack.js')}}"></script>
    <script src="{{asset('fancybox/js/fancybox/jquery.mousewheel-3.0.6.pack.js')}}"></script>
    
    <script type="text/javascript">
        var nc=$.noConflict();
        nc(document).ready(function() {
        nc(".fancybox").fancybox();
        });
    </script>
