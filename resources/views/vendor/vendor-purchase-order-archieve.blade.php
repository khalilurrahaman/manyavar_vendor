@include('layouts.vendor_header');
<!-- CSS -->
<link rel="stylesheet" href="{{asset('fancybox/css/fancybox/jquery.fancybox-buttons.css')}}">
<link rel="stylesheet" href="{{asset('fancybox/css/fancybox/jquery.fancybox-thumbs.css')}}">
<link rel="stylesheet" href="{{asset('fancybox/css/fancybox/jquery.fancybox.css')}}">
<link rel="stylesheet" href="{{asset('fancybox/demo/demo.css')}}">
<!-- DELETE -->
<script src="{{asset('dataTable/css/jquery.dataTables.min.css')}}"></script> 
<script src="{{asset('dataTable/css/buttons.dataTables.min.css')}}"></script> 
<script src="{{asset('dataTable/js/jquery.dataTables.min.js')}}"></script> 
<script src="{{asset('dataTable/js/dataTables.buttons.min.js')}}"></script> 
<script src="{{asset('dataTable/js/buttons.flash.min.js')}}"></script> 
<script src="{{asset('dataTable/js/jszip.min.js')}}"></script> 
<script src="{{asset('dataTable/js/pdfmake.min.js')}}"></script> 
<script src="{{asset('dataTable/js/vfs_fonts.js')}}"></script> 
<script src="{{asset('dataTable/js/buttons.html5.min.js')}}"></script> 
<script src="{{asset('dataTable/js/buttons.print.min.js ')}}"></script> 

<link rel="stylesheet" href="{{asset('autocomplete/jquery-ui.css')}}" />
<script src="{{asset('autocomplete/jquery-ui.js')}}"></script>
<!-- DELETE -->

        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <!--<div class="row">
                    <div class="col-lg-12 title-top">
                    <div class="row page-header">
                    	<div class="col-sm-6">
                        	<h1>Vendor <small>Purchase Order Listing</small></h1>
                        </div>
                        <div class="col-sm-6">
                        	<a href="javascript:void(0)" class="btn btn-success">Expand</a>
                        </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                	<div class="col-sm-6"><h3 class="panel-title">
                                    <i class="fa fa-users" aria-hidden="true"></i> Vendor Purchase Order Archive</h3></div>
                                    <div class="col-sm-6">
                                    
                                    <div class="shorting-area">
                            	
                                <!-- <div class="col-sm-3 pull-right">
                                        <a href="" class="btn btn-success">Back</a>
                                </div> -->
                            </div>
                                    
                                    
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                            <div class="tab-filter">
              <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab1default" data-toggle="tab" onclick="clearPrevData('po');applyFilter('default');">PO List by PO No & PO Date</a></li>
                    <li><a href="#tab3default" data-toggle="tab" onclick="clearPrevData('oth');applyFilter('default');">PO List by Design, Color,Width / Size & Cat6</a></li>
                  </ul>
                </div>
                <div class="panel-body">
                  <div class="tab-content">
                    <div class="tab-pane fade in active" id="tab1default">
                      <div class="row shorting-area">
                        <div class="filter-area">
                         <div class="col-sm-3">
                            <div class="form-group">
                              <div class="input-group date">
                                <input type="text" class="form-control date_picker" placeholder="PO date from(dd/mm/yyyy)"  id="due_from" name="due_from"/>
                                <!--<span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span>--> </div>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-group">
                              <div class="input-group date">
                                <input type="text" class="form-control date_picker" placeholder="PO date to(dd/mm/yyyy)"  id="due_to" name="due_to"/>
                               <!-- <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span>--> </div>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                          <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="Enter PO No"  id="po_number" name="po_number"/>
                          </div>
                 
                          <div class="col-sm-2"> <a href="javascript:void(0)" class="btn btn-success dateCheck" onclick="applyFilter();">Apply Filter</a>
                            <div class="clearfix"></div>
                          </div>
                          <div class="col-sm-1"> 
                          <a href="javascript:void(0)" class="btn btn-success dateCheck reset" onclick="">Clear</a>
                          <div class="clearfix"></div>  
                         </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane fade" id="tab3default">
                      <div class="row shorting-area">
                        <div class="filter-area">
                          <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="Enter Design"  id="category1" name="category1"/>
                          </div>
                          <div class="col-sm-2">
                           <input type="text" class="form-control" placeholder="Enter Color"  id="category2" name="category2"/> 
                          </div>
                          <div class="col-sm-2" style="display: none;">
                            <select class="select-drop" id="category3" name="category3">
                              <option value="">Cat 3 </option>
                            </select>
                          </div>
                          <div class="col-sm-2">
                            <input type="text" class="form-control" placeholder="Enter Width/Size"  id="category4" name="category4"/>
                          </div>
                          <div class="col-sm-2">
                             <input type="text" class="form-control" placeholder="Enter Cat6"  id="category6" name="category6"/> 
                          </div>
                          <div class="col-sm-2"> <a href="javascript:void(0)" class="btn btn-success" onclick="applyFilter();">Apply Filter</a>
                            <div class="clearfix"></div>
                          </div>
                           <div class="col-sm-1"> 
                          <a href="javascript:void(0)" class="btn btn-success dateCheck reset" onclick="">Clear</a>
                          <div class="clearfix"></div>  
                         </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                 <div class="empty_search_msg"></div>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
 <div class="clearfix"></div>
                  
                            <span id="filterResult">
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <!-- <th class="text-center">Sr. No</th> -->
                                                <th class="text-center">Image</th>
                                                <th class="text-center">PO. Date</th>
                                                <th class="text-center">Due. Date</th>
                                                <th class="text-center">Item Code</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Width/Size</th>
                                                 <th class="text-center">Cat 6</th>
                                                <th class="text-center">Price</th>
                                                <th class="text-center">Order Qty</th>
                                                <th class="text-center">Received Qty</th>
                                                <th class="text-center">Cancelled Qty</th>
                                                <th class="text-center">GIT Qty</th>
                                                <th class="text-center">Adhoc Qty</th>
                                                <th class="text-center">Pending Qty</th>
                                                
                                                <!-- <th class="last-width text-center">Action</th> -->
                                            </tr>
                                        </thead>
                                    <tbody>
                                        <tr><td style="font-size: 18px;" class="text-center" colspan="17" >Please enter your search keyword!</td></tr>
                                    </tbody>
                               </table>
                              </div>
                              
                              </span>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@include('layouts.footer');



    
<script src="{{asset('js/custom/purchase_order.js')}}"></script>
<script type="text/javascript">
   //---AutoSuggestion---//



var poList = <?php echo $poList;?>;
$(document).ready(
  function () {
    $( "#po_number" ).autocomplete({
      source:poList,
      autoFocus: true ,
      //change: function (event, ui) { alert(ui.item.value); }
    });
  }
);


var designlist = <?php echo $designlist;?>;
$(document).ready(
  function () {
    $( "#category1" ).autocomplete({
      source:designlist,
      autoFocus: true ,
      //change: function (event, ui) { alert(ui.item.value); }
    });
  }
);

var colorList = <?php echo $colorList;?>;
$(document).ready(
  function () {
    $( "#category2" ).autocomplete({
      source:colorList,
      autoFocus: true ,
      //change: function (event, ui) { alert(ui.item.value); }
    });
  }
);
</script>
<script>

var count=0;

$(document).ready(function () {   //For default data load          
		clearPrevData();
     applyFilter('default');    
    });


$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        //$('#load a').css('color', '#dfecf6');
        //$('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');

        var url = $(this).attr('href'); 
        var page=$(this).attr('href').split('page=')[1];
        
        var po_number=$("#po_number").val();
        var category1=$("#category1").val();
        var category2=$("#category2").val();
		    var category3=$("#category3").val();
        var category4=$("#category4").val();
        var due_from=$("#due_from").val();
        var due_to=$("#due_to").val();
     
        filter(page,po_number,category1,category2,category3,category4,due_from,due_to);
        //window.history.pushState("", "", url);
    });

    
});


    

    function clearPrevData(pos='')//For Clear Data of prev search
	{
        if(pos=='oth' && count==0)
        {
            count++;
        }

		$("#po_number").val('');
		$("#category1").val('');
		$("#category2").val('');
		$("#category3").val('');
		$("#category4").val('');
    $("#category6").val('');
		$("#due_from").val('');
		$("#due_to").val('');	

        var blank_msg='<tr><td style="font-size: 18px;" class="text-center" colspan="17" >Please Flfill Search Criteria! </td></tr>';

        $("#search_result_body").html(blank_msg); 		       
			//filter(); 
	}



function applyFilter(pos=''){
        var po_number=$("#po_number").val();
        var category1=$("#category1").val();
        var category2=$("#category2").val();
		    var category3=$("#category3").val();
		    var category4=$("#category4").val();
        var category6=$("#category6").val();
        var due_from=$("#due_from").val();
        var due_to=$("#due_to").val();
    if(pos=='default') {
        filter(1,po_number,category1,category2,category3,category4,category6,due_from,due_to);
        }
      else{ 
       $(".empty_search_msg").hide();
		if(po_number=='' && category1=='' && category2=='' && category3=='' && category4=='' && category6=='' && due_from=='' && due_to=='')
			$(".empty_search_msg").text('Please Select at least one criteria for filter!').css('color', 'red').show();
		else	 
        filter(1,po_number,category1,category2,category3,category4,category6,due_from,due_to);
    }
}


function filter(page=1,po_number='',category1='',category2='',category3='',category4='',category6='',due_from='',due_to=''){
    //if(po_number!='' || category1!=''  || category2!=''  || due_from!='' due_to!='')  
    $('.loading').show();
    $.ajax({
        type: 'GET', 
        url: '{{route("purchase-archieve-order-filter")}}',
        data: {po_number:po_number,category1:category1,category2:category2,category3:category3,category4:category4,category6:category6,due_from:due_from,due_to:due_to,page:page},
        success:function(data)
        {
            $('#filterResult').html(data);
            $('.loading').hide();
        },
        error: function (error) 
        {
            
            alert('Error Occured Please Try Again');
        }
    })
}
</script> 
<style type="text/css">
.tab-filter .filter-area {padding:0;}
.tab-filter .form-group {margin:0; height:30px !important;}
.tab-filter {margin:12px 0;}
.tab-filter .panel-default {border-radius:0; background:#111; padding:15px;}
.tab-filter .nav-tabs>li {margin-bottom:0px;}
.tab-filter .nav-tabs>li>a {margin-right:9px; background:#f1651d; letter-spacing:0.6px; padding:10px 19px;}
.tab-filter .panel-body {background:#fff; padding:26px 15px;}
.tab-filter .panel.with-nav-tabs .panel-heading {padding:0; border-radius:0; background:transparent; color:#fff; border:0;}
.tab-filter .panel.with-nav-tabs .nav-tabs {border-bottom:none;}
.tab-filter .with-nav-tabs.panel-default .nav-tabs > li > a, .tab-filter .with-nav-tabs.panel-default .nav-tabs > li > a:hover,
.tab-filter .with-nav-tabs.panel-default .nav-tabs > li > a:focus {color: #fff; font-family: 'Archivo Narrow', sans-serif; font-size:16px; border-radius:0;}
.tab-filter .with-nav-tabs.panel-default .nav-tabs > .open > a, .tab-filter .with-nav-tabs.panel-default .nav-tabs > .open > a:hover,
.tab-filter .with-nav-tabs.panel-default .nav-tabs > .open > a:focus, .tab-filter .with-nav-tabs.panel-default .nav-tabs > li > a:hover,
.tab-filter .with-nav-tabs.panel-default .nav-tabs > li > a:focus {color:#777; background-color:#ddd; border-color:transparent;}
.tab-filter .with-nav-tabs.panel-default .nav-tabs > li.active > a, .tab-filter .with-nav-tabs.panel-default .nav-tabs > li.active > a:hover,
.tab-filter .with-nav-tabs.panel-default .nav-tabs > li.active > a:focus {color:#555; background-color:#fff; border-color:#ddd; 
border-bottom-color:transparent;}
</style>
<script type="text/javascript">
    //--For Clearing Filter Data Elements-----//
    $(".reset").bind("click", function() {
    $("input[type=text], textarea").val("");
    $("#po_number").val("");
    $("#category1").val("");
    $("#category2").val("");
    $("#category4").val("");
     $("#category6").val("");

    });   
</script>

<!-- ********************************************** -->
<!-- JavaScript at the bottom for fast page loading -->
<!-- ********************************************** -->

<!-- Grab Google CDN's jQuery, fall back to local if offline -->
    <script src="{{asset('js/jquery-jquery.min.js')}}"></script>
    <script>window.jQuery || document.write('<script src="{{asset('fancybox/js/libs/jquery-1.7.1.min.js')}}"><\/script>')</script>
    
<!-- FancyBox -->
    <script src="{{asset('fancybox/js/fancybox/jquery.fancybox.js')}}"></script>
    <script src="{{asset('fancybox/js/fancybox/jquery.fancybox-buttons.js')}}"></script>
    <script src="{{asset('fancybox/js/fancybox/jquery.fancybox-thumbs.js')}}"></script>
    <script src="{{asset('fancybox/js/fancybox/jquery.easing-1.3.pack.js')}}"></script>
    <script src="{{asset('fancybox/js/fancybox/jquery.mousewheel-3.0.6.pack.js')}}"></script>
    
    <script type="text/javascript">
        var nc=$.noConflict();
        nc(document).ready(function() {
        nc(".fancybox").fancybox();
        });
    </script>
