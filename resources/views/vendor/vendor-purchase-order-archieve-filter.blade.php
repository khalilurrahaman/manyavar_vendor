<div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                               <!--  <th class="text-center">Sr. No</th> -->
                                                <th class="text-center">Image</th>
                                                <th class="text-center">PO. No</th>
                                                <th class="text-center">PO. Date</th>
                                                <th class="text-center">Due. Date</th>
                                                <th class="text-center">Item Code</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Width/Size</th>
                                                 <th class="text-center">Cat 6</th>
                                                <th class="text-center">Price</th>
                                                <th class="text-center">Order Qty</th>
                                                <th class="text-center">Received Qty</th>
                                                <th class="text-center">Cancelled Qty</th>
                                                <th class="text-center">GIT Qty</th>
                                                <th class="text-center">Adhoc Qty</th>
                                                <th class="text-center">Pending Qty</th>
                                                <!-- <th class="last-width text-center">Action</th> -->
                                            </tr>
                                        </thead>
                                    <tbody  id="search_result_body">
                                    <?php $incmain=0;$inc=0; ?>
                                    @foreach($purchaseOrderList as $eachOrder)
                                    <?php $inc++; $incmain++; 
                                    //$pending = ($eachOrder->ORDQTY - ($eachOrder->RCQTY+$eachOrder->CNLQTY + $eachOrder->git_qty));
                                    //$pending = $eachOrder->ORDQTY - ($eachOrder->git_rcv+$eachOrder->p_git_qty);
                                   if(empty($eachOrder->p_git_qty))
                   $pending = ($eachOrder->ORDQTY - ($eachOrder->RCQTY+$eachOrder->CNLQTY));
                else if(!empty($eachOrder->p_git_qty) && empty($eachOrder->git_rcv))
                   $pending = $eachOrder->ORDQTY - ($eachOrder->RCQTY+$eachOrder->p_git_qty+$eachOrder->CNLQTY);
                 else if(!empty($eachOrder->p_git_qty) && !empty($eachOrder->git_rcv))
                   $pending = $eachOrder->ORDQTY - ($eachOrder->git_rcv+$eachOrder->p_git_qty+$eachOrder->CNLQTY);

                                     
                                    $adhoc_qty=0;
                                    if($pending<=0)
                                    {
                                    //echo $pndg_qty=str_replace("-","Adhoc:",$pndg_qty);
                                    $adhoc_qty=str_replace("-"," ",$pending);
                                    $pending=0;
                                    
                                    }
                                    ?>
                                    <tr>
                                       <!--  <td class="text-center">{{$inc}}</td> -->
                                         <td class="text-center">
                            <div class="gal"> 
                             <?php 
                               $pGalList=DB::table('product_gallery')->select('*')->where('product_id','=',$eachOrder->product_id)->get();
                               if($eachOrder->image!='' && file_exists('upload/product/'.$eachOrder->image))
                               $main_image=URL::to('/upload/product').'/'.$eachOrder->image;
                               if(empty($main_image) && !empty($pGalList) && count($pGalList)>0)
                               {
                                  foreach ($pGalList as $pGal) 
                                  { 
                                    if($pGal->product_image!='' && file_exists('upload/product/'.$pGal->product_image))
                                    {
                                      $main_image=URL::to('/upload/product').'/'.$pGal->product_image;
                                    }
                                  }
                               }
                               if(empty($main_image))
                                $main_image=URL::to('/img').'/pre-order-img.jpg';
                              ?>
                               <a class="fancybox" rel="group{{$incmain}}" href="{{$main_image}}" >
                                <img src="{{$main_image}}" width="56">
                               </a>
                               <?php 
                               if(!empty($pGalList) && count($pGalList)>0)
                               {
                                foreach ($pGalList as $pGal2) 
                                {
                                    if($pGal2->product_image!='' && file_exists('upload/product/'.$pGal2->product_image))
                                    {
                                      $main_image=URL::to('/upload/product').'/'.$pGal2->product_image;

                                ?>                                               
                               <a class="fancybox" rel="group{{$incmain}}" href="{{$main_image}}" style="display:none;"><img src="{{$main_image}}" alt=""></a>
                                <?php 
                                }
                                }
                                }
                               ?> 
                            </div>
                        </td>
                                        <td class="text-center po-no<?php echo $inc;?>">{{$eachOrder->PONO}}</td>
                                        <td class="text-center po-date<?php echo $inc;?>">{{date('d/m/Y',strtotime($eachOrder->ORDDT))}}</td>
                                        <td class="text-center due-date<?php echo $inc;?>">{{date('d/m/Y',strtotime($eachOrder->DUEDATE))}}</td>
                                        @if(isset($eachOrder->ICODE))
                                        <td class="text-center ICODE<?php echo $inc;?>">{{$eachOrder->ICODE}}</td>
                                        @else
                                        <td class="text-center ICODE<?php echo $inc;?>"></td>
                                        @endif
                                        <td class="text-center cat-1<?php echo $inc;?>">{{$eachOrder->cat1}}</td>
                                        <td class="text-center cat-2<?php echo $inc;?>">{{$eachOrder->cat2}}</td>
                                        <td class="text-center cat-3<?php echo $inc;?>">{{$eachOrder->cat3}}</td>
                                        <td class="text-center cat-4<?php echo $inc;?>">{{$eachOrder->cat4}}</td>
                                        <td class="text-center cat-4<?php echo $inc;?>">{{$eachOrder->CNAME6}}</td>
                                        <td class="text-center price<?php echo $inc;?>">{{$eachOrder->RATE}}</td>
                                        <td class="text-center order-qty<?php echo $inc;?>">{{$eachOrder->ORDQTY}}</td>
                                        <td class="text-center received-qty<?php echo $inc;?>">{{$eachOrder->RCQTY}}</td>
                                        <td class="text-center cancelled-qty<?php echo $inc;?>">{{$eachOrder->CNLQTY}}</td>
                                        <td class="text-center git-qty<?php echo $inc;?>">{{$eachOrder->p_git_qty}}</td>
                                        <td class="text-center pending-qty<?php echo $inc;?>">{{$adhoc_qty}}</td>
                                        <td class="text-center pending-qty<?php echo $inc;?>">{{$pending}}</td>
                                        
                                    </tr>
                                    @endforeach
                                </tbody>
                               </table>
                              </div>
                              


<script>
         $(document).ready(function () {
            
            $('#example').dataTable({
                'order': [[ 2, "asc" ]],//Default Column Attribute
                'pageLength': 30,//Per Page Item
                 "searching": false,
                'dom': 'Bfrtip',//Header Info               
                'buttons': [ {extend: 'excel',exportOptions: {columns: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]},footer: false}],
                'aoColumnDefs': [          
                                   { aTargets: [ 0 ], bSortable: false },
                                   { aTargets: [ 1 ], bSortable: false },
                                   { aTargets: [ 2 ], bSortable: true },
                                   { aTargets: [ 3 ], bSortable: true },
                                   { aTargets: [ 4 ], bSortable: false },
                                   { aTargets: [ 5], bSortable: true },
                                   { aTargets: [ 6 ], bSortable: false },
                                   { aTargets: [ 7 ], bSortable: false },
                                   { aTargets: [ 8 ], bSortable: false },
                                   { aTargets: [ 9 ], bSortable: false },
                                   { aTargets: [ 10 ], bSortable: false },
                                   { aTargets: [ 11 ], bSortable: false },
                                   { aTargets: [ 12 ], bSortable: false },
                                   { aTargets: [ 13 ], bSortable: false },
                                   { aTargets: [ 14 ], bSortable: false },
                                   { aTargets: [ 15 ], bSortable: false },
                                   { aTargets: [ 16 ], bSortable: true }

                                ],
                "oLanguage": {
                                "oPaginate": {
                                                "sNext": '<i class="fa fa-chevron-right" ></i>',
                                                "sPrevious": '<i class="fa fa-chevron-left" ></i>'
                                            }
                            }
            });
        });
         
</script>                              