@include('layouts.vendor_header');
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <!--<div class="row">
                    <div class="col-lg-12 title-top">
                    <div class="row page-header">
                    	<div class="col-sm-6">
                        	<h1>Vendor <small>vendor Request for Cutting</small></h1>
                        </div>
                        <div class="col-sm-6">
                        	<a href="vendor-all-request.php" style="width:auto;" class="btn btn-warning">View All Request</a>
                        </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                	<div class="col-sm-6">
                                    	<h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i> Vendor Request for Sample Approval</h3>
                                    </div>
                                    <div class="col-sm-6 changes-area">
                                    
                                    <div class="shorting-area">
                            		
                                <div class="col-sm-3 pull-right">
                                        <a href="javascript:void(0)" id="show-btn" class="btn btn-success">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                </div>
                            </div>
                                    
                                    
                                    </div>
                                </div>
                                
                                
                                
                                
                                
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                            <div class="row shorting-area">
                            <div class="col-sm-12">
                            <div id="mySidenavR" class="sidenavR" style="display:none;">
<div class="filter-right">
<div class="filter-area">
<!--<h5>Vendor List Filter</h5>-->
    <a href="javascript:void(0)" class="closebtn" id="hide-filter">×</a>
        <div class="col-sm-2">
           <select class="select-drop" id="po_number" name="po_number">
            	<option value="">PO Number</option>
               
            </select>
        </div>
        <div class="col-sm-2">
            <select class="select-drop" id="category1" name="category1">
            	<option value="">Design</option>
               
            </select>
        </div>
        <div class="col-sm-2">
           <select class="select-drop" id="category2" name="category2">
            	<option value="">Colour</option>
                
            </select>
        </div>
        <div class="col-sm-2">
             <select class="select-drop" id="category4" name="category4">
            	<option value="">Width/Size</option>
                
            </select>
        </div>
       
        <div class="col-sm-2">
        	<a href="javascript:void(0)" class="btn btn-success" onclick="applyFilter();">Apply Filter</a>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-1"> 
            <a href="javascript:void(0)" class="btn btn-success dateCheck reset" onclick="">Clear</a>
             <div class="clearfix"></div>  
        </div>

<div class="clearfix"></div>        
	</div>
    <div class="clearfix"></div>
    </div>    
</div>
                            
                            </div>
                            
                            
                            </div>
                            <span id="filterResult">
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Sr. No</th>
                                                <th class="text-center">PO. No</th>
                                                <th class="text-center">Item Code</th>
                                                <th class="text-center">PO. Date</th>
                                                <th class="text-center">Due. Date</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Width/Size</th>
                                                <th class="text-center">Order Qty</th>
                                                <th class="text-center">Received Qty</th>
                                                <th class="text-center">GIT Qty</th>
                                                <th class="text-center">Adhoc Qty</th>
                                                <th class="text-center">Pending Qty</th>
                                                <th class="last-width1 text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    
                                  
                                </tbody>
                               </table>
                              </div>
                             
                              </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@include('layouts.footer');  
<!--add vendorSamplerequest Modal Start-->
<div id="vendorSamplerequest" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close resetmodal" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Sample Approval</h4>
      </div>
      <div class="modal-body vendorSamplerequest-modal-body">
        <div class="row shorting-area pricechange">
            <input type="hidden" name="type" id="sample_type" value="sample" />
            <div class="col-sm-3"><h5>Design : <strong class="vendorSamplerequest-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Colour : <strong class="vendorSamplerequest-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Width/Size : <strong class="vendorSamplerequest-cat4"><!--45"--></strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorSamplerequest-po-no"><!--234--></strong></h5></div>
             <div class="col-sm-3"><h5>I Code : <strong class="vendorSamplerequest-icode"></strong></h5></div>
            <div class="col-sm-3"><h5>Date : <strong class="vendorSamplerequest-date"><!--12/10/2016--></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorSamplerequest-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorSamplerequest-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorSamplerequest-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorSamplerequest-pending-qty"><!--1000--></strong></h5></div>
            <div class="clearfix"></div>
            <hr>
            <div class="clearfix"></div>
           
            <div class="col-sm-6 docet" id="docet">
            <h5>Docket No :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestaccept_new_docket" name="vendorRequestaccept_new_docket"/>
            <span id="vendorRequestaccept_docket-remarks_message"></span>
            </div>
            
            
            <div class="col-sm-6 courier" id="courier">
            <h5>Courier Name :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestaccept_new_courier" name="vendorRequestaccept_new_courier"/>
            <span id="vendorRequestaccept_courier-remarks_message"></span>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <h5>Remarks :</h5>
            <textarea class="form-control" id="vendorSamplerequest-remarks" name="vendorSamplerequest-remarks" placeholder="Remarks"></textarea>
            <span id="vendorSample_remarks_message"></span>
            </div>
            <div class="clearfix vendorSamplerequest-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorSamplereq" id="preventDouble">Submit</a>
            </div>
            <div class="col-sm-6">
            <a href="#" data-dismiss="modal" class="pup-btn resetmodal">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--add vendorChangecutting  Modal End-->
<script>
// $('#vendorRequestaccept_new_prndate').on('changeDate', function(ev){
//     $(this).datepicker('hide');
// });

var ptoken='{{csrf_token()}}';
var cutting_url='{{route("purchase-order-request")}}';
</script>


<script>

    $(document).ready(function () {
        getPoList();
        applyFilter();
    });



    function getPoList()
    {
        $.ajax({
            type: 'GET', 
            url: '{{route("vendor-po-list")}}',
            success:function(data)
            {
                $('#po_number').html(data['po']);
                getDesignList();
            },
            error: function (error) 
            {                
                alert('Error Occured Please Try Again');
            }
        });
    }

    function getDesignList()
    {
        $.ajax({
            type: 'GET', 
            url: '{{route("vendor-design-list")}}',
            success:function(data)
            {
                $('#category1').html(data['design']);
                getColorList();
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
    }

    function getColorList()
    {
        $.ajax({
            type: 'GET', 
            url: '{{route("vendor-color-list")}}',
            success:function(data)
            {
                $('#category2').html(data['color']);
                 getCategory3List();
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
    }

    function getCategory3List()//For fetch Category3 list
    {
        $.ajax({
            type: 'GET', 
            url: '{{route("vendor-category3-list")}}',
            success:function(data)
            {
                $('#category3').html(data['category3']);
                 getWidthSizeList();
            },
            error: function (error) 
            {
                
               // alert('Error Occured Please Try Again');
            }
        });
    }
    
    function getWidthSizeList()//For fetch WidthSize list
    {
        $.ajax({
            type: 'GET', 
            url: '{{route("vendor-width-size-list")}}',
            success:function(data)
            {
                $('#category4').html(data['width_size']);
            },
            error: function (error) 
            {
                
                //alert('Error Occured Please Try Again');
            }
        });
    }


$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        //$('#load a').css('color', '#dfecf6');
        //$('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');

        var url = $(this).attr('href'); 
		var page=$(this).attr('href').split('page=')[1];
		
		var po_number=$("#po_number").val();
		var category1=$("#category1").val();
		var category2=$("#category2").val();
		var category4=$("#category4").val();
	 
        filter(page,po_number,category1,category2,category4);
        //window.history.pushState("", "", url);
    });

    
});

function applyFilter(){
		var po_number=$("#po_number").val();
		var category1=$("#category1").val();
		var category2=$("#category2").val();
		var category4=$("#category4").val();
	 
        filter(1,po_number,category1,category2,category4);
}

$( "#hide-filter" ).click(function() {
	
	$("#po_number").val('');
	$("#category1").val('');
	$("#category2").val('');
	$("#category4").val('');	
	filter(); 
});

function filter(page=1,po_number='',category1='',category2='',category4=''){	
	$('.loading').show();
	$.ajax({
		type: 'GET', 
		url: '{{route("filter-sample-request")}}',
		data: {po_number:po_number,category1:category1,category2:category2,category4:category4,page:page},
		success:function(data)
		{
			$('#filterResult').html(data);
			$('.loading').hide();
		},
		error: function (error) 
		{
			
			alert('Error Occured Please Try Again');
		}
	})
}

$( ".unablepopup" ).click(function() {
    
   alert('Please wait for Admin Response against your previous Request!');
});

//--For Clearing Filter Data Elements-----//
    $(".reset").bind("click", function() {
    $("#po_number").val("");
    $("#category1").val("");
    $("#category2").val("");
    $("#category4").val("");

    });   

    $("#preventDouble").on("submit",function(){
    $(this).unbind("submit");
    $(this).on("submit",function(){return false;});
});

//--For Clearing input Data after modal dismiss-----//
    $(".resetmodal").bind("click", function() {
    $("input[type=text], textarea").val("");
    }); 
</script>   