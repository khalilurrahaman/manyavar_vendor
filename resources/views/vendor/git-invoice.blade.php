<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Invoice :: Print</title>
<style>
.invoice-box {
	max-width: 800px;
	margin: auto;
	padding: 30px;
	border: 1px solid #eee;
	box-shadow: 0 0 10px rgba(0, 0, 0, .15);
	font-size: 16px;
	line-height: 24px;
	font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
	color: #555;
}
.invoice-box table {
	width: 100%;
	line-height: inherit;
	text-align: left;
	font-size: 15px;
}
.invoice-box table td {
	padding: 9px 12px;
	vertical-align: middle;
}
.invoice-box table tr td {
	text-align: right;
}
.invoice-box table tr td:first-child {
	text-align: left;
}
.invoice-box table tr.top table td {
	padding-bottom: 20px;
}
.invoice-box table tr.top table td.title {
	font-size: 45px;
	line-height: 45px;
	color: #333;
}
.invoice-box table tr.information table td {
	padding-bottom: 40px;
}
.invoice-box table tr.heading td {
	font-size: 15px;
	background: #eee;
	border-bottom: 1px solid #ddd;
	font-weight: bold;
}
.invoice-box table tr.details td {
	padding-bottom: 20px;
}
.invoice-box table tr.item td {
	border-bottom: 1px solid #eee;
	font-size: 14px;
}
.invoice-box table tr.item.last td {
	border-bottom: none;
}
.invoice-box table tr.total td:nth-child(2) {
	border-top: 2px solid #eee;
	font-weight: bold;
}
hr {
	width: 100%;
	height: 1px;
	background: #CCC;
	margin-top: 12px;
	border: 0;
}
 @media only screen and (max-width:600px) {
.invoice-box table tr.top table td {
	width: 100%;
	display: block;
	text-align: center;
}
.invoice-box table tr.information table td {
	width: 100%;
	display: block;
	text-align: center;
}
}
</style>
</head>

<body>
<div class="invoice-box">
  <table>
    <tr>
      <td class="title"><img src="{{asset('img/mmlsmall.png')}}" style="width:100%; max-width:300px;"><br/>
        Lorem Ipsum simple dummy<br/>
        text Lorem Ipsum<br/>
        simple dummy text </td>
      <td><strong>Goods Return Note</strong></td>
    </tr>
  </table>
  <hr/>
  <table>
    <tr>
      <td class="title"> Lorem Ipsum simple dummy<br/>
        text Lorem Ipsum<br/>
        simple dummy text </td>
      <td>Created: January 1, 2015<br>
        Due: December 6, 2016</td>
    </tr>
  </table>
  <table cellpadding="0" cellspacing="0">
    <tr class="heading">
      <td>Item </td>
      <td>Name</td>
      <td>Price</td>
      <td>Qty.</td>
      <td>Amount</td>
      <td>Total</td>
    </tr>
    <tr class="item">
      <td><strong>FG245 - Lorem Ipaum Simple dummy Text...</strong></td>
      <td>Lorem Ipsum</td>
      <td>230.00</td>
      <td>46</td>
      <td>5698.00</td>
      <td>5689978.00</td>
    </tr>
    <tr class="item">
      <td><strong>F00245 - Lorem Ipaum Simple dummy Text...</strong></td>
      <td>Lorem Ipsum</td>
      <td>230.00</td>
      <td>46</td>
      <td>5698.00</td>
      <td>5689978.00</td>
    </tr>
    <tr class="item">
      <td>&nbsp;</td>
      <td><strong>Total :</strong></td>
      <td>&nbsp;</td>
      <td><strong>46</strong></td>
      <td><strong>123456</strong></td>
      <td><strong>385.00</strong></td>
    </tr>
    <tr class="item">
      <td>&nbsp;</td>
      <td><strong>Discount :</strong></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><strong>(10%)</strong></td>
      <td><strong>- 385.00</strong></td>
    </tr>
    <tr class="total">
      <td style="text-align:right;"><strong>Total Amount :</strong></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><strong>60000.00</strong></td>
    </tr>
  </table>
  <br/>
  <p><strong>Remarks :</strong> Lorem Ipsum simple dummy text.</p>
  <br/>
  <br/>
  <table>
    <tr>
      <td style="padding-left:0;"><strong>Lorem Ipsum : </strong>simple dummy text Lorem Ipsum<br/>
        <strong>Lorem Ipsum : </strong> simple dummy text Lorem Ipsum </td>
      <td style="text-align:center;"><hr/>
        Lorem Ipsum</td>
      <td style="text-align:center;"><hr/>
        Lorem Ipsum</td>
    </tr>
  </table>
</div>
</body>
</html>