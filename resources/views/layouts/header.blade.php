<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title>Manyavar Vendor Panel</title>

    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <link rel="shortcut icon" href="{{ asset('favicon.png')}}" type="image/x-icon" />

    <link href="{{asset('css/datepicker.css')}}" rel="stylesheet">
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/admin.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <script src="{{asset('js/jquery-2.1.1.js')}}"></script>
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <!-- Redirect to another page (for no-js support) (place it in your <head>) -->
    <noscript><meta http-equiv="refresh" content="0;url=http://www.enable-javascript.com/"></noscript>
    <!-- Show a message -->
    <noscript>You don't have javascript enabled! Please enable It!</noscript>
    
</head>
<?php 
use App\Http\Controllers\Controller;
//New Item
if(Controller::userAccessibilityCheck('all-rfq-request'))
	$all_rfq_request_exist=1;
if(Controller::userAccessibilityCheck('all-rfq-list'))
	$all_rfq_list_exist=1;
if(Controller::userAccessibilityCheck('add-rfq'))
	$add_rfq_exist=1;
//Purchase Order
if(Controller::userAccessibilityCheck('purchase-order-list'))
	$purchase_order_list_exist=1;
if(Controller::userAccessibilityCheck('archieve-purchase-order-list'))
	$archieve_purchase_order_list_exist=1;
if(Controller::userAccessibilityCheck('reject-purchase-order-list'))
	$reject_purchase_order_list_exist=1;
//GIT
if(Controller::userAccessibilityCheck('all-git-list'))
	$all_git_list_exist=1;
if(Controller::userAccessibilityCheck('all-grt-list'))
	$all_grt_list_exist=1;
if(Controller::userAccessibilityCheck('all-archive-git-list'))
	$all_archive_git_list_exist=1;
//Documents
if(Controller::userAccessibilityCheck('document-list'))
	$document_list_exist=1;
if(Controller::userAccessibilityCheck('archieve-document-list'))
	$archieve_document_list_exist=1;
//Action
if(Controller::userAccessibilityCheck('allwidthratechange-request'))
	$allwidthratechange_request_exist=1;
if(Controller::userAccessibilityCheck('alldateext-request'))
	$alldateext_request_exist=1;
if(Controller::userAccessibilityCheck('allcuttingsampleothers-request'))
	$allcuttingsampleothers_request_exist=1;
//Goods Return
if(Controller::userAccessibilityCheck('goods-return'))
	$goods_return_exist=1;
//Role Management
if(Controller::userAccessibilityCheck('role-list'))
	$role_list_exist=1;
//User Management
if(Controller::userAccessibilityCheck('user-list'))
	$user_list_exist=1;
//Vendor
if(Controller::userAccessibilityCheck('vendor-list'))
	$vendor_list_exist=1;
//Devision
if(Controller::userAccessibilityCheck('division-list'))
	$division_list_exist=1;
//Section
if(Controller::userAccessibilityCheck('section-list'))
	$section_list_exist=1;
//Department
if(Controller::userAccessibilityCheck('department-list'))
	$department_list_exist=1;
//Price
if(Controller::userAccessibilityCheck('price-list'))
	$price_list_exist=1;
//Document Type
if(Controller::userAccessibilityCheck('document-type-list'))
	$document_type_list_exist=1;
//Item Master
if(Controller::userAccessibilityCheck('item-master-list'))
	$item_master_list_exist=1;

?>
<body>
    <div id="wrapper">
   <header>
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="inner-top">
        <div class="row">
        	<div class="col-sm-2 logo-area">
            	<div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{route('dashboard')}}">Manyavar Vendor Panel</a>
                </div>
            </div>
            <div class="col-sm-6 paddLR0">
            	<div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="{{route('dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <?php if(!empty($all_rfq_request_exist) || !empty($all_rfq_list_exist) ||!empty($add_rfq_exist) ){?>
                    <li class="dropdown">
                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-fw fa-bar-chart-o"></i> New Item <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                        	<?php if(!empty($all_rfq_request_exist)){?>
                            <li> 
                                <a href="{{route('rfq-request')}}"> View all RFQ </a>
                            </li>
                            <?php }?>
                            <?php if(!empty($all_rfq_list_exist)){?>
                            <li>
                                <a href="{{route('rfq-list')}}"> View Archives </a>
                            </li>
                            <?php }?>
                            <?php if(!empty($add_rfq_exist)){?>
                            <li>
                                <a href="{{route('add-rfq-form')}}"> Create New </a>
                            </li>
                            <?php }?>
                        </ul>

                    </li>
					<?php }?>
                    <?php if(!empty($purchase_order_list_exist) || !empty($archieve_purchase_order_list_exist) ||!empty($reject_purchase_order_list_exist) ){?>
                    <li class="dropdown">
                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-fw fa-table"></i> Purchase Order <b class="caret"></b></a>
                        <ul class="dropdown-menu">
    						<?php if(!empty($purchase_order_list_exist)){?>
                            <li> 
                                <a href="{{route('purchase_order_list')}}"> View Open </a>
                            </li>
                            <?php }?>
                            <?php if(!empty($archieve_purchase_order_list_exist)){?>
                            <li>
                                <a href="{{route('admin-archieve-purchase-list')}}"> View Archives </a>
                            </li>
                            <?php }?>
                            <?php if(!empty($reject_purchase_order_list_exist)){?>
                            <li>
                                <a href="{{route('admin-reject-purchase-list')}}"> View Rejected </a>
                            </li>
                            <?php }?>
                           
                        </ul>

                    </li>
  					<?php }?>
                    <?php if(!empty($all_git_list_exist) || !empty($all_grt_list_exist) ||!empty($all_archive_git_list_exist) ){?>
                    <li class="dropdown">
                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-paper-plane"></i> GIT <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <?php if(!empty($all_git_list_exist)){?>
                            <li> 
                                <a href="{{route('all_git_list')}}"> View All GIT</a>
                            </li>
                            <?php }?>                            
                            <?php if(!empty($all_grt_list_exist)){?>
                            <li>
                               <!--  <a href="{{route('all_grt_list')}}"> View All GRC</a> -->
                                <a href="{{route('all_grc_list')}}"> View All GRC</a>
                            </li>
                            <?php }?>
                            <?php if(!empty($all_archive_git_list_exist)){?>
                            <li> 
                                <a href="{{route('all_archive_git_list')}}"> View GIT Archive</a>
                            </li>
                            <?php }?>                            
                        </ul>
                    </li>
                    <?php }?>
                <!-- <li>
                    <a href="" ><i class="fa fa-pencil-square-o"></i> Action </a>
                   
                </li> -->
                <?php if(!empty($allwidthratechange_request_exist) || !empty($alldateext_request_exist) ||!empty($allcuttingsampleothers_request_exist) ){?>
                <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-pencil-square-o"></i> Action <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                    	<?php if(!empty($allwidthratechange_request_exist)){?>                        
                        <li>
                            <a href="{{route('ad-allwidthratechange-request')}}">View Width and Rate Change Request</a>
                        </li>
                        <?php }?>
                        <?php if(!empty($alldateext_request_exist)){?>    
                        <li>
                            <a href="{{route('ad-alldateext-request')}}">View Due Date Ext Request</a>
                        </li>
                        <?php }?>
                        <?php if(!empty($allcuttingsampleothers_request_exist)){?>    
                        <li>
                            <a href="{{route('ad-allcuttingsampleothers-request')}}">View  Others Request</a>
                        </li>
                        <?php }?>
                    </ul>
                </li>
                <?php }?>
                <?php if(!empty($goods_return_exist)){?>   
                <li>
                    <a href="{{route('goods-returns-list')}}"><i class="fa fa-exchange"></i> Goods Return</a>
                </li>
                <?php }?>                
                <!--<li>
                    <a href="{{route('ad-reportss')}}"><i class="fa fa-comment"></i> Reports</a>
                </li>-->
              <?php if(!empty($document_list_exist) || !empty($archieve_document_list_exist)){?>
              <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-list"></i> Documents <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <?php if(!empty($document_list_exist)){?>                       
                        <li>
                            <a href="{{route('document-list')}}"><i class="fa fa-list"></i> Open </a>
                        </li>
                        <?php }?>
                        <?php if(!empty($archieve_document_list_exist)){?>   
                        <li>
                            <a href="{{route('archieve-document-list')}}"><i class="fa fa-list"></i> Archive</a>
                        </li>
                        <?php }?> 
                    </ul>
                </li> 
                <?php }?>
                <?php if(!empty($role_list_exist) || !empty($user_list_exist) || !empty($vendor_list_exist) || !empty($division_list_exist)|| !empty($section_list_exist) || !empty($department_list_exist) || !empty($price_list_exist) || !empty($item_master_list_exist) || !empty($document_type_list_exist)){?>
                <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-pencil-square-o"></i> Settings <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                    	<?php if(!empty($role_list_exist)){?>
                        <li>
                            <a href="{{route('role-listing')}}"><i class="fa fa-user"></i> Role Management</a>
                        </li>
                        <?php }?>
                        <?php if(!empty($user_list_exist)){?> 
                        <li>
                            <a href="{{route('user-listing')}}"><i class="fa fa-user"></i> User Management</a>
                        </li>
                        <?php }?>
                        <?php if(!empty($vendor_list_exist)){?> 
                        <li>
                            <a href="{{route('ven-listing')}}"> <i class="fa fa-list"></i>  Vendor</a>
                        </li>
                        <?php }?>
                        
                        <?php if(!empty($division_list_exist)){?> 
                        <!-- <li>
                            <a href="{{route('division-listing')}}"> <i class="fa fa-list"></i>  Division</a>
                        </li> -->
                        <?php }?>                         
                        <?php if(!empty($section_list_exist)){?>                        
                        <!-- <li>
                            <a href="{{route('section-listing')}}"> <i class="fa fa-list"></i>  Section</a>
                        </li> -->
                        <?php }?>
                        <?php if(!empty($department_list_exist)){?>      
                        <!-- <li>
                            <a href="{{route('departmnt-lising')}}"> <i class="fa fa-list"></i>  Department</a>
                        </li> -->
                        <?php }?>
                        <?php if(!empty($price_list_exist)){?> 
                        <li>
                            <a href="{{route('price-list')}}"><i class="fa fa-list"></i> Price List</a>
                        </li>
                        <?php }?>
                        <?php if(!empty($item_master_list_exist)){?> 
                        <li>
                            <!--<a href="{{route('item-master-list')}}"><i class="fa fa-list"></i> Item Master</a>-->
                            <!--<a href="{{route('item-master-sample')}}"><i class="fa fa-list"></i> Item Master</a>-->
                            <a href="{{route('item-master-list')}}"><i class="fa fa-list"></i> Item Master</a>
                        </li> 
                        <?php }?>   
                      	<?php if(!empty($document_type_list_exist)){?> 
                        <li>
                            <a href="{{route('document-type-list')}}"><i class="fa fa-list"></i> Documents Type</a>
                        </li>                                      
                        <?php }?>
                        
                    </ul>
                </li>
                <?php }?>
                
                </ul>
            </div>
            </div>
            <div class="col-sm-3 pull-right">
            	<ul class="nav navbar-right top-nav">
                <!--<li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="javascript:void(0)">Alert Name <span class="label label-default">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Alert Name <span class="label label-primary">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Alert Name <span class="label label-success">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Alert Name <span class="label label-info">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Alert Name <span class="label label-warning">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Alert Name <span class="label label-danger">Alert Badge</span></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:void(0)">View All</a>
                        </li>
                    </ul>
                </li>-->
                <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{ Auth::user()->name }} <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{route('admin-profileedit-req')}}"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{route('admin-change-pass')}}"><i class="glyphicon glyphicon-lock"></i> Change Password</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ url('/logout') }}"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
             <input type="button" id="trigger_fullscreen" value="click to toggle fullscreen" onclick="toggleFullScreen(document.body)">
            </div>
        </div>
        <div class="clearfix"></div>
        </div>
        </nav>
</header>
<script type="text/javascript">

function toggleFullScreen() {
 if ((document.fullScreenElement && document.fullScreenElement !== null) ||    
   (!document.mozFullScreen && !document.webkitIsFullScreen)) {
    if (document.documentElement.requestFullScreen) {  
      document.documentElement.requestFullScreen();  
    } else if (document.documentElement.mozRequestFullScreen) {  
      document.documentElement.mozRequestFullScreen();  
    } else if (document.documentElement.webkitRequestFullScreen) {  
      document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
    }  
  } else {  
    if (document.cancelFullScreen) {  
      document.cancelFullScreen();  
    } else if (document.mozCancelFullScreen) {  
      document.mozCancelFullScreen();  
    } else if (document.webkitCancelFullScreen) {  
      document.webkitCancelFullScreen();  
    }  
  }  
}


</script>