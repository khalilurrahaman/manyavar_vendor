<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title>Manyavar Vendor Panel</title>
    <link href="{{url('css/datepicker.css')}}" rel="stylesheet">
    <link href="{{url('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('css/admin.css')}}" rel="stylesheet">
    <link href="{{url('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    
</head>
<body>
    <div id="wrapper">
   <header>
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="inner-top">
        <div class="row">
        	<div class="col-sm-3 logo-area">
            	<div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{url('vendor/dashboard')}}">Manyavar Vendor Panel</a>
            </div>
            </div>
            <div class="col-sm-7 main-menu paddLR0">
            	<div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="{{url('vendor/dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{url('vendor/rfq-request')}}"><i class="fa fa-fw fa-bar-chart-o"></i> New Item <!--RFQ Request--></a>
                    </li>
                    <li>
                        <a href="{{url('vendor/purchase-order-list')}}"><i class="fa fa-fw fa-table"></i> Purchase Order</a>
                    </li>
                    <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-paper-plane"></i> GIT <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li> 
                            <a href="{{url('vendor/all-git-list')}}"> View All GIT</a>
                        </li>
                        <li>
                            <a href="{{url('vendor/all-grt-list')}}"> View All GRT</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-pencil-square-o"></i> Action <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                    	<li>
                            <a href="vendor-all-request.php">View All Request</a>
                        </li>
                        <li>
                            <a href="vendor-rate-changes.php">Request for Rate Changes</a>
                        </li>
                        <li>
                            <a href="vendor-request-cutting.php">Request for Cutting</a>
                        </li>
                        <li>
                            <a href="vendor-width-changes.php">Request for Width Changes</a>
                        </li>
                        <li>
                            <a href="vendor-date-extension.php">Request for Date Extension</a>
                        </li>
                        <li>
                            <a href="vendor-cancellation.php">Request for Cancellation</a>
                        </li>
                        <li>
                            <a href="vendor-create-support-ticket.php">Others <!--Create Support Ticket --></a>
                        </li>
                       
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0)"><i class="fa fa-exchange"></i> Goods Returned</a>
                </li>
                
                <li>
                    <a href="vendor-reports.php"><i class="fa fa-comment"></i> Reports</a>
                </li>
             
            
                <li>
                    <a href="vendor-faqs.php"><i class="fa fa-question"></i> FAQs</a>
                </li>
                    
                    
                </ul>
            </div>
            </div>
            <div class="col-sm-2 right-admin">
            	<ul class="nav navbar-right top-nav">
              
                <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> My Name <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{url('vendor/edit-profile')}}"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{url('vendor/change-password')}}"><i class="glyphicon glyphicon-lock"></i> Change Password</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ url('logout') }}"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            </div>
        </div>
        <div class="clearfix"></div>
        </div>
        </nav>
</header>