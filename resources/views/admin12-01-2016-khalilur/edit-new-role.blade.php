@include('layouts.header')

    <!--add user popup Modal Start-->
    
<div class=" quotation-pop" role="dialog">
    <div class="modal-dialog">
    @if(isset($errors))
                    <?php $i=0;?>
    @foreach ($errors->all() as $error)
    <?php $i++;?>

                    <div class="btn btn-success" id="hide-me{{$i}}" style="width:100%;background-color: red;border: none;">{{ $error }}</div>
                     @endforeach
  @endif

    <!-- Modal content-->
    <form action="{{route('update-role')}}" method="post">
        <input type="hidden" name="_token" value="{{csrf_token()}}"> 
        <div class="modal-content none-shadow">
          <div class="modal-header">
            <h4 class="modal-title">Edit Role</h4>
          </div>
          <div class="modal-body" >
            <div class="row shorting-area pricechange">
                <div class="col-sm-12">
                    <h5>Name :</h5>
                    <input type="hidden"  name="role_id" value="{{$data->id}}" />
                    <input type="text" id="edit_name" name="name" value="{{$data->name}}" onkeyup="return alphaOnly(this)" class="form-control"/>

                    <span class=" error class_err" id="edit_name_msg" ></span>
                </div>
                <div class="col-sm-12">
                    <h5>Slug :</h5>
                    <input type="text" id="edit_slug" name="slug" value="{{$data->slug}}" onkeyup="alphaOnlySlug(this)" class="form-control"/>
                    <span class=" error class_err" id="edit_slug_msg" ></span>
                </div>
                <div class="col-sm-12">
                    <h5>Description :</h5>
                    <textarea type="text" id="edit_description" name="description"  class="form-control">{{$data->description}}</textarea>
                    <span class=" error class_err" id="edit_desc_msg" ></span>
                </div>
                <div class="clearfix"></div>
                <div class="col-sm-6">
                <input class="pup-btn" onclick="return editValidate();" type="submit" value="Update">
                </div>
                <div class="col-sm-6">
                    <a href="{{route('role-listing')}}" data-dismiss="modal" class="pup-btn">Cancel
                    </a>
                </div>
            <div class="clearfix"></div>
            </div>
          </div>
          <div class="modal-footer"></div>
        </div>
    </form>
  </div>
</div>
<!--add user popup  Modal End-->
@include('layouts.footer')

<script type="text/javascript">
    $(document).ready(function () {
        $('#user-form').validate({
            rules: {
                first_name: {
                    required: true,
                },
                last_name: {
                    required: true,
                },
                user_name: {
                    required: true,
                },
            }
        });

        $('#first_name').bind('keyup keydown blur', function(e) {        
       if (e.which === 32 && !this.value.length)
            e.preventDefault();
       $(this).val(function(i, val) {        
           return val.replace(/[^a-zA-Z\s]/gi,''); 
       });       
    });
    })
</script>
<script type="text/javascript">
function editValidate()
{
    var ret_val=true;
    var name=$.trim($("#edit_name").val());    
    if(name==''){
        $("#edit_name_msg").text('Please Enter Name  !').show();
        ret_val=false;
    }
    else
    {
        //alert(name.substring(01));
        if(!isNaN(name.substring(0,1)) )
        {
            $("#edit_name_msg").text("Name Can't Start With a Number !").css('color','red').show();
            ret_val=false;
        }
        else
        {
            $("#edit_name_msg").hide();
        }
    }

    var slug=$.trim($("#edit_slug").val());
    if(slug==''){
        $("#edit_slug_msg").text('Please Enter Slug  !').show();
        ret_val=false;
    }
    else
    {
        if(!isNaN(slug.substring(0,1)) || slug.substring(0,1)=='-' )
        {
            $("#edit_slug_msg").text("Slug Can't Start With a Number and '-' !").css('color','red').show();
            ret_val=false;
        }
        else
        {
            $("#edit_slug_msg").hide();
        }
    }

     return ret_val;
} 

  setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me1" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 1000);
   setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me2" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 1000);

function alphaOnly(txt)
{

    if((txt.value.length==1))
    {
        txt.value = txt.value.replace(/[^a-zA-Z\n\r]+/g, '');
    }
    else 
    {
        txt.value = txt.value.replace(/[^a-zA-Z 0-9\n\r]+/g, '');
    }
}
function alphaOnlySlug(txt)
{

    if((txt.value.length==1))
    {
        txt.value = txt.value.replace(/[^a-zA-Z\n\r]+/g, '');
    }
    else 
    {
        txt.value = txt.value.replace(/[^a-zA-Z 0-9-\n\r]+/g, '');
    }
}
</script>