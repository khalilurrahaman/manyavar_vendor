@include('layouts.header')
<style>
.afterCreate, .shipmentDetails
{
 display:none;
}
</style>
<link rel="stylesheet" href="{{asset('autocomplete/jquery-ui.css')}}" />
<script src="{{asset('autocomplete/jquery-ui.js')}}"></script>
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <div class="row">
                <form id="rfqform" method="post" action="{{route('add-returns-form-post')}}">
                 {{ csrf_field() }}
                    <div class="col-lg-10 margin-center">
                    <div class="panel panel-default">
                    
                            <!--///Default///-->
                            <div class="panel-heading beforeCreate">
                                <h3 class="panel-title"><i class="fa fa-tree" aria-hidden="true"></i> Select Product</h3>
                            </div>
                            <div class="clearfix beforeCreate"></div>
                            <div class="panel-body custome-table beforeCreate">
                                <div class="row shorting-area add-rfq">
                                    <div class="rfq-area">
                                        <div class="col-sm-4">
                                            <button class="btn btn-warning width-small" data-toggle="modal" data-target="#selectSample" type="button">Sample Return</button>
                                        </div>

                                        <div class="col-sm-4">
                                            <button class="btn btn-info width-small" data-toggle="modal" data-target="#selectpro" type="button" onclick="item_form()">Defective Fabric</button>
                                        </div> 
                                        
                                        <div class="col-sm-4">
                                          <button class="btn btn-success width-small" data-toggle="modal" data-target="#adhok" type="button">Adhoc</button>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <!--///After Create///-->
                          
                            <input type="hidden" id="afterCreateImagehidden" name="image" value="" />
                            <input type="hidden" id="afterCreateCat1hidden" name="cat_1" value="" />
                            <input type="hidden" id="afterCreateCat2hidden" name="cat_2" value="" />
                            <input type="hidden" id="afterCreateCat3hidden" name="remarks" value=""  />
                            <div class="panel-heading afterCreate">
                                <h3 class="panel-title pull-left"><i class="fa fa-tree" aria-hidden="true"></i> Selected Product </h3>
                                <h3 class="panel-title pull-right"><a href="{{route('goods-returns-add-form')}}" style="font-size:16px; line-height:3px;"><strong>×</strong></a></h3>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix afterCreate"></div>
                            <div class="panel-body custome-table afterCreate">
                                <div class="row shorting-area add-rfq">
                                    <div class="col-sm-12">
                                    
                                    <div class="panel-body pre-order custome-table">
                                    <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" style="margin-bottom:0;">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Image</th>
                                                <th class="text-center">Cat 1</th>
                                                <th class="text-center">Cat 2</th>
                                                <th class="text-center">Remarks</th>
                                                <!-- <th class="text-center">Cat 4</th> -->
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <tr>
                                        <td class="text-center">
                                        <img class="previewing" id="afterCreateImage"  src="" title="small image" width="56">
                                        </td>
                                        <td class="text-center" id="afterCreateCat1">--</td>
                                        <td class="text-center" id="afterCreateCat2">--</td>
                                        <td class="text-center" id="afterCreateCat3">--</td>
                                        <!-- <td class="text-center" id="afterCreateCat4">--</td> -->
                                    </tr>
                                </tbody>
                               </table>
                              </div>
                                </div>                                    
                                    <div class="clearfix"></div>
                                </div>
                                </div>
                            </div>
                            
                        </div>
                       
                        
                        
                        <div class="panel panel-default shipmentDetails">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i> Select Vendor and Add Details</h3>
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                                <div class="row shorting-area add-rfq">
                                    <div class="rfq-area">
                                      <div class="col-sm-5 margin-center">
                                        <select class="selectpicker2 custom-picker " data-live-search="true" onchange="addVendor(this.value);" id="ven_id">
                                            <option data-tokens="ketchup mustard" value="">Select Vendor</option>
                                             @if(!empty($allVendor) && count($allVendor)>0)
                                             @foreach($allVendor as $eachVendor)
                                             <?php 
                                             $o_value = $eachVendor->id.','.$eachVendor->name; 
                                             $data_tokens = $eachVendor->name;
                                             $o_tvalue = $eachVendor->name;
                                             ?>
                                                <option data-tokens="{{$data_tokens}}" value="{{$o_value}}">{{$o_tvalue}}</option>
                                             @endforeach
                                             @endif
                                        </select>

                                    </div>
                                    <!--<div class="col-sm-2 text-center">
                                        <h5>---</h5>
                                    </div>-->
                                    <!--<div class="col-sm-5">
                                        <button class="btn btn-warning2 width-small marginB0" type="button">Add Vendor</button>
                                    </div>-->
                                    <div class="clearfix"></div>
                                </div>
                                
                                </div>
                                <div class="clearfix"></div>
                                <hr/>
                                <div class="table-responsive view-table">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Vendor ID</th>
                                                <th class="text-center">Name</th>
                                                <th class="text-center" style="width:226px;">Docket No</th>
                                                <th class="text-center" style="width:226px;">PRN No</th>
                                                <th class="text-center" style="width:226px;">PRN Date</th>
                                                <th class="text-center" style="width:226px;">Courier Name</th>
                                                <th class="text-center" style="width:226px;">Quantity</th>
                                                <th class="text-center" >Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id='tab_data'>
                                        <span id="tab_message"></span>
                                        </tbody>
                                    </table>
                              </div>
                              <div class="rfq-area">                                
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="comment">Remarks :</label>
                                             <textarea name="comment" class="form-control" rows="5"  id="commant"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 quotation-pop">
                                     <button type="submit" onclick="return addGoodsReturn();"  class="pup-btn" style="margin-top:0;">RETURN</button>
                                     </div>
                                    <div class="col-sm-6 quotation-pop"> 
                                     <a style="margin-top: auto;" href="{{route('goods-returns-list')}}" class="pup-btn">CANCEL</a>
                                     
                                    </div>
                                </div>  
                                
                                
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        
                    </div>
                  </form>  
                    
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@include('layouts.footer')

<!--/////////////////////////////////////////////////////////select product Modal Start//////////////////////////////////////////////////////////////-->

<<!--add RFQ Modal Start-->
<form method="post" action="{{route('add-item')}}" enctype="multipart/form-data" id="form_rfq">
 {{ csrf_field() }}
<div id="selectpro" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Defective Fabric</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
            <div class="col-sm-6">
                        <img src="{{asset('img/pre-order-img.jpg')}}" class="previewing" id="previewing" alt="" width="100%"><div class="clearfix">
                         <span  id="item_message" ></span>   
                    </div> <br/>
                    <div class="clearfix"></div>
                    <input type="file" id="file" name="file" value=""/>
                    <span class=" error class_err" id="file_msg" ></span>
                    </div>
            <div class="col-sm-3">
            <label>Category 1 :</label>
             <input type="text" placeholder="Category1" class="form-control" id="cat_1" name="cat1" value="" />
             <span class=" error class_err" id="cat_1_msg" ></span>
            </div>
            <div class="col-sm-3">
            <label>Category 2 :</label>
            <input type="text" placeholder="Category2" class="form-control" id="cat_2" name="cat2" value="" />
             <span class=" error class_err" id="cat_2_msg" ></span>
            </div>
            <script>
var cat1_list = <?php echo $cat1_list1;?>;
var cat2_list = <?php echo $cat2_list2;?>;

$(document).ready(
  function () {
    $( "#cat_1" ).autocomplete({
      source:cat1_list,
      autoFocus: true ,
      //change: function (event, ui) { alert(ui.item.value); }
    });
    $( "#cat_2" ).autocomplete({
      source:cat2_list,
      autoFocus: true ,
      //change: function (event, ui) { alert(ui.item.value); }
    });
    $( "#cat_1_sample" ).autocomplete({
      source:cat1_list,
      autoFocus: true ,
      //change: function (event, ui) { alert(ui.item.value); }
    });
    $( "#cat_2_sample" ).autocomplete({
      source:cat2_list,
      autoFocus: true ,
      //change: function (event, ui) { alert(ui.item.value); }
    });
    
  }

);
  </script>
            
            <div class="col-sm-6">
            <label>Remarks :</label>
           <textarea  placeholder="Remarks" class="form-control" id="remarks" name="remarks"  cols="50"></textarea> 
            <span class=" error class_err" id="remarks_msg" ></span>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
              <a href="javascript:void(0)" class="pup-btn" onclick="return validateForm()">Submit Now</a>
            </div>
            <div class="col-sm-6">
              <a href="javascript:void(0);" class="pup-btn" data-dismiss="modal">Cancel</a>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
</form>
<!--add RFQ  Modal End-->
<!--add RFQ Modal Start-->
<form method="post" action="{{route('add-item')}}" enctype="multipart/form-data" id="form_rfq">
 {{ csrf_field() }}
<div id="selectSample" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Sample Return</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
            <!-- <div class="col-sm-6">
                        <img src="{{asset('img/pre-order-img.jpg')}}" class="previewing" id="previewing" alt="" width="100%"><div class="clearfix">
                         <span  id="item_message" ></span>   
                    </div> <br/>
                    <div class="clearfix"></div>
                    <input type="file" id="file" name="file" value=""/>
                    <span class=" error class_err" id="file_msg" ></span>
                    </div> -->
            <div class="col-sm-6">
            <label>Category 1 :</label>
             <input type="text" placeholder="Category1" class="form-control" id="cat_1_sample" name="cat1" value="" />
             <span class=" error class_err" id="cat_1_msg_sample" ></span>
            </div>
            <div class="col-sm-6">
            <label>Category 2 :</label>
            <input type="text" placeholder="Category2" class="form-control" id="cat_2_sample" name="cat2" value="" />
             <span class=" error class_err" id="cat_2_msg_sample" ></span>
            </div>
            
            <!-- <div class="col-sm-6">
            <label>Remarks :</label>
           <textarea  placeholder="Remarks" class="form-control" id="remarks" name="remarks"  cols="50"></textarea> 
            <span class=" error class_err" id="remarks_msg" ></span>
            </div>-->
            <div class="clearfix"></div>
            <div class="col-sm-6"> 
            <a href="javascript:void(0)" class="pup-btn" onclick="return validateFormSample()">Submit Now</a>
            </div>
            <div class="col-sm-6">
             <a href="javascript:void(0);" class="pup-btn" data-dismiss="modal">Cancel</a>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
</form>
<!--add RFQ  Modal End-->


<!--select product Modal Start-->
<div id="adhok" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Adhoc Return</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
        <div class="pull-right">
                                         <a href="javascript:void(0)" id="show-btn" class="btn btn-success" style="font-size:12px; margin-bottom:12px;" onclick="getOptionFilter();">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                </div>
                                <div class="col-sm-5">
                                <div class="inner-addon right-addon">
          <i class="glyphicon glyphicon-search"></i>
         <input type="text" class="form-control" placeholder="Search" style="margin-bottom:0;" id="search_exist" name="search_exist" onkeypress="javascript: if (event.keyCode==13) getOptionFilter();">
        </div>
                              </div>
                                <div class="clearfix"></div>
        <div id="mySidenavR" class="sidenavR" style="display: none;">
<div class="filter-right">
<div class="filter-area list-filter pop-filter">
<!--<h5>Vendor List Filter</h5>-->
    <a href="javascript:void(0)" class="closebtn" id="hide-filter">×</a>
        <div class="col-sm-3">
            <a class="btn btn-default btn-select">
                <input type="hidden" class="btn-select-input" id="selectcat1" name="selectcat1" value="">
                <span class="btn-select-value">Select Cat1</span>
                <span class="btn-select-arrow glyphicon glyphicon-chevron-down"></span>
                <ul style="display: none;">
                    <li class="selected">Select Cat1</li>
                    @if(!empty($cat1_list) && count($cat1_list)>0)
                    @foreach($cat1_list as $cat1)
                    @if($cat1->cat1!='')
                    <li>{{$cat1->cat1}}</li>
                    @endif
                     @endforeach
                    @endif
                </ul>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-3">
            <a class="btn btn-default btn-select">
                <input type="hidden" class="btn-select-input" id="selectcat2" name="selectcat2" value="">
                <span class="btn-select-value">Select Cat2</span>
                <span class="btn-select-arrow glyphicon glyphicon-chevron-down"></span>
                <ul style="display: none;">
                    <li class="selected">Select Cat2</li>
                    @if(!empty($cat2_list) && count($cat2_list)>0)
                    @foreach($cat2_list as $cat2)
                    @if($cat2->cat2!='')
                    <li>{{$cat2->cat2}}</li>
                    @endif
                     @endforeach
                    @endif
                </ul>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-3">
            <a class="btn btn-default btn-select">
                <input type="hidden" class="btn-select-input" id="selectcat3" name="selectcat3" value="">
                <span class="btn-select-value">Select Cat3</span>
                <span class="btn-select-arrow glyphicon glyphicon-chevron-down"></span>
                <ul style="display: none;">
                    <li class="selected">Select Cat3</li>
                   @if(!empty($cat3_list) && count($cat3_list)>0)
                    @foreach($cat3_list as $cat3)
                    @if($cat3->cat3!='')
                    <li>{{$cat3->cat3}}</li>
                    @endif
                     @endforeach
                    @endif
                </ul>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-3">
            <a class="btn btn-default btn-select">
                <input type="hidden" class="btn-select-input" id="selectcat4" name="selectcat4" value="">
                <span class="btn-select-value">Select Cat4</span>
                <span class="btn-select-arrow glyphicon glyphicon-chevron-down"></span>
                <ul style="display: none;">
                    <li class="selected">Select Cat4</li>
                   @if(!empty($cat4_list) && count($cat4_list)>0)
                    @foreach($cat4_list as $cat4)
                    @if($cat4->cat4!='')
                    <li>{{$cat4->cat4}}</li>
                    @endif
                     @endforeach
                    @endif
                </ul>
            </a>
            <div class="clearfix"></div>
        </div>
        
        <div class="col-sm-3">
            <a href="javascript:void(0)" class="btn btn-success selectSearch">Apply</a>
        </div>
<div class="clearfix"></div>        
    </div>
    <div class="clearfix"></div>
    </div>    
</div>
            <div class="panel-body pre-order custome-table">
                                    <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" style="margin-bottom:0;">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Image</th>
                                                <th class="text-center">Cat 1</th>
                                                <th class="text-center">Cat 2</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Cat 4</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody id="existItemLIstModal">
                                    
                                    @if(!empty($allProduct) && count($allProduct)>0)
                                    @foreach($allProduct as $ext)
                                    <tr>
                                        <td class="text-center">
                                        <?php
                                            $e_image=URL::to('/')."/img/pre-order-img.jpg";
                                            if(!empty($ext->image) && file_exists(public_path().'/upload/product/'.$ext->image))
                                            {
                                               $e_image= URL::to('/') .'/upload/product/'.$ext->image;
                                            }
                                        ?>
                                        <img src="<?php echo $e_image; ?>" title="small image" width="56" class="selectProductExistClassImage{{$ext->id}}">
                                        </td>
                                        <td class="text-center selectProductExistClassCat1{{$ext->id}}">{{$ext->cat1}}</td>
                                        <td class="text-center selectProductExistClassCat2{{$ext->id}}">{{$ext->cat2}}</td>
                                        <td class="text-center selectProductExistClassCat3{{$ext->id}}">{{$ext->cat3}}</td>
                                        <td class="text-center selectProductExistClassCat4{{$ext->id}}">{{$ext->cat4}}</td>
                                        <td class="text-center">
                                        <a href="javascript:void(0);" product_id="{{$ext->id}}" class="btn btn-success selectProductExist"  style="width:56px; padding:3px 0; font-size:12px;">Select</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                     <tr>
                                        <td class="text-center" colspan="6">Product Not Available!</td>
                                    </tr>
                                    @endif
                                    
                                </tbody>
                               </table>
                              </div>
                                </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--select product  Modal End-->


<script> 
var path= "<?php echo URL::to('/') .'/upload/product/';?>";
</script>
<script>

    function addVendor(value) {
        
        var vendor =  value;

        if(vendor!=''){
            var vendor_arr = vendor.split(',');
            var id = vendor_arr[0];
            var name = vendor_arr[1];
            if($.trim($("#tab_data").html())==''){
                
                var vendor_html = '';

                vendor_html += '<tr id="row_'+id+'" ><td class="text-center">'+id+'<input type="hidden"  value="'+id+'" name="vendor_id"></td>';
                vendor_html += '<td class="text-center">'+name+'</td>';
                vendor_html += '<td class="text-center "><div class="form-group marginB0"><input type="text" name="docket_no" class="form-control marginB0" placeholder="Enter Docket No" id="docno"><span id="docno_msg"></span></div></td>';
                vendor_html += '<td class="text-center"><div class="form-group marginB0"><input type="text" name="prn_no" class="form-control marginB0" placeholder="Enter PRN No"></div></td>';
                vendor_html += '<td class="text-center"><div class="form-group marginB0"><input type="text" id="date" name="prn_date" class="form-control marginB0 input-group date" data-provide="datepicker" placeholder="Enter PRN Date"></div></td>';
                vendor_html += '<td class="text-center"><div class="form-group marginB0"><input type="text" name="courier_name" class="form-control marginB0" placeholder="Enter Courier Name"></div></td>';
                vendor_html += '<td class="text-center"><div class="form-group marginB0"><input type="text" name="quantity" class="form-control marginB0" placeholder="Enter Quantity" id="quantity" onkeypress="return isNumberKey(event)"><span id="quantity_msg"></span></div></td>';
                vendor_html += '<td class="text-center"><a href="javascript:void(0)" onclick="removeVendor('+id+')" class="btn btn-danger">Remove</a></td>';
                vendor_html += '</tr>';

                $("#tab_data").append(vendor_html);

            }else{
                alert("Can not add more then one vendor");
            }
        }else{
            alert('Please Select Any Vendor');
        }
    }


 
    function removeVendor(row_id) {
        $('#row_'+row_id).remove();
    }
    
    
     function item_form(){
        var img_path = '<?php echo asset('img/pre-order-img.jpg')?>' 
        $('#cat_1').val('');
        $('#cat_2').val('');
        $('#cat_3').val('');
        $('#cat_4').val('');
        $('#previewing').attr('src',img_path);
        $('#file').val('').css("color","black");;
    }
    
     function addGoodsReturn() {
        var ret_val=true;   
   
        var docno=$("#docno").val();
        if(docno==''){$("#docno_msg").text('Please Enter Docket No !').css('color', 'red').show();ret_val=false;}
        else{$("#docno_msg").hide();}

        var quantity=$("#quantity").val();
        if(quantity==''){$("#quantity_msg").text('Please Enter Quantity  !').css('color', 'red').show();ret_val=false;}
        else{$("#quantity_msg").hide();}

        if($.trim($("#tab_data").html())==''){
            $("#tab_message").text('Please Add a Vendor !').css('color', 'red').show();
            ret_val=false;
        }else{
            $('#tab_message').text('');
            
        }

        return ret_val;
    }
    
/*$( "#search_exist" ).keyup(function() {
 var search_str=$(this).val();
 var token='{{csrf_token()}}';
        $.ajax({
            type: 'GET', 
            url: '{{route("search-item-by-keyword")}}',
            data: {search_str:search_str, _token:token},
            success:function(data)
            {
                $('#existItemLIstModal').html(data);
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });     
});
$( "#hide-filter" ).click(function() {
 filter();
});*/

$('.selectSearch').click(function(){
         var selectcat1 = $('#selectcat1').val();
         var selectcat2 = $('#selectcat2').val();
         var selectcat3 = $('#selectcat3').val();
         var selectcat4 = $('#selectcat4').val();
        filter(selectcat1,selectcat2,selectcat3,selectcat4) ;
});
function filter(selectcat1='',selectcat2='',selectcat3='',selectcat4=''){
         
         var token='{{csrf_token()}}';
        $.ajax({
            type: 'GET', 
            url: '{{route("search-item-by-keyword")}}',
            data: {selectcat1:selectcat1,selectcat2:selectcat2,selectcat3:selectcat3,selectcat4:selectcat4, _token:token},
            success:function(data)
            {
                $('#existItemLIstModal').html(data);
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
}



function validateForm()
  { 
     
    var ret_val=true;   
   
    var cat_1=$("#cat_1").val();
    if(cat_1==''){$("#cat_1_msg").text('Please Enter Category1  !').css('color', 'red').show();ret_val=false;}
    else{$("#cat_1_msg").hide();}

    var cat_2=$("#cat_2").val();
    if(cat_2==''){$("#cat_2_msg").text('Please Enter Category2  !').css('color', 'red').show();ret_val=false;}
    else{$("#cat_2_msg").hide();}

    var remarks=$("#remarks").val();
    if(remarks==''){$("#remarks_msg").text('Please Enter Remarks  !').css('color', 'red').show();ret_val=false;}
    else{$("#remarks_msg").hide();}

    var image=$("#file").val();
    if(image==''){$("#file_msg").text('Please Enter Image  !').css('color', 'red').show();ret_val=false;}
    else{$("#file_msg").hide();}

    $('#quantity').bind('keyup keydown blur', function(e) {        
          if (e.which === 32 && !this.value.length)
               e.preventDefault();
          $(this).val(function(i, val) {        
              return val.replace(/[^0-9\s]/gi,''); 
          });       
       });



    var item_image = $("#file").val();
    var imagefile = item_image.split('.').pop();
    var imgArray= ["jpeg","png","jpg","JPEG","PNG","JPG"];
    
    if($.inArray(imagefile,imgArray)== -1){
        $("#file_msg").text('Please Select a Valid Image!').css('color', 'red').show();
       return false;
    }
    var urls = "{{route('add-goods-file')}}";

    if(ret_val)
    {
      $('.loading').show();  
    
        $.ajax({
            url: urls,
            type: 'POST',
            data:new FormData($("#form_rfq")[0]),
            processData: false,
            contentType: false,
            asynce: false,
            cache: false,
            success: function(data)
            {


                //console.log(data.id);
                //$('#product_id').val(data.id);
                //$("#afterCreateImage").attr("src",path+image);
                $("#afterCreateCat1").text(cat_1);
                $("#afterCreateCat2").text(cat_2);
                $("#afterCreateCat3").text(remarks);

                $("#afterCreateImagehidden").val(data.file_name);
                $("#afterCreateCat1hidden").val(cat_1);
                $("#afterCreateCat2hidden").val(cat_2);
                $("#afterCreateCat3hidden").val(remarks);
                /*$("#afterCreateCat4").text(data.cat4);
               
                */
                $(".beforeCreate").hide();
                $(".afterCreate").show();
                $(".shipmentDetails").show();
                $("#selectpro").modal('hide');
                $('.loading').hide(); 
                }

            

        });
             
    }   

    return ret_val;

  }
  $(function() {
        $("#file").change(function() {
            var file = this.files[0];
            var imagefile = file.type;
            var match= ["image/jpeg","image/png","image/jpg"];
            if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
            {
                $('#previewing').attr('src','pre-order-img.jpg');
                $("#item_message").text('Please Select Image!').css('color', 'red').show();
            return false;
            }
            else
            {
                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
   function imageIsLoaded(e) {
        $("#file").css("color","green");
         $("#item_message").hide();
        $('.previewing').attr('src', e.target.result);
    };


$('#quantity').bind('keyup keydown blur', function(e) {        
          if (e.which === 32 && !this.value.length)
               e.preventDefault();
          $(this).val(function(i, val) {        
              return val.replace(/[^0-9\s]/gi,''); 
          });       
       });



function validateFormSample()
  {    
    var ret_val=true;   
   
    var cat_1=$("#cat_1_sample").val();
    if(cat_1==''){$("#cat_1_msg_sample").text('Please Enter Category1  !').css('color', 'red').show();ret_val=false;}
    else{$("#cat_1_msg_sample").hide();}

    if($("#cat_2_sample").val()=='')
    {
        var cat_2='';
    }
    else
    {
        var cat_2=$("#cat_2_sample").val();
    }
    /*var cat_2=$("#cat_2_sample").val();
    if(cat_2==''){$("#cat_2_msg_sample").text('Please Enter Category2  !').css('color', 'red').show();ret_val=false;}
    else{$("#cat_2_msg_sample").hide();}*/

    if(ret_val)
    {  

    var img_path = '<?php echo asset('img/pre-order-img.jpg')?>' 
        
        $('.previewing').attr('src',img_path);
   
        $("#afterCreateCat1").text(cat_1);

        $("#afterCreateCat2").text(cat_2);
       // $("#afterCreateCat3").text()='';

        //$("#afterCreateImagehidden").val()='';
        $("#afterCreateCat1hidden").val(cat_1);
        $("#afterCreateCat2hidden").val(cat_2);
        //$("#afterCreateCat3hidden").val()='';
        /*$("#afterCreateCat4").text(data.cat4);
       
        */
        $(".beforeCreate").hide();
        $(".afterCreate").show();
        $(".shipmentDetails").show();
        $("#selectSample").modal('hide');

    }
    else
    {
        return ret_val;
    }

  }

$(document).on('click', '.selectProductSample', function(e) {
     
     var product_id = $(this).attr('product_id');
     var img = $('.selectProductSampleClassImage'+product_id).attr('src');
     
     $("#product_id").val(product_id);
     $("#afterCreateImage").attr("src",img);
     
     $(".beforeCreate").hide();
     $(".afterCreate").show();
     $(".shipmentDetails").show();
     $("#samplepro").modal('hide');
});


$(document).on('click', '.selectProductExist', function(e) {
     var product_id = $(this).attr('product_id');
     var img = $('.selectProductExistClassImage'+product_id).attr('src');
     var cat1 = $('.selectProductExistClassCat1'+product_id).text();
     var cat2 = $('.selectProductExistClassCat2'+product_id).text();
     //var cat3 = $('.selectProductExistClassCat3'+product_id).text();
     //var cat4 = $('.selectProductExistClassCat4'+product_id).text();
     
     $("#product_id").val(product_id);
     $("#afterCreateImage").attr("src",img);
     $("#afterCreateCat1").text(cat1);
     $("#afterCreateCat2").text(cat2);
     //$("#afterCreateCat3").text(cat3);
     //$("#afterCreateCat4").text(cat4);
     
     $("#afterCreateCat1hidden").val(cat1);
     $("#afterCreateCat2hidden").val(cat2);
     var img_name = img.split('product/');
     $("#afterCreateImagehidden").val('../product/'+img_name[1]);
     
     
     $(".beforeCreate").hide();
     $(".afterCreate").show();
     $(".shipmentDetails").show();
     $("#adhok").modal('hide');
});

function getOptionFilter() {
 var search_str=$('#search_exist').val();
 $('.error_msg').text('');
 if(search_str=='')
 {
     $('.error_msg').text('Please Enter Search Keyword!').css('color', 'red').show();
     return false;
 }
 //$('.loading').show();
 
 $('#mySidenavR').show();
 var token='{{csrf_token()}}';
        $.ajax({
            type: 'GET', 
            url: '{{route("get-option-filter")}}',
            data: {search_str:search_str},
            success:function(data)
            {
                
                $('#mySidenavR').html(data);
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
}


function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

</script>   


