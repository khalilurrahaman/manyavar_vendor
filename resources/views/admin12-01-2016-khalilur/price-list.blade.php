@include('layouts.header')

<script src="{{asset('css/dataTables.bootstrap.min.css')}}"></script>
  <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
   <script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script>
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                	<div class="col-sm-6"><h3 class="panel-title">
                                    <i class="fa fa-users" aria-hidden="true"></i> Price Listing</h3></div>
                                    <div class="col-sm-6">
                                    
                                    <div class="shorting-area">
                            		
                              
                                <div class="col-sm-3 pull-right">
                                        <a  data-toggle="modal" class="btn btn-success">
                                        <i class="fa fa-plus" aria-hidden="true"></i> Add New</a>
                                </div>
                            </div>
                                    
                                    
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                                <div class="table-responsive ">
                                    <table id="priceListTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Vendor</th>
                                                <th class="text-center">Item</th>
                                                <th class="text-center">Price</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    @foreach($data as $eachData)
                                     
                                    <tr>
                                        <td class="text-center">{{($eachData->getVendorByPrice->name)}} </td>
                                        <td class="text-center">@if($eachData->getItemByPrice){{($eachData->getItemByPrice->ICODE)}}@endif</td>
                                        <td class="text-center">{{$eachData->RATE}} </td>
                                        <td class="text-center">  
                                            <a   class="btn btn-success">Edit</a>
                                        </td>
                                    </tr>
                                    @endforeach                            
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                              <div class="col-sm-6">
                              	<div class="dataTables_info" role="status" aria-live="polite">Showing {{$data->count()}} of {{$data->total()}} entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                        {{ $data->links() }}
                                    </nav>
                                  </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

<script type="text/javascript">


    function ConfirmDelete(id)
        {
            var x = confirm("Are you sure you want to Delete?");
            if (x)
                return true;
            else
                return false;
        }
		$(document).ready(function () {
			
            $('#priceListTable').dataTable({
				"order": [[ 0, "asc" ]],//Default Column Attribute
				"pageLength": 25,//Per Page Item
				'sDom': 't',//Hide All Info From top and bottom
				'aTargets': [-1] //1st one, start by the right/
            });
        });
</script>
@include('layouts.footer')