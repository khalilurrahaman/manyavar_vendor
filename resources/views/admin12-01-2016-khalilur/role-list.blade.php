@include('layouts.header')

        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <div class="row">
                    <div class="col-lg-12">
                    @if(isset($errors))
                    <?php $i=0;?>
    @foreach ($errors->all() as $error)
    <?php $i++;?>

                    <div class="btn btn-success" id="hide-me{{$i}}" style="width:100%;background-color: green;border: none;">{{ $error }}</div>
                     @endforeach
  @endif
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                	<div class="col-sm-6"><h3 class="panel-title">
                                    <i class="fa fa-users" aria-hidden="true"></i>User Role Listing</h3></div>
                                    <div class="col-sm-6">
                                    
                                    <div class="shorting-area">
                            		
                              
                                <div class="col-sm-3 pull-right">
                                        <a href="{{route('add-new-role')}}"  class="btn btn-success">
                                        <i class="fa fa-plus" aria-hidden="true"></i> Add Role</a>
                                </div>
                            </div>
                                    
                                    
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">ID</th>
                                                <th class="text-center">Name</th>
                                                <th class="text-center">Slug</th>
                                                <th class="text-center">Description</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <?php $count=0;?>
                                    @foreach($roles as $row)
                                    <?php $count++;?>
                                    <tr>

                                        <td class="text-center">{{$count}}</td>
                                        <td class="text-center">{{$row->name}}</td>
                                        <td class="text-center">{{$row->slug}}</td>
                                        <td class="text-center">{{$row->description}}</td>
                                        <td class="text-center" style="width: 27%">
                                            <a href="{{route('view-user',$row->id)}}" class="btn btn-primary">View User List</a>&nbsp
                                            <a href="{{route('add-permission',$row->id)}}" class="btn btn-info">Add Permission</a>&nbsp
                                            <a href="{{route('edit-new-role',$row->id)}}" class="btn btn-primary"  data-toggle="modal">Edit</a>&nbsp
                                            <a href="{{route('delete-role',$row->id)}}" onclick="return deleteConfirm()" class="btn btn-danger">Delete</a>
                                        </td>
                                    </tr> 
                                    @endforeach                          
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                             
                        
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
        <!--add user popup Modal Start-->

        <style type="text/css">
            
            .class_err{color: red;}
        </style>
<div id="adduser" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <form action="{{route('add-role')}}" method="post">
        <input type="hidden" name="_token" value="{{csrf_token()}}"> 
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add New Role</h4>
          </div>
          <div class="modal-body">
            <div class="row shorting-area pricechange">
                <div class="col-sm-12">
                    <h5>Name :</h5>
                    <input type="text" id="name" name="name" onkeyup="return alphaOnly(this)" class="form-control"/>
                    <span class=" error class_err" id="name_msg" ></span>
                </div>
                <div class="col-sm-12">
                    <h5>Slug :</h5>
                    <input type="text" id="slug" name="slug" onkeyup="return alphaOnly(this)" class="form-control"/>
                    <span class=" error class_err" id="slug_msg" ></span>
                </div>
                <div class="col-sm-12">
                    <h5>Description :</h5>
                    <textarea type="text" id="description" name="description" class="form-control"></textarea>
                    <span class=" error class_err" id="desc_msg" ></span>
                </div>
                <div class="clearfix"></div>
                <div class="col-sm-6">
                <input class="pup-btn" onclick="return validateForm();" type="submit" value="Add Role">
                </div>
                <div class="col-sm-6">
                    <a href="#" data-dismiss="modal" class="pup-btn">Cancel
                    </a>
                </div>
            <div class="clearfix"></div>
            </div>
          </div>
          <div class="modal-footer"></div>
        </div>
    </form>
  </div>
</div>
<div id="editRole" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <form action="{{route('update-role')}}" method="post">
        <input type="hidden" name="_token" value="{{csrf_token()}}"> 
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit Role</h4>
          </div>
          <div class="modal-body">
            <div class="row shorting-area pricechange">
                <div class="col-sm-12">
                    <h5>Name :</h5>
                    <input type="hidden" id="edit_role_id" name="role_id" class="form-control"/>
                    <input type="text" id="edit_name" name="name" onkeyup="return alphaOnly(this)" class="form-control"/>
                    <span class=" error class_err" id="edit_name_msg" ></span>
                </div>
                <div class="col-sm-12">
                    <h5>Slug :</h5>
                    <input type="text" id="edit_slug" name="slug" onkeyup="return alphaOnly(this)"  class="form-control"/>
                    <span class=" error class_err" id="edit_slug_msg" ></span>
                </div>
                <div class="col-sm-12">
                    <h5>Description :</h5>
                    <textarea type="text" id="edit_description" name="description" class="form-control"></textarea>
                    <span class=" error class_err" id="edit_desc_msg" ></span>
                </div>
                <div class="clearfix"></div>
                <div class="col-sm-6">
                <input class="pup-btn" onclick="return editValidate();" type="submit" value="Add Role">
                </div>
                <div class="col-sm-6">
                    <a href="#" data-dismiss="modal" class="pup-btn">Cancel
                    </a>
                </div>
            <div class="clearfix"></div>
            </div>
          </div>
          <div class="modal-footer"></div>
        </div>
    </form>
  </div>
</div>
<!--add user popup  Modal End-->
@include('layouts.footer')

<script type="text/javascript">
function validateForm()
{    
    var ret_val=true;
    var name=$.trim($("#name").val());    
    if(name==''){
        $("#name_msg").text('Please Enter Name  !').show();
        ret_val=false;
    }
    else
    {
         var post_string='name='+name;
    $.ajax({
        url: 'role-validation',
        type: 'get',       
        data: post_string,

        success:function(data)
        {
        //console.log(data.error);
        //var value=data.error;
        //console.log(value);
        //alert(data.error);
        if(data.error)
        {
            $("#name_msg").text('Role Name Already Exists !').show();
            var ret_val=false;
        }
        else
        {
            $("#name_msg").hide();
            var ret_val=true;
        }


           
        }



    });
       /* alert(checkDuplicate(name));
        if(checkDuplicate(name))
        {
             $("#name_msg").hide();
            
        }else{
            $("#name_msg").text('Role Name Already Exists !').show();
            ret_val=false;

            

        }*/
    }

    var slug=$.trim($("#slug").val());
    if(slug==''){
        $("#slug_msg").text('Please Enter Slug  !').show();
        ret_val=false;
    }
    else{
        $("#slug_msg").hide();
    }
    

    
    return ret_val;
} 


/*function checkDuplicate(name)
{
   // var ret_val=true;

    var post_string='name='+name;
    $.ajax({
        url: 'role-validation',
        type: 'get',       
        data: post_string,
        success:function(data)
        {
        //console.log(data.error);
        var value=data.error;
        //console.log(value);
        if(value=='fail')
        {

           var ret_val=false;
        }
        else
        {
           var ret_val=true;
        }


           
        }



    });
    
    return ret_val;

}*/
function deleteConfirm()
{
    if(confirm('Are you sure'))
    {
        return true;
    }
    else
    {
        return false;
    }
}
function editRole(id)
{
    $("#edit_name_msg").hide();
    $("#edit_slug_msg").hide();
    var eurl="{{route('edit-role')}}";
    var token="{{csrf_token()}}";
    $.ajax({
        type:'post',
        url:eurl,
        data:{id:id,_token:token},
        success:function(data)
        {
            $('#edit_role_id').val(id);
            $('#edit_name').val(data.name);
            $('#edit_slug').val(data.slug);
            $('#edit_description').val(data.description);
        }



    });
}
function editValidate()
{
    var ret_val=true;
    var name=$.trim($("#edit_name").val());    
    if(name==''){
        $("#edit_name_msg").text('Please Enter Name  !').show();
        ret_val=false;
    }
    else{
        $("#edit_name_msg").hide();
    }

    var slug=$.trim($("#edit_slug").val());
    if(slug==''){
        $("#edit_slug_msg").text('Please Enter Slug  !').show();
        ret_val=false;
    }
    else{
        $("#edit_slug_msg").hide();
    }

     return ret_val;
}
</script>
<script type="text/javascript">
   setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me1" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 1000);
   setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me2" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 1000);



function alphaOnly(th) {
  if(th.value.length==1)
  {
    if(!isNaN(val)){
       return th.value.replace(/\d+/,''); 
     }
  }
   
    
}
</script>