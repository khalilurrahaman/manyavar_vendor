@include('layouts.header')
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <!--<div class="row">
                    <div class="col-lg-12 title-top">
                    <div class="row page-header">
                    	<div class="col-sm-6">
                        	<h1>Admin <small>Purchase Order Listing</small></h1>
                        </div>
                        <div class="col-sm-6">
                        	<a href="javascript:void(0)" class="btn btn-success">Expand</a>
                        </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading ">
                                <div class="row">
                                	<div class="col-sm-6">
                                    	<h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i> Purchase Order Listing</h3>
                                    </div>
                                    <div class="col-sm-6">
                                    
                                    <div class="row shorting-area">
                                    <div class="col-sm-2 purchase-filter pull-right">
                                        <a href="javascript:void(0)" id="show-btn" class="btn btn-success">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                </div>
                            </div>
                                    
                                    
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                            <div class="row shorting-area">
                            	<!--<div class="col-sm-6">
                              	<div class="dataTables_info" role="status" aria-live="polite">Showing 1 to 9 of 9 entries</div>
                              </div>
                              <div class="col-sm-2 pull-right">
                                        <a class="btn btn-default btn-select">
                                            <input type="hidden" class="btn-select-input" id="" name="" value="" />
                                            <span class="btn-select-value">Select an Item</span>
                                            <span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                                        	<ul>
                                                <li class="selected">Short By</li>
                                                <li>Due Date</li>
                                                <li>Order Date</li>
                                                <li>Pending Qty</li>
                                        	</ul>
                                        </a>
                                </div>
                            <div class="col-sm-1 pull-right">
                                        <a href="javascript:void(0)" id="show-btn" class="btn btn-success">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                </div>--> <div class="clearfix"></div>
                            <div class="col-sm-12">
                            <div id="mySidenavR" class="sidenavR" style="display:none;">
<div class="filter-right">
<div class="filter-area">
<!--<h5>Vendor List Filter</h5>-->
    <a href="javascript:void(0)" class="closebtn" id="hide-filter">×</a>
        <div class="col-sm-2">
            <a class="btn btn-default btn-select">
            	<input type="hidden" class="btn-select-input" id="" name="" value="" />
            	<span class="btn-select-value">Select an Item</span>
            	<span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                <ul>
                    <li class="selected">PO Number</li>
                    <li>PO/001233/16-17</li>
                    <li>PO/001895/16-17</li>
                    <li>PO/001008/16-17</li>
                </ul>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-2">
            <a class="btn btn-default btn-select">
            	<input type="hidden" class="btn-select-input" id="" name="" value="" />
            	<span class="btn-select-value">Select an Item</span>
            	<span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                <ul>
                    <li class="selected">Category 1</li>
                    <li>----</li>
                    <li>----</li>
                    <li>----</li>
                </ul>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-2">
            <a class="btn btn-default btn-select">
            	<input type="hidden" class="btn-select-input" id="" name="" value="" />
            	<span class="btn-select-value">Select an Item</span>
            	<span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                <ul>
                    <li class="selected">Category 2</li>
                    <li>----</li>
                    <li>----</li>
                    <li>----</li>
                </ul>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-2">
        <div class="form-group">
                <div class="input-group date">
                    <input type="text" class="form-control" placeholder="Due From"  id="example1"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-2">
        <div class="form-group">
                <div class="input-group date">
                    <input type="text" class="form-control" placeholder="Due To"  id="example2"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-2">
        	<a href="javascript:void(0)" class="btn btn-success">Apply Filter</a>
            <div class="clearfix"></div>
        </div>
<div class="clearfix"></div>        
	</div>
    <div class="clearfix"></div>
    </div>    
</div>
                            
                            </div>
                            
                            
                            </div>
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">PO. No</th>
                                                <th class="text-center">PO. Date</th>
                                                <th class="text-center">Cat 1</th>
                                                <th class="text-center">Cat 2</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Cat 4</th>
                                                <th class="text-center">Price</th>
                                                <th class="text-center">Order Qty</th>
                                                <th class="text-center">Received Qty</th>
                                                <th class="text-center">Cancelled Qty</th>
                                                <th class="text-center">GIT Qty</th>
                                                <th class="text-center">Pending Qty</th>
                                                <th class="last-width text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <?php $inc=$purchaseOrderList->perPage() * ($purchaseOrderList->currentPage()-1);; ?>
                                    @foreach($purchaseOrderList as $eachOrder)
                                    <?php $inc++; ?>
                                    <tr>
                                        <td class="text-center po-no<?php echo $inc;?>">{{$eachOrder->ORDNO}}</td>
                                        <td class="text-center po-date<?php echo $inc;?>">{{date('d/m/Y',strtotime($eachOrder->ORDDT))}}</td>
                                        <td class="text-center cat-1<?php echo $inc;?>">{{$eachOrder->cat1}}</td>
                                        <td class="text-center cat-2<?php echo $inc;?>">{{$eachOrder->cat2}}</td>
                                        <td class="text-center cat-3<?php echo $inc;?>">{{$eachOrder->cat3}}</td>
                                        <td class="text-center cat-4<?php echo $inc;?>">{{$eachOrder->cat3}}</td>
                                        <td class="text-center price<?php echo $inc;?>">{{$eachOrder->RATE}}</td>
                                        <td class="text-center order-qty<?php echo $inc;?>">{{$eachOrder->ORDQTY}}</td>
                                        <td class="text-center received-qty<?php echo $inc;?>">{{$eachOrder->RCQTY}}</td>
                                        <td class="text-center cancelled-qty<?php echo $inc;?>">{{$eachOrder->CNLQTY}}</td>
                                        <td class="text-center git-qty<?php echo $inc;?>">00</td>
                                        <td class="text-center pending-qty<?php echo $inc;?>">00.00</td>
                                        <td class="last-width text-center">
                                       <!--  <select id="action_type" name="action_type" class="select-drop" onchange="return actionType(this.value,'<?php echo $inc;?>');">
                                        <option value="">Select Type</option>
                                        <option value="vendorChangecutting">CUTTING REQ</option>
                                        <option value="vendorChangeprice">RATE CHANGE</option>
                                        <option value="vendorChangewidth">WIDTH CHANGE</option>
                                        <option value="vendorChangedate">DATE EXT</option>
                                        <option value="vendorCancellation">CANCEL</option>
                                        <option value="vendorAddremarks">OTHERS</option>
                                        </select> -->
 <span data-toggle="modal" data-target="#vendorChangecutting">
<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Request for Cutting" class="tab-btn vendorChangecutting" unique-key="1">
CUTTING REQ
</a>
<!--</span>
<span data-toggle="modal" data-target="#vendorChangeprice">
<a href="javascript:void(0)" data-toggle="tooltip" title="Request for Rate Changes" class="tab-btn vendorChangeprice" unique-key="1">RATE CHANGE</a>
</span>
<span data-toggle="modal" data-target="#vendorChangewidth">
<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Request for Width Changes" class="tab-btn vendorChangewidth" unique-key="1">WIDTH CHANGE</a>
</span>
<span data-toggle="modal" data-target="#vendorChangedate">
<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Request for Date Extension" class="tab-btn vendorChangedate" unique-key="1">DATE EXT</a>
</span>
<span data-toggle="modal" data-target="#vendorCancellation">
<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Request for Cancellation" class="tab-btn vendorCancellation" unique-key="1">CANCEL</a>
</span>
<span data-toggle="modal" data-target="#vendorAddremarks">
<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Add Remarks" class="tab-btn vendorAddremarks" unique-key="1">OTHERS</a></span> -->
                                    </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                              <div class="col-sm-5">
                                <div class="dataTables_info" role="status" aria-live="polite">Showing {{$purchaseOrderList->count()}} of {{$purchaseOrderList->total()}} entries</div>
                              </div>
                              <div class="col-sm-7">
                                    <nav aria-label="Page navigation" class="pull-right">
                                        {{ $purchaseOrderList->links() }}
                                    </nav>
                                  </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@include('layouts.footer')
<!--add vendorChangecutting Modal Start-->
<div id="vendorChangecutting" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Cutting</h4>
      </div>
      <div class="modal-body vendorChangecutting-modal-body">
        <div class="row shorting-area pricechange">
            <input type="hidden" name="type" id="cutting_type" value="cutting" />
        	<div class="col-sm-3"><h5>Cat 1 : <strong class="vendorChangecutting-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Cat 2 : <strong class="vendorChangecutting-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Cat 4 : <strong class="vendorChangecutting-cat4"><!--45"--></strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorChangecutting-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>Date : <strong class="vendorChangecutting-date"><!--12/10/2016--></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorChangecutting-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorChangecutting-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorChangecutting-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorChangecutting-pending-qty"><!--1000--></strong></h5></div>
            <div class="clearfix"></div>
            <hr>
        	<div class="clearfix"></div>
            <div class="col-sm-12">
            <h5>Remarks :</h5>
            <textarea class="form-control" id="vendorChangecutting_remarks" name="vendorChangecutting_remarks" placeholder="Remarks"></textarea>
            <span id="vendorChangecutting_remarks_message"></span>
            </div>
            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorChangecutting">Request Submit</a>
            </div>
            <div class="col-sm-6">
            <a href="{{route('purchase_order_list')}}" class="pup-btn">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--add vendorChangecutting  Modal End-->


<!--vendorChangeprice Modal Start-->
<div id="vendorChangeprice" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Rate Changes</h4>
      </div>
      <div class="modal-body vendorChangecutting-modal-body">
        <div class="row shorting-area pricechange">
        <input type="hidden" name="type" id="rate_change_type" value="rate_change" />            
            <div class="col-sm-3"><h5>Cat 1 : <strong class="vendorChangeprice-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Cat 2 : <strong class="vendorChangeprice-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Cat 4 : <strong class="vendorChangeprice-cat4"><!--45"--></strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorChangeprice-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorChangeprice-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorChangeprice-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorChangeprice-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorChangeprice-pending-qty"><!--1000--></strong></h5></div>
            <div class="col-sm-3"><h5>Current rate : <strong class="vendorChangeprice-current-rate"><!--300--></strong></h5></div>
            
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
        	<div class="col-sm-6">
            <h5>New requested rate :</h5>
            <input type="number" placeholder="" class="form-control" id="vendorChangecutting_new_requested_rate" name="vendorChangecutting_new_requested_rate"/>
            <span id="vendorChangeRate_remarks_message"></span>
            </div>
            <div class="col-sm-6">
            <h5>Reason for change in rate :</h5>
            <textarea class="form-control" id="vendorChangecutting_reason" name="vendorChangecutting_reason"></textarea>
             <span id="vendorChangeRateReason_remarks_message"></span>
            </div>

            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorChangeprice">Request Submit</a>
            </div>
        <div class="col-sm-6">
            <a href="{{route('purchase_order_list')}}" class="pup-btn">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorChangeprice  Modal End-->


<!--vendorChangewidth Modal Start-->
<div id="vendorChangewidth" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Width Changes</h4>
      </div>
      <div class="modal-body vendorChangecutting-modal-body">
        <div class="row shorting-area pricechange">
        <input type="hidden" name="type" id="width_change_type" value="width_change" />
        	<div class="col-sm-3"><h5>Cat 1 : <strong class="vendorChangewidth-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Cat 2 : <strong class="vendorChangewidth-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Cat 4 : <strong class="vendorChangewidth-cat4"><!--45"--></strong></h5></div>
            
            
             <div class="col-sm-3"><h5>PO. No : <strong class="vendorChangewidth-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorChangewidth-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorChangewidth-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorChangewidth-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorChangewidth-pending-qty"><!--1000--></strong></h5></div>
            <div class="col-sm-3"><h5>Current rate : <strong class="vendorChangewidth-current-rate"><!--37--></strong></h5></div>
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
        	<div class="col-sm-6">
            <h5>New requested rate :</h5>
            <input type="number" placeholder="" class="form-control" id="vendorChangewidth_new_requested_rate" name="vendorChangewidth_new_requested_rate"/>
            <span id="vendorNewReqrate_remarks_message"></span>
            </div>
            <div class="col-sm-6">
            <h5>New Width :</h5>
            <input type="number" placeholder="" class="form-control" id="vendorChangewidth_new_width" name="vendorChangewidth_new_width"/>
            <span id="vendorChangecuttingwidth_remarks_message"></span>
            </div><div class="clearfix"></div>
            <div class="col-sm-12">
            <h5>Reason for change in Width :</h5>
            <textarea class="form-control" id="vendorChangewidth_reason" name="vendorChangewidth_reason"></textarea>
            <span id="vendorChangecutting_width_message"></span>
            </div>
            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorChangewidth">Request Submit</a>
            </div>
        <div class="col-sm-6">
            <a href="{{route('purchase_order_list')}}" class="pup-btn">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorChangewidth  Modal End-->


<!--vendorChangedate Modal Start-->
<div id="vendorChangedate" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Due Date extension</h4>
      </div>
      <div class="modal-body vendorChangecutting-modal-body">
        <div class="row shorting-area pricechange">
           <input type="hidden" name="type" id="date_ext_type" value="date_ext" /> 
            
            <div class="col-sm-3"><h5>Cat 1 : <strong class="vendorChangedate-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Cat 2 : <strong class="vendorChangedate-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Cat 4 : <strong class="vendorChangedate-cat4"><!--45"--></strong></h5></div>
            
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorChangedate-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorChangedate-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorChangedate-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorChangedate-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorChangedate-pending-qty"><!--1000--></strong></h5></div>
            <div class="col-sm-3"><h5>Current rate : <strong class="vendorChangedate-current-rate"><!--37--></strong></h5></div>
            <div class="col-sm-4"><h5>Order Date : <strong class="vendorChangedate-order-date"><!--23/09/2016--></strong></h5></div>
            
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
        <div class="col-sm-6">
            <h5>Current Item Due Date :</h5>
            <h4 style="text-align:left"><strong class="vendorChangedate-due-date"><!--30/10/2016--></strong></h4>
            </div>
        	<div class="col-sm-6">
            <h5>Extended Date :</h5>
            <div class="form-group">
                <div class="input-group date">
                    <input type="text" class="form-control" placeholder="14/11/2016" id="vendorChangedate_extended_date" name="vendorChangedate_extended_date"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    
                </div>
                <div class="clearfix"></div>
                <span id="vendorChangecuttingdate_remarks_message"></span>
            </div>
            </div>
            
            <div class="col-sm-12">
            <h5>Reason for Extension :</h5>
            <textarea class="form-control" id="vendorChangedate_reason" name="vendorChangedate_reason"></textarea>
            <span id="vendorChangecuttingDate_remarks_message"></span>
            </div>
            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorChangedate">Request Submit</a>
            </div>
        <div class="col-sm-6">
            <a href="{{route('purchase_order_list')}}" class="pup-btn">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorChangedate  Modal End-->


<!--vendorCancellation order Modal Start-->
<div id="vendorCancellation" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Cancellation</h4>
      </div>
      <div class="modal-body vendorChangecutting-modal-body">
        <div class="row shorting-area pricechange">
        <input type="hidden" name="type" id="cancel_type" value="cancel" />             
            <div class="col-sm-3"><h5>Cat 1 : <strong class="vendorCancellation-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Cat 2 : <strong class="vendorCancellation-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Cat 4 : <strong class="vendorCancellation-cat4"><!--45"--></strong></h5></div>
            
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorCancellation-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorCancellation-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorCancellation-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorCancellation-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorCancellation-pending-qty"><!--1000--></strong></h5></div>            
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
        	<div class="col-sm-6">
            <h5>Cancel Qty :</h5>
            <input type="number" placeholder="" class="form-control" id="vendorCancellation_number" name="vendorCancellation_number"/>
             <span id="vendorChangecuttingcancel_remarks_message"></span>
            </div>
            <div class="col-sm-6">
            <h5>Reason for Cancellation :</h5>
            <textarea class="form-control" id="vendorCancellation_reason" name="vendorCancellation_reason"></textarea>
            <span id="vendorChangecuttingCancel_remarks_message"></span>
            </div>
            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorCancellation">Request Submit</a>
            </div>
        <div class="col-sm-6">
            <a href="{{route('purchase_order_list')}}" class="pup-btn">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorCancellation order  Modal End-->




<!--vendorAddremarks Modal Start-->
<div id="vendorAddremarks" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Remarks</h4>
      </div>
      <div class="modal-body vendorAddremarks-modal-body">
        <div class="row shorting-area pricechange">
         <input type="hidden" name="type" id="others_type" value="others" />
        	<div class="col-sm-3"><h5>Cat 1 : <strong class="vendorAddremarks-cat1"><!--F-635--></strong></h5></div>
            <div class="col-sm-3"><h5>Cat 2 : <strong class="vendorAddremarks-cat2"><!--307--></strong></h5></div>
            <div class="col-sm-3"><h5>Cat 4 : <strong class="vendorAddremarks-cat4"><!--45"--></strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorAddremarks-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>Date : <strong class="vendorAddremarks-date"><!--12/10/2016--></strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong class="vendorAddremarks-order-qty"><!--3000--></strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong class="vendorAddremarks-received-qty"><!--2000--></strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong class="vendorAddremarks-git-qty"><!--0--></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorAddremarks-pending-qty"><!--1000--></strong></h5></div> 
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix vendorAddremarks-modal-body-clearfix"></div>
            <div class="col-sm-12">
            <h5>Add Remarks :</h5>
            <textarea class="form-control" id="vendorAddremarks_remarks" name="vendorAddremarks_remarks"></textarea>
            <span id="vendorOthers_remarks_message"></span>
            </div>
            <span id="vendorAddremarks_remarks_message"></span>
            <div class="clearfix vendorChangecutting-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorAddremarks">Request Submit</a>
            </div>
        <div class="col-sm-6">
            <a href="{{route('purchase_order_list')}}" class="pup-btn">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorAddremarks  Modal End-->  
<script>
var ptoken='{{csrf_token()}}';
var cutting_url='{{route("purchase-order-request")}}';
</script>
<script src="{{asset('js/custom/admin_purchase_order.js')}}"></script>    