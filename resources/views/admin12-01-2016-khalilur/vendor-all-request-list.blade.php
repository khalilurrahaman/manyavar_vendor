@include('layouts.header')
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                              <div class="row">
                                  <div class="col-sm-4"><h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i> View All Request</h3></div>
                                    <div class="col-sm-8">
                                    
                                    <div class="shorting-area">
                              
                              <!-- <div class="col-sm-3 pull-right select-full">
                                        <a class="btn btn-default btn-select">
                                            <input type="hidden" class="btn-select-input" id="" name="" value="" />
                                            <span class="btn-select-value">Select an Item</span>
                                            <span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                                          <ul>
                                                <li class="selected">Division...</li>
                                               
                                          </ul>
                                        </a>
                                </div> -->                                               
                                
                                <div class="col-sm-3 pull-right select-full">
                                        <select id="req_status" name="req_status" class="select-drop" onchange="return filterByType()">
                                           <option value="">Select Status</option>
                                           <option value="new">New</option>
                                           <option value="close">Close</option>
                                           <option value="accept">Accepted</option>
                                           <option value="reject">Rejected</option>

                                        </select>
                                </div>
                                <div class="col-sm-3 pull-right select-full">
                                        <select id="req_type" name="req_type" class="select-drop" onchange="return filterByType()">
                                            <option value="">Select Type</option>
                                            <option value="cutting">Cutting</option>
                                            <option value="rate_change">Rate Change</option>
                                            <option value="width_change">Width Change</option>
                                            <option value="date_ext">Date Extention</option>
                                            <option value="cancel">Cancel</option>
                                            <option value="others">Others</option>
                                            <option value="sample">Sample</option>
                                        </select>
                                </div>
                                <div class="col-sm-3 pull-right select-full">
                                        <select id="req_po_no" name="req_po_no" class="select-drop" onchange="return filterByType()">
                                           <option value="">Select PO</option>
                                          @if(!empty($po_list))
                                          @foreach($po_list as $req)
                                           <option value="{{$req->po_no}}">{{$req->po_no}}</option>
                                          @endforeach
                                          @endif 
                                        </select>
                                </div>                          
                            </div>                                
                          </div>
                        </div>
                            </div>
                            <div class="clearfix"></div>
                              <span id="filterResult">
                            <div class="panel-body custome-table">
                            
                                <div class="table-responsive view-table">

                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Sr. No</th>
                                                <th class="text-center">PO No</th>
                                                <th class="text-center">Date</th>
                                                <th class="text-center">Type of Request</th>
                                                <th class="text-center">Vendor Name</th>
                                                <th class="text-center">Old Value</th>
                                                <th class="text-center">Requested Vaule</th>
                                                <th class="text-center">Accepted Value</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-center">Last Comments</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody id="req_request">
                                    @if(!empty($request_list))
                                    <?php $inc=$request_list->perPage() * ($request_list->currentPage()-1);;?>
                                    @foreach($request_list as $req)
                                    <?php $inc++; ?>
                                    <tr>
                                      <td class="text-center">{{$inc}}</td>
                                      <td class="text-center po-no<?php echo $inc;?>">{{$req->po_no}}</td>
                                      <td class="text-center po-date<?php echo $inc;?>">{{date("d/m/y",strtotime($req->created_at))}}</td>
                                        <td class="text-center">
                                        <?php 
                                         $type='';
                                         if($req->action_type=='cutting')
                                          $type='Request For Cutting';
                                          elseif($req->action_type=='rate_change') 
                                           $type='Request For Rate Change';
                                          elseif($req->action_type=='width_change') 
                                           $type='Request For Width Change';
                                          elseif($req->action_type=='date_ext') 
                                           $type='Request For Date Extention';
                                          elseif($req->action_type=='cancel') 
                                           $type='Request For Cancellation';
                                          elseif($req->action_type=='others') 
                                           $type='Request For Others';
                                          elseif($req->action_type=='sample') 
                                           $type='Request For Sample';
                                        ?>
                                        {{$type}}
                                        </td>
                                        <td class="text-center">@if(!empty($req->ven_name)){{$req->ven_name}}@endif</td>
                                        <td class="text-center">@if(!empty($req->old_value)){{$req->old_value}}@endif</td>
                                        <td class="text-center received-qty<?php echo $inc;?>">@if(!empty($req->new_rate)){{$req->new_rate}}@endif</td>
                                        <td class="text-center">@if(!empty($req->accepted_rate)){{$req->accepted_rate}}@endif</td>
                                        <td class="text-center">
                                       
                                       @if($req->status=='new')
                                       <span class="sm-btn btn-primary btn-xs">New</span>
                                       @elseif($req->status=='close')
                                       <span class="sm-btn btn-primary btn-warning">Closed</span>
                                       @elseif($req->status=='accept')
                                       <span class="sm-btn btn-primary btn-warning">Accepted</span>
                                       @elseif($req->status=='reject')
                                       <span class="sm-btn btn-primary btn-danger">Rejected</span> 
                                       @endif
                                        </td>
                                        <td class="text-left width-e">
                                        @if(!empty($req->req_comments) && count(($req->req_comments))>0)
                                        <?php                                       
                                       
                                        if(strlen($req->req_comments[0]->comments)<=20)                                 
                                           echo ($req->req_comments[0]->comments);
                                        else
                                         echo substr($req->req_comments[0]->comments,0,18).'..';  

                                        ?>
                                        <a href="{{route('admin-requestwise-comments', $req->id)}}" class="pull-right btn btn-warning btn-xs">
                                        <?php echo count($req->req_comments)?>
                                        Comment(s)</a>
                                        @else
                                        <a href="{{route('admin-requestwise-comments', $req->id)}}" class="btn btn-warning btn-xs pull-right">
                                        
                                        Comments</a>
                                        
                                         @endif
                                         </td>
                                        <td class="text-center">
                                        @if($req->status=='new')
                                        <a data-toggle="modal" data-target="#vendorRequestaccept" data-placement="top" title="" class="btn btn-success vendorRequestaccept" unique-key="<?php echo $inc;?>" action_id="{{$req->id}}" action_type="{{$req->action_type}}">Accept</a>
                                        <a data-toggle="modal" data-target="#vendorRequestreject" data-placement="top" title="" class="btn btn-danger vendorRequestreject" unique-key="<?php echo $inc;?>" action_id="{{$req->id}}" action_type="{{$req->action_type}}">Reject</a>
                                        @elseif($req->status=='accept')
                                        <span class="sm-btn btn-primary btn-warning">Accepted</span>
                                        @elseif($req->status=='reject')
                                        <span class="sm-btn btn-primary btn-danger">Rejected</span> 
                                        @elseif($req->status=='close')
                                        <span class="sm-btn btn-primary btn-warning">Closed</span> 
                                        @endif
                                        </td>

                                    </tr>
                                    @endforeach
                                    @endif 
                                     
                                </tbody> 
                               </table>

                              </div>
                              <div class="row">
                              <div class="col-sm-6">
                                <div class="dataTables_info" role="status" aria-live="polite">Showing {{$request_list->count()}} of {{$request_list->total()}} entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                      {{ $request_list->links() }}   
                                    </nav>
                                  </div>
                              </div>
                            </div>
                            </span>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

<!--vendorRequestaccept Modal Start-->
<div id="vendorRequestaccept" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request Accept</h4>
      </div>
      <div class="modal-body vendorRequestaccept-modal-body">
        <div class="row shorting-area pricechange">
        <input type="hidden" name="type" id="type" value="" />
         <input type="hidden" name="action_id" id="action_id" value="" />            
            <!-- <div class="col-sm-3"><h5>Cat 1 : <strong class="vendorRequestaccept-cat1"></strong></h5></div>
            <div class="col-sm-3"><h5>Cat 2 : <strong class="vendorRequestaccept-cat2"></strong></h5></div>
            <div class="col-sm-3"><h5>Cat 4 : <strong class="vendorRequestaccept-cat4"></strong></h5></div>
             -->
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorRequestaccept-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>Date : <strong class="vendorRequestaccept-date"><!--3000--></strong></h5></div>
            <!-- <div class="col-sm-3"><h5>Old Rate : <strong class="vendorRequestaccept-received-qty"></strong></h5></div> -->

            
            <div class="col-sm-3"><h5>Requested Rate : <strong class="vendorRequestaccept-received-qty"></strong></h5></div>
             <div class="clearfix"></div>
           
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
        <div class="col-sm-6 newpo" id="newpo">
            <h5>New PO :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestaccept_new_po" name="vendorRequestaccept_new_po"/>            
            </div>
            
          <div class="col-sm-6 accept" id="accept">
            <h5>Accepted rate :</h5>
            <input type="text" placeholder="" onkeypress="javascript:return isNumber(event)" class="form-control" id="vendorRequestaccept_new_requested_rate" name="vendorRequestaccept_new_requested_rate"/>
            <span id="vendorRequestaccept_rate-remarks_message"></span>
            </div>
            <div class="col-sm-6 docet" id="docet">
            <h5>Docket No :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestaccept_new_docket" name="vendorRequestaccept_new_docket"/>
            <span id="vendorRequestaccept_docket-remarks_message"></span>
            </div>
            <div class="col-sm-6 prnno" id="prnno">
            <h5>PRN No :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestaccept_new_prn" name="vendorRequestaccept_new_prn"/>
            <span id="vendorRequestaccept_prn-remarks_message"></span>
            </div>
            <div class="col-sm-6 prndate" id="prndate">
            <h5>PRN Date :</h5>
           
            <div class="input-group date">
                     <input type="text" placeholder="14/11/2016" class="form-control" id="vendorRequestaccept_new_prndate" name="vendorRequestaccept_new_prndate" data-provide="datepicker"/>
                   <!--  <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                     -->
                </div>
            <span id="vendorRequestaccept_prndate-remarks_message"></span>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6 courier" id="courier">
            <h5>Courier Name :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestaccept_new_courier" name="vendorRequestaccept_new_courier"/>
            <span id="vendorRequestaccept_courier-remarks_message"></span>
            </div>
            <div class="col-sm-12">
            <h5>Remarks :</h5>
            <textarea class="form-control" id="vendorRequestaccept_reason" name="vendorRequestaccept_reason"></textarea>
             <span id="vendorRequestaccept_remarks_message"></span>
            </div>

            <div class="clearfix vendorRequestaccept-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorRequestaccept">Submit</a>
            </div>
        <div class="col-sm-6">
            <a href="javascript:void(0)" data-dismiss="modal"  class="pup-btn">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorChangeprice  Modal End-->


<!--vendorRequestReject Modal Start-->
<div id="vendorRequestreject" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request For Reject</h4>
      </div>
      <div class="modal-body vendorRequestreject-modal-body">
        <div class="row shorting-area pricechange">
        <input type="hidden" name="type" id="type" value="" />
         <input type="hidden" name="action_id" id="action_id" value="" />            
            <!-- <div class="col-sm-3"><h5>Cat 1 : <strong class="vendorRequestreject-cat1"></strong></h5></div>
            <div class="col-sm-3"><h5>Cat 2 : <strong class="vendorRequestreject-cat2"></strong></h5></div>
            <div class="col-sm-3"><h5>Cat 4 : <strong class="vendorRequestreject-cat4"></strong></h5></div>
             -->
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorRequestreject-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>Date : <strong class="vendorRequestreject-date"><!--3000--></strong></h5></div>
            <!-- <div class="col-sm-3"><h5>Old Rate : <strong class="vendorRequestreject-received-qty"></strong></h5></div> -->
            <div class="col-sm-3"><h5>Requested Rate : <strong class="vendorRequestreject-received-qty"></strong></h5></div>
            <!--<div class="col-sm-3"><h5>GIT Qty : <strong class="vendorRequestreject-git-qty"></strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong class="vendorRequestreject-pending-qty"></strong></h5></div>
            <div class="col-sm-3"><h5>Current rate : <strong class="vendorRequestreject-current-rate"></strong></h5></div> -->
            
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
         <!--  <div class="col-sm-6 accept" id="accept">
            <h5>Accepted rate :</h5>
            <input type="number" placeholder="" class="form-control" id="vendorRequestreject" name="vendorRequestreject"/>
            <span id="vendorRequestreject-remarks_message"></span>
            </div> -->
            <div class="col-sm-12">
            <h5>Reason For Reject :</h5>
            <textarea class="form-control" id="vendorRequestreject_reason" name="vendorRequestreject_reason"></textarea>
             <span id="vendorRequestreject_remarks_message"></span>
            </div>

            <div class="clearfix vendorRequestreject-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorRequestreject">Submit</a>
            </div>
        <div class="col-sm-6">
            <a href="javascript:void(0)" data-dismiss="modal" class="pup-btn">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorRequestReject  Modal End-->
                                    
  <div class="loading">
  	<img src="{{asset('img/loading.gif')}}" alt="">  
  </div>                                  
                                    

@include('layouts.footer')
<script>
$('#vendorRequestaccept_new_prndate').on('changeDate', function(ev){
    $(this).datepicker('hide');
});

var ptoken='{{csrf_token()}}';
var cutting_url='{{route("accept-request")}}';
var rej_url='{{route("reject-request")}}';
</script>
<script src="{{asset('js/custom/admin_purchase_order.js')}}"></script>  
<script type="text/javascript">
$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        //$('#load a').css('color', '#dfecf6');
        //$('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');

        var url = $(this).attr('href'); 
        var page=$(this).attr('href').split('page=')[1]; 
    
    
        filterByType(page,2);
        //window.history.pushState("", "", url);
    });

    
});

    function filterByType(page=1,load=1)
    {
        //alert(type);
        if(load==1)
        $('.loading').show();
        var status=$('#req_status').val();
        var type=$('#req_type').val();
        var po_no=$('#req_po_no').val();
        $.ajax({
            type: 'GET', 
            url: "{{route('filter-action')}}",
            data: {type:type,po_no:po_no,status:status,page:page},
            success:function(data)
            {
                $('.loading').hide();  
                $('#filterResult').html(data);
                 
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
    }
</script>
<script>
    // WRITE THE VALIDATION SCRIPT IN THE HEAD TAG.
    function isNumber(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
            return false;

        return true;
    }    
</script>
