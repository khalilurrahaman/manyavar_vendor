<div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Name</th>
                                                <!-- <th class="text-center">Department</th>
                                                <th class="text-center">Section</th>
                                                <th class="text-center">Devision</th> -->
                                                <th class="text-center">E-mail ID</th>
                                                <th class="text-center">Phone No</th>
                                                <th class="text-center">Address</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-center">Action</th>
                                                
                                            </tr>
                                        </thead>
                                    <tbody>
                                    @if(count($vendor_list))
                                    @foreach($vendor_list as $data)
                                    <tr>
                                        <td class="text-center">{{$data->name}}</td>
                                       
                                        <td class="text-center">{{$data->email}}</td>
                                        <td class="text-center">{{$data->mobile}}</td>
                                        <td class="text-center">{{$data->address}}</td>
                                        <td class="text-center">
                                        @if($data->status==1)
                                        Active
                                        @else
                                        Dective
                                        @endif

                                        </td>
                                        <td class="text-center">
                                        <a onclick="" href="{{route('set-ven-password',$data->id)}}" class="btn btn-success">Set Password</a>
                                        @if($data->status==1)
                                        <a href="{{route('vendor-status-deact',$data->id)}}" onclick="return ConfirmDeactive()" class="btn btn-danger">Deactive</a>
                                        @else
                                        <a href="{{route('vendor-status-act',$data->id)}}" onclick="return ConfirmActive()" class="btn btn-success">Active</a>
                                        @endif

                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td>
                                            No record found
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    @endif
                                      
                                </tbody>
                               </table>
                              </div>
                              
                              <div class="row">
                              <div class="col-sm-6">
                                <div class="dataTables_info" role="status" aria-live="polite">Showing {{$vendor_list->count()}} of {{$vendor_list->total()}} entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                        {{ $vendor_list->links() }}
                                    </nav>
                                  </div>
                              </div>
                              
<script type="text/javascript">
$( "#hide-filter" ).click(function() {
    
    $("#name").val('');
    $("#email").val('');
   
    
    filter(); 
});

        </script>