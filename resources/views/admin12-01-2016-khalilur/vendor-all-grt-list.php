@include('layouts.header')
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <!--<div class="row">
                    <div class="col-lg-12 title-top">
                    <div class="row page-header">
                    	<div class="col-sm-6">
                        	<h1>Admin <small>All GRT List</small></h1>
                        </div>
                        <div class="col-sm-6">
                        	<a href="vendor-add-git.php" class="btn btn-success">Add GIT</a>
                        </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i> All GRT List</h3>
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                                <div class="table-responsive view-table">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Vendor Name</th>
                                                <th class="text-center">GRT No</th>
                                                <th class="text-center">GRT Date</th>
                                                <th class="text-center">Receive Qty</th>
                                                <th class="text-center">GIT ID</th>
                                                <th class="text-center">GIT Date</th>
                                                <th class="text-center width-lastB">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <tr>
                                    	<td class="text-center">Himika Chanda</td>
                                        <td class="text-center">GR/215637</td>
                                    	<td class="text-center">26/09/2016</td>
                                        <td class="text-center">36.00</td>
                                        <td class="text-center">GI/326589</td>
                                        <td class="text-center">21/09/2016</td>
                                        <td class="text-center width-lastB">
                                         <a href="javascript:void(0)" class="btn btn-success">EDIT / ATTACH GIT</a>
                                         </td>
                                    </tr>
                                     <tr>
                                    	<td class="text-center">Abhimanuya Sharma</td>
                                    	<td class="text-center">GR/365489</td>
                                    	<td class="text-center">22/09/2016</td>
                                        <td class="text-center">36.00</td>
                                        <td class="text-center">---</td>
                                        <td class="text-center">---</td>
                                        <td class="text-center width-lastB">
                                         <a href="javascript:void(0)" class="btn btn-success">EDIT / ATTACH GIT</a>
                                         </td>
                                    </tr>
                                    <tr>
                                    	<td class="text-center">Arjun Paul</td>
                                    	<td class="text-center">GR/215637</td>
                                    	<td class="text-center">26/09/2016</td>
                                        <td class="text-center">36.00</td>
                                        <td class="text-center">---</td>
                                        <td class="text-center">---</td>
                                        <td class="text-center width-lastB">
                                         <a href="javascript:void(0)" class="btn btn-success">EDIT / ATTACH GIT</a>
                                         </td>
                                    </tr>
                                    <tr>
                                    	<td class="text-center">Binoy Dasgupta</td>
                                    	<td class="text-center">GR/215637</td>
                                    	<td class="text-center">26/09/2016</td>
                                        <td class="text-center">36.00</td>
                                        <td class="text-center">GI/326589</td>
                                        <td class="text-center">21/09/2016</td>
                                        <td class="text-center width-lastB">
                                         <a href="javascript:void(0)" class="btn btn-success">EDIT / ATTACH GIT</a>
                                         </td>
                                    </tr>
                                    <tr>
                                    	<td class="text-center">Gajendra Yadav</td>
                                    	<td class="text-center">GR/215637</td>
                                    	<td class="text-center">26/09/2016</td>
                                        <td class="text-center">36.00</td>
                                        <td class="text-center">GI/326589</td>
                                        <td class="text-center">21/09/2016</td>
                                        <td class="text-center width-lastB">
                                         <a href="javascript:void(0)" class="btn btn-success">EDIT / ATTACH GIT</a>
                                         </td>
                                    </tr>
                                     <tr>
                                    	<td class="text-center">Chandrakanth Jana</td>
                                    	<td class="text-center">GR/215637</td>
                                    	<td class="text-center">26/09/2016</td>
                                        <td class="text-center">36.00</td>
                                        <td class="text-center">GI/326589</td>
                                        <td class="text-center">21/09/2016</td>
                                        <td class="text-center width-lastB">
                                         <a href="javascript:void(0)" class="btn btn-success">EDIT / ATTACH GIT</a>
                                         </td>
                                    </tr>
                                    <tr>
                                    	<td class="text-center">Umesh Sau</td>
                                    	<td class="text-center">GR/215637</td>
                                    	<td class="text-center">26/09/2016</td>
                                        <td class="text-center">36.00</td>
                                        <td class="text-center">---</td>
                                        <td class="text-center">---</td>
                                        <td class="text-center width-lastB">
                                         <a href="javascript:void(0)" class="btn btn-success">EDIT / ATTACH GIT</a>
                                         </td>
                                    </tr>
                                    <tr>
                                    	<td class="text-center">Srikant Sen</td>
                                    	<td class="text-center">GR/215637</td>
                                    	<td class="text-center">26/09/2016</td>
                                        <td class="text-center">36.00</td>
                                        <td class="text-center">GI/326589</td>
                                        <td class="text-center">21/09/2016</td>
                                        <td class="text-center width-lastB">
                                         <a href="javascript:void(0)" class="btn btn-success">EDIT / ATTACH GIT</a>
                                         </td>
                                    </tr>
                                    <tr>
                                    	<td class="text-center">Himika Chanda</td>
                                        <td class="text-center">GR/215637</td>
                                    	<td class="text-center">26/09/2016</td>
                                        <td class="text-center">36.00</td>
                                        <td class="text-center">GI/326589</td>
                                        <td class="text-center">21/09/2016</td>
                                        <td class="text-center width-lastB">
                                         <a href="javascript:void(0)" class="btn btn-success">EDIT / ATTACH GIT</a>
                                         </td>
                                    </tr>
                                     <tr>
                                    	<td class="text-center">Abhimanuya Sharma</td>
                                    	<td class="text-center">GR/365489</td>
                                    	<td class="text-center">22/09/2016</td>
                                        <td class="text-center">36.00</td>
                                        <td class="text-center">---</td>
                                        <td class="text-center">---</td>
                                        <td class="text-center width-lastB">
                                         <a href="javascript:void(0)" class="btn btn-success">EDIT / ATTACH GIT</a>
                                         </td>
                                    </tr>
                                    <tr>
                                    	<td class="text-center">Arjun Paul</td>
                                    	<td class="text-center">GR/215637</td>
                                    	<td class="text-center">26/09/2016</td>
                                        <td class="text-center">36.00</td>
                                        <td class="text-center">---</td>
                                        <td class="text-center">---</td>
                                        <td class="text-center width-lastB">
                                         <a href="javascript:void(0)" class="btn btn-success">EDIT / ATTACH GIT</a>
                                         </td>
                                    </tr>
                                    <tr>
                                    	<td class="text-center">Binoy Dasgupta</td>
                                    	<td class="text-center">GR/215637</td>
                                    	<td class="text-center">26/09/2016</td>
                                        <td class="text-center">36.00</td>
                                        <td class="text-center">GI/326589</td>
                                        <td class="text-center">21/09/2016</td>
                                        <td class="text-center width-lastB">
                                         <a href="javascript:void(0)" class="btn btn-success">EDIT / ATTACH GIT</a>
                                         </td>
                                    </tr>
                                    <tr>
                                    	<td class="text-center">Gajendra Yadav</td>
                                    	<td class="text-center">GR/215637</td>
                                    	<td class="text-center">26/09/2016</td>
                                        <td class="text-center">36.00</td>
                                        <td class="text-center">GI/326589</td>
                                        <td class="text-center">21/09/2016</td>
                                        <td class="text-center width-lastB">
                                         <a href="javascript:void(0)" class="btn btn-success">EDIT / ATTACH GIT</a>
                                         </td>
                                    </tr>
                                     <tr>
                                    	<td class="text-center">Chandrakanth Jana</td>
                                    	<td class="text-center">GR/215637</td>
                                    	<td class="text-center">26/09/2016</td>
                                        <td class="text-center">36.00</td>
                                        <td class="text-center">GI/326589</td>
                                        <td class="text-center">21/09/2016</td>
                                        <td class="text-center width-lastB">
                                         <a href="javascript:void(0)" class="btn btn-success">EDIT / ATTACH GIT</a>
                                         </td>
                                    </tr>
                                    <tr>
                                    	<td class="text-center">Umesh Sau</td>
                                    	<td class="text-center">GR/215637</td>
                                    	<td class="text-center">26/09/2016</td>
                                        <td class="text-center">36.00</td>
                                        <td class="text-center">---</td>
                                        <td class="text-center">---</td>
                                        <td class="text-center width-lastB">
                                         <a href="javascript:void(0)" class="btn btn-success">EDIT / ATTACH GIT</a>
                                         </td>
                                    </tr>
                                    <tr>
                                    	<td class="text-center">Srikant Sen</td>
                                    	<td class="text-center">GR/215637</td>
                                    	<td class="text-center">26/09/2016</td>
                                        <td class="text-center">36.00</td>
                                        <td class="text-center">GI/326589</td>
                                        <td class="text-center">21/09/2016</td>
                                        <td class="text-center width-lastB">
                                         <a href="javascript:void(0)" class="btn btn-success">EDIT / ATTACH GIT</a>
                                         </td>
                                    </tr>
                                    <tr>
                                    	<td class="text-center">Himika Chanda</td>
                                        <td class="text-center">GR/215637</td>
                                    	<td class="text-center">26/09/2016</td>
                                        <td class="text-center">36.00</td>
                                        <td class="text-center">GI/326589</td>
                                        <td class="text-center">21/09/2016</td>
                                        <td class="text-center width-lastB">
                                         <a href="javascript:void(0)" class="btn btn-success">EDIT / ATTACH GIT</a>
                                         </td>
                                    </tr>
                                     <tr>
                                    	<td class="text-center">Abhimanuya Sharma</td>
                                    	<td class="text-center">GR/365489</td>
                                    	<td class="text-center">22/09/2016</td>
                                        <td class="text-center">36.00</td>
                                        <td class="text-center">---</td>
                                        <td class="text-center">---</td>
                                        <td class="text-center width-lastB">
                                         <a href="javascript:void(0)" class="btn btn-success">EDIT / ATTACH GIT</a>
                                         </td>
                                    </tr>
                                    <tr>
                                    	<td class="text-center">Arjun Paul</td>
                                    	<td class="text-center">GR/215637</td>
                                    	<td class="text-center">26/09/2016</td>
                                        <td class="text-center">36.00</td>
                                        <td class="text-center">---</td>
                                        <td class="text-center">---</td>
                                        <td class="text-center width-lastB">
                                         <a href="javascript:void(0)" class="btn btn-success">EDIT / ATTACH GIT</a>
                                         </td>
                                    </tr>
                                    <tr>
                                    	<td class="text-center">Binoy Dasgupta</td>
                                    	<td class="text-center">GR/215637</td>
                                    	<td class="text-center">26/09/2016</td>
                                        <td class="text-center">36.00</td>
                                        <td class="text-center">GI/326589</td>
                                        <td class="text-center">21/09/2016</td>
                                        <td class="text-center width-lastB">
                                         <a href="javascript:void(0)" class="btn btn-success">EDIT / ATTACH GIT</a>
                                         </td>
                                    </tr>
                                    <tr>
                                    	<td class="text-center">Gajendra Yadav</td>
                                    	<td class="text-center">GR/215637</td>
                                    	<td class="text-center">26/09/2016</td>
                                        <td class="text-center">36.00</td>
                                        <td class="text-center">GI/326589</td>
                                        <td class="text-center">21/09/2016</td>
                                        <td class="text-center width-lastB">
                                         <a href="javascript:void(0)" class="btn btn-success">EDIT / ATTACH GIT</a>
                                         </td>
                                    </tr>
                                     <tr>
                                    	<td class="text-center">Chandrakanth Jana</td>
                                    	<td class="text-center">GR/215637</td>
                                    	<td class="text-center">26/09/2016</td>
                                        <td class="text-center">36.00</td>
                                        <td class="text-center">GI/326589</td>
                                        <td class="text-center">21/09/2016</td>
                                        <td class="text-center width-lastB">
                                         <a href="javascript:void(0)" class="btn btn-success">EDIT / ATTACH GIT</a>
                                         </td>
                                    </tr>
                                    <tr>
                                    	<td class="text-center">Umesh Sau</td>
                                    	<td class="text-center">GR/215637</td>
                                    	<td class="text-center">26/09/2016</td>
                                        <td class="text-center">36.00</td>
                                        <td class="text-center">---</td>
                                        <td class="text-center">---</td>
                                        <td class="text-center width-lastB">
                                         <a href="javascript:void(0)" class="btn btn-success">EDIT / ATTACH GIT</a>
                                         </td>
                                    </tr>
                                    <tr>
                                    	<td class="text-center">Srikant Sen</td>
                                    	<td class="text-center">GR/215637</td>
                                    	<td class="text-center">26/09/2016</td>
                                        <td class="text-center">36.00</td>
                                        <td class="text-center">GI/326589</td>
                                        <td class="text-center">21/09/2016</td>
                                        <td class="text-center width-lastB">
                                         <a href="javascript:void(0)" class="btn btn-success">EDIT / ATTACH GIT</a>
                                         </td>
                                    </tr>
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                              <div class="col-sm-6">
                              	<div class="dataTables_info" role="status" aria-live="polite">Showing 1 to 9 of 9 entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                        <ul class="pagination">
                                            <li class="page-item">
                                                <a class="page-link" href="javascript:void(0)" aria-label="Previous">
                                                	<span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span>
                                                </a>
                                            </li>
                                        	<li class="page-item active"><a class="page-link" href="javascript:void(0)">1</a></li>
                                       		<li class="page-item"><a class="page-link" href="javascript:void(0)">2</a></li>
                                        	<li class="page-item"><a class="page-link" href="javascript:void(0)">3</a></li>
                                        	<li class="page-item"><a class="page-link" href="javascript:void(0)">4</a></li>
                                        	<li class="page-item"><a class="page-link" href="javascript:void(0)">5</a></li>
                                        	<li class="page-item">
                                        	<a class="page-link" href="javascript:void(0)" aria-label="Next">
                                        		<span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span>
                                        	</a>
                                        	</li>
                                        </ul>
                                    </nav>
                                  </div>
                              </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@include('layouts.footer')