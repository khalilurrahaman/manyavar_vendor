@include('layouts.header')
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                	<div class="col-sm-6"><h3 class="panel-title">
                                    <i class="fa fa-users" aria-hidden="true"></i>User Listing</h3></div>
                                    <div class="col-sm-6">
                                    
                                    <div class="shorting-area">
                            		
                              
                                <div class="col-sm-3 pull-right">
                                        <a href="{{route('ad-user')}}" class="btn btn-success">
                                        <i class="fa fa-plus" aria-hidden="true"></i> Add New</a>
                                </div>
                            </div>
                                    
                                    
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Name</th>
                                                <th class="text-center">E-mail ID</th>
                                                <th class="text-center">Phone No</th>
                                                <th class="text-center">Address</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    @foreach($userList as $eachUser)
                                     
                                    <tr>
                                        <td class="text-center">{{$eachUser->name}}</td>
                                        <td class="text-center">{{$eachUser->email}}</td>
                                        <td class="text-center">{{$eachUser->mobile}}</td>
                                        <td class="text-center">{{$eachUser->address}}</td>
                                        <td class="text-center">
                                    <a href="{{route('admin-edit-user',$eachUser->id)}}" class="btn btn-primary">Edit</a>&nbsp<a href="{{route('admin-del-user',$eachUser->id)}}" onclick="return deleteConfirm()" class="btn btn-danger">Delete</a>&nbsp @if($eachUser->status==1)<a href="{{route('user-archieve',$eachUser->id)}}" class="btn btn-warning">Deactivate</a>@else<a href="{{route('users-activate',$eachUser->id)}}" class="btn btn-success">Activate</a>@endif</td>
                                    </tr>
                                    @endforeach                            
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                                  <div class="col-sm-6">
                                    <div class="dataTables_info" role="status" aria-live="polite">Showing {{$userList->count()}} of {{$userList->total()}} entries</div>
                                  </div>
                                  <div class="col-sm-6">
                                        <nav aria-label="Page navigation" class="pull-right">
                                            {{ $userList->links() }}
                                        </nav>
                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
        <!--add user popup Modal Start-->
<div id="adduser" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add New User</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
            <div class="col-sm-6">
                <h5>Name :</h5>
                <input type="text" class="form-control">
            </div>
            <div class="col-sm-6">
                <h5>Department :</h5>
                <div class="shorting-area">
                              <div class="select-full">
                                        <a class="btn btn-default btn-select">
                                            <input type="hidden" class="btn-select-input" id="" name="" value="" />
                                            <span class="btn-select-value">Select an Item</span>
                                            <span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                                            <ul>
                                                <li class="selected">Select...</li>
                                                <li>Lorem Ipsum 1</li>
                                                <li>Lorem Ipsum 2</li>
                                            </ul>
                                        </a>
                                </div>
                            </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <h5>Email ID :</h5>
                <input type="text" class="form-control">
            </div>
            <div class="col-sm-6">
                <h5>Phone No :</h5>
                <input type="text" class="form-control">
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <h5>Address :</h5>
            <textarea class="form-control">... </textarea>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="pup-btn">Add User</a>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<script type="text/javascript">
function deleteConfirm()
{
    if(confirm('Are you sure'))
    {
        return true;
    }
    else
    {
        return false;
    }
}
</script>
<!--add user popup  Modal End-->
@include('layouts.footer')