
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Sr. No</th>
                                                <th class="text-center">Image</th>
                                                <th class="text-center">Category 1</th>
                                                <th class="text-center">Category 2</th>
                                                <th class="text-center">Category 3</th>
                                                <th class="text-center">Category 4</th>                                        
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <?php $inc=$data->perPage() * ($data->currentPage()-1);; ?>
                                    @foreach($data as $eachData)
                                     <?php $inc++; ?>
                                     
                                    <tr>
                                     <td class="text-center">{{$inc}}</td>
                                     <td class="text-center">
                                        @if($eachData->image!='')
                                        <img src="{{URL::to('/upload/product').'/'.$eachData->image }}"/ style="height:60px;width: 60px;">
                                        @endif
                                        </td>
                                        <td class="text-center">{{$eachData->cat1}}</td>
                                        <td class="text-center">{{$eachData->cat2}}</td>
                                        <td class="text-center">{{$eachData->cat3}}</td>
                                        <td class="text-center">{{$eachData->cat4}}</td>
                                       
                                        <td class="text-center">                                        
                                        
                                        <a href="{{route('item-master-edit',$eachData->id)}}"  class="btn btn-success">Edit</a>
                                        <a class="btn btn-success mergeData" data-target="#selectpro" data-toggle="modal" merge-with="{{$eachData->id}}">Merge</a>
                                        </td>
                                    </tr>
                                    @endforeach                            
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                              <div class="col-sm-5">
                              	<div class="dataTables_info" role="status" aria-live="polite">Showing {{$data->count()}} of {{$data->total()}} entries</div>
                              </div>
                              <div class="col-sm-7">
                                    <nav aria-label="Page navigation" class="pull-right">
                                        {{ $data->links() }}
                                    </nav>
                                  </div>
                              </div>
                              </span>