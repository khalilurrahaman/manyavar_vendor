@include('layouts.header')
    <div id="page-wrapper">
        <div class="container-fluid inner-body-area">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default add-padding">
                        <div class="panel-heading">
                            <div class="row">
                            	<div class="col-sm-6"><h3 class="panel-title">
                                <i class="fa fa-users" aria-hidden="true"></i> Goods Returned List</h3></div>
                                <div class="col-sm-6">
                                    <div class="shorting-area">                          
                                                                
                                        <!-- <div class="col-sm-3 pull-right">
                                            <a href="{{route('goods-returns-add-form')}}" data-toggle="modal" class="btn btn-success">
                                            <i class="fa fa-plus" aria-hidden="true"></i> Add Returns</a>
                                        </div> -->
                                        <div class="col-sm-3 pull-right">
                                            <a href="{{route('goods-returns-list')}}" data-toggle="modal" class="btn btn-success">
                                            <i  aria-hidden="true"></i> Pending Returns</a>
                                        </div> 
                                    </div>
                                </div>
                            </div>                            
                        </div>
                        <div class="clearfix"></div>
                        <div class="panel-body custome-table">
                            <div class="table-responsive ">
                                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <!-- <th class="text-center">Prodict Id</th> -->
                                            <th class="text-center">Image</th>
                                            <th class="text-center">Vendor Name</th>
                                            <th class="text-center">Cat 1</th>
                                            <th class="text-center">Cat 2</th>
                                            <th class="text-center">Quantity</th>
                                            <th class="text-center">PRN No</th>
                                            <th class="text-center">PRN Date</th>
                                            <th class="text-center">Docket No</th>
                                            <th class="text-center">Courier Name</th>
                                            <th class="text-center">Admin Remarks</th>
                                            <th class="text-center">Vendor Remarks</th>
                                            <th class="text-center">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($returnList as $eachReturn)
                                        <tr>
                                            <td class="text-center">
                                                @if($eachReturn->image!='')
                                                
                                                    @if(file_exists('upload/temp/'.$eachReturn->image))
                                                    
                                                    <img src="{{URL::to('/upload/temp').'/'.$eachReturn->image }}" style="height:60px;width: 60px;">
                                                    @else
                                                    
                                                    <img src="{{URL::to('/img').'/pre-order-img.jpg' }}" style="height:60px;width: 60px;">
                                                    @endif
                                                @else
                                                <img src="{{URL::to('/img').'/pre-order-img.jpg' }}" style="height:60px;width: 60px;">
                                                @endif
                                            </td>
                                            <td class="text-center">{{$eachReturn->name}}</td>
                                            <td class="text-center">{{$eachReturn->category1}}</td>
                                            <td class="text-center">{{$eachReturn->category2}}</td>
                                            <td class="text-center">{{$eachReturn->return_quantity}}</td>
                                            <td class="text-center">{{$eachReturn->prn_no}}</td>
                                            <td class="text-center">
                                            @if($eachReturn->prn_date!='0000-00-00')
                                            {{date('d/m/Y',strtotime($eachReturn->prn_date))}}
                                            @endif

                                            </td>
                                            <td class="text-center">{{$eachReturn->docket_no}}</td>
                                            <td class="text-center">{{$eachReturn->courier_name}}</td>
                                            <td class="text-center" title="{{$eachReturn->comments}}">
                                            <?php
                                            $i=strlen($eachReturn->comments); 
                                            if(strlen($i>25)) 
                                                echo substr($eachReturn->comments,0,22).'...';
                                            else 
                                                echo $eachReturn->comments;

                                            ?>
                                                
                                            </td>
                                            <td class="text-center" title="{{$eachReturn->vendor_comments}}">
                                            <?php
                                            $i=strlen($eachReturn->vendor_comments); 
                                            if(strlen($i>25)) 
                                                echo substr($eachReturn->vendor_comments,0,22).'...';
                                            else 
                                                echo $eachReturn->vendor_comments;

                                            ?>
                                            </td>
                                            <td class="text-center">
                                            @if($eachReturn->status==1)<a href="javascript:void(0)" class="btn btn-success">Pending</a>
                                            @else<a href="javascript:void(0)" class="btn btn-danger">Closed</a>
                                            @endif</td>
                                        </tr> 
                                        @endforeach                          
                                    </tbody>
                               </table>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <div class="dataTables_info" role="status" aria-live="polite">Showing {{$returnList->count()}} of {{$returnList->total()}} entries</div>
              </div>
              <div class="col-sm-6">
                    <nav aria-label="Page navigation" class="pull-right">
                        {{ $returnList->links() }}
                    </nav>
                  </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
@include('layouts.footer')