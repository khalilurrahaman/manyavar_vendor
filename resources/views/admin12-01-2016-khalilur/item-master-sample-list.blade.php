@include('layouts.header')
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                	<div class="col-sm-6"><h3 class="panel-title">
                                    <i class="fa fa-users" aria-hidden="true"></i> Item Master Listing</h3></div>
                                    <div class="col-sm-6">
                                    
                                    <div class="shorting-area">
                            		
                              	<div class="col-sm-3 pull-right">
                                		<a href="{{route('item-master-list')}}" class="btn btn-success">Item Master</a>
                                </div>
                                <div class="col-sm-3 pull-right">
                                        <a href="{{route('item-master')}}" data-toggle="modal" class="btn btn-success">
                                        <i class="fa fa-plus" aria-hidden="true"></i> Add New</a>
                                </div>
                            </div>
                               <div class="col-sm-6">
                                    
                                    <div class="shorting-area">
                                <div class="pull-right">
                                        <a href="javascript:void(0)" id="show-btn-main" class="btn btn-success">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                </div>
                            </div>
                                    </div>     
                                    
                                    </div>
                                    
                                </div>
                                
                                
                                
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                            <div class="row shorting-area">
                          
                            <div class="col-sm-12">
                            <div id="mySidenavR-main" class="sidenavR" style="display:none;">
                            <div class="clearfix error_msg_main"></div>
                            <div class="filter-right">
                            <div class="filter-area list-filter">
                            <!--<h5>Vendor List Filter</h5>-->
                                <a href="javascript:void(0)" class="closebtn" id="hide-filter-main">×</a>
                                    <div class="col-sm-2">
                                         <input type="text" class="form-control" placeholder="Search" style="margin-bottom:0;" id="search_exist_main" name="search_exist_main" onkeypress="javascript: if (event.keyCode==13) getOptionFilterMain();">
                                    </div>
                                    <div class="col-sm-1">
                                        <a href="javascript:void(0)" class="btn btn-success" onclick="getOptionFilterMain();" style="width:60px !important;  margin-left: -6px !important;">Search</a>
                                    </div>
                                    <span id="mySidenavR-main-span" style="display:none;">
                                    </span>
                            <div class="clearfix"></div>        
                                </div>
                                <div class="clearfix"></div>
                                </div>    
                            </div>
                            
                            </div>
                            
                            
                            </div>
                            <span id="existItemLIstSample">
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Sr. No</th>
                                                <th class="text-center">Image</th>
                                                <th class="text-center">Category 1</th>
                                                <th class="text-center">Category 2</th>
                                                <th class="text-center">Category 3</th>
                                                <th class="text-center">Category 4</th>                                        
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <?php $inc=$data->perPage() * ($data->currentPage()-1);; ?>
                                    @foreach($data as $eachData)
                                     <?php $inc++; ?>
                                     
                                    <tr>
                                     <td class="text-center">{{$inc}}</td>
                                     <td class="text-center">
                                        @if($eachData->image!='')
                                        <img src="{{URL::to('/upload/product').'/'.$eachData->image }}"/ style="height:60px;width: 60px;">
                                        @endif
                                        </td>
                                        <td class="text-center">{{$eachData->cat1}}</td>
                                        <td class="text-center">{{$eachData->cat2}}</td>
                                        <td class="text-center">{{$eachData->cat3}}</td>
                                        <td class="text-center">{{$eachData->cat4}}</td>
                                       
                                        <td class="text-center">                                        
                                        
                                        <a href="{{route('item-master-edit',$eachData->id)}}"  class="btn btn-success">Edit</a>
                                        <a class="btn btn-success mergeData" data-target="#selectpro" data-toggle="modal" merge-with="{{$eachData->id}}">Merge</a>
                                        </td>
                                    </tr>
                                    @endforeach                            
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                              <div class="col-sm-5">
                              	<div class="dataTables_info" role="status" aria-live="polite">Showing {{$data->count()}} of {{$data->total()}} entries</div>
                              </div>
                              <div class="col-sm-7">
                                    <nav aria-label="Page navigation" class="pull-right">
                                        {{ $data->links() }}
                                    </nav>
                                  </div>
                              </div>
                              </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
<!--/////////////////////////////////////////////////////////select product Modal Start//////////////////////////////////////////////////////////////-->

<!--select product Modal Start-->
<div id="selectpro" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Select Product</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
        <div class="pull-right">
                <a href="javascript:void(0)" id="show-btn" class="btn btn-success" style="font-size:12px; margin-bottom:12px;" onclick="getOptionFilter();">
                <i class="fa fa-filter" aria-hidden="true"></i>Search</a>
        </div>
        <div class="col-sm-5">
        <div class="inner-addon right-addon">
        <i class="glyphicon glyphicon-search"></i>
        <input type="text" class="form-control" placeholder="Search" style="margin-bottom:0;" id="search_exist" name="search_exist" onkeypress="javascript: if (event.keyCode==13) getOptionFilter();">
        </div>
      	</div>
        <div class="clearfix error_msg"></div>
        <div id="mySidenavR" class="sidenavR" style="display: none;">
    
		</div>
            <div class="panel-body pre-order custome-table">
            
            <div class="table-responsive ">
            <table class="table table-fixed table-striped table-bordered">
          <thead>
            <tr>
                <th class="text-center col-xs-2">Image</th>
                <th class="text-center col-xs-2">Cat 1</th>
                <th class="text-center col-xs-2">Cat 2</th>
                <th class="text-center col-xs-2">Cat 3</th>
                <th class="text-center col-xs-2">Cat 4</th>
                <th class="text-center col-xs-2">Action</th>
            </tr>
          </thead>          
          <tbody id="existItemLIstModal">
          @if(!empty($live_product) && count($live_product)>0)
          @foreach($live_product as $ext)
          <div class="clearfix"></div>
            <tr>
            <div class="clearfix"></div>
              <td class="col-xs-2 text-center">
              <?php
                                            $e_image=URL::to('/')."/img/pre-order-img.jpg";
                                            if(!empty($ext->image) && file_exists(public_path().'/upload/product/'.$ext->image))
                                            {
                                               $e_image= URL::to('/') .'/upload/product/'.$ext->image;
                                            }
                                        ?>
                                        <img src="<?php echo $e_image; ?>" title="small image" width="56" class="selectProductExistClassImage{{$ext->id}}">
              </td>
              <td class="text-center col-xs-2 selectProductExistClassCat1{{$ext->id}}">{{$ext->cat1}}</td>
                                        <td class="text-center col-xs-2 selectProductExistClassCat2{{$ext->id}}">{{$ext->cat2}}</td>
                                        <td class="text-center col-xs-2 selectProductExistClassCat3{{$ext->id}}">{{$ext->cat3}}</td>
                                        <td class="text-center col-xs-2 selectProductExistClassCat4{{$ext->id}}">{{$ext->cat4}}</td>
              <td class="col-xs-2 text-center">
              <a href="javascript:void(0);" product_id="{{$ext->id}}" title-name="{{$ext->cat1}}-{{$ext->cat2}}-{{$ext->cat3}}-{{$ext->cat4}}" class="btn btn-success selectProductExist" 
              style="width:56px; padding:3px 0; font-size:12px;">Select</a>
              </td>
              <div class="clearfix"></div>
            </tr>
             <div class="clearfix"></div>
            @endforeach
                                    @else
                                    <div class="clearfix"></div>
                                     <tr>
                                    	<td class="text-center" colspan="6">Product Not Available!</td>
                                    </tr>
                                    <div class="clearfix"></div>
                                    @endif
          <div class="clearfix"></div>  
          </tbody>
        </table>
        </div>
            <div class="clearfix"></div>
                                    
                                </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<input type="hidden" id="merge_with" value="" />
<!--select product  Modal End-->
<script>
$(document).ready(function(){
    $("#hide-filter-main").click(function(){
        $("#mySidenavR-main").hide();
		filterSample();
    });
    $("#show-btn-main").click(function(){
        $("#mySidenavR-main").show();
    });
});
</script>
<script type="text/javascript">
    function ConfirmDelete(id)
        {
            var x = confirm("Are you sure you want to Delete?");
            if (x)
                return true;
            else
                return false;
        }
		

function getOptionFilter() {
 var search_str=$('#search_exist').val();
 $('.error_msg').text('');
 if(search_str=='')
 {
	 $('.error_msg').text('Please Enter Search Keyword!').css('color', 'red').show();
	 return false;
 }
 //$('.loading').show();
 
 $('#mySidenavR').show();
 var token='{{csrf_token()}}';
 		$.ajax({
			type: 'GET', 
			url: '{{route("get-option-filter-live")}}',
			data: {search_str:search_str},
			success:function(data)
			{
				
        		$('#mySidenavR').html(data);
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});
}

function getOptionFilterMain() {
 var search_str=$('#search_exist_main').val();
 $('.error_msg_main').text('');
 if(search_str=='')
 {
	 $('.error_msg_main').text('Please Enter Search Keyword!').css('color', 'red').show();
	 return false;
 }
 $('.loading').show();
 
 $('#mySidenavR-main-span').show();
 		$.ajax({
			type: 'GET', 
			url: '{{route("get-option-filter-sample")}}',
			data: {search_str:search_str},
			success:function(data)
			{
				$('.loading').hide();
        		$('#mySidenavR-main-span').html(data);
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});
}

$('.selectSearchSample').click(function(){
	     var selectcat1 = $('#search_exist_main').val();
		 var selectcat2 = $('#selectcat2').val();
		 var selectcat3 = $('#selectcat3').val();
		 var selectcat4 = $('#selectcat4').val();
		filterSample(selectcat1,selectcat2,selectcat3,selectcat4) ;
});
function filterSample(selectcat1='',selectcat2='',selectcat3='',selectcat4=''){
	   $('.loading').show();
 		$.ajax({
			type: 'GET', 
			url: '{{route("search-item-by-keyword-sample")}}',
			data: {selectcat1:selectcat1,selectcat2:selectcat2,selectcat3:selectcat3,selectcat4:selectcat4},
			success:function(data)
			{
        		$('#existItemLIstSample').html(data);
				$('.loading').hide();
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});
}


$( "#hide-filter" ).click(function() {
	$('#mySidenavR').hide();
	$('#merge_with').val('');
 filter();
});

function filter(selectcat1='',selectcat2='',selectcat3='',selectcat4=''){
		$('.loading').show(); 
 		$.ajax({
			type: 'GET', 
			url: '{{route("search-item-by-keyword-live")}}',
			data: {selectcat1:selectcat1,selectcat2:selectcat2,selectcat3:selectcat3,selectcat4:selectcat4},
			success:function(data)
			{
        		$('#existItemLIstModal').html(data);
				$('.loading').hide();
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});
}

$(document).on('click', '.selectProductExist', function(e) {
	
	var title = $(this).attr('title-name');
	if(confirm("Are you sure you want to Merge:"+title+"?"))
	{
	 var product_id = $(this).attr('product_id');
	 var merge_with= $('#merge_with').val();
     $('.loading').show(); 
 		$.ajax({
			type: 'GET', 
			url: '{{route("merge-item-master")}}',
			data: {product_id:product_id,merge_with:merge_with},
			success:function(data)
			{
				if(data!=1)
				{					
        			alert('Please Try Again!');
				}
				$("#selectpro").modal('hide');
				$('.loading').hide();
				location.reload(); 
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});
		
		
	}
	 
});

$(document).on('click', '.mergeData', function(e) {
	 var merge_with = $(this).attr('merge-with');
	 $('#merge_with').val(merge_with);
    
});

</script>
@include('layouts.footer')