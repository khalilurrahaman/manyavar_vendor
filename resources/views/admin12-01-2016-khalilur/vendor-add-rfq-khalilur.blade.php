@include('layouts.header')
    <div id="page-wrapper">
        <div class="container-fluid inner-body-area">
            <div class="row">
            <form id="rfqform" method="post" action="{{route('add-rfq-form-post')}}">
            {{ csrf_field() }}
                <div class="col-lg-8 margin-center">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-tree" aria-hidden="true"></i> Select Product OR Create Product</h3>
                        </div>
                        <div class="clearfix"></div>
                        <div class="panel-body custome-table">
                            <div class="row shorting-area add-rfq">
                                <div class="rfq-area">                                
                                <div class="col-sm-5">
                                    <select class="selectpicker custom-picker" data-live-search="true" onchange="addProduct(this.value);">
                                        <option data-tokens="ketchup mustard" value="">Select Product1</option>
                                        @foreach($allProduct as $eachProduct)
                                        <option data-tokens="mustard" value="{{$eachProduct->id}}">{{$eachProduct->cat1}}</option>
                                        @endforeach
                                    </select>
                                    <span  id="product_message" ></span>
                                    <input type="hidden" name="product_id" id="product_id" value="">
                                </div>
                                <div class="col-sm-2 text-center">
                                    <h5>-- OR --</h5>
                                </div>
                                <div class="col-sm-5">
                                    <button class="btn btn-success width-small" data-toggle="modal" data-target="#addrfq" onclick="item_form()" type="button">Create Product</button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i> Select Multiple Vendors and Add Shipment details</h3>
                        </div>
                        <div class="clearfix"></div>
                        <div class="panel-body custome-table">
                            <div class="row shorting-area add-rfq">
                                <div class="rfq-area">
                                  <div class="col-sm-5">
                                    <select class="selectpicker2 custom-picker" data-live-search="true" id="vendorTags" onchange="venId(this.value);">
                                        <option value="" data-tokens="ketchup mustard">Select Vendor</option>
                                        @foreach($allVendor as $eachVendor)
                                        <?php $o_value = $eachVendor->id.','.$eachVendor->first_name.' '.$eachVendor->last_name?>
                                            <option data-tokens="mustard" value="{{$o_value}}">{{$eachVendor->first_name.' '.$eachVendor->last_name }}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" id="ven_id"  name="" value="">
                                    <span  id="vendor_message" ></span>
                                </div>
                                <div class="col-sm-2 text-center">
                                    <h5>---</h5>
                                </div>
                                <div class="col-sm-5">
                                    <a href="javascript:void(0)" onclick="addVendor()" class="btn btn-success width-small marginB0">Add Vendor</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            </div>
                            <div class="clearfix"></div>
                            <hr/>
                            <div class="table-responsive view-table">
                                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Vendor ID</th>
                                            <th class="text-center">Name</th>
                                            <th class="text-center" style="width:226px;">Shiment Provider</th>
                                            <th class="text-center" style="width:226px;">Docket No</th>
                                            <th class="text-center" >Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id = 'tab_data'>
                                        
                                    </tbody>
                                </table>
                                <span id="tab_message"></span>
                            </div>
                            <div class="rfq-area">                                
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="comment">Remarks :</label>
                                        <textarea name="comment" class="form-control" rows="5"  id="commant"></textarea>
                                    </div>
                                    <span id="comment_message"></span>
                                </div>
                                <div class="col-sm-12 quotation-pop">
                                  <button type="submit" onclick="return addRfq();"  class="pup-btn" style="margin-top:0;">CREATE RFQ</button>
                                </div>
                            </div>      
                        </div>
                    </div>
                    <div class="clearfix"></div>     
                </div>
            </form>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

    <!--add RFQ Modal Start-->
    <form method="post" action="{{route('add-item')}}" enctype="multipart/form-data" id="form_rfq">
        {{ csrf_field() }}
        <div id="addrfq" class="modal fade quotation-pop" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Create Product</h4>
              </div>
              <div class="modal-body">
                <div class="row shorting-area pricechange">
                    <div class="col-sm-6">
                        <img src="{{asset('img/pre-order-img.jpg')}}" id="previewing" alt="" width="100%"><div class="clearfix">
                         <span  id="item_message" ></span>   
                    </div> <br/>
                    <div class="clearfix"></div>
                    <input type="file" id="file" name="file" value=""/>
                    </div>
                    <div class="col-sm-6">
                    <label>Category 1 :</label>
                        <input type="text" placeholder="Enter Category 1" class="form-control" id="cat_1" name="cat1" value="" />
                        <span  id="cat_1_message" ></span>
                    </div>
                    <div class="col-sm-6">
                        <label>Category 2 :</label>
                        <input type="text" placeholder="Enter Category 2" class="form-control" id="cat_2" name="cat2" value="" />
                        <span  id="cat_2_message" ></span>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12">
                    <a href="javascript:void(0)" class="pup-btn" onclick="newItem()">Submit</a>
                    </div>
                <div class="clearfix"></div>
                </div>
              </div>
              <div class="modal-footer"></div>
            </div>

          </div>
        </div>
    </form>
<!--add RFQ  Modal End-->
@include('layouts.footer')
<script>
    
    function addRfq() {
        
        var validated=0;

        if($('#product_id').val()==''){
            $("#product_message").text('Please Select a Product !').css('color', 'red').show();
            validated++;
        }else{
            $('#product_message').text('');
        }

        if($('#ven_id').val()=='' || $('#ven_id').val()==undefined){
            $("#vendor_message").text('Please Select a Vendor !').css('color', 'red').show();
            validated++;
        }else{
            $('#vendor_message').text('');
        }
        
        if($.trim($("#tab_data").html())==''){
            $("#tab_message").text('Please Add a Vendor !').css('color', 'red').show();
            validated++;
        }else{
            $('#tab_message').text('');
        }

        if($('#commant').val()==''){
            $("#comment_message").text('Please Give a Comment !').css('color', 'red').show();
            validated++;   
        }else{
            $('#comment_message').text('');
        }

        if(validated!=0){
            return false;
        }
    }


   

   

   
    

</script>