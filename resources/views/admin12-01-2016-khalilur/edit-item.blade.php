@include('layouts.header')
    <!--add user popup Modal Start-->
<div class=" quotation-pop" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Edit Item</h4>
          </div>
            <div class="modal-body">
                <form action="{{route('item-master-update')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                    <div class="row shorting-area pricechange">
                     <input type="hidden" name="id" id="id" value="{{$data->id}}" >
                        <div class="col-sm-12">
                            <h5>Category 1 :</h5>
                            <input type="text" name="category1" id="category1" value="{{$data->cat1}}" class="form-control">
                            <span class=" error class_err" id="category1_msg" ></span>
                        </div>
                        <div class="col-sm-12">
                            <h5>Category 2:</h5>
                            <input type="text" name="category2" id="category2" value="{{$data->cat2}}" class="form-control">
                            <span class=" error class_err" id="category2_msg" ></span>
                        </div>
                        <div class="col-sm-12">
                            <h5>Category 3 :</h5>
                            <input type="text" name="category3" id="category3" value="{{$data->cat3}}" class="form-control">
                        </div>
                        <div class="col-sm-12">
                            <h5>Category 4 :</h5>
                            <input type="text" name="category4" id="category4" value="{{$data->cat4}}" class="form-control">
                        </div>
                        <div class="clearfix"></div>
                        
                        
                        <div class="clearfix"></div>
                        <div class="col-sm-12">
                        <h5>Image :</h5>
                        <input type="file" name="image" class="form-control" id="image"/> 
                        <span class=" error class_err" id="image_msg" ></span>
                        </div>
                        <div class="col-sm-6">
                        <h5></h5>
                            @if($data->image!='')
                            <img src="{{URL::to('/upload/product').'/'.$data->image }}"/ style="height:60px;width: 60px;">
                            @endif
                        </div>
                        
                        <div class="clearfix"></div>
                        <div class="col-sm-6">
                        <input type="submit" name="submit" value="Update" onclick="return validateForm()" class="pup-btn">
                        </div>

                        <div class="col-sm-6">
                        <a href="{{URL::previous()}}" class="pup-btn">Cancel
                        </a>
                        <div class="clearfix"></div>
                    </div>
                    </div>
                </form>
           </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function validateForm()
  {    
    var ret_val=true;   
   
    var category1=$("#category1").val();
    if(category1==''){$("#category1_msg").text('Please Enter Category 1  !').show();ret_val=false;}
    else{$("#category1_msg").hide();}

    var category2=$("#category2").val();
    if(category2==''){$("#category2_msg").text('Please Enter Category 2  !').show();ret_val=false;}
    else{$("#category2_msg").hide();}

    var image = $("#image").val();
    if(image!=''){
    if (!image.match(/(?:gif|jpg|png|bmp)$/)) {$("#image_msg").text('Please Upload Image File  !').show();ret_val=false;}
    else{$("#image_msg").hide();}}



    return ret_val;

  }
</script>
<!--add user popup  Modal End-->
@include('layouts.footer')