@include('layouts.header')
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                	<div class="col-sm-5">
                                    	<h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i> View All REQ Request</h3>
                                    </div>
                                    <div class="col-sm-7">
                                        
                                        <div class="shorting-area vendor-filter">
                                            <div class="col-sm-5">
                                                <div class="inner-addon right-addon">
                                                    <i class="glyphicon glyphicon-search"></i>
                                                    <input type="text" class="form-control" placeholder="Search" style="margin-bottom:0;"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <a href="javascript:void(0)" id="show-btn" class="btn btn-success">
                                                <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                            </div>
                                            <div class="col-sm-2"><a href="{{route('rfq-list')}}" data-toggle="modal" class="btn btn-success">
                                                <i class="fa" aria-hidden="true"></i> Rfq List</a>
                                            </div>
                                            <div class="col-sm-3"><a href="{{route('add-rfq-form')}}" data-toggle="modal" class="btn btn-success">
                                            <i class="fa fa-plus" aria-hidden="true"></i> Add New</a>
                                            </div>
                                            
                                            <div class="clearfix"></div>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                            <div class="row shorting-area">
                            	
                            <div class="col-sm-12">
                            <div id="mySidenavR" class="sidenavR" style="display:none;">
<div class="filter-right">
<div class="filter-area rfq-filter">
<!--<h5>Vendor List Filter</h5>-->
    <a href="javascript:void(0)" class="closebtn" id="hide-filter">×</a>
        <div class="col-sm-2">
            <a class="btn btn-default btn-select">
            	<input type="hidden" class="btn-select-input" id="" name="" value="" />
            	<span class="btn-select-value">Select an Item</span>
            	<span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                <ul>
                    <li class="selected">REQ ID</li>
                    <li>REQ/001233/16-17</li>
                    <li>REQ/001895/16-17</li>
                    <li>REQ/001008/16-17</li>
                </ul>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-2">
            <a class="btn btn-default btn-select">
            	<input type="hidden" class="btn-select-input" id="" name="" value="" />
            	<span class="btn-select-value">Select an Item</span>
            	<span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                <ul>
                    <li class="selected">Cat 1</li>
                    <li>---</li>
                    <li>---</li>
                    <li>---</li>
                </ul>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-2">
            <a class="btn btn-default btn-select">
            	<input type="hidden" class="btn-select-input" id="" name="" value="" />
            	<span class="btn-select-value">Select an Item</span>
            	<span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                <ul>
                    <li class="selected">Cat 2</li>
                    <li>---</li>
                    <li>---</li>
                    <li>---</li>
                </ul>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-2">
            <a class="btn btn-default btn-select">
            	<input type="hidden" class="btn-select-input" id="" name="" value="" />
            	<span class="btn-select-value">Select an Item</span>
            	<span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                <ul>
                    <li class="selected">Name</li>
                    <li>----</li>
                    <li>----</li>
                    <li>----</li>
                </ul>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-2">
            <a class="btn btn-default btn-select">
            	<input type="hidden" class="btn-select-input" id="" name="" value="" />
            	<span class="btn-select-value">Select an Item</span>
            	<span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                <ul>
                    <li class="selected">Quote ID</li>
                    <li>----</li>
                    <li>----</li>
                    <li>----</li>
                </ul>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-2">
        <div class="form-group">
                <div class="input-group date">
                    <input type="text" class="form-control" placeholder="Date"  id="example2"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-2">
            <a class="btn btn-default btn-select">
            	<input type="hidden" class="btn-select-input" id="" name="" value="" />
            	<span class="btn-select-value">Select an Item</span>
            	<span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                <ul>
                    <li class="selected">Price</li>
                    <li>----</li>
                    <li>----</li>
                    <li>----</li>
                </ul>
            </a>
            <div class="clearfix"></div>
        </div>
        
        <div class="col-sm-2">
        	<a href="javascript:void(0)" class="btn btn-success">Apply Filter</a>
            <div class="clearfix"></div>
        </div>
<div class="clearfix"></div>        
	</div>
    <div class="clearfix"></div>
    </div>    
</div>
                            
                            </div>
                            
                            
                            </div>
                                <div class="table-responsive view-table">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">REQ ID</th>
                                                <th class="text-center">Cat 1</th>
                                                <th class="text-center">Cat 2</th>
                                                <th class="text-center">Vendor Name</th>
                                                <th class="text-center">Quote ID</th>
                                                <th class="text-center">Quote Date</th>
                                                <th class="text-center">Price</th>
                                                <th class="text-center">Width</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-center">Last Comments</th>
                                                <!--<th class="text-center">Comments</th>-->
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                @if(!empty($rfq_list))  
                                @foreach($rfq_list as $rfq)
                                    <tr>
                                    	<td class="text-center">{{ $rfq->req_id }}</td>
                                        <td class="text-center">{{ $rfq->cat1 }}</td>
                                        <td class="text-center">{{ $rfq->cat2 }}</td>
                                    	<td class="text-center"><?php 
                                        $vendor_details=$rfq->vendor_details;
                                        $rfq_quetes=$rfq->rfq_quetes;
                                        if(!empty($rfq_quetes[0]->rfq_comments) && count($rfq_quetes[0]->rfq_comments)>0)
                                        $rfq_comments=$rfq_quetes[0]->rfq_comments;

                                        ?>

                                        {{$vendor_details->first_name. ' '.$vendor_details->last_name}}</td>
                                        <td class="text-center">{{$rfq->quete_id}}</td>
                                        <td class="text-center">{{date('d/m/Y',strtotime($rfq->quete_date))}}</td>
                                        <td class="text-center">{{$rfq->price}}</td>
                                        <td class="text-center">{{$rfq->width}}</td>
                                        @if($rfq->quete_status == 1)
                                        <td class="text-center"><span class="btn btn-warning btn-xs">Open</span></td>
                                        @else
                                        <td class="text-success"><span class="btn btn-warning btn-xs">Close</span></td>
                                        @endif
                                        @if(!empty($rfq->rfq_quetes) && count(($rfq->rfq_quetes))>0)
                                        
                                        <td class="text-center">
                                        <?php                                        
                                        if(!empty($rfq->rfq_quetes->rfq_comments) && count($rfq->rfq_quetes->rfq_comments)>0)                                    
                                        echo substr($rfq->rfq_quetes->rfq_comments[0]->comments,0,20);
                                        ?>...
                                        <a href="">Details</a></td>
                                        @else<td class="text-center"><a href="javascript:void(0)">Details</a></td>

                                        @endif

                                        <!--<td class="text-center"><a href="vendor-comments-2.php">2 Comments</a></td>-->
                                        <td class="text-center"><a href="{{route('close-quotation', $rfq->id)}}" onclick="return closeRfqRequest('{{$rfq->id}}');" class="btn btn-success btn-xs">Close</a>
                                        <a href="" onclick="" class="btn btn-success btn-xs">Notify</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                               </table>
                              </div>
                              <!-- <div class="row">
                              <div class="col-sm-6">
                              	<div class="dataTables_info" role="status" aria-live="polite">Showing 1 to 9 of 9 entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                        <ul class="pagination">
                                            <li class="page-item">
                                                <a class="page-link" href="javascript:void(0)" aria-label="Previous">
                                                	<span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span>
                                                </a>
                                            </li>
                                        	<li class="page-item active"><a class="page-link" href="javascript:void(0)">1</a></li>
                                       		<li class="page-item"><a class="page-link" href="javascript:void(0)">2</a></li>
                                        	<li class="page-item"><a class="page-link" href="javascript:void(0)">3</a></li>
                                        	<li class="page-item"><a class="page-link" href="javascript:void(0)">4</a></li>
                                        	<li class="page-item"><a class="page-link" href="javascript:void(0)">5</a></li>
                                        	<li class="page-item">
                                        	<a class="page-link" href="javascript:void(0)" aria-label="Next">
                                        		<span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span>
                                        	</a>
                                        	</li>
                                        </ul>
                                    </nav>
                                  </div>
                              </div>
                            </div> -->
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@include('layouts.footer')

<script>
    function closeRfqRequest(rfq_id){
        var x = confirm("Are you sure you want to Close " +rfq_id);
        if (x)
            return true;
        else
            return false;
    }
</script>