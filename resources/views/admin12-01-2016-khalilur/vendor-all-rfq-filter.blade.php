  <div class="table-responsive view-table">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">REQ ID</th>
                                                <th class="text-center">Image</th>
                                                <th class="text-center">Cat 1</th>
                                                <th class="text-center">Cat 2</th>
                                                <th class="text-center">Vendor Name</th>
                                                <th class="text-center width-a">Quote ID</th>
                                                <th class="text-center width-b">Quote Date</th>
                                                <th class="text-center width-d">Price</th>
                                                <th class="text-center width-d">Width</th>
                                               <!-- <th class="text-center">Status</th>-->
                                                <th class="text-center width-e">Last Comments</th>
                                                <!--<th class="text-center">Comments</th>-->
                                                <th class="text-center">Notify</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody id="filterResult">
                                    
                                    @if(!empty($rfq_list) && count($rfq_list)>0)
                                     <?php $old_val='';?>
                                     @foreach($rfq_list as $rfq)
                                     <?php $new_val=$rfq->id;?>
                                     <tr>
                                     	<?php if($new_val!=$old_val){?>
                                    	<td class="text-center" rowspan="{{$rfq->rfq_count}}">{{$rfq->req_id}}</td>
                                      <td class="text-center" rowspan="{{$rfq->rfq_count}}">
                                        @if($rfq->image!='')
                                        
                                            @if(file_exists('upload/product/'.$rfq->image))
                                            
                                            <img src="{{URL::to('/upload/product').'/'.$rfq->image }}" style="height:60px;width: 60px;">
                                            @else
                                            
                                            <img src="{{URL::to('/img').'/pre-order-img.jpg' }}" style="height:60px;width: 60px;">
                                            @endif
                                        @else
                                        <img src="{{URL::to('/img').'/pre-order-img.jpg' }}" style="height:60px;width: 60px;">
                                        @endif
                                        </td>
                                        <th class="text-center" rowspan="{{$rfq->rfq_count}}">{{$rfq->cat1}}</th>
                                        <th class="text-center" rowspan="{{$rfq->rfq_count}}">{{$rfq->cat2}}</th>
                                        <?php
										}
										?>
                                    	<td class="text-center">{{$rfq->name}}</td>
                                        <td class="text-center" colspan="5" style="padding:0;">
                                         
                                         @if(!empty($rfq->rfq_quetes) && count($rfq->rfq_quetes)>0)
                                     	  
                                        <table class="table table-bordered table-striped" border="0" style="margin-bottom:0;"  cellspacing="0" width="100%">
                                            <tbody>
                                            @foreach($rfq->rfq_quetes as $quetes)
                                             <tr>
                                                <td class="text-center width-a">{{$quetes->quete_id}}</td>
                                                <td class="text-center width-b">{{date('d/m/Y',strtotime($quetes->created_at))}}</td>
                                                <td class="text-center width-d">{{$quetes->price}}</td>
                                                <td class="text-center width-d">{{$quetes->width}}"</td>
                                                <!--<td class="text-center"><span class="btn btn-warning btn-xs">Open</span></td>-->
                                                 <td class="text-left width-e ">
                                                 @if(!empty($quetes->rfq_comments) && count($quetes->rfq_comments)>0)
                                        <?php                                        
                                        if(!empty($quetes->rfq_comments) && count($quetes->rfq_comments)>0)
                                        {   
                                        if(count($quetes->rfq_comments[0]->comments)<=20)                                 
                                           echo ($quetes->rfq_comments[0]->comments);
                                        else
                                         echo substr($quetes->rfq_comments[0]->comments,0,18).'..';  

                                        }
                                        ?><a href="{{route('admin-comments',$quetes->id)}}" class=" pull-right btn btn-warning btn-xs"><?php echo count($quetes->rfq_comments)?> Comment(s)</a>
                                                 @else
                                                	<a href="{{route('admin-comments',$quetes->id)}}" class="btn btn-warning btn-xs pull-right"> Comment</a>
                                                 @endif
                                                 </td>
                                            </tr>
                                             @endforeach
                                        </tbody>
                                       </table>
                               			
                                   @else
                                   No Quotes Available!
                                   @endif
                               
                               </td>
                                       <td class="text-center">
                                        @if(empty($rfq->rfq_quetes) && $rfq->notify==0)
                                         <a id="notify_vendor<?php echo $rfq->id.'_'.$rfq->vendor_id; ?>" onclick="return notifyRfq('{{$rfq->id}}','{{$rfq->vendor_id}}');" class="btn btn-danger btn-xs">Notify</a>
                                         @elseif(empty($rfq->rfq_quetes) && $rfq->notify==1)
                                         <a class="btn btn-danger btn-xs">Notified</a>
                                        @endif
                                        </td>  
                                       <?php if($new_val!=$old_val){?>
                                        <td class="text-center"  rowspan="{{$rfq->rfq_count}}">
                                       <a href="{{route('close-quotation', $rfq->id)}}" onclick="return closeRfqRequest('{{$rfq->id}}');" class="btn btn-success btn-xs">Close</a>&nbsp;
                                       <?php
										}
										$old_val=$new_val;
										?>
                                        </td>
                                    </tr>
                                    @endforeach
                                   @else
                                    <tr>
                                    	<td class="text-center" colspan="11">No Rfq Available!</td>
                                    </tr>
                                   @endif
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                              <div class="col-sm-6">
                              	<div class="dataTables_info" role="status" aria-live="polite">Showing {{$rfq_list->count()}} of {{$rfq_list->total()}} entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                       {{ $rfq_list->links() }} 
                                    </nav>
                                  </div>
                              </div>