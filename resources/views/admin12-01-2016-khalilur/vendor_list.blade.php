@include('layouts.header')
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <!--<div class="row">
                    <div class="col-lg-12 title-top">
                    <div class="row page-header">
                    	<div class="col-sm-6">
                        	<h1>Vendor <small>Purchase Order Listing</small></h1>
                        </div>
                        <div class="col-sm-6">
                        	<a href="javascript:void(0)" class="btn btn-success">Expand</a>
                        </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                	<div class="col-sm-6"><h3 class="panel-title">
                                    <i class="fa fa-users" aria-hidden="true"></i> Vendor Listing</h3></div>
                                    <div class="col-sm-6">
                                    
                                        <div class="row shorting-area">
                                            <div class="col-sm-2 purchase-filter pull-right">
                                            <a href="javascript:void(0)" id="show-btn" class="btn btn-success">
                                            <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                            <div class="row shorting-area">
                                <!--<div class="col-sm-6">
                                <div class="dataTables_info" role="status" aria-live="polite">Showing 1 to 9 of 9 entries</div>
                              </div>
                              <div class="col-sm-2 pull-right">
                                        <a class="btn btn-default btn-select">
                                            <input type="hidden" class="btn-select-input" id="" name="" value="" />
                                            <span class="btn-select-value">Select an Item</span>
                                            <span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                                            <ul>
                                                <li class="selected">Short By</li>
                                                <li>Due Date</li>
                                                <li>Order Date</li>
                                                <li>Pending Qty</li>
                                            </ul>
                                        </a>
                                </div>
                            <div class="col-sm-1 pull-right">
                                        <a href="javascript:void(0)" id="show-btn" class="btn btn-success">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                </div>--> <div class="clearfix"></div>
                            <div class="col-sm-12">
                            <div id="mySidenavR" class="sidenavR" style="display:none;">
<div class="filter-right">
<div class="filter-area">
<!--<h5>Vendor List Filter</h5>-->
    <a href="javascript:void(0)" class="closebtn" id="hide-filter">×</a>
        <div class="col-sm-4">
           <input class="form-control" id="name" name="name" placeholder="Vendor Name" />
            <span class=" error class_err" id="name_msg" ></span>
        </div>

        <div class="col-sm-4">
            <input class="form-control" id="email" name="email" placeholder="Email ID" />

        </div>
             
        
        <div class="col-sm-2">
            <a href="javascript:void(0)" class="btn btn-success" onclick="applyFilter();">Apply Filter</a>
            <div class="clearfix"></div>
        </div>
<div class="clearfix"></div>        
    </div>
    <div class="clearfix"></div>
    </div>    
</div>
                            
                            </div>
                            
                            
                            </div>
                            <span id="filterResult">
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Name</th>
                                                <!-- <th class="text-center">Department</th>
                                                <th class="text-center">Section</th>
                                                <th class="text-center">Devision</th> -->
                                                <th class="text-center">E-mail ID</th>
                                                <th class="text-center">Phone No</th>
                                                <th class="text-center">Address</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-center">Action</th>
                                                
                                            </tr>
                                        </thead>
                                    <tbody>
                                    @foreach($vendor_list as $data)
                                    <tr>
                                        <td class="text-center">{{$data->name}}</td>
                                       
                                        <td class="text-center">{{$data->email}}</td>
                                        <td class="text-center">{{$data->mobile}}</td>
                                        <td class="text-center">{{$data->address}}</td>
                                        <td class="text-center">
                                        @if($data->status==1)
                                        Active
                                        @else
                                        Dective
                                        @endif

                                        </td>
                                        <td class="text-center">
                                        <a onclick="" href="{{route('set-ven-password',$data->id)}}" class="btn btn-success">Set Password</a>
                                        @if($data->status==1)
                                        <a href="{{route('vendor-status-deact',$data->id)}}" onclick="return ConfirmDeactive()" class="btn btn-danger">Deactive</a>
                                        @else
                                        <a href="{{route('vendor-status-act',$data->id)}}" onclick="return ConfirmActive()" class="btn btn-success">Active</a>
                                        @endif

                                        </td>
                                    </tr>
                                    @endforeach
                                      
                                </tbody>
                               </table>
                              </div>
                              
                              <div class="row">
                              <div class="col-sm-6">
                              	<div class="dataTables_info" role="status" aria-live="polite">Showing {{$vendor_list->count()}} of {{$vendor_list->total()}} entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                        {{ $vendor_list->links() }}
                                    </nav>
                                  </div>
                              </div>
                              </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
        <script type="text/javascript">
        function ConfirmActive(id)
        {
            var x = confirm("Are you sure you want to Activate?");
            if (x)
                return true;
            else
                return false;
        }function ConfirmDeactive(id)
        {
            var x = confirm("Are you sure you want to Deactivate?");
            if (x)
                return true;
            else
                return false;
        }


        $(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        //$('#load a').css('color', '#dfecf6');
        //$('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');

        var url = $(this).attr('href'); 
        var page=$(this).attr('href').split('page=')[1];
        
        var name=$("#name").val();
        var email=$("#email").val();
        
     
        filter(page,name,email);
        //window.history.pushState("", "", url);
    });

    
});
function applyFilter()
{

    var name=$("#name").val();
    var email=$("#email").val();
    var ret_val=true;
      
    if(name=='' && email==''){
        $("#name_msg").text('Please Enter Name Or Email ID of Vendor !').css('color','white').show();
        ret_val=false;
        return ret_val; 
    }
    else
    {
        $("#name_msg").hide();
        var ret_val=true;
        
    }
    if(ret_val)
    {
        
     
        filter(1,name,email);
    }
}

$( "#hide-filter" ).click(function() {
    
    $("#name").val('');
    $("#email").val('');
    
    
    filter(); 
});

function filter(page=1,name='',email=''){ 

    
    
    $('.loading').show();
    $.ajax({
        type: 'GET', 
        url: '{{route("filter-vendor-list")}}',
        data: {name:name,email:email,page:page},
        success:function(data)
        {
            $('#filterResult').html(data);
            $('.loading').hide();
        },
        error: function (error) 
        {
            
            alert('Error Occured Please Try Again');
        }
    })
    

}
        </script>
@include('layouts.footer')   