@include('layouts.header')

        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                	<div class="col-sm-6">
                                        <h3 class="panel-title">
                                            <i class="fa fa-users" aria-hidden="true"></i> View All REQ List
                                        </h3>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="{{route('rfq-request')}}" data-toggle="modal" class="btn btn-success">
                                            <i class="fa" aria-hidden="true"></i> Back
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Sl. No.</th>
                                                <th class="text-center">Image</th>
                                                <th class="text-center">REQ ID</th>
                                                <th class="text-center">Date</th>
                                                <th class="text-center">Cat 1</th>
                                                <th class="text-center">Cat 2</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Vendor</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <?php $i= 1;?>
                                    @foreach($rfqList as $key=>$eachRfq)
                                    <tr>
                                        <td class="text-center">{{$i}}</td>
                                        <td class="text-center">
                                        @if($eachRfq->image!='' && file_exists('upload/product'.'/'.$eachRfq->image))
                                        <img alt="sample image" src="{{URL::to('/upload/product').'/'.$eachRfq->image }}"/ style="height:60px;width: 60px;">
                                        @else
                                            <img alt="sample image" src="{{URL::to('/img').'/pre-order-img.jpg' }}"/ style="height:60px;width: 60px;">
                                        @endif
                                        </td>
                                        <td class="text-center">{{$eachRfq->req_id}}</td>
                                        <td class="text-center">{{date('d/m/Y',strtotime($eachRfq->created_at))}}</td>
                                        <td class="text-center">{{$eachRfq->cat1}}</td>
                                        <td class="text-center">{{$eachRfq->cat2}}</td>
                                        <td class="text-center">{{$eachRfq->cat3}}</td>
                                        <td class="text-center">@if(isset($eachRfq->vendor_details)){{$eachRfq->vendor_details->name}}@endif</td>
                                        <td class="text-center">@if($eachRfq->status==0)<a href="javascript:void(0);"  class="btn btn-danger btn-xs">Closed</a>@else<a href="{{route('close-rfq', $eachRfq->id)}}" onclick="return confirmClose('{{$eachRfq->req_id}}');" class="btn btn-success btn-xs">Close</a>@endif
                                        </td>
                                    </tr>
                                    <?php $i++?>
                                    @endforeach                            
                                </tbody>
                               </table>
                              </div>
                              
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
        <!--add user popup Modal Start-->
<div id="addrfq" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <form method="post" action="">
        {{ csrf_field() }}
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Select Vendor</h4>
          </div>
          <div class="modal-body">
            <div class="modal-body">
            <div class="row shorting-area pricechange">
            <h4>Add Vendor to RFQ No:<strong> {{$eachRfq->req_id}}</strong></h4>
            <input type="hidden" name="rfq_id" id="rfq_id" value="" />
            <div class="clearfix"></div>
                <div class="col-sm-4">
                <select class="btn btn-default btn-select" name="vendor_id" id="ven_list">
                    
                </select>
                <span  id="req_vendor_message" ></span>
                </div>
                <div class="clearfix"></div>
                <div class="col-sm-12">
                <input type="submit" name="submit" value="Submit" class="pup-btn">
                </div>
            <div class="clearfix"></div>
            </div>
          </div>
          </div>
          <div class="modal-footer"></div>
        </div>
    </form>
  </div>
</div>
<!--add user popup  Modal End-->
@include('layouts.footer')

<script>
    function getVendor(rfq_id) {
        var urls = "{{route('get-vendor')}}";
        $('#rfq_id').val(rfq_id);

        $.ajax({
            type: 'GET',
            url: urls,
            dataType: 'json',
            data: {rfq_id:rfq_id},
            success:function(data){
                var vendor_list = data;
                console.log(vendor_list);
                var vendor_html = '<option value="">--Select Vendor--</option>';

                for(var i=0; i<vendor_list.length; i++){

                    vendor_html+='<option value="'+ vendor_list[i].id +'">'+ vendor_list[i].first_name +' '+ vendor_list[i].last_name +'</option>';
                }

                $('#ven_list').html(vendor_html);
            }

        });
    }

    function confirmClose(rfq_id){
        var x = confirm("Are you sure you want to Close " +rfq_id);
        if (x)
            return true;
        else
            return false;
    }
</script>