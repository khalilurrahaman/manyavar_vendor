@include('layouts.header')

<div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <!--<div class="row">
                    <div class="col-lg-12 title-top">
                    <div class="row page-header">
                    	<div class="col-sm-6">
                        	<h1>Admin <small>All REQ Request List</small></h1>
                        </div>
                        <div class="col-sm-6">
                        	<a href="vendor-add-rfq.php" class="btn btn-success" style="width:auto;">Create RFQ Request</a>
                        </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                            <div class="row">
                                	<div class="col-sm-4">
                                    	<h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i> View All RFQ Request</h3>
                                    </div>
                                    <div class="col-sm-8">
                                    
                                    <div class="row shorting-area vendor-filter">
                                    <div class="col-sm-2 pull-right ">
                                        <a href="{{route('add-rfq-form')}}" class="btn btn-info" style="width:100%;">
                                        <i class="fa fa-plus" aria-hidden="true"></i> Create RFQ</a>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-sm-2 pull-right"><a href="{{route('rfq-list')}}" data-toggle="modal" class="btn btn-success">
                                                <i class="fa" aria-hidden="true"></i> Rfq List</a>
                                        </div>
                                    <div class="col-sm-3 pull-right ">
                                        <a href="javascript:void(0)" id="show-btn" class="btn btn-success">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                        <div class="clearfix"></div>
                                    </div>
                            		
                              <!--<div class="col-sm-5 pull-right">
                                <div class="inner-addon right-addon">
                                  <i class="glyphicon glyphicon-search"></i>
                                  <input type="text" class="form-control" placeholder="Search" style="margin-bottom:0;"/>
                                </div>
                               </div>-->
                                <div class="clearfix"></div>
                                
                            </div>
                                    
                                    
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                            <div class="row shorting-area">
                            	
                              <!--<div class="col-sm-2 pull-right">
                                        <a class="btn btn-default btn-select">
                                            <input type="hidden" class="btn-select-input" id="" name="" value="" />
                                            <span class="btn-select-value">Select an Item</span>
                                            <span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                                        	<ul>
                                                <li class="selected">Short By</li>
                                                <li>Due Date</li>
                                                <li>Order Date</li>
                                                <li>Pending Qty</li>
                                        	</ul>
                                        </a>
                                </div>
                            <div class="col-sm-1 pull-right" style="margin-bottom:12px;">
                                        <a href="javascript:void(0)" id="show-btn" class="btn btn-success">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                </div>
                                -->
                            <div class="col-sm-12">
                            <div id="mySidenavR" class="sidenavR" style="display:none;">
<div class="filter-right">
<div class="filter-area rfq-filter">
<!--<h5>Vendor List Filter</h5>-->
    <a href="javascript:void(0)" class="closebtn" id="hide-filter">×</a>
        <div class="col-sm-2">             
            <select class="select-drop" id="req_id" name="req_id">
            	<option value="">REQ ID</option>
                @if(!empty($req_id_list) && count($req_id_list)>0)
                @foreach($req_id_list as $req_id)
                	<option value="{{$req_id->req_id}}">{{$req_id->req_id}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="col-sm-2">
            <select class="select-drop" id="cat1" name="cat1">
            	<option value="">Cat 1</option>
                @if(!empty($cat1_list) && count($cat1_list)>0)
                @foreach($cat1_list as $cat1)
                	<option value="{{$cat1->cat1}}">{{$cat1->cat1}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="col-sm-2">
            <select class="select-drop" id="cat2" name="cat2">
            	<option value="">Cat 2</option>
                @if(!empty($cat2_list) && count($cat2_list)>0)
                @foreach($cat2_list as $cat2)
                	<option value="{{$cat2->cat2}}">{{$cat2->cat2}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="col-sm-2">
            <select class="select-drop" id="name" name="name">
            	<option value="">Name</option>
                @if(!empty($vendor_name_list) && count($vendor_name_list)>0)
                @foreach($vendor_name_list as $vendor_name)
                	<option value="{{$vendor_name->id}}">{{$vendor_name->name}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <!--<div class="col-sm-2">
        <select class="select-drop" id="quote_id" name="quote_id">
            	<option value="">Quote ID</option>
                @if(!empty($quete_list) && count($quete_list)>0)
                @foreach($quete_list as $quete)
                	<option value="{{$quete->quete_id}}">{{$quete->quete_id}}</option>
                @endforeach
                @endif
            </select>
            
        </div>-->
        <div class="col-sm-2">
        <div class="form-group">
                <div class="input-group date">
                    <input type="text" class="form-control date_picker" placeholder="dd/mm/yyyy"  id="date" name="date"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        
        
        <div class="col-sm-2">
            <select class="select-drop" id="price" name="price">
            	<option value="">Price</option>
                @if(!empty($rate_list) && count($rate_list)>0)
                @foreach($rate_list as $rate)
                	<option value="{{$rate->RATE}}">{{$rate->RATE}}</option>
                @endforeach
                @endif
            </select>
        </div>
        
        <div class="col-sm-2">
        	<a href="javascript:void(0)" class="btn btn-success" onclick="applyFilter();">Apply Filter</a>
            <div class="clearfix"></div>
        </div>
<div class="clearfix"></div>        
	</div>
    <div class="clearfix"></div>
    </div>    
</div>
                            
                            </div>
                            
                            
                            </div>
                            <span id="filterResult">
                                <div class="table-responsive view-table">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">REQ ID</th>
                                                <th class="text-center">Image</th>
                                                <th class="text-center">Cat 1</th>
                                                <th class="text-center">Cat 2</th>
                                                <th class="text-center">Vendor Name</th>
                                                <th class="text-center width-a">Quote ID</th>
                                                <th class="text-center width-b">Quote Date</th>
                                                <th class="text-center width-d">Price</th>
                                                <th class="text-center width-d">Width</th>
                                               <!-- <th class="text-center">Status</th>-->
                                                <th class="text-center width-e">Last Comments</th>
                                                <!--<th class="text-center">Comments</th>-->
                                                <th class="text-center">Notify</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody >
                                    
                                    @if(!empty($rfq_list) && count($rfq_list)>0)
                                     <?php $old_val='';?>
                                     @foreach($rfq_list as $rfq)
                                     <?php $new_val=$rfq->id;?>
                                     <tr>
                                     	<?php if($new_val!=$old_val){?>
                                        <td class="text-center" rowspan="{{$rfq->rfq_count}}">{{$rfq->req_id}}</td>
                                    	<td class="text-center" rowspan="{{$rfq->rfq_count}}">
                                        @if($rfq->image!='')
                                        
                                            @if(file_exists('upload/product/'.$rfq->image))
                                            
                                            <img src="{{URL::to('/upload/product').'/'.$rfq->image }}" style="height:60px;width: 60px;">
                                            @else
                                            
                                            <img src="{{URL::to('/img').'/pre-order-img.jpg' }}" style="height:60px;width: 60px;">
                                            @endif
                                        @else
                                        <img src="{{URL::to('/img').'/pre-order-img.jpg' }}" style="height:60px;width: 60px;">
                                        @endif
                                        </td>
                                        <th class="text-center" rowspan="{{$rfq->rfq_count}}">{{$rfq->cat1}}</th>
                                        <th class="text-center" rowspan="{{$rfq->rfq_count}}">{{$rfq->cat2}}</th>
                                        <?php
										}
										?>
                                    	<td class="text-center">{{$rfq->name}}</td>
                                        <td class="text-center" colspan="5" style="padding:0;">
                                         
                                         @if(!empty($rfq->rfq_quetes) && count($rfq->rfq_quetes)>0)
                                     	  
                                        <table class="table table-bordered table-striped" border="0" style="margin-bottom:0;"  cellspacing="0" width="100%">
                                            <tbody>
                                            @foreach($rfq->rfq_quetes as $quetes)
                                             <tr>
                                                <td class="text-center width-a">{{$quetes->quete_id}}</td>
                                                <td class="text-center width-b">{{date('d/m/Y',strtotime($quetes->created_at))}}</td>
                                                <td class="text-center width-d">{{$quetes->price}}</td>
                                                <td class="text-center width-d">{{$quetes->width}}"</td>
                                                <!--<td class="text-center"><span class="btn btn-warning btn-xs">Open</span></td>-->
                                                 <td class="text-left width-e ">
                                                 @if(!empty($quetes->rfq_comments) && count($quetes->rfq_comments)>0)
                                        <?php                                        
                                        if(!empty($quetes->rfq_comments) && count($quetes->rfq_comments)>0)
                                        {   
                                        if(count($quetes->rfq_comments[0]->comments)<=20)                                 
                                           echo ($quetes->rfq_comments[0]->comments);
                                        else
                                         echo substr($quetes->rfq_comments[0]->comments,0,18).'..';  

                                        }
                                        ?><a href="{{route('admin-comments',$quetes->id)}}" class=" pull-right btn btn-warning btn-xs"><?php echo count($quetes->rfq_comments)?> Comment(s)</a>
                                                 @else
                                                	<a href="{{route('admin-comments',$quetes->id)}}" class="btn btn-warning btn-xs pull-right"> Comment</a>
                                                 @endif
                                                 </td>
                                            </tr>
                                             @endforeach
                                        </tbody>
                                       </table>
                               			
                                   @else
                                   No Quotes Available!
                                   @endif
                               
                               </td>
                                       <td class="text-center">
                                        @if(empty($rfq->rfq_quetes) && $rfq->notify==0)
                                         <a id="notify_vendor<?php echo $rfq->id.'_'.$rfq->vendor_id; ?>" onclick="return notifyRfq('{{$rfq->id}}','{{$rfq->vendor_id}}');" class="btn btn-danger btn-xs">Notify</a>
                                         @elseif(empty($rfq->rfq_quetes) && $rfq->notify==1)
                                         <a class="btn btn-danger btn-xs">Notified</a>
                                        @endif
                                        </td>  
                                       <?php if($new_val!=$old_val){?>
                                        <td class="text-center"  rowspan="{{$rfq->rfq_count}}">
                                       <a href="{{route('close-quotation', $rfq->id)}}" onclick="return closeRfqRequest('{{$rfq->id}}');" class="btn btn-success btn-xs">Close</a>&nbsp;
                                       <?php
										}
										$old_val=$new_val;
										?>
                                        </td>
                                    </tr>
                                    @endforeach
                                   @else
                                    <tr>
                                    	<td class="text-center" colspan="11">No Rfq Available!</td>
                                    </tr>
                                   @endif
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                              <div class="col-sm-6">
                              	<div class="dataTables_info" role="status" aria-live="polite">Showing {{$rfq_list->count()}} of {{$rfq_list->total()}} entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                       {{ $rfq_list->links() }} 
                                    </nav>
                                  </div>
                              </div>
                              </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>

@include('layouts.footer')
<script>

$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        //$('#load a').css('color', '#dfecf6');
        //$('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');

        var url = $(this).attr('href'); 
		var page=$(this).attr('href').split('page=')[1]; 
		
		var req_id=$("#req_id").val();
		var cat1=$("#cat1").val();
		var cat2=$("#cat2").val();
		var name=$("#name").val();
		//var quote_id=$("#quote_id").val();
		var date=$("#date").val();
		var price=$("#price").val();
	
        filter(page,req_id,cat1,cat2,name,date,price);
        //window.history.pushState("", "", url);
    });

    
});

$( "#hide-filter" ).click(function() {
	$("#req_id").val('');
	$("#cat1").val('');
	$("#cat2").val('');
	$("#name").val('');
	$("#date").val('');
	$("#price").val('');
		
	filter(); 
});

function applyFilter(){	
	
		var req_id=$("#req_id").val();
		var cat1=$("#cat1").val();
		var cat2=$("#cat2").val();
		var name=$("#name").val();
		//var quote_id=$("#quote_id").val();
		var date=$("#date").val();
        date=date.split('/').reverse().join('-');
		var price=$("#price").val();
	
        filter(1,req_id,cat1,cat2,name,date,price);
}

function filter(page=1,req_id='',cat1='',cat2='',name='',date='',price=''){	

	$('.loading').show();
	
	$.ajax({
		type: 'GET', 
		url: '{{route("filter-rfq-request")}}',
		data: {req_id:req_id,cat1:cat1,cat2:cat2,name:name,date:date,price:price,page:page},
		success:function(data)
		{
			$('#filterResult').html(data);
			$('.loading').hide();
		},
		error: function (error) 
		{
			
			alert('Error Occured Please Try Again');
		}
	})
}
</script>
<script>
    function closeRfqRequest(rfq_id){
        var x = confirm("Are you sure you want to Close " +rfq_id);
        if (x)
            return true;
        else
            return false;
    }
</script>
<script>
    function notifyRfq(rfq_id,vendor_id){

        if(!confirm("Are you sure You want to notify "))
        {
            return false;
        }

        var token_cmnts='{{csrf_token()}}';
        var c_url='{{route("notify-vendor")}}';

       $.ajax({
        type: 'POST', 
        url: c_url,
        data: {vendor_id:vendor_id,rfq_id:rfq_id, _token:token_cmnts},
        success:function(data)
        {          
            $('#notify_vendor'+rfq_id+'_'+vendor_id).text('Notified');
        },
        error: function (error) 
        {
          console.log(error);
          alert('Error Occured Please Try Again');
        }
      });
    }
</script>