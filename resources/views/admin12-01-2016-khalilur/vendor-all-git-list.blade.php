@include('layouts.header')
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
               
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i> All GIT List</h3>
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                            
                                <div class="table-responsive view-table">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">SL No</th>
                                                <th class="text-center">Vendor Name</th>
                                                <th class="text-center">Cat 1</th>
                                                <th class="text-center">Cat 2</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Date</th>
                                                <th class="text-center">Order Qty</th>
                                                <th class="text-center">GIT Qty</th>
                                                <th class="text-center">Receive Qty</th>
                                                <th class="text-center">Pending Qty</th>
                                                <th class="text-center">GRT</th>
                                                <th class="text-center">Last Comments</th><!--
                                                <th class="text-center">Remarks</th>-->
                                                <th class="text-center width-last">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(!empty($gitList) && count($gitList)>0)
                                            <?php $inc=0;?>
                                            @foreach($gitList as $eachGit)
                                            <?php 
                                            $inc++;
											$prev_git_qty=$eachGit->prev_git_qty;
											$prev_gitqty=$prev_git_qty[0]->pgit_qty;
                                            ?>
                                            <tr>
                                                <td class="text-center">{{$inc}}</td>
                                            	<td class="text-center">{{$eachGit->name}}</td>
                                                <td class="text-center">{{$eachGit->cat1}}</td>
                                                <td class="text-center">{{$eachGit->cat2}}</td>
                                                <td class="text-center">{{$eachGit->cat3}}</td>
                                            	<td class="text-center">{{date('d/m/Y',strtotime($eachGit->updated_at))}}</td>
                                                 <td class="text-center">{{$eachGit->ORDQTY}}</td>
                                                <td class="text-center">{{$eachGit->git_qty}}</td>
                                                <td class="text-center">{{$eachGit->RCQTY}}</td>
                                                <td class="text-center">{{$eachGit->ORDQTY- $eachGit->RCQTY - $eachGit->CNLQTY-$prev_gitqty}}</td>
                                                <td class="text-center">2</td>
                                                <td class="text-center">
                                        @if(!empty($eachGit->git_comments) && count(($eachGit->git_comments))>0)
                                        <?php                                       
                                       
                                        if(strlen($eachGit->git_comments[0]->comments)<=20)                                 
                                           echo ($eachGit->git_comments[0]->comments);
                                        else
                                         echo substr($eachGit->git_comments[0]->comments,0,18).'..';  

                                        ?>
                                        <a href="{{route('admin-git-comments', $eachGit->id)}}" class="pull-right btn btn-warning btn-xs">
                                        <?php echo count($eachGit->git_comments)?>
                                        Comment(s)</a>
                                        @else
                                        <a href="{{route('admin-git-comments', $eachGit->id)}}" class="btn btn-warning btn-xs pull-right">
                                        
                                        Comments</a>
                                        
                                         @endif
                                                </td>
                                                <!--<td class="text-center"><a href="vendor-comments-2.php">2 Comments</a></td>-->
                                                <td class="text-center width-last">
                                                 
                                                    <a href="{{route('mark-complete',$eachGit->id)}}" class="btn btn-success" onclick="return completeConfirm()">MARK COMPLETE</a>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row">
                                  <div class="col-sm-6">
                                    <div class="dataTables_info" role="status" aria-live="polite">Showing {{$gitList->count()}} of {{$gitList->total()}} entries</div>
                                  </div>
                                  <div class="col-sm-6">
                                        <nav aria-label="Page navigation" class="pull-right">
                                            {{ $gitList->links() }}
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@include('layouts.footer')
<script type="text/javascript">
    function completeConfirm()
{
    if(confirm('Are you sure'))
    {
        return true;
    }
    else
    {
        return false;
    }
}
</script>