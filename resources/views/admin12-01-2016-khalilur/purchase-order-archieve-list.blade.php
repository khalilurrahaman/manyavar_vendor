@include('layouts.header')
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <!--<div class="row">
                    <div class="col-lg-12 title-top">
                    <div class="row page-header">
                    	<div class="col-sm-6">
                        	<h1>Admin <small>Purchase Order Listing</small></h1>
                        </div>
                        <div class="col-sm-6">
                        	<a href="javascript:void(0)" class="btn btn-success">Expand</a>
                        </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading ">
                                <div class="row">
                                	<div class="col-sm-6">
                                    	<h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i>Archive Purchase Order Listing</h3>
                                    </div>
                                    <div class="col-sm-6">
                                    
                                    <div class="row shorting-area">
                                   <!--  <div class="col-sm-2 purchase-filter pull-right">
                                        <a href="javascript:void(0)" id="show-btn" class="btn btn-success">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                </div> -->
                                 <div class="col-sm-3 pull-right">
                                        <a href="{{route('purchase_order_list')}}" class="btn btn-success">Back</a>
                                </div>
                            </div>
                                    
                                    
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                            <div class="row shorting-area">
                            	<!--<div class="col-sm-6">
                              	<div class="dataTables_info" role="status" aria-live="polite">Showing 1 to 9 of 9 entries</div>
                              </div>
                              <div class="col-sm-2 pull-right">
                                        <a class="btn btn-default btn-select">
                                            <input type="hidden" class="btn-select-input" id="" name="" value="" />
                                            <span class="btn-select-value">Select an Item</span>
                                            <span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                                        	<ul>
                                                <li class="selected">Short By</li>
                                                <li>Due Date</li>
                                                <li>Order Date</li>
                                                <li>Pending Qty</li>
                                        	</ul>
                                        </a>
                                </div>--> 
                                
                            <!--<div class="col-sm-1 pull-right">
                                        <a href="javascript:void(0)" id="show-btn" class="btn btn-success">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                </div>--> <div class="clearfix"></div>
                            <div class="col-sm-12">
                            <div id="mySidenavR" class="sidenavR" style="display:none;">
<div class="filter-right">
<div class="filter-area">
<!--<h5>Vendor List Filter</h5>-->
    <a href="javascript:void(0)" class="closebtn" id="hide-filter">×</a>
        <div class="col-sm-2">
           <select class="select-drop" id="po_number" name="po_number">
            	<option value="">PO Number</option>
                
                	<option value=""></option>
               
            </select>
        </div>
        <div class="col-sm-2">
        	<select class="select-drop" id="category1" name="category1">
            	<option value="">Category 1</option>
               
                	<option value=""></option>
               
            </select>
        </div>
        <div class="col-sm-2">
        	<select class="select-drop" id="category2" name="category2">
            	<option value="">Category 2</option>
               
                	<option value=""></option>
               
            </select>
        </div>
        <div class="col-sm-2">
        <div class="form-group">
                <div class="input-group date">
                    <input type="text" class="form-control date_picker" placeholder="Due From"  id="due_from" name="due_from"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-2">
        <div class="form-group">
                <div class="input-group date">
                    <input type="text" class="form-control date_picker" placeholder="Due To"  id="due_to" name="due_to"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-2">
        	<a href="javascript:void(0)" class="btn btn-success" onclick="applyFilter();">Apply Filter</a>
            <div class="clearfix"></div>
        </div>
<div class="clearfix"></div>        
	</div>
    <div class="clearfix"></div>
    </div>    
</div>
                            
                            </div>
                            
                            
                            </div>
                            <span id="filterResult">
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Sr. No</th>
                                                <th class="text-center">PO. No</th>
                                                <th class="text-center">PO. Date</th>
                                                <th class="text-center">Due. Date</th>
                                                <th class="text-center">Cat 1</th>
                                                <th class="text-center">Cat 2</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Cat 4</th>
                                                <th class="text-center">Price</th>
                                                <th class="text-center">Order Qty</th>
                                                <th class="text-center">Received Qty</th>
                                                <th class="text-center">Cancelled Qty</th>
                                               
                                                <th class="text-center">Pending Qty</th>
                                               
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <?php $inc=$purchaseOrderList->perPage() * ($purchaseOrderList->currentPage()-1);; ?>
                                    @foreach($purchaseOrderList as $eachOrder)
                                    <?php $inc++;
                                     $pending = ($eachOrder->ORDQTY - ($eachOrder->RCQTY+$eachOrder->CNLQTY ));
                                     if($pending<=0)
                                        $pending=str_replace('-', 'Adhoc:', $pending);
                                    ?>
                                     
                                    <tr>
                                        <td class="text-center">{{$inc}}</td>
                                        <td class="text-center po-no<?php echo $inc;?>">{{$eachOrder->ORDNO}}</td>
                                        <td class="text-center po-date<?php echo $inc;?>">{{date('d/m/Y',strtotime($eachOrder->ORDDT))}}</td>
                                        <td class="text-center po-date<?php echo $inc;?>">{{date('d/m/Y',strtotime($eachOrder->TIME))}}</td>
                                        <td class="text-center cat-1<?php echo $inc;?>">{{$eachOrder->cat1}}</td>
                                        <td class="text-center cat-2<?php echo $inc;?>">{{$eachOrder->cat2}}</td>
                                        <td class="text-center cat-3<?php echo $inc;?>">{{$eachOrder->cat3}}</td>
                                        <td class="text-center cat-4<?php echo $inc;?>">{{$eachOrder->cat4}}</td>
                                        <td class="text-center price<?php echo $inc;?>">{{$eachOrder->RATE}}</td>
                                        <td class="text-center order-qty<?php echo $inc;?>">{{$eachOrder->ORDQTY}}</td>
                                        <td class="text-center received-qty<?php echo $inc;?>">{{$eachOrder->RCQTY}}</td>
                                        <td class="text-center cancelled-qty<?php echo $inc;?>">{{$eachOrder->CNLQTY}}</td>
                                       
                                        <td class="text-center pending-qty<?php echo $inc;?>">{{$pending}}</td>
                                        
                                    </tr>
                                    
                                    @endforeach
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                              <div class="col-sm-5">
                                <div class="dataTables_info" role="status" aria-live="polite">Showing {{$purchaseOrderList->count()}} of {{$purchaseOrderList->total()}} entries</div>
                              </div>
                              <div class="col-sm-7">
                                    <nav aria-label="Page navigation" class="pull-right">
                                        {{ $purchaseOrderList->links() }}
                                    </nav>
                                  </div>
                              </div>
                              </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@include('layouts.footer')
