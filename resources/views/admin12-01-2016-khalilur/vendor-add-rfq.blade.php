@include('layouts.header')
<style>
.afterCreate, .shipmentDetails
{
 display:none;
}

</style>
<link rel="stylesheet" href="{{asset('autocomplete/jquery-ui.css')}}" />
<script src="{{asset('autocomplete/jquery-ui.js')}}"></script>

        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <!--<div class="row">
                    <div class="col-lg-12 title-top">
                    <div class="row page-header">
                    	<div class="col-sm-6">
                        	<h1>Admin <small>Create RFQ Request</small></h1>
                        </div>
                        <div class="col-sm-6">
                        	<a href="vendor-all-rfq.php" class="btn btn-success" style="width:auto;">View All RFQ Request</a>
                        </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                <form id="rfqform" method="post" action="{{route('add-rfq-form-post')}}">
            	 {{ csrf_field() }}
                    <div class="col-lg-8 margin-center">
                    <div class="panel panel-default">
                    
                    		<!--///Default///-->
                            <div class="panel-heading beforeCreate">
                                <h3 class="panel-title"><i class="fa fa-tree" aria-hidden="true"></i> Select Product OR Create Product</h3>
                            </div>
                            <div class="clearfix beforeCreate"></div>
                            <div class="panel-body custome-table beforeCreate">
                                <div class="row shorting-area add-rfq">
									<div class="rfq-area">
                                    <div class="col-sm-4">
                                        <button class="btn btn-warning width-small" data-toggle="modal" data-target="#samplepro" type="button">Sample Product</button>
            						</div>
                                    <div class="col-sm-4">
                                        <button class="btn btn-info width-small" data-toggle="modal" data-target="#selectpro" type="button">Select Product</button>
            						</div> 
                                    <!--<div class="col-sm-3 text-center">
            							<h5>-- OR --</h5>
            						</div>-->
                                    <div class="col-sm-4">
            		<button class="btn btn-success width-small" data-toggle="modal" data-target="#addrfq" type="button" onclick="item_form()">Create Product</button>
            						</div>
                                    <div class="clearfix"></div>
                                </div>
                                </div>
                            </div>
                            <!--///After Create///-->
                            <input type="hidden" id="product_id" name="product_id" value="" />
                            <div class="panel-heading afterCreate">
                                <h3 class="panel-title pull-left"><i class="fa fa-tree" aria-hidden="true"></i> Select Product </h3>
                                <h3 class="panel-title pull-right"><a href="{{route('add-rfq-form')}}" style="font-size:16px; line-height:3px;"><strong>×</strong></a></h3>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix afterCreate"></div>
                            <div class="panel-body custome-table afterCreate">
                                <div class="row shorting-area add-rfq">
									<div class="col-sm-12">
                                    
                                    <div class="panel-body pre-order custome-table">
                                    <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" style="margin-bottom:0;">
                                        <thead>
                                            <tr>
                                            	<th class="text-center">Image</th>
                                                <th class="text-center">Cat 1</th>
                                                <th class="text-center">Cat 2</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Cat 4</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <tr>
                                    	<td class="text-center">
                                        <img id="afterCreateImage" src="{{asset('img/pre-order-img.jpg')}}" title="small image" width="56">
                                        </td>
                                        <td class="text-center" id="afterCreateCat1">--</td>
                                        <td class="text-center" id="afterCreateCat2">--</td>
                                        <td class="text-center" id="afterCreateCat3">--</td>
                                        <td class="text-center" id="afterCreateCat4">--</td>
                                    </tr>
                                </tbody>
                               </table>
                              </div>
                                </div>                                    
                                    <div class="clearfix"></div>
                                </div>
                                </div>
                            </div>
                            
                        </div>
                      
                        <div class="panel panel-default shipmentDetails">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i> Select Multiple Vendors and Add Shipment details</h3>
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                                <div class="row shorting-area add-rfq">
									<div class="rfq-area">
                                      <div class="col-sm-5 margin-center">
                                        <select class="selectpicker2 custom-picker " data-live-search="true" onchange="addVendor(this.value);" id="ven_id">
                                            <option data-tokens="ketchup mustard" value="">Select Vendor</option>
                                             @if(!empty($allVendor) && count($allVendor)>0)
                                             @foreach($allVendor as $eachVendor)
                                        	 <?php 
											 $o_value = $eachVendor->id.','.$eachVendor->name; 
											 $data_tokens = $eachVendor->name;
											 $o_tvalue = $eachVendor->name;
											 ?>
                                            	<option data-tokens="{{$data_tokens}}" value="{{$o_value}}">{{$o_tvalue}}</option>
                                        	 @endforeach
                                        	 @endif
                                        </select>

            						</div>
                                    <!--<div class="col-sm-2 text-center">
            							<h5>---</h5>
            						</div>-->
                                    <!--<div class="col-sm-5">
            							<button class="btn btn-warning2 width-small marginB0" type="button">Add Vendor</button>
            						</div>-->
                                    <div class="clearfix"></div>
                                </div>
                                
                                </div>
                                <div class="clearfix"></div>
                                <hr/>
                                <div class="table-responsive view-table">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Vendor ID</th>
                                                <th class="text-center">Name</th>
                                                <th class="text-center" style="width:226px;">Shiment Provider</th>
                                                <th class="text-center" style="width:226px;">Docket No</th>
                                                <th class="text-center" >Remarks</th>
                                                <th class="text-center" >Action</th>
                                            </tr>
                                        </thead>
                                    <tbody id='tab_data'></tbody>
                               </table>
                               <span id="tab_message"></span>
                              </div>
                              <div class="rfq-area">                                
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                        	<label for="comment">Remarks :</label>
                                        	 <textarea name="comment" class="form-control" rows="5"  id="commant"></textarea>
                                        </div>
            						</div>
                                    <div class="col-sm-6 quotation-pop">
                                     <button type="submit" onclick="return addRfq();"  class="pup-btn" style="margin-top:0;">CREATE RFQ</button>
                                     </div>
                                     <div class="col-sm-6 quotation-pop">
                                     <a style="margin-top: auto;" href="{{route('add-rfq-form')}}" class="pup-btn">CANCEL</a>
                                     
            				        </div>
                                </div>  
                                
                                
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        
                    </div>
                  </form>  
                    
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@include('layouts.footer')

<!--/////////////////////////////////////////////////////////sample product Modal Start//////////////////////////////////////////////////////////////-->

<!--sample product Modal Start-->
<div id="samplepro" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Sample Product</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
                                <div class="clearfix"></div>
        
            <div class="panel-body pre-order custome-table">
                                    <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" style="margin-bottom:0;">
                                        <thead>
                                            <tr>
                                            	<th class="text-center">Image</th>
                                                <th class="text-center">Cat 1</th>
                                                <th class="text-center">Cat 2</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Cat 4</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    @if(!empty($sampleProduct) && count($sampleProduct)>0)
                                    @foreach($sampleProduct as $sample)
                                    <tr>
                                    	<td class="text-center">
 										<?php
                                            $s_image=URL::to('/')."/img/pre-order-img.jpg";
                                            if(!empty($sample->image) && file_exists(public_path().'/upload/product/'.$sample->image))
                                            {
                                               $s_image= URL::to('/') .'/upload/product/'.$sample->image;
                                            }
                                        ?>
                                        <img src="<?php echo $s_image; ?>" title="small image" width="56" class="selectProductSampleClassImage{{$sample->id}}">
                                        </td>
                                        <td class="text-center selectProductSampleClassCat1{{$sample->id}}">{{$sample->cat1}}</td>
                                        <td class="text-center selectProductSampleClassCat2{{$sample->id}}">{{$sample->cat2}}</td>
                                        <td class="text-center selectProductSampleClassCat3{{$sample->id}}">{{$sample->cat3}}</td>
                                        <td class="text-center selectProductSampleClassCat4{{$sample->id}}">{{$sample->cat4}}</td>
                                        <td class="text-center">
                                        <a href="javascript:void(0);" product_id="{{$sample->id}}" class="btn btn-success selectProductSample" style="width:56px; padding:3px 0; font-size:12px;">Select</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                     <tr>
                                    	<td class="text-center" colspan="6">Sample Product Not Available!</td>
                                    </tr>
                                    @endif
                                    
                                </tbody>
                               </table>
                              </div>
                                </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--sample product  Modal End-->


<!--/////////////////////////////////////////////////////////select product Modal Start//////////////////////////////////////////////////////////////-->

<!--select product Modal Start-->
<div id="selectpro" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Select Product</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
        <div class="pull-right">
                <a href="javascript:void(0)" id="show-btn" class="btn btn-success" style="font-size:12px; margin-bottom:12px;" onclick="getOptionFilter();">
                <i class="fa fa-filter" aria-hidden="true"></i>Search</a>
        </div>
        <div class="col-sm-5">
        <div class="inner-addon right-addon">
        <i class="glyphicon glyphicon-search"></i>
        <input type="text" class="form-control" placeholder="Search" style="margin-bottom:0;" id="search_exist" name="search_exist" onkeypress="javascript: if (event.keyCode==13) getOptionFilter();">
        </div>
      	</div>
        <div class="clearfix error_msg"></div>
        <div id="mySidenavR" class="sidenavR" style="display: none;">
    
		</div>
            <div class="panel-body pre-order custome-table">
            
            <div class="table-responsive ">
            <table class="table table-fixed table-striped table-bordered">
          <thead>
            <tr>
                <th class="text-center col-xs-2">Image</th>
                <th class="text-center col-xs-2">Cat 1</th>
                <th class="text-center col-xs-2">Cat 2</th>
                <th class="text-center col-xs-2">Cat 3</th>
                <th class="text-center col-xs-2">Cat 4</th>
                <th class="text-center col-xs-2">Action</th>
            </tr>
          </thead>          
          <tbody id="existItemLIstModal">
          @if(!empty($allProduct) && count($allProduct)>0)
          @foreach($allProduct as $ext)
          <div class="clearfix"></div>
            <tr>
            <div class="clearfix"></div>
              <td class="col-xs-2 text-center">
              <?php
                                            $e_image=URL::to('/')."/img/pre-order-img.jpg";
                                            if(!empty($ext->image) && file_exists(public_path().'/upload/product/'.$ext->image))
                                            {
                                               $e_image= URL::to('/') .'/upload/product/'.$ext->image;
                                            }
                                        ?>
                                        <img src="<?php echo $e_image; ?>" title="small image" width="56" class="selectProductExistClassImage{{$ext->id}}">
              </td>
              <td class="text-center col-xs-2 selectProductExistClassCat1{{$ext->id}}">{{$ext->cat1}}</td>
                                        <td class="text-center col-xs-2 selectProductExistClassCat2{{$ext->id}}">{{$ext->cat2}}</td>
                                        <td class="text-center col-xs-2 selectProductExistClassCat3{{$ext->id}}">{{$ext->cat3}}</td>
                                        <td class="text-center col-xs-2 selectProductExistClassCat4{{$ext->id}}">{{$ext->cat4}}</td>
              <td class="col-xs-2 text-center">
              <a href="javascript:void(0);" product_id="{{$ext->id}}" class="btn btn-success selectProductExist" 
              style="width:56px; padding:3px 0; font-size:12px;">Select</a>
              </td>
              <div class="clearfix"></div>
            </tr>
             <div class="clearfix"></div>
            @endforeach
                                    @else
                                    <div class="clearfix"></div>
                                     <tr>
                                    	<td class="text-center" colspan="6">Product Not Available!</td>
                                    </tr>
                                    <div class="clearfix"></div>
                                    @endif
          <div class="clearfix"></div>  
          </tbody>
        </table>
        </div>
            <div class="clearfix"></div>
                                    
                                </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--select product  Modal End-->


<!--/////////////////////////////////////////////////////////add RFQ Modal Start//////////////////////////////////////////////////////////////-->
<form method="post" action="{{route('add-item')}}" enctype="multipart/form-data" id="form_rfq">
 {{ csrf_field() }}
<div id="addrfq" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Create Product</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
            <div class="col-sm-4">
                        <img src="{{asset('img/pre-order-img.jpg')}}" id="previewing" alt="" width="100%"><div class="clearfix">
                         <span  id="item_message" ></span>   
                    </div> <br/>
                    <div class="clearfix"></div>
                    <input type="file" id="file" name="file" value=""/>
                    </div>
                  
            <div class="col-sm-4">
               <label>Category 1 :</label>
                         <input type="text" placeholder="Category1" class="form-control" id="cat_1" name="cat1" value="" />
           
            </div>
            <div class="col-sm-4">
            <label>Category 2 :</label>
            <input type="text" placeholder="Category2" class="form-control" id="cat_2" name="cat2" value="" />
            </div>
            
            <div class="col-sm-4">
            <label>Category 3 :</label>
            <input type="text" placeholder="Category3" class="form-control" id="cat_3" name="cat3" value="" />
            </div>
            <div class="col-sm-4">
            <label>Category 4 :</label>
           <input type="text" placeholder="Category4" class="form-control" id="cat_4" name="cat4" value="" />
            </div>

            <div class="clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn" onclick="newItem()">Submit</a>
            </div>
            <div class="col-sm-6">
             <a href="javascript:void(0);" class="pup-btn" data-dismiss="modal">Cancel</a>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
</form>
<!--add RFQ  Modal End-->
<script src="{{asset('js/custom/admin_rfq.js')}}"></script> 
<script> 
var path= "<?php echo URL::to('/') .'/upload/product/';?>";
</script>

<script>
var cat1_list = <?php echo $cat1_list;?>;
var cat2_list = <?php echo $cat2_list;?>;
var cat3_list = <?php echo $cat3_list;?>;
var cat4_list = <?php echo $cat4_list;?>;
$(document).ready(
  function () {
    $( "#cat_1" ).autocomplete({
      source:cat1_list,
      autoFocus: true ,
      //change: function (event, ui) { alert(ui.item.value); }
    });
    $( "#cat_2" ).autocomplete({
      source:cat2_list,
      autoFocus: true ,
      //change: function (event, ui) { alert(ui.item.value); }
    });
    $( "#cat_3" ).autocomplete({
      source:cat3_list,
      autoFocus: true ,
      //change: function (event, ui) { alert(ui.item.value); }
    });
    $( "#cat_4" ).autocomplete({
      source:cat4_list,
      autoFocus: true ,
      //change: function (event, ui) { alert(ui.item.value); }
    });
  }

);
  </script>

<script>

    function newItem(){
        
        var urls = "{{route('add-item')}}";
        var cat_1 = $("#cat_1").val(); 
        var cat_2 = $("#cat_2").val();
		var cat_3 = $("#cat_3").val(); 
		var cat_4 = $("#cat_4").val(); 
        var item_image = $("#file").val();
        var imagefile = item_image.split('.').pop();
        var imgArray= ["jpeg","png","jpg","JPEG","PNG","JPG"];
		
		if($.inArray(imagefile,imgArray)== -1){
            $("#item_message").text('Please Select a Valid Image!').css('color', 'red').show();
           return false;
        }
       $('.loading').show();
        $.ajax({
            url: urls,
            type: 'POST',
            data:new FormData($("#form_rfq")[0]),
            processData: false,
            contentType: false,
            asynce: false,
            cache: false,
            success: function(data)
            {
                console.log(data.id);
                $('#product_id').val(data.id);
				$("#afterCreateImage").attr("src",path+data.image);
				$("#afterCreateCat1").text(data.cat1);
				$("#afterCreateCat2").text(data.cat2);
				$("#afterCreateCat3").text(data.cat3);
				$("#afterCreateCat4").text(data.cat4);
               
				
				$(".beforeCreate").hide();
				$(".afterCreate").show();
				$(".shipmentDetails").show();
				$("#addrfq").modal('hide');
                $('.loading').hide();
	 
            },error: function(data){
                alert(data.error);
            }
            

        });
    };
	
	$(function() {
        $("#file").change(function() {
            var file = this.files[0];
            var imagefile = file.type;
            var match= ["image/jpeg","image/png","image/jpg"];
            if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
            {
                $('#previewing').attr('src','pre-order-img.jpg');
                $("#item_message").text('Please Select Image!').css('color', 'red').show();
            return false;
            }
            else
            {
                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
    function imageIsLoaded(e) {
        $("#file").css("color","green");
		 $("#item_message").hide();
        $('#previewing').attr('src', e.target.result);
    };
	
	var v_list = Array();
	function addVendor(value) {
        
        var vendor =  value;

        if(vendor!=''){
            var vendor_arr = vendor.split(',');
            var id = vendor_arr[0];
            var name = vendor_arr[1];
            
            if($.inArray(id, v_list) == -1)
            {
                v_list.push(id);              

                
                var vendor_html = '';

                vendor_html += '<tr id="row_'+id+'" ><td class="text-center">'+id+'<input type="hidden"  value="'+id+'" name="vendor_id[]"></td>';
                vendor_html += '<td class="text-center">'+name+'</td>';
                vendor_html += '<td class="text-center"><div class="form-group marginB0"><input type="text" name="shipment_provider[]" class="form-control marginB0" placeholder="Enter Shipment Provider"></div></td>';
                    vendor_html += '<td class="text-center "><div class="form-group marginB0"><input type="text" name="docket_no[]" class="form-control marginB0" placeholder="Enter Docket No"></div></td>';
					vendor_html += '<td class="text-center "><div class="form-group marginB0"><input type="text" name="remarks_indv[]" class="form-control marginB0" placeholder="Enter Remarks"></div></td>';
                    vendor_html += '<td class="text-center"><a href="javascript:void(0)" onclick="removeVendor('+id+')" class="btn btn-danger">Remove</a></td>';
                vendor_html += '</tr>';

                $("#tab_data").append(vendor_html);
            }else{
                alert(name+ ' Already added');
            }
        }else{
            alert('Please Select Any Vendor');
        }
    }


 
    function removeVendor(row_id) {
        v_list.pop(row_id);
        $('#row_'+row_id).remove();
    }
	
	
	 function item_form(){
        var img_path = '<?php echo asset('img/pre-order-img.jpg')?>' 
        $('#cat_1').val('');
        $('#cat_2').val('');
		$('#cat_3').val('');
		$('#cat_4').val('');
        $('#previewing').attr('src',img_path);
        $('#file').val('').css("color","black");;
    }
	
	 function addRfq() {
        if($.trim($("#tab_data").html())==''){
            $("#tab_message").text('Please Add a Vendor !').css('color', 'red').show();
            return false;
        }else{
            $('#tab_message').text('');
			return true;
        }
    }
	

function getOptionFilter() {
 var search_str=$('#search_exist').val();
 $('.error_msg').text('');
 if(search_str=='')
 {
	 $('.error_msg').text('Please Enter Search Keyword!').css('color', 'red').show();
	 return false;
 }
 //$('.loading').show();
 
 $('#mySidenavR').show();
 var token='{{csrf_token()}}';
 		$.ajax({
			type: 'GET', 
			url: '{{route("get-option-filter")}}',
			data: {search_str:search_str},
			success:function(data)
			{
				
        		$('#mySidenavR').html(data);
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});
}


$( "#hide-filter" ).click(function() {
	$('#mySidenavR').hide();
 filter();
});

$('.selectSearch').click(function(){
	     var selectcat1 = $('#search_exist').val();
		 var selectcat2 = $('#selectcat2').val();
		 var selectcat3 = $('#selectcat3').val();
		 var selectcat4 = $('#selectcat4').val();
		filter(selectcat1,selectcat2,selectcat3,selectcat4) ;
});
function filter(selectcat1='',selectcat2='',selectcat3='',selectcat4=''){
	   $('.loading').show();
 		$.ajax({
			type: 'GET', 
			url: '{{route("search-item-by-keyword")}}',
			data: {selectcat1:selectcat1,selectcat2:selectcat2,selectcat3:selectcat3,selectcat4:selectcat4},
			success:function(data)
			{
        		$('#existItemLIstModal').html(data);
				$('.loading').hide();
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});
}

</script>   