@include('layouts.header')
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <!--<div class="row">
                    <div class="col-lg-12 title-top">
                    <div class="row page-header">
                    	<div class="col-sm-6">
                        	<h1>Vendor <small>Create GIT</small></h1>
                        </div>
                        <div class="col-sm-6">
                        	<a href="javascript:void(0)" class="btn btn-success">Add GIT</a>
                        </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    
                    
                    <div class="col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i> Add New Vendor</h3>
                            </div>
                            <div class="clearfix"></div>
                              
                            @if(isset($errors))
                            @foreach ($errors->all() as $error)

                               <div style="padding-left: 145px;" class="alert alert-success alert-dismissable">{{ $error }}</div>
                            
                            @endforeach
                          @endif
                            <div class="panel-body custome-table">
                            <form action="{{url('insert-vendor')}}" method="post">	
                                <div class="row shorting-area">
                                	{{ csrf_field() }}
                                    <div class="col-sm-12">
            							<h5>First Name :</h5>
            							<input type="text" placeholder="" name="first_name" required class="form-control"/>
            						</div>
                                    <div class="col-sm-12">
            							<h5>Last Name :</h5>
            							<input type="text" placeholder="" name="last_name" required class="form-control"/>
            						</div>
                                    <div class="col-sm-12">
                                        <h5>Username :</h5>
                                        <input type="text" placeholder="" name="username" required class="form-control"/>
                                    </div>
                                    <div class="col-sm-12">
                                        <h5>Email ID :</h5>
                                        <input type="email" placeholder="" name="email" required class="form-control"/>
                                    </div>
                                    <!-- <div class="col-sm-12">
                                        <h5>Password :</h5>
                                        <input type="password" placeholder="" name="password" required class="form-control"/>
                                    </div>
                                    <div class="col-sm-12">
                                        <h5>Confirm Password :</h5>
                                        <input type="password" placeholder="" name="confirm_password" required class="form-control"/>
                                    </div> -->
                                    
                                    <div class="col-sm-12">
                                        <h5>Department :</h5>
                                            <select name="department" id="" class="form-control btn btn-default btn-select" required="required">
                                                <option  value="">Select Department</option>
                                                @foreach($Department as $value)
                                                <option  value="{{$value->id}}">{{$value->name}}</option>
                                                @endforeach
                                            </select>
                                    </div> 
                                    <div class="col-sm-12">
                                        <h5>Section :</h5>
                                        <select name="section" id="" class="form-control btn btn-default btn-select" required="required">
                                                <option  value="">Select Section</option>
                                                @foreach($Section as $value)
                                                <option  value="{{$value->id}}">{{$value->name}}</option>
                                                @endforeach
                                                
                                        </select>
                                    </div>
                                    <div class="col-sm-12">
                                        <h5>Devision :</h5>
                                        <select name="devision" id="" class="form-control btn btn-default btn-select" required="required">
                                                <option  value="">Select Devision</option>
                                                @foreach($Devision as $value)
                                                <option  value="{{$value->id}}">{{$value->name}}</option>
                                                @endforeach
                                                
                                        </select>
                                    </div> 
                                    <div class="col-sm-12">
                                        <h5>Mobile :</h5>
                                        <input type="text" placeholder="" name="mobile" required class="form-control"/>
                                    </div> 
                                    <div class="col-sm-12">
            							<h5>Adderess :</h5>
            							<textarea placeholder="" name="adderess" required class="form-control"></textarea>
            						</div> 
                                </div> 
                            <br/> 
                              <div class="col-sm-12 quotation-pop">
            						<input type="submit" value="submit" class="pup-btn" style="margin-top:0;"></input>
            				</div>
                            </form> 
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@include('layouts.footer')  