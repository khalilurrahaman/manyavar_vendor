<!--<h5>Vendor List Filter</h5>-->

<div class="filter-right">
<div class="filter-area list-filter pop-filter">
<!--<h5>Vendor List Filter</h5>-->
    <div><a href="javascript:void(0)" class="closebtn" id="hide-filter" >×<div class="clearfix"></div></a></div>
    <div class="clear"></div>
    
       
        <div class="col-sm-3">
            <input type="text" class="form-control" placeholder="Enter Colour"  id="selectcat2" name="selectcat2"/>  
        </div>
        <div class="col-sm-3">
           <input type="text" class="form-control" placeholder="Enter Cat3"  id="selectcat3" name="selectcat3"/>  
        </div>
        <div class="col-sm-3">
            
              <input type="text" class="form-control" placeholder="Enter Width/Size"  id="selectcat4" name="selectcat4"/>  
        </div>
        
        <div class="col-sm-3">
        	<a href="javascript:void(0)" class="btn btn-success selectSearch">Apply</a>
        </div>
<div class="clearfix"></div>        
	</div>
    <div class="clearfix"></div>
    </div> 

<script>

var cat2_list = <?php echo $cat2_list;?>;
$(document).ready(
  function () {
    $( "#selectcat2" ).autocomplete({
      source:cat2_list,
      autoFocus: true ,
      //change: function (event, ui) { alert(ui.item.value); }
    });
  }
);

var cat3_list = <?php echo $cat3_list;?>;
$(document).ready(
  function () {
    $( "#selectcat3" ).autocomplete({
      source:cat3_list,
      autoFocus: true ,
      //change: function (event, ui) { alert(ui.item.value); }
    });
  }
);

var cat4_list = <?php echo $cat4_list;?>;
$(document).ready(
  function () {
    $( "#selectcat4" ).autocomplete({
      source:cat4_list,
      autoFocus: true ,
      //change: function (event, ui) { alert(ui.item.value); }
    });
  }
);


$( "#hide-filter" ).click(function() {
	$('#mySidenavR').hide();
    $('#search_exist').val('');
    
    $('#existItemLIstModal').html('<tr><td class="text-center" colspan="6" style="border:none;">Search Product!</td></tr>');
    
 //filter();
});
$( "#hide-filter-main" ).click(function() {
    $('#mySidenavR').hide();
     $('#search_exist').val('');
     $('#existItemLIstModal').html('<tr><td class="text-center" colspan="6" style="border:none;">Search Product!</td></tr>');
 //filter();
});

function filter(selectcat1='',selectcat2='',selectcat3='',selectcat4=''){
		$('.loading').show(); 
 		$.ajax({
			type: 'GET', 
			url: '{{route("search-item-by-keyword")}}',
			data: {selectcat1:selectcat1,selectcat2:selectcat2,selectcat3:selectcat3,selectcat4:selectcat4},
			success:function(data)
			{
        		$('#existItemLIstModal').html(data);
				$('.loading').hide();
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});
}

$('.selectSearch').click(function(){
	     var selectcat1 = $('#search_exist').val();
		 var selectcat2 = $('#selectcat2').val();
		 var selectcat3 = $('#selectcat3').val();
		 var selectcat4 = $('#selectcat4').val();
		filter(selectcat1,selectcat2,selectcat3,selectcat4) ;
});


</script>
