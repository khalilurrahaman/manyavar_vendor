@include('layouts.header')


<style type="text/css">
      .blnk_txt {   
        animation-duration: 400ms;
        animation-name: blink;
        animation-iteration-count: infinite;
        animation-direction: alternate;
      }
     
      @keyframes blink {
        from {
          opacity: 1;
        }

        to {
          opacity: .5;
        }
      }
  </style>


<div id="page-wrapper">
  <div class="container-fluid inner-body-area">
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-bell" aria-hidden="true"></i> Notification
            (<?php if(!empty($notificationList) && count($notificationList)>0){
                      echo count($notificationList);
                    }else{ 
                      echo '0';
                    }?>)

                    @if($notificationList->unread!=0)
                     <span style="padding: 4px; border-radius: 10%;" class="btn-danger blnk_txt"><span id="total_unread">{{$notificationList->unread}}</span> Unread </span>
                    
                     @endif

            </h3>
          </div>
          <div class="panel-body">
            <div class="list-group"> @if(!empty($notificationList) && count($notificationList)>0)
              @foreach($notificationList as $notification) 
              
              <a href="javascript:void(0)" class="list-group-item notificationId{{$notification->id}}" > 
              <span onclick="readNotification({{$notification->id}})" class="badge"> 
              @if($notification->status=='NEW')
                <span id="read_btn_{{$notification->id}}" class="btn btn-danger btn-xs blnk_txt">New</span>
              @else

              @endif
              <?php 
        			  $time_one = new DateTime(date('Y-m-d H:i:s')) ;
        			  $time_two = new DateTime($notification->created_at);
        			  $difference = $time_one->diff($time_two);
        			  echo $difference->format('%a days %h hours %i minutes');?>
              ago </span>

              <span >
              
              <span onclick="redirectUrl({{$notification->id}},'{{$notification->red_url}}')">
              <i class="fa fa-fw fa-check"></i>{{$notification->message}} by {{$notification->sender_name}}
              </span>

              </a> 
             
              @endforeach
              @else 
              <i class="fa fa-fw fa-check"></i>Notification Board is Empty!
              @endif
              </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row --> 
  </div>
  <!-- /.container-fluid --> 
</div>
<!-- /#page-wrapper --> 
@include('layouts.footer')
<script language="JavaScript">

 function redirectUrl(id,url)
 {
    readNotification(id);
    setTimeout(function () {
        window.location.href=url;
    }, 800);
    
 }

 function readNotification(id)
 {        
            $.ajax({
            type: 'GET', 
            url: '{{route("admin-read-notification")}}',
            data: {id:id},
            success:function(data)
            {
                                
                if(data==1)
                {
                    $('#read_btn_'+id).remove();

                    var unread=$('#total_unread').text()-1;
                    if(unread!=0)
                      $('#total_unread').text(unread);
                    else
                      $('#total_unread').parent().hide();
                }
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
       
    }
</script>