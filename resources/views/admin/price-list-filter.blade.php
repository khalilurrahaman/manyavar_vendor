
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                               <!--  <th class="text-center">Sr. No</th> -->                                                
                                                <th class="text-center">Image</th>
                                                <th class="text-center">Vendor</th>
                                                <th class="text-center">Item</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Cat3</th>
                                                <th class="text-center">Width/Size</th>
                                                <th class="text-center">Cat6</th>
                                                <th class="text-center">Price</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                   <!--  <?php $inc=$data->perPage() * ($data->currentPage()-1);; ?> -->
                                    @foreach($data as $eachData)
                                     <?php $inc++; ?>
                                     
                                    <tr>
                                        <!-- <td class="text-center">{{$inc}}</td> -->
                                       
                                        <td class="text-center">
                                          <div class="gal"> 
                             <?php 
                              $item_image=URL::to('/img').'/pre-order-img.jpg';
                               if($eachData->image!='' && file_exists('upload/product/'.$eachData->image))
                               $item_image=URL::to('/upload/product').'/'.$eachData->image;
                                                            ?>
                               <a class="fancybox" rel="group{{$inc}}" href="{{$item_image}}" >
                                <img src="{{$item_image}}" width="150">
                               </a>
                                        
                               <a class="fancybox" rel="group{{$inc}}" href="{{$item_image}}" style="display:none;"><img src="{{$item_image}}" alt=""></a>
                              
                            </div>
                                         </td>
                                        <td class="text-center">{{$eachData->name}} </td>
                                        <td class="text-center">@if($eachData->ICODE){{($eachData->ICODE)}}@endif</td>                                        
                                        <td class="text-center">{{$eachData->cat1}} </td>
                                        <td class="text-center">{{$eachData->cat2}} </td>
                                        <td class="text-center">{{$eachData->cat3}} </td>
                                        <td class="text-center">{{$eachData->cat4}} </td>
                                        <td class="text-center">{{$eachData->CNAME6}} </td>
                                        <td class="text-center">
                                       {{$eachData->RATE}}
                                        @if($eachData->request_status==2)
                                        (old rate:{{$eachData->old_rate}} updated on {{date("d/m/y",strtotime($eachData->updated_at))}})
                                        @endif
                                        @if($eachData->request_status==1)
                                        <br><span>Requested Price:{{$eachData->requested_rate}}</span><br>
                                        <?php if(!empty($price_edit)){?> <a href="{{route('price-edit',$eachData->id)}}"  class="btn btn-success">Accept</a><?php }?> 
                                        <a href="{{route('reject-req-price', $eachData->id)}}" class="btn btn-danger">Reject</a>
                                        @endif
                                        @if($eachData->request_status==3)
                                        <br><span>Admin Negotiated Price:{{$eachData->  negotiate_rate}}</span><br>
                                         <a class="btn btn-success">Pending</a> 
                                        @endif
                                        @if($eachData->request_status==4)
                                        <br><span>Requested Price:{{$eachData->requested_rate}}</span><br>
                                        @if(!empty($eachData->negotiate_rate))<br><span>Admin Negotiated Price:{{$eachData->negotiate_rate}}</span><br>@endif
                                         <a class="btn btn-danger">Rejected</a> 
                                        @endif
                                        </td>
                                       
                                    </tr>
                                    @endforeach                            
                                </tbody>
                               </table>
                              </div>
                              <!-- <div class="row">
                              <div class="col-sm-6">
                              	<div class="dataTables_info" role="status" aria-live="polite">Showing {{$data->count()}} of {{$data->total()}} entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                        {{ $data->links() }}
                                    </nav>
                                  </div>
                              </div> -->
<script type="text/javascript">


    function ConfirmDelete(id)
        {
            var x = confirm("Are you sure you want to Delete?");
            if (x)
                return true;
            else
                return false;
        }
		$(document).ready(function () {
      
            $('#example').dataTable({
        'order': [[ 1, "asc" ]],//Default Column Attribute
        'pageLength': 10,//Per Page Item
         "searching": false,
        'dom': 'Bfrtip',//Header Info               
        'buttons': ['excel'],//['copy', 'csv', 'excel', 'pdf', 'print']
                'aoColumnDefs': [          
                                   { aTargets: [ 0 ], bSortable: false },
                                   { aTargets: [ 1 ], bSortable: true },
                                   { aTargets: [ 2 ], bSortable: true },
                                   { aTargets: [ 3 ], bSortable: false },
                                   { aTargets: [ 4 ], bSortable: false },
                                   { aTargets: [ 5 ], bSortable: false },
                                   { aTargets: [ 6 ], bSortable: false },
                                   { aTargets: [ 7 ], bSortable: false },
                                   { aTargets: [ 8 ], bSortable: false }
                                ],
                "oLanguage": {
                                "oPaginate": {
                                                "sNext": '<i class="fa fa-chevron-right" ></i>',
                                                "sPrevious": '<i class="fa fa-chevron-left" ></i>'
                                            }
                            }
            });
        });


    setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 2000);

    
</script>
