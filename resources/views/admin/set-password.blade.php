@include('layouts.header')
    <!--add user popup Modal Start-->
<div class=" quotation-pop" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Set Vendor Password</h4>
          </div>
             @if(isset($errors))
                            @foreach ($errors->all() as $error)

                               <div style="padding-left: 145px;" class="alert alert-success alert-dismissable">{{ $error }}</div>
                            
                            @endforeach
                          @endif

                   <div class="modal-body">  
                      <form action="{{route('update-passss')}}" method="post">  
                       <div class="row shorting-area pricechange">
                       {{ csrf_field() }}
                    <input type="hidden"  name="id" value='{{$username->id}}' >           
                      <div class="col-sm-12">
                            <h5>User Email Id :</h5>
                            <input type="text" name="email" value="{{$username->email}}"  class="form-control" readonly>
                        </div>

                        <div class="col-sm-12">
                            <h5>Password :</h5>
                            <input type="password" name="pass" id="pass" minlength="6" class="form-control">
                            <span class="error class_err" id="pass_message" ></span>
                        </div>
                        <div class="col-sm-12">
                            <h5>Confirm Password :</h5>
                            <input type="password" name="con_pass"  class="form-control">
                        </div>
                       
                        <div class="clearfix"></div>
                        <div class="col-sm-6">
                        <input type="submit" name="submit" value="Save" onclick="return validation()" class="pup-btn">
                        </div>
                        <div class="col-sm-6">
                          <a href="{{route('ven-listing')}}" class="pup-btn">Cancel</a>
                      </div>

                        <div class="clearfix"></div>
                    </div>
                     </form> 
                
           </div>
        </div>
    </div>
</div>
<!--add user popup  Modal End-->
@include('layouts.footer')
<script type="text/javascript">
  function validation()
   {
       var data=$('#pass').val();
       if(data.length<6)
       {
           $("#pass_message").text('password strength should be at least six characters.').css("color", "red").show();
           return false;
       }
       else
       {
            $('#pass_message').hide();
            return true;
       }

    
   
   }
</script>