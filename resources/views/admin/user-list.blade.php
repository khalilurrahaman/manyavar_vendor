@include('layouts.header')
<!--------------------------------Start Shorting-------------------------->
<style>
.dt-buttons{margin-right:0px !important;}
.dt-buttons>a {padding-left:18px;}
</style>
<script src="{{asset('dataTable/css/jquery.dataTables.min.css')}}"></script>
<script src="{{asset('dataTable/css/buttons.dataTables.min.css')}}"></script>

   
<script src="{{asset('dataTable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('dataTable/js/dataTables.buttons.min.js')}}"></script>   
<script src="{{asset('dataTable/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('dataTable/js/jszip.min.js')}}"></script>   
<script src="{{asset('dataTable/js/pdfmake.min.js')}}"></script>
<script src="{{asset('dataTable/js/vfs_fonts.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.print.min.js ')}}"></script>
<!--------------------------------End Shorting-------------------------->
<?php 
use App\Http\Controllers\Controller;
if(Controller::userAccessibilityCheck('user_add'))
	$user_add_exist=1;
if(Controller::userAccessibilityCheck('edit_user'))
	$edit_user_exist=1;
if(Controller::userAccessibilityCheck('delete_user'))
	$delete_user_exist=1;
if(Controller::userAccessibilityCheck('status_user'))
	$status_user_exist=1;
?>

        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <div class="row">
                    <div class="col-lg-12">
                    @if(isset($errors))
                        @foreach ($errors->all() as $error)

                                        <div class="btn btn-success" id="hide-me" style="width:100%;background-color: green;border: none;">{{ $error }}</div>
                                         @endforeach
                      @endif
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                	<div class="col-sm-6"><h3 class="panel-title">
                                    <i class="fa fa-users" aria-hidden="true"></i>User Listing</h3></div>
                                    <div class="col-sm-6">
                                    
                                    <div class="shorting-area">
                            		
                              	<?php if(!empty($user_add_exist)){?> 
                                <div class="col-sm-3 pull-right">
                                        <a href="{{route('ad-user')}}" class="btn btn-success">
                                        <i class="fa fa-plus" aria-hidden="true"></i> Add New</a>
                                </div>
                                <?php }?>  
                            </div>
                                    
                                    
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Name</th>
                                                <th class="text-center">E-mail ID</th>
                                                <th class="text-center">Phone No</th>
                                                <th class="text-center">Address</th>
                                                <th class="text-center">Division</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    @foreach($userList as $eachUser)
                                     
                                    <tr>
                                        <td class="text-center">{{$eachUser->name}}</td>
                                        <td class="text-center">{{$eachUser->email}}</td>
                                        <td class="text-center">{{$eachUser->mobile}}</td>
                                        <td class="text-center">{{$eachUser->address}}</td>
                                        <td class="text-center">
                                        <?php
                                        $division=explode(',',$eachUser->devision_id);
                                        $i=count($division);
                                        $all_division_name='';
                                        for($j=0;$j<$i;$j++)
                                        {
                                            $division_name=$data=App\Models\Devision::where('id','=',$division[$j])->first();

                                           $all_division_name=$all_division_name.$division_name['name'].',';
                                        }

                                        echo  rtrim($all_division_name,',');
                                        ?>
                                        </td>
                                        
                                        <td class="text-center">
                                    <?php if(!empty($edit_user_exist)){?>   
                                    <a href="{{route('admin-edit-user',$eachUser->id)}}" class="btn btn-primary">Edit</a>&nbsp
                                    <?php }?>
                                    <?php if(!empty($delete_user_exist)){?>   
                                    <a href="{{route('admin-del-user',$eachUser->id)}}" onclick="return deleteConfirm()" class="btn btn-danger">Delete</a>&nbsp
                                    <?php }?>
                                    <?php if(!empty($status_user_exist)){?>   
                                    @if($eachUser->status==1)<a href="{{route('user-archieve',$eachUser->id)}}"onclick="return ConfirmDeactive()" class="btn btn-warning">Inactivate</a>@else<a href="{{route('users-activate',$eachUser->id)}}" onclick="return ConfirmActive()" class="btn btn-success">Activate</a>@endif
                                     <?php }?>
                                     </td>
                                    </tr>
                                    @endforeach                            
                                </tbody>
                               </table>
                              </div>
                              
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
        <!--add user popup Modal Start-->
<div id="adduser" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add New User</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
            <div class="col-sm-6">
                <h5>Name :</h5>
                <input type="text" class="form-control">
            </div>
            <div class="col-sm-6">
                <h5>Department :</h5>
                <div class="shorting-area">
                              <div class="select-full">
                                        <a class="btn btn-default btn-select">
                                            <input type="hidden" class="btn-select-input" id="" name="" value="" />
                                            <span class="btn-select-value">Select an Item</span>
                                            <span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                                            <ul>
                                                <li class="selected">Select...</li>
                                                <li>Lorem Ipsum 1</li>
                                                <li>Lorem Ipsum 2</li>
                                            </ul>
                                        </a>
                                </div>
                            </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <h5>Email ID :</h5>
                <input type="text" class="form-control">
            </div>
            <div class="col-sm-6">
                <h5>Phone No :</h5>
                <input type="text" class="form-control">
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <h5>Address :</h5>
            <textarea class="form-control">... </textarea>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="pup-btn">Add User</a>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<script type="text/javascript">
function deleteConfirm()
{
    if(confirm('Are you sure'))
    {
        return true;
    }
    else
    {
        return false;
    }
}
$(document).ready(function () {
            
            $('#example').dataTable({
                'order': [[ 0, "asc" ]],//Default Column Attribute
                'pageLength': 10,//Per Page Item
                 "searching": false,
                'dom': 'Bfrtip',//Header Info               
                'buttons': [ {extend: 'excel',exportOptions: {columns: [0,1,2,3,4]},footer: false}],
                'aoColumnDefs': [          
                                   { aTargets: [ 0 ], bSortable: true },
                                   { aTargets: [ 1 ], bSortable: false },
                                   { aTargets: [ 2 ], bSortable: true },
                                   { aTargets: [ 3 ], bSortable: false },
                                   { aTargets: [ 4 ], bSortable: false },
                                   { aTargets: [ 5 ], bSortable: false }
                                ],
                "oLanguage": {
                                "oPaginate": {
                                                "sNext": '<i class="fa fa-chevron-right" ></i>',
                                                "sPrevious": '<i class="fa fa-chevron-left" ></i>'
                                            }
                            }
            });
        });


setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 2000);



function ConfirmActive(id)
        {
            var x = confirm("Are you sure you want to Activate?");
            if (x)
                return true;
            else
                return false;
        }function ConfirmDeactive(id)
        {
            var x = confirm("Are you sure you want to Inactivate?");
            if (x)
                return true;
            else
                return false;
        }
</script>
<!--add user popup  Modal End-->
@include('layouts.footer')