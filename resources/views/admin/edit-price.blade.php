@include('layouts.header')
    <!--add user popup Modal Start-->
<div class=" quotation-pop" role="dialog">
    <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content none-shadow">
      <div class="modal-header ">
        <h4 class="modal-title">Edit Price</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
        <form action="{{route('price-update')}}" method="post">
        {{ csrf_field() }}
         @if(isset($errors))
                            @foreach ($errors->all() as $error)
                              <div style="padding-left: 145px;" class="alert alert-success alert-dismissable">{{ $error }}</div>
                            @endforeach
                    @endif
            <div class="col-sm-12">
                <h5>Vendor :</h5>
             <input type="hidden" class="form-control" name="id" id="priceId" value="{{$data->id}}">
              <input type="hidden" class="form-control" name="old_rate" id="old_rate" value="{{$data->RATE}}">
              <input type="hidden" class="form-control" name="req_price" id="req_price" value="{{$data->requested_rate}}">
                <input type="text" class="form-control" name="name" id="name" value="{{$data->getVendorByPrice->name}}" readonly>
            </div>
            <div class="col-sm-12">
                <h5>Item :</h5>
                <input type="text" class="form-control" name="name" id="name" value="{{$data->getItemByPrice->ICODE}}" readonly>
            </div>
            
            <div class="clearfix"></div>
             <div class="col-sm-12">
            <h5>Current Price :</h5>
            <textarea class="form-control" name="orgprice" id="" readonly>{{$data->RATE}}</textarea>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <h5>Requested Price :</h5>
            <textarea class="form-control" name="reqprice" id="" readonly>{{$data->requested_rate}}</textarea>
            </div>
            <div class="col-sm-12">
            <h5>Enter New Price :</h5>
            <textarea class="form-control" name="price" id="editprice" onkeypress="javascript:return isNumber(event)" required="required">{{$data->requested_rate}}</textarea>
            <span class=" error class_err" id="description_msg_edit" ></span>
            </div>
            
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <button class="pup-btn">Submit</button>
            </div>
            <div class="col-sm-6">
                <a href="{{route('price-list')}}" data-dismiss="modal" class="pup-btn">Cancel</a>
            </div>
        </form>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
  </div>
<!--add user popup  Modal End-->
@include('layouts.footer')
<script>
    // WRITE THE VALIDATION SCRIPT IN THE HEAD TAG.
    function isNumber(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
            return false;

        return true;
    }    
</script>
 