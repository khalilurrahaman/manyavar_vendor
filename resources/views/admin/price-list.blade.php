<?php 
use App\Http\Controllers\Controller;
if(Controller::userAccessibilityCheck('price-edit'))
	$price_edit=1;

?>
@include('layouts.header')
<!-- CSS -->
<link rel="stylesheet" href="{{asset('fancybox/css/fancybox/jquery.fancybox-buttons.css')}}">
<link rel="stylesheet" href="{{asset('fancybox/css/fancybox/jquery.fancybox-thumbs.css')}}">
<link rel="stylesheet" href="{{asset('fancybox/css/fancybox/jquery.fancybox.css')}}">
<link rel="stylesheet" href="{{asset('fancybox/demo/demo.css')}}">
<!-- DELETE -->

<style>
.dt-buttons{margin-right:0px !important;}
.dt-buttons>a {padding-left:18px;}
</style>
<link rel="stylesheet" href="{{asset('autocomplete/jquery-ui.css')}}" />
<script src="{{asset('autocomplete/jquery-ui.js')}}"></script>

<script src="{{asset('dataTable/css/jquery.dataTables.min.css')}}"></script>
<script src="{{asset('dataTable/css/buttons.dataTables.min.css')}}"></script>

   
<script src="{{asset('dataTable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('dataTable/js/dataTables.buttons.min.js')}}"></script>   
<script src="{{asset('dataTable/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('dataTable/js/jszip.min.js')}}"></script>   
<script src="{{asset('dataTable/js/pdfmake.min.js')}}"></script>
<script src="{{asset('dataTable/js/vfs_fonts.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.print.min.js ')}}"></script>
   
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                	<div class="col-sm-6"><h3 class="panel-title">
                                    <i class="fa fa-users" aria-hidden="true"></i> Price Listing</h3>
                                    </div>
                                    <div class="col-sm-6">
                                    
                                    <div class="shorting-area">
                                    <div class="pull-right">
                                            <a href="javascript:void(0)" id="show-btn-main" class="btn btn-success">
                                            <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                    </div>
                                    </div>
                                    </div>
                                </div>
                                
                             @if(isset($errors))
                            @foreach ($errors->all() as $error)
                              <div style="padding-left: 145px;" id="hide-me" class="alert alert-success alert-dismissable">{{ $error }}</div>
                            @endforeach
                            @endif    
                                
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                                <div class="row shorting-area">
                              
                                <div class="col-sm-12">
                                <div id="mySidenavR-main" class="sidenavR" style="display:none;">
                                <div class="clearfix error_msg_main"></div>
                                    <div class="filter-right">
                                        <div class="filter-area list-filter">
                                        <!--<h5>Vendor List Filter</h5>-->
                                            <a href="{{route('price-list')}}" class="closebtn" id="hide-filter-main">×</a>
                                                <div class="col-sm-2">
                                                     <input type="text" class="form-control" placeholder="Search by vendor" id="vendor_autocomplete" name="vendor">
                                                </div>
                                                <div class="col-sm-2">
                                                     <input type="text" class="form-control" placeholder="Search by design" id="design_autocomplete" name="design">
                                                </div>
                                                <div class="col-sm-1">
                                                    <a href="javascript:void(0)" class="btn btn-success" onclick="filterbyKeyword();" style="width:60px !important;  margin-left: -6px !important;">Search</a>
                                                </div>
                                                <span id="mySidenavR-main-span" style="display:none;"></span>
                                            <div class="clearfix"></div>        
                                        </div>                              
                                        <div class="clearfix"></div>
                                    </div>    
                                </div>
                                </div>
                                </div>
                                <span id="filterlist">
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <!-- <th class="text-center">Sr. No</th> -->                                 
                                                <th class="text-center">Image</th>
                                                <th class="text-center">Vendor</th>
                                                <th class="text-center">Item</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Cat3</th>
                                                <th class="text-center">Width/Size</th>
                                                <th class="text-center">Cat6</th>
                                                <th class="text-center">Price</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <?php $inc=0; ?>
                                    @foreach($data as $eachData)
                                     <?php $inc++; ?>
                                     
                                    <tr>
                                        <!-- <td class="text-center">{{$inc}}</td> -->
                                        <td class="text-center">
                            <div class="gal"> 
                             <?php 
                              $item_image=URL::to('/img').'/pre-order-img.jpg';
                               if($eachData->image!='' && file_exists('upload/product/'.$eachData->image))
                               $item_image=URL::to('/upload/product').'/'.$eachData->image;
                                                            ?>
                               <a class="fancybox" rel="group{{$inc}}" href="{{$item_image}}" >
                                <img src="{{$item_image}}" width="150">
                               </a>
                                        
                               <a class="fancybox" rel="group{{$inc}}" href="{{$item_image}}" style="display:none;"><img src="{{$item_image}}" alt=""></a>
                              
                            </div>
                                         </td>

                                        <td class="text-center">{{$eachData->name}} </td>
                                        <td class="text-center">@if($eachData->ICODE){{($eachData->ICODE)}}@endif</td>                                        
                                        <td class="text-center">{{$eachData->cat1}} </td>
                                        <td class="text-center">{{$eachData->cat2}} </td>
                                        <td class="text-center">{{$eachData->cat3}} </td>
                                        <td class="text-center">{{$eachData->cat4}} </td>
                                        <td class="text-center">{{$eachData->CNAME6}} </td>
                                        <td class="text-center">
                                        {{$eachData->RATE}}
                                        @if($eachData->request_status==2)
                                        (old rate:{{$eachData->old_rate}} updated on {{date("d/m/y",strtotime($eachData->updated_at))}})
                                        @endif
                                        @if($eachData->request_status==1)
                                        <br><span>Requested Price:{{$eachData->requested_rate}}</span><br>
                                        <?php if(!empty($price_edit)){?> <a href="{{route('price-edit',$eachData->id)}}"  class="btn btn-success">Accept</a><?php }?> 
                                        <a href="{{route('reject-req-price', $eachData->id)}}" class="btn btn-danger">Reject</a>
                                        @endif
                                        @if($eachData->request_status==3)
                                        <br><span>Admin Negotiated Price:{{$eachData->  negotiate_rate}}</span><br>
                                         <a class="btn btn-success">Pending</a> 
                                        @endif
                                        @if($eachData->request_status==4)
                                        <br><span>Requested Price:{{$eachData->requested_rate}}</span><br>
                                        @if(!empty($eachData->negotiate_rate))<br><span>Admin Negotiated Price:{{$eachData->negotiate_rate}}</span><br>@endif
                                         <a class="btn btn-danger">Rejected</a> 
                                        @endif
                                        </td>
                                       
                                    </tr>
                                    @endforeach                            
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                             
                              </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
 
<script type="text/javascript">


    function ConfirmDelete(id)
        {
            var x = confirm("Are you sure you want to Delete?");
            if (x)
                return true;
            else
                return false;
        }
		$(document).ready(function () {
			
            $('#example').dataTable({
				//'order': [[ 1, "asc" ]],//Default Column Attribute
				'pageLength': 10,//Per Page Item
				 "searching": false,
				'dom': 'Bfrtip',//Header Info               
				'buttons': ['excel'],//['copy', 'csv', 'excel', 'pdf', 'print']
                'aoColumnDefs': [          
                                   { aTargets: [ 0 ], bSortable: false },
                                   { aTargets: [ 1 ], bSortable: true },
                                   { aTargets: [ 2 ], bSortable: true },
                                   { aTargets: [ 3 ], bSortable: false },
                                   { aTargets: [ 4 ], bSortable: false },
                                   { aTargets: [ 5 ], bSortable: false },
                                   { aTargets: [ 6 ], bSortable: false },
                                   { aTargets: [ 7 ], bSortable: false },
                                   { aTargets: [ 8 ], bSortable: false }
                                ],
                "oLanguage": {
                                "oPaginate": {
                                                "sNext": '<i class="fa fa-chevron-right" ></i>',
                                                "sPrevious": '<i class="fa fa-chevron-left" ></i>'
                                            }
                            }
            });
        });


    setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 2000);

    $(document).ready(function(){
    $("#hide-filter-main").click(function(){
        $("#mySidenavR-main").hide();
        filterSample();
    });
    $("#show-btn-main").click(function(){
        $("#mySidenavR-main").show();
    });
    });

    $( "#vendor_autocomplete" ).autocomplete({
        source: "{{route('search-autocomplete-vendor')}}",
        minLength: 1,

          select: function(event, ui) { 
            $('#vendor_autocomplete').val(ui.item.value);
          }
        });

    $( "#design_autocomplete" ).autocomplete({
        source: "{{route('search-autocomplete-design')}}",
        minLength: 1,

          select: function(event, ui) { 
            $('#design_autocomplete').val(ui.item.value);
          }
        });

    function filterbyKeyword(){
       var vendor = $('#vendor_autocomplete').val();
       var design = $('#design_autocomplete').val();
       $('.error_msg_main').text('');
         if(vendor =='' && design =='')
         {
             $('.error_msg_main').text('Please Enter Search Keyword!').css('color', 'red').show();
             return false;
         }
       $('.loading').show();
       
        $.ajax({
            type: 'GET', 
            url: '{{route("search-item-by-keyword-admin")}}',
            data: {selectcat1:vendor,selectcat2:design},
            success:function(data)
            {
                $('#filterlist').html(data);
                $('.loading').hide();
            },
            error: function (error) 
            { 
                alert('Error Occured Please Try Again');
            }
        });
    }
</script>

<!-- ********************************************** -->
<!-- JavaScript at the bottom for fast page loading -->
<!-- ********************************************** -->

    <!-- Grab Google CDN's jQuery, fall back to local if offline -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="{{asset('fancybox/js/libs/jquery-1.7.1.min.js')}}"><\/script>')</script>
        
    <!-- FancyBox -->
        <script src="{{asset('fancybox/js/fancybox/jquery.fancybox.js')}}"></script>
        <script src="{{asset('fancybox/js/fancybox/jquery.fancybox-buttons.js')}}"></script>
        <script src="{{asset('fancybox/js/fancybox/jquery.fancybox-thumbs.js')}}"></script>
        <script src="{{asset('fancybox/js/fancybox/jquery.easing-1.3.pack.js')}}"></script>
        <script src="{{asset('fancybox/js/fancybox/jquery.mousewheel-3.0.6.pack.js')}}"></script>
        
        <script type="text/javascript">
        var nc=$.noConflict();
        nc(document).ready(function() {
        nc(".fancybox").fancybox();
        });
    </script>
@include('layouts.footer')
