@include('layouts.header')
<!-- CSS -->
<link rel="stylesheet" href="{{asset('fancybox/css/fancybox/jquery.fancybox-buttons.css')}}">
<link rel="stylesheet" href="{{asset('fancybox/css/fancybox/jquery.fancybox-thumbs.css')}}">
<link rel="stylesheet" href="{{asset('fancybox/css/fancybox/jquery.fancybox.css')}}">
<link rel="stylesheet" href="{{asset('fancybox/demo/demo.css')}}">
<!-- DELETE -->
<!--Start Shorting-->
<style>
.dt-buttons{margin-right:0px !important;}
.dt-buttons>a {padding-left:18px;}
</style>
<script src="{{asset('dataTable/css/jquery.dataTables.min.css')}}"></script> 
<script src="{{asset('dataTable/css/buttons.dataTables.min.css')}}"></script> 
<script src="{{asset('dataTable/js/jquery.dataTables.min.js')}}"></script> 
<script src="{{asset('dataTable/js/dataTables.buttons.min.js')}}"></script> 
<script src="{{asset('dataTable/js/buttons.flash.min.js')}}"></script> 
<script src="{{asset('dataTable/js/jszip.min.js')}}"></script> 
<script src="{{asset('dataTable/js/pdfmake.min.js')}}"></script> 
<script src="{{asset('dataTable/js/vfs_fonts.js')}}"></script> 
<script src="{{asset('dataTable/js/buttons.html5.min.js')}}"></script> 
<script src="{{asset('dataTable/js/buttons.print.min.js ')}}"></script> 

<link rel="stylesheet" href="{{asset('autocomplete/jquery-ui.css')}}" />
<script src="{{asset('autocomplete/jquery-ui.js')}}"></script>
<!--End Shorting-->
<div id="page-wrapper">
    <div class="container-fluid inner-body-area">                 
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default add-padding">
                    <div class="panel-heading ">
                        <div class="row">
                          <div class="col-sm-6">
                              <h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i> Purchase Order Listing</h3>
                            </div>
                            
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="panel-body custome-table">
                        <div class="tab-filter">
              <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab4default" data-toggle="tab" onclick="clearPrevData('next15');">PO to be expired in next 15 days</a></li>
                    <li><a href="#tab5default" data-toggle="tab" onclick="clearPrevData('prev15');">PO expired</a></li>

                    <li ><a href="#tab1default" data-toggle="tab" onclick="clearPrevData('vendorwise');">PO List by Vendorwise PO No & PO Date</a></li>
                    <!-- <li><a href="#tab2default" data-toggle="tab" onclick="clearPrevData();">PO List by Vendor Name</a></li> -->
                    <li><a href="#tab3default" data-toggle="tab" onclick="clearPrevData();">PO List by Design, Color, Cat3, Width / Size,Cat6</a></li>

                    

                  </ul>
                </div>
                <div class="panel-body">
                  <div class="tab-content">
                    <div class="tab-pane fade" id="tab1default">
                      <div class="row shorting-area">
                        <div class="filter-area">


                          <div class="col-sm-3">
                            <div class="form-group">
                              <div class="input-group date">
                                <input type="text" class="form-control" placeholder="Enter Vendor Name"  id="vendore_name" name="vendore_name"/> </div>
                            </div>
                            <div class="clearfix"></div>
                          </div>

                         <div class="col-sm-3">
                            <div class="form-group">
                              <div class="input-group date">
                                <input type="text" class="form-control date_picker" placeholder="PO date from(dd/mm/yyyy)"  id="due_from" name="due_from"/> </div>
                            </div>
                            <div class="clearfix"></div>
                          </div>


                          <div class="col-sm-3">
                            <div class="form-group">
                              <div class="input-group date">
                                <input type="text" class="form-control date_picker" placeholder="PO date to(dd/mm/yyyy)"  id="due_to" name="due_to"/> </div>
                            </div>
                            <div class="clearfix"></div>
                          </div>


                          <div class="col-sm-3 commonHideClass" style="display: none;">
                            <input type="text" class="form-control" placeholder="Enter PO No"  id="po_number" name="po_number"  />                            
                          </div>


                          <div class="col-sm-3 onlyHideClass"> 
                          <a href="javascript:void(0)" class="btn btn-success dateCheck" onclick="getFilterCriteria();">Search</a>
                            <div class="clearfix"></div>
                          </div>

                          <div class="col-sm-2 commonHideClass" style="display: none;"> 
                          <a href="javascript:void(0)" class="btn btn-success dateCheck" onclick="applyFilter();">Apply Filter</a>
                            <div class="clearfix"></div>
                          </div>
                          <div class="col-sm-1 commonHideClass" style="display: none;"> 
                          <a href="javascript:void(0)" class="btn btn-success dateCheck reset" onclick="">Clear</a>
                          <div class="clearfix"></div>  
                         </div>

                          <div class="clearfix"></div>
                        </div>
                      </div>
                    </div>
                    <!-- <div class="tab-pane fade" id="tab2default">
                      <div class="row shorting-area">
                        <div class="filter-area">
                          <div class="col-sm-3">
                          
                              <input type="text" class="form-control" placeholder="Enter Vendor Name"  id="vendore_name" name="vendore_name"/>                            
                          </div>
                          <div class="col-sm-2"> <a href="javascript:void(0)" class="btn btn-success" onclick="applyFilter();">Apply Filter</a>
                            <div class="clearfix"></div>
                          </div>
                          <div class="col-sm-1"> 
                          <a href="javascript:void(0)" class="btn btn-success dateCheck reset" onclick="">Clear</a>
                          <div class="clearfix"></div>  
                         </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                    </div> -->

                    <div class="tab-pane fade" id="tab3default">
                      <div class="row shorting-area">
                        <div class="filter-area">

                          <div class="col-sm-3">                           
                            <input type="text" class="form-control" placeholder="Enter Design"  id="category1" name="category1"/>  
                          </div>

                          <div class="col-sm-2">                       
                            <input type="text" class="form-control" placeholder="Enter Color"  id="category2" name="category2"/>  
                          </div>

                          <div class="col-sm-2">                        
                            <input type="text" class="form-control" placeholder="Enter Cat 3"  id="category3" name="category3"/>  
                          </div>

                          <div class="col-sm-2">                            
                            <input type="text" class="form-control" placeholder="Enter Width.Size"  id="category4" name="category4"/>  
                          </div>

                          <div class="col-sm-2">                            
                            <input type="text" class="form-control" placeholder="Enter Cat6"  id="category6" name="category6"/>  
                          </div>


                          <div class="col-sm-2"> <a href="javascript:void(0)" class="btn btn-success" onclick="applyFilter();">Apply Filter</a>
                            <div class="clearfix"></div>
                          </div>
                          <div class="col-sm-1"> 
                          <a href="javascript:void(0)" class="btn btn-success dateCheck reset" onclick="">Clear</a>
                          <div class="clearfix"></div>  
                         </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                    </div>


                     <div class="tab-pane fade in active" id="tab4default">
                       <input id="next15" name="next15" type="hidden" value="" >
                     </div>



                     <div class="tab-pane fade" id="tab5default">
                         <input id="prev15" name="prev15" type="hidden" value="">
                     </div>

                  </div>
                 <div class="empty_search_msg"></div>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
 <div class="clearfix"></div>
                        <span id="filterResult">
                            <div class="table-responsive ">
                                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <!--<th class="text-center">Sr. No</th>-->
                                            <th class="text-center">Image</th>
                                            <th class="text-center">PO. No</th>
                                            <th class="text-center">Vendor Name</th>
                                            <th class="text-center">PO. Date</th>
                                            <th class="text-center">Due. Date</th>
                                            <th class="text-center">Item Code</th>
                                            <th class="text-center">Design</th>
                                            <th class="text-center">Colour</th>
                                            <th class="text-center">Cat3</th>
                                            <th class="text-center">Size/Width</th>
                                             <th class="text-center">Cat 6</th>
                                            <th class="text-center">Price</th>
                                            <th class="text-center">Order Qty</th>
                                            <th class="text-center">Received Qty</th>
                                            <th class="text-center">Cancelled Qty</th>
                                            <th class="text-center">GIT Qty</th>
                                            <th class="text-center">Adhoc Qty</th>
                                            <th class="text-center">Pending Qty</th>
                                            
                                        </tr>
                                    </thead>
                                     <!-- <tfoot>
                                          <tr>
                                              <th colspan="4" style="text-align:right">Total:</th>
                                              <th></th>
                                          </tr>
                                      </tfoot> -->

                                    <tbody>
                                        <tr>
                                          <td style="font-size: 18px;" class="text-center" colspan="18" > Please enter your search keyword! </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    <!-- /.row -->
    </div>
<!-- /.container-fluid -->
</div>
 <!-- /#page-wrapper -->
@include('layouts.footer')
<!--add vendorChangecutting Modal Start-->




<script>
var ptoken='{{csrf_token()}}';
var cutting_url='{{route("purchase-order-request")}}';
</script>
<script src="{{asset('js/custom/admin_purchase_order.js')}}"></script>  
<script>      
    
  $(document).ready(function () {   //For default data load          
    //clearPrevData();
    clearPrevData('next15');
        
  });     
  
  function clearPrevData(pos='')//For Clear Data of prev search
  {   
      

      $("#po_number").val('');
      $("#category1").val('');
      $("#category2").val('');
      $("#category3").val('');
      $("#category4").val('');
      $("#category6").val('');
      $("#due_from").val('');
      $("#due_to").val('');
      $("#vendore_name").val('');  

      $("#next15").val('');
      $("#prev15").val('');

      if(pos=='vendorwise')
      {
        $('.onlyHideClass').show();
        $('.commonHideClass').hide();
      }

      if(pos=='next15')
      {
        $("#next15").val(1);
        applyFilter();
      }
      else if(pos=='prev15')
      {
        $("#prev15").val(2);
        applyFilter();
      }else
      {
        filter(); 
      }
      

      // var blank_msg='<tr><td style="font-size: 18px;" class="text-center" colspan="18" > Please enter your search keyword! </td></tr>';

      // $("#search_result_body").html(blank_msg); 


      //filter(); 
  }

/*
    $('#due_from').on('changeDate', function(ev){
        $(this).datepicker('hide');
    });
    $('#due_to').on('changeDate', function(ev){
        $(this).datepicker('hide');
    });
*/

    $(function() {
      $('body').on('click', '.pagination a', function(e) {
          e.preventDefault();

          var page=$(this).attr('href').split('page=')[1];
          
          var po_number=$("#po_number").val();
          var category1=$("#category1").val();
          var category2=$("#category2").val();
          var category3=$("#category3").val();
          var category4=$("#category4").val();
          var category6=$("#category6").val();
          var due_from=$("#due_from").val();

          var next15=$("#next15").val();
          var prev15=$("#prev15").val();

          var due_to=$("#due_to").val();
          var vendore_name=$("#vendore_name").val();
      
     
          filter(page,po_number,category1,category2,category3,category4,category6,due_from,due_to,vendore_name,next15,prev15);
          
      });

      
  });

function applyFilter(){

    var due_from=$("#due_from").val();
    var due_to=$("#due_to").val();
    var po_number=$("#po_number").val();

    var vendore_name=$("#vendore_name").val();

    var category1=$("#category1").val();
    var category2=$("#category2").val();
    var category3=$("#category3").val();
    var category4=$("#category4").val();
    var category6=$("#category6").val();

    var next15=$("#next15").val();
    var prev15=$("#prev15").val();
   
    
    
    $(".empty_search_msg").hide();
   
    if(po_number=='' && category1=='' && category2=='' && category3=='' && category4=='' && category6=='' && due_from=='' && due_to=='' && vendore_name=='' && next15=='' && prev15==''){
     
      $(".empty_search_msg").text('Please Enter at least one criteria for filter!').css('color', 'red').show();
    }
    else{ 
      filter(1,po_number,category1,category2,category3,category4,category6,due_from,due_to,vendore_name,next15,prev15);

    }
}



function filter(page=1,po_number='',category1='',category2='',category3='',category4='',category6='',due_from='',due_to='',vendore_name='',next15='',prev15=''){  //For fetch Filter data 
  
  $('.loading').show();
  $.ajax({
    type: 'GET', 
    url: '{{route("filter-purchase-order")}}',
    data: {po_number:po_number,category1:category1,category2:category2,category3:category3,category4:category4,category6:category6,due_from:due_from,due_to:due_to,vendore_name:vendore_name,page:page,next15:next15,prev15:prev15},
    success:function(data)
    {
      $('#filterResult').html(data);                           
            $('.loading').hide();            
    },
    error: function (error) 
    {
      
      alert('Error Occured Please Try Again');
    }
  })
}
var vendorList = <?php echo $vendorList;?>;
$(document).ready(
  function () {
    $( "#vendore_name" ).autocomplete({
      source:vendorList,
      autoFocus: true ,
      //change: function (event, ui) { alert(ui.item.value); }
    });
  }
);

var designlist = <?php echo $designlist;?>;
$(document).ready(
  function () {
    $( "#category1" ).autocomplete({
      source:designlist,
      autoFocus: true ,
      //change: function (event, ui) { alert(ui.item.value); }
    });
  }
);

var colorList = <?php echo $colorList;?>;
$(document).ready(
  function () {
    $( "#category2" ).autocomplete({
      source:colorList,
      autoFocus: true ,
      //change: function (event, ui) { alert(ui.item.value); }
    });
  }
);

function getFilterCriteria(){  //For fetch Filter data 
 
   var due_from=$("#due_from").val();
   var due_to=$("#due_to").val();
   var vendore_name=$("#vendore_name").val();
  $('.loading').show();
  $.ajax({
    type: 'GET', 
    url: '{{route("filter-purchase-order-po-list-auto")}}',
    data: {due_from:due_from,due_to:due_to,vendore_name:vendore_name},
    success:function(poList)
    {
      $( "#po_number" ).autocomplete({
      source:poList,
      autoFocus: true ,
      });
      $('.onlyHideClass').hide();
      $('.commonHideClass').show();                           
      $('.loading').hide();            
    },
    error: function (error) 
    {
      
      alert('Error Occured Please Try Again');
    }
  })
}
</script>
<style type="text/css">
.tab-filter .filter-area {padding:0;}
.tab-filter .form-group {margin:0; height:30px !important;}
.tab-filter {margin:12px 0;}
.tab-filter .panel-default {border-radius:0; background:#111; padding:15px;}
.tab-filter .nav-tabs>li {margin-bottom:0px;}
.tab-filter .nav-tabs>li>a {margin-right:9px; background:#f1651d; letter-spacing:0.6px; padding:10px 19px;}
.tab-filter .panel-body {background:#fff; padding:26px 15px;}
.tab-filter .panel.with-nav-tabs .panel-heading {padding:0; border-radius:0; background:transparent; color:#fff; border:0;}
.tab-filter .panel.with-nav-tabs .nav-tabs {border-bottom:none;}
.tab-filter .with-nav-tabs.panel-default .nav-tabs > li > a, .tab-filter .with-nav-tabs.panel-default .nav-tabs > li > a:hover,
.tab-filter .with-nav-tabs.panel-default .nav-tabs > li > a:focus {color: #fff; font-family: 'Archivo Narrow', sans-serif; font-size:16px; border-radius:0;}
.tab-filter .with-nav-tabs.panel-default .nav-tabs > .open > a, .tab-filter .with-nav-tabs.panel-default .nav-tabs > .open > a:hover,
.tab-filter .with-nav-tabs.panel-default .nav-tabs > .open > a:focus, .tab-filter .with-nav-tabs.panel-default .nav-tabs > li > a:hover,
.tab-filter .with-nav-tabs.panel-default .nav-tabs > li > a:focus {color:#777; background-color:#ddd; border-color:transparent;}
.tab-filter .with-nav-tabs.panel-default .nav-tabs > li.active > a, .tab-filter .with-nav-tabs.panel-default .nav-tabs > li.active > a:hover,
.tab-filter .with-nav-tabs.panel-default .nav-tabs > li.active > a:focus {color:#555; background-color:#fff; border-color:#ddd; 
border-bottom-color:transparent;}
</style>
<script type="text/javascript">
   //--For Clearing Filter Data Elements-----//
    $(".reset").bind("click", function() {
    $("input[type=text], textarea").val("");
    $("#po_number").val("");
    $("#category1").val("");
    $("#category2").val("");
    $("#category3").val("");
    $("#category4").val("");
    $("#category6").val(""); 

    $("#due_from").val('');
    $("#due_to").val('');
    $("#vendore_name").val('');
    $("#next15").val('');
    $("#prev15").val('');

    $("#po_number").hide();
    });   
</script>
<!-- ********************************************** -->
<!-- JavaScript at the bottom for fast page loading -->
<!-- ********************************************** -->

<!-- Grab Google CDN's jQuery, fall back to local if offline -->
    <script src="{{asset('js/jquery-jquery.min.js')}}"></script>
    <script>window.jQuery || document.write('<script src="{{asset('fancybox/js/libs/jquery-1.7.1.min.js')}}"><\/script>')</script>
    
<!-- FancyBox -->
    <script src="{{asset('fancybox/js/fancybox/jquery.fancybox.js')}}"></script>
    <script src="{{asset('fancybox/js/fancybox/jquery.fancybox-buttons.js')}}"></script>
    <script src="{{asset('fancybox/js/fancybox/jquery.fancybox-thumbs.js')}}"></script>
    <script src="{{asset('fancybox/js/fancybox/jquery.easing-1.3.pack.js')}}"></script>
    <script src="{{asset('fancybox/js/fancybox/jquery.mousewheel-3.0.6.pack.js')}}"></script>
    
    <script type="text/javascript">
        var nc=$.noConflict();
        nc(document).ready(function() {
        nc(".fancybox").fancybox();
        });
    </script>
