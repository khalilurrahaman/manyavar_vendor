@include('layouts.header')
    <!--add user popup Modal Start-->
<div class=" quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content none-shadow">
      <div class="modal-header">
        <h4 class="modal-title">Add New Department</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
        <form action="{{route('vendor-dept-add')}}" method="post">
        {{ csrf_field() }}
        @if(isset($errors))
                            @foreach ($errors->all() as $error)
                              <div style="padding-left: 145px;" class="alert alert-success alert-dismissable">{{ $error }}</div>
                            @endforeach
                    @endif
            <div class="col-sm-12">
                <h5>Name :</h5>
                <input type="text" class="form-control" name="name" id="name" >
                <span class=" error class_err" id="name_msg" ></span>
            </div>
            
            
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <h5>Description :</h5>
            <textarea class="form-control" name="description" id="department_description" ></textarea>
            <span class=" error class_err" id="description_msg" ></span>
            </div>
            
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <button class="pup-btn" onclick="return validateForm();">Add Department</button>
            </div>
            <div class="col-sm-6">
                <a href="{{route('departmnt-lising')}}" data-dismiss="modal" class="pup-btn">Cancel</a>
            </div>
        </form>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--add user popup  Modal End-->
@include('layouts.footer')

<script type="text/javascript">
    function validateForm()
  {    
    var ret_val=true;   
   
    var name=$("#name").val();
    if(name==''){$("#name_msg").text('Please Enter Name  !').show();ret_val=false;}
    else{$("#name_msg").hide();}

    var department_description=$("#department_description").val();
    if(department_description==''){$("#description_msg").text('Please Enter Description  !').show();ret_val=false;}
    else{$("#description_msg").hide();}


    return ret_val;

  }

    



</script>