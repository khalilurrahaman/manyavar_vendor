@include('layouts.header')
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <!--<div class="row">
                    <div class="col-lg-12 title-top">
                    <div class="row page-header">
                    	<div class="col-sm-6">
                        	<h1>Vendor <small>Purchase Order Listing</small></h1>
                        </div>
                        <div class="col-sm-6">
                        	<a href="javascript:void(0)" class="btn btn-success">Expand</a>
                        </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                	<div class="col-sm-6"><h3 class="panel-title">
                                    <i class="fa fa-users" aria-hidden="true"></i> Vendor Purchase Order Listing</h3></div>
                                    <div class="col-sm-6">
                                    
                                    <div class="shorting-area">
                            		
                              <div class="col-sm-4 pull-right">
                                        <a class="btn btn-default btn-select">
                                            <input type="hidden" class="btn-select-input" id="" name="" value="" />
                                            <span class="btn-select-value">Select an Item</span>
                                            <span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                                        	<ul>
                                                <li class="selected">Short By</li>
                                                <li>Due Date</li>
                                                <li>Order Date</li>
                                                <li>Pending Qty</li>
                                        	</ul>
                                        </a>
                                </div>
                                <div class="col-sm-3 pull-right">
                                        <a href="javascript:void(0)" id="show-btn" class="btn btn-success">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                </div>
                                <div class="col-sm-3 pull-right">
                                        <a href="{{url('add-vendor')}}" id="show-btn" class="btn btn-success">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Add Venor</a>
                                </div>
                            </div>
                                    
                                    
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                            <div class="row shorting-area">
                            <div class="col-sm-12">
                            <div id="mySidenavR" class="sidenavR" style="display:none;">
<div class="filter-right">
<div class="filter-area small-filter">
<!--<h5>Vendor List Filter</h5>-->
    <a href="javascript:void(0)" class="closebtn" id="hide-filter">×</a>
        <div class="col-sm-1">
            <a class="btn btn-default btn-select">
            	<input type="hidden" class="btn-select-input" id="" name="" value="" />
            	<span class="btn-select-value">Select an Item</span>
            	<span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                <ul>
                    <li class="selected">PO No</li>
                    <li>PO/001233/16-17</li>
                    <li>PO/001895/16-17</li>
                    <li>PO/001008/16-17</li>
                </ul>
            </a>
        </div>
        <div class="col-sm-1">
            <a class="btn btn-default btn-select">
            	<input type="hidden" class="btn-select-input" id="" name="" value="" />
            	<span class="btn-select-value">Select an Item</span>
            	<span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                <ul>
                    <li class="selected">Panding Qty</li>
                    <li>--</li>
                    <li>--</li>
                    <li>--</li>
                </ul>
            </a>
        </div>
        <div class="col-sm-1">
            <a class="btn btn-default btn-select">
            	<input type="hidden" class="btn-select-input" id="" name="" value="" />
            	<span class="btn-select-value">Select an Item</span>
            	<span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                <ul>
                    <li class="selected">Design</li>
                    <li>----</li>
                    <li>----</li>
                    <li>----</li>
                </ul>
            </a>
        </div>
        <div class="col-sm-1">
            <a class="btn btn-default btn-select">
            	<input type="hidden" class="btn-select-input" id="" name="" value="" />
            	<span class="btn-select-value">Select an Item</span>
            	<span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                <ul>
                    <li class="selected">Colour</li>
                    <li>----</li>
                    <li>----</li>
                    <li>----</li>
                </ul>
            </a>
        </div>
        <div class="col-sm-1">
            <a class="btn btn-default btn-select">
            	<input type="hidden" class="btn-select-input" id="" name="" value="" />
            	<span class="btn-select-value">Select an Item</span>
            	<span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                <ul>
                    <li class="selected">Width/Size</li>
                    <li>--</li>
                    <li>--</li>
                    <li>--</li>
                </ul>
            </a>
        </div>
        <div class="col-sm-1">
        <div class="form-group">
                <div class="input-group date">
                    <input type="text" class="form-control" placeholder="Due From"  id="example1"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-sm-1">
        <div class="form-group">
                <div class="input-group date">
                    <input type="text" class="form-control" placeholder="Due To"  id="example2"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-sm-1">
        	<a href="javascript:void(0)" class="btn btn-success">Apply Filter</a>
        </div>
<div class="clearfix"></div>        
	</div>
    <div class="clearfix"></div>
    </div>    
</div>
                            
                            </div>
                            
                            
                            </div>
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">ID</th>
                                                <th class="text-center">Username</th>
                                                <th class="text-center">First Name</th>
                                                <th class="text-center">Last Name</th>
                                                <th class="text-center">Email ID</th>
                                                <th class="last-width text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    @foreach($vendor_list as $data)
                                    <tr>
                                        <td class="text-center">{{$data->id}}</td>
                                        <td class="text-center">{{$data->username}}</td>
                                        <td class="text-center">{{$data->first_name}}</td>
                                        <td class="text-center">{{$data->last_name}}</td>                                       
                                        <td class="text-center">{{$data->email}}</td>
                                        <td class="last-width text-center">
<span ><a href="{{url('edit-vendor/'.$data->id)}}" data-toggle="tooltip"  title="Edit Vendor" class="tab-btn">EDIT</a></span>
<span ><a href="javascript:void(0)" data-toggle="tooltip" title="Delete Vendor" class="tab-btn">DELETE</a></span>


                            		</td>
                                    </tr>
                                    @endforeach
                                    
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                              <div class="col-sm-6">
                              	<div class="dataTables_info" role="status" aria-live="polite">Showing {{$vendor_list->count()}} of {{$vendor_list->total()}} entries</div>
                              </div>
                                
                                 
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                    {{ $vendor_list->links() }}
                                        <!-- <ul class="pagination">
                                            <li class="page-item">
                                                <a class="page-link" href="javascript:void(0)" aria-label="Previous">
                                                	<span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span>
                                                </a>
                                            </li>
                                        	<li class="page-item active"><a class="page-link" href="javascript:void(0)">1</a></li>
                                       		<li class="page-item"><a class="page-link" href="javascript:void(0)">2</a></li>
                                        	<li class="page-item"><a class="page-link" href="javascript:void(0)">3</a></li>
                                        	<li class="page-item"><a class="page-link" href="javascript:void(0)">4</a></li>
                                        	<li class="page-item"><a class="page-link" href="javascript:void(0)">5</a></li>
                                        	<li class="page-item">
                                        	<a class="page-link" href="javascript:void(0)" aria-label="Next">
                                        		<span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span>
                                        	</a>
                                        	</li>
                                        </ul> -->
                                        
                                    </nav>
                                  </div> 
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@include('layouts.footer')