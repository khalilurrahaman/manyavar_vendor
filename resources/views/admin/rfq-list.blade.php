@include('layouts.header')
<!-- CSS -->
<link rel="stylesheet" href="{{asset('fancybox/css/fancybox/jquery.fancybox-buttons.css')}}">
<link rel="stylesheet" href="{{asset('fancybox/css/fancybox/jquery.fancybox-thumbs.css')}}">
<link rel="stylesheet" href="{{asset('fancybox/css/fancybox/jquery.fancybox.css')}}">
<link rel="stylesheet" href="{{asset('fancybox/demo/demo.css')}}">
<!-- DELETE -->
<!---Start Shorting-->
<style>
.dt-buttons{margin-right:0px !important;}
.dt-buttons>a {padding-left:18px;}
</style>
<script src="{{asset('dataTable/css/jquery.dataTables.min.css')}}"></script>
<script src="{{asset('dataTable/css/buttons.dataTables.min.css')}}"></script>

   
<script src="{{asset('dataTable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('dataTable/js/dataTables.buttons.min.js')}}"></script>   
<script src="{{asset('dataTable/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('dataTable/js/jszip.min.js')}}"></script>   
<script src="{{asset('dataTable/js/pdfmake.min.js')}}"></script>
<script src="{{asset('dataTable/js/vfs_fonts.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.print.min.js ')}}"></script>
<!--End Shorting-->
<div id="page-wrapper">
    <div class="container-fluid inner-body-area">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default add-padding">
                    <div class="panel-heading">
                        <div class="row">
                        	<div class="col-sm-6">
                                <h3 class="panel-title">
                                    <i class="fa fa-users" aria-hidden="true"></i> View All REQ List
                                </h3>
                            </div>
                            <div class="col-sm-6">
                                <div class="row shorting-area vendor-filter">
                                
                                    <div class="col-sm-3 pull-right ">
                                        <a href="javascript:void(0)" id="show-btn" class="btn btn-success">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="panel-body custome-table">
                        <div class="row shorting-area">
                            <div class="col-sm-12">
                                <div id="mySidenavR" class="sidenavR" style="display:none;">
                                    <div class="filter-right">
                                        <div class="filter-area rfq-filter">
                                            <a href="javascript:void(0)" class="closebtn" id="hide-filter">×</a>
                                            <div class="col-sm-2">             
                                                <select class="select-drop" id="req_id" name="req_id">
                                                    <option value="">REQ ID</option>
                                                    @if(!empty($req_id_list) && count($req_id_list)>0)
                                                    @foreach($req_id_list as $req_id)
                                                        <option value="{{$req_id->req_id}}">{{$req_id->req_id}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="col-sm-2">
                                                <select class="select-drop" id="cat1" name="cat1">
                                                    <option value="">Design</option>
                                                    @if(!empty($cat1_list) && count($cat1_list)>0)
                                                    @foreach($cat1_list as $cat1)
                                                        <option value="{{$cat1->cat1}}">{{$cat1->cat1}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="col-sm-2">
                                                <select class="select-drop" id="cat2" name="cat2">
                                                    <option value="">Colour</option>
                                                    @if(!empty($cat2_list) && count($cat2_list)>0)
                                                    @foreach($cat2_list as $cat2)
                                                        <option value="{{$cat2->cat2}}">{{$cat2->cat2}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="col-sm-2">
                                                <select class="select-drop" id="name" name="name">
                                                    <option value="">Name</option>
                                                    @if(!empty($vendor_name_list) && count($vendor_name_list)>0)
                                                    @foreach($vendor_name_list as $vendor_name)
                                                        <option value="{{$vendor_name->id}}">{{$vendor_name->name}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            <!-- <div class="col-sm-2">
                                                <select class="select-drop" id="status" name="status">
                                                    <option value="">Status</option>
                                                    @if(!empty($quete_list) && count($quete_list)>0)
                                                    @foreach($quete_list as $quete)
                                                        <option value="{{$quete->status}}">{{$quete->status}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div> -->
                                            <div class="col-sm-2">
                                            <div class="form-group">
                                                    <div class="input-group date">
                                                        <input type="text" class="form-control date_picker" placeholder="Date"  id="date" name="date"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-sm-2">
                                                <a href="javascript:void(0)" class="btn btn-success" onclick="applyFilter();">Apply Filter</a>
                                                <div class="clearfix"></div>
                                            </div>
                                             <div class="col-sm-1"> 
                                                <a href="javascript:void(0)" class="btn btn-success dateCheck reset" onclick="">Clear</a>
                                                <div class="clearfix"></div>  
                                            </div>
                                            <div class="clearfix"></div>        
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>    
                                </div>   
                            </div>
                        </div>
                        <span id="filterResult">
                            <div class="table-responsive view-table">
                                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Sl. No.</th>
                                            <th class="text-center">Image</th>
                                            <th class="text-center">REQ ID</th>
                                            <th class="text-center">Date</th>
                                            <th class="text-center">Design</th>
                                            <th class="text-center">Colour</th>
                                            <th class="text-center">Width/Size</th>
                                             <th class="text-center">Rate</th>
                                            <th class="text-center">Vendor Name</th>
                                            <th class="text-center">Remarks</th>
                                            <th class="text-center">Last Comment</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                                               
                                    </tbody>
                                </table>
                            </div>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
@include('layouts.footer')
<script>

$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        //$('#load a').css('color', '#dfecf6');
        //$('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');

        var url = $(this).attr('href'); 
        var page=$(this).attr('href').split('page=')[1]; 
        
        var req_id=$("#req_id").val();
        var cat1=$("#cat1").val();
        var cat2=$("#cat2").val();
        var name=$("#name").val();
        //var quote_id=$("#quote_id").val();
        var date=$("#date").val();
        //var price=$("#price").val();
    
        filter(page,req_id,cat1,cat2,name,date);
        //window.history.pushState("", "", url);
    });

    
});

$( "#hide-filter" ).click(function() {
    $("#req_id").val('');
    $("#cat1").val('');
    $("#cat2").val('');
    $("#name").val('');
    $("#date").val('');
    //$("#price").val('');
        
    filter(); 
});

  $(document).ready(function () {        
        applyFilter();
    });


function applyFilter(){ 
    
        var req_id=$("#req_id").val();
        var cat1=$("#cat1").val();
        var cat2=$("#cat2").val();
        var name=$("#name").val();
        //var quote_id=$("#quote_id").val();
        var date=$("#date").val();
        //var price=$("#price").val();
    
        filter(1,req_id,cat1,cat2,name,date);
}

function filter(page=1,req_id='',cat1='',cat2='',name='',date=''){  

    $('.loading').show();
    
    $.ajax({
        type: 'GET', 
        url: '{{route("filter-concluded-rfq")}}',
        data: {req_id:req_id,cat1:cat1,cat2:cat2,name:name,date:date,page:page},
        success:function(data)
        {
            $('#filterResult').html(data);
            $('.loading').hide();
        },
        error: function (error) 
        {
            
            alert('Error Occured Please Try Again');
        }
    })
}
</script>
<script>
    function closeRfqRequest(rfq_id){
        var x = confirm("Are you sure you want to Close " +rfq_id);
        if (x)
            return true;
        else
            return false;
    }

     //--For Clearing Filter Data Elements-----//
    $(".reset").bind("click", function() {
    $("input[type=text], textarea").val("");
    $("#req_id").val("");
    $("#cat1").val("");
    $("#cat2").val("");
    $("#name").val("");

    });   
</script>

<!-- ********************************************** -->
<!-- JavaScript at the bottom for fast page loading -->
<!-- ********************************************** -->

<!-- Grab Google CDN's jQuery, fall back to local if offline -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{{asset('fancybox/js/libs/jquery-1.7.1.min.js')}}"><\/script>')</script>
    
<!-- FancyBox -->
    <script src="{{asset('fancybox/js/fancybox/jquery.fancybox.js')}}"></script>
    <script src="{{asset('fancybox/js/fancybox/jquery.fancybox-buttons.js')}}"></script>
    <script src="{{asset('fancybox/js/fancybox/jquery.fancybox-thumbs.js')}}"></script>
    <script src="{{asset('fancybox/js/fancybox/jquery.easing-1.3.pack.js')}}"></script>
    <script src="{{asset('fancybox/js/fancybox/jquery.mousewheel-3.0.6.pack.js')}}"></script>
    
    <script type="text/javascript">
        var nc=$.noConflict();
        nc(document).ready(function() {
        nc(".fancybox").fancybox();
        });
    </script>
