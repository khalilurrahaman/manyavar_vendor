@include('layouts.header')
<!-- All Page Css will go below --> 
<link href="{{asset('bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
    <!--add user popup Modal Start-->
<div class=" quotation-pop" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add New Item</h4>
            </div>
            <div class="modal-body">
                <form action="{{route('add-item-master')}}" id="add_item" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                    <div class="row shorting-area pricechange">
                        <div class="col-sm-12">
                        <input type="hidden" id="getCount" value="0" />
                            <div class="form-group" id="add_more_doc_p">
                                 <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 170px; height: 125px;">
                                       <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="Document Image"/>
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 170px; max-height: 150px;">
                                    </div>
                                    <div>
                                       <span class="btn default btn-file">
                                       <span class="fileinput-new">
                                       Select Image</span>
                                       <span class="fileinput-exists">
                                       Change </span>
                                       <input type="file" name="each_image[]" class="document_image"  multiple="true" />
                                       </span>
                                       <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                                       Remove </a>
                                    </div>
                                 </div>
                              </div>

                              <div class="form-group">
                                 <button type="button"  class="btn green-mint button_add" onclick="add_more_document();">Add Image</button> 
                              </div>
                              </div>
                        <div class="col-sm-12">
            <label>Width/Size Required?:</label>
            <input type="checkbox" name="width_size_required" id="width_size_required" value="1" class="form-control">
            </div>
                        <div class="col-sm-12">
                            <h5>Design :</h5>
                            <input type="text" name="category1" id="category1" class="form-control">
                            <span class="error class_err" id="category1_msg" ></span>
                        </div>
                        <div class="col-sm-12">
                            <h5>Colour :</h5>
                            <input type="text" name="category2" id="category2" class="form-control">
                            <span class="error class_err" id="category2_msg" ></span>
                        </div>
                        <div class="col-sm-12">
                            <h5>Category 3 :</h5>
                            <input type="text" name="category3" id="category3" class="form-control">
                        </div>
                        <div class="col-sm-12">
                            <h5>Width/Size :</h5>
                            <input type="text" name="category4" id="category4" class="form-control">
                        </div>
                        <div class="clearfix"></div>
                        
                        
                        <div class="clearfix"></div>
                        <div class="col-sm-6">
                        <input type="submit" name="submit" value="Add Item" onclick="return validateForm()" class="pup-btn">
                        </div>
                        <div class="col-sm-6">
                        <a href="{{URL::previous()}}" class="pup-btn">Cancel
                        </a>
                        <div class="clearfix"></div>
                        </div>
                        <!-- <div class="clearfix"></div> -->
                    </div>
                </form>
           </div>
        </div>
    </div>
</div>
<script src="{{asset('bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>

<script type="text/javascript">

function remove_history_doc_p (v_div_id) {
   var dd = '#new_v_doc_'+v_div_id;
   $(dd).remove();
};

var new_doc_count = 0;
function add_more_document (){
   
   new_doc_count++;
   
   var countt = $("#getCount").val();
   //alert(countt);
   countt = parseInt(countt) + 1;

   var div = '<div id="new_v_doc_'+countt+'" class="fileinput fileinput-new" data-provides="fileinput">'+
                  '<div class="fileinput-new thumbnail" style="width: 170px; height: 125px;">'+
                     '<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="Document Image"/>'+
                  '</div>'+
                  '<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 170px; max-height: 125px;">'+
                  '</div>'+
                  '<div>'+
                     '<span class="btn default btn-file">'+
                     '<span class="fileinput-new">'+
                     'Select Image</span>'+
                     '<span class="fileinput-exists">'+
                     'Change </span>'+

                     '<input name="each_image[]" class="document_image" type="file" />'+
                     '</span>'+
                     '<button type="button"  class="btn red-mint button_add" onclick="remove_history_doc_p('+countt+');">Remove</button>'+
                     //'<a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">'+
                     //'Remove </a>'+
                  '</div>'+
               '</div>'+
            '</div>';
   //$(".bs-select").selectPicker('refresh'); 

   $('#add_more_doc_p').append(div);
   $(".bs-select").selectpicker('refresh'); 
   $("#getCount").val(countt);
   //alert($("#getCount").val());
}
function validateForm()
  {    
    var ret_val=true;   
   
    var category1=$("#category1").val();
    if(category1==''){
        $("#category1_msg").text('Please Enter Design !').css('color', 'red').show();
        ret_val=false;
    }
    else{
        $("#category1_msg").hide();
    }

    var category2=$("#category2").val();
    if(category2==''){
        $("#category2_msg").text('Please Enter Colour !').css('color', 'red').show();
        ret_val=false;
    }
    else{
        $("#category2_msg").hide();
    }

    var item_image = $("#file").val();
    if(item_image==''){
        $("#image_msg").text('Please Select Image !').css('color', 'red').show();
        ret_val=false;
    }else{
        $("#image_msg").hide();
    }
    return ret_val;
}
</script>
<!--add user popup  Modal End-->
@include('layouts.footer')