@ include('header.blade.php')
<script src="{{asset('dataTable/css/jquery.dataTables.min.css')}}"></script>
<script src="{{asset('dataTable/css/buttons.dataTables.min.css')}}"></script>

   
<script src="{{asset('dataTable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('dataTable/js/dataTables.buttons.min.js')}}"></script>   
<script src="{{asset('dataTable/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('dataTable/js/jszip.min.js')}}"></script>   
<script src="{{asset('dataTable/js/pdfmake.min.js')}}"></script>
<script src="{{asset('dataTable/js/vfs_fonts.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.print.min.js ')}}"></script>
   
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <!--<div class="row">
                    <div class="col-lg-12 title-top">
                    <div class="row page-header">
                    	<div class="col-sm-6">
                        	<h1>Vendor <small>Purchase Order Listing</small></h1>
                        </div>
                        <div class="col-sm-6">
                        	<a href="javascript:void(0)" class="btn btn-success">Expand</a>
                        </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                	<div class="col-sm-6"><h3 class="panel-title">
                                    <i class="fa fa-users" aria-hidden="true"></i> Vendor User Listing</h3></div>
                                    <div class="col-sm-6">
                                    
                                    <div class="shorting-area">
                            		
                              
                                <div class="col-sm-3 pull-right">
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#adduser" class="btn btn-success">
                                        <i class="fa fa-plus" aria-hidden="true"></i> Add New</a>
                                </div>
                            </div>
                                    
                                    
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Name</th>
                                                <th class="text-center">Department</th>
                                                <th class="text-center">E-mail ID</th>
                                                <th class="text-center">Phone No</th>
                                                <th class="text-center">Address</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <tr>
                                        <td class="text-center">Tapan Ghose</td>
                                        <td class="text-center">Sherwani</td>
                                        <td class="text-center">tapan@gmail.com</td>
                                        <td class="text-center">+91 1234567890</td>
                                        <td class="text-center">Kolkata</td>
                                        <td class="text-center"><a href="javascript:void(0)" class="btn btn-success">Accept</a></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">Tapan Ghose</td>
                                        <td class="text-center">Sherwani</td>
                                        <td class="text-center">tapan@gmail.com</td>
                                        <td class="text-center">+91 1234567890</td>
                                        <td class="text-center">Kolkata</td>
                                        <td class="text-center"><a href="javascript:void(0)" class="btn btn-success">Accept</a></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">Tapan Ghose</td>
                                        <td class="text-center">Sherwani</td>
                                        <td class="text-center">tapan@gmail.com</td>
                                        <td class="text-center">+91 1234567890</td>
                                        <td class="text-center">Kolkata</td>
                                        <td class="text-center"><a href="javascript:void(0)" class="btn btn-success">Accept</a></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">Tapan Ghose</td>
                                        <td class="text-center">Sherwani</td>
                                        <td class="text-center">tapan@gmail.com</td>
                                        <td class="text-center">+91 1234567890</td>
                                        <td class="text-center">Kolkata</td>
                                        <td class="text-center"><a href="javascript:void(0)" class="btn btn-success">Accept</a></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">Tapan Ghose</td>
                                        <td class="text-center">Sherwani</td>
                                        <td class="text-center">tapan@gmail.com</td>
                                        <td class="text-center">+91 1234567890</td>
                                        <td class="text-center">Kolkata</td>
                                        <td class="text-center"><a href="javascript:void(0)" class="btn btn-success">Accept</a></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">Tapan Ghose</td>
                                        <td class="text-center">Sherwani</td>
                                        <td class="text-center">tapan@gmail.com</td>
                                        <td class="text-center">+91 1234567890</td>
                                        <td class="text-center">Kolkata</td>
                                        <td class="text-center"><a href="javascript:void(0)" class="btn btn-danger">Reject</a></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">Tapan Ghose</td>
                                        <td class="text-center">Sherwani</td>
                                        <td class="text-center">tapan@gmail.com</td>
                                        <td class="text-center">+91 1234567890</td>
                                        <td class="text-center">Kolkata</td>
                                        <td class="text-center"><a href="javascript:void(0)" class="btn btn-success">Accept</a></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">Tapan Ghose</td>
                                        <td class="text-center">Sherwani</td>
                                        <td class="text-center">tapan@gmail.com</td>
                                        <td class="text-center">+91 1234567890</td>
                                        <td class="text-center">Kolkata</td>
                                        <td class="text-center"><a href="javascript:void(0)" class="btn btn-success">Accept</a></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">Tapan Ghose</td>
                                        <td class="text-center">Sherwani</td>
                                        <td class="text-center">tapan@gmail.com</td>
                                        <td class="text-center">+91 1234567890</td>
                                        <td class="text-center">Kolkata</td>
                                        <td class="text-center"><a href="javascript:void(0)" class="btn btn-success">Accept</a></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">Tapan Ghose</td>
                                        <td class="text-center">Sherwani</td>
                                        <td class="text-center">tapan@gmail.com</td>
                                        <td class="text-center">+91 1234567890</td>
                                        <td class="text-center">Kolkata</td>
                                        <td class="text-center"><a href="javascript:void(0)" class="btn btn-success">Accept</a></td>
                                    </tr>
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                              <div class="col-sm-6">
                              	<div class="dataTables_info" role="status" aria-live="polite">Showing 1 to 9 of 9 entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                        <ul class="pagination">
                                            <li class="page-item">
                                                <a class="page-link" href="javascript:void(0)" aria-label="Previous">
                                                	<span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span>
                                                </a>
                                            </li>
                                        	<li class="page-item active"><a class="page-link" href="javascript:void(0)">1</a></li>
                                       		<li class="page-item"><a class="page-link" href="javascript:void(0)">2</a></li>
                                        	<li class="page-item"><a class="page-link" href="javascript:void(0)">3</a></li>
                                        	<li class="page-item"><a class="page-link" href="javascript:void(0)">4</a></li>
                                        	<li class="page-item"><a class="page-link" href="javascript:void(0)">5</a></li>
                                        	<li class="page-item">
                                        	<a class="page-link" href="javascript:void(0)" aria-label="Next">
                                        		<span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span>
                                        	</a>
                                        	</li>
                                        </ul>
                                    </nav>
                                  </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@php include('layouts.footer')