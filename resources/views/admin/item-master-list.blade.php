@include('layouts.header')
<!--------------------------------Start Shorting-------------------------->
<style>
.dt-buttons{margin-right:0px !important;}
.dt-buttons>a {padding-left:18px;}
</style>

<link rel="stylesheet" href="{{asset('autocomplete/jquery-ui.css')}}" />
<script src="{{asset('autocomplete/jquery-ui.js')}}"></script>

<script src="{{asset('dataTable/css/jquery.dataTables.min.css')}}"></script>
<script src="{{asset('dataTable/css/buttons.dataTables.min.css')}}"></script>

   
<script src="{{asset('dataTable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('dataTable/js/dataTables.buttons.min.js')}}"></script>   
<script src="{{asset('dataTable/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('dataTable/js/jszip.min.js')}}"></script>   
<script src="{{asset('dataTable/js/pdfmake.min.js')}}"></script>
<script src="{{asset('dataTable/js/vfs_fonts.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.print.min.js ')}}"></script>
<!--------------------------------End Shorting-------------------------->
<?php 
use App\Http\Controllers\Controller;
if(Controller::userAccessibilityCheck('sample-item-list'))
	$sample_item_list_exist=1;
?>
<div id="page-wrapper">
  <div class="container-fluid inner-body-area">
    <div class="row">
      <div class="col-lg-12">
      @if(isset($errors))
                        @foreach ($errors->all() as $error)

                                        <div class="btn btn-success" id="hide-me" style="width:100%;background-color: green;border: none;">{{ $error }}</div>
                                         @endforeach
                      @endif
        <div class="panel panel-default add-padding">
          <div class="panel-heading">
            <div class="row">
              <div class="col-sm-6">
                <h3 class="panel-title"> <i class="fa fa-users" aria-hidden="true"></i> Item Master Listing</h3>
              </div>
              <div class="col-sm-6">
                <div class="shorting-area">
                  <?php if(!empty($sample_item_list_exist)){?>   
                  <div class="col-sm-3 pull-right"> <a href="{{route('item-master-sample')}}" class="btn btn-success">Sample Item</a> </div>
                  <?php }?>   
                 <!-- <div class="col-sm-3 pull-right"> 
                 <a href="{{route('item-master')}}" data-toggle="modal" class="btn btn-success"> 
                 <i class="fa fa-plus" aria-hidden="true"></i> Add New</a> 
                 </div>-->
                 <div class="shorting-area">
                                <div class="pull-right">
                                        <a href="javascript:void(0)" id="show-btn-main" class="btn btn-success">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                </div>
                            </div>
                </div>
                <!--<div class="col-sm-6">
                                    
                                     <div class="shorting-area">
                                <div class="pull-right">
                                        <a href="javascript:void(0)" id="show-btn-main" class="btn btn-success">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                </div>
                            </div>
                                    </div>-->
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="panel-body custome-table">
            <div class="row shorting-area">
                          
                            <div class="col-sm-12">
                            <div id="mySidenavR-main" class="sidenavR" style="display:none;">
                            <div class="clearfix error_msg_main"></div>
                            <div class="filter-right">
                            <div class="filter-area list-filter">
                            <!--<h5>Vendor List Filter</h5>-->
                                <a href="javascript:void(0)" class="closebtn" id="hide-filter-main">×</a>
                                    <div class="col-sm-2">
                                         <input type="text" class="form-control" placeholder="Search" style="margin-bottom:0;" id="search_exist_main" name="search_exist_main" onkeypress="javascript: if (event.keyCode==13) getOptionFilterMain();">
                                    </div>
                                    <div class="col-sm-1">
                                        <a href="javascript:void(0)" class="btn btn-success" onclick="getOptionFilterMain();" style="width:60px !important;  margin-left: -6px !important;">Search</a>
                                    </div>
                                    <span id="mySidenavR-main-span" style="display:none;">
                                    </span>
                            <div class="clearfix"></div>        
                                </div>
                                <div class="clearfix"></div>
                                </div>    
                            </div>
                            
                            </div>
                            
                            
                            </div>
                              <span id="existItemLIstLive">
            <div class="table-responsive ">
              <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th class="text-center">Sr. No</th>
                    <th class="text-center">Image</th>
                    <th class="text-center">Design</th>
                    <th class="text-center">Colour</th>
                    <th class="text-center">Category 3</th>
                    <th class="text-center">Width/Size</th>
                    <!-- <th class="text-center">Action</th> -->
                  </tr>
                </thead>
                <tbody>
                <?php $inc=0; ?>
                @foreach($data as $eachData)
                <?php $inc++; 
                 $pGalList=DB::table('product_gallery')->select('*')->where('product_id','=',$eachData->id)->get();
                      #echo "<pre>";print_r($pGalList);
                  $data_mouseover=URL::to('/img').'/pre-order-img.jpg';
                                     
                  if(!empty($pGalList) && count($pGalList)>0)
                    {
                      if($eachData->image!='' && file_exists('upload/product/'.$eachData->image))
                          $data_mouseover=URL::to('/upload/product').'/'.$eachData->image.'#200';
                          $timeoutcount=1;
                          foreach ($pGalList as $pGal) 
                           {
                            $timeout=$timeoutcount*400;
                            $data_mouseover .=' '.URL::to('/upload/product').'/'.$pGal->product_image.'#'.$timeout.' ';
                            $timeoutcount++;
                            }

                           }
                       $data_mouseover=rtrim($data_mouseover," ");
                ?>
                <tr>
                  <td class="text-center">{{$inc}}</td>
                  <td class="text-center"><!--  @if($eachData->image!='') <img src="{{URL::to('/upload/product').'/'.$eachData->image }}"/ style="height:60px;width: 60px;"> @endif --> 
                  @if($eachData->image!='')
                    @if(file_exists('upload/product/'.$eachData->image))
                    <img src="{{URL::to('/upload/product').'/'.$eachData->image }}" data-mouseover="{{$data_mouseover}}" style="height:60px;width: 60px;"/>
                    @else
                    <img src="{{URL::to('/img').'/pre-order-img.jpg' }}" data-mouseover="{{$data_mouseover}}" style="height:60px;width: 60px;"/>
                    @endif
                  @else
                    <img src="{{URL::to('/img').'/pre-order-img.jpg' }}" data-mouseover="{{$data_mouseover}}" style="height:60px;width: 60px;"/>
                  @endif
                  </td>
                  <td class="text-center">{{$eachData->cat1}}</td>
                  <td class="text-center">{{$eachData->cat2}}</td>
                  <td class="text-center">{{$eachData->cat3}}</td>
                  <td class="text-center">{{$eachData->cat4}}</td>
                  <!-- <td class="text-center"><a href="{{route('item-master-edit',$eachData->id)}}"  class="btn btn-success">Edit</a></td> -->
                </tr>
                @endforeach
                  </tbody>
                
              </table>
            </div>
            </span>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row --> 
  </div>
  <!-- /.container-fluid --> 
</div>
<!-- /#page-wrapper --> 
<script type="text/javascript">
  var cat1_list = <?php echo $cat1_list;?>;
$(document).ready(
  function () {
    $( "#search_exist_main" ).autocomplete({
      source:cat1_list,
      autoFocus: true ,
      //change: function (event, ui) { alert(ui.item.value); }
    });
  }

);
</script>
 <script type="text/javascript">$('img').on('mouseover', function() {
    var self = this,
        i = 0,
        images = $(this).data('mouseover').split(/\s+/);
    
    (function nextImage() {
        var next = images[i++ % images.length].split('#');
        $(self).data('timeout', setTimeout(function() {
            self.src = next[0];
            nextImage();
        }, next[1]));
    })();
    
}).on('mouseout', function() {
    clearTimeout($(this).data('timeout'));
    this.src = $(this).attr('src');
});
</script>
<script>
$(document).ready(function () {
            
            $('#example').dataTable({
                'order': [[ 0, "asc" ]],//Default Column Attribute
                'pageLength': 10,//Per Page Item
                 "searching": false,
                'dom': 'Bfrtip',//Header Info               
                'buttons': [ {extend: 'excel',exportOptions: {columns: [0,2,3,4,5]},footer: false}],
                'aoColumnDefs': [          
                                   { aTargets: [ 0 ], bSortable: true },
                                   { aTargets: [ 1 ], bSortable: false },
                                   { aTargets: [ 2 ], bSortable: true },
                                   { aTargets: [ 3 ], bSortable: false },
                                   { aTargets: [ 4 ], bSortable: false },
                                   { aTargets: [ 5 ], bSortable: false }
                                ],
                "oLanguage": {
                                "oPaginate": {
                                                "sNext": '<i class="fa fa-chevron-right" ></i>',
                                                "sPrevious": '<i class="fa fa-chevron-left" ></i>'
                                            }
                            }
            });
        });
$(document).ready(function(){
    $("#hide-filter-main").click(function(){
        $("#mySidenavR-main").hide();
		filterLive();
    });
    $("#show-btn-main").click(function(){
        $("#mySidenavR-main").show();
    });
});
</script> 
<script type="text/javascript">
    function ConfirmDelete(id)
        {
            var x = confirm("Are you sure you want to Delete?");
            if (x)
                return true;
            else
                return false;
        }
		
		
	
function getOptionFilterMain() {
 var search_str=$('#search_exist_main').val();
 $('.error_msg_main').text('');
 if(search_str=='')
 {
	 $('.error_msg_main').text('Please Enter Search Keyword!').css('color', 'red').show();
	 return false;
 }
 $('.loading').show();
 
 $('#mySidenavR-main-span').show();
 		$.ajax({
			type: 'GET', 
			url: '{{route("get-option-filter-live-list")}}',
			data: {search_str:search_str},
			success:function(data)
			{
				$('.loading').hide();
        		$('#mySidenavR-main-span').html(data);
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});
}

$('.selectSearchLive').click(function(){
	     var selectcat1 = $('#search_exist_main').val();
		 var selectcat2 = $('#selectcat2').val();
		 var selectcat3 = $('#selectcat3').val();
		 var selectcat4 = $('#selectcat4').val();
		filterLive(selectcat1,selectcat2,selectcat3,selectcat4) ;
});
function filterLive(selectcat1='',selectcat2='',selectcat3='',selectcat4=''){
	   $('.loading').show();
 		$.ajax({
			type: 'GET', 
			url: '{{route("search-item-by-keyword-live-list")}}',
			data: {selectcat1:selectcat1,selectcat2:selectcat2,selectcat3:selectcat3,selectcat4:selectcat4},
			success:function(data)
			{
        		$('#existItemLIstLive').html(data);
				$('.loading').hide();
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});
}

setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 2000);
</script> 
@include('layouts.footer')