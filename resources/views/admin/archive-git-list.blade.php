@include('layouts.header')
<!--Start Shorting-->
<style>
.dt-buttons{margin-right:0px !important;}
.dt-buttons>a {padding-left:18px;}
</style>
<!-- <script src="{{asset('dataTable/css/jquery.dataTables.min.css')}}"></script>
<script src="{{asset('dataTable/css/buttons.dataTables.min.css')}}"></script>

   
<script src="{{asset('dataTable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('dataTable/js/dataTables.buttons.min.js')}}"></script>   
<script src="{{asset('dataTable/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('dataTable/js/jszip.min.js')}}"></script>   
<script src="{{asset('dataTable/js/pdfmake.min.js')}}"></script>
<script src="{{asset('dataTable/js/vfs_fonts.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.print.min.js ')}}"></script> -->
<!--End Shorting-->
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
               <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                  @if(Session::has('alert-' . $msg))
            
                  <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} 
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  </p>
                  @endif
                @endforeach
              </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h3 class="panel-title">
                                        <i class="fa fa-users" aria-hidden="true"></i>
                                        Archive GIT List</h3>
                                    </div>
                                    <div class="col-sm-6"> 
                                        <div class="shorting-area">
                                            <div class="col-sm-3 purchase-filter pull-right">
                                                <a href="javascript:void(0)" onclick="getVendor();" id="show-btn" class="btn btn-success">
                                                <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                            <div id="mySidenavR" class="sidenavR" style="display:none;">
                                <div class="filter-right">
                            <div class="filter-area">
                            <!--<h5>Vendor List Filter</h5>-->
                                <a href="javascript:void(0)" class="closebtn" id="hide-filter">×</a>
                                   <div class="col-sm-2">                                       
                                        <select class="select-drop" id="vendor" name="vendor">
                                            <option value="">Select Vendor</option>
                                            
                                        </select>
                                    </div>
                                    <div class="col-sm-2">                                       
                                        <select class="select-drop" id="cat1" name="category1">
                                            <option value="">Select Design</option>
                                            
                                        </select>
                                    </div>
                                    <div class="col-sm-2">                                       
                                        <select class="select-drop" id="cat2" name="category2">
                                            <option value="">Select Colour</option>   
                                        </select>
                                    </div>
                                    <!-- <div class="col-sm-2">                                       
                                        <select class="select-drop" id="cat3" name="category3">
                                            <option value="">Select Cat3</option>
                                            
                                        </select>
                                    </div> -->
                                    <!-- <div class="col-sm-2">                                       
                                        <select class="select-drop" id="cat4" name="category4">
                                            <option value="">Select Width/Size</option>   
                                        </select>
                                    </div> -->
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                                <div class="input-group date">
                                                    <input type="text" class="form-control date_picker" placeholder="Date From(dd/mm/yy)"  id="due_from" name="due_from"/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                                <div class="input-group date">
                                                    <input type="text" class="form-control date_picker" placeholder="Date To(dd/mm/yy)"  id="due_to" name="due_to"/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    <div class="col-sm-2" style="width: 8%;padding: 0px;">
                                        <a href="javascript:void(0)" class="btn btn-success" onclick="applyFilter();">Apply Filter</a>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-sm-1"> 
                                      <a href="javascript:void(0)" class="btn btn-success dateCheck reset" onclick="">Clear</a>
                                      <div class="clearfix"></div>  
                                     </div>
                                    <div class="clearfix"></div>        
                                </div>
                                <div class="clearfix"></div>
                                </div>    
                            </div>
                                                
                        </div>
                            <div class="clearfix"></div>
                            <span id="filterResult">
                            <div class="panel-body custome-table">
                            
                                <div class="table-responsive view-table">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">SL No</th>
                                                <th class="text-center">GIT No</th>
                                                <th class="text-center">Vendor Name</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Date</th>
                                                <th class="text-center">GIT Qty</th>
                                                <th class="text-center">Receive Qty</th>
                                                
                                                <th class="text-center">Adhoc Qty</th>
                                                <th class="text-center">Pending GIT Qty</th>
                                                <!-- <th class="text-center">GRT</th>-->
                                                 <th class="text-center">Mark Complete Remarks</th>
                                                <th class="text-center">Last Comments</th><!--
                                                <th class="text-center">Remarks</th>-->
                                                <!--  <th class="text-center">Action</th> -->
                                                <!--<th class="text-center width-last">Action</th>-->
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                        </tbody>
                                    </table>
                                </div>
                               
                            </div>
                            </span>
                        </div>
                    </div>
                    
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@include('layouts.footer')
<!--ResolveGit Modal Start-->
<div id="ResolveGit" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Resolve GIT:<strong id="git_no"></strong></h4>
      </div>
      <input type="hidden" id="per_git_id"  value=""/>
      <input type="hidden" id="request_type"  value=""/>
      <div class="modal-body resolveGit-modal-body">
        <div class="row shorting-area pricechange">
            <div class="col-sm-12">
            <h5>Remarks:</h5>
            <textarea id="resolve_remarks" name="resolve_remarks" class="form-control"></textarea>
            <span  id="resolve_remarks_message"  class="commonMessage"></span>  
            </div>
            <div class="clearfix resolveGit-clearfix"></div>
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="pup-btn submitresolveGitClass">Submit</a>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--ResolveGit  Modal End-->

<!--ResolvedGit Modal Start-->
<div id="ResolvedGit" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Resolve GIT:<strong id="git_no99"></strong></h4>
      </div>
      <br />
      <div class="itemDetailsTable99" style="width:90%; padding-left:10%">
       <!--Append table Here-->
       </div>
      <div class="modal-body resolveGit-modal-body">
        <div class="row shorting-area pricechange">
            <div class="col-sm-4 previewing99" style="padding-left:258px;">
             
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--ResolvedGit  Modal End-->

<!--ViewRemarks Modal Start-->
<div id="ViewRemarks" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Resolve Remarks Of GIT:<strong id="git_no_r"></strong></h4>
      </div>
      <div class="modal-body resolveGit-modal-body">
        <div class="row shorting-area pricechange">
            <div class="col-sm-12">
            <h5 id="admin_remarks_r"></h5>
            </div>
            <div class="col-sm-12">
            <h5 id="vendor_remarks_r"></h5>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--ViewRemarks  Modal End-->

<!--MakComplete Modal Start-->
<div id="MakCompleteModal" class="modal fade quotation-pop" role="dialog">
<form  action="{{route('mark-complete-post')}}" method="post">
{{ csrf_field() }}
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Mark Complete</h4>
      </div>
      <input type="hidden" id="mark_complete_git_id" name="mark_complete_git_id"  value=""/>
      <div class="modal-body resolveGit-modal-body">
        <div class="row shorting-area pricechange">
            <div class="col-sm-12">
            <h5>Remarks:</h5>
            <textarea id="mark_complete_remarks" name="mark_complete_remarks" class="form-control" required="required"></textarea>
            <span  id="mark_complete_remarks_message"  class="commonMessage"></span>  
            </div>
            <div class="clearfix resolveGit-clearfix"></div>
            <div class="col-sm-12">
            <input type="submit" value="Submit" class="pup-btn"/>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>
  </div>
  </form>
</div>
<!--MakComplete  Modal End-->
<script>
$('.ResolveGitClass').click(function(){
//Fetch All Records
 var request_type=$(this).text();
 var git_no=$(this).attr('git_no');
 //Set All Records in Modal
 $('#request_type').val(request_type);
 $('#git_no').text(git_no); 
 
});

$('.ViewRemarksClass').click(function(){
 //Fetch All Records
 var admin_remarks=$(this).attr('admin_remarks');
 var vendor_remarks=$(this).attr('vendor_remarks');
 var git_no=$(this).attr('git_no');
 //Set All Records in Modal
 $('#git_no_r').text(git_no); 
 if(admin_remarks!='Admin:')
 	$('#admin_remarks_r').text(admin_remarks); 
 if(vendor_remarks!='Vendor:')
 	$('#vendor_remarks_r').text(vendor_remarks); 
});

$('.submitresolveGitClass').click(function(){
	
    var per_git_id=$('#per_git_id').val();
    var resolve_remarks=$('#resolve_remarks').val();
	var request_type=$('#request_type').val();
	var git_no=$('#git_no').text();
	
    if(resolve_remarks=='')
	 {
		 $("#resolve_remarks_message").text('Please enter remarks!').css('color', 'red');
		return false;
	 }
	 else
	 {
		 $("#resolve_remarks_message").hide(); 
	 }
		$.ajax({
			type: 'GET', 
			url: '{{route("resolve-git-records-admin")}}',
			data: {git_id:per_git_id,resolve_remarks:resolve_remarks,request_type:request_type,git_no:git_no},
			success:function(data)
			{
					if(data==1)
					{
        				$('.resolveGit-modal-body').html('Requested successfull!');
						 window.location.reload();
					}
					else
						$('.resolveGit-clearfix').text('Please Try again!');
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});
  
});






</script>
<!--  <script>
		$(document).ready(function () {
			
            $('#example').dataTable({
				'order': [[ 0, "asc" ]],//Default Column Attribute
				'pageLength': 500,//Per Page Item
				//'sDom': 't',//Hide All Info From top and bottom
				'aTargets': [-1], //1st one, start by the right/
				"paging":   false,
        		"info":     false,
				"searching": false,
				'dom': 'Bfrtip',
				'buttons': ['excel']//['copy', 'csv', 'excel', 'pdf', 'print']
            });
        });
		</script> -->
<script type="text/javascript">
function completeConfirm(git_no='')
{
    if(confirm('Are You Sure wanted to close:'+git_no))
    {
		
		$('#mark_complete_git_id').val(git_no);
		$('#MakCompleteModal').modal('show');
		return false;		
        //return true;
    }
    else
    {
        return false;
    }
}

    function getVendor()
    {

        $.ajax({
            type: 'GET',
            url: '{{route("get-archive-git-vendor")}}',
            //data: {value:val},
            success:function(data)
            {

                $("#vendor").html(data['vendor']);
                getCat1();
            }
            /*error:function(error)
            {
                alert('Error Occured Please Try Again');
            }*/

        });

        
    }
    function getCat1()
    {

        $.ajax({
            type: 'GET',
            url: '{{route("get-archive-git-cat1")}}',
            //data: {value:val},
            success:function(data)
            {

                $("#cat1").html(data['cat1']);
                getCat2();
            }
            

        });

        
    }
    function getCat2()
    {
        $.ajax({
            type: 'GET',
            url: '{{route("get-archive-git-cat2")}}',
            //data: {value:val},
            success:function(data)
            {
                $("#cat2").html(data['cat2']);
            }
            

        });

        
    }
   /* function getCat3(val)
    {
        $.ajax({
            type: 'GET',
            url: '{{route("get-vendor-git-cat3")}}',
            data: {value:val},
            success:function(data)
            {
                $("#cat3").html(data['cat3']);
                getCat4();
            }
            
        });

        
    }
    function getCat4(val)
    {
        $.ajax({
            type: 'GET',
            url: '{{route("get-vendor-git-cat4")}}',
            data: {value:val},
            success:function(data)
            {
                $("#cat4").html(data['cat4']);
            }
            

        });

        
    }*/













</script>

<script>
$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        var url = $(this).attr('href'); 
        var page=$(this).attr('href').split('page=')[1];
        
        //var po_number=$("#po_number").val();
        var vendor=$("#vendor").val();
        var category1=$("#cat1").val();
        var category2=$("#cat2").val();
        /*var category3=$("#cat3").val();
        var category4=$("#cat4").val();*/
        var due_from=$("#due_from").val();
        var due_to=$("#due_to").val();
     
        filter(page,vendor,category1,category2,due_from,due_to);
    });    
});


function applyFilter(){
        var vendor=$("#vendor").val();
        var category1=$("#cat1").val();
        var category2=$("#cat2").val();
        /*var category3=$("#cat3").val();
        var category4=$("#cat4").val();*/
        var due_from=$("#due_from").val();   
        var due_to=$("#due_to").val();   
        filter(1,category1,category2,vendor,due_from,due_to);
}

$( "#hide-filter" ).click(function() {
    
    $("#cat1").val('');
    $("#cat2").val('');
    $("#vendor").val('');
    /*$("#cat3").val('');
    $("#cat4").val('');*/
    $("#due_from").val('');
    $("#due_to").val('');
    
    filter(); 
});

function filter(page=1,category1='',category2='',vendor='',due_from='',due_to=''){
    //if(po_number!='' || category1!=''  || category2!=''  || due_from!='' due_to!='')  
    $('.loading').show();
    $.ajax({
        type: 'GET', 
        url: '{{route("filter-admin-git-archive")}}',
        data: {category1:category1,category2:category2,vendor:vendor,due_from:due_from,due_to:due_to,page:page},
        success:function(data)
        {
            $('#filterResult').html(data);
            $('.loading').hide();
        },
        error: function (error) 
        {
            
            alert('Error Occured Please Try Again');
        }
    })
}
$(document).ready(function () {        
        applyFilter();
});
setTimeout(function() {
$('.alert').fadeOut('slow');
}, 2000); // <-- time in milliseconds

 //--For Clearing Filter Data Elements-----//
    $(".reset").bind("click", function() {
    $("input[type=text], textarea").val("");
    $("#vendor").val("");
    $("#cat1").val("");
    $("#cat2").val("");

    });   
</script> 