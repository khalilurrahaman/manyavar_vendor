@include('layouts.header')
<!--------------------------------Start Shorting-------------------------->
<style>
.dt-buttons{margin-right:0px !important;}
.dt-buttons>a {padding-left:18px;}
</style>
<script src="{{asset('dataTable/css/jquery.dataTables.min.css')}}"></script>
<script src="{{asset('dataTable/css/buttons.dataTables.min.css')}}"></script>

   
<script src="{{asset('dataTable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('dataTable/js/dataTables.buttons.min.js')}}"></script>   
<script src="{{asset('dataTable/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('dataTable/js/jszip.min.js')}}"></script>   
<script src="{{asset('dataTable/js/pdfmake.min.js')}}"></script>
<script src="{{asset('dataTable/js/vfs_fonts.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.print.min.js ')}}"></script>
<!--------------------------------End Shorting-------------------------->







<?php 
use App\Http\Controllers\Controller;
if(Controller::userAccessibilityCheck('document-list-delete'))
	$document_list_delete_exist=1;
?>
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <!--<div class="row">
                    <div class="col-lg-12 title-top">
                    <div class="row page-header">
                    	<div class="col-sm-6">
                        	<h1>Vendor <small>Purchase Order Listing</small></h1>
                        </div>
                        <div class="col-sm-6">
                        	<a href="javascript:void(0)" class="btn btn-success">Expand</a>
                        </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-lg-12">
                    @if(isset($errors))
                        @foreach ($errors->all() as $error)

                                        <div class="btn btn-success" id="hide-me" style="width:100%;background-color: green;border: none;">{{ $error }}</div>
                                         @endforeach
                      @endif
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                	<div class="col-sm-6"><h3 class="panel-title">
                                    <i class="fa fa-users" aria-hidden="true"></i> Document Listing</h3></div>
                                    <div class="col-sm-6">
                                    
                                        <div class="row shorting-area">
                                            <div class="col-sm-2 purchase-filter pull-right">
                                            <a href="javascript:void(0)" onclick="getDocumentVendor(1);" id="show-btn" class="btn btn-success">
                                            <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                            </div>
<div class="clearfix"></div>
 <div class="panel-body custome-table">
<div class="row shorting-area">
<div class="col-sm-12">
<div id="mySidenavR" class="sidenavR" style="display:none;">
<div class="filter-right">
<div class="filter-area">
<!--<h5>Vendor List Filter</h5>-->
    <a href="javascript:void(0)"  class="closebtn" id="hide-filter">×</a>
        
        
        <div class="col-sm-2">
           
            <select class="select-drop" id="name" name="vendore_name">
                <option value="">Select Vendor</option>               
            </select>
        </div>
        
        
        <div class="col-sm-2">
            
            <select class="select-drop" id="type" name="type">
                <option value="">Select Document Type</option>                
            </select>
        </div>
        <div class="col-sm-2">
            
            <select class="select-drop" id="month" name="month">
                <option value="">Select Month</option>
            </select>


        </div>
        <div class="col-sm-2">
        <div class="form-group">
                <div class="input-group date">
                    <input type="text" class="form-control date_picker" placeholder="Date"  id="date" name="date"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-2">
            <a href="javascript:void(0)" class="btn btn-success" onclick="applyFilter(1);">Apply Filter</a>
            <div class="clearfix"></div>
        </div>
         <div class="col-sm-1"> 
                          <a href="javascript:void(0)" class="btn btn-success dateCheck reset" onclick="">Clear</a>
                          <div class="clearfix"></div>  
                         </div>
<div class="clearfix"></div>        
    </div>
    <div class="clearfix"></div>
    </div>    
</div>
                            
</div>
</div>

                      
                           

                            <span id="filterResult">
                               
                              </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
        <script type="text/javascript">
		$(document).ready(function () {
			
            $('#example').dataTable({
				'order': [[ 0, "asc" ]],//Default Column Attribute
				'pageLength': 500,//Per Page Item
				//'sDom': 't',//Hide All Info From top and bottom
				'aTargets': [-1], //1st one, start by the right/
				"paging":   false,
        		"info":     false,
				"searching": false,
				//'dom': 'Bfrtip',
				//'buttons': ['excel', 'pdf', 'print']//['copy', 'csv', 'excel', 'pdf', 'print']
            });
        });
        function ConfirmActive(id)
        {
            var x = confirm("Are you sure you want to Activate?");
            if (x)
                return true;
            else
                return false;
        }
		function ConfirmDeactive(id)
        {
            var x = confirm("Are you sure you want to Archieve?");
            if (x)
                return true;
            else
                return false;
        }
$(document).ready(function () {        
        applyFilter(1);
    });
$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        $('#load a').css('color', '#dfecf6');
        //$('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');

        var url = $(this).attr('href'); 
        var page=$(this).attr('href').split('page=')[1];
        //alert(page);
        
        var name=$("#name").val();
        var type=$("#type").val();
        var month=$("#month").val();
        var date=$("#date").val();
        var status=1;
        

        
        
     
        filter(page,type,month,name,date,status);
        //window.history.pushState("", "", url);
    });

    
});

$( "#hide-filter" ).click(function() {
    var name=$("#name").val('');
    var type=$("#type").val('');
    var month=$("#month").val('');
    var date=$("#date").val('');
    var status=1;
    filter(); 
});

function applyFilter(value){ 
    
        var name=$("#name").val();
        var type=$("#type").val();
        var month=$("#month").val();
        var date=$("#date").val();
        var status=value;
       
    
        filter(1,type,month,name,date,status);
}

function filter(page=1,type='',month='',name='',date='',status=1){ 

    $('.loading').show();
    
    $.ajax({
        type: 'GET', 
        url: '{{route("filter-document-return")}}',
        data: {page:page,type:type,month:month,name:name,date:date,status:status},
        success:function(data)
        {
            $('#filterResult').html(data);
            $('.loading').hide();
        },
        error: function (error) 
        {
            
            alert('Error Occured Please Try Again');
        }
    })
}

function getDocumentMonth(value)
    {
        $.ajax({
            type: 'GET',
            url: '{{route("get-document-month")}}',
            data: {value:value},
            success:function(data)
            {
                $("#month").html(data['month']);
            }
            

        });

        
    }
    function getDocumentVendor(value)
    {
        $('#mySidenavR').show();

        $.ajax({
            type: 'GET',
            url: '{{route("get-document-vendor")}}',
            data: {value:value},
            success:function(data)
            {

                $("#name").html(data['vendor']);
                getDocumentType(1);
            }
            

        });

        
    }
    function getDocumentType(value)
    {
        $.ajax({
            type: 'GET',
            url: '{{route("get-document-type")}}',
            data: {value:value},
            success:function(data)
            {
                $("#type").html(data['type']);
                getDocumentMonth(1);
            }
           

        });

        
    }

 //--For Clearing Filter Data Elements-----//
    $(".reset").bind("click", function() {
    $("#name").val("");
    $("#month").val("");
    $("#type").val("");
    $("#date").val("");

    }); 
   


setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 2000);


        </script>
@include('layouts.footer')   