@include('layouts.header')
    <!--add user popup Modal Start-->
<div class=" quotation-pop" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content none-shadow">
          <div class="modal-header">
            <h4 class="modal-title">Edit User</h4>
          </div>
            <div class="modal-body">
                <form action="{{route ('update-user-admin')}}" method="post">
                {{ csrf_field() }}
                @if(isset($errors))
                            @foreach ($errors->all() as $error)
                              <div style="padding-left: 145px;" class="alert alert-success alert-dismissable">{{ $error }}</div>
                            @endforeach
                    @endif
                    <div class="row shorting-area pricechange">
                        <div class="col-sm-6">
                            <h5>Name :</h5>
                            <input type="text" name="name" value="{{$users->name}}" class="form-control">
                            <input type="hidden" name="id" value="{{$users->id}}">
                        </div>
                        <div class="col-sm-6">
                            <h5>Email ID :</h5>
                            <input type="text" name="new_email" value="{{$users->email}}" class="form-control">
                            <input type="hidden" name="email" value="{{$users->email}}">
                        </div>
                        <div class="col-sm-6">
                            <h5>User Name :</h5>
                            <input type="text" name="new_username" value="{{$users->username}}" class="form-control" onblur="this.value=removeSpaces(this.value);" readonly="readonly">
                            <input type="hidden" name="username" value="{{$users->username}}">
                        </div>
                        <div class="col-sm-6">
                            <h5>Phone No :</h5>
                            <input type="text" name="mobile" id="mobile" value="{{$users->mobile}}" class="form-control" maxlength="10" onkeypress="return isNumberKey(event)">
                            <span class="error class_err" id="mobile_message" ></span>
                        </div>
                        <div class="clearfix"></div>                        
                        
                        
                        
                        
                        <div class="clearfix"></div>
                        <div class="col-sm-6">
                            <h5>Role :</h5>
                            <div class="shorting-area">
                                <div class="select-full">

                                    <select class="select-drop" name="role_id">
                                        @foreach($roleList as $eachRole)
                                        <option <?php if($users->role_id == $eachRole->id) echo 'selected="selected"' ?> value="{{$eachRole->id}}">{{$eachRole->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                        <h5>Address :</h5>
                        <textarea name="address" class="form-control" contenteditable='true'>{{$users->address}}</textarea>
                        </div>
                        <div class="col-sm-6">
                        <h5>Division :</h5>
                        @foreach($devisionList as $devisionList)
                        <?php 
                        $user_division=explode(',',$users->devision_id);
                        ?>
                          <div class="checkbox " style="padding-left: 20px;">
                            <input type="checkbox" name="division[]" <?php if(in_array($devisionList->id,$user_division)) echo "checked"; ?> value="{{ $devisionList->id }}" >{{ $devisionList->name }}</input>
                            </div>
                            @endforeach
                          
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-6">
                        <input type="submit" name="submit" value="Update" class="pup-btn" onclick="return validation()">
                        </div>
                        <div class="col-sm-6">
                        <a href="{{URL::previous()}}" class="pup-btn">Cancel
                        </a>
                        <div class="clearfix"></div>
                    </div>


                    </div>
                </form>

           </div>
         <div class="modal-footer"></div>

        </div>
    </div>
</div>
<!--add user popup  Modal End-->
@include('layouts.footer')
<script language="javascript" type="text/javascript">
function removeSpaces(string) {
	if (string.match(/\s/g)){
		alert('User name should take only one word,not spaces!');
	}
 return string.split(' ').join('');
}
</script>
<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
 
  function validation()
   {
       var data=$('#mobile').val();
       if(data.length!=10)
       {
           $("#mobile_message").text('Mobile no should accept 10 digits').css("color", "black").show();
           return false;
       }
       else
       {
            $('#mobile_message').hide();
            return true;
       }

    
   
   }   
</script>