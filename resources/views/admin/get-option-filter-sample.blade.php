<!--<h5>Vendor List Filter</h5>-->
        <div class="col-sm-2">
            <select class="select-drop" id="selectcat2" name="selectcat2">
            	<option value="">Select Colour</option>
                @if(!empty($cat2_list) && count($cat2_list)>0)
                @foreach($cat2_list as $cat2)
                	<option value="{{$cat2->cat2}}">{{$cat2->cat2}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="col-sm-2">
            <select class="select-drop" id="selectcat3" name="selectcat3">
            	<option value="">Select Cat3</option>
                @if(!empty($cat3_list) && count($cat3_list)>0)
                @foreach($cat3_list as $cat3)
                	<option value="{{$cat3->cat3}}">{{$cat3->cat3}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="col-sm-2">
            <select class="select-drop" id="selectcat4" name="selectcat4">
            	<option value="">Select Width/Size</option>
                @if(!empty($cat4_list) && count($cat4_list)>0)
                @foreach($cat4_list as $cat4)
                	<option value="{{$cat4->cat4}}">{{$cat4->cat4}}</option>
                @endforeach
                @endif
            </select>
        </div>
        
        <div class="col-sm-2">
        	<a href="javascript:void(0)" class="btn btn-success selectSearchSample">Apply</a>
        </div>
        <script>
		$('.selectSearchSample').click(function(){
	     var selectcat1 = $('#search_exist_main').val();
		 var selectcat2 = $('#selectcat2').val();
		 var selectcat3 = $('#selectcat3').val();
		 var selectcat4 = $('#selectcat4').val();
		filterSample(selectcat1,selectcat2,selectcat3,selectcat4) ;
		});
		</script>
