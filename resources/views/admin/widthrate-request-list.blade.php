<?php 
use App\Http\Controllers\Controller;
if(Controller::userAccessibilityCheck('rate-accept'))
	$action_accept_exist=1;
if(Controller::userAccessibilityCheck('rate-reject'))
	$action_reject_exist=1;
if(Controller::userAccessibilityCheck('action-close'))
	$action_close_exist=1;
if(Controller::userAccessibilityCheck('action-comments'))
	$action_comments_exist=1;
?>
@include('layouts.header')
<!--------------------------------Start Shorting-------------------------->
<style>
.dt-buttons{margin-right:0px !important;}
.dt-buttons>a {padding-left:18px;}
</style>
<style type="text/css">
 .blnk_txt {   
  animation-duration: 400ms;
  animation-name: blink;
  animation-iteration-count: infinite;
  animation-direction: alternate;
  color: red;
  }
                                   
  @keyframes blink {
  from {
      opacity: 1;
        }

   to {
      opacity: .7;
      }
     }
</style>
<script src="{{asset('dataTable/css/jquery.dataTables.min.css')}}"></script>
<script src="{{asset('dataTable/css/buttons.dataTables.min.css')}}"></script>

   
<script src="{{asset('dataTable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('dataTable/js/dataTables.buttons.min.js')}}"></script>   
<script src="{{asset('dataTable/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('dataTable/js/jszip.min.js')}}"></script>   
<script src="{{asset('dataTable/js/pdfmake.min.js')}}"></script>
<script src="{{asset('dataTable/js/vfs_fonts.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.print.min.js ')}}"></script>
<!--------------------------------End Shorting-------------------------->
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                              <div class="row">
                                  <div class="col-sm-4"><h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i> View All Width/Rate Change Request</h3></div>
                                    <div class="col-sm-8">
                                    
                                    <div class="shorting-area">
                              
                              <!-- <div class="col-sm-3 pull-right select-full">
                                        <a class="btn btn-default btn-select">
                                            <input type="hidden" class="btn-select-input" id="" name="" value="" />
                                            <span class="btn-select-value">Select an Item</span>
                                            <span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                                          <ul>
                                                <li class="selected">Division...</li>
                                               
                                          </ul>
                                        </a>
                                </div> -->                                               
                                
                                <div class="col-sm-3 pull-right select-full">
                                        <select id="req_status" name="req_status" class="select-drop" onchange="return filterByType()">
                                           <option value="">Select Status</option>
                                           <option value="inprocess">In Process</option>
                                           <option value="new">New</option>
                                           <option value="close">Closed</option>
                                           <option value="accept">Accepted</option>
                                           <option value="reject">Rejected</option>

                                        </select>
                                </div>
                                <div class="col-sm-3 pull-right select-full">
                                        <select id="req_type" name="req_type" class="select-drop" onchange="return filterByType()">
                                            <option value="">Select Type</option>
                                            <option value="rate_change">Rate Change</option>
                                            <option value="width_change">Width Change</option>
                                        </select>
                                </div>
                                <div class="col-sm-3 pull-right select-full">
                                        <select id="req_po_no" name="req_po_no" class="select-drop" onchange="return filterByType()">
                                           <option value="">Select PO</option>
                                          @if(!empty($po_list))
                                          @foreach($po_list as $req)
                                           <option value="{{$req->po_no}}">{{$req->po_no}}</option>
                                          @endforeach
                                          @endif 
                                        </select>
                                </div>                          
                            </div>                                
                          </div>
                        </div>
                            </div>
                            <div class="clearfix"></div>
                              <span id="filterResult">
                            <div class="panel-body custome-table">
                            
                                <div class="table-responsive view-table">

                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Sr. No</th>
                                                <th class="text-center">Request</th>
                                                <th class="text-center">PO Details</th>
                                                <th class="text-center">Product Info<br>(Design and Color)</th>
                                                <th class="text-center">Vendor Name</th>
                                                <th class="text-center">Old Values<br>(Price and Width)</th>
                                                <th class="text-center">Requested Vaules<br>(Price and Width)</th>
                                                <th class="text-center">Negotiated Rate</th>
                                                <th class="text-center">Accepted Values<br>(Price and Width)</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-center">Action</th>
                                                <th class="text-center">Last Comments</th>
                                            </tr>
                                        </thead>
                                    <tbody id="req_request">
                                    @if(!empty($request_list))
                                    <?php $inc=$request_list->perPage() * ($request_list->currentPage()-1);;?>
                                    @foreach($request_list as $req)
                                    <?php $inc++; ?>
                                    <tr>
                                    
                                      <td class="text-center">{{$inc}}</td>
                                      <td class="text-center">
                                      {{date("d/m/y",strtotime($req->created_at))}}<br>
                                      <?php 
                                        $type='';
                                        if($req->action_type=='rate_change')
                                          $type='Rate Change';          
                                        elseif($req->action_type=='width_change') 
                                          $type='Width Change';
                                         

                                        ?>
                                       {{$type}}
                                      </td>  
                                      <td class="text-center">
                                        <span class="po-no<?php echo $inc;?>">{{$req->po_no}}</span><br>
                                        <span class="po-date<?php echo $inc;?>">{{date("d/m/y",strtotime($req->order_date))}}</span>

                                      </td>   
                                      <td class="text-center">
                                       {{$req->cat1}}<br>
                                       {{$req->cat2}}
                                      </td> 
                                        <td class="text-center">
                                        @if(!empty($req->ven_dtls->name)){{$req->ven_dtls->name}}@endif
                                        </td>
                                        <td class="text-center">
                                        @if(!empty($req->old_value)){{$req->old_value}}@endif<br>
                                        @if(!empty($req->old_width)){{$req->old_width}}"@endif
                                        </td>
                                        <td class="text-center">
                                       @if(!empty($req->new_rate)){{$req->new_rate}}@endif<br>
                                        @if(!empty($req-> new_width)){{$req->new_width}}"@endif
                                        </td>
                                         <td class="text-center">
                                         @if($req->new_rate!=$req->negotiated_rate)
                                         {{$req->negotiated_rate}}@endif
                                         </td>
                                        <td class="text-center">
                                        @if($req->new_rate==$req->negotiated_rate){{$req->negotiated_rate}}@else{{$req->accepted_rate}}@endif<br>
                                        @if(!empty($req->new_width)){{$req->new_width}}"@endif
                                        </td>
                                        <td class="text-center">
                                       
                                       @if($req->status=='new')
                                       <span class="sm-btn btn-primary btn-xs">New</span>
                                       @elseif($req->status=='close')
                                       <span class="sm-btn btn-primary btn-warning">Accepted</span>
                                       @elseif($req->status=='inprocess')
                                       <span style="padding: 1px 9px;" class="sm-btn btn-primary btn-xs">In Process</span>
                                       @elseif($req->status=='accept')
                                       <span class="sm-btn btn-primary btn-warning">Accepted</span>
                                       @elseif($req->status=='reject')
                                       <span class="sm-btn btn-primary btn-danger">Rejected</span> 
                                       @endif
                                        </td>
                                        
                                        <td class="text-center">
                                        @if($req->status=='new')
                                         @if($req->notify==1)
                                         <span class="blnk_txt">Please Take Action</span>
                                         @endif<br>
                                         <?php if(!empty($action_accept_exist)){?> 
                                        <a data-toggle="modal" data-target="#vendorRequestaccept" data-placement="top" title="" class="btn btn-success vendorRequestaccept" unique-key="<?php echo $inc;?>" action_id="{{$req->id}}" action_type="{{$req->action_type}}" old_value="{{$req->old_value}}" req_rate="{{$req->new_rate}}" old_width="{{$req->old_width}}" req_width="{{$req->new_width}}" cat_1_details="{{$req->cat1}}" cat_2_details="{{$req->cat2}}">Accept</a><?php }?> 
                                        <?php if(!empty($action_reject_exist)){?> 
                                        <a data-toggle="modal" data-target="#vendorRequestreject" data-placement="top" title="" class="btn btn-danger vendorRequestreject" unique-key="<?php echo $inc;?>" action_id="{{$req->id}}" action_type="{{$req->action_type}}" req_rate="{{$req->new_rate}}" old_width="{{$req->old_width}}" req_width="{{$req->new_width}}" old_value="{{$req->old_value}}" cat_1_details="{{$req->cat1}}" cat_2_details="{{$req->cat2}}">Reject</a><?php }?> 
                                       @elseif($req->status=='inprocess')
                                       <span class="sm-btn btn-primary btn-xs">Pending</span>
                                       @elseif($req->status=='accept')
                                        <?php if(!empty($action_close_exist)){?> 
                                       <a class="btn btn-success vendorRequestclose" unique-key="<?php echo $inc;?>" action_id="{{$req->id}}" action_type="{{$req->action_type}}" old_value="{{$req->old_value}}" req_rate="{{$req->new_rate}}" old_width="{{$req->old_width}}" req_width="{{$req->new_width}}" class="btn btn-success btn-xs" onclick="getpolist({{$req->user_id}})">Close</a><?php }?> 
                                        @elseif($req->status=='reject')
                                        <span class="sm-btn btn-primary btn-warning">Closed</span> 
                                        @elseif($req->status=='close')
                                        <span class="sm-btn btn-primary btn-warning">Closed</span> 
                                        @endif
                                        </td>

                                        <td class="text-left width-e">
                                        <?php if(!empty($action_comments_exist)){?>
                                        @if(!empty($req->req_comments) && count(($req->req_comments))>0)
                                        <?php                                       
                                       
                                        if(strlen($req->req_comments[0]->comments)<=20)                                 
                                           echo ($req->req_comments[0]->comments);
                                        else
                                         echo substr($req->req_comments[0]->comments,0,18).'..';  

                                        ?>
                                        <a href="{{route('admin-requestwise-comments', $req->id)}}" class="pull-right btn btn-warning btn-xs">
                                        <?php echo count($req->req_comments)?>
                                        Comment(s)</a>
                                        @else
                                        <a href="{{route('admin-requestwise-comments', $req->id)}}" class="btn btn-warning btn-xs pull-right">
                                        
                                        Comments</a>
                                        
                                         @endif
                                         <?php }?>
                                         </td>

                                    </tr>
                                    @endforeach
                                    @endif 
                                     
                                </tbody> 
                               </table>

                              </div>
                              <div class="row">
                              <div class="col-sm-6">
                                <div class="dataTables_info" role="status" aria-live="polite">Showing {{$request_list->count()}} of {{$request_list->total()}} entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                      {{ $request_list->links() }}   
                                    </nav>
                                  </div>
                              </div>
                            </div>
                            </span>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

<!--vendorRequestaccept Modal Start-->
<div id="vendorRequestaccept" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close resetmodal" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request Accept</h4>
      </div>
      <div class="modal-body vendorRequestaccept-modal-body">
        <div class="row shorting-area pricechange">
        <input type="hidden" name="type" id="type" value="" />
         <input type="hidden" name="action_id" id="action_id" value="" /> 

            <div class="col-sm-3"><h5>PO. No : <strong class="vendorRequestaccept-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>Date : <strong class="vendorRequestaccept-date"><!--3000--></strong></h5></div>
            <div class="col-sm-3" id="vendorRequestdetails-cat_1_details"><h5>Design : <strong class="vendorRequestdetails-cat_1_details"></strong></h5></div> 
            <div class="col-sm-3" id="vendorRequestdetails-cat_2_details"><h5>Color : <strong class="vendorRequestdetails-cat_2_details"></strong></h5></div> 
            <div class="col-sm-3" id="vendorRequestdetails-cat_4_details"><h5>Size/Width : <strong class="vendorRequestdetails-cat_4_details"></strong></h5></div> 
            <div class="col-sm-3" id="old-rate"><h5>Old Rate : <strong class="vendorRequestaccept-old-rate"></strong></h5></div> 
            
            <div class="col-sm-3" id="reqst-rate"><h5>Requested Rate : <strong class="vendorRequestaccept-request-rate"></strong></h5></div>

            <div class="col-sm-3" id="old-width"><h5>Old Width : <strong class="vendorRequestaccept-old-width"></strong></h5></div> 

            <div class="col-sm-3" id="reqes-width"><h5>Requested Width : <strong class="vendorRequestaccept-reqes-width"></strong></h5></div> 
             <div class="clearfix"></div>
           
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
          <div class="col-sm-6 accept" id="accept">
            <h5>Enter Rate :</h5>
            <input type="text" placeholder="" onkeypress="javascript:return isNumber(event)" class="form-control" id="vendorRequestaccept_new_requested_rate" name="vendorRequestaccept_new_requested_rate"/>
            <span id="vendorRequestaccept_rate-remarks_message"></span>
            </div>
            <!-- <div class="col-sm-6 docet" id="docet">
            <h5>Docket No :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestaccept_new_docket" name="vendorRequestaccept_new_docket"/>
            <span id="vendorRequestaccept_docket-remarks_message"></span>
            </div>
            <div class="col-sm-6 prnno" id="prnno">
            <h5>PRN No :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestaccept_new_prn" name="vendorRequestaccept_new_prn"/>
            <span id="vendorRequestaccept_prn-remarks_message"></span>
            </div>
            <div class="col-sm-6 prndate" id="prndate">
            <h5>PRN Date :</h5>
           
            <div class="input-group date">
                     <input type="text" placeholder="dd/mm/yy" class="form-control date_picker" id="vendorRequestaccept_new_prndate" name="vendorRequestaccept_new_prndate"/>
                </div>
            <span id="vendorRequestaccept_prndate-remarks_message"></span>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6 courier" id="courier">
            <h5>Courier Name :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestaccept_new_courier" name="vendorRequestaccept_new_courier"/>
            <span id="vendorRequestaccept_courier-remarks_message"></span>
            </div> -->
            <div class="col-sm-12">
            <h5>Remarks :</h5>
            <textarea class="form-control" id="vendorRequestaccept_reason" name="vendorRequestaccept_reason"></textarea>
             <span id="vendorRequestaccept_remarks_message"></span>
            </div>

            <div class="clearfix vendorRequestaccept-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorRequestaccept">Accept</a>
            </div>
        <div class="col-sm-6">
            <a href="javascript:void(0)" data-dismiss="modal"  class="pup-btn resetmodal">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorChangeprice  Modal End-->


<!--vendorRequestReject Modal Start-->
<div id="vendorRequestreject" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request For Reject</h4>
      </div>
      <div class="modal-body vendorRequestreject-modal-body">
        <div class="row shorting-area pricechange">
        <input type="hidden" name="type" id="type" value="" />
         <input type="hidden" name="action_id" id="action_id" value="" /> 
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorRequestreject-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>Date : <strong class="vendorRequestreject-date"><!--3000--></strong></h5></div>
            <div class="col-sm-3" id="vendorReqreject-cat_1_details"><h5>Design : <strong class="vendorReqreject-cat_1_details"></strong></h5></div> 
            <div class="col-sm-3" id="vendorReqreject-cat_2_details"><h5>Color : <strong class="vendorReqreject-cat_2_details"></strong></h5></div> 
            <div class="col-sm-3" id="vendorReqreject-cat_4_details"><h5>Size/Width : <strong class="vendorReqreject-cat_4_details"></strong></h5></div> 
            <div class="col-sm-3" id="reject-pen-qty"><h5>Pending Qty : <strong class="vendorRequestreject-pen-qty"></strong></h5></div>
            <div class="col-sm-3" id="reject-old-rate"><h5>Old Rate : <strong class="vendorRequestreject-old-rate"></strong></h5></div> 
            
            <div class="col-sm-3" id="reject-reqst-rate"><h5>Requested Rate : <strong class="vendorRequestreject-request-rate"></strong></h5></div>

            <div class="col-sm-3" id="reject-old-width"><h5>Old Width : <strong class="vendorRequestreject-old-width"></strong></h5></div> 

            <div class="col-sm-3" id="reject-reqes-width"><h5>Requested Width : <strong class="vendorRequestreject-reqes-width"></strong></h5></div> 
            
              
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
            <div class="col-sm-12">
            <h5>Reason For Reject :</h5>
            <textarea class="form-control" id="vendorRequestreject_reason" name="vendorRequestreject_reason"></textarea>
             <span id="vendorRequestreject_remarks_message"></span>
            </div>

            <div class="clearfix vendorRequestreject-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorRequestreject">Submit</a>
            </div>
        <div class="col-sm-6">
            <a href="javascript:void(0)" data-dismiss="modal" class="pup-btn">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorRequestReject  Modal End-->


<!--Close Modal Start-->
<div id="vendorRequestclose" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close resetmodal" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request Close</h4>
      </div>
      <div class="modal-body vendorRequestclose-modal-body">
        <div class="row shorting-area pricechange">
        <input type="hidden" name="type" id="type" value="" />
         <input type="hidden" name="action_id" id="action_id" value="" /> 
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorRequestclose-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>Date : <strong class="vendorRequestclose-date"><!--3000--></strong></h5></div>
            <div class="col-sm-3" id="close-old-rate"><h5>Old Rate : <strong class="vendorRequestclose-old-rate"></strong></h5></div> 
            
            <div class="col-sm-3" id="close-reqst-rate"><h5>Requested Rate : <strong class="vendorRequestclose-request-rate"></strong></h5></div>

             <div class="clearfix"></div>
           
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
        <div class="col-sm-6 newpo" id="newpo">
             <select class="select-drop"  id="vendorRequestaccept_new_po">
                                                
            </select> 
             <span id="vendorRequestclose_rate-remarks_message"></span>         
            </div>
            <div class="clearfix vendorRequestclose-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorRequestclose">Close</a>
            </div>
        <div class="col-sm-6">
            <a href="javascript:void(0)" data-dismiss="modal"  class="pup-btn resetmodal">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--Close  Modal End-->
                                    
  <div class="loading">
  	<img src="{{asset('img/loading.gif')}}" alt="">  
  </div>                                  
                                    

@include('layouts.footer')
<script src="{{asset('js/custom/admin_purchase_order.js')}}"></script>  
<script>
// $(document).ready(function () {
			
//             $('#example').dataTable({
// 				'order': [[ 0, "asc" ]],//Default Column Attribute
// 				'pageLength': 500,//Per Page Item
// 				//'sDom': 't',//Hide All Info From top and bottom
// 				'aTargets': [-1], //1st one, start by the right/
// 				"paging":   false,
//         		"info":     false,
// 				"searching": false,
// 				'dom': 'Bfrtip',
// 				'buttons': ['excel']//['copy', 'csv', 'excel', 'pdf', 'print']
//             });
//         });
$('#vendorRequestaccept_new_prndate').on('changeDate', function(ev){
    $(this).datepicker('hide');
});

var ptoken='{{csrf_token()}}';
var cutting_url='{{route("accept-request")}}';
var rej_url='{{route("reject-request")}}';
var close_url='{{route("closewidthrate-request")}}';
</script>

<script type="text/javascript">
$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        //$('#load a').css('color', '#dfecf6');
        //$('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');

        var url = $(this).attr('href'); 
        var page=$(this).attr('href').split('page=')[1]; 
    
    
        filterByType(page,2);
        //window.history.pushState("", "", url);
    });

    
});

    function filterByType(page=1,load=1)
    {
        //alert(type);
        if(load==1)
        $('.loading').show();
        var status=$('#req_status').val();
        var type=$('#req_type').val();
        var po_no=$('#req_po_no').val();
        $.ajax({
            type: 'GET', 
            url: "{{route('filter-allwidthratechange')}}",
            data: {type:type,po_no:po_no,status:status,page:page},
            success:function(data)
            {
                $('.loading').hide();  
                $('#filterResult').html(data);
                 
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
    }





    function getpolist(id)
    { //alert(id);
        $.ajax({
            type: 'GET', 
            url: '{{route("vendor-newpo-selectlist")}}',
            data:{id:id},
            dataType:'html',
            success:function(data)
            {
                $('#vendorRequestaccept_new_po').html(data);
                 $('#vendorRequestclose').modal('show');
               
            },
            error: function (error) 
            {                
                alert('Error Occured Please Try Again');
            }
        });
    }

</script>
<script>
    // WRITE THE VALIDATION SCRIPT IN THE HEAD TAG.
    function isNumber(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
            return false;

        return true;
    }    

    //--For Clearing input Data after modal dismiss-----//
    $(".resetmodal").bind("click", function() {
    $("input[type=text], textarea").val("");
    });   

</script>
