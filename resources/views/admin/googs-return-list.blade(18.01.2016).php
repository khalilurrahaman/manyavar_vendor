@include('layouts.header')	
<!--------------------------------Start Shorting-------------------------->
<style>
.dt-buttons{margin-right:0px !important;}
.dt-buttons>a {padding-left:18px;}
</style>
<script src="{{asset('dataTable/css/jquery.dataTables.min.css')}}"></script>
<script src="{{asset('dataTable/css/buttons.dataTables.min.css')}}"></script>

   
<script src="{{asset('dataTable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('dataTable/js/dataTables.buttons.min.js')}}"></script>   
<script src="{{asset('dataTable/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('dataTable/js/jszip.min.js')}}"></script>   
<script src="{{asset('dataTable/js/pdfmake.min.js')}}"></script>
<script src="{{asset('dataTable/js/vfs_fonts.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.print.min.js ')}}"></script>
<!--------------------------------End Shorting-------------------------->
    <div id="page-wrapper">  	
        <div class="container-fluid inner-body-area">
            <div class="row">             	
                <div class="col-lg-12">
                @if(!empty($errors->all()) )

			    	<div class="btn btn-success" id="hide-me" style="width:100%;background-color: green;border: none;">{{ $errors->all()[0] }}</div>
			  	@endif
                    <div class="panel panel-default add-padding">
                        <div class="panel-heading">
                            <div class="row">
                            	<div class="col-sm-6"><h3 class="panel-title">
                                <i class="fa fa-users" aria-hidden="true"></i> Goods Return List</h3></div>
                                <div class="col-sm-6">
                                    <div class="shorting-area">                          
                                                                
                                        <div class="col-sm-3 pull-right">
                                            <a href="{{route('goods-returns-add-form')}}" data-toggle="modal" class="btn btn-success">
                                            <i class="fa fa-plus" aria-hidden="true"></i> Add Returns</a>
                                        </div>
                                        <div class="col-sm-3 pull-right">
                                            <a href="{{route('goods-returns-closed')}}" data-toggle="modal" class="btn btn-success">
                                            <i  aria-hidden="true"></i> Closed Return</a>
                                        </div>
                                        <div class="col-sm-2 purchase-filter pull-right">
                                            <a href="javascript:void(0)" id="show-btn" class="btn btn-success">
                                            <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                        </div> 
                                    </div>
                                </div>
                            </div>                            
                        </div>
                        <div class="col-sm-12">
                            <div id="mySidenavR" class="sidenavR" style="display:none;">
<div class="filter-right">
<div class="filter-area">
<!--<h5>Vendor List Filter</h5>-->
    <a href="javascript:void(0)" class="closebtn" id="hide-filter">×</a>
        
        
        <div class="col-sm-2">
           <select class="select-drop" id="name" name="vendore_name">
                <option value="">Vendore Name</option>
                @if(!empty($vendor_name_list) && count($vendor_name_list)>0)
                @foreach($vendor_name_list as $vendore)
                    <option value="{{$vendore->id}}">{{$vendore->name}}</option>
                @endforeach
                @endif
            </select>
        </div>
        
        
        <div class="col-sm-2">
            <select class="select-drop" id="cat1" name="category1">
                <option value="">Category 1</option>
                @if(!empty($cat1_list) && count($cat1_list)>0)
                @foreach($cat1_list as $cat1)
                    <option value="{{$cat1->category1}}">{{$cat1->category1}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="col-sm-2">
            <select class="select-drop" id="cat2" name="category2">
                <option value="">Category 2</option>
                @if(!empty($cat2_list) && count($cat2_list)>0)
                @foreach($cat2_list as $cat2)
                    <option value="{{$cat2->category2}}">{{$cat2->category2}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="col-sm-2">
            <a href="javascript:void(0)" class="btn btn-success" onclick="applyFilter();">Apply Filter</a>
            <div class="clearfix"></div>
        </div>
<div class="clearfix"></div>        
    </div>
    <div class="clearfix"></div>
    </div>    
</div>
                            
                            </div>
                        <div class="clearfix"></div>
                        <span id="filterResult">
                        <div class="panel-body custome-table">
                            <div class="table-responsive ">
                                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <!-- <th class="text-center">Prodict Id</th> -->
                                            <th class="text-center">Image</th>
                                            <th class="text-center">Vendor Name</th>
                                            <th class="text-center">Cat 1</th>
                                            <th class="text-center">Cat 2</th>
                                            <th class="text-center">Defective Remarks</th>
                                            <th class="text-center">Quantity</th>
                                            <th class="text-center">PRN No</th>
                                            <th class="text-center">PRN Date</th>
                                            <th class="text-center">Docket No</th>
                                            <th class="text-center">Courier Name</th>
                                            <th class="text-center">Admin Remarks</th>
                                            <!-- <th class="text-center">Vendor Remarks</th> -->
                                            <th class="text-center">Last Comments</th>
                                            <th class="text-center">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($returnList as $eachReturn)
                                        <tr>
                                            <td class="text-center">
                                                @if($eachReturn->image!='')
                                                
	                                                @if(file_exists('upload/temp/'.$eachReturn->image))
	                                                
	                                                <img src="{{URL::to('/upload/temp').'/'.$eachReturn->image }}" style="height:60px;width: 60px;">
	                                                @else
	                                                
	                                                <img src="{{URL::to('/img').'/pre-order-img.jpg' }}" style="height:60px;width: 60px;">
	                                                @endif
                                                @else
                                                <img src="{{URL::to('/img').'/pre-order-img.jpg' }}" style="height:60px;width: 60px;">
                                                @endif
                                            </td>
                                            <td class="text-center">{{$eachReturn->name}}</td>
                                            <td class="text-center">{{$eachReturn->category1}}</td>
                                            <td class="text-center">{{$eachReturn->category2}}</td>
                                            <td class="text-center" title="{{$eachReturn->reason}}">
                                            <?php
                                            $i=strlen($eachReturn->reason); 
                                            if(strlen($i>25)) 
                                                echo substr($eachReturn->reason,0,22).'...';
                                            else 
                                                echo $eachReturn->reason;

                                            ?>
                                                
                                            </td>
                                            <td class="text-center">{{$eachReturn->return_quantity}}</td>
                                            <td class="text-center">{{$eachReturn->prn_no}}</td>
                                            <td class="text-center">
                                            @if($eachReturn->prn_date!='0000-00-00')
                                            {{date('d/m/Y',strtotime($eachReturn->prn_date))}}
                                           	@endif

                                            </td>
                                            <td class="text-center">{{$eachReturn->docket_no}}</td>
                                            <td class="text-center">{{$eachReturn->courier_name}}</td>
                                            
                                            <td class="text-center" title="{{$eachReturn->comments}}">
                                            <?php
                                            $i=strlen($eachReturn->comments); 
                                            if(strlen($i>25)) 
                                                echo substr($eachReturn->comments,0,22).'...';
                                            else 
                                                echo $eachReturn->comments;

                                            ?>
                                                
                                            </td>
                                            
                                            <td class="text-center">
                                           
                                            @if(!empty($eachReturn->good_comments) && count(($eachReturn->good_comments))>0)
                                            <?php                                       
                                       
                                            if(strlen($eachReturn->good_comments[0]->comments)<=20)                                 
                                             echo ($eachReturn->good_comments[0]->comments);
                                            else
                                             echo substr($eachReturn->good_comments[0]->comments,0,18).'..';  

                                            ?>
                                                
                                                <a href="{{route('admin-goods-comments', $eachReturn->id)}}" class="btn btn-warning btn-xs pull-right">
                                                <?php echo count($eachReturn->good_comments)?>
                                                Comment(s)
                                                </a>
                                               
                                            @else
                                               
                                                <a href="{{route('admin-goods-comments', $eachReturn->id)}}" class="btn btn-warning btn-xs pull-right">                     
                                                Comments
                                                </a>
                                               
                                            @endif
                                            </td>
                                            <td class="text-center">
                                            @if($eachReturn->status==1)<a href="javascript:void(0)" class="btn btn-success">Pending</a>
                                            @else<a href="javascript:void(0)" class="btn btn-danger">Closed</a>
                                            @endif</td>
                                        </tr> 
                                        @endforeach                          
                                    </tbody>
                               </table>
                          </div>
                          <div class="row">
                              <div class="col-sm-6">
                                <div class="dataTables_info" role="status" aria-live="polite">Showing {{$returnList->count()}} of {{$returnList->total()}} entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                        {{ $returnList->links() }}
                                    </nav>
                                  </div>
                            </div>
                        </div>
                        </span>
                    </div>
                </div>
            </div>
            
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>

    <!-- /#page-wrapper -->

<script>

$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        $('#load a').css('color', '#dfecf6');
        //$('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');

        var url = $(this).attr('href'); 
        var page=$(this).attr('href').split('page=')[1];
        //alert(page);
        var cat1=$("#cat1").val();
        var cat2=$("#cat2").val();
        var name=$("#name").val();
        
        
     
        filter(page,cat1,cat2,name);
        //window.history.pushState("", "", url);
    });

    
});

$( "#hide-filter" ).click(function() {
    $("#cat1").val('');
    $("#cat2").val('');
    $("#name").val('');
        
    filter(); 
});

function applyFilter(){ 
    
        var cat1=$("#cat1").val();
        var cat2=$("#cat2").val();
        var name=$("#name").val();
       
    
        filter(1,cat1,cat2,name);
}

function filter(page=1,cat1='',cat2='',name=''){ 

    $('.loading').show();
    
    $.ajax({
        type: 'GET', 
        url: '{{route("filter-goods-return")}}',
        data: {page:page,cat1:cat1,cat2:cat2,name:name},
        success:function(data)
        {
            $('#filterResult').html(data);
            $('.loading').hide();
        },
        error: function (error) 
        {
            
            alert('Error Occured Please Try Again');
        }
    })
}
</script>



<script type="text/javascript">
$(document).ready(function () {
			
            $('#example').dataTable({
				'order': [[ 0, "asc" ]],//Default Column Attribute
				'pageLength': 500,//Per Page Item
				//'sDom': 't',//Hide All Info From top and bottom
				'aTargets': [-1], //1st one, start by the right/
				"paging":   false,
        		"info":     false,
				"searching": false,
				'dom': 'Bfrtip',
				'buttons': ['excel', 'pdf', 'print']//['copy', 'csv', 'excel', 'pdf', 'print']
            });
        });
	 setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 2000);
</script>
   
@include('layouts.footer')