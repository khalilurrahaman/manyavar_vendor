@include('layouts.header')
    <!--add user popup Modal Start-->
    <style>
.avl-img{position: absolute;
top: 42px;
right: -9px;
width: 20px;}
</style>
<div class=" quotation-pop" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content none-shadow">
          <div class="modal-header">
            <h4 class="modal-title">Add New User</h4>
          </div>
            <div class="modal-body">
                <form id="user-form" action="{{route('adding-user')}}" method="post">
                {{ csrf_field() }}
                @if(isset($errors))
                            @foreach ($errors->all() as $error)
                              <div style="padding-left: 145px;" class="alert alert-success alert-dismissable">{{ $error }}</div>
                            @endforeach
                    @endif
                    <div class="row shorting-area pricechange">
                        <div class="col-sm-6">
                            <h5>Name :</h5>
                            <input type="text" id="name" name="name" class="form-control" autocomplete='off'>
                        </div>
                        <div class="col-sm-6">
                            <h5>Email ID :</h5>
                            <input type="text" name="email" class="form-control" autocomplete="off">
                        </div>
                        <div class="col-sm-6">
                            <h5>User Name :</h5>
                            <input type="text" name="user_name" class="form-control" autocomplete='off'  onblur="this.value=removeSpaces(this.value);" onkeyup="userAvailability(this.value)"/>
                             <span class="availability"></span>
                             <span class="availability-msg" style="color:red;"></span>
                        </div>
                        <div class="col-sm-6">
                            <h5>Password :</h5>
                            <input type="Password" name="password" class="form-control" autocomplete='off'>
                        </div>
                        <div class="clearfix"></div>
                        
                        <div class="col-sm-6">
                            <h5>Phone No :</h5>
                            <input type="text" name="mobile_no" id="mobile_no" class="form-control" autocomplete="off" maxlength="10" onkeypress="return isNumberKey(event)" onblur="return validation()">
                             <span class="error class_err" id="mobile_message" ></span>
                        </div>
                        <div class="col-sm-6">
                            <h5>Role :</h5>
                            <div class="shorting-area">
                                <div class="select-full">

                                    <select class="select-drop" name="role_id">
                                        @foreach($roleList as $eachRole)
                                        <option class="selected" value="{{$eachRole->id}}">{{$eachRole->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-12">
                        <h5>Address :</h5>
                        <textarea name="address" class="form-control" autocomplete="off"> </textarea>
                        </div>

                        <div class="col-sm-6">
                        <h5>Division :</h5>
                        @foreach($devisionList as $devisionList)
                          <div class="checkbox " style="padding-left: 20px;">
                            <input type="checkbox" name="division[]" value="{{ $devisionList->id }}" >{{ $devisionList->name }}</input>
                            </div>
                            @endforeach
                          
                        </div>
                        
                        <div class="clearfix"></div>
                        <div class="col-sm-6">
                        <input type="submit" name="submit" value="Add User" class="pup-btn" onclick="">
                        </div>
                       <div class="col-sm-6">
                        <a href="{{URL::previous()}}" class="pup-btn">Cancel
                        </a>
                        <div class="clearfix"></div>
                        </div>
                    </div>
                </form>
           </div>
                 <div class="modal-footer"></div>

        </div>

    </div>
</div>
<!--add user popup  Modal End-->
@include('layouts.footer')
<script language="javascript" type="text/javascript">
function removeSpaces(string) {
	if (string.match(/\s/g)){
		alert('User name should take only one word,not spaces!');
	}
 return string.split(' ').join('');
}
</script>
<script type="text/javascript">

function userAvailability(user)
{
	$('.availability-msg').text('');
	$('.availability').html('<img src="{{asset("img/loading-gif-large.gif")}}" alt="Loading" class="avl-img" width="10%">');
    $.ajax({
	type: 'GET',
	url: '{{route("user-availability")}}',
	data: {user:user},
	success:function(data)
	{
		if(data==1)
		{
			$('.availability').html('<img src="{{asset("img/available.png")}}" alt="Availabil" class="avl-img" width="10%">');
		}
		else
		{
			$('.availability').html('<img src="{{asset("img/not-available.png")}}" alt="Not Availabil" class="avl-img" width="10%">');
			$('.availability-msg').text(user+ ' already registered');
		}
	}
   });
}


    $(document).ready(function () {
        $('#user-form').validate({
            rules: {
                name: {
                    required: true,
                },
                email: {
                    required: true,
                },
                user_name: {
                    required: true,
                },
                password: {
                    required: true,
                },
                mobile_no: {
                    required: true,
                }

            }
        });

        $('#name').bind('keyup keydown blur', function(e) {        
           if (e.which === 32 && !this.value.length)
                e.preventDefault();
           $(this).val(function(i, val) {        
               return val.replace(/[^a-zA-Z\s]/gi,''); 
           });       
        });
		
		
    })

    
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
 
  function validation()
   {
       var data=$('#mobile_no').val();
       if(data.length!=10)
       {
           $("#mobile_message").text('Mobile no should accept 10 digits').css("color", "black").show();
           return false;
       }
       else
       {
            $('#mobile_message').hide();
            return true;
       }

    
   
   }   

</script>