<?php 
use App\Http\Controllers\Controller;
if(Controller::userAccessibilityCheck('goods-return-comments'))
	$goods_return_comments_exist=1;
	
?>
 <div class="panel-body custome-table">
                            <div class="table-responsive ">
                                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <!-- <th class="text-center">Prodict Id</th> -->
                                            <th class="text-center">Image</th>
                                            <th class="text-center">Vendor Name</th>
                                            <th class="text-center">Design</th>
                                            <th class="text-center">Colour</th>
                                            <th class="text-center">Defective Remarks</th>
                                            <th class="text-center">Quantity</th>
                                            <th class="text-center">PRN No</th>
                                            <th class="text-center">PRN Date</th>
                                            <th class="text-center">Docket No</th>
                                            <th class="text-center">Courier Name</th>
                                            <!-- <th class="text-center">Admin Remarks</th> -->
                                            <!-- <th class="text-center">Vendor Remarks</th> -->
                                            <th class="text-center">Last Comments</th>
                                            <th class="text-center">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $inc=0;?>
                                        @foreach($returnList as $eachReturn)
                                    <?php $inc++;?>
                                        <tr>
                                            <td class="text-center">
                            <div class="gal"> 
                             <?php 
                               $main_image=URL::to('/img').'/pre-order-img.jpg';
                               if($eachReturn->image!='' && file_exists('upload/temp/'.$eachReturn->image))
                               $main_image=URL::to('/upload/temp').'/'.$eachReturn->image;                                
                              ?>
                               <a class="fancybox" rel="group{{$inc}}" href="{{$main_image}}" >
                                <img src="{{$main_image}}" width="150">
                               </a>                               
                               <a class="fancybox" rel="group{{$inc}}" href="{{$main_image}}" style="display:none;"><img src="{{$main_image}}" alt=""></a>
                               
                            </div>
                        </td>
                                            <td class="text-center">{{$eachReturn->name}}</td>
                                            <td class="text-center">{{$eachReturn->category1}}</td>
                                            <td class="text-center">{{$eachReturn->category2}}</td>
                                            <td class="text-center" title="{{$eachReturn->reason}}">
                                            <?php
                                            $i=strlen($eachReturn->reason); 
                                            if(strlen($i>25)) 
                                                echo substr($eachReturn->reason,0,22).'...';
                                            else 
                                                echo $eachReturn->reason;

                                            ?>
                                                
                                            </td>
                                            <td class="text-center">{{$eachReturn->return_quantity}}</td>
                                            <td class="text-center">{{$eachReturn->prn_no}}</td>
                                            <td class="text-center">
                                            @if($eachReturn->prn_date!='0000-00-00')
                                            {{date('d/m/Y',strtotime($eachReturn->prn_date))}}
                                            @endif

                                            </td>
                                            <td class="text-center">{{$eachReturn->docket_no}}</td>
                                            <td class="text-center">{{$eachReturn->courier_name}}</td>
                                            
                                            <!-- <td class="text-center" title="{{$eachReturn->comments}}">
                                            <?php
                                            $i=strlen($eachReturn->comments); 
                                            if(strlen($i>25)) 
                                                echo substr($eachReturn->comments,0,22).'...';
                                            else 
                                                echo $eachReturn->comments;

                                            ?>
                                                
                                            </td> -->
                                            <!-- <td class="text-center" title="{{$eachReturn->vendor_comments}}">
                                            <?php
                                            $i=strlen($eachReturn->vendor_comments); 
                                            if(strlen($i>25)) 
                                                echo substr($eachReturn->vendor_comments,0,22).'...';
                                            else 
                                                echo $eachReturn->vendor_comments;

                                            ?>
                                            </td> -->
                                            <td class="text-center">
                                            <?php if(!empty($goods_return_comments_exist)){?>
                                            @if(!empty($eachReturn->good_comments) && count(($eachReturn->good_comments))>0)
                                            <?php                                       
                                       
                                            if(strlen($eachReturn->good_comments[0]->comments)<=20)                                 
                                             echo ($eachReturn->good_comments[0]->comments);
                                            else
                                             echo substr($eachReturn->good_comments[0]->comments,0,18).'..';  

                                            ?>
                                                
                                                <a href="{{route('admin-goods-comments', $eachReturn->id)}}" class="btn btn-warning btn-xs pull-right">
                                                <?php echo count($eachReturn->good_comments)?>
                                                Comment(s)
                                                </a>
                                               
                                            @else
                                               
                                                <a href="{{route('admin-goods-comments', $eachReturn->id)}}" class="btn btn-warning btn-xs pull-right">                     
                                                Comments
                                                </a>
                                               
                                            @endif
                                            <?php }?>
                                            </td>
                                            <td class="text-center">
                                            @if($eachReturn->status==1)<button class="btn btn-success" style="cursor: inherit;">Pending</button>
                                            @else<button class="btn btn-danger" style="cursor: inherit;">Closed</button>
                                            @endif</td>
                                        </tr> 
                                        @endforeach                          
                                    </tbody>
                               </table>
                          </div>
                          
                        </div>
                        
                    </div>
                </div>
            </div>

            <!-- /#Hover Image -->
    <script type="text/javascript">$('img').on('mouseover', function() {
    var self = this,
        i = 0,
        images = $(this).data('mouseover').split(/\s+/);
    
    (function nextImage() {
        var next = images[i++ % images.length].split('#');
        $(self).data('timeout', setTimeout(function() {
            self.src = next[0];
            nextImage();
        }, next[1]));
    })();
    
}).on('mouseout', function() {
    clearTimeout($(this).data('timeout'));
    this.src = $(this).attr('src');
});
</script>
            <script type="text/javascript">
$(document).ready(function () {
            
            $('#example').dataTable({
                'order': [[ 1, "asc" ]],//Default Column Attribute
                'pageLength': 10,//Per Page Item
                 "searching": false,
                'dom': 'Bfrtip',//Header Info               
                'buttons': [ {extend: 'excel',exportOptions: {columns: [1,2,3,4,5,6,7,8,9,10,11]},footer: false}],
                'aoColumnDefs': [          
                                   { aTargets: [ 0 ], bSortable: false },
                                   { aTargets: [ 1 ], bSortable: true },
                                   { aTargets: [ 2 ], bSortable: false },
                                   { aTargets: [ 3 ], bSortable: false },
                                   { aTargets: [ 4 ], bSortable: false },
                                   { aTargets: [ 5], bSortable: true },
                                   { aTargets: [ 6 ], bSortable: false },
                                   { aTargets: [ 7 ], bSortable: false },
                                   { aTargets: [ 8 ], bSortable: false },
                                   { aTargets: [ 9 ], bSortable: false },
                                   { aTargets: [ 10 ], bSortable: false },
                                   { aTargets: [ 11 ], bSortable: false }
                                ],
                "oLanguage": {
                                "oPaginate": {
                                                "sNext": '<i class="fa fa-chevron-right" ></i>',
                                                "sPrevious": '<i class="fa fa-chevron-left" ></i>'
                                            }
                            }
            });
        });
     setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 2000);
</script>