@include('layouts.header')
<!--------------------------------Start Shorting-------------------------->
<style>
.dt-buttons{margin-right:0px !important;}
.dt-buttons>a {padding-left:18px;}
</style>
<script src="{{asset('dataTable/css/jquery.dataTables.min.css')}}"></script>
<script src="{{asset('dataTable/css/buttons.dataTables.min.css')}}"></script>

   
<script src="{{asset('dataTable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('dataTable/js/dataTables.buttons.min.js')}}"></script>   
<script src="{{asset('dataTable/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('dataTable/js/jszip.min.js')}}"></script>   
<script src="{{asset('dataTable/js/pdfmake.min.js')}}"></script>
<script src="{{asset('dataTable/js/vfs_fonts.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.print.min.js ')}}"></script>
<!--------------------------------End Shorting-------------------------->
<?php 
use App\Http\Controllers\Controller;
if(Controller::userAccessibilityCheck('section-add'))
	$section_add_exist=1;
if(Controller::userAccessibilityCheck('section-edit'))
	$section_edit_exist=1;
if(Controller::userAccessibilityCheck('section-delete'))
	$section_delete_exist=1;
?>
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <!--<div class="row">
                    <div class="col-lg-12 title-top">
                    <div class="row page-header">
                    	<div class="col-sm-6">
                        	<h1>Vendor <small>Purchase Order Listing</small></h1>
                        </div>
                        <div class="col-sm-6">
                        	<a href="javascript:void(0)" class="btn btn-success">Expand</a>
                        </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-lg-12">
                    @if(isset($errors))
                        @foreach ($errors->all() as $error)

                                        <div class="btn btn-success" id="hide-me" style="width:100%;background-color: green;border: none;">{{ $error }}</div>
                                         @endforeach
                      @endif
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                	<div class="col-sm-6"><h3 class="panel-title">
                                    <i class="fa fa-users" aria-hidden="true"></i> Section Listing</h3></div>
                                    <div class="col-sm-6">
                                    
                                    <div class="shorting-area">
                            		
                              <?php if(!empty($section_add_exist)){?>   
                                <div class="col-sm-3 pull-right">
                                        <a href="{{route('add-section')}}"  class="btn btn-success">
                                        <i class="fa fa-plus" aria-hidden="true"></i> Add New</a>
                                </div>
                                <?php }?>       
                            </div>
                                    
                                    
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">ID</th>
                                                <th class="text-center">Name</th>
                                                <th class="text-center">Description</th>
                                                
                                                <!-- <th class="text-center">Address</th> -->
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    @foreach($list as $value)
                                    <tr>
                                        <td class="text-center">{{$value->id}}</td>
                                        <td class="text-center">{{$value->name}}</td>
                                        <td class="text-center">{{$value->description}}</td>
                                        
                                        
                                        <td class="text-center">
                                        <?php if(!empty($section_edit_exist)){?>   
                                        <a href="{{route('edit-section',$value->id)}}" class="btn btn-success"  >Edit</a>
                                         <?php }?> 
                                         <?php if(!empty($section_delete_exist)){?>       
                                        <a  href="{{route('vendor-sec-delete',$value->id)}}" onclick="return ConfirmDelete()" class="btn btn-danger">Delete</a>
                                         <?php }?> 
                                        </td>
                                    </tr>
                                    
                                    @endforeach
                                    
                                    
                                    
                                    
                                    
                                    
                                </tbody>
                               </table>
                              </div>
                             
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
        <!--add Department popup Modal Start-->
<div id="addDepartment" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add New Section</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
        <form action="{{route('vendor-sec-add')}}" method="post">
        {{ csrf_field() }}
            <div class="col-sm-12">
                <h5>Name :</h5>
                <input type="text" class="form-control" name="name" id="name" >
                <span class=" error class_err" id="name_msg" ></span>
            </div>
            
            
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <h5>Description :</h5>
            <textarea class="form-control" name="description" id="department_description" ></textarea>
            <span class=" error class_err" id="description_msg" ></span>
            </div>
            
            <div class="clearfix"></div>
            <div class="col-sm-6">
            <button class="pup-btn" onclick="return validateForm();">Add Section</button>
            </div>

            <div class="col-sm-6">
                <a href="#" data-dismiss="modal" class="pup-btn">Cancel</a>
            </div>

        </form>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--add Department popup  Modal End-->
<!--edit Department popup Modal Start-->
<div id="editDepartment" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Section</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
        <form action="{{route('vendor-sec-ed')}}" method="post">
        {{ csrf_field() }}
            <div class="col-sm-12">
                <h5>Name :</h5>
                <input type="hidden" class="form-control" name="id" id="deptId" >
                <input type="text" class="form-control" name="name" id="editName" >
                <span class=" error class_err" id="name_msg_edit" ></span>
            </div>
            
            
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <h5>Description :</h5>
            <textarea class="form-control" name="description" id="editDescription" ></textarea>
            <span class=" error class_err" id="description_msg_edit" ></span>
            </div>
            
            <div class="clearfix"></div>
            <div class="col-sm-6">
            <button class="pup-btn" onclick="return validateFormEdit();">Edit Section</button>
            </div>
            <div class="col-sm-6">
               <a href="#" data-dismiss="modal" class="pup-btn">Cancel
                </a>
            </div>
        </form>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--edit Department popup  Modal End-->
<script type="text/javascript">
$(document).ready(function () {
			
            $('#example').dataTable({
				'order': [[ 0, "asc" ]],//Default Column Attribute
				'pageLength': 500,//Per Page Item
				//'sDom': 't',//Hide All Info From top and bottom
				'aTargets': [-1], //1st one, start by the right/
				"paging":   false,
        		"info":     false,
				"searching": false,
				//'dom': 'Bfrtip',
				//'buttons': ['excel', 'pdf', 'print']//['copy', 'csv', 'excel', 'pdf', 'print']
            });
        });
    function validateForm()
  {    
    var ret_val=true;   
   
    var name=$("#name").val();
    if(name==''){$("#name_msg").text('Please Enter Name  !').show();ret_val=false;}
    else{$("#name_msg").hide();}

    var department_description=$("#department_description").val();
    if(department_description==''){$("#description_msg").text('Please Enter Description  !').show();ret_val=false;}
    else{$("#description_msg").hide();}


    return ret_val;

  } 
  function validateFormEdit()
  {    
    var ret_val=true;   
   
    var name=$("#editName").val();
    if(name==''){$("#name_msg_edit").text('Please Enter Name  !').show();ret_val=false;}
    else{$("#name_msg_edit").hide();}

    var department_description=$("#editDescription").val();
    if(department_description==''){$("#description_msg_edit").text('Please Enter Description  !').show();ret_val=false;}
    else{$("#description_msg_edit").hide();}


    return ret_val;

  }

  function getEditId(id){
    $('#deptId').val('');  

    var eurl = "{{route('sec-eddata')}}";    
    var token = "{{ csrf_token()}}";
    $.ajax({
        type: 'POST', 
        url: eurl,
        data: {'id' : id , '_token' : token},
       
        success:function(data){
          $('#deptId').val(data.id);
          $('#editName').val(data.name);
          $('#editDescription').val(data.description);
          
        }
    });
 

    }



</script>
<script>
   /* function ConfirmDelete(id)
    {
        var x = confirm("Are you sure you want to delete?");
         if (x)
        {
            
            var eurl = "<?php echo url('/vendor-department-delete') ?>";
            eurl = eurl+'/'+id;
            var token = "{{ csrf_token()}}";
            $.ajax({
                type: 'POST', 
                url: eurl,
                data: {id:id , _token:token},
                dataType: 'json',
                
                   success: function(status){
                  if(status=='success')
                  {           
                    history.go(0)  
                  }
        }
                
            });

             
        }
        else
        return false;
    }*/
    function ConfirmDelete(id)
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 2000);
</script> 
@include('layouts.footer') 