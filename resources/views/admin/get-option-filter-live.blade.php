<!--<h5>Vendor List Filter</h5>-->

@if(!empty($cat2_list) && count($cat2_list)>0)
<div class="filter-right">
<div class="filter-area list-filter pop-filter">
<!--<h5>Vendor List Filter</h5>-->
    <div><a href="javascript:void(0)" class="closebtn" id="hide-filter" >×<div class="clearfix"></div></a></div>
    <div class="clear"></div>
    
       
        <div class="col-sm-3">
            <select class="select-drop" id="selectcat2" name="selectcat2">
            	<option value="">Select Colour</option>
                @if(!empty($cat2_list) && count($cat2_list)>0)
                @foreach($cat2_list as $cat2)
                	<option value="{{$cat2->cat2}}">{{$cat2->cat2}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="col-sm-3">
            <select class="select-drop" id="selectcat3" name="selectcat3">
            	<option value="">Select Cat3</option>
                @if(!empty($cat3_list) && count($cat3_list)>0)
                @foreach($cat3_list as $cat3)
                	<option value="{{$cat3->cat3}}">{{$cat3->cat3}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="col-sm-3">
            <select class="select-drop" id="selectcat4" name="selectcat4">
            	<option value="">Select Width/Size</option>
                @if(!empty($cat4_list) && count($cat4_list)>0)
                @foreach($cat4_list as $cat4)
                	<option value="{{$cat4->cat4}}">{{$cat4->cat4}}</option>
                @endforeach
                @endif
            </select>
        </div>
        
        <div class="col-sm-3">
        	<a href="javascript:void(0)" class="btn btn-success selectSearch">Apply</a>
        </div>
<div class="clearfix"></div>        
	</div>
    <div class="clearfix"></div>
    </div> 
@else
 <h5 style="color:#F00;">Invalid Search!</h5>
@endif

<script>
$( "#hide-filter" ).click(function() {
	$('#mySidenavR').hide();
	$('#merge_with').val('');
    $('#search_exist').val('');
    
  $('#existItemLIstModal').html('<tr><td class="text-center" colspan="6" style="border:none;">Search Product!</td></tr>');
 //filter();
});

function filter(selectcat1='',selectcat2='',selectcat3='',selectcat4=''){
		$('.loading').show(); 
 		$.ajax({
			type: 'GET', 
			url: '{{route("search-item-by-keyword-live")}}',
			data: {selectcat1:selectcat1,selectcat2:selectcat2,selectcat3:selectcat3,selectcat4:selectcat4},
			success:function(data)
			{
        		$('#existItemLIstModal').html(data);
				$('.loading').hide();
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});
}

$('.selectSearch').click(function(){
	     var selectcat1 = $('#search_exist').val();
		 var selectcat2 = $('#selectcat2').val();
		 var selectcat3 = $('#selectcat3').val();
		 var selectcat4 = $('#selectcat4').val();
		filter(selectcat1,selectcat2,selectcat3,selectcat4) ;
});


</script>
