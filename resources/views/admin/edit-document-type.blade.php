@include('layouts.header');
<link href="{{asset('css/uploadfilemulti.css')}}" rel="stylesheet">
<script src="{{asset('js/multi-file-upload/jquery-1.8.0.min.js')}}"></script>
<script src="{{asset('js/multi-file-upload/jquery.fileuploadmulti.min.js')}}"></script>
    <!--add user popup Modal Start-->
<div class=" quotation-pop" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content none-shadow">
          <div class="modal-header">
            <h4 class="modal-title">Edit Document Type</h4>
          </div>
            <div class="modal-body">

                <form action="{{route('update-document-type')}}" method="post" enctype="multipart/form-data">  
                    <div class="row shorting-area pricechange">
                    {{ csrf_field() }}
                    @if(isset($errors))
                            @foreach ($errors->all() as $error)
                              <div style="padding-left: 145px;" class="alert alert-success alert-dismissable">{{ $error }}</div>
                            @endforeach
                    @endif
                      

                       
                       <input type="hidden" name="id" class="form-control" required value='{{$data->id}}' /> 
                        
                         
                        <div class="clearfix"></div>
                        <div class="col-sm-12">
                            <h5>Type Name:</h5>
                            <input type="text" name="type_name" class="form-control" required value='{{$data->type_name}}' />       
                        </div>
                        <div class="col-sm-12">
                            <h5>Description :</h5>
                            <textarea name="description" class="form-control" required >{{$data->description}}</textarea>      
                        </div>

                        <div class="col-sm-6">
                        <input type="submit"  name="submit" onclick="return validation()"  value="Submit" class="pup-btn">
                        </div>
                        <div class="col-sm-6">
                        <a href="{{route('document-type-list')}}" class="pup-btn">Cancel
                        </a>
                        <div class="clearfix"></div>
                    </div>
                     </form> 
               
           </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{asset('js/jquery-1.6.1.min.js')}}"></script>
     <script type='text/javascript'>
          $(document).ready(function(){
                // This will add new input field
               $("#add-file-field").click(function(){
                    $("#text").append("<div class='added-field'><div class='col-sm-8'><input name='file[]' type='file' class='form-control'/></div><input type='button' class='remove-btn btn btn-danger col-sm-2' value='Remove' /></div>");
               });
               // The live function binds elements which are added to the DOM at later time
               // So the newly added field can be removed too
               $(".remove-btn").live('click',function() {
                    $(this).parent().remove();
               });
          });
     </script>
<!--add user popup  Modal End-->
@include('layouts.footer');
