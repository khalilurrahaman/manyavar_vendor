<?php 
use App\Http\Controllers\Controller;
if(Controller::userAccessibilityCheck('action-accept'))
	$action_accept_exist=1;
if(Controller::userAccessibilityCheck('action-reject'))
	$action_reject_exist=1;
if(Controller::userAccessibilityCheck('action-close'))
	$action_close_exist=1;
if(Controller::userAccessibilityCheck('action-comments'))
	$action_comments_exist=1;
?>
                            <div class="panel-body custome-table">
                            
                                <div class="table-responsive view-table">

                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Sr. No</th>
                                                <th class="text-center">Request</th>
                                                <th class="text-center">PO Details</th>
                                                <th class="text-center">Product Info<br>(Design and Color)</th>
                                                <th class="text-center">Vendor Name</th>
                                                <th class="text-center">Delivery Date</th>
                                                <th class="text-center">Ext. Date</th>
                                                <th class="text-center">No Of Extended</th>
                                                <th class="text-center">Reason</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-center">Action</th>
                                                <th class="text-center">Last Comments</th>
                                            </tr>
                                        </thead>
                                    <tbody id="req_request">
                                    @if(!empty($request_list))
                                    <?php $inc=$request_list->perPage() * ($request_list->currentPage()-1);;?>
                                    @foreach($request_list as $req)
                                    <?php $inc++; ?>
                                    <tr>
                                    
                                      <td class="text-center">{{$inc}}</td>
                                      <td class="text-center">
                                        {{date("d/m/y",strtotime($req->created_at))}}<br>
                                        <?php 
                                        $type='';
                                        if($req->action_type=='date_ext')
                                          $type='Due Date Extention'; 
                                        ?>
                                         {{$type}}
                                        </td>                                      
                                       <td class="text-center">
                                       <span class="po-no<?php echo $inc;?>">{{$req->po_no}}</span><br>
                                        <span class="po-date<?php echo $inc;?>">{{date("d/m/y",strtotime($req->order_date))}}</span>
                                      </td>  
                                       <td class="text-center">
                                        {{$req->cat1}}<br>
                                        {{$req->cat2}}
                                      </td> 
                                        <td class="text-center">
                                        @if(!empty($req->ven_dtls->name)){{$req->ven_dtls->name}}@endif</td>
                                        <td class="text-center">@if(!empty($req->po_date)){{date("d/m/y",strtotime($req->po_date))}}@endif</td>
                                        <td class="text-center">{{date("d/m/y",strtotime($req->ext_date))}}</td>
                                        <td class="text-center">
                                          {{$req->extended_no}}
                                        </td>     
                                        <td class="text-center">
                                        {{$req->Reason}}
                                        </td>
                                        <td class="text-center">
                                       
                                       @if($req->status=='new')
                                       <span class="sm-btn btn-primary btn-xs">New</span>
                                       @elseif($req->status=='close')
                                       <span class="sm-btn btn-primary btn-warning">Accepted</span>
                                       @elseif($req->status=='accept')
                                       <span class="sm-btn btn-primary btn-warning">Accepted</span>
                                       @elseif($req->status=='reject')
                                       <span class="sm-btn btn-primary btn-danger">Rejected</span> 
                                       @endif
                                        </td>
                                        
                                        <td class="text-center">
                                        @if($req->status=='new')
                                         @if($req->notify==1)
                                         <span class="blnk_txt">Please Take Action</span>
                                         @endif<br>
                                       <?php if(!empty($action_accept_exist)){?>
                                        <a data-toggle="modal" data-target="#vendorRequestaccept" data-placement="top" title="" class="btn btn-success vendorRequestaccept" unique-key="<?php echo $inc;?>" action_id="{{$req->id}}" action_type="{{$req->action_type}}" old_value="{{$req->old_value}}" req_rate="{{$req->new_rate}}" old_width="{{$req->old_width}}" req_width="{{$req->new_width}}" cat_1_details="{{$req->cat1}}" cat_2_details="{{$req->cat2}}" due_date="{{date('d/m/y',strtotime($req->po_date))}}" extd_date="{{date('d/m/y',strtotime($req->ext_date))}}" reason="{{$req->Reason}}">Accept</a><?php }?> 
                                       <?php if(!empty($action_reject_exist)){?>
                                        <a data-toggle="modal" data-target="#vendorRequestreject" data-placement="top" title="" class="btn btn-danger vendorRequestreject" unique-key="<?php echo $inc;?>" action_id="{{$req->id}}" action_type="{{$req->action_type}}" req_rate="{{$req->new_rate}}" old_width="{{$req->old_width}}" req_width="{{$req->new_width}}" old_value="{{$req->old_value}}">Reject</a><?php }?> 
                                        @elseif($req->status=='accept')
                                        <span class="sm-btn btn-primary btn-warning">Closed</span>
                                        @elseif($req->status=='reject')
                                        <span class="sm-btn btn-primary btn-warning">Closed</span> 
                                        @elseif($req->status=='close')
                                        <span class="sm-btn btn-primary btn-warning">Closed</span> 
                                        @endif
                                        </td>

                                        <td class="text-left width-e">
                                        <?php if(!empty($action_comments_exist)){?> 
                                        @if(!empty($req->req_comments) && count(($req->req_comments))>0)
                                        <?php                                       
                                       
                                        if(strlen($req->req_comments[0]->comments)<=20)                                 
                                           echo ($req->req_comments[0]->comments);
                                        else
                                         echo substr($req->req_comments[0]->comments,0,18).'..';  

                                        ?>
                                        <a href="{{route('admin-requestwise-comments', $req->id)}}" class="pull-right btn btn-warning btn-xs">
                                        <?php echo count($req->req_comments)?>
                                        Comment(s)</a>
                                        @else
                                        <a href="{{route('admin-requestwise-comments', $req->id)}}" class="btn btn-warning btn-xs pull-right">
                                        
                                        Comments</a>
                                        
                                         @endif
                                         <?php }?> 
                                         </td>

                                    </tr>
                                    @endforeach
                                    @endif 
                                     
                                </tbody> 
                               </table>

                              </div>
                              <div class="row">
                              <div class="col-sm-6">
                                <div class="dataTables_info" role="status" aria-live="polite">Showing {{$request_list->count()}} of {{$request_list->total()}} entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                      {{ $request_list->links() }}   
                                    </nav>
                                  </div>
                              </div>
                            </div>
                            
