@include('layouts.header');
    <!--add user popup Modal Start-->
<div class=" quotation-pop" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Edit Profile</h4>
          </div>
            <div class="modal-body">

                <form action="{{route('admin-profileupdate-req')}}" method="post">  
                    <div class="row shorting-area pricechange">
                    {{ csrf_field() }}
                    <input type="hidden"  name="id" value='{{$userdata->id}}' >
                    @if(isset($errors))
                            @foreach ($errors->all() as $error)
                              <div style="padding-left: 145px;" id="hide-me" class="alert alert-success alert-dismissable">{{ $error }}</div>
                            @endforeach
                    @endif
                      <div class="col-sm-12">
                            <h5>Name :</h5>
                            <input type="text" name="name" value='{{$userdata->name}}' class="form-control">
                        </div>

                        <div class="col-sm-12">
                            <h5>User Name :</h5>
                            <input type="text" name="username" value='{{$userdata->username}}' class="form-control">
                        </div>
                        <div class="col-sm-12">
                            <h5>Email ID :</h5>
                            <input type="text" name="email" value='{{$userdata->email}}' class="form-control">
                        </div>
                        <div class="col-sm-12">
                            <h5>Mobile :</h5>
                            <input type="text" name="mobile" value='{{$userdata->mobile}}' id="mobile" onkeypress="return isNumberKey(event)" class="form-control">
                            <span class="error class_err" id="mobile_message" ></span>

                        </div>
                        <div class="col-sm-12">
                            <h5>Address :</h5>
                            <input type="text" name="address" value='{{$userdata->address}}' class="form-control">
                        </div>
                         
                        <div class="clearfix"></div>
                        <div class="col-sm-6">
                        <input type="submit" name="submit" value="Submit" onclick="return validation()" class="pup-btn">
                        </div>
                        <div class="col-sm-6">
                        <a href="{{route('dashboard')}}" class="pup-btn">Cancel
                        </a>
                        <div class="clearfix"></div>
                    </div>
                     </form> 
               
           </div>
        </div>
    </div>
</div>
<!--add user popup  Modal End-->
@include('layouts.footer');

<script>
  function validation()
   {
       var data=$('#mobile').val();
       if(data.length!=10)
       {
           $("#mobile_message").text('Mobile no should accept 10 digits').css("color", "red").show();
           return false;
       }
       else
       {
            $('#mobile_message').hide();
            return true;
       }

    
   
   }
   function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me" ).fadeOut( "slow", function() {
    // Animation complete.
    window.location = "<?php echo route('dashboard')?>";
    });
}, 2000);
    
</script>
