@include('layouts.header')
<!--------------------------------Start Shorting-------------------------->
<style>
.dt-buttons{margin-right:0px !important;}
.dt-buttons>a {padding-left:18px;}
</style>
<script src="{{asset('dataTable/css/jquery.dataTables.min.css')}}"></script>
<script src="{{asset('dataTable/css/buttons.dataTables.min.css')}}"></script>

   
<script src="{{asset('dataTable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('dataTable/js/dataTables.buttons.min.js')}}"></script>   
<script src="{{asset('dataTable/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('dataTable/js/jszip.min.js')}}"></script>   
<script src="{{asset('dataTable/js/pdfmake.min.js')}}"></script>
<script src="{{asset('dataTable/js/vfs_fonts.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.print.min.js ')}}"></script>
<!--------------------------------End Shorting-------------------------->
<?php 
use App\Http\Controllers\Controller;
if(Controller::userAccessibilityCheck('vendor-set-password'))
	$vendor_set_password_exist=1;
if(Controller::userAccessibilityCheck('vendor-status'))
	$vendor_status_exist=1;
?>
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <!--<div class="row">
                    <div class="col-lg-12 title-top">
                    <div class="row page-header">
                    	<div class="col-sm-6">
                        	<h1>Vendor <small>Purchase Order Listing</small></h1>
                        </div>
                        <div class="col-sm-6">
                        	<a href="javascript:void(0)" class="btn btn-success">Expand</a>
                        </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-lg-12">
                    @if(isset($errors))
                        @foreach ($errors->all() as $error)

                                        <div class="btn btn-success" id="hide-me" style="width:100%;background-color: green;border: none;">{{ $error }}</div>
                                         @endforeach
                      @endif
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                	<div class="col-sm-6"><h3 class="panel-title">
                                    <i class="fa fa-users" aria-hidden="true"></i> Vendor Listing</h3></div>
                                    <div class="col-sm-6">
                                    
                                        <div class="row shorting-area">
                                            <div class="col-sm-2 purchase-filter pull-right">
                                            <a href="javascript:void(0)" id="show-btn" class="btn btn-success">
                                            <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                            <div class="row shorting-area">
                                <!--<div class="col-sm-6">
                                <div class="dataTables_info" role="status" aria-live="polite">Showing 1 to 9 of 9 entries</div>
                              </div>
                              <div class="col-sm-2 pull-right">
                                        <a class="btn btn-default btn-select">
                                            <input type="hidden" class="btn-select-input" id="" name="" value="" />
                                            <span class="btn-select-value">Select an Item</span>
                                            <span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
                                            <ul>
                                                <li class="selected">Short By</li>
                                                <li>Due Date</li>
                                                <li>Order Date</li>
                                                <li>Pending Qty</li>
                                            </ul>
                                        </a>
                                </div>
                            <div class="col-sm-1 pull-right">
                                        <a href="javascript:void(0)" id="show-btn" class="btn btn-success">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                </div>--> <div class="clearfix"></div>
                            <div class="col-sm-12">
                            <div id="mySidenavR" class="sidenavR" style="display:none;">
<div class="filter-right">
<div class="filter-area">
<!--<h5>Vendor List Filter</h5>-->
    <a href="javascript:void(0)" class="closebtn" id="hide-filter">×</a>
        <div class="col-sm-4">
           <input class="form-control" id="name" name="name" placeholder="Vendor Name" />
            <span class=" error class_err" id="name_msg" ></span>
        </div>

        <div class="col-sm-4">
            <input class="form-control" id="email" name="email" placeholder="Email ID" />

        </div>
             
        
        <div class="col-sm-2">
            <a href="javascript:void(0)" class="btn btn-success" onclick="applyFilter();">Apply Filter</a>
            <div class="clearfix"></div>
        </div>
<div class="clearfix"></div>        
    </div>
    <div class="clearfix"></div>
    </div>    
</div>
                            
                            </div>
                            
                            
                            </div>
                            <span id="filterResult">
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Name</th>
                                                <!-- <th class="text-center">Department</th>
                                                <th class="text-center">Section</th>
                                                <th class="text-center">Devision</th> -->
                                                <th class="text-center">E-mail ID</th>
                                                <th class="text-center">Phone No</th>
                                                <th class="text-center">Address</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-center">Action</th>
                                                
                                            </tr>
                                        </thead>
                                    <tbody>
                                    @foreach($vendor_list as $data)
                                    <tr>
                                        <td class="text-center">{{$data->name}}</td>
                                       
                                        <td class="text-center">{{$data->email}}</td>
                                        <td class="text-center">{{$data->mobile}}</td>
                                        <td class="text-center">{{$data->address}}</td>
                                        <td class="text-center">
                                        @if($data->status==1)
                                        Active
                                        @else
                                        Inactive
                                        @endif

                                        </td>
                                        <td class="text-center">
                                        <?php if(!empty($vendor_set_password_exist)){?>   
                                        <a onclick="" href="{{route('set-ven-password',$data->id)}}" class="btn btn-success">Set Password</a>
                                        <?php }?>
                                        <?php if(!empty($vendor_status_exist)){?>   
                                        @if($data->status==1)
                                        <a href="{{route('vendor-status-deact',$data->id)}}" onclick="return ConfirmDeactive()" class="btn btn-danger">Inactive</a>
                                        @else
                                        <a href="{{route('vendor-status-act',$data->id)}}" onclick="return ConfirmActive()" class="btn btn-success">Active</a>
                                        @endif
										<?php }?>
                                        </td>
                                    </tr>
                                    @endforeach
                                      
                                </tbody>
                               </table>
                              </div>
                              
                              
                              </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
        <script type="text/javascript">
		$(document).ready(function () {
            
            $('#example').dataTable({
                'order': [[ 0, "asc" ]],//Default Column Attribute
                'pageLength': 10,//Per Page Item
                 "searching": false,
                'dom': 'Bfrtip',//Header Info               
                'buttons': [ {extend: 'excel',exportOptions: {columns: [0,1,2,3,4]},footer: false}],
                'aoColumnDefs': [          
                                   { aTargets: [ 0 ], bSortable: true },
                                   { aTargets: [ 1 ], bSortable: false },
                                   { aTargets: [ 2 ], bSortable: false },
                                   { aTargets: [ 3 ], bSortable: false },
                                   { aTargets: [ 4 ], bSortable: false },
                                   { aTargets: [ 5 ], bSortable: false }
                                ],
                "oLanguage": {
                                "oPaginate": {
                                                "sNext": '<i class="fa fa-chevron-right" ></i>',
                                                "sPrevious": '<i class="fa fa-chevron-left" ></i>'
                                            }
                            }
            });
        });
        function ConfirmActive(id)
        {
            var x = confirm("Are you sure you want to Activate?");
            if (x)
                return true;
            else
                return false;
        }function ConfirmDeactive(id)
        {
            var x = confirm("Are you sure you want to Inactivate?");
            if (x)
                return true;
            else
                return false;
        }


        $(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        //$('#load a').css('color', '#dfecf6');
        //$('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');

        var url = $(this).attr('href'); 
        var page=$(this).attr('href').split('page=')[1];
        
        var name=$("#name").val();
        var email=$("#email").val();
        
     
        filter(page,name,email);
        //window.history.pushState("", "", url);
    });

    
});
function applyFilter()
{

    var name=$("#name").val();
    var email=$("#email").val();
    var ret_val=true;
      
    if(name=='' && email==''){
        $("#name_msg").text('Please Enter Name Or Email ID of Vendor !').css('color','white').show();
        ret_val=false;
        return ret_val; 
    }
    else
    {
        $("#name_msg").hide();
        var ret_val=true;
        
    }
    if(ret_val)
    {
        
     
        filter(1,name,email);
    }
}

$( "#hide-filter" ).click(function() {
    
    $("#name").val('');
    $("#email").val('');
    
    
    //filter(); 
});

function filter(page=1,name='',email=''){ 

    
    
    $('.loading').show();
    $.ajax({
        type: 'GET', 
        url: '{{route("filter-vendor-list")}}',
        data: {name:name,email:email,page:page},
        success:function(data)
        {
            $('#filterResult').html(data);
            $('.loading').hide();
        },
        error: function (error) 
        {
            
            alert('Error Occured Please Try Again');
        }
    })
    

}

setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 2000);
        </script>
@include('layouts.footer')   