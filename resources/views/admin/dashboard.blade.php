@include('layouts.header');
<?php
use App\Http\Controllers\Controller;
if(Controller::userAccessibilityCheck('latest-request-list'))
	$latest_request_list_exist=1;
if(Controller::userAccessibilityCheck('latest-request-details'))
	$latest_request_details_exist=1;
if(Controller::userAccessibilityCheck('latest-request-accept'))
	$latest_request_accept_exist=1;	
if(Controller::userAccessibilityCheck('latest-request-reject'))
	$latest_request_reject_exist=1;
if(Controller::userAccessibilityCheck('dashboard-notifications'))
	$dashboard_notifications_exist=1;
?>
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <div class="row">
                    <div class="col-lg-8 dash-left">
                    
                     <style type="text/css">
                                    .blnk_txt {   
                                      animation-duration: 400ms;
                                      animation-name: blink;
                                      animation-iteration-count: infinite;
                                      animation-direction: alternate;
                                    }
                                   
                                    @keyframes blink {
                                      from {
                                        opacity: 1;
                                      }

                                      to {
                                        opacity: .5;
                                      }
                                    }
                                </style>
                    <div class="row">
                    	<div class="col-sm-12">
                            @if(isset($errors))
                                @foreach ($errors->all() as $error)
                                    <div class="btn btn-success" id="hide-me" style="width:100%;background-color:red;border:none;">{{ $error }}</div>
                                @endforeach
                            @endif
                            <?php if(!empty($latest_request_list_exist)){?>
                    		<div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title pull-left" style="padding-top:6px; padding-bottom:6px;">
                                    <i class="fa fa-list-alt" aria-hidden="true"></i> 
                                    Latest Request 
                                    </h3>
                                    
                                </div>
                                <div class="clearfix"></div>
                                <div class="panel-body pre-order custome-table">
                                    <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" style="margin-bottom:0;">
                                        <thead>
                                            <tr>                                                   
                                                <th class="text-center">Sr. No</th>
                                                <th class="text-center">PO Details</th>
                                                <th class="text-center">ICODE</th>
                                                <th class="text-center">Product Info<br>(Design and Color)</th>
                                                <th class="text-center">Vendor Name</th>
                                                <th class="text-center">Request</th>
                                                <th class="text-center">Remarks/Reason</th>
                                                <th class="text-center">Action</th>

                                            </tr>
                                        </thead>
                                    <tbody>
                                    @if(count($request_list)>0)
                                    <?php $inc=0;?>
                                    @foreach($request_list as $req)
                                    <?php $inc++; ?>
                                    <tr>
                                    
                                        <td class="text-center">{{$inc}}</td>

                                        <td class="text-center">
                                        <span class="po-no<?php echo $inc;?>">{{$req->po_no}}</span><br>
                                        <span class="po-date<?php echo $inc;?>">{{date("d/m/y",strtotime($req->order_date))}}</span>
                                        </td>
                                        <td class="text-center">{{$req->ICODE}}</td>
                                        <td class="text-center">
                                       {{$req->cat1}}<br>
                                       {{$req->cat2}}
                                      </td> 
                                        <td  class="text-center">
                                        @if(!empty($req->ven_name)){{$req->ven_name}}@endif
                                        </td>


                                        <td class="text-center">
                                        <?php 
                                        $type='';
                                        if($req->action_type=='rate_change')
                                            $type='Request For Rate Change';          
                                        elseif($req->action_type=='width_change') 
                                            $type='Request For Width Change';

                                        elseif($req->action_type=='date_ext')
                                            $type='Request For Due Date Extention'; 

                                        elseif($req->action_type=='cutting')
                                            $type='Request For Sample';
                                        elseif($req->action_type=='cancel') 
                                            $type='Request For Cancellation';
                                        elseif($req->action_type=='others') 
                                            $type='Request For Others';
                                        elseif($req->action_type=='sample') 
                                            $type='Sample for Approval';



                                        ?>
                                        {{$type}}<br>
                                        {{date("d/m/y",strtotime($req->created_at))}}
                                        </td>
                                        <td class="text-center">
                                        @if(!empty($req->Reason)){{$req->Reason}}@endif
                                        @if(!empty($req->remarks)){{$req->remarks}}@endif
                                        </td>
                                        <td class="text-center">
                                        @if($req->status=='new')
                                        <!-- <a class="btn btn-success requestDetails" data-toggle="modal" data-target="#requestDetails" data-placement="top" title="" unique-key="<?php echo $inc;?>" action_id="{{$req->id}}" action_type="{{$req->action_type}}" old_value="{{$req->old_value}}" req_rate="{{$req->new_rate}}" old_width="{{$req->old_width}}" req_width="{{$req->new_width}}" pen_qty="{{$req->pending_qty}}" can_qty="{{$req->cancel_qty}}" cat_1_details="{{$req->cat1}}" cat_2_details="{{$req->cat2}}">Details</a> -->
                                        
                                        @if($req->action_type!='others')
                                        <?php if(!empty($latest_request_details_exist)){?>
                                        <a data-toggle="modal" data-target="#vendorRequestaccept" data-placement="top" title="" class="btn btn-success vendorRequestaccept" unique-key="<?php echo $inc;?>" action_id="{{$req->id}}" action_type="{{$req->action_type}}" old_value="{{$req->old_value}}" req_rate="{{$req->new_rate}}" old_width="{{$req->old_width}}" req_width="{{$req->new_width}}" pen_qty="{{$req->pending_qty}}" can_qty="{{$req->cancel_qty}}" cat_1_details="{{$req->cat1}}" cat_2_details="{{$req->cat2}}" reason="{{$req->Reason}}" due_date="{{date('d/m/y',strtotime($req->po_date))}}" extd_date="{{date('d/m/y',strtotime($req->ext_date))}}">
                                        Details
                                        </a>
                                        <?php }?>
                                         @else
                                        <?php if(!empty($latest_request_accept_exist)){?>
                                        <a href="{{route('admin-acceptothers-request',$req->id)}}" class="btn btn-success">Accept</a>
                                        <?php }?>
                                        @endif
                                        <?php if(!empty($latest_request_reject_exist)){?>
                                        <a data-toggle="modal" data-target="#vendorRequestreject" data-placement="top" title="" class="btn btn-danger vendorRequestreject" unique-key="<?php echo $inc;?>" action_id="{{$req->id}}" action_type="{{$req->action_type}}" old_value="{{$req->old_value}}" req_rate="{{$req->new_rate}}" old_width="{{$req->old_width}}" req_width="{{$req->new_width}}" pen_qty="{{$req->pending_qty}}" can_qty="{{$req->cancel_qty}}" cat_1_details="{{$req->cat1}}" cat_2_details="{{$req->cat2}}" reason="{{$req->Reason}}" due_date="{{date('d/m/y',strtotime($req->po_date))}}" extd_date="{{date('d/m/y',strtotime($req->ext_date))}}">
                                        Reject
                                        </a>
                                        <?php }?>
                                        @endif
                                        
                                        </td>

                                    </tr>
                                    @endforeach

                                    @else

                                    <tr>
                                    
                                      <td class="text-center" colspan="8"> No New Request Available </td>

                                    </tr>
                                    @endif 
                                </tbody>
                               </table>
                              </div>
                                </div>
                            </div>
                            <?php }?>
                        </div>
                    </div>
                    </div>
                    <div class="col-lg-4">
                    <?php if(!empty($dashboard_notifications_exist)){?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-bell" aria-hidden="true"></i> Notifications (@if(!empty($notificationList) && count($notificationList)>0) {{$notificationList->tot}} 
                                 @else 
                                 0
                                 @endif) </h3>
                            </div>
                           <div class="panel-body">
                                <div class="list-group">
                                	@if(!empty($notificationList) && count($notificationList)>0)
                                    @foreach($notificationList as $notification)
                                 	<a href="javascript:void(0)" onclick="redirectUrl({{$notification->id}},'{{$notification->red_url}}')" class="list-group-item">
                                        <span class="badge" style="padding: 10px;">
										<?php 
										$time_one = new DateTime(date('Y-m-d H:i:s')) ;
										$time_two = new DateTime($notification->created_at);
										$difference = $time_one->diff($time_two);
										echo $difference->format('%a days %h hours %i minutes');?> ago
                                        </span>
                                        
                                        <i class="fa fa-fw fa-check"></i>{{$notification->message}} by {{$notification->sender_name}}
                                  			
                                    </a>
                                    @endforeach
                                    @else
                                        <i class="fa fa-fw fa-check"></i>Notification Board is Empty!
                                    @endif
                                    
                                </div>
                                <div class="text-right">
                                    <a href="{{route('notification')}}">View All Notifications &nbsp; <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <?php }?>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
<!--vendorRequestDetails Modal Start-->
<div id="requestDetails" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Details</h4>
      </div>
      <div class="modal-body vendorRequestdetails-modal-body">
        <div class="row shorting-area pricechange">
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorRequestdetails-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>Date : <strong class="vendorRequestdetails-date"><!--3000--></strong></h5></div>
            <div class="col-sm-3" id="vendorRequestdetails-cat_1_details"><h5>Design : <strong class="vendorRequestdetails-cat_1_details"></strong></h5></div> 
  			<div class="col-sm-3" id="vendorRequestdetails-cat_2_details"><h5>Color : <strong class="vendorRequestdetails-cat_2_details"></strong></h5></div> 
            <div class="col-sm-3" id="pen-qty-details"><h5>Pending Qty : <strong class="vendorRequestdetails-pen-qty"></strong></h5></div>
            <div class="col-sm-3" id="can-qty-details"><h5>Requested Cancel Qty : <strong class="vendorRequestdetails-can-qty"></strong></h5></div> 
            <div class="col-sm-3" id="old-rate-details"><h5>Old Rate : <strong class="vendorRequestdetails-old-rate"></strong></h5></div> 
            <div class="col-sm-3" id="reqst-rate-details"><h5>Requested Rate : <strong class="vendorRequestdetails-request-rate"></strong></h5></div>
            <div class="col-sm-3" id="old-width-details"><h5>Old Width : <strong class="vendorRequestdetails-old-width"></strong></h5></div> 
            <div class="col-sm-3" id="reqes-width-details"><h5>Requested Width : <strong class="vendorRequestdetails-reqes-width"></strong></h5></div> 
            <div class="clearfix"></div>
            <div class="clearfix"></div>
        </div>
      </div>
    </div>

  </div>
</div>
<!--vendorRequestDetails Modal End-->
<!--vendorRequestaccept Modal Start-->
<div id="vendorRequestaccept" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span class="vendorRequestaccept-act_type"></span>&nbsp Request Details</h4>
      </div>
      <div class="modal-body vendorRequestaccept-modal-body">
        <div class="row shorting-area pricechange">
        <input type="hidden" name="type" id="type" value="" />
         <input type="hidden" name="action_id" id="action_id" value="" /> 
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorRequestaccept-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>Date : <strong class="vendorRequestaccept-date"><!--3000--></strong></h5></div>
            <div class="col-sm-3" id="vendorRequestaccept-due-date"><h5>Due Date : <strong class="vendorRequestaccept-due-date"></strong></h5></div> 
             <div class="col-sm-3" id="vendorRequestdetails-cat_1_details"><h5>Design : <strong class="vendorRequestdetails-cat_1_details"></strong></h5></div> 
            <div class="col-sm-3" id="vendorRequestdetails-cat_2_details"><h5>Color : <strong class="vendorRequestdetails-cat_2_details"></strong></h5></div> 
            <div class="col-sm-3" id="vendorRequestdetails-cat_4_details"><h5>Size/Width : <strong class="vendorRequestdetails-cat_4_details"></strong></h5></div> 
           <!--  <div class="col-sm-3" id="act_type"><h5>Request Type : <strong class="vendorRequestaccept-act_type"></strong></h5></div>  -->
            <div class="col-sm-3" id="pen-qty"><h5>Pending Qty : <strong class="vendorRequestaccept-pen-qty"></strong></h5></div> 

            <div class="col-sm-3" id="can-qty"><h5>Requested Cancel Qty : <strong class="vendorRequestaccept-can-qty"></strong></h5></div> 
            <div class="col-sm-3" id="vendorRequestdetails-cancel-reason"><h5>Reason : <strong class="vendorRequestdetails-cancel-reason"></strong></h5></div> 
            <div class="col-sm-3" id="old-rate"><h5>Old Rate : <strong class="vendorRequestaccept-old-rate"></strong></h5></div> 
            
            <div class="col-sm-3" id="reqst-rate"><h5>Requested Rate : <strong class="vendorRequestaccept-request-rate"></strong></h5></div>

            <div class="col-sm-3" id="old-width"><h5>Old Width : <strong class="vendorRequestaccept-old-width"></strong></h5></div> 

            <div class="col-sm-3" id="reqes-width"><h5>Requested Width : <strong class="vendorRequestaccept-reqes-width"></strong></h5></div> 
            <div class="col-sm-3" id="vendorRequestdetails-due-date"><h5>Due Date : <strong class="vendorRequestdetails-due-date"></strong></h5></div> 
             <div class="col-sm-3" id="vendorRequestdetails-ext-date"><h5>Req. Ext. Date : <strong class="vendorRequestdetails-ext-date"></strong></h5></div> 
             <div class="col-sm-3" id="vendorRequestdetails-ext-date-reason"><h5>Reason : <strong class="vendorRequestdetails-ext-date-reason"></strong></h5></div> 
             <div class="clearfix"></div>
           
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
            
          <div class="col-sm-6 accept" id="accept">
            <h5>Enter Rate :</h5>
            <input type="text" placeholder="" onkeypress="javascript:return isNumber(event)" class="form-control" id="vendorRequestaccept_new_requested_rate" name="vendorRequestaccept_new_requested_rate"/>
            <span id="vendorRequestaccept_rate-remarks_message"></span>
            </div>
            <div class="col-sm-6 docet" id="docet">
            <h5>Docket No :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestaccept_new_docket" name="vendorRequestaccept_new_docket"/>
            <span id="vendorRequestaccept_docket-remarks_message"></span>
            </div>
            <div class="col-sm-6 prnno" id="prnno">
            <h5>PRN No :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestaccept_new_prn" name="vendorRequestaccept_new_prn"/>
            <span id="vendorRequestaccept_prn-remarks_message"></span>
            </div>
            <div class="col-sm-6 prndate" id="prndate">
            <h5>PRN Date :</h5>
           
            <div class="input-group date">
                     <input type="text" placeholder="dd/mm/yy" class="form-control date_picker" id="vendorRequestaccept_new_prndate" name="vendorRequestaccept_new_prndate"/>
                </div>
            <span id="vendorRequestaccept_prndate-remarks_message"></span>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6 courier" id="courier">
            <h5>Courier Name :</h5>
            <input type="text" placeholder="" class="form-control" id="vendorRequestaccept_new_courier" name="vendorRequestaccept_new_courier"/>
            <span id="vendorRequestaccept_courier-remarks_message"></span>
            </div>
            <div class="col-sm-12">
            <h5>Remarks :</h5>
            <textarea class="form-control" id="vendorRequestaccept_reason" name="vendorRequestaccept_reason"></textarea>
             <span id="vendorRequestaccept_remarks_message"></span>
            </div>

            <div class="clearfix vendorRequestaccept-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorRequestaccept">Accept</a>
            </div>
        <div class="col-sm-6">
            <a href="javascript:void(0)" data-dismiss="modal"  class="pup-btn">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorRequestaccept  Modal End-->


<!--vendorRequestReject Modal Start-->
<div id="vendorRequestreject" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request For Reject</h4>
      </div>
      <div class="modal-body vendorRequestreject-modal-body">
        <div class="row shorting-area pricechange">
        <input type="hidden" name="type" id="type" value="" />
         <input type="hidden" name="action_id" id="action_id" value="" /> 
            <div class="col-sm-3"><h5>PO. No : <strong class="vendorRequestreject-po-no"><!--234--></strong></h5></div>
            <div class="col-sm-3"><h5>Date : <strong class="vendorRequestreject-date"><!--3000--></strong></h5></div>
            <div class="col-sm-3" id="vendorReqreject-cat_1_details"><h5>Design : <strong class="vendorReqreject-cat_1_details"></strong></h5></div> 
            <div class="col-sm-3" id="vendorReqreject-cat_2_details"><h5>Color : <strong class="vendorReqreject-cat_2_details"></strong></h5></div> 
            <div class="col-sm-3" id="vendorReqreject-cat_4_details"><h5>Size/Width : <strong class="vendorReqreject-cat_4_details"></strong></h5></div> 
            <div class="col-sm-3" id="reject-pen-qty"><h5>Pending Qty : <strong class="vendorRequestreject-pen-qty"></strong></h5></div> 

            <div class="col-sm-3" id="reject-can-qty"><h5>Requested Cancel Qty : <strong class="vendorRequestreject-can-qty"></strong></h5></div> 
            <div class="col-sm-3" id="vendorReqreject-cancel-reason"><h5>Reason : <strong class="vendorReqreject-cancel-reason"></strong></h5></div> 
            <div class="col-sm-3" id="reject-old-rate"><h5>Old Rate : <strong class="vendorRequestreject-old-rate"></strong></h5></div> 
            
            <div class="col-sm-3" id="reject-reqst-rate"><h5>Requested Rate : <strong class="vendorRequestreject-request-rate"></strong></h5></div>

            <div class="col-sm-3" id="reject-old-width"><h5>Old Width : <strong class="vendorRequestreject-old-width"></strong></h5></div> 

            <div class="col-sm-3" id="reject-reqes-width"><h5>Requested Width : <strong class="vendorRequestreject-reqes-width"></strong></h5></div> 
            <div class="col-sm-3" id="vendorReqreject-due-date"><h5>Due Date : <strong class="vendorReqreject-due-date"></strong></h5></div> 
             <div class="col-sm-3" id="vendorReqreject-ext-date"><h5>Req. Ext. Date : <strong class="vendorReqreject-ext-date"></strong></h5></div> 
             <div class="col-sm-3" id="vendorReqreject-ext-date-reason"><h5>Reason : <strong class="vendorReqreject-ext-date-reason"></strong></h5></div> 
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
            <div class="col-sm-12">
            <h5>Reason For Reject :</h5>
            <textarea class="form-control" id="vendorRequestreject_reason" name="vendorRequestreject_reason"></textarea>
             <span id="vendorRequestreject_remarks_message"></span>
            </div>

            <div class="clearfix vendorRequestreject-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn submitvendorRequestreject">Submit</a>
            </div>
        <div class="col-sm-6">
            <a href="javascript:void(0)" data-dismiss="modal" class="pup-btn">Cancel
            </a>
            <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--vendorRequestReject  Modal End-->



@include('layouts.footer'); 



<script>
$('.requestDetails').click(function(){
	
		 var unique_key = $(this).attr('unique-key');
		 var action_id = $(this).attr('action_id');
		 var type = $(this).attr('action_type');
		 var price = $(this).attr('old_value');
		 var request_rate = $(this).attr('req_rate');
		 var old_width = $(this).attr('old_width');
		 var request_width = $(this).attr('req_width');
		 var pen_qty = $(this).attr('pen_qty');
		 var can_qty = $(this).attr('can_qty');
		 var cat_1_details = $(this).attr('cat_1_details');
		 var cat_2_details = $(this).attr('cat_2_details');

		 //alert(type);
		 //Fetch All Records
		 var po_no=  $('.po-no'+unique_key).text();
		 var po_date=  $('.po-date'+unique_key).text();
		 var due_date=  $('.due-date'+unique_key).text();
		 var cat_1=  $('.cat-1'+unique_key).text();
		 var cat_2=  $('.cat-2'+unique_key).text();
		 var cat_3=  $('.cat-3'+unique_key).text();
		 var cat_4=  $('.cat-4'+unique_key).text();
		 //var price=  $('.price'+unique_key).text(); 
		 var order_qty=  $('.order-qty'+unique_key).text(); 
		 //var request_rate=  $('.received-qty'+unique_key).text(); 
		 var cancelled_qty=  $('.cancelled-qty'+unique_key).text(); 
		 var git_qty=  $('.git-qty'+unique_key).text();
		 var pending_qty=  $('.pending-qty'+unique_key).text();
		 
		 //Set All Records in Modal		 
		 $('.vendorRequestdetails-cat1').text(cat_1); 
		 $('.vendorRequestdetails-cat2').text(cat_2); 
		 $('.vendorRequestdetails-cat4').text(cat_4); 
		 $('.vendorRequestdetails-po-no').text(po_no);
		 $('.vendorRequestdetails-date').text(po_date);
		 $('.vendorRequestdetails-order-qty').text(order_qty); 
		 $('.vendorRequestdetails-request-rate').text(request_rate); 
		 $('.vendorRequestdetails-git-qty').text(git_qty); 
		 $('.vendorRequestdetails-old-rate').text(price); 
		 $('.vendorRequestdetails-old-width').text(old_width);
		 $('.vendorRequestdetails-reqes-width').text(request_width);
		 $('.vendorRequestdetails-pen-qty').text(pen_qty);
		 $('.vendorRequestdetails-can-qty').text(can_qty);
		 $('.vendorRequestdetails-cat_1_details').text(cat_1_details);
		 $('.vendorRequestdetails-cat_2_details').text(cat_2_details);

		 $('#vendorRequestaccept_new_requested_rate').val(request_rate);
		 $('#old-rate-details').show();
		 $('#reqst-rate-details').show(); 
		 $('#old-width-details').show(); 
		 $('#reqes-width-details').show(); 

		 if((type != 'width_change'))
		 {
		 $('#old-width-details').hide(); 
		 $('#reqes-width-details').hide(); 
		 }

		 if((type != 'width_change') && (type != 'rate_change'))
		 {
		 $('#old-rate-details').hide();
		 $('#reqst-rate-details').hide();   
		}
		

		$('#pen-qty-details').hide(); 
		$('#can-qty-details').hide(); 
		if(type == 'cancel')
		{
		$('#pen-qty-details').show(); 
		$('#can-qty-details').show();
		}
		 
});
 function redirectUrl(id,url)
 {
    readNotification(id);
    setTimeout(function () {
        window.location.href=url;
    }, 1000);
    
 }


 function readNotification(id)
 {        
        $.ajax({
            type: 'GET', 
            url: '{{route("admin-read-notification")}}',
            data: {id:id},
            success:function(data)
            {                                
                if(data==1)
                {
                    $('#read_btn_'+id).remove();

                    var unread=$('#total_unread').text()-1;
                    if(unread!=0)
                      $('#total_unread').text(unread);
                    else
                      $('#total_unread').parent().hide();
                }
            },
            error: function (error) 
            {                
                alert('Error Occured Please Try Again');
            }
        });       
    }



    $('.notificationDetails').click(function(){
         var notification_id=$(this).attr('notification_id');
         $('#notificationDetails').modal('show');
    });

    $('#vendorRequestaccept_new_prndate').on('changeDate', function(ev){
    $(this).datepicker('hide');
});

var ptoken='{{csrf_token()}}';
var cutting_url='{{route("accept-request")}}';
var rej_url='{{route("reject-request")}}';
</script>

<script src="{{asset('js/custom/purchase_order.js')}}"></script>
<script src="{{asset('js/custom/admin_purchase_order.js')}}"></script> 

<script>
    var token='{{csrf_token()}}';
    var q_url='{{route("add-rfq-quotation")}}';
</script>
<script src="{{asset('js/custom/rfq.js')}}"></script>
<script>
    // WRITE THE VALIDATION SCRIPT IN THE HEAD TAG.
    function isNumber(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
            return false;

        return true;
    }    
setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 2000);
</script> 