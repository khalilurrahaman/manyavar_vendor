@include('layouts.header')
<!--------------------------------Start Shorting-------------------------->
<style>
.dt-buttons{margin-right:0px !important;}
.dt-buttons>a {padding-left:18px;}
</style>
<link rel="stylesheet" href="{{asset('autocomplete/jquery-ui.css')}}" />
<script src="{{asset('autocomplete/jquery-ui.js')}}"></script>

<script src="{{asset('dataTable/css/jquery.dataTables.min.css')}}"></script>
<script src="{{asset('dataTable/css/buttons.dataTables.min.css')}}"></script>

   
<script src="{{asset('dataTable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('dataTable/js/dataTables.buttons.min.js')}}"></script>   
<script src="{{asset('dataTable/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('dataTable/js/jszip.min.js')}}"></script>   
<script src="{{asset('dataTable/js/pdfmake.min.js')}}"></script>
<script src="{{asset('dataTable/js/vfs_fonts.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.print.min.js ')}}"></script>
<!--------------------------------End Shorting-------------------------->
<?php 
use App\Http\Controllers\Controller;
if(Controller::userAccessibilityCheck('item-master-list'))
	$item_master_list_exist=1;
if(Controller::userAccessibilityCheck('sample-item-add'))
	$sample_item_add_exist=1;
if(Controller::userAccessibilityCheck('sample-item-edit'))
	$sample_item_edit_exist=1;
if(Controller::userAccessibilityCheck('sample-item-merge'))
	$sample_item_merge_exist=1;
?>
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <div class="row">
                    <div class="col-lg-12">
                    @if(isset($errors))
                        @foreach ($errors->all() as $error)

                                        <div class="btn btn-success" id="hide-me" style="width:100%;background-color: green;border: none;">{{ $error }}</div>
                                         @endforeach
                      @endif
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                	 <div class="col-sm-6"><h3 class="panel-title">
                                    <i class="fa fa-users" aria-hidden="true"></i> Sample Item Listing</h3>
                                    </div>
                                    <div class="col-sm-6">
                                    <div class="shorting-area">
                                  	<?php if(!empty($item_master_list_exist)){?>   	
                                    	<div class="col-sm-3 pull-right">
                                      		<a href="{{route('item-master-list')}}" class="btn btn-success">Item Master</a>
                                      </div>
                                      <?php }?>    
                                      <?php if(!empty($sample_item_add_exist)){?>   
                                      <div class="col-sm-3 pull-right">
                                              <a href="{{route('item-master')}}" data-toggle="modal" class="btn btn-success">
                                              <i class="fa fa-plus" aria-hidden="true"></i> Add New</a>
                                      </div>
                                      <?php }?>    
                                    </div>
                                    <div class="col-sm-6">
                                    
                                    <div class="shorting-area">
                                    <div class="pull-right">
                                            <a href="javascript:void(0)" id="show-btn-main" class="btn btn-success">
                                            <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                            <div class="row shorting-area">
                          
                            <div class="col-sm-12">
                            <div id="mySidenavR-main" class="sidenavR" style="display:none;">
                            <div class="clearfix error_msg_main"></div>
                            <div class="filter-right">
                            <div class="filter-area list-filter">
                            <!--<h5>Vendor List Filter</h5>-->
                                <a href="javascript:void(0)" class="closebtn" id="hide-filter-main">×</a>
                                    <div class="col-sm-2">
                                         <input type="text" class="form-control" placeholder="Search" style="margin-bottom:0;" id="search_exist_main" name="search_exist_main" onkeypress="javascript: if (event.keyCode==13) getOptionFilterMain();">
                                    </div>
                                    <div class="col-sm-1">
                                        <a href="javascript:void(0)" class="btn btn-success" onclick="getOptionFilterMain();" style="width:60px !important;  margin-left: -6px !important;">Search</a>
                                    </div>
                                    <span id="mySidenavR-main-span" style="display:none;">
                                    </span>
                            <div class="clearfix"></div>        
                                </div>
                                <div class="clearfix"></div>
                                </div>    
                            </div>
                            
                            </div>
                            
                            
                            </div>
                            <span id="existItemLIstSample">
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Sr. No</th>
                                                <th class="text-center">Image</th>
                                                <th class="text-center">Width/Size Required</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Category 3</th>
                                                <th class="text-center">Width/Size</th>                                        
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <?php $inc=0; ?>
                                    @foreach($data as $eachData)
                                     <?php 
                                     $inc++;
                                     
                                     $pGalList=DB::table('product_gallery')->select('*')->where('product_id','=',$eachData->id)->get();
                                     #echo "<pre>";print_r($pGalList);
                                     $data_mouseover=URL::to('/img').'/pre-order-img.jpg';
                                     if($eachData->image!='' && file_exists('upload/product/'.$eachData->image))
                                      $data_mouseover=URL::to('/upload/product').'/'.$eachData->image.'#200';
                                     if(!empty($pGalList) && count($pGalList)>0)
                                     {
                                      
                                      $timeoutcount=1;
                                      foreach ($pGalList as $pGal) 
                                      {
                                        $timeout=$timeoutcount*400;
                                        $data_mouseover .=' '.URL::to('/upload/product').'/'.$pGal->product_image.'#'.$timeout.' ';
                                        $timeoutcount++;
                                      }

                                     }
                                     $data_mouseover=rtrim($data_mouseover," ");
                                     ?>
                                    
                                    <tr>
                                     <td class="text-center">{{$inc}}</td>
                                     <td class="text-center">
                                       @if($eachData->image!='')
                                            @if(file_exists('upload/product/'.$eachData->image))
                                             <img src="{{URL::to('/upload/product').'/'.$eachData->image }}" data-mouseover="{{$data_mouseover}}" style="height:60px;width: 60px;"/>
                                            @else
                                              <img src="{{URL::to('/img').'/pre-order-img.jpg' }}" data-mouseover="{{$data_mouseover}}" style="height:60px;width: 60px;"/>
                                            @endif
                                        @else
                                          <img src="{{URL::to('/img').'/pre-order-img.jpg' }}" data-mouseover="{{$data_mouseover}}" style="height:60px;width: 60px;"/>
                                        @endif
                                        </td>
                                        <td class="text-center">{{!empty($eachData->width_size_required)?'Yes':'No'}}</td>
                                        <td class="text-center">{{$eachData->cat1}}</td>
                                        <td class="text-center">{{$eachData->cat2}}</td>
                                        <td class="text-center">{{$eachData->cat3}}</td>
                                        <td class="text-center">{{$eachData->cat4}}</td>
                                       
                                        <td class="text-center">                                        
                                        <?php if(!empty($sample_item_edit_exist)){?> 
                                        <a href="{{route('item-master-edit',$eachData->id)}}"  class="btn btn-success">Edit</a>
                                        <?php }?>    
                                        <?php if(!empty($sample_item_merge_exist)){?> 
                                        <a class="btn btn-success mergeData" data-target="#selectpro" data-toggle="modal" merge-with="{{$eachData->id}}">Merge</a>
                                        <?php }?></td>
                                    </tr>
                                    @endforeach                            
                                </tbody>
                               </table>
                              </div>
                              
                              </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
<!--/////////////////////////////////////////////////////////select product Modal Start//////////////////////////////////////////////////////////////-->

<!--select product Modal Start-->
<div id="selectpro" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close hide-filter" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Select Product</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
        <div class="pull-right">
                <a href="javascript:void(0)" id="show-btn" class="btn btn-success" style="font-size:12px; margin-bottom:12px;" onclick="getOptionFilter();">
                <i class="fa fa-filter" aria-hidden="true"></i>Search</a>
        </div>
        <div class="col-sm-5">
        <div class="inner-addon right-addon">
        <i class="glyphicon glyphicon-search"></i>
        <input type="text" class="form-control" placeholder="Search By Design" style="margin-bottom:0;" id="search_exist" name="search_exist" onkeypress="javascript: if (event.keyCode==13) getOptionFilter();">
        </div>
      	</div>
        <div class="clearfix error_msg"></div>
        <div id="mySidenavR" class="sidenavR" style="display: none;">
    
		</div>
            <div class="panel-body pre-order custome-table">
            
            <div class="table-responsive ">
            <table class="table table-fixed table-striped table-bordered">
          <thead>
            <tr>
                <th class="text-center col-xs-2">Image</th>
                <th class="text-center col-xs-2">Design</th>
                <th class="text-center col-xs-2">Colour</th>
                <th class="text-center col-xs-2">Cat 3</th>
                <th class="text-center col-xs-2">Width/Size</th>
                <th class="text-center col-xs-2">Action</th>
            </tr>
          </thead>          
          <tbody id="existItemLIstModal">
         <!--  @if(!empty($live_product) && count($live_product)>0)
          @foreach($live_product as $ext)
          <div class="clearfix"></div>
            <tr>
            <div class="clearfix"></div>
              <td class="col-xs-2 text-center">
              <?php
                                            $e_image=URL::to('/')."/img/pre-order-img.jpg";
                                            if(!empty($ext->image) && file_exists(public_path().'/upload/product/'.$ext->image))
                                            {
                                               $e_image= URL::to('/') .'/upload/product/'.$ext->image;
                                            }
                                        ?>
                                        <img src="<?php echo $e_image; ?>" title="small image" width="56" height="56" class="selectProductExistClassImage{{$ext->id}}">
              </td>
              <td class="text-center col-xs-2 selectProductExistClassCat1{{$ext->id}}">{{$ext->cat1}}</td>
                                        <td class="text-center col-xs-2 selectProductExistClassCat2{{$ext->id}}">{{$ext->cat2}}</td>
                                        <td class="text-center col-xs-2 selectProductExistClassCat3{{$ext->id}}">{{$ext->cat3}}</td>
                                        <td class="text-center col-xs-2 selectProductExistClassCat4{{$ext->id}}">{{$ext->cat4}}</td>
              <td class="col-xs-2 text-center">
              <a href="javascript:void(0);" product_id="{{$ext->id}}" title-name="{{$ext->cat1}}-{{$ext->cat2}}-{{$ext->cat3}}-{{$ext->cat4}}" class="btn btn-success selectProductExist" 
              style="width:56px; padding:3px 0; font-size:12px;">Select</a>
              </td>
              <div class="clearfix"></div>
            </tr>
             <div class="clearfix"></div>
            @endforeach
                                    @else
                                    <div class="clearfix"></div>
                                     <tr>
                                    	<td class="text-center" colspan="6">Product Not Available!</td>
                                    </tr>
                                    <div class="clearfix"></div>
                                    @endif -->
                                    <tr><td class="text-center" colspan="6" style="border:none;">Search Product!</td></tr>
          <div class="clearfix"></div>  
          </tbody>
        </table>
        </div>
            <div class="clearfix"></div>
                                    
                                </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<input type="hidden" id="merge_with" value="" />
 <!-- /#Hover Image -->
 <script type="text/javascript">
  var cat1_list = <?php echo $cat1_list;?>;
$(document).ready(
  function () {
    $( "#search_exist_main" ).autocomplete({
      source:cat1_list,
      autoFocus: true ,
      //change: function (event, ui) { alert(ui.item.value); }
    });
  }

);
</script>
    <script type="text/javascript">$('img').on('mouseover', function() {
    var self = this,
        i = 0,
        images = $(this).data('mouseover').split(/\s+/);
    
    (function nextImage() {
        var next = images[i++ % images.length].split('#');
        $(self).data('timeout', setTimeout(function() {
            self.src = next[0];
            nextImage();
        }, next[1]));
    })();
    
}).on('mouseout', function() {
    clearTimeout($(this).data('timeout'));
    this.src = $(this).attr('src');
});
</script>
<!--select product  Modal End-->
<script>
$(document).ready(function () {
            
            $('#example').dataTable({
                'order': [[ 0, "asc" ]],//Default Column Attribute
                'pageLength': 10,//Per Page Item
                 "searching": false,
                'dom': 'Bfrtip',//Header Info               
                'buttons': [ {extend: 'excel',exportOptions: {columns: [0,2,3,4,5,6]},footer: false}],
                'aoColumnDefs': [          
                                   { aTargets: [ 0 ], bSortable: true },
                                   { aTargets: [ 1 ], bSortable: false },
                                   { aTargets: [ 2 ], bSortable: true },
                                   { aTargets: [ 3 ], bSortable: false },
                                   { aTargets: [ 4 ], bSortable: false },
                                   { aTargets: [ 5 ], bSortable: false }
                                ],
                "oLanguage": {
                                "oPaginate": {
                                                "sNext": '<i class="fa fa-chevron-right" ></i>',
                                                "sPrevious": '<i class="fa fa-chevron-left" ></i>'
                                            }
                            }
            });
        });
$(document).ready(function(){
    $("#hide-filter-main").click(function(){
        $("#mySidenavR-main").hide();
        $('#search_exist').val('');
    
        $('#existItemLIstModal').html('<tr><td class="text-center" colspan="6" style="border:none;">Search Product!</td></tr>');
		    //filterSample();
    });
    $("#show-btn-main").click(function(){
        $("#mySidenavR-main").show();
    });
});
</script>
<script type="text/javascript">
    function ConfirmDelete(id)
        {
            var x = confirm("Are you sure you want to Delete?");
            if (x)
                return true;
            else
                return false;
        }
		

function getOptionFilter() {
 var search_str=$('#search_exist').val();
 $('.error_msg').text('');
 if(search_str=='')
 {
	 $('.error_msg').text('Please Enter Search Keyword!').css('color', 'red').show();
	 return false;
 }
 //$('.loading').show();
 
 $('#mySidenavR').show();
 var token='{{csrf_token()}}';
 		$.ajax({
			type: 'GET', 
			url: '{{route("get-option-filter-live")}}',
			data: {search_str:search_str},
			success:function(data)
			{
				
        		$('#mySidenavR').html(data);
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});
}

function getOptionFilterMain() {
 var search_str=$('#search_exist_main').val();
 $('.error_msg_main').text('');
 if(search_str=='')
 {
	 $('.error_msg_main').text('Please Enter Search Keyword!').css('color', 'red').show();
	 return false;
 }
 $('.loading').show();
 
 $('#mySidenavR-main-span').show();
 		$.ajax({
			type: 'GET', 
			url: '{{route("get-option-filter-sample")}}',
			data: {search_str:search_str},
			success:function(data)
			{
				$('.loading').hide();
        		$('#mySidenavR-main-span').html(data);
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});
}

$('.selectSearchSample').click(function(){
	   var selectcat1 = $('#search_exist_main').val();
		 var selectcat2 = $('#selectcat2').val();
		 var selectcat3 = $('#selectcat3').val();
		 var selectcat4 = $('#selectcat4').val();
		filterSample(selectcat1,selectcat2,selectcat3,selectcat4) ;
});
function filterSample(selectcat1='',selectcat2='',selectcat3='',selectcat4=''){
	   $('.loading').show();
 		$.ajax({
			type: 'GET', 
			url: '{{route("search-item-by-keyword-sample")}}',
			data: {selectcat1:selectcat1,selectcat2:selectcat2,selectcat3:selectcat3,selectcat4:selectcat4},
			success:function(data)
			{
        		$('#existItemLIstSample').html(data);
				$('.loading').hide();
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});
}


$( "#hide-filter" ).click(function() {
  
	$('#mySidenavR').hide();
	$('#merge_with').val('');
  $('#search_exist').val('');
    
  $('#existItemLIstModal').html('<tr><td class="text-center" colspan="6" style="border:none;">Search Product!</td></tr>');
  //filter();
});

$( ".hide-filter" ).click(function() {
  
  $('#mySidenavR').hide();
  $('#merge_with').val('');
  $('#search_exist').val('');
    
  $('#existItemLIstModal').html('<tr><td class="text-center" colspan="6" style="border:none;">Search Product!</td></tr>');
  //filter();
});

function filter(selectcat1='',selectcat2='',selectcat3='',selectcat4=''){
		$('.loading').show(); 
 		$.ajax({
			type: 'GET', 
			url: '{{route("search-item-by-keyword-live")}}',
			data: {selectcat1:selectcat1,selectcat2:selectcat2,selectcat3:selectcat3,selectcat4:selectcat4},
			success:function(data)
			{
        		$('#existItemLIstModal').html(data);
				$('.loading').hide();
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});
}

$(document).on('click', '.selectProductExist', function(e) {
	
	var title = $(this).attr('title-name');
	if(confirm("Are you sure you want to Merge:"+title+"?"))
	{
	 var product_id = $(this).attr('product_id');
	 var merge_with= $('#merge_with').val();
     $('.loading').show(); 
 		$.ajax({
			type: 'GET', 
			url: '{{route("merge-item-master")}}',
			data: {product_id:product_id,merge_with:merge_with},
			success:function(data)
			{
				if(data!=1)
				{					
        			alert('Please Try Again!');
				}
				$("#selectpro").modal('hide');
				$('.loading').hide();
				location.reload(); 
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});
		
		
	}
	 
});

$(document).on('click', '.mergeData', function(e) {
	 var merge_with = $(this).attr('merge-with');
	 $('#merge_with').val(merge_with);
    
});

setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 2000);

</script>
@include('layouts.footer')