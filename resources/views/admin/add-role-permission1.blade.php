@include('layouts.header')
<link href="{{ URL::asset('css/selectbox/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ URL::asset('css/selectbox/multi-select.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ URL::asset('css/selectbox/select2.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ URL::asset('css/selectbox/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <!--add user popup Modal Start-->

<div class=" quotation-pop" role="dialog">
    <div class="modal-dialog">


    @if(!empty($errors->all()) )

                    <div class="btn btn-success" id="hide-me" style="width:100%;background-color: green;border: none;">{{ $errors->all()[0] }}</div>
                    @endif
        <!-- Modal content-->
        <div class="modal-content  none-shadow">
        
          <div class="modal-header">
            <h4 class="modal-title">Add Permission</h4>
          </div>


            <div class="modal-body">
                <form id="user-form" action="{{route('insert-add-permission')}}" method="post">
                {{ csrf_field() }}
                  <input type="hidden" name="role_id" value="{{$role_user->id}}">
                  
                    <div class="row shorting-area pricechange">
                        <div class="col-sm-12">
                            <h5>Role : {{$role_user->name}}</h5>

                            
                        </div>
                        <div class="col-sm-12">
                            <select multiple="multiple" class="multi-select" id="my_multi_select2" name="permission[]">
                                @foreach ($allPermission as $moduleName => $allModulePermission)
                                  <optgroup label="{{ $moduleName }}">
                                    @foreach ($allModulePermission as $eachPermission)
                                      @if(in_array($eachPermission->slug,$assigned_permissions))
                                        <option selected value="{{$eachPermission->id }}">{{ $eachPermission->name }}</option>
                                      @else
                                        <option value="{{$eachPermission->id }}">{{ $eachPermission->name }}</option>
                                      @endif
                                    @endforeach
                                  </optgroup>
                                @endforeach
                            </select>
                                            
                        </div>
                        <div class="col-sm-6">
                        <input type="submit" name="submit" value="Submit" class="pup-btn">
                        </div>
                        <div class="col-sm-6">
                        <a href="{{route('role-listing')}}" class="pup-btn">Cancel
                        </a>
                        <d
                        
                    </div>
                </form>
           </div>
        </div>
    </div>
</div>
<!--add user popup  Modal End-->
@include('layouts.footer')

<script type="text/javascript">
    $(document).ready(function () {
        $('#user-form').validate({
            rules: {
                first_name: {
                    required: true,
                },
                last_name: {
                    required: true,
                },
                user_name: {
                    required: true,
                },
            }
        });

        $('#first_name').bind('keyup keydown blur', function(e) {        
       if (e.which === 32 && !this.value.length)
            e.preventDefault();
       $(this).val(function(i, val) {        
           return val.replace(/[^a-zA-Z\s]/gi,''); 
       });       
    });
    })
</script>
<script src="{{ URL::asset('js/selectbox/components-multi-select.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/selectbox/bootstrap-select.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/selectbox/jquery.multi-select.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/selectbox/select2.full.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
   setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 2000);
</script>