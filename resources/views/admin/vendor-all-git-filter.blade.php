<?php 
use App\Http\Controllers\Controller;
if(Controller::userAccessibilityCheck('git-comments'))
	$git_comments_exist=1;
if(Controller::userAccessibilityCheck('mark-complete'))
	$mark_complete_exist=1;
?>
<div class="panel-body custome-table">
                            
                                <div class="table-responsive view-table">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">SL No</th>
                                                <th class="text-center">GIT No</th>
                                                <th class="text-center">Vendor Name</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Date</th>
                                                <th class="text-center">GIT Qty</th>
                                                <th class="text-center">Receive Qty</th>
                                                
                                                <th class="text-center">Adhoc Qty</th>
                                                <th class="text-center">Pending GIT Qty</th>
                                                <!-- <th class="text-center">GRT</th>-->
                                                <th class="text-center">Last Comments</th><!--
                                                <th class="text-center">Remarks</th>-->
                                                 <th class="text-center">Action</th>
                                                <!--<th class="text-center width-last">Action</th>-->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(!empty($gitList) && count($gitList)>0)
                                            <?php $old_val='';$inc=0;?>
                                            @foreach($gitList as $eachGit)
                                            <?php

                                           
                                            $new_val=$eachGit->git_no; 
                                            // $prev_git_qty=$eachGit->prev_git_qty;
                                            // $prev_gitqty=$prev_git_qty[0]->pgit_qty;

                                            $pndg_qty=$eachGit->git_qty - $eachGit->git_rcv;
                                            $pndg_qty_calc=$pndg_qty;
                                            $adhoc_qty=0;
                                            $inadequate_qty=0;
                                            if($pndg_qty<=0)
                                            {
                                                $adhoc_qty=str_replace("-","",$pndg_qty);
                                                $pndg_qty=0;
                                            }
                                            $grc_qnty=0;
                                            if($eachGit->git_qty!=$grc_qnty)
                                            {
                                                $inadequate_qty=$eachGit->git_qty-$grc_qnty;
                                            }
                                            ?>
                                            <tr>
                                                
                                                <?php if($new_val!=$old_val){
                                            	$inc++;
												?>
                                                <td class="text-center" rowspan="{{$eachGit->rowspan}}">{{$inc}}</td>
                                                <td class="text-center" rowspan="{{$eachGit->rowspan}}">{{$eachGit->git_no}}</td>
                                                
                                                <td class="text-center" rowspan="{{$eachGit->rowspan}}">{{$eachGit->name}}</td>
                                                <?php }?>
                                                <td class="text-center">{{$eachGit->cat1}}</td>
                                                <td class="text-center">{{$eachGit->cat2}}</td>
                                                <td class="text-center">{{$eachGit->cat3}}</td>
                                                <td class="text-center">{{date('d/m/Y',strtotime($eachGit->git_date))}}</td>
                                               
                                                <td class="text-center">{{$eachGit->git_qty}}</td>
                                                <td class="text-center">{{$eachGit->git_rcv}}</td>
                                                
                                                <td class="text-center">{{$adhoc_qty}}</td>
                                                <td class="text-center">{{$pndg_qty}}</td>
                                                <!--<td class="text-center">2</td>-->
                                                <td class="text-center">
                                                 <?php if(!empty($git_comments_exist)){?>
                                        @if(!empty($eachGit->git_comments) && count(($eachGit->git_comments))>0)
                                        <?php                                       
                                       
                                        if(strlen($eachGit->git_comments[0]->comments)<=20)                                 
                                           echo ($eachGit->git_comments[0]->comments);
                                        else
                                         echo substr($eachGit->git_comments[0]->comments,0,18).'..';  

                                        ?>
                                        <a href="{{route('admin-git-comments', $eachGit->id)}}" class="pull-right btn btn-warning btn-xs">
                                        <?php echo count($eachGit->git_comments)?>
                                        Comment(s)</a>
                                        @else
                                        <a href="{{route('admin-git-comments', $eachGit->id)}}" class="btn btn-warning btn-xs pull-right">
                                        
                                        Comments</a>
                                        
                                         @endif
                                          <?php }?>
                                        </td>
                                         <?php if($new_val!=$old_val){?>
                                         <td class="text-center" rowspan="{{$eachGit->rowspan}}">
                                        
                                            <?php
                                            $admin_remarks= 'Admin:'.$eachGit->admin_cn_resolve_remarks;                                           
                                            $vendor_remarks= 'Vendor:'.$eachGit->vendor_cn_resolve_remarks;
                                            if($admin_remarks !='Admin:' || $vendor_remarks!='Vendor:'){
                                            ?>
                                           <span data-toggle="modal" data-target="#ViewRemarks">
                                            <a href="javascript:void(0);" class="btn btn-success btn-xs ViewRemarksClass"  admin_remarks="{{$admin_remarks}}" vendor_remarks="{{$vendor_remarks}}" git_no="{{$eachGit->git_no}}">View CN Remarks</a>
                                           </span>
                                            <?php
                                            }?>
                                            
                                            <?php
											$sb_admin_remarks= 'Admin:'.$eachGit->admin_sb_resolve_remarks;											
											$sb_vendor_remarks= 'Vendor:'.$eachGit->vendor_sb_resolve_remarks;
											if($sb_admin_remarks !='Admin:' || $sb_vendor_remarks!='Vendor:'){
											?>
                                           <span data-toggle="modal" data-target="#ViewRemarks">
                                        	<a href="javascript:void(0);" class="btn btn-success btn-xs ViewRemarksClass"  admin_remarks="{{$sb_admin_remarks}}" vendor_remarks="{{$sb_vendor_remarks}}" git_no="{{$eachGit->git_no}}">View SB Remarks</a>
                                           </span>
                                           <br />
                                            <?php
											}?>
                                            
                                            <?php
                                            if(!empty($eachGit->cn_doc ) && file_exists(public_path().'/upload/gitdoc/'. $eachGit->cn_doc))
                                            {
                                               $cn_doc= URL::to('/') .'/upload/gitdoc/'. $eachGit->cn_doc;
                                               echo '<span data-toggle="modal" data-target="#ResolvedGit"><a cn_doc_attc="'.$cn_doc.'" class="btn btn-success btn-xs ResolvedGitClass" git_no="'.$eachGit->git_no.'">View CN Attachment</a></span>';
                                            }
											if(!empty($eachGit->sb_doc ) && file_exists(public_path().'/upload/gitdoc/'. $eachGit->sb_doc))
                                            {
                                               $sb_doc= URL::to('/') .'/upload/gitdoc/'. $eachGit->sb_doc;
                                               echo '<a href="'.$sb_doc.'" target="_blank" class="btn btn-success btn-xs">View SB Attachment</a>';
                                            }
                                            ?> 
                                            
                                            
                                            
                                            
                                            <?php //For attachment Section
                                            if($eachGit->is_sbc>0)
                                            {
                                                $text='Request for SB';                                                
                                                $target='#ResolveGit';
                                                $class='warning';
                                                if($eachGit->admin_is_resolve==1)
                                                {
                                                    $target='javascript:void(0);';
                                                    $class='success';
													$text='Requested SB';  
                                                }
												if($eachGit->vendor_is_resolve==1)
                                                {
													$text='SB Received';  
                                                }
                                            ?> 
                                         <br />
                                        <span data-toggle="modal" data-target="{{$target}}">
                                            <a href="javascript:void(0);" class="btn btn-{{$class}} btn-xs ResolveGitClass"  git_no="{{$eachGit->git_no}}">{{$text}}</a>
                                        </span> 
                                       
                                        
                                         <?php }?>
                                            <?php if(!empty($mark_complete_exist)){?>
                                      
                                                <a href="{{route('mark-complete',$eachGit->id)}}" class="btn btn-success" onclick="return completeConfirm('{{$eachGit->git_no}}')">MARK COMPLETE</a>                           
                                            <?php }?>
                                            </td>
                                            <?php }?>
                                            </tr>
                                             <?php $old_val=$new_val;?>
                                            @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row">
                                  <div class="col-sm-6">
                                    <div class="dataTables_info" role="status" aria-live="polite">Showing {{$gitList->count()}} of {{$gitList->total()}} entries</div>
                                  </div>
                                  <div class="col-sm-6">
                                        <nav aria-label="Page navigation" class="pull-right">
                                            {{ $gitList->links() }}
                                        </nav>
                                    </div>
                                </div>
                            </div>
                            
<script>
$('.ResolveGitClass').click(function(){
 //Fetch All Records
 var request_type=$(this).text();
 var git_no=$(this).attr('git_no');
 //Set All Records in Modal
 $('#request_type').val(request_type);
 $('#git_no').text(git_no); 
 
});
$('.ViewRemarksClass').click(function(){
 //Fetch All Records
 var admin_remarks=$(this).attr('admin_remarks');
 var vendor_remarks=$(this).attr('vendor_remarks');
 var git_no=$(this).attr('git_no');
 //Set All Records in Modal
 $('#git_no_r').text(git_no); 
 if(admin_remarks!='Admin:')
 	$('#admin_remarks_r').text(admin_remarks); 
 if(vendor_remarks!='Vendor:')
 	$('#vendor_remarks_r').text(vendor_remarks); 
});

$('.ResolvedGitClass').click(function(){
 //Fetch All Records
 var git_no=$(this).attr('git_no');
 var type=$(this).text();
 var cn_doc_attc=$(this).attr('cn_doc_attc');
 //Set All Records in Modal
 $('#git_no99').text(git_no); 
 //Appent Table
	$.ajax({
			type: 'GET', 
			url: '{{route("fetch-git-records")}}',
			data: {git_no:git_no,type:type},
			success:function(data)
			{
				console.log(data);
				if(data.cnList.length > 0)
				{  
					
					var table_html = '<table id="example2" class="table table-striped table-bordered" cellspacing="0" width="100%">';
					table_html += '<thead><tr><th class="text-center">Item</th><th class="text-center">Qty</th><th class="text-center">Rate</th><th class="text-center">Gross Amt</th><th class="text-center">Charge Type</th><th class="text-center">Charge Apply</th><th class="text-center">Net Amount</th></tr></thead>';
						table_html += '<tbody>';
						 
							for(var i=0;i<data.cnList.length;i++)
							{
								var item_name=data.cnList[i]['ITEM_NAME'];
								var cn_qty=data.cnList[i]['cn_qty'];
								var rate=data.cnList[i]['RATE'];
								var gross_amnt=cn_qty*rate;
								var disc_rate=0;
				
								var tax_type='NA';
								var tax=data.cnList[i]['disc_rate'];
								var tax_type=data.cnList[i]['CHGNAME'];
								if(tax_type=='Discount')
									var net_amnt=gross_amnt-(tax*gross_amnt/100);
								else
									var net_amnt=gross_amnt+(tax*gross_amnt/100);
								table_html += '<tr>';
									table_html += '<td class="text-center">'+item_name+'</td>';
									table_html += '<td class="text-center">'+cn_qty+'</td>';
									table_html += '<td class="text-center">'+rate+'</td>';
									table_html += '<td class="text-center">'+gross_amnt+'</td>';
									table_html += '<td class="text-center">'+tax_type+'</td>';
									table_html += '<td class="text-center">'+tax+'%</td>';
									table_html += '<td class="text-center">'+net_amnt+'</td>';
								table_html += '</tr>';
							}
						table_html += '</tbody>';
				  table_html += '</table>';
				  if(cn_doc_attc!='')
				  {
				  $('.previewing99').html('<a target="_blank" href="'+cn_doc_attc+'">Attachment</a>');
				  }
			  	  $('.itemDetailsTable99').html(table_html);
				}
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});
});

</script>
<!-- <script>
        $(document).ready(function () {
            
            $('#example').dataTable({
                'order': [[ 0, "asc" ]],//Default Column Attribute
                'pageLength': 500,//Per Page Item
                //'sDom': 't',//Hide All Info From top and bottom
                'aTargets': [-1], //1st one, start by the right/
                "paging":   false,
                "info":     false,
                "searching": false,
                'dom': 'Bfrtip',
                'buttons': ['excel']//['copy', 'csv', 'excel', 'pdf', 'print']
            });
        });
        </script> -->