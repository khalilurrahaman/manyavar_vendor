<?php 
use App\Http\Controllers\Controller;
if(Controller::userAccessibilityCheck('document-list-delete'))
  $document_list_delete_exist=1;
?>
<div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Vendor Name</th>
                                                <th class="text-center">Document Type</th>
                                                <th class="text-center">Month</th>
                                                <th class="text-center">Remarks</th>
                                                <th class="text-center">Date & Time</th>
                                                <th class="text-center">File</th>
                                                @if($status==1)
                                                <th class="text-center">Action</th>
                                                @endif
                                                
                                            </tr>
                                        </thead>
                                    <tbody>
                                    @foreach($document_list as $data)
                                    <tr>
                                        <td class="text-center">@if(!empty($data->getDocVendor->name)){{$data->getDocVendor->name}}@endif</td>
                                        <td class="text-center">@if(!empty($data->getDocType->type_name)){{$data->getDocType->type_name}}@endif</td>
                                        <td class="text-center">{{$data->month}}</td>
                                        <td class="text-center">{{$data->remarks}}</td>
                                        <td class="text-center">{{date('d/m/Y g:i A',strtotime($data->created_at))}}</td>
                                        <td class="text-center">
                                         <?php 
										$document_list_view = DB::table('document_files')->select('*')->where('document_id','=',$data->id)->orderBy('id','asc')										->get();								  $inc=0;
										if(!empty($document_list_view)){
											foreach($document_list_view as $docs){
										if($docs->document_file!='')
										{
										   if(file_exists('Document_upload/'.$docs->document_file))
										   { 
										    $inc++;
											$document_file=URL::to('/Document_upload').'/'.$docs->document_file;
											echo '<a href="'.$document_file.'" target="_blank" >Document'.$inc.'</a>'.'<br>';
										   }
										}
										
                                        }}?>
                                        </td>
                                        @if($status==1)
                                        <td class="text-center">
                                        <?php if(!empty($document_list_delete_exist)){?> 
                                      @if($data->status==1)
                                        <a href="{{route('docs-status-deactive',$data->id)}}" onclick="return ConfirmDeactive()" class="btn btn-danger">Delete</a>
                                      @endif
                                      <?php }?>
                                        </td>
                                        @endif
                                    </tr>
                                    @endforeach
                                      
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                              <div class="col-sm-6">
                                <div class="dataTables_info" role="status" aria-live="polite">Showing {{$document_list->count()}} of {{$document_list->total()}} entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                        {{ $document_list->links() }}
                                    </nav>
                                  </div>
                              </div>