@include('layouts.header')
<!---Start Shorting-->

<!--End Shorting-->
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <!--<div class="row">
                    <div class="col-lg-12 title-top">
                    <div class="row page-header">
                        <div class="col-sm-6">
                            <h1>Admin <small>All GRT List</small></h1>
                        </div>
                        <div class="col-sm-6">
                            <a href="vendor-add-git.php" class="btn btn-success">Add GIT</a>
                        </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i> All GRC List</h3>
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                                <div class="table-responsive view-table">





                                   


                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                               <th class="text-center col-sm-2">Sl No.</th>
                                                <th class="text-center col-sm-3">Vendor Name</th>
                                                <th class="text-center col-sm-1">GRC No</th>
                                                <th class="text-center col-sm-1">GRC Date</th>
                                                <th class="text-center col-sm-1">Receive Qty</th>
                                                <th class="text-center col-sm-1">GIT ID</th>
                                                <th class="text-center col-sm-1">GIT Date</th>
                                                <th class="text-center col-sm-3">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <?php $i=0;?>
                                    @foreach($grt_list as $row)

                                       <?php $i++;?>
                                        <tr>
                                            <td class="text-center col-sm-2">{{$i}}</td>
                                            <td class="text-center col-sm-3">{{$row->name}}</td>
                                            <td class="text-center col-sm-1">GRC/{{$row->GRTNO}}</td>
                                            <td class="text-center col-sm-1">{{date('d/m/Y',strtotime($row->GRTDT))}}</td>
                                            <td class="text-center col-sm-1">{{$row->RTINVQTY}}</td>
                                            <td class="text-center col-sm-1">{{$row->git_no}}</td>
                                            <td class="text-center col-sm-1">{{date('d/m/Y',strtotime($row->git_date))}}</td>
                                            <td class="text-center col-sm-3">
                                            <span data-toggle="modal" data-target="#ResolveGRC">
                                            <a href="javascript:void(0)" class="btn btn-success ResolveGrcClass" grc_no="{{$row->GRTNO}}" grc_id="{{$row->id}}" git_id="{{$row->git_no}}">EDIT / ATTACH GIT</a>
                                            </span>
                                             </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                               </table>
                              </div>
                              
                            <div class="row">
                                  <div class="col-sm-6">
                                    <div class="dataTables_info" role="status" aria-live="polite">Showing {{$grt_list->count()}} of {{$grt_list->total()}} entries</div>
                                  </div>
                                  <div class="col-sm-6">
                                        <nav aria-label="Page navigation" class="pull-right">
                                            {{ $grt_list->links() }}
                                        </nav>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@include('layouts.footer')
<!--ResolveGit Modal Start-->
<form method="post" action="{{route('resolve-grc-records-admin')}}" enctype="multipart/form-data" id="form_resolve">
 {{ csrf_field() }}
<div id="ResolveGRC" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Resolve GRC:<strong id="grc_no"></strong></h4>
      </div>
      <br />
      <div class="itemDetailsTable" style="width:90%; padding-left:10%">
       <!--Append table Here-->
       </div>
       <input type="hidden" id="grc_id" name="grc_id"  value=""/>
      <div class="modal-body resolveGit-modal-body">
        <div class="row shorting-area pricechange">
            <div class="col-sm-4">
                        <img src="{{asset('img/doc.png')}}" id="previewing" alt="" width="80%">
                        <div class="clearfix">
                         <span  id="item_message"  class="commonMessage"></span>   
                    </div> <br/>
                    <div class="clearfix"></div>
                    <input type="file" id="file" name="file" value=""/>
                    </div>
                  
            <div class="col-sm-8">
            <h5>GIT ID:</h5>
            <input type="text" id="git_id" name="git_id" class="form-control">
             <span  id="git_id_message"  class="commonMessage"></span>  
            </div>

            <div class="clearfix resolveGit-clearfix"></div>
            <div class="col-sm-6">
            <a href="javascript:void(0)" class="pup-btn" onclick="resolveGrc()">Submit</a>
            </div>
            <div class="col-sm-6">
             <a href="javascript:void(0);" class="pup-btn" data-dismiss="modal">Cancel</a>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
</form>
<!--ResolveGit  Modal End-->
<script>
$('.ResolveGrcClass').click(function(){
 //Fetch All Records
 var grc_no=$(this).attr('grc_no');
 var grc_id=$(this).attr('grc_id');
 var git_id=$(this).attr('git_id');

 
 //Set All Records in Modal 
 $('#grc_no').text('GRC/'+grc_no);
 $('#grc_id').val(grc_id);
 $('#git_id').val(git_id);

 
});


    function resolveGrc(){
        var urls = "{{route('resolve-grc-records-admin')}}";
        var item_image = $("#file").val();
        var git_id = $("#git_id").val();
        var imagefile = item_image.split('.').pop();
        var imgArray= ["jpeg","png","jpg","JPEG","PNG","JPG","PDF","pdf","DOC","doc","DOCX","docx"];
        
        var ret_val=true;
        $(".commonMessage").text('');//Remove previous all message
        if($.inArray(imagefile,imgArray)== -1){
            $("#item_message").text('Please Select a File!').css('color', 'red').show();
            ret_val=false;
        }
        else
        {
             $("#item_message").hide();
        }
         if(git_id=='')
         {
             $("#git_id_message").text('Please enter GIT ID!').css('color', 'red');
             ret_val=false;
         }
         else
        {
             $("#git_id_message").hide();
        }
        
         if(ret_val==false)
         {
             return false;
         }
     
        $.ajax({
            url: urls,
            type: 'POST',
            data:new FormData($("#form_resolve")[0]),
            processData: false,
            contentType: false,
            asynce: false,
            cache: false,
            success: function(data)
            {
               if(data==1)
                {
                    $('.resolveGit-modal-body').html('Attachment sent sucessfully!');
                     window.location.reload();
                }
                else
                    $('.resolveGit-clearfix').text('Please Try again!').css('color', 'red').show();
     
            },error: function(data){
                alert(data.error);
            }
            

        });
    };
    
</script>
<script>
    $(document).ready(function () {
      
            $('#example').dataTable({
        'order': [[ 0, "asc" ]],//Default Column Attribute
        'pageLength': 500,//Per Page Item
        //'sDom': 't',//Hide All Info From top and bottom
        'aTargets': [-1], //1st one, start by the right/
        "paging":   false,
            "info":     false,
        "searching": false,
        'dom': 'Bfrtip',
        'buttons': ['excel']//['copy', 'csv', 'excel', 'pdf', 'print']
            });
        });
    </script>