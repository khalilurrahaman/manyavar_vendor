@include('layouts.header')
    <!--add user popup Modal Start-->
<div class=" quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content none-shadow">
      <div class="modal-header">
        <h4 class="modal-title">Edit Section</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
        <form action="{{route('vendor-sec-ed')}}" method="post">
        {{ csrf_field() }}
        @if(isset($errors))
                            @foreach ($errors->all() as $error)
                              <div style="padding-left: 145px;" class="alert alert-success alert-dismissable">{{ $error }}</div>
                            @endforeach
                    @endif
            <div class="col-sm-12">
                <h5>Name :</h5>
                <input type="hidden" class="form-control" name="id" id="deptId" value="{{$section->id}}" >
                <input type="text" class="form-control" name="name" id="editName" value="{{$section->name}}" >
                <span class=" error class_err" id="name_msg_edit" ></span>
            </div>
            
            
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <h5>Description :</h5>
            <textarea class="form-control" name="description" id="editDescription" >{{$section->description}}</textarea>
            <span class=" error class_err" id="description_msg_edit" ></span>
            </div>
            
            <div class="clearfix"></div>
            <div class="col-sm-6">
            <button class="pup-btn" onclick="return validateFormEdit();">Edit Section</button>
            </div>
            <div class="col-sm-6">
               <a href="{{route('section-listing')}}" data-dismiss="modal" class="pup-btn">Cancel
                </a>
            </div>
        </form>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
  </div>
<!--add user popup  Modal End-->
@include('layouts.footer')

  <script type="text/javascript">
  function validateFormEdit()
  {    
    var ret_val=true;   
   
    var name=$("#editName").val();
    if(name==''){$("#name_msg_edit").text('Please Enter Name  !').show();ret_val=false;}
    else{$("#name_msg_edit").hide();}

    var department_description=$("#editDescription").val();
    if(department_description==''){$("#description_msg_edit").text('Please Enter Description  !').show();ret_val=false;}
    else{$("#description_msg_edit").hide();}


    return ret_val;

  }
  </script>