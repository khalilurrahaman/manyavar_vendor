@include('layouts.header')
<!--Start Shorting-->
<style>
.dt-buttons{margin-right:0px !important;}
.dt-buttons>a {padding-left:18px;}
</style>
<script src="{{asset('dataTable/css/jquery.dataTables.min.css')}}"></script>
<script src="{{asset('dataTable/css/buttons.dataTables.min.css')}}"></script>

   
<script src="{{asset('dataTable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('dataTable/js/dataTables.buttons.min.js')}}"></script>   
<script src="{{asset('dataTable/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('dataTable/js/jszip.min.js')}}"></script>   
<script src="{{asset('dataTable/js/pdfmake.min.js')}}"></script>
<script src="{{asset('dataTable/js/vfs_fonts.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.print.min.js ')}}"></script>
<!--End Shorting-->
<div id="page-wrapper">
    <div class="container-fluid inner-body-area">                 
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default add-padding">
                    <div class="panel-heading ">
                        <div class="row">
                        	<div class="col-sm-6">
                            	<h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i> Purchase Order Listing</h3>
                            </div>
                            <div class="col-sm-6">                            
                                <div class="row shorting-area">
                                    <div class="col-sm-2 pull-right">
                                        <a href="{{route('admin-reject-purchase-list')}}" class="btn btn-danger">Rejected</a>
                                    </div>
                                    <div class="col-sm-2 pull-right">
                                        <a href="{{route('admin-archieve-purchase-list')}}" class="btn btn-success">Archive</a>
                                    </div>
                                    <div class="col-sm-2 purchase-filter pull-right">
                                        <a href="javascript:void(0)" onclick="getPoList();" id="show-btn" class="btn btn-success">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                    </div>
                                </div>                                                        
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="panel-body custome-table">
                        <div class="row shorting-area">   
                            <div class="clearfix"></div>
                            <div class="col-sm-12">
                                <div id="mySidenavR" class="sidenavR" style="display:none;">
                                    <div class="filter-right">

                                        <!--<h5>Vendor List Filter</h5>-->
                                        <div class="filter-area">
                                            <a href="javascript:void(0)" class="closebtn" id="hide-filter">×</a>
                                            <div class="col-sm-2">
                                                <select class="select-drop" id="po_number" name="po_number">
                                                	<option value="">PO Number</option>               
                                                </select>
                                            </div>
                                        
                                            <div class="col-sm-2">
                                               <select class="select-drop" id="vendore_name" name="vendore_name">
                                                	<option value="">Vendore Name</option>               
                                                </select>
                                            </div>        
                                            
                                            <div class="col-sm-2">
                                            	<select class="select-drop" id="category1" name="category1">
                                                	<option value="">Design</option>
                                                   
                                                </select>
                                            </div>

                                            <div class="col-sm-2">
                                            	<select class="select-drop" id="category2" name="category2">
                                                	<option value="">Color</option>               
                                                </select>
                                            </div>

                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    <div class="input-group date">
                                                        <input type="text" class="form-control date_picker" placeholder="Due From(dd/mm/yy)"  id="due_from" name="due_from"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    <div class="input-group date">
                                                        <input type="text" class="form-control date_picker" placeholder="Due To(dd/mm/yy)"  id="due_to" name="due_to"/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="col-sm-2">
                                            	<a href="javascript:void(0)" class="btn btn-success" onclick="applyFilter();">Apply Filter</a>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="clearfix"></div>        
                            	        </div>
                                        <div class="clearfix"></div>
                                    </div>    
                                </div>  
                            </div>                                                      
                        </div>

                        <span id="filterResult">
                            <div class="table-responsive ">
                                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Sr. No</th>
                                            <th class="text-center">PO. No</th>
                                            <th class="text-center">Vendor Name</th>
                                            <th class="text-center">PO. Date</th>
                                            <th class="text-center">Due. Date</th>
                                            <th class="text-center">Item Code</th>
                                            <th class="text-center">Design</th>
                                            <th class="text-center">Colour</th>
                                            <th class="text-center">Cat3</th>
                                            <th class="text-center">Size/Width</th>
                                            <th class="text-center">Price</th>
                                            <th class="text-center">Order Qty</th>
                                            <th class="text-center">Received Qty</th>
                                            <th class="text-center">Cancelled Qty</th>
                                            <th class="text-center">GIT Qty</th>
                                            <th class="text-center">Pending Qty</th>
                                            <th class="text-center">Adhoc Qty</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                
                                    </tbody>
                                </table>
                            </div>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    <!-- /.row -->
    </div>
<!-- /.container-fluid -->
</div>
 <!-- /#page-wrapper -->
@include('layouts.footer')
<!--add vendorChangecutting Modal Start-->




<script>
var ptoken='{{csrf_token()}}';
var cutting_url='{{route("purchase-order-request")}}';
</script>
<script src="{{asset('js/custom/admin_purchase_order.js')}}"></script>  
<script>   

    function getPoList()
    {
        $('.loading').show();
        $.ajax({
            type: 'GET', 
            url: '{{route("po-list")}}',
            success:function(data)
            {
                $('#po_number').html(data['po']);
                getVendorList();
                $('.loading').hide();
            }
            error: function (error) 
            {                
                alert('Error Occured Please Try Again');
            }
        });
    }

    function getVendorList()
    {
        $.ajax({
            type: 'GET', 
            url: '{{route("get-vendorlist")}}',
            success:function(data)
            {
                $('#vendore_name').html(data['vendor']);
                getDesignList();
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
    }


    function getDesignList()
    {
        $.ajax({
            type: 'GET', 
            url: '{{route("design-list")}}',
            success:function(data)
            {
                $('#category1').html(data['design']);
                getColorList();
                $('.loading').hide();
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
    }

    function getColorList()
    {
        $.ajax({
            type: 'GET', 
            url: '{{route("color-list")}}',
            success:function(data)
            {
                $('#category2').html(data['color']);
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
    }

    $(document).ready(function () {        
        applyFilter();
    });


    $('#due_from').on('changeDate', function(ev){
        $(this).datepicker('hide');
    });
    $('#due_to').on('changeDate', function(ev){
        $(this).datepicker('hide');
    });


    $(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        //$('#load a').css('color', '#dfecf6');
        //$('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');

        var url = $(this).attr('href'); 
		var page=$(this).attr('href').split('page=')[1];
		
		var po_number=$("#po_number").val();
		var category1=$("#category1").val();
		var category2=$("#category2").val();
		var due_from=$("#due_from").val();
		var due_to=$("#due_to").val();
		var vendore_name=$("#vendore_name").val();
		
	 
        filter(page,po_number,category1,category2,due_from,due_to,vendore_name);
        //window.history.pushState("", "", url);
    });

    
});

function applyFilter(){
		var po_number=$("#po_number").val();
		var category1=$("#category1").val();
		var category2=$("#category2").val();
		var due_from=$("#due_from").val();
		var due_to=$("#due_to").val();
		var vendore_name=$("#vendore_name").val();
	 
        filter(1,po_number,category1,category2,due_from,due_to,vendore_name);
}

$( "#hide-filter" ).click(function() {
	
	$("#po_number").val('');
	$("#category1").val('');
	$("#category2").val('');
	$("#due_from").val('');
	$("#due_to").val('');
	$("#vendore_name").val('');
	
	filter(); 
});

function filter(page=1,po_number='',category1='',category2='',due_from='',due_to='',vendore_name=''){	
	$('.loading').show();
	$.ajax({
		type: 'GET', 
		url: '{{route("filter-purchase-order")}}',
		data: {po_number:po_number,category1:category1,category2:category2,due_from:due_from,due_to:due_to,vendore_name:vendore_name,page:page},
		success:function(data)
		{
			$('#filterResult').html(data);           			           
            $('.loading').hide();            
		},
		error: function (error) 
		{
			
			alert('Error Occured Please Try Again');
		}
	})
}
</script>   