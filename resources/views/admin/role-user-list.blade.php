@include('layouts.header')
<script src="{{asset('dataTable/css/jquery.dataTables.min.css')}}"></script>
<script src="{{asset('dataTable/css/buttons.dataTables.min.css')}}"></script>

   
<script src="{{asset('dataTable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('dataTable/js/dataTables.buttons.min.js')}}"></script>   
<script src="{{asset('dataTable/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('dataTable/js/jszip.min.js')}}"></script>   
<script src="{{asset('dataTable/js/pdfmake.min.js')}}"></script>
<script src="{{asset('dataTable/js/vfs_fonts.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('dataTable/js/buttons.print.min.js ')}}"></script>
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <div class="row">
                    <div class="col-lg-12">
                    
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                	<div class="col-sm-6"><h3 class="panel-title">
                                    <i class="fa fa-users" aria-hidden="true"></i>User Listing</h3></div>
                                    <div class="col-sm-6">
                                    
                                    <div class="shorting-area">
                            		
                              
                                
                            </div>
                                    
                                    
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">ID</th>
                                                <th class="text-center">Name</th>
                                                <th class="text-center">Email</th>
                                                <th class="text-center">Phone No</th>
                                                <th class="text-center">Role</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <?php $count=0;?>
                                    @foreach($user as $row)
                                    <?php $count++;?>
                                    <tr>
                                        <td class="text-center">{{$count}}</td>
                                        <td class="text-center">{{$row->name}}</td>
                                        <td class="text-center">{{$row->email}}</td>
                                        <td class="text-center">{{$row->mobile}}</td>
                                        <td class="text-center">{{$row->role_name}}</td>
                                        
                                    </tr> 
                                    @endforeach                          
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                             
                        
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
        <!--add user popup Modal Start-->

        <style type="text/css">
            
            .class_err{color: red;}
        </style>
<div id="adduser" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <form action="{{route('add-role')}}" method="post">
        <input type="hidden" name="_token" value="{{csrf_token()}}"> 
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add New Role</h4>
          </div>
          <div class="modal-body">
            <div class="row shorting-area pricechange">
                <div class="col-sm-12">
                    <h5>Name :</h5>
                    <input type="text" id="name" name="name" class="form-control"/>
                    <span class=" error class_err" id="name_msg" ></span>
                </div>
                <div class="col-sm-12">
                    <h5>Slug :</h5>
                    <input type="text" id="slug" name="slug" class="form-control"/>
                    <span class=" error class_err" id="slug_msg" ></span>
                </div>
                <div class="col-sm-12">
                    <h5>Description :</h5>
                    <textarea type="text" id="description" name="description" class="form-control"></textarea>
                    <span class=" error class_err" id="desc_msg" ></span>
                </div>
                <div class="clearfix"></div>
                <div class="col-sm-6">
                <input class="pup-btn" onclick="return validateForm();" type="submit" value="Add Role">
                </div>
                <div class="col-sm-6">
                    <a href="#" data-dismiss="modal" class="pup-btn">Cancel
                    </a>
                </div>
            <div class="clearfix"></div>
            </div>
          </div>
          <div class="modal-footer"></div>
        </div>
    </form>
  </div>
</div>
<div id="editRole" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <form action="{{route('update-role')}}" method="post">
        <input type="hidden" name="_token" value="{{csrf_token()}}"> 
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit Role</h4>
          </div>
          <div class="modal-body">
            <div class="row shorting-area pricechange">
                <div class="col-sm-12">
                    <h5>Name :</h5>
                    <input type="hidden" id="edit_role_id" name="role_id" class="form-control"/>
                    <input type="text" id="edit_name" name="name" class="form-control"/>
                    <span class=" error class_err" id="edit_name_msg" ></span>
                </div>
                <div class="col-sm-12">
                    <h5>Slug :</h5>
                    <input type="text" id="edit_slug" name="slug" class="form-control"/>
                    <span class=" error class_err" id="edit_slug_msg" ></span>
                </div>
                <div class="col-sm-12">
                    <h5>Description :</h5>
                    <textarea type="text" id="edit_description" name="description" class="form-control"></textarea>
                    <span class=" error class_err" id="edit_desc_msg" ></span>
                </div>
                <div class="clearfix"></div>
                <div class="col-sm-6">
                <input class="pup-btn" onclick="return editValidate();" type="submit" value="Add Role">
                </div>
                <div class="col-sm-6">
                    <a href="#" data-dismiss="modal" class="pup-btn">Cancel
                    </a>
                </div>
            <div class="clearfix"></div>
            </div>
          </div>
          <div class="modal-footer"></div>
        </div>
    </form>
  </div>
</div>
<!--add user popup  Modal End-->
@include('layouts.footer')

<script type="text/javascript">

$(document).ready(function () {
            
            $('#example').dataTable({
                'order': [[ 0, "asc" ]],//Default Column Attribute
                'pageLength': 10,//Per Page Item
                 "searching": false,
                'dom': 'Bfrtip',//Header Info               
                'buttons': ['excel'],//['copy', 'csv', 'excel', 'pdf', 'print']
                'aoColumnDefs': [          
                                   { aTargets: [ 0 ], bSortable: true },
                                   { aTargets: [ 1 ], bSortable: true },
                                   { aTargets: [ 2 ], bSortable: false },
                                   { aTargets: [ 3 ], bSortable: false },
                                   { aTargets: [ 4 ], bSortable: false }
                                ],
                "oLanguage": {
                                "oPaginate": {
                                                "sNext": '<i class="fa fa-chevron-right" ></i>',
                                                "sPrevious": '<i class="fa fa-chevron-left" ></i>'
                                            }
                            }
            });
        });


function validateForm()
{    
    var ret_val=true;
    var name=$.trim($("#name").val());    
    if(name==''){
        $("#name_msg").text('Please Enter Name  !').show();
        ret_val=false;
    }
    else{
        $("#name_msg").hide();
    }

    var slug=$.trim($("#slug").val());
    if(slug==''){
        $("#slug_msg").text('Please Enter Slug  !').show();
        ret_val=false;
    }
    else{
        $("#slug_msg").hide();
    }

    return ret_val;
} 
function deleteConfirm()
{
    if(confirm('Are you sure'))
    {
        return true;
    }
    else
    {
        return false;
    }
}
function editRole(id)
{
    $("#edit_name_msg").hide();
    $("#edit_slug_msg").hide();
    var eurl="{{route('edit-role')}}";
    var token="{{csrf_token()}}";
    $.ajax({
        type:'post',
        url:eurl,
        data:{id:id,_token:token},
        success:function(data)
        {
            $('#edit_role_id').val(id);
            $('#edit_name').val(data.name);
            $('#edit_slug').val(data.slug);
            $('#edit_description').val(data.description);
        }



    });
}
function editValidate()
{
    var ret_val=true;
    var name=$.trim($("#edit_name").val());    
    if(name==''){
        $("#edit_name_msg").text('Please Enter Name  !').show();
        ret_val=false;
    }
    else{
        $("#edit_name_msg").hide();
    }

    var slug=$.trim($("#edit_slug").val());
    if(slug==''){
        $("#edit_slug_msg").text('Please Enter Slug  !').show();
        ret_val=false;
    }
    else{
        $("#edit_slug_msg").hide();
    }

     return ret_val;
}
</script>