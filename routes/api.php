<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1'], function() {

	Route::post('/versionCheck', [
	    'as' => 'versionCheck',
	    'uses' => 'ApiController@versionCheck',
	]);

	Route::post('/login', [
	    'as' => 'login',
	    'uses' => 'ApiController@login',
	]);

	Route::post('/logout', [
	    'as' => 'logout',
	    'uses' => 'ApiController@logout',
	]);

	Route::post('/changePassword', [
	    'as' => 'changePassword',
	    'uses' => 'ApiController@changePassword',
	]);

	Route::post('/getPolist', [
	    'as' => 'getPolist',
	    'uses' => 'ApiController@getPolist',
	]);

	Route::post('/getPoDetails', [
	    'as' => 'getPoDetails',
	    'uses' => 'ApiController@getPoDetails',
	]);
		
	Route::post('/acceptOrReject', [
	    'as' => 'acceptOrReject',
	    'uses' => 'ApiController@poStatus',
	]);
	
	Route::get('/user', function (Request $request) {
	    return $request->user();
	})->middleware('auth:api');

});