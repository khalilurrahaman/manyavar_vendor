<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/



Route::get('/docs', function () {
    return view('welcome');
});

Route::get('/info', 'oci8ConnectionController@getInfo');

Auth::routes();

Route::get('/', 'HomeController@userLogin');
Route::get('/home', 'HomeController@userLogin');
Route::get('/logout', 'HomeController@userLogout');
Route::get('/forgot-password',[
	'as'=> 'forgot-password',
	'uses'=>'ForgotController@index'
	]);
/*Route::get('/reset-password/{email}',[
	'as'=> 'reset-password',
	'uses'=>'ForgotController@resetPassword'
	]);*/
Route::get('/reset-password',[
	'as'=> 'reset-password',
	'uses'=>'ForgotController@resetPassword'
	]);
Route::post('/submitResetPassword',[
	'as'=> 'submitResetPassword',
	'uses'=>'ForgotController@submitResetPassword'
	]);
Route::get('/reset-successfull',[
	'as'=> 'reset-successfull',
	'uses'=>'ForgotController@resetSuccess'
	]);
Route::post('/forgot-password', 'ForgotController@checkUser');
Route::get('/check-login', [
	'as'=>'check-login',
	'uses'=>'HomeController@checkLogin'
]);

Route::get('/sync-database', [
 	'as'=>'sync-database',
    'uses'=>'synkController@index'
]);

/*Vendor Manegment*/
//Route::group(['middleware' => 'auth'], function () {

Route::group(['prefix' => 'admin','middleware'=>'auth'], function() {

	Route::get('/dashboard',[
		'as' => 'dashboard',
		'uses' => 'DashboardController@adminDashboard',
	]);

	Route::get('/all-rfq-request',[
		'as' => 'rfq-request',
		'uses' => 'RequestQuotationController@index',
	]);

	Route::get('/clost-request/{id}',[
		'as' => 'close-quotation',
		'uses' => 'RequestQuotationController@closeRequest',
	]);

	
	Route::get('/all-rfq-list',[
		'as' => 'rfq-list',
		'uses' => 'RequestQuotationController@rfqList',
	]);

	Route::get('/filter-concluded-rfq',[
		'as' => 'filter-concluded-rfq',
		'uses' => 'RequestQuotationController@filterConcludedRfq',
	]);

	Route::get('/filter-action',[
		'as' => 'filter-action',
		'uses' => 'AdminAllRequestController@filterAction',
	]);

	Route::post('/notify-vendor',[
		'as' => 'notify-vendor',
		'uses' => 'RequestQuotationController@notifyVendor',
	]);



	Route::get('/get-vendor',[
		'as' => 'get-vendor',
		'uses' => 'RequestQuotationController@getVendorList',
	]);

	Route::get('/close-rfq/{id}',[
		'as' => 'close-rfq',
		'uses' => 'RequestQuotationController@closeRfq',
	]);

	Route::post('/add_item',[
		'as' => 'add-item',
		'uses' => 'RequestQuotationController@addNewItem',
	]);

	Route::get('/add-rfq', [
		'as' => 'add-rfq-form',
		'uses' => 'RequestQuotationController@getAddrfq',
	]);

	Route::post('/add-rfq', [
		'as' => 'add-rfq-form-post',
		'uses' => 'RequestQuotationController@postAddrfq',
	]);
	Route::get('/get-option-filter', [
		'as' => 'get-option-filter',
		'uses' => 'RequestQuotationController@getOptionFilter',
	]);
	Route::get('/search-item-by-keyword', [
		'as' => 'search-item-by-keyword',
		'uses' => 'RequestQuotationController@searchItemByKeyword',
	]);
	Route::get('/resolve-git',[
    	'as' => 'resolve-git-records-admin',
        'uses' => 'AdminGitController@resolveGitRecords'
    ]);

    Route::post('/resolve-grc-records-admin',[
		'as' => 'resolve-grc-records-admin',
		'uses' => 'VendorGitController@resolveGitRecordsAdmin',
	]);

	Route::get('/commants/{id}', [
		'as' => 'admin-comments',
		'uses' => 'RequestQuotationController@adminCommant',
	]);

	Route::post('/commants', [
		'as' => 'admin-add-comments',
		'uses' => 'RequestQuotationController@adminAddCommant',
	]);

    Route::get('/faq',[
    	'as' => 'admin-faq-req',
        'uses' => 'AdminFaqController@viewfaq'
    ]);

    Route::get('/reports',[
    	'as' => 'ad-reportss',
        'uses' => 'AdminReportsController@viewReport'
    ]);

    Route::get('/all-request',[
    	'as' => 'ad-all-request',
        'uses' => 'AdminAllRequestController@viewAllRequest'
    ]);
    
    Route::get('/filter-allcuttingsampleothers',[
		'as' => 'filter-allcuttingsampleothers',
		'uses' => 'AdminAllRequestController@filterAction',
	]);

    Route::get('/allcuttingsampleothers-request',[
    	'as' => 'ad-allcuttingsampleothers-request',
        'uses' => 'AdminAllRequestController@viewAllcuttingsapmleothersRequest'
    ]);
    
    Route::get('/allwidthratechange-request',[
    	'as' => 'ad-allwidthratechange-request',
        'uses' => 'AdminAllRequestController@viewAllwidthrateRequest'
    ]);
    
    Route::get('/filter-allwidthratechange',[
		'as' => 'filter-allwidthratechange',
		'uses' => 'AdminAllRequestController@filterAllwidthratechange',
	]);

	Route::get('/alldateext-request',[
    	'as' => 'ad-alldateext-request',
        'uses' => 'AdminAllRequestController@viewAlldateextRequest'
    ]);
    
    Route::get('/filter-alldateext',[
		'as' => 'filter-alldateext',
		'uses' => 'AdminAllRequestController@filterAlldateext',
	]);

    Route::post('/accept-request',[
    	'as' => 'accept-request',
        'uses' => 'AdminAllRequestController@acceptRequest'
    ]);
    
    Route::post('/reject-request',[
    	'as' => 'reject-request',
        'uses' => 'AdminAllRequestController@rejectRequest'
    ]);
    
    Route::get('/vendor-newpo-selectlist', [
		'as' => 'vendor-newpo-selectlist',
		'uses' => 'AdminAllRequestController@getnewselectPoList'
	]);

    Route::get('/admin-acceptothers-request/{id}',[
    	'as' => 'admin-acceptothers-request',
        'uses' => 'AdminAllRequestController@acceptOthersRequest'
    ]);
    
    Route::post('/closewidthrate-request',[
    	'as' => 'closewidthrate-request',
        'uses' => 'AdminAllRequestController@closeRequest'
    ]);

    Route::get('/req-comments/{req_id}', [
		'as' => 'admin-requestwise-comments',
		'uses' => 'AdminAllRequestController@reqWiseComments',
	]);

	Route::post('/add-request-comments', [
		'as' => 'add-adminrequest-comments',
		'uses' => 'AdminAllRequestController@addReqComments',
	]);

	/*GIT*/
	Route::get('/all-git-list',[ 
		'as' => 'all_git_list',
		'uses' =>'AdminGitController@viewGit'
	]);

	Route::get('/mark-complete/{id}',[ 
		'as' => 'mark-complete',
		'uses' =>'AdminGitController@markCompleteGit'
	]);
	
	Route::get('/all-archive-git-list',[ 
		'as' => 'all_archive_git_list',
		'uses' =>'AdminGitController@viewArchiveGit'
	]);
	Route::get('/filter-admin-git-archive', [
		'as' => 'filter-admin-git-archive',
		'uses' => 'AdminGitController@filterArchiveGit',
	]);

	Route::get('/get-archive-git-vendor', [
		'as' => 'get-archive-git-vendor',
		'uses' => 'AdminGitController@getArchiveVendor',
	]);
	
	Route::get('/get-archive-git-cat1', [
		'as' => 'get-archive-git-cat1',
		'uses' => 'AdminGitController@getArchiveCat1',
	]);
	
	Route::get('/get-archive-git-cat2', [
		'as' => 'get-archive-git-cat2',
		'uses' => 'AdminGitController@getArchiveCat2',
	]);

	/*GIT Next Change*/
	Route::post('/mark-complete-post',[ 
		'as' => 'mark-complete-post',
		'uses' =>'AdminGitController@markCompleteGitPost'
	]);

	Route::get('/all-grt-list',[
		'as' => 'all_grt_list',
		'uses' => 'VendorGitController@viewGrt'
	]);

	Route::get('/all-grc-list',[
		'as' => 'all_grc_list',
		'uses' => 'VendorGitController@viewGrc'
	]);

	Route::get('/get-admin-git-vendor', [
		'as' => 'get-admin-git-vendor',
		'uses' => 'AdminGitController@getAdminVendor',
	]);
	
	Route::get('/get-admin-git-cat1', [
		'as' => 'get-admin-git-cat1',
		'uses' => 'AdminGitController@getAdminCat1',
	]);
	
	Route::get('/get-admin-git-cat2', [
		'as' => 'get-admin-git-cat2',
		'uses' => 'AdminGitController@getAdminCat2',
	]);
	Route::get('/filter-admin-git', [
		'as' => 'filter-admin-git',
		'uses' => 'AdminGitController@filterAdminGit',
	]);

	//================Adfmin Purchase order list=============//
	Route::get('/purchase-order-list', [
		'as' => 'purchase_order_list',
		'uses' => 'PurchaseOrderController@index'
	]);

	Route::get('/po-list', [
		'as' => 'po-list',
		'uses' => 'PurchaseOrderController@getPoList'
	]);

	Route::get('/get-vendorlist', [
		'as' => 'get-vendorlist',
		'uses' => 'PurchaseOrderController@getVendorList'
	]);

	Route::get('/design-list', [
		'as' => 'design-list',
		'uses' => 'PurchaseOrderController@getDesignList'
	]);

	Route::get('/color-list', [
		'as' => 'color-list',
		'uses' => 'PurchaseOrderController@getColorList'
	]);
	
	Route::get('/main-category3-list', [
		'as' => 'main-category3-list',
		'uses' => 'PurchaseOrderController@getCategory3List'
	]);

	Route::get('/main-width-size-list', [
		'as' => 'main-width-size-list',
		'uses' => 'PurchaseOrderController@getWidthSizeList'
	]);
	
	Route::get('/filter-purchase-order', [
		'as' => 'filter-purchase-order',
		'uses' => 'PurchaseOrderController@filterPurchaseOrder'
	]);

	Route::get('/filter-purchase-order-po-list-auto', [
		'as' => 'filter-purchase-order-po-list-auto',
		'uses' => 'PurchaseOrderController@filterPurchaseOrderAutoSearch'
	]);

	//================Archive order list=============//
    Route::get('/archieve-purchase-order-list', [
		'as' => 'admin-archieve-purchase-list',
		'uses' => 'PurchaseOrderController@archievePOview'
	]);

	Route::get('/archive-po-list', [
		'as' => 'archive-po-list',
		'uses' => 'PurchaseOrderController@getArchivePoList'
	]);

	Route::get('/archive-get-vendorlist', [
		'as' => 'archive-get-vendorlist',
		'uses' => 'PurchaseOrderController@getVendorList'
	]);

	Route::get('/archive-design-list', [
		'as' => 'archive-design-list',
		'uses' => 'PurchaseOrderController@getArchiveDesignList'
	]);

	Route::get('/archive-color-list', [
		'as' => 'archive-color-list',
		'uses' => 'PurchaseOrderController@getArchiveColorList'
	]);
	
	Route::get('/archive-category3-list', [
		'as' => 'archive-category3-list',
		'uses' => 'PurchaseOrderController@getArchiveCategory3List'
	]);
	
	Route::get('/archive-width-size-list', [
		'as' => 'archive-width-size-list',
		'uses' => 'PurchaseOrderController@getArchiveWidthSizeList'
	]);

	Route::get('/filter-archieve-purchase-order', [
		'as' => 'filter-archieve-purchase-order',
		'uses' => 'PurchaseOrderController@filterArchievePurchaseOrder'
	]);


	//================Rejected order list=============//
	Route::get('/reject-purchase-order-list', [
		'as' => 'admin-reject-purchase-list',
		'uses' => 'PurchaseOrderController@rejectPOview'
	]);

	Route::get('/reject-po-list', [
		'as' => 'reject-po-list',
		'uses' => 'PurchaseOrderController@getRejectPoList'
	]);

	Route::get('/reject-get-vendorlist', [
		'as' => 'reject-get-vendorlist',
		'uses' => 'PurchaseOrderController@getRejectVendorList'
	]);

	Route::get('/reject-design-list', [
		'as' => 'reject-design-list',
		'uses' => 'PurchaseOrderController@getRejectDesignList'
	]);

	Route::get('/reject-color-list', [
		'as' => 'reject-color-list',
		'uses' => 'PurchaseOrderController@getRejectColorList'
	]);
	
	Route::get('/reject-category3-list', [
		'as' => 'reject-category3-list',
		'uses' => 'PurchaseOrderController@getRejectCategory3List'
	]);
	
	Route::get('/reject-width-size-list', [
		'as' => 'reject-width-size-list',
		'uses' => 'PurchaseOrderController@getRejectWidthSizeList'
	]);

	Route::get('/filter-reject-purchase-order', [
		'as' => 'filter-reject-purchase-order',
		'uses' => 'PurchaseOrderController@filterRejectPurchaseOrder'
	]);


    Route::get('/git-comments/{git_id}', [
		'as' => 'admin-git-comments',
		'uses' => 'AdminGitController@adminGitComment',
	]);

	Route::post('/comments', [
		'as' => 'admin-add-git-comments',
		'uses' => 'AdminGitController@addGitComment',
	]);


	Route::get('/goods-return', [
		'as' => 'goods-returns-list',
		'uses' => 'GoodsReturnController@viewList'
	]);

    Route::get('/admin-goods-comments/{id}', [
		'as' => 'admin-goods-comments',
		'uses' => 'GoodsReturnController@adminGoodsComments',
	]);

    Route::post('/add-admin-goods-comments', [
		'as' => 'add-admin-goods-comments',
		'uses' => 'GoodsReturnController@addAdminGoodsComments',
	]);
	
	Route::get('/add-return', [
		'as' => 'goods-returns-add-form',
		'uses' => 'GoodsReturnController@viewAddform'
	]);
	Route::get('/closed-return', [
		'as' => 'goods-returns-closed',
		'uses' => 'GoodsReturnController@closedReturn'
	]);

	Route::post('/add-return', [
		'as' => 'add-returns-form-post',
		'uses' => 'GoodsReturnController@postAddreturn'
	]);
	Route::get('/get-goods-vendor',[
		'as'=>'get-goods-vendor',
		'uses'=>'GoodsReturnController@getVendor'
	]);

	Route::get('/get-goods-cat1',[
		'as'=>'get-goods-cat1',
		'uses'=>'GoodsReturnController@getCat1'
	]);
	Route::get('/get-goods-cat2',[
		'as'=>'get-goods-cat2',
		'uses'=>'GoodsReturnController@getCat2'
	]);

	/*User*/
	Route::get('/user-list',[
		'as' => 'user-listing', 
	    'uses' => 'AdminUserController@usetList'
	]);

	Route::get('/user_add',[
		'as' => 'ad-user',
	    'uses' => 'AdminUserController@userAdd'
	]);

	Route::post('/add_user',[
		'as' => 'adding-user',
	    'uses' => 'AdminUserController@postAddUser'
	]);

	Route::get('/edit_user/{user_id}',[
		'as' => 'admin-edit-user',
	    'uses' => 'AdminUserController@getEditUser'
	]);

	Route::post('/update_user',[
		'as' => 'update-user-admin',
	    'uses' => 'AdminUserController@updateUser'
	]);

	Route::get('/delete_user/{id}',[
		'as' => 'admin-del-user',
	    'uses' => 'AdminUserController@deleteUser'
	]);

	Route::get('/user/archive/{id}',[
		'as' => 'user-archieve',
	    'uses' => 'AdminUserController@archiveUser'
	]);

	Route::get('/users/activate/{id}',[
		'as' => 'users-activate',
	    'uses' => 'AdminUserController@activateUser'
	]);
	
	Route::get('/user-availability', [
		'as' => 'user-availability',
		'uses' => 'AdminUserController@userAvailability',
	]);
	/*End User*/
	
	/*User Role Management*/
	Route::get('/role-list',[
		'as' => 'role-listing', 
	    'uses' => 'roleController@index'
	]);

	Route::post('/add-role',[
		'as' => 'add-role',
	    'uses' => 'roleController@roleAdd'
	]);

	Route::post('/update-role',[
		'as' => 'update-role',
	    'uses' => 'roleController@roleUpdate'
	]);

	Route::post('/edit-role',[
		'as' => 'edit-role',
	    'uses' => 'roleController@roleEdit'
	]);

	Route::get('/delete-role/{id}',[
		'as' => 'delete-role',
	    'uses' => 'roleController@roleDelete'
	]);

	Route::get('/add-permission/{id}',[
		'as' => 'add-permission',
	    'uses' => 'roleController@addPermission'
	]);

	Route::post('/insert-add-permission',[
		'as' => 'insert-add-permission',
	    'uses' => 'roleController@insertPermission'
	]);
	Route::get('/view-user/{id}',[
		'as' => 'view-user',
	    'uses' => 'roleController@viewUser'
	]);

	Route::get('/role-validation',[
		'as' => 'role-validation',
	    'uses' => 'roleController@roleValidation'
	]);

	Route::get('/add-new-role',[
		'as' => 'add-new-role',
	    'uses' => 'roleController@addNewRole'
	]);
	Route::get('/edit-new-role/{id}',[
		'as' => 'edit-new-role',
	    'uses' => 'roleController@editNewRole'
	]);


	Route::get('/vendor-list',[
		'as' => 'ven-listing',
	    'uses' => 'AdminVendorController@index'
	]);

	
	Route::get('/add-vendor', 'AdminVendorController@addVendor');
	Route::post('/insert-vendor', 'AdminVendorController@insertVendor');
	Route::get('/edit-vendor/{id}', 'AdminVendorController@editVendor');

	Route::get('/vendor-status-active/{id}',[
		'as' => 'vendor-status-act',
	    'uses' => 'AdminVendorController@statusActive'
	]);

	Route::get('/vendor-status-deactive/{id}',[
		'as' => 'vendor-status-deact',
	    'uses' => 'AdminVendorController@statusDeactive'
	]);
	Route::get('/filter-vendor-list', [
		'as' => 'filter-vendor-list',
		'uses' => 'AdminVendorController@filterVendorList'
	]);


	Route::post('/update-vendor', 'AdminVendorController@updateVendor');
	/*Department*/
	Route::get('/vendor-department-list',[
		'as' => 'departmnt-lising',
	    'uses' => 'AdminDepartmentController@index'
	]);

	Route::post('/vendor-department-add',[
		'as' => 'vendor-dept-add',
	    'uses' => 'AdminDepartmentController@addDepartment'
	]);

	Route::post('/dept-editData',[
		'as' => 'dept-eddata',
	    'uses' => 'AdminDepartmentController@getEditData'
	]);

	Route::post('/vendor-department-edit',[
		'as' => 'vendor-dept-edit',
	    'uses' => 'AdminDepartmentController@updateDepartment'
	]);

	Route::get('/vendor-department-delete/{id}',[
		'as' => 'ven-dept-delete',
	    'uses' => 'AdminDepartmentController@deleteDepartment'
	]);
	Route::get('/add-department',[
		'as' => 'add-department',
	    'uses' => 'AdminDepartmentController@addDepartmentPage'
	]);
	Route::get('/edit-department/{id}',[
		'as' => 'edit-department',
	    'uses' => 'AdminDepartmentController@editDepartmentPage'
	]);


	/*Section*/
	Route::get('/vendor-section-list',[
		'as' => 'section-listing',
	    'uses' => 'AdminSectionController@index'
	]);

	Route::post('/vendor-section-add',[
		'as' => 'vendor-sec-add',
	    'uses' => 'AdminSectionController@addSection'
	]);

	Route::post('/sec-editData',[
		'as' => 'sec-eddata',
	    'uses' => 'AdminSectionController@getEditData'
	]);

	Route::post('/vendor-section-edit',[
		'as' => 'vendor-sec-ed',
	    'uses' => 'AdminSectionController@updateSection'
	]);

	Route::get('/vendor-section-delete/{id}',[
		'as' => 'vendor-sec-delete',
	    'uses' => 'AdminSectionController@deleteSection'
	]);

	Route::get('/add-section',[
		'as' => 'add-section',
	    'uses' => 'AdminSectionController@addSectionPage'
	]);
	Route::get('/edit-section/{id}',[
		'as' => 'edit-section',
	    'uses' => 'AdminSectionController@editSectionPage'
	]);


	/*Devision*/
	Route::get('/vendor-devision-list',[
		'as' => 'division-listing',
	    'uses' => 'AdminDevisionController@index'
	]);

	Route::get('/add-devision',[
		'as' => 'add-devision',
	    'uses' => 'AdminDevisionController@addDevisionPage'
	]);
	Route::get('/edit-devision/{id}',[
		'as' => 'edit-devision',
	    'uses' => 'AdminDevisionController@editDevisionPage'
	]);

	Route::post('/vendor-devision-add',[
		'as' => 'ven-devisionadd',
	    'uses' => 'AdminDevisionController@addDevision'
	]);

	Route::post('/dev-editData',[
		'as' => 'devision-eddata',
	    'uses' => 'AdminDevisionController@getEditData'
	]);

	Route::post('/vendor-devision-edit',[
		'as' => 'ven-devision-updatedata',
	    'uses' => 'AdminDevisionController@updateDevision'
	]);

	Route::get('/vendor-devision-delete/{id}',[
		'as' => 'ven-dev-delete',
	    'uses' => 'AdminDevisionController@deleteDevision'
	]);

	/*Profile and password set*/
	Route::get('/edit-profile',[
		'as' => 'admin-profileedit-req',
	    'uses' => 'AdminUserController@editProfile'
	    ]);

	Route::post('/update-profile',[
		'as' => 'admin-profileupdate-req',
	    'uses' => 'AdminUserController@updateProfile'
	    ]);

	Route::get('/change-password',[
		'as' => 'admin-change-pass',
	    'uses' => 'AdminUserController@editPassword'
	    ]);

	Route::post('/update-password',[
		'as' => 'admin-updt-password',
	    'uses' => 'AdminUserController@updatePassword'
	    ]);

	Route::get('/set-password/{id}',[
		'as' => 'set-ven-password',
	    'uses' => 'AdminVendorController@vendorpass'
	]);

	Route::post('/set-ven-password',[
		'as' => 'update-passss',
	    'uses' => 'AdminVendorController@setpassword'
	]);

/*Item master*/

	Route::get('/item-master',[
		'as' => 'item-master',
	    'uses' => 'ItemMasterController@itemMaster'
	]);
	
	
	
	
	Route::post('/add-item-master',[
		'as' => 'add-item-master',
	    'uses' => 'ItemMasterController@addItem'
	]);
	Route::get('/item-master-list',[
		'as' => 'item-master-list',
	    'uses' => 'ItemMasterController@index'
	]);
	
	Route::get('/item-master-sample',[
		'as' => 'item-master-sample',
	    'uses' => 'ItemMasterController@sample'
	]);
	
	Route::get('/search-item-by-keyword-sample',[
		'as' => 'search-item-by-keyword-sample',
	    'uses' => 'ItemMasterController@searchItemSample'
	]);
	
	Route::get('/search-item-by-keyword-live-list',[
		'as' => 'search-item-by-keyword-live-list',
	    'uses' => 'ItemMasterController@searchItemLive'
	]);

	Route::get('/get-option-filter-live', [
		'as' => 'get-option-filter-live',
		'uses' => 'ItemMasterController@getOptionFilter',
	]);
	
	
	Route::get('/get-option-filter-live-list', [
		'as' => 'get-option-filter-live-list',
		'uses' => 'ItemMasterController@getOptionFilterList',
	]);
	
	Route::get('/search-item-by-keyword-live', [
		'as' => 'search-item-by-keyword-live',
		'uses' => 'ItemMasterController@searchItemByKeyword',
	]);
	
	Route::get('/merge-item-master', [
		'as' => 'merge-item-master',
		'uses' => 'ItemMasterController@mergeItem',
	]);
	
	
	Route::get('/get-option-filter-sample', [
		'as' => 'get-option-filter-sample',
		'uses' => 'ItemMasterController@getOptionFilterSample',
	]);

	Route::get('/search/autocompleteVendor/', [
	    'as' => 'search-autocomplete-vendor',
	    'uses' => 'ItemMasterController@autocompleteVendor',
	]);

	Route::get('/search/autocompleteDesign/', [
	    'as' => 'search-autocomplete-design',
	    'uses' => 'ItemMasterController@autocompleteDesign',
	]);
	
	
	Route::get('/item-master-edit/{id}',[
		'as' => 'item-master-edit',
	    'uses' => 'ItemMasterController@editItem'
	]);
	Route::post('/item-master-update',[
		'as' => 'item-master-update',
	    'uses' => 'ItemMasterController@updateItem'
	]);
	Route::get('/item-master-delete/{id}',[
		'as' => 'item-master-delete',
	    'uses' => 'ItemMasterController@deleteItem'
	]);

	Route::get('/delete-product-image', [
		'as' => 'delete-product-image',
		'uses' => 'ItemMasterController@deleteProductImage',
	]);
/*Price list*/
    Route::get('/price-list',[
		'as' => 'price-list',
	    'uses' => 'PriceController@index'
	]);

     Route::get('/price-edit/{id}',[
		'as' => 'price-edit',
	    'uses' => 'PriceController@editPrice'
	]);
	Route::post('/price-update',[
		'as' => 'price-update',
	    'uses' => 'PriceController@updatePrice'
	]);

	 Route::get('/reject-req-price/{id}',[
    	'as' => 'reject-req-price',
        'uses' => 'PriceController@rejectPricechangeReq'
    ]);

	Route::get('/search-item-by-keyword-admin',[
		'as' => 'search-item-by-keyword-admin',
	    'uses' => 'PriceController@searchByKeywordAdmin'
	]);

	Route::get('/search-item-by-keyword-vendor',[
		'as' => 'search-item-by-keyword-vendor',
	    'uses' => 'PriceController@searchByKeywordVendor'
	]);
	
	Route::get('/search/autocompleteDesignVendor/', [
	    'as' => 'search-autocomplete-design-vendor',
	    'uses' => 'PriceController@autocompleteDesignVendor',
	]);
/*Goods return*/
Route::post('/add-goods-file',[
		'as' => 'add-goods-file',
	    'uses' => 'GoodsReturnController@addGoodsFile'
	]);
Route::get('/filter-goods-return',[
		'as' => 'filter-goods-return',
	    'uses' => 'GoodsReturnController@filterGoodsReturn'
	]);
Route::get('/filter-goods-return-closed',[
		'as' => 'filter-goods-return-closed',
	    'uses' => 'GoodsReturnController@filterGoodsReturnClosed'
	]);

Route::get('/goods-comments/{id}', [
		'as' => 'goods-comments',
		'uses' => 'GoodsReturnController@goodsComments',
	]);

Route::post('/add-goods-comments', [
		'as' => 'add-goods-comments',
		'uses' => 'GoodsReturnController@addGoodsComments',
	]);
	
Route::get('/filter-rfq-request',[
'as' => 'filter-rfq-request',
'uses' => 'RequestQuotationController@filterRfqRequest'
]);

Route::get('/notification',[
		'as' => 'notification',
	    'uses' => 'DashboardController@notification'
]);

Route::get('/admin-read-notification',[
    	'as' => 'admin-read-notification',
        'uses' => 'DashboardController@readNotification'
]);

Route::get('/document-list',[
		'as'=>'document-list',
		'uses'=>'UploadDocumentController@documentList'
		]);
Route::get('/archieve-document-list',[
		'as'=>'archieve-document-list',
		'uses'=>'UploadDocumentController@archieveDocumentList'
		]);
Route::get('/docs-status-deactive/{id}',[
		'as' => 'docs-status-deactive',
	    'uses' => 'UploadDocumentController@statusDeactive'
	]);
Route::get('/document-type-list',[
		'as' => 'document-type-list',
	    'uses' => 'UploadDocumentController@documentTypeList'
	]);
Route::get('/document-type',[
		'as' => 'document-type',
	    'uses' => 'UploadDocumentController@documentType'
	]);
Route::post('/insert-document-type',[
		'as' => 'insert-document-type',
	    'uses' => 'UploadDocumentController@insertDocumentType'
	]);
Route::get('/document-type-delete/{id}',[
		'as' => 'document-type-delete',
	    'uses' => 'UploadDocumentController@deleteDocumentType'
	]);
Route::get('/edit-document-type/{id}',[
		'as' => 'edit-document-type',
	    'uses' => 'UploadDocumentController@editDocumentType'
	]);
Route::post('/update-document-type',[
		'as' => 'update-document-type',
	    'uses' => 'UploadDocumentController@updateDocumentType'
	]);
Route::get('/get-document-month',[
		'as' => 'get-document-month',
	    'uses' => 'UploadDocumentController@getDocumentMonth'
	]);
Route::get('/get-document-type',[
		'as' => 'get-document-type',
	    'uses' => 'UploadDocumentController@getDocumentType'
	]);
Route::get('/get-document-vendor',[
		'as' => 'get-document-vendor',
	    'uses' => 'UploadDocumentController@getDocumentVendor'
	]);
Route::get('/filter-document-return',[
		'as' => 'filter-document-return',
	    'uses' => 'UploadDocumentController@filterDocumentReturn'
	]);

});


Route::group(['prefix' => 'vendor','middleware'=>'auth'], function() {

	Route::get('/dashboard',[
		'as' =>'ven-dashboard',
	    'uses' => 'VendorDashboardController@vendorDashboard'
	]);

	Route::get('/dashboard-po',[
		'as' =>'vendor-po-accept',
	    'uses' => 'VendorDashboardController@updatePoStatus'
	]);
	Route::post('/dashboard-po-reject',[
		'as' =>'vendor-po-reject',
	    'uses' => 'VendorDashboardController@rejectPoStatus'
	]);

	Route::get('/dashboard-po-details',[
		'as' =>'vendor-po-details',
	    'uses' => 'VendorDashboardController@viewPo'
	]);
    
    Route::get('/new-po',[
		'as' =>'vendor-po-new',
	    'uses' => 'VendorDashboardController@allnewPO'
	]);

	Route::get('/vendor-new-po',[
		'as' =>'vendor-new-po-accept',
	    'uses' => 'VendorDashboardController@selectallPoStatus'
	]);

//Vendor Rfq Section Start
	Route::get('/rfq-request',[
	    'as' => 'ven-rfq-request', 
		'uses' => 'VendorRfqRequestController@index'
		]);
		
	Route::post('/add-rfq-quotation', [
		'as' => 'add-rfq-quotation',
		'uses' => 'VendorRfqRequestController@addRfqQuotation',
	]);
	
	Route::get('/commants/{quotation_id}', [
		'as' => 'quotationwise-comments',
		'uses' => 'VendorRfqRequestController@quotationWiseComments',
	]);
	
	Route::post('/add-quotation-comments', [
		'as' => 'add-quotation-comments',
		'uses' => 'VendorRfqRequestController@addQuotationComments',
	]);
	Route::get('/rfq-rejected',[
	    'as' => 'ven-rfq-rejected', 
		'uses' => 'VendorRfqRequestController@rejectedrfq'
		]);
//Vendor Rfq Section End


	
	// Route::get('/purchase-order-list',[
	// 	'as' => 'purchase-order-listing',
	//     'uses' => 'VendorPurchaseOrderController@index'
	//     ]);

	// Route::get('/all-git-list', 'VendorGitController@viewGit');

	// Route::get('/all-grt-list', 'VendorGitController@viewGrt');

//Vendor Purchase Order Section End
    Route::get('/purchase-order-list',[
        'as' => 'purchase-order-listing',
        'uses' => 'VendorPurchaseOrderController@purchaseOrderlistview'
        ]);

	Route::get('/filter-purchase-order', [
		'as' => 'filter-purchase-ordering',
		'uses' => 'VendorPurchaseOrderController@filterPurchaseOrder'
	]);

	Route::get('/vendor-po-list', [
		'as' => 'vendor-po-list',
		'uses' => 'VendorPurchaseOrderController@getPoList'
	]);

	Route::get('/vendor-design-list', [
		'as' => 'vendor-design-list',
		'uses' => 'VendorPurchaseOrderController@getDesignList'
	]);

	Route::get('/vendor-color-list', [
		'as' => 'vendor-color-list',
		'uses' => 'VendorPurchaseOrderController@getColorList'
	]);
	
	Route::get('/vendor-category3-list', [
		'as' => 'vendor-category3-list',
		'uses' => 'VendorPurchaseOrderController@getCategory3List'
	]);
	
	Route::get('/vendor-width-size-list', [
		'as' => 'vendor-width-size-list',
		'uses' => 'VendorPurchaseOrderController@getWidthSizeList'
	]);

	
    Route::post('/purchase-order-request',[
        'as' => 'purchase-order-request',
        'uses' => 'VendorPurchaseOrderController@purchaseOrderRequest'
    ]);

    Route::get('/archieve-purchase-order-list',[
        'as' => 'archieve-purchase-order-listing',
        'uses' => 'VendorPurchaseOrderController@archievePOlistview'
    ]);

    Route::get('/vendor-archive-po-list', [
		'as' => 'vendor-archive-po-list',
		'uses' => 'VendorPurchaseOrderController@getArchvePoList'
	]);

	Route::get('/vendor-archive-design-list', [
		'as' => 'vendor-archive-design-list',
		'uses' => 'VendorPurchaseOrderController@getArchveDesignList'
	]);

	Route::get('/vendor-archive-color-list', [
		'as' => 'vendor-archive-color-list',
		'uses' => 'VendorPurchaseOrderController@getArchveColorList'
	]);
	
	Route::get('/vendor-archive-category3-list', [
		'as' => 'vendor-archive-category3-list',
		'uses' => 'VendorPurchaseOrderController@getArchveCategory3List'
	]);
	
	Route::get('/vendor-archive-width-size-list', [
		'as' => 'vendor-archive-width-size-list',
		'uses' => 'VendorPurchaseOrderController@getArchveWidthSizeList'
	]);

    Route::get('/purchase-archieve-order-filter',[
        'as' => 'purchase-archieve-order-filter',
        'uses' => 'VendorPurchaseOrderController@filterArchievePurchaseOrder'
    ]);
//Vendor Purchase Order Section End


//Vendor Git Section End    
    Route::get('/all-git-list',[
         'as' => 'git-lsting-vendor',
         'uses' => 'VendorGitController@viewAllVendorGit'
         ]);
	Route::get('/add-git',[
		'as' => 'add-git-vendor',
	    'uses' => 'VendorGitController@viewAddVendorGit'
	    ]);
	Route::post('/submit-git', [
		'as' => 'submit-git-records',
		'uses' => 'VendorGitController@submitGitRecords',
	]);
	Route::get('/view-invoice/{per_git_id}',[
    	'as' => 'view-git-invoice',
        'uses' => 'VendorGitController@generateInvoice'
    ]);
	
	Route::get('/download-invoice/{per_git_id}',[
    	'as' => 'download-git-invoice',
        'uses' => 'VendorGitController@downloadInvoice'
    ]);
	
	Route::get('/update-git',[
    	'as' => 'update-git-records',
        'uses' => 'VendorGitController@updateGitRecords'
    ]);
	
	Route::get('/fetch-git-records',[
    	'as' => 'fetch-git-records',
        'uses' => 'VendorGitController@fetchGitRecords'
    ]);
	
	Route::post('/resolve-git',[
		'as' => 'resolve-git-records',
		'uses' => 'VendorGitController@resolveGitRecords',
	]);
	
	Route::get('/filter-add-git',[
		'as' => 'filter-add-git',
		'uses' => 'VendorGitController@filterAddGit',
	]);

	Route::get('/git_comments/{git_id}', [
		'as' => 'gitwise-comments',
		'uses' => 'VendorGitController@gitWiseComments',
	]);
	Route::post('/add-git-comments', [
		'as' => 'add-git-comments',
		'uses' => 'VendorGitController@addGitComments',
	]);

	
	Route::get('/get-vendor-git-cat1', [
		'as' => 'get-vendor-git-cat1',
		'uses' => 'VendorGitController@getVendorCat1',
	]);
	
	Route::get('/get-vendor-git-cat2', [
		'as' => 'get-vendor-git-cat2',
		'uses' => 'VendorGitController@getVendorCat2',
	]);
	
	Route::get('/chalan-no-availability', [
		'as' => 'chalan-no-availability',
		'uses' => 'VendorGitController@chalanNoAvailability',
	]);
	
	Route::get('/get-vendor-git-cat3', [
		'as' => 'get-vendor-git-cat3',
		'uses' => 'VendorGitController@getVendorCat3',
	]);

	Route::get('/get-vendor-git-cat4', [
		'as' => 'get-vendor-git-cat4',
		'uses' => 'VendorGitController@getVendorCat4',
	]);
	Route::get('/filter-vendor-git', [
		'as' => 'filter-vendor-git',
		'uses' => 'VendorGitController@filterVendorGit',
	]);

	Route::get('/all-git-archive-list',[
         'as' => 'git-archive-lsting-vendor',
         'uses' => 'VendorGitController@viewArchiveVendorGit'
         ]);
	Route::get('/filter-vendor-git-archive', [
		'as' => 'filter-vendor-git-archive',
		'uses' => 'VendorGitController@filterArchiveVendorGit',
	]);
	Route::get('/get-vendor-archive-git-cat1', [
		'as' => 'get-vendor-archive-git-cat1',
		'uses' => 'VendorGitController@getVendorArchiveCat1',
	]);
	Route::get('/get-vendor-archive-git-cat2', [
		'as' => 'get-vendor-archive-git-cat2',
		'uses' => 'VendorGitController@getVendorArchiveCat2',
	]);
	Route::get('/get-vendor-archive-git-cat3', [
		'as' => 'get-vendor-archive-git-cat3',
		'uses' => 'VendorGitController@getVendorArchiveCat3',
	]);
	Route::get('/get-vendor-archive-git-cat4', [
		'as' => 'get-vendor-archive-git-cat4',
		'uses' => 'VendorGitController@getVendorArchiveCat4',
	]);
	//Vendor Git Section End	
	Route::get('/request-comments/{req_id}', [
		'as' => 'requestwise-comments',
		'uses' => 'VendorRequestController@requestWiseComments',
	]);
	Route::post('/add-request-comments', [
		'as' => 'add-request-comments',
		'uses' => 'VendorRequestController@addRequestComments',
	]);

	Route::get('/vendor-filter-action',[
		'as' => 'vendor-filter-action',
		'uses' => 'VendorRequestController@filterAction',
	]);

    Route::get('/all-request',[
    	'as' => 'all-requst-vendor',
        'uses' => 'VendorRequestController@viewAllRequest'
        ]);

    Route::get('/vendor-filter-all-request',[
		'as' => 'vendor-filter-all-request',
		'uses' => 'VendorRequestController@filterAllRequest',
	]);

    Route::get('/all-widthsampleothers-request',[
    	'as' => 'all-requst-widthsampleothers',
        'uses' => 'VendorRequestController@viewAllcuttingsampleothersRequest'
        ]);
    Route::get('/all-widthratechange-request',[
    	'as' => 'all-requst-widthratechange',
        'uses' => 'VendorRequestController@viewAllwidthrateRequest'
        ]);
    Route::get('/vendor-filter-widthratechange',[
		'as' => 'vendor-filter-widthratechange',
		'uses' => 'VendorRequestController@widthrateRequestFilter',
	]);

	Route::get('/all-dateext-request',[
    	'as' => 'all-requst-dateext',
        'uses' => 'VendorRequestController@viewAlldateextRequest'
        ]);
    Route::get('/vendor-filter-dateext',[
		'as' => 'vendor-filter-dateext',
		'uses' => 'VendorRequestController@dateextRequestFilter',
	]);

    Route::post('/vendor-accept-request',[
    	'as' => 'vendor-accept-request',
        'uses' => 'VendorRequestController@acceptRequest'
    ]);
    
    Route::get('/vendor-notify-request/{id}',[
    	'as' => 'vendor-notify-request',
        'uses' => 'VendorRequestController@notifyToAdmin'
    ]);

    Route::get('/vendor-reject-request/{id}',[
    	'as' => 'vendor-reject-request',
        'uses' => 'VendorRequestController@rejectRequest'
    ]);

    Route::get('/sample-request',[
	    'as' => 'sample-request', 
		'uses' => 'VendorRequestController@sampleRequest'
		]);
		
	Route::get('/filter-sample',[
	    'as' => 'filter-sample-request', 
		'uses' => 'VendorRequestController@filterSampleRequest'
		]);
		
		
    Route::get('/rate-changes',[
    	'as' => 'rate-changing-vendor',
        'uses' => 'VendorRequestController@viewRateChange'
        ]);
		
	 Route::get('/filter-rate',[
    	'as' => 'filter-rate-changes',
        'uses' => 'VendorRequestController@filterRateChange'
        ]);
		
		
    Route::get('/request-cutting',[
    	'as' => 'cutting-req',
        'uses' => 'VendorRequestController@viewRequestCutting'
        ]);
	
	 Route::get('/filter-cutting',[
    	'as' => 'filter-cutting-req',
        'uses' => 'VendorRequestController@filterRequestCutting'
        ]);
		
    Route::get('/width-changes',[
    	'as' => 'width-changing',
        'uses' => 'VendorRequestController@viewWidthChanges'
        ]);
		
	Route::get('/filter-width',[
    	'as' => 'filter-width-changes',
        'uses' => 'VendorRequestController@filterWidthChanges'
        ]);
		
		
    Route::get('/date-extention',[
    	'as' => 'date-extention-req',
        'uses' => 'VendorRequestController@viewVendorDateExt'
        ]);
		
	Route::get('/filter-date',[
    	'as' => 'filter-date-extention',
        'uses' => 'VendorRequestController@filterVendorDateExt'
        ]);
		
    Route::get('/calcellation',[
    	'as' => 'calcellation-req',
        'uses' => 'VendorRequestController@viewVendorCalcellation'
        ]);
		
	 Route::get('/filter-calcellation',[
    	'as' => 'filter-calcellation-req',
        'uses' => 'VendorRequestController@filterVendorCalcellation'
        ]);
		
		
    Route::get('/create-support-ticket',[
    	'as' => 'support-ticket-req',
        'uses' => 'VendorRequestController@viewVendorcreateSupportTicket'
    ]);

    Route::get('/filter-others',[
    	'as' => 'filter-create-support',
        'uses' => 'VendorRequestController@filterCreateSupportTicket'
        ]);

    Route::post('/add-other-request',[
		'as' => 'add-other-request',
		'uses' => 'VendorRequestController@addSupportRequest',
	]);

    Route::get('/goods-receive',[
    	'as' => 'goods-receive-list',
        'uses' => 'GoodsReturnController@goodsReceiveList'
    ]);
    Route::get('/goods-closed',[
    	'as' => 'goods-closed',
        'uses' => 'GoodsReturnController@goodsVendorClosedList'
    ]);

    Route::post('/goods-receive',[
    	'as' => 'goods-received-add',
        'uses' => 'GoodsReturnController@goodsReceiveAccept'
    ]);
    Route::get('/filter-vendor-goods-return',[
		'as' => 'filter-vendor-goods-return',
	    'uses' => 'GoodsReturnController@filterVendorGoodsReturn'
	]);
    Route::get('/filter-vendor-closed-return',[
		'as' => 'filter-vendor-closed-return',
	    'uses' => 'GoodsReturnController@filterVendorclosedReturn'
	]);
	Route::get('/get-vendor-goods-cat1',[
		'as'=>'get-vendor-goods-cat1',
		'uses'=>'GoodsReturnController@getVendorCat1'
	]);
	Route::get('/get-vendor-goods-cat2',[
		'as'=>'get-vendor-goods-cat2',
		'uses'=>'GoodsReturnController@getVendorCat2'
	]);



	/* Document Upload*/
	Route::get('/upload-document',[
		'as'=>'upload-document',
		'uses'=>'UploadDocumentController@index'
		]);
	
	Route::post('/insert-document',[
		'as'=>'insert-document',
		'uses'=>'UploadDocumentController@insertDocument'
		]);


    Route::get('/price-list',[
		'as' => 'vendor-price-list',
	    'uses' => 'PriceController@vendorPriceList'
	]);
    
    // Route::get('/request-price-change/{id}',[
    // 	'as' => 'request-price-change',
    //     'uses' => 'PriceController@requestforPricechangeToAdmin'
    // ]);

    Route::get('/request-price-change-vendor/{id}',[
    	'as' => 'request-price-change-vendor',
        'uses' => 'PriceController@vendorRequestforPricechange'
    ]);

    Route::post('/price-change-vendor',[
		'as' => 'price-change-vendor',
	    'uses' => 'PriceController@postRequestforPricechange'
	]);

	Route::post('/vendor-accept-nego-price',[
    	'as' => 'vendor-accept-nego-price',
        'uses' => 'PriceController@acceptNegotiatePrice'
    ]);
    
    Route::get('/vendor-reject-nego-price/{id}',[
    	'as' => 'vendor-reject-nego-price',
        'uses' => 'PriceController@rejectNegotiatePrice'
    ]);

    Route::get('/reports',[
    	'as' => 'reports-req',
        'uses' => 'VendorReportsController@viewAllReports'
        ]);
    Route::get('/faq',[
    	'as' => 'faq-req',
        'uses' => 'VendorFaqController@vendorfaq'
        ]);
   
	Route::get('/edit-profile',[
		'as' => 'ed-profile-req',
	    'uses' => 'AdminVendorController@editProfile'
	    ]);

	Route::post('/update-profile',[
		'as' => 'upd-profile-req',
	    'uses' => 'AdminVendorController@updateProfile'
	    ]);

	Route::get('/change-password',[
		'as' => 'change-pass',
	    'uses' => 'AdminVendorController@editPassword'
	    ]);

	Route::post('/update-password',[
		'as' => 'updt-password',
	    'uses' => 'AdminVendorController@updatePassword'
	    ]);

	Route::get('/all-notification',[
		'as' => 'all-notification',
	    'uses' => 'VendorDashboardController@allNotification'
	    ]);
		
	Route::get('/read-notification',[
    	'as' => 'read-notification',
        'uses' => 'VendorDashboardController@readNotification'
    ]);
    Route::get('/document-listing',[
		'as'=>'document-listing',
		'uses'=>'UploadDocumentController@vendorDocumentList'
		]);
    Route::get('/get-document-month-vendor',[
		'as' => 'get-document-month-vendor',
	    'uses' => 'UploadDocumentController@getDocumentMonthVendor'
	]);
	Route::get('/get-document-type-vendor',[
			'as' => 'get-document-type-vendor',
		    'uses' => 'UploadDocumentController@getDocumentTypeVendor'
		]);
	
	Route::get('/filter-document-return-vendor',[
			'as' => 'filter-document-return-vendor',
		    'uses' => 'UploadDocumentController@filterDocumentReturnVendor'
		]);



});
		
		
	Route::get('/vendor-comments',[
		'uses' => 'VendorCommentsController@index'
		]);
		
	Route::get('/vendor-all-request',[
	    'uses' => 'VendorPurchaseRequestController@index'
	    ]);
		
		
	Route::get('/vendir-details', [
    'as'=>'vendir-details',
    'uses'=>'VendorInformationController@syncVendorDetails'
]);
		
Route::get('/product-sync', [
    'as'=>'product-sync',
    'uses'=>'ProductSyncController@getProduct'
]);	

//});
