
$(document).on('click', '.selectProductSample', function(e) {
	 
	 var product_id = $(this).attr('product_id');
	 var img = $('.selectProductSampleClassImage'+product_id).attr('src');
	 var cat1 = $('.selectProductSampleClassCat1'+product_id).text();
	 var cat2 = $('.selectProductSampleClassCat2'+product_id).text();
	 var cat3 = $('.selectProductSampleClassCat3'+product_id).text();
	 var cat4 = $('.selectProductSampleClassCat4'+product_id).text();
	 
	 $("#product_id").val(product_id);
	 $("#afterCreateImage").attr("src",img);
	 $("#afterCreateCat1").text(cat1);
	 $("#afterCreateCat2").text(cat2);
	 $("#afterCreateCat3").text(cat3);
	 $("#afterCreateCat4").text(cat4);
	 
	 
	 $(".beforeCreate").hide();
	 $(".afterCreate").show();
	 $(".shipmentDetails").show();
	 $("#samplepro").modal('hide');
});


$(document).on('click', '.selectProductExist', function(e) {
	 var product_id = $(this).attr('product_id');
	 var img = $('.selectProductExistClassImage'+product_id).attr('src');
	 var cat1 = $('.selectProductExistClassCat1'+product_id).text();
	 var cat2 = $('.selectProductExistClassCat2'+product_id).text();
	 var cat3 = $('.selectProductExistClassCat3'+product_id).text();
	 var cat4 = $('.selectProductExistClassCat4'+product_id).text();
	 
	 $("#product_id").val(product_id);
	 $("#afterCreateImage").attr("src",img);
	 $("#afterCreateCat1").text(cat1);
	 $("#afterCreateCat2").text(cat2);
	 $("#afterCreateCat3").text(cat3);
	 $("#afterCreateCat4").text(cat4);
	 
	// $("#afterCreateCat1hidden").val(cat1);
    // $("#afterCreateCat2hidden").val(cat2);
	 
	 
	 $(".beforeCreate").hide();
	 $(".afterCreate").show();
	 $(".shipmentDetails").show();
     $("#selectpro").modal('hide');
});

