//For set rfq_id in Add Quotation Modal
$('.addQuotationClass').click(function(){
		 $("#req_price_message").hide();
	 	 $("#req_width_message").hide();
		 var rfq_id = $(this).attr('rfq_id');
		 var rfq_details_id = $(this).attr('rfq_details_id');
		 var rfq_no = $(this).attr('rfq_no');
		 var width_size_required = $(this).attr('width_size_required');
		 
		 //alert(rfq_id);
		 
		 $('.emptyValue').val(''); 
		 $('#rfq_id').val(rfq_id); 
		 $('#rfq_details_id').val(rfq_details_id); 
		 $('.rfq_id').text(rfq_no); 
		 $('#vendorChangewidthisfabric').val(width_size_required);	 
		 
});

$('.addQuotationClassClose').click(function(){
	alert('Quotation already Closed!');
		 
});


$('.submitQuotation').click(function(){
	
	var rfq_id=$("#rfq_id").val();
	var rfq_details_id=$("#rfq_details_id").val();
	//alert(rfq_details_id);

	
  	var req_price=$("#req_price").val(); 
    if(req_price=='')
		 $("#req_price_message").text('Please enter Price!').css('color', 'red').show();
    else if(req_price<=0)
		 $("#req_price_message").text('You have enter invalid Price!').css('color', 'red').show();
	else 
		 $("#req_price_message").hide();
		 

    var req_width=$("#req_width").val();    
	var width_size_required=$("#vendorChangewidthisfabric").val();
    if(width_size_required=='1' && req_width=='')
		 $("#req_width_message").text('Please enter New Width!').css('color', 'red').show();
	else if(width_size_required!='1' && req_width=='')
		 $("#req_width").val('NA');
	else 
		 $("#req_width_message").hide();

  //   if(req_width=='')
		// $("#req_width_message").text('Please enter Width!').css('color', 'red').show();
		// if(req_width=='')
		// $("#req_width").val('NA');
		
	//else if(req_width<=0)
		//$("#req_width_message").text('You have enter invalid Width!').css('color', 'red').show();
  //   else
		// $("#req_width_message").hide();
	if(req_price>0 && req_width!=0)
	{
		$.ajax({
			type: 'POST', 
			url: q_url,
			data: {rfq_id:rfq_id,rfq_details_id:rfq_details_id,rfq_width:req_width,rfq_price:req_price, _token:token},
			success:function(data)
			{
				console.log(data);
				    $('.clearfix').text('');
					if(data==1)
					{
        				$('.modal-body').html('Quotation added successfully!');
						 window.location.reload();
					}
					else
						$('.clearfix').text('Please Try again!');
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});		
	}
  
});