


//For Accept Modal
//$('.vendorRequestaccept').click(function(){
	$(document).on('click', '.vendorRequestaccept', function(e) {	
	//alert(1);
		 var unique_key = $(this).attr('unique-key');
		 var action_id = $(this).attr('action_id');
		 var type = $(this).attr('action_type');
		 var price = $(this).attr('old_value');
		 var request_rate = $(this).attr('req_rate');
		 var old_width = $(this).attr('old_width');
		 var request_width = $(this).attr('req_width');
		 var pen_qty = $(this).attr('pen_qty');
		 var can_qty = $(this).attr('can_qty');
		 var cat_1_details = $(this).attr('cat_1_details');
		 var cat_2_details = $(this).attr('cat_2_details');
		 var podue_date = $(this).attr('due_date');
		 var extend_date = $(this).attr('extd_date');
		 var extend_reason = $(this).attr('reason');

		 //alert(type);
		 //Fetch All Records/
		 var po_no=  $('.po-no'+unique_key).text();
		 var po_date=  $('.po-date'+unique_key).text();
		 var due_date=  $('.due-date'+unique_key).text();
		 var cat_1=  $('.cat-1'+unique_key).text();
		 var cat_2=  $('.cat-2'+unique_key).text();
		 var cat_3=  $('.cat-3'+unique_key).text();
		 var cat_4=  $('.cat-4'+unique_key).text();
		 //var price=  $('.price'+unique_key).text(); 
		 var order_qty=  $('.order-qty'+unique_key).text(); 
		 //var request_rate=  $('.received-qty'+unique_key).text(); 
		 var cancelled_qty=  $('.cancelled-qty'+unique_key).text(); 
		 var git_qty=  $('.git-qty'+unique_key).text();
		 var pending_qty=  $('.pending-qty'+unique_key).text();
		 
		 //Set All Records in Modal		 
		 $('.vendorRequestaccept-cat1').text(cat_1); 
		 $('.vendorRequestaccept-cat2').text(cat_2); 
		 $('.vendorRequestaccept-cat4').text(cat_4); 
		 $('.vendorRequestaccept-po-no').text(po_no);
		 $('.vendorRequestaccept-date').text(po_date);
		 $('.vendorRequestaccept-order-qty').text(order_qty); 
		 $('.vendorRequestaccept-request-rate').text(request_rate); 
		 $('.vendorRequestaccept-git-qty').text(git_qty); 
		 $('.vendorRequestaccept-old-rate').text(price); 
		 $('.vendorRequestaccept-old-width').text(old_width);
		 $('.vendorRequestaccept-reqes-width').text(request_width);
		 $('.vendorRequestaccept-pen-qty').text(pen_qty);
		 $('.vendorRequestaccept-can-qty').text(can_qty);
		 $('.vendorRequestdetails-cat_1_details').text(cat_1_details);
		 $('.vendorRequestdetails-cat_2_details').text(cat_2_details);
		 $('.vendorRequestdetails-cat_4_details').text(old_width);
		 $('.vendorRequestdetails-due-date').text(podue_date);
		 $('.vendorRequestdetails-ext-date').text(extend_date);
		 $('.vendorRequestdetails-ext-date-reason').text(extend_reason);
		 $('.vendorRequestdetails-cancel-reason').text(extend_reason);
		 $('.vendorRequestaccept-due-date').text(podue_date);

		 $('#action_id').val(action_id);
		 $('#type').val(type);
		 var actiontype='';
		 if(type=='rate_change')
		 {
		 	var actiontype='Rate Change';
		 }
		 if(type=='cancel')
		 {
		 	var actiontype='Cancellation';
		 }
		 if(type=='width_change')
		 {
		 	var actiontype='Width Change';
		 }
		 if(type=='date_ext')
		 {
		 	var actiontype='Due Date Extention';
		 }
		 if(type=='cutting')
		 {
		 	var actiontype='Cutting';
		 }
		 if(type=='others')
		 {
		 	var actiontype='Others';
		 }
		 if(type=='sample')
		 {
		 	var actiontype='Inspection';
		 }
		 $('.vendorRequestaccept-act_type').text(actiontype);
		 $('#accept').show();
		 $('#vendorRequestaccept_new_requested_rate').val(request_rate);
		 $('#old-rate').show();
		 $('#reqst-rate').show(); 
		 $('#old-width').show(); 
		 $('#reqes-width').show(); 

		 if((type != 'width_change')){
		 $('#old-width').hide(); 
		 $('#reqes-width').hide(); 
		}

		 if((type != 'width_change') && (type != 'rate_change')){
		 //alert(1);
		 $('#accept').hide();
		 $('#old-rate').hide();
		 $('#reqst-rate').hide();   
		}
        $('#newpo').show(); 
        if((type != 'width_change') && (type != 'rate_change') && (type != 'cutting')){
		 //alert(1);
		 $('#newpo').hide(); 
		}
		$('#docet').hide(); 
		$('#prnno').hide(); 
		$('#prndate').hide(); 
		$('#courier').hide(); 
		if(type == 'cutting'){
		 //alert(1);
		$('#docet').show(); 
		$('#courier').show(); 
		}

		$('#pen-qty').hide(); 
		$('#can-qty').hide(); 
		$('#vendorRequestdetails-cancel-reason').hide(); 
		$('#vendorRequestaccept-due-date').hide();
		if(type == 'cancel'){
		 //alert(1);
		$('#pen-qty').show(); 
		$('#can-qty').show(); 
		$('#vendorRequestdetails-cancel-reason').show();
		$('#vendorRequestaccept-due-date').show();
		}

		$('#vendorRequestdetails-due-date').hide(); 
		$('#vendorRequestdetails-ext-date').hide(); 
		$('#vendorRequestdetails-ext-date-reason').hide(); 
		if(type == 'date_ext'){
		 //alert(1);
		$('#vendorRequestdetails-due-date').show(); 
		$('#vendorRequestdetails-ext-date').show(); 
		$('#vendorRequestdetails-ext-date-reason').show(); 
		}
		 
});
$('.submitvendorRequestaccept').click(function(){
	var reqst_val=$(".vendorRequestaccept-request-rate").text();
	var action_id = $('#action_id').val();
    var po_no=$(".vendorRequestaccept-po-no").text(); 
	var type=$("#type").val();  		
  	var reason=$("#vendorRequestaccept_reason").val(); 
  	var accept_val=$("#vendorRequestaccept_new_requested_rate").val();
  	var new_po=$("#vendorRequestaccept_new_po").val(); 
  	var docket_no=$("#vendorRequestaccept_new_docket").val();
  	//var prn_no=$("#vendorRequestaccept_new_prn").val();
  	//var prn_date=$("#vendorRequestaccept_new_prndate").val();
  	var courier_no=$("#vendorRequestaccept_new_courier").val();
    //alert(accept_val);
	if((type=='width_change' || type=='rate_change') && accept_val==''){
		 $("#vendorRequestaccept_rate-remarks_message").text('Please enter Rate!').css('color', 'red').show();
	return false;
	}
	else 
		 $("#vendorRequestaccept_rate-remarks_message").hide();
    
    if((type=='cutting') && docket_no==''){
		 $("#vendorRequestaccept_docket-remarks_message").text('Please enter Docket No!').css('color', 'red').show();
	return false;
	}
	else 
		 $("#vendorRequestaccept_docket-remarks_message").hide();
	// if((type=='cutting') && prn_no==''){
	// 	 $("#vendorRequestaccept_prn-remarks_message").text('Please enter PRN No!').css('color', 'red').show();
	// return false;
	// }
	// else 
	// 	 $("#vendorRequestaccept_prn-remarks_message").hide();
	// if((type=='cutting') && prn_date==''){
	// 	 $("#vendorRequestaccept_prndate-remarks_message").text('Please enter Date!').css('color', 'red').show();
	// return false;
	// }
	// else 
	// 	 $("#vendorRequestaccept_prndate-remarks_message").hide();
	if((type=='cutting') && courier_no==''){
		 $("#vendorRequestaccept_courier-remarks_message").text('Please enter Courier Name!').css('color', 'red').show();
	return false;
	}
	else 
		 $("#vendorRequestaccept_courier-remarks_message").hide();
 
	
		$.ajax({
			type: 'POST', 
			url: cutting_url,
			data: {po_no:po_no,type:type,new_po:new_po,docket_no:docket_no,courier_no:courier_no,reason:reason,accept_val:accept_val,reqst_val:reqst_val,action_id:action_id,_token:ptoken},
			success:function(data)
			{
				    
					if(data==1)
					{
        				$('.vendorRequestaccept-modal-body').html('Request has been accepted successfully!');
						 window.location.reload();
					}
					else
						$('.vendorRequestaccept-clearfix').text('Please Try again!');
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});	
	
});



//For Reject Modal
//$('.vendorRequestreject').click(function(){
	$(document).on('click', '.vendorRequestreject', function(e) {	
	//alert(1);
		 var unique_key = $(this).attr('unique-key');
		 var action_id = $(this).attr('action_id');
		 var type = $(this).attr('action_type');
		 var price = $(this).attr('old_value');
		 var request_rate = $(this).attr('req_rate');
		 var old_width = $(this).attr('old_width');
		 var request_width = $(this).attr('req_width');
		 var pen_qty = $(this).attr('pen_qty');
		 var can_qty = $(this).attr('can_qty');
		 var cat_1_details = $(this).attr('cat_1_details');
		 var cat_2_details = $(this).attr('cat_2_details');
		 var podue_date = $(this).attr('due_date');
		 var extend_date = $(this).attr('extd_date');
		 var extend_reason = $(this).attr('reason');
		 //alert(type);
		 //Fetch All Records
		 var po_no=  $('.po-no'+unique_key).text();
		 var po_date=  $('.po-date'+unique_key).text();
		 var due_date=  $('.due-date'+unique_key).text();
		 var cat_1=  $('.cat-1'+unique_key).text();
		 var cat_2=  $('.cat-2'+unique_key).text();
		 var cat_3=  $('.cat-3'+unique_key).text();
		 var cat_4=  $('.cat-4'+unique_key).text();
		 //var price=  $('.price'+unique_key).text(); 
		 var order_qty=  $('.order-qty'+unique_key).text(); 
		 var received_qty=  $('.received-qty'+unique_key).text(); 
		 var cancelled_qty=  $('.cancelled-qty'+unique_key).text(); 
		 var git_qty=  $('.git-qty'+unique_key).text();
		 var pending_qty=  $('.pending-qty'+unique_key).text();
		 
		 //Set All Records in Modal		 
		 $('.vendorRequestreject-cat1').text(cat_1); 
		 $('.vendorRequestreject-cat2').text(cat_2); 
		 $('.vendorRequestreject-cat4').text(cat_4); 
		 $('.vendorRequestreject-po-no').text(po_no);
		 $('.vendorRequestreject-date').text(po_date);
		 $('.vendorRequestreject-order-qty').text(order_qty); 
		 $('.vendorRequestreject-received-qty').text(received_qty); 
		 $('.vendorRequestreject-git-qty').text(git_qty);
		 $('.vendorRequestreject-old-rate').text(price); 
		 $('.vendorRequestreject-old-width').text(old_width);
		 $('.vendorRequestreject-reqes-width').text(request_width);
		 $('.vendorRequestreject-pen-qty').text(pen_qty);
		 $('.vendorRequestreject-can-qty').text(can_qty);
		 $('.vendorReqreject-cat_1_details').text(cat_1_details);
		 $('.vendorReqreject-cat_2_details').text(cat_2_details);
		 $('.vendorReqreject-cat_4_details').text(old_width);
		 $('.vendorReqreject-due-date').text(podue_date);
		 $('.vendorReqreject-ext-date').text(extend_date);
		 $('.vendorReqreject-ext-date-reason').text(extend_reason);
		 $('.vendorReqreject-cancel-reason').text(extend_reason);
		 $('.vendorReqreject-podue-date').text(podue_date);


		 $('#action_id').val(action_id);
		 $('#type').val(type);
		 $('#accept').show();
		 $('.vendorRequestreject-request-rate').text(request_rate); 
		 $('#reject-old-rate').show();
		 $('#reject-reqst-rate').show(); 
		 $('#reject-old-width').show(); 
		 $('#reject-reqes-width').show(); 

		 if((type != 'width_change')){
		 $('#reject-old-width').hide(); 
		 $('#reject-reqes-width').hide(); 
		}

		 if((type != 'width_change') && (type != 'rate_change')){
		 //alert(1);
		 $('#accept').hide();
		 $('#reject-old-rate').hide();
		 $('#reject-reqst-rate').hide();   
		}
        

		$('#reject-pen-qty').hide(); 
		$('#reject-can-qty').hide(); 
		$('#vendorReqreject-cancel-reason').hide(); 
		$('#vendorReqreject-podue-date').hide(); 
		if(type == 'cancel'){
		 //alert(1);
		$('#reject-pen-qty').show(); 
		$('#reject-can-qty').show(); 
		$('#vendorReqreject-cancel-reason').show();
		$('#vendorReqreject-podue-date').show();
		}

		$('#vendorReqreject-due-date').hide(); 
		$('#vendorReqreject-ext-date').hide(); 
		$('#vendorReqreject-ext-date-reason').hide(); 
		if(type == 'date_ext'){
		 //alert(1);
		$('#vendorReqreject-due-date').show(); 
		$('#vendorReqreject-ext-date').show(); 
		$('#vendorReqreject-ext-date-reason').show(); 
		}
		 
});
$('.submitvendorRequestreject').click(function(){
	var action_id = $('#action_id').val();
    var po_no=$(".vendorRequestreject-po-no").text(); 
	var type=$("#type").val();  		
  	var reason=$("#vendorRequestreject_reason").val(); 
  
    
    if(reason=='')
		 $("#vendorRequestreject_remarks_message").text('Please enter Reason!').css('color', 'red').show();
	else 
		 $("#vendorRequestreject_remarks_message").hide();
		 
	if(reason!=''){
		$.ajax({
			type: 'POST', 
			url: rej_url,
			data: {po_no:po_no,type:type,reason:reason,action_id:action_id,_token:ptoken},
			success:function(data)
			{
				    
					if(data==1)
					{
        				$('.vendorRequestreject-modal-body').html('Request has been rejected successfully!');
						 window.location.reload();
					}
					else
						$('.vendorRequestreject-clearfix').text('Please Try again!');
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});	
	}
});


//For Close Modal
//$('.vendorRequestclose').click(function(){
	$(document).on('click', '.vendorRequestclose', function(e) {
	    //alert(1);
		 var unique_key = $(this).attr('unique-key');
		 var action_id = $(this).attr('action_id');
		 var type = $(this).attr('action_type');
		 var price = $(this).attr('old_value');
		 var request_rate = $(this).attr('req_rate');
		 var old_width = $(this).attr('old_width');
		 var request_width = $(this).attr('req_width');
		 //alert(type);
		 //Fetch All Records
		 var po_no=  $('.po-no'+unique_key).text();
		 var po_date=  $('.po-date'+unique_key).text();
		 var due_date=  $('.due-date'+unique_key).text();
		 var cat_1=  $('.cat-1'+unique_key).text();
		 var cat_2=  $('.cat-2'+unique_key).text();
		 var cat_3=  $('.cat-3'+unique_key).text();
		 var cat_4=  $('.cat-4'+unique_key).text();
		 //var price=  $('.price'+unique_key).text(); 
		 var order_qty=  $('.order-qty'+unique_key).text(); 
		 //var request_rate=  $('.received-qty'+unique_key).text(); 
		 var cancelled_qty=  $('.cancelled-qty'+unique_key).text(); 
		 var git_qty=  $('.git-qty'+unique_key).text();
		 var pending_qty=  $('.pending-qty'+unique_key).text();
		 
		 //Set All Records in Modal		 
		 $('.vendorRequestclose-cat1').text(cat_1); 
		 $('.vendorRequestclose-cat2').text(cat_2); 
		 $('.vendorRequestclose-cat4').text(cat_4); 
		 $('.vendorRequestclose-po-no').text(po_no);
		 $('.vendorRequestclose-date').text(po_date);
		 $('.vendorRequestclose-order-qty').text(order_qty); 
		 $('.vendorRequestclose-request-rate').text(request_rate); 
		 $('.vendorRequestclose-git-qty').text(git_qty); 
		 $('.vendorRequestclose-old-rate').text(price); 
		 $('.vendorRequestclose-old-width').text(old_width);
		 $('.vendorRequestclose-reqes-width').text(request_width);
		 $('#action_id').val(action_id);
		 $('#type').val(type);
		 $('#accept').show();
		 $('#vendorRequestaccept_new_requested_rate').val(request_rate);
		 $('#close-old-rate').show();
		 $('#close-reqst-rate').show(); 
		 $('#old-width').show(); 
		 $('#reqes-width').show(); 

		 if((type != 'width_change')){
		 $('#old-width').hide(); 
		 $('#reqes-width').hide(); 
		}

		 if((type != 'width_change') && (type != 'rate_change')){
		 //alert(1);
		 $('#accept').hide();
		 $('#close-old-rate').hide();
		 $('#close-reqst-rate').hide();   
		}
        $('#newpo').show(); 
        
});
$('.submitvendorRequestclose').click(function(){
	
	var action_id = $('#action_id').val();
 //    var po_no=$(".vendorRequestclose-po-no").text(); 
	// var type=$("#type").val();
  	var new_po=$("#vendorRequestaccept_new_po").val(); 
    //alert(accept_val);
	if(new_po==''){
		 $("#vendorRequestclose_rate-remarks_message").text('Please enter new PO!').css('color', 'red').show();
	return false;
	}
	else 
		 $("#vendorRequestclose_rate-remarks_message").hide();
    
    
 
	
		$.ajax({
			type: 'POST', 
			url: close_url,
			data: {new_po:new_po,action_id:action_id,_token:ptoken},
			success:function(data)
			{
				    
					if(data==1)
					{
        				$('.vendorRequestclose-modal-body').html('Request has been closed successfully!');
						 window.location.reload();
					}
					else
						$('.vendorRequestclose-clearfix').text('Please Try again!');
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});	
	
});
////////////--------------------------//////////

