//For Vendor Change Cutting Modal
$('.vendorChangecutting').click(function(){
	
		 var unique_key = $(this).attr('unique-key');
		 
		 //Fetch All Records
		 var po_no=  $('.po-no'+unique_key).text();
		 var po_date=  $('.po-date'+unique_key).text();
		 var due_date=  $('.due-date'+unique_key).text();
		 var cat_1=  $('.cat-1'+unique_key).text();
		 var cat_2=  $('.cat-2'+unique_key).text();
		 var cat_3=  $('.cat-3'+unique_key).text();
		 var cat_4=  $('.cat-4'+unique_key).text();
		 var price=  $('.price'+unique_key).text(); 
		 var order_qty=  $('.order-qty'+unique_key).text(); 
		 var received_qty=  $('.received-qty'+unique_key).text(); 
		 var cancelled_qty=  $('.cancelled-qty'+unique_key).text(); 
		 var git_qty=  $('.git-qty'+unique_key).text();
		 var pending_qty=  $('.pending-qty'+unique_key).text();
		 var icode=  $('.ICODE'+unique_key).text();
		 
		 //Set All Records in Modal		 
		 $('.vendorChangecutting-cat1').text(cat_1);
		 if (cat_2!='')	{  
		 $('.vendorChangecutting-cat2').text(cat_2);
		 }
		 else{
		 $('.vendorChangecutting-cat2').text('NA');	
		 } 
		 if (cat_4!='')	{ 
		 $('.vendorChangecutting-cat4').text(cat_4);
		 }
		 else{
		 $('.vendorChangecutting-cat4').text('NA');	
		 }  
		 $('.vendorChangecutting-po-no').text(po_no); 
		 $('.vendorChangecutting-date').text(po_date); 
		 $('.vendorChangecutting-order-qty').text(order_qty); 
		 $('.vendorChangecutting-received-qty').text(received_qty); 
		 $('.vendorChangecutting-git-qty').text(git_qty); 
		 $('.vendorChangecutting-pending-qty').text(pending_qty); 
		 $('.vendorChangecutting-icode').text(icode); 
		 
});

$('.submitvendorChangecutting').click(function(){

	//$('.loading').show();
	var old_width=$('.vendorChangecutting-cat4').text();
	var order_date=$('.vendorChangecutting-date').text();
	var cat_1=$(".vendorChangecutting-cat1").text();
	var cat_2=$(".vendorChangecutting-cat2").text();
    var po_no=$(".vendorChangecutting-po-no").text(); 
	var type=$("#cutting_type").val();
  	var remarks=$("#vendorChangecutting_remarks").val();
  	var Icode=$(".vendorChangecutting-icode").text(); 
	//alert(old_width);
 //    if(remarks=='')
	// {
	// 	 $("#vendorChangecutting_remarks_message").text('Please enter Remarks!').css('color', 'red').show();
	// 	 return false;
	// }
	// else 
	// 	 $("#vendorChangecutting_remarks_message").hide();

		$.ajax({
			type: 'POST', 
			url: cutting_url,
			data: {po_no:po_no,old_width:old_width,Icode:Icode,type:type,order_date:order_date,cat_1:cat_1,cat_2:cat_2,remarks:remarks,_token:ptoken},
			success:function(data)
			{
					if(data==1)
					{
        				$('.vendorChangecutting-modal-body').html('Request added successfully!');
						 
						 setTimeout(function () {
						 //$('#vendorChangecutting').hide(); 	
                          //window.location.reload();
                           $('#vendorChangecutting').modal('toggle');
                          applyFilter();
                     }, 1000);
					}
					else
						$('.vendorChangecutting-clearfix').text('Please Try again!');
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});
  
});


//For Vendor Change Price Modal
$('.vendorChangeprice').click(function(){
	
		 var unique_key = $(this).attr('unique-key');
		 
		 //Fetch All Records
		 var po_no=  $('.po-no'+unique_key).text();
		 var po_date=  $('.po-date'+unique_key).text();
		 var due_date=  $('.due-date'+unique_key).text();
		 var cat_1=  $('.cat-1'+unique_key).text();
		 var cat_2=  $('.cat-2'+unique_key).text();
		 var cat_3=  $('.cat-3'+unique_key).text();
		 var cat_4=  $('.cat-4'+unique_key).text();
		 var price=  $('.price'+unique_key).text(); 
		 var order_qty=  $('.order-qty'+unique_key).text(); 
		 var received_qty=  $('.received-qty'+unique_key).text(); 
		 var cancelled_qty=  $('.cancelled-qty'+unique_key).text(); 
		 var git_qty=  $('.git-qty'+unique_key).text();
		 var pending_qty=  $('.pending-qty'+unique_key).text();
		 var icode=  $('.ICODE'+unique_key).text();
		
		 
		 //Set All Records in Modal	
		 if (cat_1!='')	{ 
		 $('.vendorChangeprice-cat1').text(cat_1);
         }		 
		 else{
		 $('.vendorChangeprice-cat1').text('NA');	
		 }
         if (cat_2!='')	{ 
		 $('.vendorChangeprice-cat2').text(cat_2);
		 }
		 else{
		 $('.vendorChangeprice-cat2').text('NA');	
		 } 
		 if (cat_4!='')	{
		 $('.vendorChangeprice-cat4').text(cat_4);
		 }
		 else{
		 $('.vendorChangeprice-cat4').text('NA');	
		 }  
		 $('.vendorChangeprice-po-no').text(po_no); 
		 $('.vendorChangeprice-order-qty').text(order_qty); 
		 $('.vendorChangeprice-received-qty').text(received_qty); 
		 $('.vendorChangeprice-git-qty').text(git_qty); 
		 $('.vendorChangeprice-pending-qty').text(pending_qty); 
		 $('.vendorChangeprice-current-rate').text(price); 
		 $('.vendorChangeprice-po-date').text(po_date); 
		 $('.vendorChangeprice-icode').text(icode);
		 
});

$('.submitvendorChangeprice').click(function(){
	var Icode=$(".vendorChangeprice-icode").text();
	var order_date=$('.vendorChangeprice-po-date').text();
	var cat_1=$('.vendorChangeprice-cat1').text();
	var cat_2=$('.vendorChangeprice-cat2').text();
	var old_rate=$('.vendorChangeprice-current-rate').text();
    var po_no=$(".vendorChangeprice-po-no").text(); 
	var type=$("#rate_change_type").val();
  	var new_rate=$("#vendorChangecutting_new_requested_rate").val(); 
  	var reason=$("#vendorChangecutting_reason").val();
  	var old_width=$('.vendorChangeprice-cat4').text();
  	 
	//alert(cat_1);
	if(new_rate=='')
		 $("#vendorChangeRate_remarks_message").text('Please enter New Rate !').css('color', 'red').show();
	else 
		 $("#vendorChangeRate_remarks_message").hide();
    if(reason=='')
		 $("#vendorChangeRateReason_remarks_message").text('Please enter Reason!').css('color', 'red').show();
	else 
		 $("#vendorChangeRateReason_remarks_message").hide();
	
	//alert(old_rate);	 
		 
	if(reason!='' && new_rate!=='')
	{
		$('.loading').show();

		$.ajax({
			type: 'POST', 
			url: cutting_url,
			data: {po_no:po_no,old_width:old_width,Icode:Icode,type:type,reason:reason,order_date:order_date,cat_1:cat_1,cat_2:cat_2,old_rate:old_rate,new_rate:new_rate,_token:ptoken},
			success:function(data)
			{
				    
					if(data==1)
					{
        				$('.vendorChangecutting-modal-body').html('Request added successfully!');
						 
						 setTimeout(function () {
						 //$('#vendorChangeprice').hide(); 
                          //window.location.reload();
                          
						  $('#vendorChangeprice').modal('toggle');
                           applyFilter();
                     }, 1000);
					}
					else
						$('.vendorChangecutting-clearfix').text('Please Try again!');
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});		
     }
  
});




//For Vendor Change Width Modal
//$('.vendorChangewidth').click(function(){
$(document).on('click', '.vendorChangewidth', function(e) {	
	
		 var unique_key = $(this).attr('unique-key');
		 
		 //Fetch All Records
		 var width_size_required = $(this).attr('width_size_required');
		 var po_no=  $('.po-no'+unique_key).text();
		 var po_date=  $('.po-date'+unique_key).text();
		 var due_date=  $('.due-date'+unique_key).text();
		 var cat_1=  $('.cat-1'+unique_key).text();
		 var cat_2=  $('.cat-2'+unique_key).text();
		 var cat_3=  $('.cat-3'+unique_key).text();
		 var cat_4=  $('.cat-4'+unique_key).text();
		 var price=  $('.price'+unique_key).text(); 
		 var order_qty=  $('.order-qty'+unique_key).text(); 
		 var received_qty=  $('.received-qty'+unique_key).text(); 
		 var cancelled_qty=  $('.cancelled-qty'+unique_key).text(); 
		 var git_qty=  $('.git-qty'+unique_key).text();
		 var pending_qty=  $('.pending-qty'+unique_key).text();
		 var icode=  $('.ICODE'+unique_key).text();
		 
		 //Set All Records in Modal	
		 $('#vendorChangewidthisfabric').val(width_size_required);	 
		 $('.vendorChangewidth-cat1').text(cat_1);
		 if (cat_2!='')	{   
		 $('.vendorChangewidth-cat2').text(cat_2); 
		 }
		 else{
		 $('.vendorChangewidth-cat2').text('NA');
		 }
		 if (cat_4!='')	{ 
		 $('.vendorChangewidth-cat4').text(cat_4); 
		 }
		 else{
		 $('.vendorChangewidth-cat4').text('NA');
		 }
		 $('.vendorChangewidth-po-no').text(po_no); 
		 $('.vendorChangewidth-order-qty').text(order_qty); 
		 $('.vendorChangewidth-received-qty').text(received_qty); 
		 $('.vendorChangewidth-git-qty').text(git_qty); 
		 $('.vendorChangewidth-pending-qty').text(pending_qty); 
		 $('.vendorChangewidth-current-rate').text(price);
		 $('.vendorChangewidth-po-date').text(po_date);
		 $('.vendorChangewidth-icode').text(icode);  
		 
});
//$('.submitvendorChangewidth').click(function(){
$(document).on('click', '.submitvendorChangewidth', function(e) {	
	var Icode=$(".vendorChangewidth-icode").text();
	var order_date=$('.vendorChangewidth-po-date').text();
	var cat_1=$('.vendorChangewidth-cat1').text();
	var cat_2=$('.vendorChangewidth-cat2').text();
	var old_width=$('.vendorChangewidth-cat4').text();
	var old_rate=$('.vendorChangewidth-current-rate').text();
    var po_no=$(".vendorChangewidth-po-no").text(); 
	var type=$("#width_change_type").val();
  	var new_rate=$("#vendorChangewidth_new_requested_rate").val();
  	var width_size_required=$("#vendorChangewidthisfabric").val();
  	var new_width=$("#vendorChangewidth_new_width").val();
  	var reason=$("#vendorChangewidth_reason").val();
  	//var new_po=$("#vendorRequestwidth_new_po").val(); 
	//alert(cat_1);
	if(new_rate=='')
		 $("#vendorNewReqrate_remarks_message").text('Please enter New Rate!').css('color', 'red').show();
	else 
		 $("#vendorNewReqrate_remarks_message").hide();	
	
	if(width_size_required=='1' && new_width=='')
		 $("#vendorChangecuttingwidth_remarks_message").text('Please enter New Width!').css('color', 'red').show();
	else if(width_size_required!='1' && new_width=='')
		 $("#vendorChangewidth_new_width").val('NA');
	else 
		 $("#vendorChangecuttingwidth_remarks_message").hide();
	// if(new_width=='')
	// 	 $("#vendorChangecuttingwidth_remarks_message").text('Please enter New Width!').css('color', 'red').show();
	// else 
	// 	 $("#vendorChangecuttingwidth_remarks_message").hide();
	// if(new_width=='')
	// 	 $("#vendorChangewidth_new_width").val('NA');
    if(reason=='')
		 $("#vendorChangecutting_width_message").text('Please enter Reason!').css('color', 'red').show();
	else 
		 $("#vendorChangecutting_width_message").hide();
		 
	if(reason!='' && new_width!=='')
	{
		$('.loading').show();
		$.ajax({
			type: 'POST', 
			url: cutting_url,
			data: {po_no:po_no,Icode:Icode,type:type,reason:reason,order_date:order_date,cat_1:cat_1,cat_2:cat_2,old_width:old_width,old_rate:old_rate,new_rate:new_rate,new_width:new_width,_token:ptoken},
			success:function(data)
			{
				    
					if(data==1)
					{
        				$('.vendorChangecutting-modal-body').html('Request added successfully!');
						  
						 setTimeout(function () {
						 //$('#vendorChangewidth').hide();
                          //window.location.reload();
                          $('#vendorChangewidth').modal('toggle');
                           applyFilter();
                     }, 1000);
					}
					else
						$('.vendorChangecutting-clearfix').text('Please Try again!');
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});		
     }
  
});



//For Vendor Change Date Modal
$('.vendorChangedate').click(function(){
	
		 var unique_key = $(this).attr('unique-key');
		 
		 //Fetch All Records
		 var po_no=  $('.po-no'+unique_key).text();
		 var po_date=  $('.po-date'+unique_key).text();
		 var due_date=  $('.due-date'+unique_key).text();
		 var cat_1=  $('.cat-1'+unique_key).text();
		 var cat_2=  $('.cat-2'+unique_key).text();
		 var cat_3=  $('.cat-3'+unique_key).text();
		 var cat_4=  $('.cat-4'+unique_key).text();
		 var price=  $('.price'+unique_key).text(); 
		 var order_qty=  $('.order-qty'+unique_key).text(); 
		 var received_qty=  $('.received-qty'+unique_key).text(); 
		 var cancelled_qty=  $('.cancelled-qty'+unique_key).text(); 
		 var git_qty=  $('.git-qty'+unique_key).text();
		 var pending_qty=  $('.pending-qty'+unique_key).text();
		 var icode=  $('.ICODE'+unique_key).text();
		 
		 //Set All Records in Modal		 
		 $('.vendorChangedate-cat1').text(cat_1);
		 if (cat_2!='')	{  
		 $('.vendorChangedate-cat2').text(cat_2); 
		 }
		 else{
		 $('.vendorChangedate-cat2').text('NA'); 	
		 }
		 if (cat_4!='')	{ 
		 $('.vendorChangedate-cat4').text(cat_4); 
		 }
		 else{
		 $('.vendorChangedate-cat4').text('NA'); 	
		 }
		 $('.vendorChangedate-po-no').text(po_no); 
		 $('.vendorChangedate-order-qty').text(order_qty); 
		 $('.vendorChangedate-received-qty').text(received_qty); 
		 $('.vendorChangedate-git-qty').text(git_qty); 
		 $('.vendorChangedate-pending-qty').text(pending_qty); 
		 $('.vendorChangedate-current-rate').text(price);  
		 $('.vendorChangedate-order-date').text(po_date);
		 $('.vendorChangedate-due-date').text(due_date);
		 $('.vendorChangedate-icode').text(icode); 
		 
		 
		 
		 
});
$('.submitvendorChangedate').click(function(){
	var Icode=$(".vendorChangedate-icode").text();
	var cat_1=$(".vendorChangedate-cat1").text(); 
	var cat_2=$(".vendorChangedate-cat2").text(); 
    var po_no=$(".vendorChangedate-po-no").text(); 
	var type=$("#date_ext_type").val();
	var order_date=$(".vendorChangedate-order-date").text(); 
	var del_date=$(".vendorChangedate-due-date").text(); 
  	var ext_date=$("#vendorChangedate_extended_date").val();  	
  	var reason=$("#vendorChangedate_reason").val();
  	var old_width=$('.vendorChangedate-cat4').text(); 
  	//var new_po=$("#vendorRequestdate_new_po").val();
	//alert(del_date);
	 if(ext_date=='')
		 $("#vendorChangecuttingdate_remarks_message").text('Please enter New Date !').css('color', 'red').show();
	else 
		 $("#vendorChangecuttingdate_remarks_message").hide();
    if(reason=='')
		 $("#vendorChangecuttingDate_remarks_message").text('Please enter Reason!').css('color', 'red').show();
	else 
		 $("#vendorChangecuttingDate_remarks_message").hide();
		 
	if(reason!='' && ext_date!=='')
	{
		$('.loading').show();
		$.ajax({
			type: 'POST', 
			url: cutting_url,
			data: {po_no:po_no,old_width:old_width,Icode:Icode,type:type,order_date:order_date,reason:reason,cat_1:cat_1,cat_2:cat_2,del_date:del_date,ext_date:ext_date,_token:ptoken},
			success:function(data)
			{
				    
					if(data==1)
					{
        				$('.vendorChangecutting-modal-body').html('Request added successfully!');
						 
						 setTimeout(function () {
						 //$('#vendorChangedate').hide(); 
                          //window.location.reload();
                          $('#vendorChangedate').modal('toggle');
                           applyFilter();
                     }, 1000);
					}
					else
						$('.vendorChangecutting-clearfix').text('Please Try again!');
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});		
     }
  
});




//For Vendor Cancellation Modal
$('.vendorCancellation').click(function(){
	
		 var unique_key = $(this).attr('unique-key');
		 
		 //Fetch All Records
		 var po_no=  $('.po-no'+unique_key).text();
		 var po_date=  $('.po-date'+unique_key).text();
		 var due_date=  $('.due-date'+unique_key).text();
		 var cat_1=  $('.cat-1'+unique_key).text();
		 var cat_2=  $('.cat-2'+unique_key).text();
		 var cat_3=  $('.cat-3'+unique_key).text();
		 var cat_4=  $('.cat-4'+unique_key).text();
		 var price=  $('.price'+unique_key).text(); 
		 var order_qty=  $('.order-qty'+unique_key).text(); 
		 var received_qty=  $('.received-qty'+unique_key).text(); 
		 var cancelled_qty=  $('.cancelled-qty'+unique_key).text(); 
		 var git_qty=  $('.git-qty'+unique_key).text();
		 var pending_qty=  $('.pending-qty'+unique_key).text();
		 var icode=  $('.ICODE'+unique_key).text();
		 //alert(due_date);
		 //Set All Records in Modal		 
		 $('.vendorCancellation-cat1').text(cat_1); 
		 if (cat_2!='')	{  
		 $('.vendorCancellation-cat2').text(cat_2);
		 }
		 else{
		  $('.vendorCancellation-cat2').text('NA');	
		 } 
		 if (cat_4!='')	{  
		 $('.vendorCancellation-cat4').text(cat_4); 
		 }
		 else{
		  $('.vendorCancellation-cat4').text('NA');	
		 } 
		 $('.vendorCancellation-po-no').text(po_no); 
		 $('.vendorCancellation-order-qty').text(order_qty); 
		 $('.vendorCancellation-received-qty').text(received_qty); 
		 $('.vendorCancellation-git-qty').text(git_qty); 
		 $('.vendorCancellation-pending-qty').text(pending_qty);
		 $('.vendorCancellation-po-date').text(po_date); 
		 $('.vendorCancellation-due-date').text(due_date);
		 $('.vendorCancellation-icode').text(icode);
		 
		 
});
$('.submitvendorCancellation').click(function(){
	var Icode=$(".vendorCancellation-icode").text();
	var order_date=$(".vendorCancellation-po-date").text();
	var del_date=$(".vendorCancellation-due-date").text(); 
	var cat_1=$(".vendorCancellation-cat1").text();
	var cat_2=$(".vendorCancellation-cat2").text();
    var po_no=$(".vendorCancellation-po-no").text(); 
	var type=$("#cancel_type").val();
	var pen_qty=$(".vendorCancellation-pending-qty").text(); 
  	var can_qty=$("#vendorCancellation_number").val();  	
  	var reason=$("#vendorCancellation_reason").val(); 
  	var old_width=$('.vendorCancellation-cat4').text(); 
	//alert(pen_qty);
	//alert(del_date);
	if(can_qty=='')
		 $("#vendorChangecuttingcancel_remarks_message").text('Please enter Cancel Quantity !').css('color', 'red').show();
	else 
		 $("#vendorChangecuttingcancel_remarks_message").hide();
    if(reason=='')
		 $("#vendorChangecuttingCancel_remarks_message").text('Please enter  Reason!').css('color', 'red').show();
	else 
		 $("#vendorChangecuttingCancel_remarks_message").hide();

	if(parseInt(can_qty) > parseInt(pen_qty))
		 $("#vendorcancelqty_message").text('Cancel Quantity should not be more than Pending Quantity!').css('color', 'red').show();
	else 
		 $("#vendorcancelqty_message").hide();	
		 
	if(reason!='' && can_qty!=='' && parseInt(can_qty) <= parseInt(pen_qty))
	{
		$('.loading').show();
		$.ajax({
			type: 'POST', 
			url: cutting_url,
			data: {po_no:po_no,old_width:old_width,Icode:Icode,type:type,pen_qty:pen_qty,order_date:order_date,cat_1:cat_1,cat_2:cat_2,del_date:del_date,reason:reason,can_qty:can_qty,_token:ptoken},
			success:function(data)
			{
				    
					if(data==1)
					{
						
        				$('.vendorChangecutting-modal-body').html('Request added successfully!');
						
						 setTimeout(function () {
						 //$('#vendorCancellation').hide(); 
                          //window.location.reload();
                          $('#vendorCancellation').modal('toggle');
                           applyFilter();
                     }, 1000);
					}
					else
						$('.vendorChangecutting-clearfix').text('Please Try again!');
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});		
     }
  
});




//For Vendor Addremarks Modal
$('.vendorAddremarks').click(function(){
	
		 var unique_key = $(this).attr('unique-key');
		 
		 //Fetch All Records
		 var po_no=  $('.po-no'+unique_key).text();
		 var po_date=  $('.po-date'+unique_key).text();
		 var due_date=  $('.due-date'+unique_key).text();
		 var cat_1=  $('.cat-1'+unique_key).text();
		 var cat_2=  $('.cat-2'+unique_key).text();
		 var cat_3=  $('.cat-3'+unique_key).text();
		 var cat_4=  $('.cat-4'+unique_key).text();
		 var price=  $('.price'+unique_key).text(); 
		 var order_qty=  $('.order-qty'+unique_key).text(); 
		 var received_qty=  $('.received-qty'+unique_key).text(); 
		 var cancelled_qty=  $('.cancelled-qty'+unique_key).text(); 
		 var git_qty=  $('.git-qty'+unique_key).text();
		 var pending_qty=  $('.pending-qty'+unique_key).text();
		 var icode=  $('.ICODE'+unique_key).text();
		 
		 //Set All Records in Modal		 
		 $('.vendorAddremarks-cat1').text(cat_1); 
		 if (cat_2!='')	{ 
		 $('.vendorAddremarks-cat2').text(cat_2); 
		 }
		 else{
		 $('.vendorAddremarks-cat2').text('NA'); 
		 }
		 if (cat_4!='')	{
		 $('.vendorAddremarks-cat4').text(cat_4); 
		 }
		 else{
		 $('.vendorAddremarks-cat4').text('NA'); 
		 }
		 $('.vendorAddremarks-po-no').text(po_no);
		 $('.vendorAddremarks-date').text(po_date);
		 $('.vendorAddremarks-order-qty').text(order_qty); 
		 $('.vendorAddremarks-received-qty').text(received_qty); 
		 $('.vendorAddremarks-git-qty').text(git_qty); 
		 $('.vendorAddremarks-pending-qty').text(pending_qty);
		 $('.vendorAddremarks-icode').text(icode); 
		 
		 
});
$('.submitvendorAddremarks').click(function(){
	var Icode=$(".vendorAddremarks-icode").text();
	var order_date=$(".vendorAddremarks-date").text(); 
	var cat_1=$(".vendorAddremarks-cat1").text(); 
	var cat_2=$(".vendorAddremarks-cat2").text(); 
    var po_no=$(".vendorAddremarks-po-no").text(); 
	var type=$("#others_type").val();  		
  	var reason=$("#vendorAddremarks_remarks").val();
  	var old_width=$('.vendorAddremarks-cat4').text();
  	//var new_po=$("#vendorRequestothers_new_po").val(); 
	//alert(type);
    if(reason=='')
		 $("#vendorOthers_remarks_message").text('Please enter Remarks!').css('color', 'red').show();
	else 
		 $("#vendorOthers_remarks_message").hide();
		 
	if(reason!='')
	{
		$('.loading').show();
		$.ajax({
			type: 'POST', 
			url: cutting_url,
			data: {po_no:po_no,old_width:old_width,Icode:Icode,type:type,order_date:order_date,cat_1:cat_1,cat_2:cat_2,reason:reason,_token:ptoken},
			success:function(data)
			{
				    
					if(data==1)
					{
        				$('.vendorAddremarks-modal-body').html('Request added successfully!');
						
						 setTimeout(function () {
						 //$('#vendorAddremarks').hide(); 	
                          //window.location.reload();
                          $('#vendorAddremarks').modal('toggle');
                           applyFilter();
                     }, 1000);
					}
					else
						$('.vendorChangecutting-clearfix').text('Please Try again!');
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});		
     }
  
});


//For Vendor SampleRequest Modal
$('.vendorSamplerequest').click(function(){
	
		 var unique_key = $(this).attr('unique-key');
		 
		 //Fetch All Records
		 var po_no=  $('.po-no'+unique_key).text();
		 var po_date=  $('.po-date'+unique_key).text();
		 var due_date=  $('.due-date'+unique_key).text();
		 var cat_1=  $('.cat-1'+unique_key).text();
		 var cat_2=  $('.cat-2'+unique_key).text();
		 var cat_3=  $('.cat-3'+unique_key).text();
		 var cat_4=  $('.cat-4'+unique_key).text();
		 var price=  $('.price'+unique_key).text(); 
		 var order_qty=  $('.order-qty'+unique_key).text(); 
		 var received_qty=  $('.received-qty'+unique_key).text(); 
		 var cancelled_qty=  $('.cancelled-qty'+unique_key).text(); 
		 var git_qty=  $('.git-qty'+unique_key).text();
		 var pending_qty=  $('.pending-qty'+unique_key).text();
		 var icode=  $('.ICODE'+unique_key).text();
		 
		 //Set All Records in Modal		 
		 $('.vendorSamplerequest-cat1').text(cat_1); 
		 if (cat_2!='')	{
		 $('.vendorSamplerequest-cat2').text(cat_2); 
		 }
		 else{
		  $('.vendorSamplerequest-cat2').text('NA'); 	
		 }
		 if (cat_4!='')	{
		 $('.vendorSamplerequest-cat4').text(cat_4); 
		 }
		 else{
		  $('.vendorSamplerequest-cat4').text('NA'); 	
		 }
		 $('.vendorSamplerequest-po-no').text(po_no);
		 $('.vendorSamplerequest-date').text(po_date);
		 $('.vendorSamplerequest-order-qty').text(order_qty); 
		 $('.vendorSamplerequest-received-qty').text(received_qty); 
		 $('.vendorSamplerequest-git-qty').text(git_qty); 
		 $('.vendorSamplerequest-pending-qty').text(pending_qty);
		 $('.vendorSamplerequest-icode').text(icode); 
		 
		 
});
$('.submitvendorSamplereq').click(function(){
	var Icode=$(".vendorSamplerequest-icode").text();
	var order_date=$(".vendorSamplerequest-date").text(); 
	var cat_1=$(".vendorSamplerequest-cat1").text(); 
	var cat_2=$(".vendorSamplerequest-cat2").text(); 
    var po_no=$(".vendorSamplerequest-po-no").text(); 
	var type=$("#sample_type").val();  		
  	var reason=$("#vendorSamplerequest-remarks").val();
  	var docket_no=$("#vendorRequestaccept_new_docket").val();
  	//var prn_no=$("#vendorRequestaccept_new_prn").val();
  	//var prn_date=$("#vendorRequestaccept_new_prndate").val();
  	var courier_no=$("#vendorRequestaccept_new_courier").val();
  	var old_width=$('.vendorSamplerequest-cat4').text();
  	//var new_po=$("#vendorRequestaccept_new_po").val(); 
	//alert(new_po);
	if((type=='width_change' || type=='rate_change') && accept_val==''){
		 $("#vendorRequestaccept_rate-remarks_message").text('Please enter Rate!').css('color', 'red').show();
	return false;
	}
	else 
		 $("#vendorRequestaccept_rate-remarks_message").hide();
    
    if((type=='sample') && docket_no==''){
		 $("#vendorRequestaccept_docket-remarks_message").text('Please enter Docket No!').css('color', 'red').show();
	return false;
	}
	else 
		 $("#vendorRequestaccept_docket-remarks_message").hide();
	// if((type=='sample') && prn_no==''){
	// 	 $("#vendorRequestaccept_prn-remarks_message").text('Please enter PRN No!').css('color', 'red').show();
	// return false;
	// }
	// else 
	// 	 $("#vendorRequestaccept_prn-remarks_message").hide();
	// if((type=='sample') && prn_date==''){
	// 	 $("#vendorRequestaccept_prndate-remarks_message").text('Please enter Date!').css('color', 'red').show();
	// return false;
	// }
	// else 
	// 	 $("#vendorRequestaccept_prndate-remarks_message").hide();
	if((type=='sample') && courier_no==''){
		 $("#vendorRequestaccept_courier-remarks_message").text('Please enter Courier Name!').css('color', 'red').show();
	return false;
	}
	else 
		 $("#vendorRequestaccept_courier-remarks_message").hide();
 //    if(reason=='')
	// 	 $("#vendorSample_remarks_message").text('Please enter Remarks!').css('color', 'red').show();
	// else 
	// 	 $("#vendorSample_remarks_message").hide();
		 
	// if(reason!='')
	// {
		$('.loading').show();
		$.ajax({
			type: 'POST', 
			url: cutting_url,
			data: {po_no:po_no,old_width:old_width,Icode:Icode,type:type,order_date:order_date,cat_1:cat_1,cat_2:cat_2,docket_no:docket_no,courier_no:courier_no,reason:reason,_token:ptoken},
			success:function(data)
			{
				    
					if(data==1)
					{
        				$('.vendorSamplerequest-modal-body').html('Request added successfully!');
						
						 setTimeout(function () {
						 //$('#vendorSamplerequest').hide(); 
                          //window.location.reload();
                          $('#vendorSamplerequest').modal('toggle');
                           applyFilter();
                     }, 1000);
					}
					else
						$('.vendorSamplerequest-clearfix').text('Please Try again!');
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});		
    //}
  
});

////////////--------------------------//////////

function actionType(modal_id,unique_key){
	if(modal_id!='')
	{
		 $('#'+modal_id).modal('show');
		 //Fetch All Records
		 var po_no=  $('.po-no'+unique_key).text();
		 var po_date=  $('.po-date'+unique_key).text();
		 var due_date=  $('.due-date'+unique_key).text();
		 var cat_1=  $('.cat-1'+unique_key).text();
		 var cat_2=  $('.cat-2'+unique_key).text();
		 var cat_3=  $('.cat-3'+unique_key).text();
		 var cat_4=  $('.cat-4'+unique_key).text();
		 var price=  $('.price'+unique_key).text(); 
		 var order_qty=  $('.order-qty'+unique_key).text(); 
		 var received_qty=  $('.received-qty'+unique_key).text(); 
		 var cancelled_qty=  $('.cancelled-qty'+unique_key).text(); 
		 var git_qty=  $('.git-qty'+unique_key).text();
		 var pending_qty=  $('.pending-qty'+unique_key).text();
		 var icode=  $('.ICODE'+unique_key).text();
		 //Set All Records in Modal		
		 if(modal_id=='vendorChangecutting') 
		 {
		 $('.vendorChangecutting-cat1').text(cat_1);
		 if(cat_2!=''){ 
		 $('.vendorChangecutting-cat2').text(cat_2); 
		 }
		 else{
		 $('.vendorChangecutting-cat2').text('NA');	
		 }
		 if(cat_4!=''){ 
		 $('.vendorChangecutting-cat4').text(cat_4);
		 }
		 else{
		 $('.vendorChangecutting-cat4').text('NA');	
		 } 
		 $('.vendorChangecutting-po-no').text(po_no); 
		 $('.vendorChangecutting-date').text(po_date); 
		 $('.vendorChangecutting-order-qty').text(order_qty); 
		 $('.vendorChangecutting-received-qty').text(received_qty); 
		 $('.vendorChangecutting-git-qty').text(git_qty); 
		 $('.vendorChangecutting-pending-qty').text(pending_qty);
		 $('.vendorChangecutting-icode').text(icode);
		 }
		 //Set All Records in Modal
		  if(modal_id=='vendorChangeprice') 
		 {		 
		 $('.vendorChangeprice-cat1').text(cat_1); 
		 if(cat_2!=''){ 
		 $('.vendorChangeprice-cat2').text(cat_2); 
		 }
		 else{
		 $('.vendorChangeprice-cat2').text('NA');	
		 }
		 if(cat_4!=''){ 
		 $('.vendorChangeprice-cat4').text(cat_4);
		 }
		 else{
		 $('.vendorChangeprice-cat4').text('NA');	
		 } 
		 $('.vendorChangeprice-po-no').text(po_no); 
		 $('.vendorChangeprice-order-qty').text(order_qty); 
		 $('.vendorChangeprice-received-qty').text(received_qty); 
		 $('.vendorChangeprice-git-qty').text(git_qty); 
		 $('.vendorChangeprice-pending-qty').text(pending_qty); 
		 $('.vendorChangeprice-current-rate').text(price); 
		 $('.vendorChangeprice-po-date').text(po_date);
		 $('.vendorChangeprice-icode').text(icode); 
		 }
		 //Set All Records in Modal	
		  if(modal_id=='vendorChangewidth'){  
		 $('.vendorChangewidth-cat1').text(cat_1); 
		 if(cat_2!=''){ 
		 $('.vendorChangewidth-cat2').text(cat_2); 
		 }
		 else{
		 $('.vendorChangewidth-cat2').text('NA');	
		 }
		 if(cat_4!=''){ 
		 $('.vendorChangewidth-cat4').text(cat_4);
		 }
		 else{
		 $('.vendorChangewidth-cat4').text('NA');	
		 }  
		 $('.vendorChangewidth-po-no').text(po_no); 
		 $('.vendorChangewidth-order-qty').text(order_qty); 
		 $('.vendorChangewidth-received-qty').text(received_qty); 
		 $('.vendorChangewidth-git-qty').text(git_qty); 
		 $('.vendorChangewidth-pending-qty').text(pending_qty); 
		 $('.vendorChangewidth-current-rate').text(price);
		 $('.vendorChangewidth-po-date').text(po_date);  
		 $('.vendorChangewidth-icode').text(icode);
		}
		//Set All Records in Modal
		 if(modal_id=='vendorChangedate'){ 	 
		 $('.vendorChangedate-cat1').text(cat_1); 
		 if(cat_2!=''){ 
		 $('.vendorChangedate-cat2').text(cat_2); 
		 }
		 else{
		 $('.vendorChangedate-cat2').text('NA');	
		 }
		 if(cat_4!=''){ 
		 $('.vendorChangedate-cat4').text(cat_4);
		 }
		 else{
		 $('.vendorChangedate-cat4').text('NA');	
		 }   
		 $('.vendorChangedate-po-no').text(po_no); 
		 $('.vendorChangedate-order-qty').text(order_qty); 
		 $('.vendorChangedate-received-qty').text(received_qty); 
		 $('.vendorChangedate-git-qty').text(git_qty); 
		 $('.vendorChangedate-pending-qty').text(pending_qty); 
		 $('.vendorChangedate-current-rate').text(price);
		 $('.vendorChangedate-order-date').text(po_date);
		 $('.vendorChangedate-due-date').text(due_date); 
		 $('.vendorChangedate-icode').text(icode);
		}
		 //Set All Records in Modal	
		 if(modal_id=='vendorCancellation'){ 	 
		 $('.vendorCancellation-cat1').text(cat_1); 
		 if(cat_2!=''){ 
		 $('.vendorCancellation-cat2').text(cat_2); 
		 }
		 else{
		 $('.vendorCancellation-cat2').text('NA');	
		 }
		 if(cat_4!=''){ 
		 $('.vendorCancellation-cat4').text(cat_4);
		 }
		 else{
		 $('.vendorCancellation-cat4').text('NA');	
		 }   
		 $('.vendorCancellation-po-no').text(po_no); 
		 $('.vendorCancellation-order-qty').text(order_qty); 
		 $('.vendorCancellation-received-qty').text(received_qty); 
		 $('.vendorCancellation-git-qty').text(git_qty); 
		 $('.vendorCancellation-pending-qty').text(pending_qty);
		 $('.vendorCancellation-po-date').text(po_date); 
		 $('.vendorCancellation-due-date').text(due_date);
		 $('.vendorCancellation-icode').text(icode);
       }
       //Set All Records in Modal	
       	 if(modal_id=='vendorAddremarks'){ 	 
		 $('.vendorAddremarks-cat1').text(cat_1); 
		 if(cat_2!=''){ 
		 $('.vendorAddremarks-cat2').text(cat_2); 
		 }
		 else{
		 $('.vendorAddremarks-cat2').text('NA');	
		 }
		 if(cat_4!=''){ 
		 $('.vendorAddremarks-cat4').text(cat_4);
		 }
		 else{
		 $('.vendorAddremarks-cat4').text('NA');	
		 }   
		 
		 $('.vendorAddremarks-po-no').text(po_no);
		 $('.vendorAddremarks-date').text(po_date);
		 $('.vendorAddremarks-order-qty').text(order_qty); 
		 $('.vendorAddremarks-received-qty').text(received_qty); 
		 $('.vendorAddremarks-git-qty').text(git_qty); 
		 $('.vendorAddremarks-pending-qty').text(pending_qty);
		 $('.vendorAddremarks-icode').text(icode); 
		}
		//Set All Records in Modal	
       	 if(modal_id=='vendorSamplerequest'){ 	 
		 $('.vendorSamplerequest-cat1').text(cat_1); 
		 if(cat_2!=''){ 
		 $('.vendorSamplerequest-cat2').text(cat_2); 
		 }
		 else{
		 $('.vendorSamplerequest-cat2').text('NA');	
		 }
		 if(cat_4!=''){ 
		 $('.vendorSamplerequest-cat4').text(cat_4);
		 }
		 else{
		 $('.vendorSamplerequest-cat4').text('NA');	
		 }   
		 $('.vendorSamplerequest-po-no').text(po_no);
		 $('.vendorSamplerequest-date').text(po_date);
		 $('.vendorSamplerequest-order-qty').text(order_qty); 
		 $('.vendorSamplerequest-received-qty').text(received_qty); 
		 $('.vendorSamplerequest-git-qty').text(git_qty); 
		 $('.vendorSamplerequest-pending-qty').text(pending_qty);
		 $('.vendorSamplerequest-icode').text(icode); 
		}
	}
	else
	{
		alert('Please Select a Type');
	}
}
