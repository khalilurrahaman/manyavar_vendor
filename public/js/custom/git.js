//For set AddGIT
//$('.addGitClass').click(function(){
$(document).on('click', '.addGitClass', function(e) {	
	 var uniqueKey = $(this).attr('uniqueKey');
	 //Fetch All Td value of the current Tr
	 var id = $('.'+uniqueKey+'-id').val();
	 var keyClass='buttonAddGitCart'+id;
	 var img = $('.'+uniqueKey+'-img').attr('src');
	 var cat_1 = $('.'+uniqueKey+'-cat-1').text();
	 var cat_2 = $('.'+uniqueKey+'-cat-2').text();
	 var cat_3 = $('.'+uniqueKey+'-cat-3').text();
	 var cat_4 = $('.'+uniqueKey+'-cat-4').text(); 
	 var order_qty = $('.'+uniqueKey+'-order-qty').text();
	 var received_qty = $('.'+uniqueKey+'-received-qty').text();
	 var pending_git_qty = $('.'+uniqueKey+'-pending-git-qty').text();
	 var pending_qty = $('.'+uniqueKey+'-pending-qty').text();
	 var add_git_qty = $('.'+uniqueKey+'-add-git-qty').val();
	 var ORDCODE = $('.'+uniqueKey+'-ORDCODE').val();
	 var ICODE = $('.'+uniqueKey+'-ICODE').val();
	 var pd_id = $('.'+uniqueKey+'-pd_id').val();
	 var p_qty=pending_qty-add_git_qty;
	 //Add Git Quantity Validation
	 var p_qty_show=0;
	 if(p_qty>0)
	 p_qty_show =p_qty;
	 var ret_val=true;
	 $(".commonMessage").text('');//Remove previous all message
	 if(add_git_qty=='')
	 {
		 $("."+uniqueKey+'-message').text('Please enter Git Quantity!').css('color', 'red');
		 ret_val=false;
	 }
     else if(add_git_qty<=0)
	 {
		 $("."+uniqueKey+'-message').text('You have enter invalid Git Quantity!').css('color', 'red');
		 ret_val=false;
	 }
	 else
	 { 
		 $("."+uniqueKey+'-message').text('');
		 if(parseInt(add_git_qty)>pending_qty)
		 { 
		 	/*if (!window.confirm("Do you really want to leave?")) { 
			  return false;
			}*/
		 	var x=confirm("Are you sure to Add Extra Git?");
            if(!x)
			   return false;
		 }
	 }
	
		 
	 //Remove current Tr and Add in Right Side
	 if(typeof add_git_qty != "undefined" && ret_val==true)
	 {
		//Remove Current tr
	 	$('.'+uniqueKey+'-tr').remove();
		
		//Add in Right Side		
		var right_html='<tr class="'+keyClass+'-tr">';
			right_html +='<td class="text-center '+keyClass+'-cat-1">'+cat_1+'</td>';
			right_html +='<td class="text-center '+keyClass+'-cat-2">'+cat_2+'</td>';
			right_html +='<td class="text-center '+keyClass+'-cat-3">'+cat_3+'</td>';
			right_html +='<td class="text-center '+keyClass+'-cat-4">'+cat_4+'</td>';
			right_html +='<input type="hidden" class="'+keyClass+'-id" value="'+id+'"  />';//hidden
			right_html +='<input type="hidden" class="'+keyClass+'-order_qty" value="'+order_qty+'"  />';//hidden
			right_html +='<input type="hidden" class="'+keyClass+'-pending-git-qty" value="'+pending_git_qty+'"  />';//hidden
			right_html +='<input type="hidden" class="'+keyClass+'-received-qty" value="'+received_qty+'"  />';//hidden
			right_html +='<input type="hidden" class="'+keyClass+'-img" value="'+img+'"  />';//hidden
			right_html +='<input type="hidden" class="'+keyClass+'-org-pndng" value="'+pending_qty+'"  />';//hidden
			right_html +='<input type="hidden" class="'+keyClass+'-git-qty" name="git_qty[]" value="'+add_git_qty+'"  />';//hidden
			right_html +='<input type="hidden" class="'+keyClass+'-pd_id" name="pd_id[]" value="'+pd_id+'"  />';//hidden
			right_html +='<input type="hidden" class="'+keyClass+'-ORDCODE" name="ORDCODE[]" value="'+ORDCODE+'"  />';//hidden
			right_html +='<input type="hidden" class="'+keyClass+'-ICODE" name="ICODE[]" value="'+ICODE+'"  />';//hidden
			right_html +='<input type="hidden" class="'+keyClass+'-pending-qty" value="'+p_qty+'"  />';//hidden
			
			right_html +='<td  class="text-center"><span class="'+keyClass+'-add-git-qty">'+add_git_qty+'</span><input type="text" class="git-qty-input '+keyClass+'-git-qty-input updateGitClass" value="'+add_git_qty+'"  uniqueKey="'+keyClass+'"/></td>';
			right_html +='<td class="text-center '+keyClass+'-pending-qty-show">'+p_qty_show+'</td>';
			
			right_html +='<td class="text-center"><a href="javascript:void(0)" style="color:green;" class="editGitClass" uniqueKey="'+keyClass+'"><i class="fa fa-edit" aria-hidden="true"></i></a><a href="javascript:void(0)" style="color:#F00;" class="removeGitClass" uniqueKey="'+keyClass+'"><i class="fa fa-times" aria-hidden="true"></i></a></td>';					
		right_html +='</tr>';
		
		if($('.empty-table-GitCart').length==1)
			$('.tbody-table-GitCart').html(right_html);
		else
			$('.tbody-table-GitCart').prepend(right_html);
		
		//For Empty Case						
		if($('#tableGit >tbody >tr').length==0)
		{
			$('.tbody-table-GitAdd').html('<tr class="empty-table-GitAdd"><td colspan="11">No Records available!</td></tr>');
		}
	 }
	 									
	 								
 
		 
});
//For Edit GIT current tr
$(document).on('click', '.editGitClass', function(e) {
	 var uniqueKey = $(this).attr('uniqueKey');
	 //Fetch All Td value of the current Tr
	 var id = $('.'+uniqueKey+'-id').val();
	 var keyClass='buttonAddGit'+id;
	 var add_git_qty = $('.'+uniqueKey+'-add-git-qty').text();
	 $('.'+uniqueKey+'-add-git-qty').hide();
	 $('.'+uniqueKey+'-git-qty-input').show();
});

//For Update GIT current tr
$(document).on('keyup', '.updateGitClass', function(e) {
	 var add_git_qty=$(this).val();
     var uniqueKey = $(this).attr('uniqueKey');
	 //Fetch All Td value of the current Tr
	 var id = $('.'+uniqueKey+'-id').val();
	 var keyClass='buttonAddGit'+id;
	
	 $('.'+uniqueKey+'-git-qty').val(add_git_qty);
	 
	 
	 var pending_qty = $('.'+uniqueKey+'-org-pndng').val();
	 var p_qty=pending_qty-add_git_qty;
	 var p_qty_show=0;
	 if(p_qty>0)
	 p_qty_show =p_qty;
	 
	 $('.'+uniqueKey+'-pending-qty-show').text(p_qty_show);
	 
	 
});

//For Remove GIT current tr
//$('.removeGitClass').click(function(){
$(document).on('click', '.removeGitClass', function(e) {
	 var uniqueKey = $(this).attr('uniqueKey');
	 //Fetch All Td value of the current Tr
	 var id = $('.'+uniqueKey+'-id').val();
	 var keyClass='buttonAddGit'+id;
	 var img = $('.'+uniqueKey+'-img').val();
	 var cat_1 = $('.'+uniqueKey+'-cat-1').text();
	 var cat_2 = $('.'+uniqueKey+'-cat-2').text();
	 var cat_3 = $('.'+uniqueKey+'-cat-3').text();
	 var cat_4 = $('.'+uniqueKey+'-cat-4').text(); 
	 var order_qty = $('.'+uniqueKey+'-order_qty').val();
	 var pd_id = $('.'+uniqueKey+'-pd_id').val();
	 var received_qty = $('.'+uniqueKey+'-received-qty').val();
	 var pending_git_qty = $('.'+uniqueKey+'-pending-git-qty').val();
	 var pending_qty = $('.'+uniqueKey+'-pending-qty').val();
	 var add_git_qty = $('.'+uniqueKey+'-add-git-qty').text();
	 var p_qty=+pending_qty + +add_git_qty;
	 var ORDCODE = $('.'+uniqueKey+'-ORDCODE').val();
	 var ICODE = $('.'+uniqueKey+'-ICODE').val();
	//Remove Current tr
	//$(this).closest("tr").remove();
	$('.'+uniqueKey+'-tr').remove();
	//Add in Laft Side		
		var left_html='<tr class="'+keyClass+'-tr">';
        	left_html +='<td class="text-center">';
            left_html +='<div class="picture">';
			left_html +='<a class="small" href="#nogo" title="small image">';
			left_html +='<img src="'+img+'" title="small image" width="56" class="'+keyClass+'-img"/>';
			left_html +='<img class="large" src="'+img+'" title="large image" /></a>';
			left_html +='</div>';
            left_html +='</td>';
            left_html +='<td class="text-center '+keyClass+'-cat-1">'+cat_1+'</td>';
            left_html +='<td class="text-center '+keyClass+'-cat-2">'+cat_2+'</td>';
            left_html +='<td class="text-center '+keyClass+'-cat-3">'+cat_3+'</td>';
            left_html +='<td class="text-center '+keyClass+'-cat-4">'+cat_4+'</td>';
            left_html +='<td class="text-center '+keyClass+'-order-qty">'+order_qty+'</td>';
            left_html +='<td class="text-center '+keyClass+'-received-qty">'+received_qty+'</td>';
            left_html +='<td class="text-center '+keyClass+'-pending-git-qty">'+pending_git_qty+'</td>';
            left_html +='<td class="text-center '+keyClass+'-pending-qty">'+p_qty+'</td>';
            left_html +='<td class="text-center last-area  new-git">';
            left_html +='<div class="input-group">';
			
            left_html +='<input type="hidden"  class="'+keyClass+'-id" value="'+id+'">';//hidden
			left_html +='<input type="hidden" class="'+keyClass+'-ORDCODE" name="ORDCODE[]" value="'+ORDCODE+'"  />';//hidden
			left_html +='<input type="hidden" class="'+keyClass+'-ICODE" name="ICODE[]" value="'+ICODE+'"  />';//hidden
			left_html +='<input type="hidden" class="'+keyClass+'-pd_id" name="pd_id[]" value="'+pd_id+'"  />';//hidden
			
            left_html +='<input type="text" class="form-control '+keyClass+'-add-git-qty" placeholder="Add GIT" onkeypress="return isNumber(event);">';
            left_html +='<span class="input-group-btn">';
            left_html +='<button class="btn btn-info addGitClass" uniqueKey="'+keyClass+'"  type="button">ADD</button>';
            left_html +='</span>';
            left_html +='</div>';
            left_html +='<span class="'+keyClass+'-message commonMessage"></span>';
            left_html +='</td>';			
		left_html +='</tr>';
	
		if($('.empty-table-GitAdd').length==1)
		{
			$('.tbody-table-GitAdd').html(left_html);
			
		}
		else
		{
			$('.tbody-table-GitAdd').prepend(left_html);
		}
				
	//For Empty Case
	if($('#tableGitCart >tbody >tr').length==0)
	{
		$('.tbody-table-GitCart').html('<tr class="empty-table-GitCart"><td colspan="7">Add Git for further process!</td></tr>');
	}
});




//Submit Git
$(document).on('click', '.submitGitClass', function(e) {
  
  var ret_val=false;
  var availability = $('#availability').val();

  var challan_no = $('#challan_no').val();
  if(challan_no==''){$("#challan_no_message").text('Please enter Challan No!').css('color', 'red').show();ret_val=true;}
  else if(availability==0){$("#challan_no_message").text('Challan No Already Exist!').css('color', 'red').show();ret_val=true;}
  else{$("#challan_no_message").hide(); }
  /* var lorry_no = $('#lorry_no').val();
   if(lorry_no==''){$("#lorry_no_message").text('Please enter Lorry No!').css('color', 'red').show();ret_val=true;}
   else{$("#lorry_no_message").hide(); }
  */
  
  /*var transport_no = $('#transport_no').val();
  if(transport_no==''){$("#transport_no_message").text('Please enter Transport No!').css('color', 'red').show();ret_val=true;}
  else{$("#transport_no_message").hide(); }*/
  
  var date = $('#date').val();
  if(date==''){$("#date_message").text('Please enter Date !').css('color', 'red').show();ret_val=true;}
  else{$("#date_message").hide(); }
  
 
  var empty_row = $('.empty-table-GitCart').length;
  var rowCount = $('.tbody-table-GitCart tr').length;
  if(empty_row==1 || rowCount==0)
  {
  $("#git_cart_message").text('Add Item for further process!').css('color', 'red').show();
  ret_val=true;
  }
  else
  {
	  var git_qty =  $('input[name="git_qty[]"]') ;
	  for(var i=0;i<git_qty.length;i++)
	  {
		  if($.trim(git_qty.eq(i).val())=='')
		  {
			 alert('New Git Cannot be blank!');
			  ret_val=true;
			  break;			  
		  }	  
	  }
	
	
  $("#git_cart_message").hide(); 
  }
  if(ret_val==false )
  {
   $("#addGitForm").submit();
  }

});





