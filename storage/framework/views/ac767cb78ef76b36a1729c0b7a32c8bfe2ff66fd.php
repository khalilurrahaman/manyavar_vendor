<div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <!-- <th class="text-center">Sr. No</th> -->
                                                <th class="text-center">Image</th>
                                                <th class="text-center">Item</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Cat3</th>
                                                <th class="text-center">Width/Size</th>
                                                <th class="text-center">Cat6</th>
                                                <th class="text-center">Price</th>
                                                <th class="text-center col-sm-2">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <?php $inc=$data->perPage() * ($data->currentPage()-1);; ?>
                                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $eachData): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                     <?php $inc++; ?>
                                     
                                    <tr>
                                       <!--  <td class="text-center"><?php echo e($inc); ?></td> -->
                                       
                                        <td class="text-center">
                                        <div class="gal"> 
                             <?php 
                              $item_image=URL::to('/img').'/pre-order-img.jpg';
                               if($eachData->image!='' && file_exists('upload/product/'.$eachData->image))
                               $item_image=URL::to('/upload/product').'/'.$eachData->image;
                                                            ?>
                               <a class="fancybox" rel="group<?php echo e($inc); ?>" href="<?php echo e($item_image); ?>" >
                                <img src="<?php echo e($item_image); ?>" width="150">
                               </a>
                                        
                               <a class="fancybox" rel="group<?php echo e($inc); ?>" href="<?php echo e($item_image); ?>" style="display:none;"><img src="<?php echo e($item_image); ?>" alt=""></a>
                              
                            </div>
                                         </td>
                                        <td class="text-center"><?php if($eachData->ICODE): ?><?php echo e(($eachData->ICODE)); ?><?php endif; ?></td>
                                        <td class="text-center"><?php echo e($eachData->cat1); ?> </td>
                                        <td class="text-center"><?php echo e($eachData->cat2); ?> </td>
                                        <td class="text-center"><?php echo e($eachData->cat3); ?> </td>
                                        <td class="text-center"><?php echo e($eachData->cat4); ?> </td>
                                        <td class="text-center"><?php echo e($eachData->CNAME6); ?> </td>
                                        <td class="text-center"><?php echo e($eachData->RATE); ?> 
                                        <?php if($eachData->request_status==2): ?>
                                        (old rate:<?php echo e($eachData->old_rate); ?> updated on <?php echo e(date("d/m/y",strtotime($eachData->updated_at))); ?>)
                                        <?php endif; ?>
                                        </td>
                                        <td class="text-center">
                                          <?php if($eachData->request_status==1): ?>
                                          <span>Requested Price:<?php echo e($eachData->requested_rate); ?></span><br>  
                                            <a class="btn btn-success">Request Sent</a>
                                         <?php endif; ?>   
                                         <?php if($eachData->request_status!=3 && $eachData->request_status!=1): ?>
                                         <?php if($eachData->request_status==4): ?>
                                         <span>Requested Price:<?php echo e($eachData->requested_rate); ?></span><br>
                                        <?php if(!empty($eachData->negotiate_rate)): ?><br><span>Admin Negotiated Price:<?php echo e($eachData->negotiate_rate); ?></span><br><?php endif; ?>
                                         <span class="btn-danger">Rejected</span><br> 
                                         <?php endif; ?>
                                            <a href="<?php echo e(route('request-price-change-vendor', $eachData->id)); ?>" class="btn btn-success">Request For Price Change</a>
                                         <?php endif; ?>
                                          <?php if($eachData->request_status==3): ?> 
                                            <span class="">Admin Negotiated Price:<?php echo e($eachData->negotiate_rate); ?></span><br>
                                            <a onclick="return acceptRequest('<?php echo e($eachData->id); ?>','<?php echo e($eachData->negotiate_rate); ?>');" class="btn btn-success">Accept</a>
                                            <a href="<?php echo e(route('vendor-reject-nego-price', $eachData->id)); ?>" class="btn btn-danger">Reject</a>
                                         <?php endif; ?>  
                                        </td>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>                            
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                              <div class="col-sm-6">
                                <div class="dataTables_info" role="status" aria-live="polite">Showing <?php echo e($data->count()); ?> of <?php echo e($data->total()); ?> entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                        <?php echo e($data->links()); ?>

                                    </nav>
                                  </div>
                              </div>