<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!--------------------------------Start Shorting-------------------------->
<style>
.dt-buttons{margin-right:0px !important;}
.dt-buttons>a {padding-left:18px;}
</style>
<script src="<?php echo e(asset('dataTable/css/jquery.dataTables.min.css')); ?>"></script>
<script src="<?php echo e(asset('dataTable/css/buttons.dataTables.min.css')); ?>"></script>

   
<script src="<?php echo e(asset('dataTable/j0s/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(asset('dataTable/js/dataTables.buttons.min.js')); ?>"></script>   
<script src="<?php echo e(asset('dataTable/js/buttons.flash.min.js')); ?>"></script>
<script src="<?php echo e(asset('dataTable/js/jszip.min.js')); ?>"></script>   
<script src="<?php echo e(asset('dataTable/js/pdfmake.min.js')); ?>"></script>
<script src="<?php echo e(asset('dataTable/js/vfs_fonts.js')); ?>"></script>
<script src="<?php echo e(asset('dataTable/js/buttons.html5.min.js')); ?>"></script>
<script src="<?php echo e(asset('dataTable/js/buttons.print.min.js ')); ?>"></script>
<!--------------------------------End Shorting-------------------------->
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <!--<div class="row">
                    <div class="col-lg-12 title-top">
                    <div class="row page-header">
                    	<div class="col-sm-6">
                        	<h1>Vendor <small>Purchase Order Listing</small></h1>
                        </div>
                        <div class="col-sm-6">
                        	<a href="javascript:void(0)" class="btn btn-success">Expand</a>
                        </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-lg-12">
                    <?php if(isset($errors)): ?>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>

                                        <div class="btn btn-success" id="hide-me" style="width:100%;background-color: green;border: none;"><?php echo e($error); ?></div>
                                         <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                      <?php endif; ?>
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                                <div class="row">
                                	<div class="col-sm-6"><h3 class="panel-title">
                                    <i class="fa fa-users" aria-hidden="true"></i> Archive Listing</h3></div>
                                    <div class="col-sm-6">
                                    
                                        <div class="row shorting-area">
                                            <div class="col-sm-2 purchase-filter pull-right">
                                            <a href="javascript:void(0)" onclick="getDocumentVendor(0);" id="show-btn" class="btn btn-success">
                                            <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                            <div class="clearfix"></div>
 <div class="panel-body custome-table">
<div class="row shorting-area">
<div class="col-sm-12">
<div id="mySidenavR" class="sidenavR" style="display:none;">
<div class="filter-right">
<div class="filter-area">
<!--<h5>Vendor List Filter</h5>-->
    <a href="javascript:void(0)"  class="closebtn" id="hide-filter">×</a>
        
        
        <div class="col-sm-2">
           
            <select class="select-drop" id="name" name="vendore_name">
                <option value="">Select Vendor</option>               
            </select>
        </div>
        
        
        <div class="col-sm-2">
            
            <select class="select-drop" id="type" name="type">
                <option value="">Select Document Type</option>                
            </select>
        </div>
        <div class="col-sm-2">
            
            <select class="select-drop" id="month" name="month">
                <option value="">Select Month</option>
            </select>


        </div>
        <div class="col-sm-2">
        <div class="form-group">
                <div class="input-group date">
                    <input type="text" class="form-control datepicker" placeholder="Date"  id="date" name="date"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-2">
            <a href="javascript:void(0)" class="btn btn-success" onclick="applyFilter(0);">Apply Filter</a>
            <div class="clearfix"></div>
        </div>
         <div class="col-sm-1"> 
                          <a href="javascript:void(0)" class="btn btn-success dateCheck reset" onclick="">Clear</a>
                          <div class="clearfix"></div>  
                         </div>
<div class="clearfix"></div>        
    </div>
    <div class="clearfix"></div>
    </div>    
</div>
                            
</div>
</div>

                      
                       
                           
                            <span id="filterResult">
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Vendor Name</th>
                                                <th class="text-center">Document Type</th>
                                                <th class="text-center">Month</th>
                                                <th class="text-center">Remarks</th>
                                                <th class="text-center">Date & Time</th>
                                                <th class="text-center">File</th>
                                                
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <?php $__currentLoopData = $document_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <tr>
                                        <td class="text-center"><?php if(!empty($data->getDocVendor->name)): ?><?php echo e($data->getDocVendor->name); ?><?php endif; ?></td>
                                        <td class="text-center"><?php if(!empty($data->getDocType->type_name)): ?><?php echo e($data->getDocType->type_name); ?><?php endif; ?></td>
                                        <td class="text-center"><?php echo e($data->month); ?></td>
                                        <td class="text-center"><?php echo e($data->remarks); ?></td>
                                        <td class="text-center"><?php echo e(date('d/m/Y g:i A',strtotime($data->created_at))); ?></td>
                                        <td class="text-center">
                                         <?php 
										$document_list_view = DB::table('document_files')->select('*')->where('document_id','=',$data->id)->orderBy('id','asc')										->get();								  $inc=0;
										if(!empty($document_list_view)){
											foreach($document_list_view as $docs){
										if($docs->document_file!='')
										{
										   if(file_exists('Document_upload/'.$docs->document_file))
										   { 
										    $inc++;
											$document_file=URL::to('/Document_upload').'/'.$docs->document_file;
											echo '<a href="'.$document_file.'" target="_blank" >Document'.$inc.'</a>'.'<br>';
										   }
										}
										
                                        }}?>
                                        </td>
                                        
                                       
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                      
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                              <div class="col-sm-6">
                                <div class="dataTables_info" role="status" aria-live="polite">Showing <?php echo e($document_list->count()); ?> of <?php echo e($document_list->total()); ?> entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                        <?php echo e($document_list->links()); ?>

                                    </nav>
                                  </div>
                              </div>
                              </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
      <script type="text/javascript">
        // $(document).ready(function () {
            
        //     $('#example').dataTable({
        //         'order': [[ 0, "asc" ]],//Default Column Attribute
        //         'pageLength': 500,//Per Page Item
        //         //'sDom': 't',//Hide All Info From top and bottom
        //         'aTargets': [-1], //1st one, start by the right/
        //         "paging":   false,
        //         "info":     false,
        //         "searching": false,
        //         //'dom': 'Bfrtip',
        //         //'buttons': ['excel', 'pdf', 'print']//['copy', 'csv', 'excel', 'pdf', 'print']
        //     });
        // });
        function ConfirmActive(id)
        {
            var x = confirm("Are you sure you want to Activate?");
            if (x)
                return true;
            else
                return false;
        }
        function ConfirmDeactive(id)
        {
            var x = confirm("Are you sure you want to Archieve?");
            if (x)
                return true;
            else
                return false;
        }
$(document).ready(function () {        
        applyFilter(0);
    });
$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        $('#load a').css('color', '#dfecf6');
        //$('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');

        var url = $(this).attr('href'); 
        var page=$(this).attr('href').split('page=')[1];
        //alert(page);
        
        var name=$("#name").val();
        var type=$("#type").val();
        var month=$("#month").val();
        var date=$("#date").val();
        var status=0;
        

        
        
     
        filter(page,type,month,name,date,status);
        //window.history.pushState("", "", url);
    });

    
});

$( "#hide-filter" ).click(function() {
    var name=$("#name").val('');
    var type=$("#type").val('');
    var month=$("#month").val('');
    var date=$("#date").val('');
    var status=0;
    filter(); 
});

function applyFilter(value){ 
    
        var name=$("#name").val();
        var type=$("#type").val();
        var month=$("#month").val();
        var date=$("#date").val();
        var status=value;
       
    
        filter(1,type,month,name,date,status);
}

function filter(page=1,type='',month='',name='',date='',status=0){ 

    $('.loading').show();
    
    $.ajax({
        type: 'GET', 
        url: '<?php echo e(route("filter-document-return")); ?>',
        data: {page:page,type:type,month:month,name:name,date:date,status:status},
        success:function(data)
        {
            $('#filterResult').html(data);
            $('.loading').hide();
        },
        error: function (error) 
        {
            
            alert('Error Occured Please Try Again');
        }
    })
}

function getDocumentMonth(value)
    {
        $.ajax({
            type: 'GET',
            url: '<?php echo e(route("get-document-month")); ?>',
            data: {value:value},
            success:function(data)
            {
                $("#month").html(data['month']);
            }
            

        });

        
    }
    function getDocumentVendor(value)
    {
        $('#mySidenavR').show();

        $.ajax({
            type: 'GET',
            url: '<?php echo e(route("get-document-vendor")); ?>',
            data: {value:value},
            success:function(data)
            {

                $("#name").html(data['vendor']);
                getDocumentType(0);
            }
            

        });

        
    }
    function getDocumentType(value)
    {
        $.ajax({
            type: 'GET',
            url: '<?php echo e(route("get-document-type")); ?>',
            data: {value:value},
            success:function(data)
            {
                $("#type").html(data['type']);
                getDocumentMonth(0);
            }
           

        });

        
    }

 //--For Clearing Filter Data Elements-----//
    $(".reset").bind("click", function() {
    $("#name").val("");
    $("#month").val("");
    $("#type").val("");
    $("#date").val("");

    }); 
   



setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 2000);


 $(document).ready(function () {
                $('.datepicker').datepicker({                    
                    format: "dd/mm/yyyy",
                    autoclose: true

                   
                });
                 });

        </script>
<?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>   