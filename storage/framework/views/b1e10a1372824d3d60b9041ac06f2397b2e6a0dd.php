<div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                               <!--  <th class="text-center">Sr. No</th> -->
                                                <th class="text-center">Image</th>
                                                <th class="text-center">PO. No</th>
                                                <th class="text-center">Vendor Name</th>
                                                <th class="text-center">PO. Date</th>
                                                <th class="text-center">Due. Date</th>
                                                <th class="text-center">Item Code</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Width/Size</th>
                                                 <th class="text-center">Cat 6</th>
                                                <th class="text-center">Price</th>
                                                <th class="text-center">Order Qty</th>
                                                <th class="text-center">Received Qty</th>
                                                <th class="text-center">Cancelled Qty</th>
                                               
                                                <th class="text-center">Pending Qty</th>
                                               
                                            </tr>
                                        </thead>
                                    <tbody id="search_result_body">
                                    <?php $inc=0; ?>
                                    <?php $__currentLoopData = $purchaseOrderList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $eachOrder): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <?php $inc++;
                                     $pending = ($eachOrder->ORDQTY - ($eachOrder->RCQTY+$eachOrder->CNLQTY ));
                                     if($pending<=0)
                                        $pending=str_replace('-', 'Adhoc:', $pending);
                                    ?>
                                     
                                    <tr>
                                        <!-- <td class="text-center"><?php echo e($inc); ?></td> -->
                                        <td class="text-center">
                            <div class="gal"> 
                             <?php 
                               $pGalList=DB::table('product_gallery')->select('*')->where('product_id','=',$eachOrder->product_id)->get();
                               if($eachOrder->image!='' && file_exists('upload/product/'.$eachOrder->image))
                               $main_image=URL::to('/upload/product').'/'.$eachOrder->image;
                               if(empty($main_image) && !empty($pGalList) && count($pGalList)>0)
                               {
                                  foreach ($pGalList as $pGal) 
                                  { 
                                    if($pGal->product_image!='' && file_exists('upload/product/'.$pGal->product_image))
                                    {
                                      $main_image=URL::to('/upload/product').'/'.$pGal->product_image;
                                    }
                                  }
                               }
                               if(empty($main_image))
                                $main_image=URL::to('/img').'/pre-order-img.jpg';
                              ?>
                               <a class="fancybox" rel="group<?php echo e($inc); ?>" href="<?php echo e($main_image); ?>" >
                                <img src="<?php echo e($main_image); ?>" width="56">
                               </a>
                               <?php 
                               if(!empty($pGalList) && count($pGalList)>0)
                               {
                                foreach ($pGalList as $pGal2) 
                                {
                                    if($pGal2->product_image!='' && file_exists('upload/product/'.$pGal2->product_image))
                                    {
                                      $main_image=URL::to('/upload/product').'/'.$pGal2->product_image;

                                ?>                                               
                               <a class="fancybox" rel="group<?php echo e($inc); ?>" href="<?php echo e($main_image); ?>" style="display:none;"><img src="<?php echo e($main_image); ?>" alt=""></a>
                                <?php 
                                }
                                }
                                }
                               ?> 
                            </div>
                        </td>
                                        <td class="text-center po-no<?php echo $inc;?>"><?php echo e($eachOrder->PONO); ?></td>
                                        <td class="text-center v-name<?php echo $inc;?>"><?php echo e($eachOrder->name); ?></td>
                                        <td class="text-center po-date<?php echo $inc;?>"><?php echo e(date('d/m/Y',strtotime($eachOrder->ORDDT))); ?></td>
                                        <td class="text-center po-date<?php echo $inc;?>"><?php echo e(date('d/m/Y',strtotime($eachOrder->DUEDATE))); ?></td>
                                        <?php if(isset($eachOrder->ICODE)): ?>
                                        <td class="text-center ICODE<?php echo $inc;?>"><?php echo e($eachOrder->ICODE); ?></td>
                                        <?php else: ?>
                                        <td class="text-center ICODE<?php echo $inc;?>"></td>
                                        <?php endif; ?>
                                        <td class="text-center cat-1<?php echo $inc;?>"><?php echo e($eachOrder->cat1); ?></td>
                                        <td class="text-center cat-2<?php echo $inc;?>"><?php echo e($eachOrder->cat2); ?></td>
                                        <td class="text-center cat-3<?php echo $inc;?>"><?php echo e($eachOrder->cat3); ?></td>
                                        <td class="text-center cat-4<?php echo $inc;?>"><?php echo e($eachOrder->cat4); ?></td>
                                        <td class="text-center cat-4<?php echo $inc;?>"><?php echo e($eachOrder->CNAME6); ?></td>
                                        <td class="text-center price<?php echo $inc;?>"><?php echo e($eachOrder->RATE); ?></td>
                                        <td class="text-center order-qty<?php echo $inc;?>"><?php echo e($eachOrder->ORDQTY); ?></td>
                                        <td class="text-center received-qty<?php echo $inc;?>"><?php echo e($eachOrder->RCQTY); ?></td>
                                        <td class="text-center cancelled-qty<?php echo $inc;?>"><?php echo e($eachOrder->CNLQTY); ?></td>
                                       
                                        <td class="text-center pending-qty<?php echo $inc;?>"><?php echo e($pending); ?></td>
                                        
                                    </tr>
                                    
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                </tbody>
                               </table>
                              </div>
                             
 <script>
                             $(document).ready(function () {
            
            $('#example').dataTable({
                'order': [[ 3, "asc" ]],//Default Column Attribute
                'pageLength': 30,//Per Page Item
                 "searching": false,
                'dom': 'Bfrtip',//Header Info               
                'buttons': [ {extend: 'excel',exportOptions: {columns: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]},footer: false}],
                'aoColumnDefs': [          
                                   { aTargets: [ 0 ], bSortable: false },
                                   { aTargets: [ 1 ], bSortable: false },
                                   { aTargets: [ 2 ], bSortable: false },
                                   { aTargets: [ 3 ], bSortable: true },
                                   { aTargets: [ 4 ], bSortable: true },
                                   { aTargets: [ 5], bSortable: false },
                                   { aTargets: [ 6 ], bSortable: true },
                                   { aTargets: [ 7 ], bSortable: false },
                                   { aTargets: [ 8 ], bSortable: false },
                                   { aTargets: [ 9 ], bSortable: false },
                                   { aTargets: [ 10 ], bSortable: false },
                                   { aTargets: [ 11 ], bSortable: false },
                                   { aTargets: [ 12 ], bSortable: false },
                                   { aTargets: [ 13 ], bSortable: false },
                                   { aTargets: [ 14 ], bSortable: false },
                                   { aTargets: [ 15 ], bSortable: true }
                                ],
                "oLanguage": {
                                "oPaginate": {
                                                "sNext": '<i class="fa fa-chevron-right" ></i>',
                                                "sPrevious": '<i class="fa fa-chevron-left" ></i>'
                                            }
                            }
            });
        });
                              </script>