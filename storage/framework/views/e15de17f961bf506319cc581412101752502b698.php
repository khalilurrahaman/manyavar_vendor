<?php 
use App\Http\Controllers\Controller;
if(Controller::userAccessibilityCheck('goods-return-comments'))
	$goods_return_comments_exist=1;
	
?>

<div class="panel-body custome-table">
                            <div class="table-responsive ">
                                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <!-- <th class="text-center">Prodict Id</th> -->
                                            <th class="text-center">Image</th>
                                            <th class="text-center">Vendor Name</th>
                                            <th class="text-center">Design</th>
                                            <th class="text-center">Colour</th>
                                            <th class="text-center">Defective Remarks</th>
                                            <th class="text-center">Quantity</th>
                                            <th class="text-center">PRN No</th>
                                            <th class="text-center">PRN Date</th>
                                            <th class="text-center">Docket No</th>
                                            <th class="text-center">Courier Name</th>
                                            <!-- <th class="text-center">Admin Remarks</th> -->
                                            <th class="text-center">Vendor Remarks</th>
                                            <th class="text-center">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     <?php $inc=0;?>
                                        <?php $__currentLoopData = $returnList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $eachReturn): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                         <?php $inc++;?>
                                        <tr>
                                            <td class="text-center">
                            <div class="gal"> 
                             <?php 
                               $main_image=URL::to('/img').'/pre-order-img.jpg';
                               if($eachReturn->image!='' && file_exists('upload/temp/'.$eachReturn->image))
                               $main_image=URL::to('/upload/temp').'/'.$eachReturn->image;
                               
                              ?>
                               <a class="fancybox" rel="group<?php echo e($inc); ?>" href="<?php echo e($main_image); ?>" >
                                <img src="<?php echo e($main_image); ?>" width="56">
                               </a>
                                                               
                               <a class="fancybox" rel="group<?php echo e($inc); ?>" href="<?php echo e($main_image); ?>" style="display:none;"><img src="<?php echo e($main_image); ?>" alt=""></a>
                              
                            </div>
                        </td>
                                            <td class="text-center"><?php echo e($eachReturn->name); ?></td>
                                            <td class="text-center"><?php echo e($eachReturn->category1); ?></td>
                                            <td class="text-center"><?php echo e($eachReturn->category2); ?></td>
                                            <td class="text-center" title="<?php echo e($eachReturn->reason); ?>">
                                            <?php
                                            $i=strlen($eachReturn->reason); 
                                            if(strlen($i>25)) 
                                                echo substr($eachReturn->reason,0,22).'...';
                                            else 
                                                echo $eachReturn->reason;

                                            ?>
                                                
                                            </td>
                                            <td class="text-center"><?php echo e($eachReturn->return_quantity); ?></td>
                                            <td class="text-center"><?php echo e($eachReturn->prn_no); ?></td>
                                            <td class="text-center">
                                            <?php if($eachReturn->prn_date!='0000-00-00'): ?>
                                            <?php echo e(date('d/m/Y',strtotime($eachReturn->prn_date))); ?>

                                            <?php endif; ?>

                                            </td>
                                            <td class="text-center"><?php echo e($eachReturn->docket_no); ?></td>
                                            <td class="text-center"><?php echo e($eachReturn->courier_name); ?></td>
                                            <!-- <td class="text-center" title="<?php echo e($eachReturn->comments); ?>">
                                            <?php
                                            $i=strlen($eachReturn->comments); 
                                            if(strlen($i>25)) 
                                                echo substr($eachReturn->comments,0,22).'...';
                                            else 
                                                echo $eachReturn->comments;

                                            ?>
                                                
                                            </td> -->
                                            <td class="text-center" title="<?php echo e($eachReturn->vendor_comments); ?>">
                                             <?php if(!empty($goods_return_comments_exist)){?>
                                             
                                            <?php
                                            $i=strlen($eachReturn->vendor_comments); 
                                            if(strlen($i>25)) 
                                                echo substr($eachReturn->vendor_comments,0,22).'...';
                                            else 
                                                echo $eachReturn->vendor_comments;

                                            ?>
                                             <?php }?>
                                            </td>
                                            <td class="text-center">
                                            <?php if($eachReturn->status==1): ?><a href="javascript:void(0)" class="btn btn-success">Pending</a>
                                            <?php else: ?><a href="javascript:void(0)" class="btn btn-danger">Closed</a>
                                            <?php endif; ?></td>
                                        </tr> 
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>                          
                                    </tbody>
                               </table>
                          </div>
                       
                        </div>
                    </div>
                </div>
            </div>
<script type="text/javascript">
$(document).ready(function () {
            
            $('#example').dataTable({
                'order': [[ 0, "asc" ]],//Default Column Attribute
                'pageLength': 10,//Per Page Item
                 "searching": false,
                'dom': 'Bfrtip',//Header Info               
                'buttons': [ {extend: 'excel',exportOptions: {columns: [1,2,3,4,5,6,7,8,9,10,11]},footer: false}],
                'aoColumnDefs': [          
                                   { aTargets: [ 0 ], bSortable: false },
                                   { aTargets: [ 1 ], bSortable: true },
                                   { aTargets: [ 2 ], bSortable: false },
                                   { aTargets: [ 3 ], bSortable: false },
                                   { aTargets: [ 4 ], bSortable: false },
                                   { aTargets: [ 5], bSortable: true },
                                   { aTargets: [ 6 ], bSortable: false },
                                   { aTargets: [ 7 ], bSortable: false },
                                   { aTargets: [ 8 ], bSortable: false },
                                   { aTargets: [ 9 ], bSortable: false },
                                   { aTargets: [ 10 ], bSortable: false },
                                   { aTargets: [ 11 ], bSortable: false }
                                ],
                "oLanguage": {
                                "oPaginate": {
                                                "sNext": '<i class="fa fa-chevron-right" ></i>',
                                                "sPrevious": '<i class="fa fa-chevron-left" ></i>'
                                            }
                            }
            });
        });
     setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 2000);
</script>