<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title>Manyavar Vendor Panel</title>

    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <link rel="shortcut icon" href="<?php echo e(asset('favicon.png')); ?>" type="image/x-icon" />
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/admin.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="container-fluid">
             	<div class="screen-login">
                	<div class="row">
                   		<div class="login-form">
                            <div class="head">
                                <img src="img/admin.jpg" alt="">
                              
                            </div>
                            <?php 
							if(isset($_COOKIE['remember_me_coockie'])) 
							{
								$coockie_records=unserialize($_COOKIE['remember_me_coockie']);//echo dd(unserialize($_COOKIE['remember_me_coockie']));
							}
							?>
                            <form class="card" role="form" method="POST" action="<?php echo e(url('/login')); ?>" autocomplete="off">
                            <?php echo e(csrf_field()); ?>

                                <div class="input-container">

                                    <!--<input type="text" name="username" autocomplete="off" id="username" value="<?php echo e(old('username')); ?>"/>-->
                                    <input type="text" name="username" autocomplete="off" id="username" value="<?php if(!empty($coockie_records['username'])){echo $coockie_records['username'];}?>"/>
                                    <label>Username </label>
                                    <div class="bar"></div>
                                </div>
                                <span class=" error class_err" id="username_msg" style="position: relative;top: -25px;"></span>

                                <div class="input-container">
                                    <input type="password" name="password" autocomplete="off" id="password" value="<?php if(!empty($coockie_records['password'])){echo $coockie_records['password'];}?>"/>
                                    <label>Password</label>
                                    <div class="bar"></div>
                                </div>
                                    <span class=" error class_err" id="password_msg" style="position: relative;top: -25px;"></span>
                                    <?php if(isset($errors)): ?>
                                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <span  id="password_msg1" style="color:red;position: relative;top: -25px;"><?php echo e($error); ?></span>
                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                     <?php endif; ?>
                                <div class="row remember-me">
                                <div class="col-sm-6">
                                <label><input type="checkbox" name="remember" <?php if(!empty($coockie_records)){echo 'checked';}?>/> Remember me</label>
                                </div>
                                <div class="col-sm-6">
                                    <label class="pull-right"><a href="<?php echo e(url('/forgot-password')); ?>" class="text-right">Forgot Password?</a></label>
                                </div>
                                </div>
                                <div class="row btn-top">
                                <div class="col-sm-12">
                                	<button type="submit" class="btn btn-orange" onclick="return validateForm();">Login</button>
                                </div>
                                </div>
                            </form>
							</div>
            				<div class="copy-right">
								<p> © 2016 Manyavar Vendor Panel. All rights reserved</p>
							</div>
                   	</div>
             	</div>

            </div>
        </div>
    </div> 
    <style>
	input:-webkit-autofill {background-color:transparent !important; border:0 !important; }
	</style>
 <script src="js/jquery-2.1.1.js"></script>   
    <script>
	$(document).ready(function(){
    $( document ).on( 'focus', ':input', function(){
        $( this ).attr( 'autocomplete', 'off' );
    });
});


    setInterval(function () { 
       
        window.location.reload();
    },180000);

	</script>
    <script type="text/javascript">
 function validateForm()
  {    
    var ret_val=true;   
   
    var name=$("#username").val();
    if(name==''){$("#username_msg").text('Please Enter Username  !').css('color','red').show();ret_val=false;}
    else{$("#username_msg").hide();}

    var department_description=$("#password").val();
    if(department_description==''){$("#password_msg").text('Please Enter Password  !').css('color','red').show();ret_val=false;}
    else{$("#password_msg").hide();}


    return ret_val;

  } 

setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#password_msg1" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 2000);
    



</script>