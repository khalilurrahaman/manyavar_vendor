<div class="table-responsive view-table">
    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="text-center">Sl. No.</th>
                <th class="text-center">Image</th>
                <th class="text-center">REQ ID</th>
                <th class="text-center">Date</th>
                <th class="text-center">Design</th>
                <th class="text-center">Colour</th>
                <th class="text-center">Width/Size</th>
                 <th class="text-center">Rate</th>
                <th class="text-center">Vendor Name</th>
                <th class="text-center">Remarks</th>
                <th class="text-center">Comment</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $i= 1; $inc=0;?>
            <?php if(count($rfqList)>0): ?>
            <?php $__currentLoopData = $rfqList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$eachRfq): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
             <?php $inc++;?>
            <tr>
                <td class="text-center"><?php echo e($i); ?></td>
                <td class="text-center">
                <div class="gal"> 
                             <?php 
                               $pGalList=DB::table('product_gallery')->select('*')->where('product_id','=',$eachRfq->product_id)->get();
                               if($eachRfq->image!='' && file_exists('upload/product/'.$eachRfq->image))
                               $main_image=URL::to('/upload/product').'/'.$eachRfq->image;
                               if(empty($main_image) && !empty($pGalList) && count($pGalList)>0)
                               {
                                  foreach ($pGalList as $pGal) 
                                  { 
                                    if($pGal->product_image!='' && file_exists('upload/product/'.$pGal->product_image))
                                    {
                                      $main_image=URL::to('/upload/product').'/'.$pGal->product_image;
                                    }
                                  }
                               }
                               if(empty($main_image))
                                $main_image=URL::to('/img').'/pre-order-img.jpg';
                              ?>
                               <a class="fancybox" rel="group<?php echo e($inc); ?>" href="<?php echo e($main_image); ?>" >
                                <img src="<?php echo e($main_image); ?>" width="56">
                               </a>
                               <?php 
                               if(!empty($pGalList) && count($pGalList)>0)
                               {
                                foreach ($pGalList as $pGal2) 
                                {
                                    if($pGal2->product_image!='' && file_exists('upload/product/'.$pGal2->product_image))
                                    {
                                      $main_image=URL::to('/upload/product').'/'.$pGal2->product_image;

                                ?>                                               
                               <a class="fancybox" rel="group<?php echo e($inc); ?>" href="<?php echo e($main_image); ?>" style="display:none;"><img src="<?php echo e($main_image); ?>" alt=""></a>
                                <?php 
                                }
                                }
                                }
                               ?> 
                            </div>
                </td>
                <td class="text-center"><?php echo e($eachRfq->req_id); ?></td>
                <td class="text-center"><?php echo e(date('d/m/Y',strtotime($eachRfq->created_at))); ?></td>
                <td class="text-center"><?php echo e($eachRfq->cat1); ?></td>
                <td class="text-center"><?php echo e($eachRfq->cat2); ?></td>
                <td class="text-center"><?php echo e($eachRfq->cat4); ?></td>
                <td class="text-center"><?php echo e($eachRfq->RATE); ?></td>
                <td class="text-center"><?php echo e($eachRfq->name); ?></td>
                <td class="text-center"><?php echo e($eachRfq->indv_remarks); ?></td>
                <td class="text-left width-e ">
                    <?php if(!empty($eachRfq->rfq_comments) && count($eachRfq->rfq_comments)>0): ?>
                    <?php                                        
                    if(!empty($eachRfq->rfq_comments) && count($eachRfq->rfq_comments)>0)
                    {   
                      if(count($eachRfq->rfq_comments[0]->comments)<=20)                                 
                         echo ($eachRfq->rfq_comments[0]->comments);
                      else
                       echo substr($eachRfq->rfq_comments[0]->comments,0,18).'..';
                    }
                    ?>
                    <a href="<?php echo e(route('admin-comments',$eachRfq->rfq_details_id)); ?>" class=" pull-right btn btn-warning btn-xs"><?php echo count($eachRfq->rfq_comments)?> Comment(s)</a>
                     
                     <?php endif; ?>
                 </td>

                <td class="text-center">
                    <a href="javascript:void(0);"  class="btn btn-danger btn-xs">Concluded</a>
                </td>


            </tr>
            <?php $i++?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            <?php else: ?>
                <td class="text-center" colspan="10" > No Record Found </td>
            <?php endif; ?>                         
        </tbody>
    </table>
</div>

<div class="row">
  <div class="col-sm-5">
    <div class="dataTables_info" role="status" aria-live="polite">Showing <?php echo e($rfqList->count()); ?> of <?php echo e($rfqList->total()); ?> entries</div>
  </div>
  <div class="col-sm-7">
        <nav aria-label="Page navigation" class="pull-right">
            <?php echo e($rfqList->links()); ?>

        </nav>
      </div>
  </div>
  <!-- /#Hover Image -->
    <script type="text/javascript">$('img').on('mouseover', function() {
    var self = this,
        i = 0,
        images = $(this).data('mouseover').split(/\s+/);
    
    (function nextImage() {
        var next = images[i++ % images.length].split('#');
        $(self).data('timeout', setTimeout(function() {
            self.src = next[0];
            nextImage();
        }, next[1]));
    })();
    
}).on('mouseout', function() {
    clearTimeout($(this).data('timeout'));
    this.src = $(this).attr('src');
});
</script>