<?php 
use App\Http\Controllers\Controller;
if(Controller::userAccessibilityCheck('rate-accept'))
	$action_accept_exist=1;
if(Controller::userAccessibilityCheck('rate-reject'))
	$action_reject_exist=1;
if(Controller::userAccessibilityCheck('action-close'))
	$action_close_exist=1;
if(Controller::userAccessibilityCheck('action-comments'))
	$action_comments_exist=1;
?>
                            <div class="panel-body custome-table">
                            
                                <div class="table-responsive view-table">

                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Sr. No</th>
                                                <th class="text-center">Request</th>
                                                <th class="text-center">PO Details</th>
                                                <th class="text-center">Product Info<br>(Design and Color)</th>
                                                <th class="text-center">Vendor Name</th>
                                                <th class="text-center">Old Values<br>(Price and Width)</th>
                                                <th class="text-center">Requested Vaules<br>(Price and Width)</th>
                                                <th class="text-center">Negotiated Rate</th>
                                                <th class="text-center">Accepted Values<br>(Price and Width)</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-center">Action</th>
                                                <th class="text-center">Last Comments</th>
                                            </tr>
                                        </thead>
                                    <tbody id="req_request">
                                    <?php if(!empty($request_list)): ?>
                                    <?php $inc=$request_list->perPage() * ($request_list->currentPage()-1);;?>
                                    <?php $__currentLoopData = $request_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $req): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <?php $inc++; ?>
                                    <tr>
                                    
                                      <td class="text-center"><?php echo e($inc); ?></td>
                                      <td class="text-center">
                                      <?php echo e(date("d/m/y",strtotime($req->created_at))); ?><br>
                                      <?php 
                                        $type='';
                                        if($req->action_type=='rate_change')
                                          $type='Rate Change';          
                                        elseif($req->action_type=='width_change') 
                                          $type='Width Change';
                                         

                                        ?>
                                       <?php echo e($type); ?>

                                      </td>  
                                      <td class="text-center">
                                        <span class="po-no<?php echo $inc;?>"><?php echo e($req->po_no); ?></span><br>
                                        <span class="po-date<?php echo $inc;?>"><?php echo e(date("d/m/y",strtotime($req->order_date))); ?></span>

                                      </td>   
                                      <td class="text-center">
                                       <?php echo e($req->cat1); ?><br>
                                       <?php echo e($req->cat2); ?>

                                      </td> 
                                        <td class="text-center">
                                        <?php if(!empty($req->ven_dtls->name)): ?><?php echo e($req->ven_dtls->name); ?><?php endif; ?>
                                        </td>
                                        <td class="text-center">
                                        <?php if(!empty($req->old_value)): ?><?php echo e($req->old_value); ?><?php endif; ?><br>
                                        <?php if(!empty($req->old_width)): ?><?php echo e($req->old_width); ?>"<?php endif; ?>
                                        </td>
                                        <td class="text-center">
                                       <?php if(!empty($req->new_rate)): ?><?php echo e($req->new_rate); ?><?php endif; ?><br>
                                        <?php if(!empty($req-> new_width)): ?><?php echo e($req->new_width); ?>"<?php endif; ?>
                                        </td>
                                         <td class="text-center">
                                         <?php if($req->new_rate!=$req->negotiated_rate): ?>
                                         <?php echo e($req->negotiated_rate); ?><?php endif; ?>
                                         </td>
                                        <td class="text-center">
                                        <?php if($req->new_rate==$req->negotiated_rate): ?><?php echo e($req->negotiated_rate); ?><?php else: ?><?php echo e($req->accepted_rate); ?><?php endif; ?><br>
                                        <?php if(!empty($req->new_width)): ?><?php echo e($req->new_width); ?>"<?php endif; ?>
                                        </td>
                                        <td class="text-center">
                                       
                                       <?php if($req->status=='new'): ?>
                                       <span class="sm-btn btn-primary btn-xs">New</span>
                                       <?php elseif($req->status=='close'): ?>
                                       <span class="sm-btn btn-primary btn-warning">Accepted</span>
                                       <?php elseif($req->status=='inprocess'): ?>
                                       <span class="sm-btn btn-primary btn-xs">In Process</span>
                                       <?php elseif($req->status=='accept'): ?>
                                       <span class="sm-btn btn-primary btn-warning">Accepted</span>
                                       <?php elseif($req->status=='reject'): ?>
                                       <span class="sm-btn btn-primary btn-danger">Rejected</span> 
                                       <?php endif; ?>
                                        </td>
                                        
                                        <td class="text-center">
                                        <?php if($req->status=='new'): ?>
                                         <?php if($req->notify==1): ?>
                                         <span class="blnk_txt">Please Take Action</span>
                                         <?php endif; ?><br>
                                        <?php if(!empty($action_accept_exist)){?> 
                                        <a data-toggle="modal" data-target="#vendorRequestaccept" data-placement="top" title="" class="btn btn-success vendorRequestaccept" unique-key="<?php echo $inc;?>" action_id="<?php echo e($req->id); ?>" action_type="<?php echo e($req->action_type); ?>" old_value="<?php echo e($req->old_value); ?>" req_rate="<?php echo e($req->new_rate); ?>" old_width="<?php echo e($req->old_width); ?>" req_width="<?php echo e($req->new_width); ?>" cat_1_details="<?php echo e($req->cat1); ?>" cat_2_details="<?php echo e($req->cat2); ?>">Accept</a>
                                         <?php }?> 
                                        <?php if(!empty($action_reject_exist)){?> 
                                        <a data-toggle="modal" data-target="#vendorRequestreject" data-placement="top" title="" class="btn btn-danger vendorRequestreject" unique-key="<?php echo $inc;?>" action_id="<?php echo e($req->id); ?>" action_type="<?php echo e($req->action_type); ?>" req_rate="<?php echo e($req->new_rate); ?>" old_width="<?php echo e($req->old_width); ?>" req_width="<?php echo e($req->new_width); ?>" old_value="<?php echo e($req->old_value); ?>">Reject</a>
                                         <?php }?> 
                                        <?php elseif($req->status=='inprocess'): ?>
                                       <span class="sm-btn btn-primary btn-xs">Pending</span>
                                       <?php elseif($req->status=='accept'): ?>
                                       <?php if(!empty($action_close_exist)){?> 
                                       <a class="btn btn-success vendorRequestclose" unique-key="<?php echo $inc;?>" action_id="<?php echo e($req->id); ?>" action_type="<?php echo e($req->action_type); ?>" old_value="<?php echo e($req->old_value); ?>" req_rate="<?php echo e($req->new_rate); ?>" old_width="<?php echo e($req->old_width); ?>" req_width="<?php echo e($req->new_width); ?>" class="btn btn-success btn-xs" onclick="getpolist(<?php echo e($req->user_id); ?>)">Close</a>
                                        <?php }?> 
                                        <?php elseif($req->status=='reject'): ?>
                                        <span class="sm-btn btn-primary btn-warning">Closed</span> 
                                        <?php elseif($req->status=='close'): ?>
                                        <span class="sm-btn btn-primary btn-warning">Closed</span> 
                                        <?php endif; ?>
                                        </td>

                                        <td class="text-left width-e">
                                        <?php if(!empty($action_comments_exist)){?> 
                                        <?php if(!empty($req->req_comments) && count(($req->req_comments))>0): ?>
                                        <?php                                       
                                       
                                        if(strlen($req->req_comments[0]->comments)<=20)                                 
                                           echo ($req->req_comments[0]->comments);
                                        else
                                         echo substr($req->req_comments[0]->comments,0,18).'..';  

                                        ?>
                                        <a href="<?php echo e(route('admin-requestwise-comments', $req->id)); ?>" class="pull-right btn btn-warning btn-xs">
                                        <?php echo count($req->req_comments)?>
                                        Comment(s)</a>
                                        <?php else: ?>
                                        <a href="<?php echo e(route('admin-requestwise-comments', $req->id)); ?>" class="btn btn-warning btn-xs pull-right">
                                        
                                        Comments</a>
                                        
                                         <?php endif; ?>
                                         <?php }?> 
                                         </td>

                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                    <?php endif; ?> 
                                     
                                </tbody> 
                               </table>

                              </div>
                              <div class="row">
                              <div class="col-sm-6">
                                <div class="dataTables_info" role="status" aria-live="polite">Showing <?php echo e($request_list->count()); ?> of <?php echo e($request_list->total()); ?> entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                      <?php echo e($request_list->links()); ?>   
                                    </nav>
                                  </div>
                              </div>
                            </div>

<script>
// $(document).ready(function () {
			
//             $('#example').dataTable({
// 				'order': [[ 0, "asc" ]],//Default Column Attribute
// 				'pageLength': 500,//Per Page Item
// 				//'sDom': 't',//Hide All Info From top and bottom
// 				'aTargets': [-1], //1st one, start by the right/
// 				"paging":   false,
//         		"info":     false,
// 				"searching": false,
// 				'dom': 'Bfrtip',
// 				'buttons': ['excel']//['copy', 'csv', 'excel', 'pdf', 'print']
//             });
//         });

</script>                            
 

 


                          