<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<link href="<?php echo e(URL::asset('css/selectbox/bootstrap-select.min.css')); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(URL::asset('css/selectbox/multi-select.css')); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(URL::asset('css/selectbox/select2.min.css')); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(URL::asset('css/selectbox/select2-bootstrap.min.css')); ?>" rel="stylesheet" type="text/css"/>
    <!--add user popup Modal Start-->

<div class=" quotation-pop" role="dialog">
    <div class="modal-dialog">


    <?php if(!empty($errors->all()) ): ?>

                    <div class="btn btn-success" id="hide-me" style="width:100%;background-color: green;border: none;"><?php echo e($errors->all()[0]); ?></div>
                    <?php endif; ?>
        <!-- Modal content-->
        <div class="modal-content  none-shadow">
        
          <div class="modal-header">
            <h4 class="modal-title">
            Add Permission for Role : <?php echo e($role_user->name); ?>

            <div class="pull-right"><a href="<?php echo e(route('role-listing')); ?>" class="btn btn-info">Back</a></div>
            </h4>
          </div>


            <div class="modal-body">
                <form id="user-form" action="<?php echo e(route('insert-add-permission')); ?>" method="post">
                <?php echo e(csrf_field()); ?>

                  <input type="hidden" name="role_id" value="<?php echo e($role_user->id); ?>">
                  
                    <div class="row shorting-area pricechange">
                        <div class="col-sm-12">
                            <div class="col-sm-7"><h5>Permission List</h5></div>
                            <div class="col-sm-5"><h5>Permission Given</h5></div>
                        </div>
                        <div class="col-sm-12">
                            <select multiple="multiple" class="multi-select" id="my_multi_select2" name="permission[]">
                                <?php $__currentLoopData = $allPermission; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $moduleName => $allModulePermission): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                  <optgroup label="<?php echo e($moduleName); ?>">
                                    <?php $__currentLoopData = $allModulePermission; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $eachPermission): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                      <?php if(in_array($eachPermission->slug,$assigned_permissions)): ?>
                                        <option selected value="<?php echo e($eachPermission->id); ?>"><?php echo e($eachPermission->name); ?></option>
                                      <?php else: ?>
                                        <option value="<?php echo e($eachPermission->id); ?>"><?php echo e($eachPermission->name); ?></option>
                                      <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                  </optgroup>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            </select>
                                            
                        </div>
                        <div class="col-sm-6">
                        <input type="submit" name="submit" value="Submit" class="pup-btn">
                        </div>
                        <div class="col-sm-6">
                        <a href="<?php echo e(route('role-listing')); ?>" class="pup-btn">Cancel
                        </a>
                        </div>
                        
                    </div>
                </form>
           </div>
        </div>
    </div>
</div>
<!--add user popup  Modal End-->
<?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<script type="text/javascript">
    $(document).ready(function () {
        $('#user-form').validate({
            rules: {
                first_name: {
                    required: true,
                },
                last_name: {
                    required: true,
                },
                user_name: {
                    required: true,
                },
            }
        });

        $('#first_name').bind('keyup keydown blur', function(e) {        
       if (e.which === 32 && !this.value.length)
            e.preventDefault();
       $(this).val(function(i, val) {        
           return val.replace(/[^a-zA-Z\s]/gi,''); 
       });       
    });
    })
</script>
<script src="<?php echo e(URL::asset('js/selectbox/components-multi-select.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(URL::asset('js/selectbox/bootstrap-select.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(URL::asset('js/selectbox/jquery.multi-select.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(URL::asset('js/selectbox/select2.full.min.js')); ?>" type="text/javascript"></script>
<script type="text/javascript">
   setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 2000);
</script>