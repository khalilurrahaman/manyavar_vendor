<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!--add user popup Modal Start-->
<div class=" quotation-pop" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content none-shadow">
          <div class="modal-header">
            <h4 class="modal-title">Edit User</h4>
          </div>
            <div class="modal-body">
                <form action="<?php echo e(route ('update-user-admin')); ?>" method="post">
                <?php echo e(csrf_field()); ?>

                <?php if(isset($errors)): ?>
                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                              <div style="padding-left: 145px;" class="alert alert-success alert-dismissable"><?php echo e($error); ?></div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    <?php endif; ?>
                    <div class="row shorting-area pricechange">
                        <div class="col-sm-6">
                            <h5>Name :</h5>
                            <input type="text" name="name" value="<?php echo e($users->name); ?>" class="form-control">
                            <input type="hidden" name="id" value="<?php echo e($users->id); ?>">
                        </div>
                        <div class="col-sm-6">
                            <h5>Email ID :</h5>
                            <input type="text" name="new_email" value="<?php echo e($users->email); ?>" class="form-control">
                            <input type="hidden" name="email" value="<?php echo e($users->email); ?>">
                        </div>
                        <div class="col-sm-6">
                            <h5>User Name :</h5>
                            <input type="text" name="new_username" value="<?php echo e($users->username); ?>" class="form-control" onblur="this.value=removeSpaces(this.value);" readonly="readonly">
                            <input type="hidden" name="username" value="<?php echo e($users->username); ?>">
                        </div>
                        <div class="col-sm-6">
                            <h5>Phone No :</h5>
                            <input type="text" name="mobile" id="mobile" value="<?php echo e($users->mobile); ?>" class="form-control" maxlength="10" onkeypress="return isNumberKey(event)">
                            <span class="error class_err" id="mobile_message" ></span>
                        </div>
                        <div class="clearfix"></div>                        
                        
                        
                        
                        
                        <div class="clearfix"></div>
                        <div class="col-sm-6">
                            <h5>Role :</h5>
                            <div class="shorting-area">
                                <div class="select-full">

                                    <select class="select-drop" name="role_id">
                                        <?php $__currentLoopData = $roleList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $eachRole): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <option <?php if($users->role_id == $eachRole->id) echo 'selected="selected"' ?> value="<?php echo e($eachRole->id); ?>"><?php echo e($eachRole->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                        <h5>Address :</h5>
                        <textarea name="address" class="form-control" contenteditable='true'><?php echo e($users->address); ?></textarea>
                        </div>
                        <div class="col-sm-6">
                        <h5>Division :</h5>
                        <?php $__currentLoopData = $devisionList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $devisionList): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <?php 
                        $user_division=explode(',',$users->devision_id);
                        ?>
                          <div class="checkbox " style="padding-left: 20px;">
                            <input type="checkbox" name="division[]" <?php if(in_array($devisionList->id,$user_division)) echo "checked"; ?> value="<?php echo e($devisionList->id); ?>" ><?php echo e($devisionList->name); ?></input>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                          
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-6">
                        <input type="submit" name="submit" value="Update" class="pup-btn" onclick="return validation()">
                        </div>
                        <div class="col-sm-6">
                        <a href="<?php echo e(URL::previous()); ?>" class="pup-btn">Cancel
                        </a>
                        <div class="clearfix"></div>
                    </div>


                    </div>
                </form>

           </div>
         <div class="modal-footer"></div>

        </div>
    </div>
</div>
<!--add user popup  Modal End-->
<?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script language="javascript" type="text/javascript">
function removeSpaces(string) {
	if (string.match(/\s/g)){
		alert('User name should take only one word,not spaces!');
	}
 return string.split(' ').join('');
}
</script>
<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
 
  function validation()
   {
       var data=$('#mobile').val();
       if(data.length!=10)
       {
           $("#mobile_message").text('Mobile no should accept 10 digits').css("color", "black").show();
           return false;
       }
       else
       {
            $('#mobile_message').hide();
            return true;
       }

    
   
   }   
</script>