<div class="clearfix"></div>
<footer>
<p>© 2016 Manyavar Vendor Panel. All rights reserved, Powered by <a href="http://bluehorse.in/" target="_blank";>Bluehorse</a></p>
</footer>
<!--add remarks Modal Start-->
<div id="addremarks" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Remarks</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
        	<div class="col-sm-3"><h5>Cat 1 : <strong>F-505</strong></h5></div>
            <div class="col-sm-3"><h5>Cat 2 : <strong>307</strong></h5></div>
            <div class="col-sm-3"><h5>Cat 4 : <strong>45"</strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong>234</strong></h5></div>
            <div class="col-sm-3"><h5>Date : <strong>12/10/2016</strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong>3000</strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong>2000</strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong>0</strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong>1000</strong></h5></div>
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
            <div class="col-sm-12">
            <h5>Add Remarks :</h5>
            <textarea class="form-control">Add Remarks </textarea>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="pup-btn">Request Submit</a>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--add remarks  Modal End-->

<!--add cutting Modal Start-->
<div id="changecutting" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Cutting</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
        	<div class="col-sm-3"><h5>Cat 1 : <strong>F-635</strong></h5></div>
            <div class="col-sm-3"><h5>Cat 2 : <strong>307</strong></h5></div>
            <div class="col-sm-3"><h5>Cat 4 : <strong>45"</strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong>234</strong></h5></div>
            <div class="col-sm-3"><h5>Date : <strong>12/10/2016</strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong>3000</strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong>2000</strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong>0</strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong>1000</strong></h5></div>
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
            <div class="col-sm-12">
            <h5>Remarks :</h5>
            <textarea class="form-control">Remarks </textarea>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="pup-btn">Request Submit</a>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--add cutting  Modal End-->

<!--changewidth Modal Start-->
<div id="changewidth" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Width Changes</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
        	<div class="col-sm-3"><h5>Cat 1 : <strong>F-953209</strong></h5></div>
            <div class="col-sm-3"><h5>Cat 2 : <strong>301</strong></h5></div>
            <div class="col-sm-3"><h5>Cat 4 : <strong>44"</strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong>233</strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong>3000</strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong>1000</strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong>300</strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong>700</strong></h5></div>
            <div class="col-sm-3"><h5>Current rate : <strong>37</strong></h5></div>
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
        	<div class="col-sm-6">
            <h5>New requested rate :</h5>
            <input type="number" placeholder="37" class="form-control"/>
            </div>
            <div class="col-sm-6">
            <h5>New Width :</h5>
            <input type="number" placeholder="44" class="form-control"/>
            </div><div class="clearfix"></div>
            <div class="col-sm-12">
            <h5>Reason for change in Width :</h5>
            <textarea class="form-control">Change in loom</textarea>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="pup-btn">Request Submit</a>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--changewidth  Modal End-->

<!--cancel order Modal Start-->
<div id="cancellation" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Cancellation</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
        	<div class="col-sm-3"><h5>Cat 1 : <strong>F-505</strong></h5></div>
            <div class="col-sm-3"><h5>Cat 2 : <strong>307</strong></h5></div>
            <div class="col-sm-3"><h5>Cat 4 : <strong>45"</strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong>234</strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong>3000</strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong>2000</strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong>0</strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong>1000</strong></h5></div>
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
        	<div class="col-sm-6">
            <h5>Cancel Qty :</h5>
            <input type="number" placeholder="1000" class="form-control"/>
            </div>
            <div class="col-sm-6">
            <h5>Reason for Cancellation :</h5>
            <textarea class="form-control">Cannot Dispatch </textarea>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="pup-btn">Request Submit</a>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--cancel order  Modal End-->

<!--Due date extension Modal Start-->
<div id="changedate" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Due Date extension</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
        	<div class="col-sm-3"><h5>Cat 1 : <strong>F-954762</strong></h5></div>
            <div class="col-sm-3"><h5>Cat 2 : <strong>310</strong></h5></div>
            <div class="col-sm-3"><h5>Cat 4 : <strong>58"</strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong>324</strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong>3000</strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong>2000</strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong>0</strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong>0</strong></h5></div>
            <div class="col-sm-3"><h5>Current rate : <strong>1000</strong></h5></div>
            <div class="col-sm-4"><h5>Order Date : <strong>23/09/2016</strong></h5></div>
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
        <div class="col-sm-6">
            <h5>Current Item Due Date :</h5>
            <h4 style="text-align:left"><strong>30/10/2016</strong></h4>
            </div>
        	<div class="col-sm-6">
            <h5>Extended Date :</h5>
            <div class="form-group">
                <div class="input-group date">
                    <input type="text" class="form-control" placeholder="14/11/2016"  id="example44"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                <div class="clearfix"></div>
            </div>
            </div>
            
            <div class="col-sm-12">
            <h5>Reason for Extension :</h5>
            <textarea class="form-control">Slow Production</textarea>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="pup-btn">Request Submit</a>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--Due date extension  Modal End-->

<!--changeprice Modal Start-->
<div id="changeprice" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request for Rate Changes</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
        	<div class="col-sm-3"><h5>Cat 1 : <strong>F-505</strong></h5></div>
            <div class="col-sm-3"><h5>Cat 2 : <strong>301</strong></h5></div>
            <div class="col-sm-3"><h5>Cat 4 : <strong>44"</strong></h5></div>
            
            <div class="col-sm-3"><h5>PO. No : <strong>212</strong></h5></div>
            <div class="col-sm-3"><h5>Order Qty : <strong>2000</strong></h5></div>
            <div class="col-sm-3"><h5>Received qty : <strong>1000</strong></h5></div>
            <div class="col-sm-3"><h5>GIT Qty : <strong>300</strong></h5></div>
            <div class="col-sm-3"><h5>Pending qty : <strong>1000</strong></h5></div>
            <div class="col-sm-3"><h5>Current rate : <strong>300</strong></h5></div>
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
        	<div class="col-sm-6">
            <h5>New requested rate :</h5>
            <input type="number" placeholder="46" class="form-control"/>
            </div>
            <div class="col-sm-6">
            <h5>Reason for change in rate :</h5>
            <textarea class="form-control">Dyeing cost increased</textarea>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="pup-btn">Request Submit</a>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--changeprice  Modal End-->


<!-- Modal Start-->
<div id="expiry-pop" class="modal fade quotation-pop details-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">PO/004567/16-17</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
        	<div class="col-sm-3"><h5>PO No. : <strong>PO/001038/16-17</strong></h5></div>
            <div class="col-sm-3"><h5>PO Date : <strong>10/09/2016</strong></h5></div>
            <div class="col-sm-3"><h5>Document No : <strong>----</strong></h5></div>
            <div class="col-sm-3"><h5>ValidIty Period : <strong>14-JUN-16</strong></h5></div>
            <div class="col-sm-3"><h5>To : <strong>14-JUN-16</strong></h5></div>
             <div class="col-sm-3"><h5>Credit Days : <strong>30</strong></h5></div>
             <!--<div class="col-sm-3"><h5>Form Name : <strong>None</strong></h5></div>-->
            <div class="col-sm-3"><h5>Currency : <strong>Rupees</strong></h5></div>
            <div class="col-sm-3"><h5>Exchange Rate : <strong>1000</strong></h5></div>
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
        	<div class="custome-table">
            <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                            	<th class="text-center">SL No.</th>
                                                <th class="text-center">Cat 1</th>
                                                <th class="text-center">Cat 2</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Cat 4</th>
                                                <th class="text-center">Due Date</th>
                                                <th class="text-center">Qty</th>
                                                <th class="text-center">UOM</th>
                                                <th class="text-center">Rate</th>
                                                <th class="text-center">Amount</th>
                                                <th class="text-center">Basic</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <tr>
                                        <td class="text-center">1.</td>
                                        <td class="text-center">--</td>
                                        <td class="text-center">44"</td>
                                        <td class="text-center">44"</td>
                                        <td class="text-center">126.00</td>
                                        <td class="text-center">29-JUL-16</td>
                                        <td class="text-center">200.000</td>
                                        <td class="text-center">Mtrs</td>
                                        <td class="text-center">125.000</td>
                                        <td class="text-center">25000.00</td>
                                        <td class="text-center">25000.00</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center" style="border:0;">&nbsp;</td>
                                        <td class="text-center" style="border:0;">&nbsp;</td>
                                        <td class="text-center" style="border:0;">&nbsp;</td>
                                        <td class="text-center" style="border:0;">&nbsp;</td>
                                        <td class="text-center" style="border:0;">&nbsp;</td>
                                        <td class="text-center" style="border:0;"><strong>Total :</strong></td>
                                        <td class="text-center" style="border:0;"><strong>200.000</strong></td>
                                        <td class="text-center" style="border:0;">&nbsp;</td>
                                        <td class="text-center" style="border:0;">&nbsp;</td>
                                        <td class="text-center" style="border:0;"><strong>25000.00</strong></td>
                                        <td class="text-center" style="border:0;"><strong>25000.00</strong></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center" style="border:0;">&nbsp;</td>
                                        <td class="text-center" style="border:0;">&nbsp;</td>
                                        <td class="text-center" style="border:0;">&nbsp;</td>
                                        <td class="text-center" style="border:0;">&nbsp;</td>
                                        <td class="text-center" style="border:0;">&nbsp;</td>
                                        <td class="text-center" style="border:0;"><strong>CST Purchase</strong></td>
                                        <td class="text-center" style="border:0;"><strong>&nbsp;</strong></td>
                                        <td class="text-center" style="border:0;">&nbsp;</td>
                                        <td class="text-center" style="border:0;">&nbsp;</td>
                                        <td class="text-center" style="border:0;"><strong>3 % on 25000</strong></td>
                                        <td class="text-center" style="border:0;"><strong>-750.00</strong></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center" style="border:0;">&nbsp;</td>
                                        <td class="text-center" style="border:0;">&nbsp;</td>
                                        <td class="text-center" style="border:0;">&nbsp;</td>
                                        <td class="text-center" style="border:0;">&nbsp;</td>
                                        <td class="text-center" style="border:0;">&nbsp;</td>
                                        <td class="text-center" style="border:0;">&nbsp;</td>
                                        <td class="text-center" style="border:0;">&nbsp;</td>
                                        <td class="text-center" style="border:0;">&nbsp;</td>
                                        <td class="text-center" style="border:0;">&nbsp;</td>
                                        <td class="text-center" style="border:0;"><strong>Net Amount</strong></td>
                                        <td class="text-center" style="border:0;"><strong>24250.00</strong></td>
                                    </tr>
                                   
                                   
                                    
                                </tbody>
                               </table>
                              </div>
            	<div class="col-sm-2 accept-area">
                	<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Request for Date Extension" class="btn btn-success">Date Ext</a>
            	</div>
                <div class="col-sm-2 accept-area">
                	<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Request for Add GIT" class="btn btn-info">Add GIT</a>
            	</div>
                <div class="col-sm-2 accept-area">
                	<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Request for Cancellation" class="btn btn-danger">Cancel</a>
            	</div>
            </div>
            
            
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!-- Modal End-->

    </div>

 <!-- /#wrapper -->
    <script src="<?php echo e(asset('js/bootstrap-datepicker.js')); ?>"></script>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<?php echo e(asset('css/bootstrap-select.min.css')); ?>">
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo e(asset('js/bootstrap-select.min.js')); ?>"></script>

   <!-- <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').DataTable();
			} );
		</script>-->
<script>
$(document).ready(function(){
    $("#hide-filter").click(function(){
        $("#mySidenavR").hide();
    });
    $("#show-btn").click(function(){
        $("#mySidenavR").show();
    });
});
</script>

<script>
var IDLE_TIMEOUT = 1200; //seconds
var _idleSecondsTimer = null;
var _idleSecondsCounter = 0;

document.onclick = function() {
    _idleSecondsCounter = 0;
};

document.onmousemove = function() {
    _idleSecondsCounter = 0;
};

document.onkeypress = function() {
    _idleSecondsCounter = 0;
};

_idleSecondsTimer = window.setInterval(CheckIdleTime, 1000);
function CheckIdleTime() {
     _idleSecondsCounter++;
   /*  var oPanel = document.getElementById("SecondsUntilExpire");
     if (oPanel)
         oPanel.innerHTML = (IDLE_TIMEOUT - _idleSecondsCounter) + "";*/
    if (_idleSecondsCounter >= IDLE_TIMEOUT) {    
		  $.ajax({
			  url: "<?php echo e(route('check-login')); ?>",
			  type: 'get',          
			  data:  '',
			  success: function(hdata) {
				  console.log(hdata);
				  if(hdata=='fail'){
					alert('You are logged out, Please log in again!');
					window.location.href="<?php echo e(url('/')); ?>";
				  }      
			  }
		  });
		  
        window.clearInterval(_idleSecondsTimer);
    }
}
/*setInterval(function () { 
      var urls = "<?php echo e(route('check-login')); ?>";      
      $.ajax({
          url: urls,
          type: 'get',          
          data:  '',
          success: function(hdata) {
              console.log(hdata);
              if(hdata=='fail'){
                alert('You are logged out, Please log in again!');
                window.location.href="<?php echo e(url('/')); ?>";
              }      
          }
      });
         
  },60000);*/
</script>




<script>
$(document).ready(function () {
    $(".btn-select").each(function (e) {
        var value = $(this).find("ul li.selected").html();
        if (value != undefined) {
            $(this).find(".btn-select-input").val(value);
            $(this).find(".btn-select-value").html(value);
        }
    });
});

$(document).on('click', '.btn-select', function (e) {
    e.preventDefault();
    var ul = $(this).find("ul");
    if ($(this).hasClass("active")) {
        if (ul.find("li").is(e.target)) {
            var target = $(e.target);
            target.addClass("selected").siblings().removeClass("selected");
            var value = target.html();
            $(this).find(".btn-select-input").val(value);
            $(this).find(".btn-select-value").html(value);
        }
        ul.hide();
        $(this).removeClass("active");
    }
    else {
        $('.btn-select').not(this).each(function () {
            $(this).removeClass("active").find("ul").hide();
        });
        ul.slideDown(300);
        $(this).addClass("active");
    }
});

$(document).on('click', function (e) {
    var target = $(e.target).closest(".btn-select");
    if (!target.length) {
        $(".btn-select").removeClass("active").find("ul").hide();
    }
});

</script> 


<!--<script>
 $(function(){
    $(".dropdown").hover(            
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
                $(this).toggleClass('open');           
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                $(this).toggleClass('open');           
            });
    });
    
</script>-->
<style>
.dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 0;
	
 }
 .navbar-inverse .navbar-nav .open .dropdown-menu > li > a {color:#fff;}
</style>
<div class="loading">
    <img src="<?php echo e(asset('img/loading.gif')); ?>" alt="">  
  </div>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
<script type="text/javascript">
            // When the document is ready
            $(document).ready(function () {
                
                $('#example1').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                }); 
				
				$('#example2').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
				
				$('.date_picker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });

                $('.datepicker').datepicker({                    
                    format: "dd/mm/yyyy",
                    autoclose: true

                   
                });

                $('.datepickerClass').datepicker({                    
                    format: "dd/mm/yyyy",
                    autoclose: true,
                    startDate: '+1d' 
                   
                });

                $('#vendorChangedate_extended_date').datepicker({                    
                    format: "dd/mm/yyyy",
                    autoclose: true,
                    startDate: '+1d',
                    //endDate: '+1m'
                   
                });


				$('.datepickerEnddateClass').datepicker({                    
                    format: "dd/mm/yyyy",
                    autoclose: true,
                    endDate: '+d' 
                   
                });
            
            
            });
        </script>
 <script>
 $('.selectpicker').selectpicker({
  style: 'btn-primary',
  size: 4
});
 $('.selectpicker2').selectpicker({
  style: 'btn-warning2',
  size: 4
});
$(function() {
	$(".datepickerEnddateClass").val('<?php echo date('d/m/Y')?>');
});
</script>
</body>
</html>