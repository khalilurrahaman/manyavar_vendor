
        <div class="panel-body custome-table">
                                <div class="table-responsive view-table">
                                    <table id="tableGit" class="table table-striped table-bordered" cellspacing="0" width="100%" style="margin-bottom:0;">
                                        <thead>
                                            <tr>
                                            	<th class="text-center">Image</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Width/Size</th>
                                                <th class="text-center">Order Qty</th>
                                                <th class="text-center">Received Qty</th>
                                                <th class="text-center">Pending GIT Qty</th>
                                                <th class="text-center">Pending Qty</th>
                                                <th class="text-center">New GIT</th>
                                            </tr>
                                        </thead>
                                    <tbody class="tbody-table-GitAdd">
                                    <?php if(!empty($purchaseOrderList) && count($purchaseOrderList)>0): ?>
                                    <?php $inc=0;?>
                                    <?php $__currentLoopData = $purchaseOrderList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $purchase): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>                                    
                                    <?php 
                  									$inc++;
                  									//$pndg=($purchase->ORDQTY)-($purchase->RCQTY + $purchase->CNLQTY + $purchase->git_qty);
                                    //$pndg=($purchase->ORDQTY - $purchase->git_rcv);
                                    $pndg = $purchase->ORDQTY - ($purchase->git_rcv+$purchase->p_git_qty);
                  									?>
                                    <?php if($pndg>0): ?>
                                    <tr class="buttonAddGit<?php echo e($inc); ?>-tr">
                                    	<td class="text-center">
                                            <div class="gal"> 
                                             <?php 
                                               $pGalList=DB::table('product_gallery')->select('*')->where('product_id','=',$purchase->product_id)->get();
                                               if($purchase->image!='' && file_exists('upload/product/'.$purchase->image))
                                               $main_image=URL::to('/upload/product').'/'.$purchase->image;
                                               if(empty($main_image) && !empty($pGalList) && count($pGalList)>0)
                                               {
                                                  foreach ($pGalList as $pGal) 
                                                  { 
                                                    if($pGal->product_image!='' && file_exists('upload/product/'.$pGal->product_image))
                                                    {
                                                      $main_image=URL::to('/upload/product').'/'.$pGal->product_image;
                                                    }
                                                  }
                                               }
                                               if(empty($main_image))
                                                $main_image=URL::to('/img').'/pre-order-img.jpg';
                                              ?>
                                               <a class="fancybox" rel="group<?php echo e($inc); ?>" href="<?php echo e($main_image); ?>" >
                                                <img src="<?php echo e($main_image); ?>" width="150" class="buttonAddGit<?php echo e($inc); ?>-img">
                                               </a>
                                               <?php 
                                               if(!empty($pGalList) && count($pGalList)>0)
                                               {
                                                foreach ($pGalList as $pGal2) 
                                                {
                                                    if($pGal2->product_image!='' && file_exists('upload/product/'.$pGal2->product_image))
                                                    {
                                                      $main_image=URL::to('/upload/product').'/'.$pGal2->product_image;

                                                ?>                                               
                                               <a class="fancybox" rel="group<?php echo e($inc); ?>" href="<?php echo e($main_image); ?>" style="display:none;"><img src="<?php echo e($main_image); ?>" alt=""></a>
                                                <?php 
                                                }
                                                }
                                                }
                                               ?> 
                                            </div>
                                        </td>
                                    	<td class="text-center buttonAddGit<?php echo e($inc); ?>-cat-1"><?php echo e($purchase->cat1); ?></td>
                                    	<td class="text-center buttonAddGit<?php echo e($inc); ?>-cat-2"><?php echo e($purchase->cat2); ?></td>
                                        <td class="text-center buttonAddGit<?php echo e($inc); ?>-cat-3"><?php echo e($purchase->cat3); ?></td>
                                    	<td class="text-center buttonAddGit<?php echo e($inc); ?>-cat-4"><?php echo e($purchase->cat4); ?></td>
                                        <td class="text-center buttonAddGit<?php echo e($inc); ?>-order-qty"><?php echo e($purchase->ORDQTY); ?></td>
                                        <td class="text-center buttonAddGit<?php echo e($inc); ?>-received-qty"><?php echo e($purchase->RCQTY); ?></td>
                                    	<td class="text-center buttonAddGit<?php echo e($inc); ?>-pending-git-qty"><?php echo e($purchase->p_git_qty); ?></td>
                                        <td class="text-center buttonAddGit<?php echo e($inc); ?>-pending-qty"><?php echo e($pndg); ?></td>
                                        <td class="text-center last-area  new-git">
                                            <div class="input-group">
                                            	<input type="hidden"  class="buttonAddGit<?php echo e($inc); ?>-id" value="<?php echo e($inc); ?>">
                                                <input type="hidden"  class="buttonAddGit<?php echo e($inc); ?>-ORDCODE" value="<?php echo e($purchase->ORDCODE); ?>">
                                                <input type="hidden"  class="buttonAddGit<?php echo e($inc); ?>-ICODE" value="<?php echo e($purchase->ICODE); ?>">
                                                <input type="hidden"  class="buttonAddGit<?php echo e($inc); ?>-pd_id" value="<?php echo e($purchase->pd_id); ?>">
                                            	<input type="text" class="form-control buttonAddGit<?php echo e($inc); ?>-add-git-qty" placeholder="Add GIT" onkeypress="javascript:return isNumber(event)"> 
                                                <span class="input-group-btn">
                                                <button class="btn btn-info addGitClass" uniqueKey="buttonAddGit<?php echo e($inc); ?>" type="button">ADD</button>
                                                </span>
                                            </div>
                                            <span class="buttonAddGit<?php echo e($inc); ?>-message commonMessage"></span>
                                            
                                        </td>
                                    </tr>
                                    <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                    <?php else: ?>
                                    <tr>
                                    	<td class="text-center" colspan="10">No Records Available!</td>
                                    </tr>
                                    <?php endif; ?> 
                                </tbody>
                               </table>
                              </div>


                            

                            
</div>




<script>
    // WRITE THE VALIDATION SCRIPT IN THE HEAD TAG.
    function isNumber(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
            return false;

        return true;
    }    
 </script>                          