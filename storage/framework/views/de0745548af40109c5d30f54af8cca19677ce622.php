<div class="panel-body custome-table">
                            
                                <div class="table-responsive view-table">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">SL No</th>
                                                <th class="text-center">GIT No</th>
                                                <th class="text-center">Vendor Name</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Date</th>
                                                <th class="text-center">GIT Qty</th>
                                                <th class="text-center">Receive Qty</th>
                                                
                                                <th class="text-center">Adhoc Qty</th>
                                                <th class="text-center">Pending GIT Qty</th>
                                                <!-- <th class="text-center">GRT</th>-->
                                                 <th class="text-center">Mark Complete Remarks</th>
                                                <th class="text-center">Last Comments</th>
                                                <!--<th class="text-center">Remarks</th>-->
                                                <!--  <th class="text-center">Action</th> -->
                                                <!--<th class="text-center width-last">Action</th>-->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(!empty($gitList) && count($gitList)>0): ?>
                                            <?php $old_val='';$inc=0;?>
                                            <?php $__currentLoopData = $gitList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $eachGit): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                            <?php

                                           
                                            $new_val=$eachGit->git_no; 
                                            // $prev_git_qty=$eachGit->prev_git_qty;
                                            // $prev_gitqty=$prev_git_qty[0]->pgit_qty;

                                            $pndg_qty=$eachGit->git_qty - $eachGit->git_rcv;
                                            $pndg_qty_calc=$pndg_qty;
                                            $adhoc_qty=0;
                                            $inadequate_qty=0;
                                            if($pndg_qty<=0)
                                            {
                                                $adhoc_qty=str_replace("-","",$pndg_qty);
                                                $pndg_qty=0;
                                            }
                                            $grc_qnty=0;
                                            if($eachGit->git_qty!=$grc_qnty)
                                            {
                                                $inadequate_qty=$eachGit->git_qty-$grc_qnty;
                                            }
                                            ?>
                                            <tr>
                                                
                                                <?php if($new_val!=$old_val){
                                            	$inc++;
												?>
                                                <td class="text-center" rowspan="<?php echo e($eachGit->rowspan); ?>"><?php echo e($inc); ?></td>
                                                <td class="text-center" rowspan="<?php echo e($eachGit->rowspan); ?>"><?php echo e($eachGit->git_no); ?></td>
                                                
                                                <td class="text-center" rowspan="<?php echo e($eachGit->rowspan); ?>"><?php echo e($eachGit->name); ?></td>
                                                <?php }?>
                                                <td class="text-center"><?php echo e($eachGit->cat1); ?></td>
                                                <td class="text-center"><?php echo e($eachGit->cat2); ?></td>
                                                <td class="text-center"><?php echo e($eachGit->cat3); ?></td>
                                                <td class="text-center"><?php echo e(date('d/m/Y',strtotime($eachGit->git_date))); ?></td>
                                               
                                                <td class="text-center"><?php echo e($eachGit->git_qty); ?></td>
                                                <td class="text-center"><?php echo e($eachGit->git_rcv); ?></td>
                                                
                                                <td class="text-center"><?php echo e($adhoc_qty); ?></td>
                                                <td class="text-center"><?php echo e($pndg_qty); ?></td>
                                                <?php if($new_val!=$old_val){
												?>
                                                <td class="text-center" rowspan="<?php echo e($eachGit->rowspan); ?>"><?php echo e($eachGit->mark_complete_remarks); ?></td>
                                                <?php }?>
                                                <!--<td class="text-center">2</td>-->
                                                <td class="text-center">
                                        <?php if(!empty($eachGit->git_comments) && count(($eachGit->git_comments))>0): ?>
                                        <?php                                       
                                       
                                        if(strlen($eachGit->git_comments[0]->comments)<=20)                                 
                                           echo ($eachGit->git_comments[0]->comments);
                                        else
                                         echo substr($eachGit->git_comments[0]->comments,0,18).'..';  

                                        ?>
                                        <a href="javascript:void(0);" class="pull-right btn btn-warning btn-xs">
                                        <?php echo count($eachGit->git_comments)?>
                                        Comment(s)</a>
                                        <?php else: ?>
                                        <a href="javascript:void(0);" class="btn btn-warning btn-xs pull-right">
                                        
                                        Comments</a>
                                        
                                         <?php endif; ?>
                                        </td>
                                         
                                            </tr>
                                             <?php $old_val=$new_val;?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row">
                                  <div class="col-sm-6">
                                    <div class="dataTables_info" role="status" aria-live="polite">Showing <?php echo e($gitList->count()); ?> of <?php echo e($gitList->total()); ?> entries</div>
                                  </div>
                                  <div class="col-sm-6">
                                        <nav aria-label="Page navigation" class="pull-right">
                                            <?php echo e($gitList->links()); ?>

                                        </nav>
                                    </div>
                                </div>
                            </div>
                            
<script>
$('.ResolveGitClass').click(function(){
 //Fetch All Records
 var request_type=$(this).text();
 var git_no=$(this).attr('git_no');
 //Set All Records in Modal
 $('#request_type').val(request_type);
 $('#git_no').text(git_no); 
 
});
$('.ViewRemarksClass').click(function(){
 //Fetch All Records
 var admin_remarks=$(this).attr('admin_remarks');
 var vendor_remarks=$(this).attr('vendor_remarks');
 var git_no=$(this).attr('git_no');
 //Set All Records in Modal
 $('#git_no_r').text(git_no); 
 if(admin_remarks!='Admin:')
 	$('#admin_remarks_r').text(admin_remarks); 
 if(vendor_remarks!='Vendor:')
 	$('#vendor_remarks_r').text(vendor_remarks); 
});

$('.ResolvedGitClass').click(function(){
 //Fetch All Records
 var git_no=$(this).attr('git_no');
 var type=$(this).text();
 var cn_doc_attc=$(this).attr('cn_doc_attc');
 //Set All Records in Modal
 $('#git_no99').text(git_no); 
 //Appent Table
	$.ajax({
			type: 'GET', 
			url: '<?php echo e(route("fetch-git-records")); ?>',
			data: {git_no:git_no,type:type},
			success:function(data)
			{
				console.log(data);
				if(data.cnList.length > 0)
				{  
					
					var table_html = '<table id="example2" class="table table-striped table-bordered" cellspacing="0" width="100%">';
					table_html += '<thead><tr><th class="text-center">Item</th><th class="text-center">Qty</th><th class="text-center">Rate</th><th class="text-center">Gross Amt</th><th class="text-center">Charge Type</th><th class="text-center">Charge Apply</th><th class="text-center">Net Amount</th></tr></thead>';
						table_html += '<tbody>';
						 
							for(var i=0;i<data.cnList.length;i++)
							{
								var item_name=data.cnList[i]['ITEM_NAME'];
								var cn_qty=data.cnList[i]['cn_qty'];
								var rate=data.cnList[i]['RATE'];
								var gross_amnt=cn_qty*rate;
								var disc_rate=0;
				
								var tax_type='NA';
								var tax=data.cnList[i]['disc_rate'];
								var tax_type=data.cnList[i]['CHGNAME'];
								if(tax_type=='Discount')
									var net_amnt=gross_amnt-(tax*gross_amnt/100);
								else
									var net_amnt=gross_amnt+(tax*gross_amnt/100);
								table_html += '<tr>';
									table_html += '<td class="text-center">'+item_name+'</td>';
									table_html += '<td class="text-center">'+cn_qty+'</td>';
									table_html += '<td class="text-center">'+rate+'</td>';
									table_html += '<td class="text-center">'+gross_amnt+'</td>';
									table_html += '<td class="text-center">'+tax_type+'</td>';
									table_html += '<td class="text-center">'+tax+'%</td>';
									table_html += '<td class="text-center">'+net_amnt+'</td>';
								table_html += '</tr>';
							}
						table_html += '</tbody>';
				  table_html += '</table>';
				  if(cn_doc_attc!='')
				  {
				  $('.previewing99').html('<a target="_blank" href="'+cn_doc_attc+'">Attachment</a>');
				  }
			  	  $('.itemDetailsTable99').html(table_html);
				}
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});
});

</script>

<!-- <script>
        $(document).ready(function () {
            
            $('#example').dataTable({
                'order': [[ 0, "asc" ]],//Default Column Attribute
                'pageLength': 500,//Per Page Item
                //'sDom': 't',//Hide All Info From top and bottom
                'aTargets': [-1], //1st one, start by the right/
                "paging":   false,
                "info":     false,
                "searching": false,
                'dom': 'Bfrtip',
                'buttons': ['excel']//['copy', 'csv', 'excel', 'pdf', 'print']
            });
        });
        </script> -->