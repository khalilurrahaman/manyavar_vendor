<?php echo $__env->make('layouts.vendor_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>;
<!-- CSS -->
<link rel="stylesheet" href="<?php echo e(asset('fancybox/css/fancybox/jquery.fancybox-buttons.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('fancybox/css/fancybox/jquery.fancybox-thumbs.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('fancybox/css/fancybox/jquery.fancybox.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('fancybox/demo/demo.css')); ?>">
<!-- DELETE -->
<style>
.avl-img{position: absolute;
top: 42px;
right: -9px;
width: 20px;}
.git-qty-input{display:none; width:50px;}
.input-group-btn > .btn {margin:0;}
@media  screen and (min-width:768px){.paddingR0 {padding-right:0;}}
.new-git .form-control {padding:6px 2px; text-align:center; font-size:11px;}
.input-group-btn:last-child>.btn, .input-group-btn:last-child>.btn-group {padding:8px 4px;}

</style>
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
               
                <div class="row">
                    <div class="col-sm-6 paddingR0">
                        <div class="panel panel-default add-padding">
                        	<div class="panel-heading">
                                <div class="row">
                                	<div class="col-sm-6">
                                    	<h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i> Vendor Create GIT</h3>
                                    </div>
                                    <div class="col-sm-6">
                                    
                                    <div class="shorting-area">
                                <div class="pull-right">
                                        <a href="javascript:void(0)" id="show-btn" class="btn btn-success">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                </div>
                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                            
                            <div class="row shorting-area">
                          
                            <div class="col-sm-12">
                            <div id="mySidenavR" class="sidenavR" style="display:none;">
<div class="filter-right">
<div class="filter-area list-filter">
<!--<h5>Vendor List Filter</h5>-->
    <a href="javascript:void(0)" class="closebtn" id="hide-filter">×</a>
        <div class="col-sm-2">
             <select class="select-drop" id="category1" name="category1">
            	<option value="">Design</option>
                
            </select>
        </div>
        <div class="col-sm-2">
            <select class="select-drop" id="category2" name="category2">
            	<option value="">Colour</option>
               
            </select>
        </div>
        <div class="col-sm-2">
            <select class="select-drop" id="category3" name="category3">
            	<option value="">Category 3</option>
                
            </select>
        </div>
        <div class="col-sm-2">
            <select class="select-drop" id="category4" name="category4">
            	<option value="">Width/Size</option>
                
            </select>
        </div>
      
        <div class="col-sm-2">
        	<a href="javascript:void(0)" class="btn btn-success" onclick="applyFilter();">Apply Filter</a>
        </div>
        <div class="col-sm-2"> 
                          <a href="javascript:void(0)" class="btn btn-success dateCheck reset" onclick="">Clear</a>
                          <div class="clearfix"></div>  
                         </div>
<div class="clearfix"></div>        
	</div>

    <div class="clearfix"></div>
    </div>    
</div>
                            
                            </div>
                            
                            
                            </div>
                             <span id="filterResult">
                                <div class="table-responsive view-table" style="font-size:11px;">
                                    <table id="tableGit" class="table table-striped table-bordered" cellspacing="0" width="100%" style="margin-bottom:0;">
                                        <thead>
                                            <tr>
                                            	<th class="text-center col-sm-2">Image</th>
                                                <th class="text-center col-sm-2">Design</th>
                                                <th class="text-center col-sm-2" style="width:66px;">Colour</th>
                                                <th class="text-center col-sm-2">Cat 3</th>
                                                <th class="text-center col-sm-2">Width/<br/>Size</th>
                                                <th class="text-center col-sm-2">Order Qty</th>
                                                <th class="text-center col-sm-2">Received Qty</th>
                                                <th class="text-center col-sm-2">Pending GIT Qty</th>
                                                <th class="text-center col-sm-2">Pending Qty</th>
                                                <th class="text-center col-sm-2" style="width:166px;">New GIT</th>
                                            </tr>
                                        </thead>

                                        
                                        <tbody class="tbody-table-GitAdd">
                                       
                                        </tbody>


                               </table>
                              </div>
                              </span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-6">
                    <form id="addGitForm" method="post" action="<?php echo e(route('submit-git-records')); ?>">
                     <?php echo e(csrf_field()); ?>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i> Vendor Create GIT Submit</h3>
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                            	<div class="table-responsive view-table git-table">
                                    <table id="tableGitCart" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                      <thead>
                                            <tr>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Width/Size</th>
                                                <th class="text-center">New GIT</th>
                                                 <th class="text-center">Pending Qty</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead> 
                                    <tbody class="tbody-table-GitCart">
                                    <tr class="empty-table-GitCart">
                                    	<td colspan="7" >Add Git for further process!</td>
                                    </tr>
                                </tbody>
                               </table>
                              </div>
                              <span id="git_cart_message"></span>
                              <br/>
                                <div class="row shorting-area">
                                	<div class="col-sm-3">
            							<h5>Challan No :</h5>
            							<input type="text" id="challan_no" name="challan_no" class="form-control" onblur="chalanNoAvailability(this.value)" onkeyup="chalanNoAvailability(this.value)" value="<?php echo '';?>" />
                                        <input type="hidden" id="availability" value="0" />
                                        <span class="availability"></span>
                                        <span id="challan_no_message"></span>
            						</div>
                                     <div class="col-sm-3">
            							<h5>Date :</h5>
            							<input type="text" id="date" name="date" class="form-control datepickerEnddateClass"/>
                                        <span id="date_message"></span>
            						</div>
                                    <div class="col-sm-3">
            							<h5>Courier Name :</h5>
            							<input type="text"  id="lorry_no" name="lorry_no" class="form-control"/>
                                        <span id="lorry_no_message"></span>
            						</div>
                                    <div class="col-sm-3">
            							<h5>Docket No :</h5>
            							<input type="text" id="transport_no" name="transport_no" class="form-control"/>
                                        <span id="transport_no_message"></span>
            						</div>
                                </div>
                            
                            
                            
                              <div class="col-sm-12 quotation-pop">
            						<a href="javascript:void(0)" class="pup-btn submitGitClass" style="margin-top:0;">Submit GIT</a>
            				</div>
                              
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
<?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>;   
<!-- ********************************************** -->
<!-- JavaScript at the bottom for fast page loading -->
<!-- ********************************************** -->

    <!-- Grab Google CDN's jQuery, fall back to local if offline -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo e(asset('fancybox/js/libs/jquery-1.7.1.min.js')); ?>"><\/script>')</script>
        
    <!-- FancyBox -->
        <script src="<?php echo e(asset('fancybox/js/fancybox/jquery.fancybox.js')); ?>"></script>
        <script src="<?php echo e(asset('fancybox/js/fancybox/jquery.fancybox-buttons.js')); ?>"></script>
        <script src="<?php echo e(asset('fancybox/js/fancybox/jquery.fancybox-thumbs.js')); ?>"></script>
        <script src="<?php echo e(asset('fancybox/js/fancybox/jquery.easing-1.3.pack.js')); ?>"></script>
        <script src="<?php echo e(asset('fancybox/js/fancybox/jquery.mousewheel-3.0.6.pack.js')); ?>"></script>
        
        <!-- <script type="text/javascript">
            $(document).ready(function() {
               
                $(".fancybox").fancybox();
            });
        </script> -->
<script src="<?php echo e(asset('js/custom/git.js')); ?>"></script>    
<script>
$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        var url = $(this).attr('href'); 
        var page=$(this).attr('href').split('page=')[1];


        
        var category1=$("#category1").val();
        var category2=$("#category2").val();
        var category3=$("#category3").val();
        var category4=$("#category4").val();
        filter(page,category1,category2,category3,category4);
    });    
});



$(document).ready(function () {  

    $('#challan_no').val('');
    $('.loading').show();
    getDesignList()

    applyFilter();
});


function chalanNoAvailability(challan_no)
{
	if(challan_no!='')
    {
        	$('.availability').html('<img src="<?php echo e(asset("img/loading-gif-large.gif")); ?>" alt="Loading" class="avl-img" width="80%">');
        	$("#challan_no_message").text('');
        	 setTimeout(function(){
            //$('#hide-me').fadeout();
            $.ajax({
        	type: 'GET',
        	url: '<?php echo e(route("chalan-no-availability")); ?>',
        	data: {challan_no:challan_no},
        	success:function(data)
        	{
        		$('#availability').val(data);
        		if(data==1)
        			$('.availability').html('<img src="<?php echo e(asset("img/available.png")); ?>" alt="Availabil" class="avl-img" width="20%">');
        		else
        			$('.availability').html('<img src="<?php echo e(asset("img/not-available.png")); ?>" alt="Not Availabil" class="avl-img" width="20%">');
        	}
           });
        }, 2000);
    }else
    {
       $('.availability').html(''); 
    }
	
}
function getDesignList()
    {
        $.ajax({
            type: 'GET', 
            url: '<?php echo e(route("vendor-design-list")); ?>',
            success:function(data)
            {
                $('#category1').html(data['design']);
                getColorList();
                
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
    }

    function getColorList()
    {
        $.ajax({
            type: 'GET', 
            url: '<?php echo e(route("vendor-color-list")); ?>',
            success:function(data)
            {
                $('#category2').html(data['color']);
                 getCategory3List();
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
    }
 function getCategory3List()//For fetch Category3 list
    {
        $.ajax({
            type: 'GET', 
            url: '<?php echo e(route("vendor-category3-list")); ?>',
            success:function(data)
            {
                $('#category3').html(data['category3']);
                 getWidthSizeList();

                 $('.loading').hide();

            },
            error: function (error) 
            {
                
               // alert('Error Occured Please Try Again');
            }
        });
    }
    
    function getWidthSizeList()//For fetch WidthSize list
    {
        $.ajax({
            type: 'GET', 
            url: '<?php echo e(route("vendor-width-size-list")); ?>',
            success:function(data)
            {
                $('#category4').html(data['width_size']);
            },
            error: function (error) 
            {
                
                //alert('Error Occured Please Try Again');
            }
        });
    }



$('#date').on('changeDate', function(ev){
    $(this).datepicker('hide');
});

function applyFilter(){
		//var pending_qty=$("#pending_qty").val();
		var category1=$("#category1").val();
		var category2=$("#category2").val();
		var category3=$("#category3").val();
		var category4=$("#category4").val();
        filter(1,category1,category2,category3,category4);
}

$( "#hide-filter" ).click(function() {
	
	//$("#pending_qty").val('');
	$("#category1").val('');
	$("#category2").val('');
	$("#category3").val('');
	$("#category4").val('');
	
	filter(); 
});

function filter(page=1,category1='',category2='',category3='',category4=''){	
	$('.loading').show();
	$.ajax({
		type: 'GET', 
		url: '<?php echo e(route("filter-add-git")); ?>',
		data: {category1:category1,category2:category2,category3:category3,category4:category4,page:page},
		success:function(data)
		{
			$('#filterResult').html(data);
			$('.loading').hide();
		},
		error: function (error) 
		{
			
			//alert('Error Occured Please Try Again');
		}
	})
}
</script>   
<script>
    // WRITE THE VALIDATION SCRIPT IN THE HEAD TAG.
    function isNumber(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
            return false;

        return true;
    }    

    //--For Clearing Filter Data Elements-----//
    $(".reset").bind("click", function() {
    $("#category1").val("");
    $("#category2").val("");
    $("#category3").val("");
    $("#category4").val("");


    });   

    // $(".resetval").bind("click", function() {
    // $("input[type=text], textarea").val("");
    // });   
</script>   
 <script type="text/javascript">
        var nc=$.noConflict();
        nc(document).ready(function() {
        nc(".fancybox").fancybox();
        });
    </script>
