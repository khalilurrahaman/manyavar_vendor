<div class="panel-body custome-table">
                                <div class="table-responsive view-table">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Sl. No</th>
                                                <th class="text-center">GIT No</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Width/Size</th>
                                                <th class="text-center">Date</th>
                                                <th class="text-center">GIT Qty</th>
                                                <th class="text-center">Receive Qty</th>
                                                <th class="text-center">Adhoc Qty</th>
                                                <th class="text-center">Pending GIT Qty</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-center">Last Comments</th>
                                                 <th class="text-center">Mark Complete Remarks</th>
                                                <th class="text-center">Challan No</th>
                                                
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <?php if(count($gitList)>0): ?>
                                    <?php $old_val='';$inc=0;?>
                                    <?php $__currentLoopData = $gitList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $git): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <?php 
                                    $new_val=$git->git_no;
                                    $class='warning';
                                    $status=$git->status;
                                    if($git->status=='Rejected')
                                        $class='danger';
                                    if($git->status=='Close')
                                    {
                                        $class='success';
                                        $status='Closed';
                                    }
                                        
                                    
                                    //$prev_git_qty=$git->prev_git_qty;
                                    //$prev_gitqty=$prev_git_qty[0]->pgit_qty;
                                    
                                    $pndg_qty=$git->git_qty-$git->git_rcv;
                                    $adhoc_qty=0;
                                    $inadequate_qty=0;
                                    if($pndg_qty<=0)
                                    {
                                    //echo $pndg_qty=str_replace("-","Adhoc:",$pndg_qty);
                                    $adhoc_qty=str_replace("-"," ",$pndg_qty);
                                    $pndg_qty=0;
                                    
                                    }
                                    $grc_qnty=0;
                                    if($git->git_qty!=$grc_qnty)
                                    {
                                    $inadequate_qty=$git->git_qty-$grc_qnty;
                                    }       
                                    ?>
                                    <tr>
                                     <?php if($new_val!=$old_val){
                                            	$inc++;
												?>
                                        <td class="text-center" rowspan="<?php echo e($git->rowspan); ?>"><?php echo e($inc); ?></td>
                                        <td class="text-center" rowspan="<?php echo e($git->rowspan); ?>"><?php echo e($git->git_no); ?></td>
                                         <?php }?>
                                        <td class="text-center"><?php echo e($git->cat1); ?></td>
                                        <td class="text-center"><?php echo e($git->cat2); ?></td>
                                        <td class="text-center"><?php echo e($git->cat3); ?></td>
                                        <td class="text-center"><?php echo e($git->cat4); ?></td>
                                        <td class="text-center"><?php echo e(date('d/m/Y',strtotime($git->git_date))); ?></td>
                                        <td class="text-center"><?php echo e($git->git_qty); ?></td>
                                        <td class="text-center"><?php echo e($git->git_rcv); ?></td>
                                        
                                         <td class="text-center"><?php echo e($adhoc_qty); ?></td>
                                        <td class="text-center"><?php echo e($pndg_qty); ?></td>
                                        <td class="text-center"><span class="btn btn-<?php echo e($class); ?> btn-xs"><?php echo e($status); ?></span></td>
                                        <td class="text-center">
                                       <?php if(!empty($git->git_comments) && count(($git->git_comments))>0): ?>
                                        <?php                                       
                                       
                                        if(strlen($git->git_comments[0]->comments)<=20)                                 
                                           echo ($git->git_comments[0]->comments);
                                        else
                                         echo substr($git->git_comments[0]->comments,0,18).'..';  

                                        ?>
                                        <?php if($git->status!='Close'): ?>
                                            <a href="<?php echo e(route('gitwise-comments', $git->id)); ?>" class="pull-right btn btn-warning btn-xs">
                                            <?php echo count($git->git_comments)?>
                                            Comment(s)</a>
                                            <?php else: ?>
                                             <a class="pull-right btn btn-warning btn-xs">
                                            <?php echo count($git->git_comments)?>
                                            Comment(s)</a>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <?php if($git->status!='Close'): ?>
                                            <a href="<?php echo e(route('gitwise-comments', $git->id)); ?>" class="btn btn-warning btn-xs pull-right">
                                            Comments</a>
                                            <?php else: ?>
                                            <a class="btn btn-warning btn-xs pull-right">
                                            Comments</a>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                        </td>
                                        
                                        <?php if($new_val!=$old_val){?>
                                        <td class="text-center" rowspan="<?php echo e($git->rowspan); ?>"><?php echo e($git->mark_complete_remarks); ?></td>
                                        <td class="text-center" rowspan="<?php echo e($git->rowspan); ?>"><?php echo e($git->challan_no); ?></td>
                                        <td class="text-center" rowspan="<?php echo e($git->rowspan); ?>">
                                           <?php
											$admin_remarks= 'Admin:'.$git->admin_cn_resolve_remarks;											
											$vendor_remarks= 'Vendor:'.$git->vendor_cn_resolve_remarks;
											if($admin_remarks !='Admin:' || $vendor_remarks!='Vendor:'){
											?>
                                           <span data-toggle="modal" data-target="#ViewRemarks">
                                        	<a href="javascript:void(0);" class="btn btn-success btn-xs ViewRemarksClass"  admin_remarks="<?php echo e($admin_remarks); ?>" vendor_remarks="<?php echo e($vendor_remarks); ?>" git_no="<?php echo e($git->git_no); ?>">View CN Remarks</a>
                                           </span>
                                            <?php
											}?>
                                            
                                            <?php
											$sb_admin_remarks= 'Admin:'.$git->admin_sb_resolve_remarks;											
											$sb_vendor_remarks= 'Vendor:'.$git->vendor_sb_resolve_remarks;
											if($sb_admin_remarks !='Admin:' || $sb_vendor_remarks!='Vendor:'){
											?>
                                           <span data-toggle="modal" data-target="#ViewRemarks">
                                        	<a href="javascript:void(0);" class="btn btn-success btn-xs ViewRemarksClass"  admin_remarks="<?php echo e($sb_admin_remarks); ?>" vendor_remarks="<?php echo e($sb_vendor_remarks); ?>" git_no="<?php echo e($git->git_no); ?>">View SB Remarks</a>
                                           </span>
                                           <br />
                                            <?php
											}?>
                                         
                                       
                                        <?php 
										if($git->status!='Close')
										{
										//-----------CN Section-------
									 	if($git->is_cn>0)
										{
										$target='#ResolveGit';
										$class='warning';
										$class2='ResolveGitClass';
										$text_type='Attach CN';
										if($git->vendor_is_resolve==1 && $git->cn_doc!='' )
										{
											#$target='javascript:void(0);';
											$target='#ResolvedGit';
											$class='success';
											$class2='ResolvedGitClass';
											$text_type='Attached CN';
										}
										$cn_doc_attc='';
										if($git->cn_doc!='')
										{
										   if(file_exists('upload/gitdoc/'.$git->cn_doc))
										   { 
											$cn_doc_attc=URL::to('/upload/gitdoc').'/'.$git->cn_doc;
										   }
										}
											
										?>   
                                        <span data-toggle="modal" data-target="<?php echo e($target); ?>">
                                            <a href="javascript:void(0);" class="btn btn-<?php echo e($class); ?> btn-xs <?php echo e($class2); ?>"  git_id="<?php echo e($git->per_git_id); ?>" git_no="<?php echo e($git->git_no); ?>" pd_id="<?php echo e($git->pd_id); ?>" id="<?php echo e($git->id); ?>" cn_doc_attc="<?php echo e($cn_doc_attc); ?>"><?php echo e($text_type); ?></a>
                                        </span>
                                       <?php
									   }
									   if($git->admin_is_resolve==1)
									   {
										//-----------SB Section-------
										$target2='#ResolveGit';
										$class2='warning';
										$text_type2='Attach SB';
										if($git->vendor_is_resolve==1 && $git->sb_doc!='')
										{
											$target2='javascript:void(0);';
											$class2='success';
											$text_type2='Attached SB';
										}
										?>   
                                        <span data-toggle="modal" data-target="<?php echo e($target2); ?>">
                                            <a href="javascript:void(0);" class="btn btn-<?php echo e($class2); ?> btn-xs ResolveGitClass"  git_id="<?php echo e($git->per_git_id); ?>" git_no="<?php echo e($git->git_no); ?>" pd_id="<?php echo e($git->pd_id); ?>" id="<?php echo e($git->id); ?>"><?php echo e($text_type2); ?></a>
                                        </span>
                                        <?php }?> 
                                        <!-- <span data-toggle="modal" data-target="#updateGit">
                                            <a href="javascript:void(0);" class="btn btn-success btn-xs updateGitClass" ch_no="<?php echo e($git->challan_no); ?>" lorry_no="<?php echo e($git->lorry_no); ?>" transport_no="<?php echo e($git->transport_no); ?>">Update</a>
                                        </span> -->
                                        <?php }?> 
                                        
                                        <a href="<?php echo e(route('view-git-invoice',$git->per_git_id)); ?>" class="btn btn-danger btn-xs" target="_blank">
                                        View packing slip
                                        </a>
                                        
                                        <a href="<?php echo e(route('download-git-invoice',$git->per_git_id)); ?>" class="btn btn-warning btn-xs">
                                        Download packing slip
                                        </a>
                                        </td>
                                         <?php
                                            }?>
                                    </tr>
                                    <?php $old_val=$new_val;?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                   <?php else: ?> 
                                    <tr>
                                        <td class="text-center" colspan="16">No Records Available!</td>
                                    </tr>
                                    <?php endif; ?>                                   
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                              <div class="col-sm-6">
                                <div class="dataTables_info" role="status" aria-live="polite">Showing <?php echo e($gitList->count()); ?> of <?php echo e($gitList->total()); ?> entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                       <?php echo e($gitList->links()); ?>

                                    </nav>
                                  </div>
                              </div>
                            </div>
                        
                    </div>
                </div>
            </div>
            
           
<script type="text/javascript">
$('.updateGitClass').click(function(){
 //Fetch All Records
 var ch_no=$(this).attr('ch_no');
 var lorry_no=$(this).attr('lorry_no');
 var transport_no=$(this).attr('transport_no');

 //Set All Records in Modal	
 $('#ch_no').text(ch_no);
 $('#lorry_no').val(lorry_no); 
 $('#transport_no').val(transport_no);

});

$('.ViewRemarksClass').click(function(){
 //Fetch All Records
 var admin_remarks=$(this).attr('admin_remarks');
 var vendor_remarks=$(this).attr('vendor_remarks');
 var git_no=$(this).attr('git_no');
 //Set All Records in Modal
 $('#git_no_r').text(git_no); 
 if(admin_remarks!='Admin:')
 	$('#admin_remarks_r').text(admin_remarks); 
 if(vendor_remarks!='Vendor:')
 	$('#vendor_remarks_r').text(vendor_remarks); 
});

$('.ViewRemarksCloseClass').click(function(){
 	$('#git_no_r').text(''); 
 	$('#admin_remarks_r').text(''); 
 	$('#vendor_remarks_r').text(''); 
});



$('.ResolveGitClass').click(function(){
 //Fetch All Records
 var per_git_id=$(this).attr('git_id');
 var git_no=$(this).attr('git_no');
 var pd_id=$(this).attr('pd_id');
 var id=$(this).attr('id');
 var type=$(this).text();
 //Set All Records in Modal	
 $('#per_git_id').val(per_git_id);
 $('#git_no').text(git_no); 
 $('#pd_id').val(pd_id);
 $('#id').val(id);
 $('#type').val(type);
 //Appent Table
 if(type=='Attach CN')
 {
	$.ajax({
			type: 'GET', 
			url: '<?php echo e(route("fetch-git-records")); ?>',
			data: {git_no:git_no,type:type},
			success:function(data)
			{
				console.log(data);
				if(data.cnList.length > 0)
				{  
					
					var table_html = '<table id="example2" class="table table-striped table-bordered" cellspacing="0" width="100%">';
					table_html += '<thead><tr><th class="text-center">Item</th><th class="text-center">Qty</th><th class="text-center">Rate</th><th class="text-center">Gross Amt</th><th class="text-center">Charge Type</th><th class="text-center">Charge Apply</th><th class="text-center">Net Amount</th></tr></thead>';
						table_html += '<tbody>';
						 
							for(var i=0;i<data.cnList.length;i++)
							{
								var item_name=data.cnList[i]['ITEM_NAME'];
								var cn_qty=data.cnList[i]['cn_qty'];
								var rate=data.cnList[i]['RATE'];
								var gross_amnt=cn_qty*rate;
								var disc_rate=0;
				
								var tax_type='NA';
								var tax=data.cnList[i]['disc_rate'];
								var tax_type=data.cnList[i]['CHGNAME'];
								if(tax_type=='Discount')
									var net_amnt=gross_amnt-(tax*gross_amnt/100);
								else
									var net_amnt=gross_amnt+(tax*gross_amnt/100);
								table_html += '<tr>';
									table_html += '<td class="text-center">'+item_name+'</td>';
									table_html += '<td class="text-center">'+cn_qty+'</td>';
									table_html += '<td class="text-center">'+rate+'</td>';
									table_html += '<td class="text-center">'+gross_amnt+'</td>';
									table_html += '<td class="text-center">'+tax_type+'</td>';
									table_html += '<td class="text-center">'+tax+'%</td>';
									table_html += '<td class="text-center">'+net_amnt+'</td>';
								table_html += '</tr>';
							}
						table_html += '</tbody>';
				  table_html += '</table>';
			  $('.itemDetailsTable').html(table_html);
				}
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});
 
 }
});


$('.ResolvedGitClass').click(function(){
 //Fetch All Records
 var per_git_id=$(this).attr('git_id');
 var git_no=$(this).attr('git_no');
 var pd_id=$(this).attr('pd_id');
 var id=$(this).attr('id');
 var type=$(this).text();
 var cn_doc_attc=$(this).attr('cn_doc_attc');
 //Set All Records in Modal
 $('#git_no99').text(git_no); 
 //Appent Table
 if(type=='Attached CN')
 {
	$.ajax({
			type: 'GET', 
			url: '<?php echo e(route("fetch-git-records")); ?>',
			data: {git_no:git_no,type:type},
			success:function(data)
			{
				console.log(data);
				if(data.cnList.length > 0)
				{  
					
					var table_html = '<table id="example2" class="table table-striped table-bordered" cellspacing="0" width="100%">';
					table_html += '<thead><tr><th class="text-center">Item</th><th class="text-center">Qty</th><th class="text-center">Rate</th><th class="text-center">Gross Amt</th><th class="text-center">Charge Type</th><th class="text-center">Charge Apply</th><th class="text-center">Net Amount</th></tr></thead>';
						table_html += '<tbody>';
						 
							for(var i=0;i<data.cnList.length;i++)
							{
								var item_name=data.cnList[i]['ITEM_NAME'];
								var cn_qty=data.cnList[i]['cn_qty'];
								var rate=data.cnList[i]['RATE'];
								var gross_amnt=cn_qty*rate;
								var disc_rate=0;
				
								var tax_type='NA';
								var tax=data.cnList[i]['disc_rate'];
								var tax_type=data.cnList[i]['CHGNAME'];
								if(tax_type=='Discount')
									var net_amnt=gross_amnt-(tax*gross_amnt/100);
								else
									var net_amnt=gross_amnt+(tax*gross_amnt/100);
								table_html += '<tr>';
									table_html += '<td class="text-center">'+item_name+'</td>';
									table_html += '<td class="text-center">'+cn_qty+'</td>';
									table_html += '<td class="text-center">'+rate+'</td>';
									table_html += '<td class="text-center">'+gross_amnt+'</td>';
									table_html += '<td class="text-center">'+tax_type+'</td>';
									table_html += '<td class="text-center">'+tax+'%</td>';
									table_html += '<td class="text-center">'+net_amnt+'</td>';
								table_html += '</tr>';
							}
						table_html += '</tbody>';
				  table_html += '</table>';
				  if(cn_doc_attc!='')
				  {
				  $('.previewing99').html('<a target="_blank" href="'+cn_doc_attc+'">Attachment</a>');
				  }
			  	  $('.itemDetailsTable99').html(table_html);
				}
			},
			error: function (error) 
			{
				
				alert('Error Occured Please Try Again');
			}
		});
 
 }
});



setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 2000);
</script>

