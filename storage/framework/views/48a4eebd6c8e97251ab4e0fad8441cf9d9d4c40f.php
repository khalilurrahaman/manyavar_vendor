<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!--add user popup Modal Start-->
    <link href="<?php echo e(asset('bootstrap-fileinput/bootstrap-fileinput.css')); ?>" rel="stylesheet" type="text/css"/>

<div class=" quotation-pop" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Edit Item</h4>
          </div>
            <div class="modal-body">
                <form action="<?php echo e(route('item-master-update')); ?>" method="post" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

                    <div class="row shorting-area pricechange">
                    <div class="col-sm-12">
                    
                    

                              <?php 
                              $count =count($outreachgal); 
                              $main_image='http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
                              if($data->image!='')
                              {
                              $main_image=URL::to('/upload/product').'/'.$data->image;
                              $count =count($outreachgal)+1; 
                              }
                              ?>
                              <input type="hidden" id="getCount" value="<?php echo e($count); ?>" />

                              <div class="form-group" id="add_more_doc_p">
                                <div id="0" class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 160px; height: 125px;">
                                       <img src="<?php echo e($main_image); ?>" alt="Document Image"/>
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 160px; max-height: 125px;">
                                    </div>
                                    <div>
                                       <span class="btn default btn-file">
                                       <span class="fileinput-new">
                                       Change</span>
                                        
                                       <span class="fileinput-exists">
                                       Change </span>
                                       
                                       <input type="file" name="each_image[]" class="document_image" />
                                       </span>
                                    </div>
                                    
                                 </div>
                                <?php if(!empty($outreachgal)){ ?>
                              <?php $inc=0;?>
                              <?php $__currentLoopData = $outreachgal; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$eachoutreachgal): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                              <?php $inc++;?>
                                 <div id="<?php echo e($inc); ?>" class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 160px; height: 125px;">
                                       <img src="<?php echo e(URL::to('/upload/product').'/'.$eachoutreachgal->product_image); ?>" alt="Document Image"/>
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 160px; max-height: 125px;">
                                    </div>
                                    <div>
                                       <span class="btn default btn-file">
                                       <span class="fileinput-new">
                                      Change</span>
                                        
                                       <span class="fileinput-exists">
                                       Change </span>
                                       
                                       <input type="file" name="each_image[]" class="document_image" />
                                       </span>
                                       <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                                       Remove </a>
                                          <button type="button"  class="btn red-mint button_add" onclick="remove_history_gal('<?php echo e($inc); ?>','<?php echo e($eachoutreachgal->id); ?>');">Remove
                                          </button> 
                                    </div>
                                    
                                 </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>

                            
                          <?php }?>
                              </div>

                           
                              
                              <!-- <p id="add_more_doc_p"></p> -->
                              <div class="form-group">
                                 <button type="button"  class="btn green-mint button_add" onclick="add_more_document();">Add Image</button> 
                              </div>
                    </div>
                     <input type="hidden" name="id" id="id" value="<?php echo e($data->id); ?>" >
                    
                        <div class="col-sm-12">
            <label>Width/Size Required?:</label>
            <input type="checkbox" name="width_size_required" id="width_size_required" value="1" class="form-control" <?php if(!empty($data->width_size_required)){echo 'checked';}?>>
            </div>
                        <div class="col-sm-12">
                            <h5>Design :</h5>
                            <input type="text" name="category1" id="category1" value="<?php echo e($data->cat1); ?>" class="form-control">
                            <span class=" error class_err" id="category1_msg" ></span>
                        </div>
                        <div class="col-sm-12">
                            <h5>Colour :</h5>
                            <input type="text" name="category2" id="category2" value="<?php echo e($data->cat2); ?>" class="form-control">
                            <span class=" error class_err" id="category2_msg" ></span>
                        </div>
                        <div class="col-sm-12">
                            <h5>Category 3 :</h5>
                            <input type="text" name="category3" id="category3" value="<?php echo e($data->cat3); ?>" class="form-control">
                        </div>
                        <div class="col-sm-12">
                            <h5>Width/Size :</h5>
                            <input type="text" name="category4" id="category4" value="<?php echo e($data->cat4); ?>" class="form-control">
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-6">
                        <input type="submit" name="submit" value="Update" onclick="return validateForm()" class="pup-btn">
                        </div>

                        <div class="col-sm-6">
                        <a href="<?php echo e(URL::previous()); ?>" class="pup-btn">Cancel
                        </a>
                        <div class="clearfix"></div>
                    </div>
                    </div>
                </form>
           </div>
        </div>
    </div>
</div>
<script src="<?php echo e(asset('bootstrap-fileinput/bootstrap-fileinput.js')); ?>" type="text/javascript"></script>
<script type="text/javascript">
function remove_history_gal (id,p_id) {
    $.ajax({

        type: 'GET', 
        url: '<?php echo e(route("delete-product-image")); ?>',
        data: {p_id:p_id},
         success: function(data)
         {//alert(data);
            if(data==1){
               $('#'+id).remove();

            }else{

               alert_error('Error,Please Try after some time');
            }
            
         }
      });

};
function remove_history_doc_p (v_div_id) {
   var dd = '#new_v_doc_'+v_div_id;
   $(dd).remove();
};
var new_doc_count = 0;
function add_more_document (){
   
   new_doc_count++;
   
   var countt = $("#getCount").val();
   //alert(countt);
   countt = parseInt(countt) + 1;

   var div = '<div id="new_v_doc_'+countt+'" class="fileinput fileinput-new" data-provides="fileinput">'+
                  '<div class="fileinput-new thumbnail" style="width: 170px; height: 125px;">'+
                     '<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="Document Image"/>'+
                  '</div>'+
                  '<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 170px; max-height: 125px;">'+
                  '</div>'+
                  '<div>'+
                     '<span class="btn default btn-file">'+
                     '<span class="fileinput-new">'+
                     'Select Image</span>'+
                     '<span class="fileinput-exists">'+
                     'Change </span>'+

                     '<input name="each_image[]" class="document_image" type="file" />'+
                     '</span>'+
                     '<button type="button"  class="btn red-mint button_add" onclick="remove_history_doc_p('+countt+');">Remove</button>'+
                     //'<a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">'+
                     //'Remove </a>'+
                  '</div>'+
               '</div>'+
            '</div>';
   //$(".bs-select").selectPicker('refresh'); 

   $('#add_more_doc_p').append(div);
   $(".bs-select").selectpicker('refresh'); 
   $("#getCount").val(countt);
   //alert($("#getCount").val());
}
function validateForm()
  {    
    var ret_val=true;   
   
    var category1=$("#category1").val();
    if(category1==''){
        $("#category1_msg").text('Please Enter Design !').css('color', 'red').show();
        ret_val=false;
    }
    else{
        $("#category1_msg").hide();
    }

    var category2=$("#category2").val();
    if(category2==''){
        $("#category2_msg").text('Please Enter Colour !').css('color', 'red').show();
        ret_val=false;
    }
    else{
        $("#category2_msg").hide();
    }

    var item_image = $("#file").val();
    if(item_image==''){
        $("#image_msg").text('Please Select Image !').css('color', 'red').show();
        ret_val=false;
    }else{
        $("#image_msg").hide();
    }
    return ret_val;
}
</script>
<!--add user popup  Modal End-->
<?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>