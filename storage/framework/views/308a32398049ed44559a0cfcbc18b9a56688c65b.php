
                                <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Sr. No</th>
                                                <th class="text-center">Image</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Category 3</th>
                                                <th class="text-center">Width/Size</th>                                        
                                                <!-- <th class="text-center">Action</th> -->
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <?php $inc=$data->perPage() * ($data->currentPage()-1);; ?>
                                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $eachData): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                     <?php $inc++;
                                     $pGalList=DB::table('product_gallery')->select('*')->where('product_id','=',$eachData->id)->get();
                                      #echo "<pre>";print_r($pGalList);
                                      $data_mouseover=URL::to('/img').'/pre-order-img.jpg';
                                                     
                                       if(!empty($pGalList) && count($pGalList)>0)
                                        {
                                          if($eachData->image!='' && file_exists('upload/product/'.$eachData->image))
                                            $data_mouseover=URL::to('/upload/product').'/'.$eachData->image.'#200';
                                            $timeoutcount=1;
                                            foreach ($pGalList as $pGal) 
                                           {
                                            $timeout=$timeoutcount*400;
                                            $data_mouseover .=' '.URL::to('/upload/product').'/'.$pGal->product_image.'#'.$timeout.' ';
                                            $timeoutcount++;
                                            }

                                           }
                                       $data_mouseover=rtrim($data_mouseover," ");

                                      ?>
                                     
                                    <tr>
                                     <td class="text-center"><?php echo e($inc); ?></td>
                                     <td class="text-center">
                                        <!-- <?php if($eachData->image!=''): ?>
                                        <img src="<?php echo e(URL::to('/upload/product').'/'.$eachData->image); ?>"/ style="height:60px;width: 60px;">
                                        <?php endif; ?> -->
                                        <?php if($eachData->image!=''): ?>
                                            <?php if(file_exists('upload/product/'.$eachData->image)): ?>
                                             <img src="<?php echo e(URL::to('/upload/product').'/'.$eachData->image); ?>" data-mouseover="<?php echo e($data_mouseover); ?>" style="height:60px;width: 60px;"/>
                                            <?php else: ?>
                                              <img src="<?php echo e(URL::to('/img').'/pre-order-img.jpg'); ?>" data-mouseover="<?php echo e($data_mouseover); ?>" style="height:60px;width: 60px;"/>
                                            <?php endif; ?>
                                        <?php else: ?>
                                          <img src="<?php echo e(URL::to('/img').'/pre-order-img.jpg'); ?>" data-mouseover="<?php echo e($data_mouseover); ?>" style="height:60px;width: 60px;"/>
                                        <?php endif; ?>
                                        </td>
                                        <td class="text-center"><?php echo e($eachData->cat1); ?></td>
                                        <td class="text-center"><?php echo e($eachData->cat2); ?></td>
                                        <td class="text-center"><?php echo e($eachData->cat3); ?></td>
                                        <td class="text-center"><?php echo e($eachData->cat4); ?></td>
                                       
                                        <!-- <td class="text-center">                                        
                                        
                                        <a href="<?php echo e(route('item-master-edit',$eachData->id)); ?>"  class="btn btn-success">Edit</a>
                                        </td> -->
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>                            
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                              <div class="col-sm-5">
                              	<div class="dataTables_info" role="status" aria-live="polite">Showing <?php echo e($data->count()); ?> of <?php echo e($data->total()); ?> entries</div>
                              </div>
                              <div class="col-sm-7">
                                    <nav aria-label="Page navigation" class="pull-right">
                                        <?php echo e($data->links()); ?>

                                    </nav>
                                  </div>
                              </div>
                              </span>


 <script type="text/javascript">$('img').on('mouseover', function() {
    var self = this,
        i = 0,
        images = $(this).data('mouseover').split(/\s+/);
    
    (function nextImage() {
        var next = images[i++ % images.length].split('#');
        $(self).data('timeout', setTimeout(function() {
            self.src = next[0];
            nextImage();
        }, next[1]));
    })();
    
}).on('mouseout', function() {
    clearTimeout($(this).data('timeout'));
    this.src = $(this).attr('src');
});
</script>                             
<script>
$(document).ready(function () {
            
            $('#example').dataTable({
                'order': [[ 0, "asc" ]],//Default Column Attribute
                'pageLength': 10,//Per Page Item
                 "searching": false,
                'dom': 'Bfrtip',//Header Info               
                'buttons': [ {extend: 'excel',exportOptions: {columns: [0,2,3,4,5]},footer: false}],
                'aoColumnDefs': [          
                                   { aTargets: [ 0 ], bSortable: true },
                                   { aTargets: [ 1 ], bSortable: false },
                                   { aTargets: [ 2 ], bSortable: true },
                                   { aTargets: [ 3 ], bSortable: false },
                                   { aTargets: [ 4 ], bSortable: false },
                                   { aTargets: [ 5 ], bSortable: false }
                                ],
                "oLanguage": {
                                "oPaginate": {
                                                "sNext": '<i class="fa fa-chevron-right" ></i>',
                                                "sPrevious": '<i class="fa fa-chevron-left" ></i>'
                                            }
                            }
            });
        });
</script>