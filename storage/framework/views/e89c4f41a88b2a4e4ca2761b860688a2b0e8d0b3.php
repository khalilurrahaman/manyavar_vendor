<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title>Manyavar Vendor Panel</title>

    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <link rel="shortcut icon" href="<?php echo e(asset('favicon.png')); ?>" type="image/x-icon" />

    <link href="<?php echo e(url('css/datepicker.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(url('css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(url('css/admin.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(url('css/font-awesome.min.css')); ?>" rel="stylesheet" type="text/css">
    <!-- <script src="<?php echo e(asset('js/jquery-3.1.1.min.js')); ?>"></script> -->
    <script src="<?php echo e(asset('js/jquery-2.1.1.js')); ?>"></script>
    <script src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>
    
</head>
<body>
    <div id="wrapper">
   <header>
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="inner-top">
        <div class="row">
        	<div class="col-sm-2 logo-area">
            	<div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo e(route('ven-dashboard')); ?>">Manyavar Vendor Panel</a>
            </div>
            </div>
            <div class="col-sm-6 paddLR0">
            	<div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="<?php echo e(route('ven-dashboard')); ?>"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="<?php echo e(route('ven-rfq-request')); ?>"><i class="fa fa-fw fa-bar-chart-o"></i> New Item <!--RFQ Request--></a>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-paper-plane"></i>  Purchase Order<b class="caret"></b></a>
                       <ul class="dropdown-menu">
                        <li> 
                            <a href="<?php echo e(route('purchase-order-listing')); ?>"> View All Open</a>
                        </li>
                        <li>
                            <a href="<?php echo e(route('archieve-purchase-order-listing')); ?>"> View Archive</a>
                        </li>
                    </ul> 
                    </li>
                    <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-paper-plane"></i> GIT <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li> 
                            <a href="<?php echo e(route('git-lsting-vendor')); ?>"> View All GIT</a>
                        </li>
                        <li>
                            <a href="<?php echo e(route('add-git-vendor')); ?>">  Add GIT</a>
                        </li>
                        <li>
                            <a href="<?php echo e(route('git-archive-lsting-vendor')); ?>">View GIT Archive </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-pencil-square-o"></i> Action <b class="caret"></b></a>
                    <ul class="dropdown-menu">                    	
                       <!--  <li>
                            <a href="<?php echo e(route('all-requst-widthratechange')); ?>">View Width and Rate Change Request</a>
                        </li>
                        <li>
                            <a href="<?php echo e(route('all-requst-dateext')); ?>">View Due Date Ext Request</a>
                        </li> -->
                        <li>
                            <a href="<?php echo e(route('all-requst-vendor')); ?>">View All Request</a>
                        </li>
                         <li>
                          <!--   <a href="<?php echo e(route('sample-request')); ?>">Request for Inspection</a> -->
                            <a href="<?php echo e(route('sample-request')); ?>">Sample for Approval</a>
                        </li>
                        <li>
                            <a href="<?php echo e(route('rate-changing-vendor')); ?>">Request for Rate Changes</a>
                        </li>
                        <li>
                            <a href="<?php echo e(route('cutting-req')); ?>">Request for Sample</a>
                        </li>
                        <li>
                            <a href="<?php echo e(route('width-changing')); ?>">Request for Width Changes</a>
                        </li>
                        <li>
                            <a href="<?php echo e(route('date-extention-req')); ?>">Request for Due Date Extension</a>
                        </li>
                        <li>
                            <a href="<?php echo e(route('calcellation-req')); ?>">Request for Cancellation</a>
                        </li>
                        <li>
                            <a href="<?php echo e(route('support-ticket-req')); ?>">Others <!--Create Support Ticket --></a>
                        </li>
                       
                    </ul>
                </li>
                <li>
                    <a href="<?php echo e(route('goods-receive-list')); ?>"><i class="fa fa-exchange"></i> Goods Return</a>
                </li>
                <li>
                    <a href="<?php echo e(route('vendor-price-list')); ?>"><i class="fa fa-exchange"></i>Price List</a>
                </li>
                <li>
                    <a href="<?php echo e(route('document-listing')); ?>"><i class="fa fa-list"></i>Documents</a>
                </li>
                <!--<li>
                    <a href="<?php echo e(route('reports-req')); ?>"><i class="fa fa-comment"></i> Reports</a>
                </li>-->
             
            
               
                    
                </ul>
            </div>
            </div>
            <div class="col-sm-3 pull-right">
            	<ul class="nav navbar-right top-nav">
              
                <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo e(Auth::user()->name); ?><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?php echo e(route('ed-profile-req')); ?>"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo e(route('change-pass')); ?>"><i class="glyphicon glyphicon-lock"></i> Change Password</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo e(route('logout')); ?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
              <input type="button" id="trigger_fullscreen" value="click to toggle fullscreen" onclick="toggleFullScreen(document.body)">
            </div>
        </div>
        <div class="clearfix"></div>
        </div>
        </nav>
</header>
<script type="text/javascript">

function toggleFullScreen() {
 if ((document.fullScreenElement && document.fullScreenElement !== null) ||    
   (!document.mozFullScreen && !document.webkitIsFullScreen)) {
    if (document.documentElement.requestFullScreen) {  
      document.documentElement.requestFullScreen();  
    } else if (document.documentElement.mozRequestFullScreen) {  
      document.documentElement.mozRequestFullScreen();  
    } else if (document.documentElement.webkitRequestFullScreen) {  
      document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
    }  
  } else {  
    if (document.cancelFullScreen) {  
      document.cancelFullScreen();  
    } else if (document.mozCancelFullScreen) {  
      document.mozCancelFullScreen();  
    } else if (document.webkitCancelFullScreen) {  
      document.webkitCancelFullScreen();  
    }  
  }  
}


</script>