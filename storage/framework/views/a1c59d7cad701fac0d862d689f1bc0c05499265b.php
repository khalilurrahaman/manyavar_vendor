<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- CSS -->
<link rel="stylesheet" href="<?php echo e(asset('fancybox/css/fancybox/jquery.fancybox-buttons.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('fancybox/css/fancybox/jquery.fancybox-thumbs.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('fancybox/css/fancybox/jquery.fancybox.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('fancybox/demo/demo.css')); ?>">
<!-- DELETE -->
<div id="page-wrapper">
            <div class="container-fluid inner-body-area">
             
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default add-padding">
                            <div class="panel-heading">
                            <div class="row">
                                	<div class="col-sm-6">
                                    	<h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i> View All RFQ Request</h3>
                                    </div>
                                    <div class="col-sm-6">
                                    
                                    <div class="row shorting-area vendor-filter">
                                    
                                    <div class="col-sm-3 pull-right ">
                                        <a href="javascript:void(0)" id="show-btn" class="btn btn-success">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                        <div class="clearfix"></div>
                                    </div>
                            		
                              
                                <div class="clearfix"></div>
                                
                            </div>
                                    
                                    
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body custome-table">
                            <div class="row shorting-area">
                            	
                            
                            <div class="col-sm-12">
                            <div id="mySidenavR" class="sidenavR" style="display:none;">
<div class="filter-right">
<div class="filter-area rfq-filter">
<!--<h5>Vendor List Filter</h5>-->
    <a href="javascript:void(0)" class="closebtn" id="hide-filter">×</a>
        <div class="col-sm-2">             
            <select class="select-drop" id="req_id" name="req_id">
            	<option value="">REQ ID</option>
                <?php if(!empty($req_id_list) && count($req_id_list)>0): ?>
                <?php $__currentLoopData = $req_id_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $req_id): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                	<option value="<?php echo e($req_id->req_id); ?>"><?php echo e($req_id->req_id); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                <?php endif; ?>
            </select>
        </div>
        <div class="col-sm-2">
            <select class="select-drop" id="cat1" name="cat1">
            	<option value="">Design</option>
                <?php if(!empty($cat1_list) && count($cat1_list)>0): ?>
                <?php $__currentLoopData = $cat1_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat1): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                	<option value="<?php echo e($cat1->cat1); ?>"><?php echo e($cat1->cat1); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                <?php endif; ?>
            </select>
        </div>
        <div class="col-sm-2">
            <select class="select-drop" id="cat2" name="cat2">
            	<option value="">Colour</option>
                <?php if(!empty($cat2_list) && count($cat2_list)>0): ?>
                <?php $__currentLoopData = $cat2_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat2): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                	<option value="<?php echo e($cat2->cat2); ?>"><?php echo e($cat2->cat2); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                <?php endif; ?>
            </select>
        </div>
        <div class="col-sm-2">
            <select class="select-drop" id="name" name="name">
            	<option value="">Name</option>
                <?php if(!empty($vendor_name_list) && count($vendor_name_list)>0): ?>
                <?php $__currentLoopData = $vendor_name_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vendor_name): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                	<option value="<?php echo e($vendor_name->id); ?>"><?php echo e($vendor_name->name); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                <?php endif; ?>
            </select>
        </div>
        
        <div class="col-sm-2">
        <div class="form-group">
                <div class="input-group date">
                    <input type="text" class="form-control date_picker" placeholder="Date"  id="date" name="date"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        
        
       <!-- <div class="col-sm-2">
            <select class="select-drop" id="price" name="price">
            	<option value="">Price</option>
                <?php if(!empty($rate_list) && count($rate_list)>0): ?>
                <?php $__currentLoopData = $rate_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rate): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                	<option value="<?php echo e($rate->RATE); ?>"><?php echo e($rate->RATE); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                <?php endif; ?>
            </select>
        </div>-->
        
        <div class="col-sm-2">
        	<a href="javascript:void(0)" class="btn btn-success" onclick="applyFilter();">Apply Filter</a>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-1"> 
            <a href="javascript:void(0)" class="btn btn-success dateCheck reset" onclick="">Clear</a>
            <div class="clearfix"></div>  
        </div>
<div class="clearfix"></div>        
	</div>
    <div class="clearfix"></div>
    </div>    
</div>
                            
                            </div>
                            
                            
                            </div>
                            <span id="filterResult">
                                <div class="table-responsive view-table">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">REQ ID</th>
                                                <th class="text-center">Image</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Vendor Name</th>
                                                <th class="text-center">Remarks</th>
                                                <th class="text-center">Created At</th>
                                                <th class="text-center width-a">Quote ID</th>
                                                <th class="text-center width-b">Quote Date</th>
                                                <th class="text-center width-d">Rate</th>
                                                <th class="text-center width-d">Width</th>
                                               <!-- <th class="text-center">Status</th>-->
                                                <th class="text-center width-e">Last Comments</th>
                                                <!--<th class="text-center">Comments</th>-->
                                                <th class="text-center">Notify</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody >
                                    
                                   
                                    </tbody>
                               </table>
                              </div>
                              
                              </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>

<?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script>

$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        //$('#load a').css('color', '#dfecf6');
        //$('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');

        var url = $(this).attr('href'); 
		var page=$(this).attr('href').split('page=')[1]; 
		
		var req_id=$("#req_id").val();
		var cat1=$("#cat1").val();
		var cat2=$("#cat2").val();
		var name=$("#name").val();
		//var quote_id=$("#quote_id").val();
		var date=$("#date").val();
		//var price=$("#price").val();
	
        filter(page,req_id,cat1,cat2,name,date);
        //window.history.pushState("", "", url);
    });

    
});

$( "#hide-filter" ).click(function() {
	$("#req_id").val('');
	$("#cat1").val('');
	$("#cat2").val('');
	$("#name").val('');
	$("#date").val('');
	//$("#price").val('');
		
	filter(); 
});

$(document).ready(function () {        
    applyFilter();
});


function applyFilter(){	
	
		var req_id=$("#req_id").val();
		var cat1=$("#cat1").val();
		var cat2=$("#cat2").val();
		var name=$("#name").val();
		//var quote_id=$("#quote_id").val();
		var date=$("#date").val();
		//var price=$("#price").val();
	
        filter(1,req_id,cat1,cat2,name,date);
}

function filter(page=1,req_id='',cat1='',cat2='',name='',date=''){	

	$('.loading').show();
	
	$.ajax({
		type: 'GET', 
		url: '<?php echo e(route("filter-rfq-request")); ?>',
		data: {req_id:req_id,cat1:cat1,cat2:cat2,name:name,date:date,page:page},
		success:function(data)
		{
			$('#filterResult').html(data);
			$('.loading').hide();
		},
		error: function (error) 
		{
			
			alert('Error Occured Please Try Again');
		}
	})
}
</script>
<script>
    function closeRfqRequest(rfq_id){
        var x = confirm("Are you sure you want to Conclude " +rfq_id);
        if (x)
            return true;
        else
            return false;
    }


     //--For Clearing Filter Data Elements-----//
    $(".reset").bind("click", function() {
    $("input[type=text], textarea").val("");
    $("#req_id").val("");
    $("#cat1").val("");
    $("#cat2").val("");
    $("#name").val("");

    });   
</script>
<script>
    function notifyRfq(rfq_id,vendor_id){

        if(!confirm("Are you sure You want to notify "))
        {
            return false;
        }

        var token_cmnts='<?php echo e(csrf_token()); ?>';
        var c_url='<?php echo e(route("notify-vendor")); ?>';

       $.ajax({
        type: 'POST', 
        url: c_url,
        data: {vendor_id:vendor_id,rfq_id:rfq_id, _token:token_cmnts},
        success:function(data)
        {          
            $('#notify_vendor'+rfq_id+'_'+vendor_id).text('Notified');
        },
        error: function (error) 
        {
          console.log(error);
          alert('Error Occured Please Try Again');
        }
      });
    }
</script>

<!-- ********************************************** -->
<!-- JavaScript at the bottom for fast page loading -->
<!-- ********************************************** -->

<!-- Grab Google CDN's jQuery, fall back to local if offline -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php echo e(asset('fancybox/js/libs/jquery-1.7.1.min.js')); ?>"><\/script>')</script>
    
<!-- FancyBox -->
    <script src="<?php echo e(asset('fancybox/js/fancybox/jquery.fancybox.js')); ?>"></script>
    <script src="<?php echo e(asset('fancybox/js/fancybox/jquery.fancybox-buttons.js')); ?>"></script>
    <script src="<?php echo e(asset('fancybox/js/fancybox/jquery.fancybox-thumbs.js')); ?>"></script>
    <script src="<?php echo e(asset('fancybox/js/fancybox/jquery.easing-1.3.pack.js')); ?>"></script>
    <script src="<?php echo e(asset('fancybox/js/fancybox/jquery.mousewheel-3.0.6.pack.js')); ?>"></script>
    
    <script type="text/javascript">
        var nc=$.noConflict();
        nc(document).ready(function() {
        nc(".fancybox").fancybox();
        });
    </script>
