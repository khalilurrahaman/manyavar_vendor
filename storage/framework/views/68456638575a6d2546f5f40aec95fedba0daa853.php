<div class="panel-body custome-table">
                            <div class="table-responsive ">
                                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <!-- <th class="text-center">Prodict Id</th> -->
                                            <th class="text-center">Image</th>
                                            <th class="text-center">Vendor Name</th>
                                            <th class="text-center">Design</th>
                                            <th class="text-center">Colour</th>
                                            <th class="text-center">Defective Remarks</th>
                                            <th class="text-center">Quantity</th>
                                            <th class="text-center">PRN No</th>
                                            <th class="text-center">PRN Date</th>
                                            <th class="text-center">Docket No</th>
                                            <th class="text-center">Courier Name</th>
                                            <!-- <th class="text-center">Admin Remarks</th> -->
                                            <th class="text-center">Vendor Remarks</th>
                                            <th class="text-center">Last Comments</th>
                                            <th class="text-center">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $inc=0;?>
                                    <?php if(count($receiveList)): ?>
                                        <?php $__currentLoopData = $receiveList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $eachReturn): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                         <?php $inc++;?>
                                        <tr>
                                             <td class="text-center">
                            <div class="gal"> 
                             <?php 
                               $main_image=URL::to('/img').'/pre-order-img.jpg';
                               if($eachReturn->image!='' && file_exists('upload/temp/'.$eachReturn->image))
                               $main_image=URL::to('/upload/temp').'/'.$eachReturn->image;
                               
                              ?>
                               <a class="fancybox" rel="group<?php echo e($inc); ?>" href="<?php echo e($main_image); ?>" >
                                <img src="<?php echo e($main_image); ?>" width="150">
                               </a>
                                                    
                               <a class="fancybox" rel="group<?php echo e($inc); ?>" href="<?php echo e($main_image); ?>" style="display:none;"><img src="<?php echo e($main_image); ?>" alt=""></a>
                              
                            </div>
                        </td>
                                            <td class="text-center"><?php echo e($eachReturn->name); ?></td>
                                            <td class="text-center"><?php echo e($eachReturn->category1); ?></td>
                                            <td class="text-center"><?php echo e($eachReturn->category2); ?></td>
                                            <td class="text-center" title="<?php echo e($eachReturn->reason); ?>">
                                            <?php
                                            $i=strlen($eachReturn->reason); 
                                            if(strlen($i>25)) 
                                                echo substr($eachReturn->reason,0,22).'...';
                                            else 
                                                echo $eachReturn->reason;

                                            ?>
                                                
                                            </td>
                                            <td class="text-center"><?php echo e($eachReturn->return_quantity); ?></td>
                                            <td class="text-center"><?php echo e($eachReturn->prn_no); ?></td>
                                            <td class="text-center">
                                            <?php if($eachReturn->prn_date!='0000-00-00'): ?>
                                            <?php echo e(date('d/m/Y',strtotime($eachReturn->prn_date))); ?>

                                            <?php endif; ?>
                                            </td>
                                            <td class="text-center"><?php echo e($eachReturn->docket_no); ?></td>
                                            <td class="text-center"><?php echo e($eachReturn->courier_name); ?></td>
                                            <!-- <td class="text-center" title="<?php echo e($eachReturn->comments); ?>">
                                            <?php
                                            $i=strlen($eachReturn->comments); 
                                            if(strlen($i>25)) 
                                                echo substr($eachReturn->comments,0,22).'...';
                                            else 
                                                echo $eachReturn->comments;

                                            ?>
                                                
                                            </td> -->
                                            <td class="text-center" title="<?php echo e($eachReturn->vendor_comments); ?>">
                                            <?php
                                            $i=strlen($eachReturn->vendor_comments); 
                                            if(strlen($i>25)) 
                                                echo substr($eachReturn->vendor_comments,0,22).'...';
                                            else 
                                                echo $eachReturn->vendor_comments;

                                            ?>
                                            </td>
                                            <td class="text-center">
                                            
                                            <?php if(!empty($eachReturn->good_comments) && count(($eachReturn->good_comments))>0): ?>
                                            <?php                                       
                                       
                                            if(strlen($eachReturn->good_comments[0]->comments)<=20)                                 
                                             echo ($eachReturn->good_comments[0]->comments);
                                            else
                                             echo substr($eachReturn->good_comments[0]->comments,0,18).'..';  

                                            ?>
                                                <?php if($eachReturn->status==1): ?>
                                                <a href="<?php echo e(route('goods-comments', $eachReturn->id)); ?>" class="btn btn-warning btn-xs pull-right">
                                                <?php echo count($eachReturn->good_comments)?>
                                                Comment(s)
                                                </a>
                                                <?php else: ?>
                                                <a class="btn btn-warning btn-xs pull-right">
                                                <?php echo count($eachReturn->good_comments)?>
                                                Comment(s)
                                                </a>
                                                <?php endif; ?>
                                            <?php else: ?>
                                                <?php if($eachReturn->status==1): ?>
                                                <a href="<?php echo e(route('goods-comments', $eachReturn->id)); ?>" class="btn btn-warning btn-xs pull-right">                     
                                                Comments
                                                </a>
                                                <?php else: ?>
                                                <a class="btn btn-warning btn-xs pull-right">
                                                Comments
                                                </a>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                            </td>
                                            <td class="text-center">
                                            <?php if($eachReturn->status==1): ?><button onclick="getRequest('<?php echo e($eachReturn->id); ?>','<?php echo e($eachReturn->category1); ?>','<?php echo e($eachReturn->prn_no); ?>','<?php echo e($eachReturn->docket_no); ?>','<?php echo e($eachReturn->courier_name); ?>');" data-toggle="modal" data-target="#addremark" class="btn btn-success">Accept</button>
                                            <?php else: ?><button href="javascript:void(0)" class="btn btn-danger">Closed</button>
                                            <?php endif; ?></td>
                                        </tr> 
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?> 
                                        <?php else: ?>
                                        <td valign="top" colspan="14" class="dataTables_empty">No data available in table</td>
                                        <?php endif; ?>                          
                                    </tbody>
                               </table>
                          </div>
                          <div class="row">
                              <div class="col-sm-6">
                                <div class="dataTables_info" role="status" aria-live="polite">Showing <?php echo e($receiveList->count()); ?> of <?php echo e($receiveList->total()); ?> entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                        <?php echo e($receiveList->links()); ?>

                                    </nav>
                                  </div>
                            </div>

                        </div>
                        <!-- /#Hover Image -->
    <script type="text/javascript">$('img').on('mouseover', function() {
    var self = this,
        i = 0,
        images = $(this).data('mouseover').split(/\s+/);
    
    (function nextImage() {
        var next = images[i++ % images.length].split('#');
        $(self).data('timeout', setTimeout(function() {
            self.src = next[0];
            nextImage();
        }, next[1]));
    })();
    
}).on('mouseout', function() {
    clearTimeout($(this).data('timeout'));
    this.src = $(this).attr('src');
});
</script>