 <div class="panel-body custome-table">
                              
                                <div class="table-responsive view-table">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Sr. No</th>
                                                <th class="text-center">Request</th>
                                                <th class="text-center">PO Details</th>
                                                <th class="text-center">Product Info<br>(Design and Color)</th>
                                                <th class="text-center">Old Values<br>(Price and Width)</th>
                                                <th class="text-center">Requested Values<br>(Price and Width)</th>
                                                <th class="text-center">Negotiated Rate</th>
                                                <th class="text-center">Accepted Values<br>(Price and Width)</th>
                                                <th class="text-center">Delivery Date</th>
                                                <th class="text-center">Ext. Date</th>
                                                <th class="text-center">Admin Remarks</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-center">Action</th>
                                                <th class="text-center">Last Comments</th>
                                            </tr>
                                        </thead>
                                    <tbody id="req_request">
                                    <?php if(!empty($request_list)): ?>
                                     <?php $inc=$request_list->perPage() * ($request_list->currentPage()-1);;?>
                                    <?php $__currentLoopData = $request_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $req): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <?php $inc++; ?>
                                    <tr>
                                      <td class="text-center"><?php echo e($inc); ?></td>
                                      <td class="text-center">
                                      <?php echo e(date("d/m/y",strtotime($req->created_at))); ?><br>
                                      <?php 
                                        $type='';
                                        if($req->action_type=='rate_change')
                                          $type='Rate Change';          
                                        elseif($req->action_type=='width_change') 
                                          $type='Width Change';
                                         elseif($req->action_type=='cutting')
                                          $type='Sample';          
                                          elseif($req->action_type=='cancel') 
                                           $type='Cancellation';
                                          elseif($req->action_type=='others') 
                                           $type='Others';
                                          elseif($req->action_type=='sample') 
                                           $type='Sample for Approval';
                                         elseif($req->action_type=='date_ext')
                                          $type='Due Date Extention'; 
                                         

                                        ?>
                                        <?php echo e($type); ?>

                                      </td>  
                                      <td class="text-center">
                                        <?php echo e($req->po_no); ?><br>
                                        <?php echo e(date("d/m/y",strtotime($req->order_date))); ?>


                                      </td>   
                                      <td class="text-center">
                                        <?php echo e($req->cat1); ?><br>
                                        <?php echo e($req->cat2); ?>

                                      </td>                 
                                        <td class="text-center">
                                        <?php if(!empty($req->old_value)): ?><?php echo e($req->old_value); ?><?php else: ?><?php echo e('NA'); ?><?php endif; ?>
                                        <br>
                                        <?php if(!empty($req->old_width)): ?><?php echo e($req->old_width); ?>"<?php else: ?><?php echo e('NA'); ?><?php endif; ?>
                                        </td>
                                        <td class="text-center">
                                        <?php if(!empty($req->new_rate)): ?><?php echo e($req->new_rate); ?><?php else: ?><?php echo e('NA'); ?><?php endif; ?><br>
                                        <?php if(!empty($req-> new_width)): ?><?php echo e($req->new_width); ?>"<?php else: ?><?php echo e('NA'); ?><?php endif; ?>
                                        </td>
                                        <td class="text-center">
                                        <?php if($req->new_rate!=$req->negotiated_rate): ?>
                                         <?php echo e($req->negotiated_rate); ?><?php else: ?><?php echo e('NA'); ?><?php endif; ?>
                                        </td>
                                        <td class="text-center">
                                        <?php if($req->action_type=='rate_change'|| $req->action_type=='width_change'): ?>
                                        <?php if($req->new_rate==$req->negotiated_rate): ?><?php echo e($req->negotiated_rate); ?><?php else: ?><?php echo e($req->accepted_rate); ?><?php endif; ?><br>
                                        <?php if(!empty($req->new_width)): ?><?php echo e($req->new_width); ?>"<?php endif; ?>
                                        <?php else: ?><?php echo e('NA'); ?><?php endif; ?>
                                        </td>
                                        <td class="text-center"><?php if(!empty($req->po_date)): ?><?php echo e(date("d/m/y",strtotime($req->po_date))); ?><?php else: ?><?php echo e('NA'); ?><?php endif; ?></td>                                     
                                        <td class="text-center"><?php if(!empty($req->ext_date)): ?><?php echo e(date("d/m/y",strtotime($req->ext_date))); ?><?php else: ?><?php echo e('NA'); ?><?php endif; ?></td>
                                        <td class="text-center"><?php if(!empty($req->admin_remarks)): ?><?php echo e($req->admin_remarks); ?><?php endif; ?></td>  
                                        <td class="text-center">
                                        <?php if($req->status=='new'): ?>
                                       <span class="sm-btn btn-primary btn-xs">New</span>
                                       <?php elseif($req->status=='close'): ?>
                                       <span class="sm-btn btn-xs btn-warning">Accepted</span>
                                       <?php elseif($req->status=='inprocess'): ?>
                                       <span class="sm-btn btn-primary btn-xs">In Process</span>
                                      <?php elseif($req->status=='accept'): ?>
                                       <span class="sm-btn btn-xs btn-warning">Accepted</span>
                                       <?php elseif($req->status=='reject'): ?>
                                       <span class="sm-btn btn-xs btn-danger">Rejected</span> 
                                       <?php endif; ?>
                                       </td>
                                        
                                     <td class="text-center">
                                       <?php if($req->status=='new'): ?>
                                       <span class="sm-btn btn-primary btn-xs">Pending</span> 
                                       <?php
                                       $time = strtotime($req->created_at);

                                        $curtime = time();

                                        if(($curtime-$time) > 3600 && ($req->status=='new') && ($req->notify==0)) { //seconds(72 hrs)?>   
                                         <a href="<?php echo e(route('vendor-notify-request', $req->id)); ?>" onclick="return notifyReq('<?php echo e($req->id); ?>');" class="btn btn-success btn-danger">Notify</a>
                                        <?php } elseif(($curtime-$time) > 3600 && ($req->status=='new') && ($req->notify==1)) {?>
                                       <span class="sm-btn btn-xs btn-danger">Notified</span>
                                         
                                       <?php }
                                       ?> 
                                       <?php elseif($req->status=='close'): ?>
                                        <span class="sm-btn btn-xs btn-warning">Closed</span>
                                        <?php elseif($req->status=='inprocess'): ?>
                                        <a onclick="return acceptRequest('<?php echo e($req->id); ?>','<?php echo e($req->negotiated_rate); ?>');" class="btn btn-success btn-xs">Accept</a>
                                       <a href="<?php echo e(route('vendor-reject-request',$req->id)); ?>" onclick="return rejectRequest('<?php echo e($req->id); ?>');" class="btn btn-success btn-danger">Reject</a>
                                       <?php elseif($req->status=='accept'): ?>
                                       <span class="sm-btn btn-xs btn-warning">Closed</span>
                                        <?php elseif($req->status=='reject'): ?> 
                                        <span class="sm-btn btn-xs btn-warning">Closed</span> 
                                        <?php endif; ?>
                                        <!-- <a href="" onclick="return rejectRequest('<?php echo e($req->id); ?>');" class="btn btn-success btn-danger">Reject</a> -->
                                     </td>
                                      
                                      <td class="text-center">
                                         <?php if(!empty($req->req_comments) && count(($req->req_comments))>0): ?>
                                        <?php                                       
                                       
                                        if(strlen($req->req_comments[0]->comments)<=20)                                 
                                           echo ($req->req_comments[0]->comments);
                                        else
                                         echo substr($req->req_comments[0]->comments,0,18).'..';  

                                        ?>
                                          <a href="<?php echo e(route('requestwise-comments', $req->id)); ?>" class="pull-right btn btn-warning btn-xs">
                                          <?php echo count($req->req_comments)?>
                                          Comment(s)</a>

                                        <?php else: ?>
                                          <a href="<?php echo e(route('requestwise-comments', $req->id)); ?>" class="btn btn-warning btn-xs pull-right">
                                          Comments</a>
                                         <?php endif; ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                    <?php endif; ?> 
                                </tbody>
                               </table>
                              </div>
                              
                              <div class="row">
                              <div class="col-sm-6">
                                <div class="dataTables_info" role="status" aria-live="polite">Showing <?php echo e($request_list->count()); ?> of <?php echo e($request_list->total()); ?> entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                       <?php echo e($request_list->links()); ?>  
                                    </nav>
                                  </div>
                              </div>
                            </div>