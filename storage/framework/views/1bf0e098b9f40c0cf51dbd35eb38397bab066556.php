<div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Name</th>
                                                <!-- <th class="text-center">Department</th>
                                                <th class="text-center">Section</th>
                                                <th class="text-center">Devision</th> -->
                                                <th class="text-center">E-mail ID</th>
                                                <th class="text-center">Phone No</th>
                                                <th class="text-center">Address</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-center">Action</th>
                                                
                                            </tr>
                                        </thead>
                                    <tbody>
                                    <?php if(count($vendor_list)): ?>
                                    <?php $__currentLoopData = $vendor_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <tr>
                                        <td class="text-center"><?php echo e($data->name); ?></td>
                                       
                                        <td class="text-center"><?php echo e($data->email); ?></td>
                                        <td class="text-center"><?php echo e($data->mobile); ?></td>
                                        <td class="text-center"><?php echo e($data->address); ?></td>
                                        <td class="text-center">
                                        <?php if($data->status==1): ?>
                                        Active
                                        <?php else: ?>
                                        Inactive
                                        <?php endif; ?>

                                        </td>
                                        <td class="text-center">
                                        <a onclick="" href="<?php echo e(route('set-ven-password',$data->id)); ?>" class="btn btn-success">Set Password</a>
                                        <?php if($data->status==1): ?>
                                        <a href="<?php echo e(route('vendor-status-deact',$data->id)); ?>" onclick="return ConfirmDeactive()" class="btn btn-danger">Inactive</a>
                                        <?php else: ?>
                                        <a href="<?php echo e(route('vendor-status-act',$data->id)); ?>" onclick="return ConfirmActive()" class="btn btn-success">Active</a>
                                        <?php endif; ?>

                                        </td>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                    <?php else: ?>
                                    <tr>
                                        <td>
                                            No Vendor Available !
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <?php endif; ?>
                                      
                                </tbody>
                               </table>
                              </div>
                              
                              
                              
<script type="text/javascript">
$(document).ready(function () {
            
            $('#example').dataTable({
                'order': [[ 0, "asc" ]],//Default Column Attribute
                'pageLength': 10,//Per Page Item
                 "searching": false,
                'dom': 'Bfrtip',//Header Info               
                'buttons': [ {extend: 'excel',exportOptions: {columns: [0,1,2,3,4]},footer: false}],
                'aoColumnDefs': [          
                                   { aTargets: [ 0 ], bSortable: true },
                                   { aTargets: [ 1 ], bSortable: false },
                                   { aTargets: [ 2 ], bSortable: false },
                                   { aTargets: [ 3 ], bSortable: false },
                                   { aTargets: [ 4 ], bSortable: false },
                                   { aTargets: [ 5 ], bSortable: false }
                                ],
                "oLanguage": {
                                "oPaginate": {
                                                "sNext": '<i class="fa fa-chevron-right" ></i>',
                                                "sPrevious": '<i class="fa fa-chevron-left" ></i>'
                                            }
                            }
            });
        });
$( "#hide-filter" ).click(function() {
    
    $("#name").val('');
    $("#email").val('');
   
    
    //filter(); 
});

        </script>