<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- CSS -->
<link rel="stylesheet" href="<?php echo e(asset('fancybox/css/fancybox/jquery.fancybox-buttons.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('fancybox/css/fancybox/jquery.fancybox-thumbs.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('fancybox/css/fancybox/jquery.fancybox.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('fancybox/demo/demo.css')); ?>">
<!-- DELETE -->
<!--Start Shorting-->
<style>
.dt-buttons{margin-right:0px !important;}
.dt-buttons>a {padding-left:18px;}
</style>
<script src="<?php echo e(asset('dataTable/css/jquery.dataTables.min.css')); ?>"></script>
<script src="<?php echo e(asset('dataTable/css/buttons.dataTables.min.css')); ?>"></script>

   
<script src="<?php echo e(asset('dataTable/js/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(asset('dataTable/js/dataTables.buttons.min.js')); ?>"></script>   
<script src="<?php echo e(asset('dataTable/js/buttons.flash.min.js')); ?>"></script>
<script src="<?php echo e(asset('dataTable/js/jszip.min.js')); ?>"></script>   
<!-- <script src="<?php echo e(asset('dataTable/js/pdfmake.min.js')); ?>"></script> -->
<script src="<?php echo e(asset('dataTable/js/vfs_fonts.js')); ?>"></script>
<script src="<?php echo e(asset('dataTable/js/buttons.html5.min.js')); ?>"></script>
<script src="<?php echo e(asset('dataTable/js/buttons.print.min.js ')); ?>"></script>
<!--------------------------------End Shorting-------------------------->
<?php 
use App\Http\Controllers\Controller;
if(Controller::userAccessibilityCheck('add_return'))
	$add_return_exist=1;
if(Controller::userAccessibilityCheck('goods-return'))
	$goods_return_exist=1;
?>
    <div id="page-wrapper">
        <div class="container-fluid inner-body-area">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default add-padding">
                        <div class="panel-heading">
                            <div class="row">
                            	<div class="col-sm-6"><h3 class="panel-title">
                                <i class="fa fa-users" aria-hidden="true"></i> Goods Return List</h3></div>
                                <div class="col-sm-6">
                                    <div class="shorting-area">                          
                                         <?php if(!empty($add_return_exist)){?>                               
                                        <div class="col-sm-3 pull-right">
                                            <a href="<?php echo e(route('goods-returns-add-form')); ?>" data-toggle="modal" class="btn btn-success">
                                            <i class="fa fa-plus" aria-hidden="true"></i> Add Returns</a>
                                        </div>
                                        <?php }?>
                                        <?php if(!empty($goods_return_exist)){?>
                                        <div class="col-sm-2 pull-right">
                                            <a href="<?php echo e(route('goods-returns-list')); ?>" data-toggle="modal" class="btn btn-success">
                                            <i  aria-hidden="true"></i> Pending</a>
                                        </div> 
                                         <?php }?>
                                        <div class="col-sm-2 purchase-filter pull-right">
                                            <a href="javascript:void(0)" onclick="getVendor(0);" id="show-btn" class="btn btn-success">
                                            <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                        <div class="col-sm-12">
                            <div id="mySidenavR" class="sidenavR" style="display:none;">
<div class="filter-right">
<div class="filter-area">
<!--<h5>Vendor List Filter</h5>-->
    <a href="javascript:void(0)" class="closebtn" id="hide-filter">×</a>
        
        
        <div class="col-sm-2">
           <!-- <select class="select-drop" id="name" name="vendore_name">
                <option value="">Vendore Name</option>
                <?php if(!empty($vendor_name_list) && count($vendor_name_list)>0): ?>
                <?php $__currentLoopData = $vendor_name_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vendore): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <option value="<?php echo e($vendore->id); ?>"><?php echo e($vendore->name); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                <?php endif; ?>
            </select> -->
            <select class="select-drop" id="name" name="vendore_name">
                <option value="">Select Vendor</option>               
            </select>
        </div>
        
        
        <div class="col-sm-2">
            <!-- <select class="select-drop" id="cat1" name="category1">
                <option value="">Design</option>
                <?php if(!empty($cat1_list) && count($cat1_list)>0): ?>
                <?php $__currentLoopData = $cat1_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat1): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <option value="<?php echo e($cat1->category1); ?>"><?php echo e($cat1->category1); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                <?php endif; ?>
            </select> -->
            <select class="select-drop" id="cat1" name="category1">
                <option value="">Select Design</option>                
            </select>
        </div>
        <div class="col-sm-2">
            <!-- <select class="select-drop" id="cat2" name="category2">
                <option value="">Colour</option>
                <?php if(!empty($cat2_list) && count($cat2_list)>0): ?>
                <?php $__currentLoopData = $cat2_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat2): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <option value="<?php echo e($cat2->category2); ?>"><?php echo e($cat2->category2); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                <?php endif; ?>
            </select> -->
            <select class="select-drop" id="cat2" name="category2">
                <option value="">Select Colour</option>
            </select>


        </div>
        <div class="col-sm-2">
            <a href="javascript:void(0)" class="btn btn-success" onclick="applyFilter();">Apply Filter</a>
            <div class="clearfix"></div>
        </div>
         <div class="col-sm-1"> 
                          <a href="javascript:void(0)" class="btn btn-success dateCheck reset" onclick="">Clear</a>
                          <div class="clearfix"></div>  
                         </div>
<div class="clearfix"></div>        
    </div>
    <div class="clearfix"></div>
    </div>    
</div>
                            
                            </div>
                        <div class="clearfix"></div>
                        <span id="filterResult">
                        <div class="panel-body custome-table">
                            <div class="table-responsive ">
                                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <!-- <th class="text-center">Prodict Id</th> -->
                                            <th class="text-center">Image</th>
                                            <th class="text-center">Vendor Name</th>
                                            <th class="text-center">Design</th>
                                            <th class="text-center">Colour</th>
                                            <th class="text-center">Defective Remarks</th>
                                            <th class="text-center">Quantity</th>
                                            <th class="text-center">PRN No</th>
                                            <th class="text-center">PRN Date</th>
                                            <th class="text-center">Docket No</th>
                                            <th class="text-center">Courier Name</th>
                                            <!-- <th class="text-center">Admin Remarks</th> -->
                                            <th class="text-center">Vendor Remarks</th>
                                            <th class="text-center">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                                        
                                    </tbody>
                               </table>
                          </div>
                      
                        </div>
                        </span>
                    </div>
                </div>
            </div>
            
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
<?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<script>
$(document).ready(function () {        
        applyFilter();
    });


$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        $('#load a').css('color', '#dfecf6');
        //$('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');

        var url = $(this).attr('href'); 
        var page=$(this).attr('href').split('page=')[1];
        //alert(page);
        var cat1=$("#cat1").val();
        var cat2=$("#cat2").val();
        var name=$("#name").val();
        
        
     
        filter(page,cat1,cat2,name);
        //window.history.pushState("", "", url);
    });

    
});

$( "#hide-filter" ).click(function() {
    $("#cat1").val('');
    $("#cat2").val('');
    $("#name").val('');
        
    filter(); 
});

function applyFilter(){ 
    
        var cat1=$("#cat1").val();
        var cat2=$("#cat2").val();
        var name=$("#name").val();
       
    
        filter(1,cat1,cat2,name);
}

function filter(page=1,cat1='',cat2='',name=''){ 

    $('.loading').show();
    
    $.ajax({
        type: 'GET', 
        url: '<?php echo e(route("filter-goods-return-closed")); ?>',
        data: {page:page,cat1:cat1,cat2:cat2,name:name},
        success:function(data)
        {
            $('#filterResult').html(data);
            $('.loading').hide();
        },
        error: function (error) 
        {
            
            alert('Error Occured Please Try Again');
        }
    })
}
</script>


<script type="text/javascript">
$(document).ready(function () {
            
            $('#example').dataTable({
                'order': [[ 0, "asc" ]],//Default Column Attribute
                'pageLength': 10,//Per Page Item
                 "searching": false,
                'dom': 'Bfrtip',//Header Info               
                'buttons': [ {extend: 'excel',exportOptions: {columns: [1,2,3,4,5,6,7,8,9,10,11]},footer: false}],
                'aoColumnDefs': [          
                                   { aTargets: [ 0 ], bSortable: false },
                                   { aTargets: [ 1 ], bSortable: true },
                                   { aTargets: [ 2 ], bSortable: false },
                                   { aTargets: [ 3 ], bSortable: false },
                                   { aTargets: [ 4 ], bSortable: false },
                                   { aTargets: [ 5], bSortable: true },
                                   { aTargets: [ 6 ], bSortable: false },
                                   { aTargets: [ 7 ], bSortable: false },
                                   { aTargets: [ 8 ], bSortable: false },
                                   { aTargets: [ 9 ], bSortable: false },
                                   { aTargets: [ 10 ], bSortable: false },
                                   { aTargets: [ 11 ], bSortable: false }
                                ],
                "oLanguage": {
                                "oPaginate": {
                                                "sNext": '<i class="fa fa-chevron-right" ></i>',
                                                "sPrevious": '<i class="fa fa-chevron-left" ></i>'
                                            }
                            }
            });
        });
     setTimeout(function(){
    //$('#hide-me').fadeout();
    $( "#hide-me" ).fadeOut( "slow", function() {
    // Animation complete.
    });
}, 2000);
</script>
<script type="text/javascript">
    function getVendor(val)
    {
        $.ajax({
            type: 'GET',
            url: '<?php echo e(route("get-goods-vendor")); ?>',
            data: {value:val},
            success:function(data)
            {
                $("#name").html(data['vendor']);
                getCat1(0);
            }
            /*error:function(error)
            {
                alert('Error Occured Please Try Again');
            }*/

        });

        
    }
    function getCat1(val)
    {

        $.ajax({
            type: 'GET',
            url: '<?php echo e(route("get-goods-cat1")); ?>',
            data: {value:val},
            success:function(data)
            {

                $("#cat1").html(data['cat1']);
                getCat2(0);
            }
            /*error:function(error)
            {
                alert('Error Occured Please Try Again');
            }*/

        });

        
    }
    function getCat2(val)
    {
        $.ajax({
            type: 'GET',
            url: '<?php echo e(route("get-goods-cat2")); ?>',
            data: {value:val},
            success:function(data)
            {
                $("#cat2").html(data['cat2']);
            }
            /*error:function(error)
            {
                alert('Error Occured Please Try Again');
            }*/

        });

        
    }

    //--For Clearing Filter Data Elements-----//
    $(".reset").bind("click", function() {
    $("#name").val("");
    $("#cat1").val("");
    $("#cat2").val("");

    });   
</script>

<!-- ********************************************** -->
<!-- JavaScript at the bottom for fast page loading -->
<!-- ********************************************** -->

<!-- Grab Google CDN's jQuery, fall back to local if offline -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php echo e(asset('fancybox/js/libs/jquery-1.7.1.min.js')); ?>"><\/script>')</script>
    
<!-- FancyBox -->
    <script src="<?php echo e(asset('fancybox/js/fancybox/jquery.fancybox.js')); ?>"></script>
    <script src="<?php echo e(asset('fancybox/js/fancybox/jquery.fancybox-buttons.js')); ?>"></script>
    <script src="<?php echo e(asset('fancybox/js/fancybox/jquery.fancybox-thumbs.js')); ?>"></script>
    <script src="<?php echo e(asset('fancybox/js/fancybox/jquery.easing-1.3.pack.js')); ?>"></script>
    <script src="<?php echo e(asset('fancybox/js/fancybox/jquery.mousewheel-3.0.6.pack.js')); ?>"></script>
    
    <script type="text/javascript">
        var nc=$.noConflict();
        nc(document).ready(function() {
        nc(".fancybox").fancybox();
        });
    </script>