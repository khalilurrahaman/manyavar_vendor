<?php 
use App\Http\Controllers\Controller;
if(Controller::userAccessibilityCheck('rfq-quotation-comment'))
	$rfq_quotation_comment_exist=1;
if(Controller::userAccessibilityCheck('rfq-request-notify'))
	$rfq_request_notify_exist=1;
if(Controller::userAccessibilityCheck('rfq-request-conclude'))
	$rfq_request_conclude_exist=1;
?>
  <div class="table-responsive view-table">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">REQ ID</th>
                                                <th class="text-center">Image</th>
                                                <th class="text-center">Design</th>
                                                <th class="text-center">Colour</th>
                                                <th class="text-center">Vendor Name</th>
                                                <th class="text-center">Remarks</th>
                                                <th class="text-center">Created At</th>
                                                <th class="text-center width-a">Quote ID</th>
                                                <th class="text-center width-b">Quote Date</th>
                                                <th class="text-center width-d">Rate</th>
                                                <th class="text-center width-d">Width</th>
                                               <!-- <th class="text-center">Status</th>-->
                                                <th class="text-center width-e">Last Comments</th>
                                                <!--<th class="text-center">Comments</th>-->
                                                <th class="text-center">Notify</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody id="filterResult">
                                    <?php $inc=0;?>
                                    <?php if(!empty($rfq_list) && count($rfq_list)>0): ?>
                                     <?php $old_val='';?>
                                     <?php $__currentLoopData = $rfq_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rfq): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                     <?php 
                                     $new_val=$rfq->id;
                                      $inc++;
                                     ?>
                                     <tr>
                                     	<?php if($new_val!=$old_val){?>
                                    	<td class="text-center" rowspan="<?php echo e($rfq->rfq_count); ?>"><?php echo e($rfq->req_id); ?></td>
                                      <td class="text-center" rowspan="<?php echo e($rfq->rfq_count); ?>">
                            <div class="gal"> 
                             <?php 
                               $main_image='';
                               $pGalList=DB::table('product_gallery')->select('*')->where('product_id','=',$rfq->product_id)->get();
                               if($rfq->image!='' && file_exists('upload/product/'.$rfq->image))
                               $main_image=URL::to('/upload/product').'/'.$rfq->image;
                               if(empty($main_image) && !empty($pGalList) && count($pGalList)>0)
                               {
                                  foreach ($pGalList as $pGal) 
                                  { 
                                    if($pGal->product_image!='' && file_exists('upload/product/'.$pGal->product_image))
                                    {
                                      $main_image=URL::to('/upload/product').'/'.$pGal->product_image;
                                    }
                                  }
                               }
                               if(empty($main_image))
                                $main_image=URL::to('/img').'/pre-order-img.jpg';
                              ?>
                               <a class="fancybox" rel="group<?php echo e($inc); ?>" href="<?php echo e($main_image); ?>" >
                                <img src="<?php echo e($main_image); ?>" style="width: 150px;" width="56">
                               </a>
                               <?php 
                               if(!empty($pGalList) && count($pGalList)>0)
                               {
                                foreach ($pGalList as $pGal2) 
                                {
                                    if($pGal2->product_image!='' && file_exists('upload/product/'.$pGal2->product_image))
                                    {
                                      $main_image=URL::to('/upload/product').'/'.$pGal2->product_image;

                                ?>                                               
                               <a class="fancybox" rel="group<?php echo e($inc); ?>" href="<?php echo e($main_image); ?>" style="display:none;"><img src="<?php echo e($main_image); ?>" alt=""></a>
                                <?php 
                                }
                                }
                                }
                               ?> 
                            </div>
                                        </td>
                                        <th class="text-center" rowspan="<?php echo e($rfq->rfq_count); ?>"><?php echo e($rfq->cat1); ?></th>
                                        <th class="text-center" rowspan="<?php echo e($rfq->rfq_count); ?>"><?php echo e($rfq->cat2); ?></th>
                                        <?php
										}
										?>
                                    	  <td class="text-center"><?php echo e($rfq->name); ?></td>
                                        <td class="text-center"><?php echo e($rfq->indv_remarks); ?></td>
                                        <td class="text-center"><?php echo e(date('d/m/Y',strtotime($rfq->created_at))); ?></td>
                                        <td class="text-center" colspan="5" style="padding:0;">
                                         
                                         <?php if(!empty($rfq->rfq_quetes) && count($rfq->rfq_quetes)>0): ?>
                                     	  
                                        <table class="table table-bordered table-striped" border="0" style="margin-bottom:0;"  cellspacing="0" width="100%">
                                            <tbody>

                                            <?php $old_ven='';?>
                                            <?php $__currentLoopData = $rfq->rfq_quetes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $quetes): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                              <?php $new_ven=$quetes->rfq_details_id;?>

                                             <tr>
                                                <td class="text-center width-a"><?php echo e($quetes->quete_id); ?></td>
                                                <td class="text-center width-b"><?php echo e(date('d/m/Y',strtotime($quetes->created_at))); ?></td>
                                                <td class="text-center width-d"><?php echo e($quetes->price); ?></td>
                                                <td class="text-center width-d"><?php if($quetes->width==0){echo 'NA';}else{echo $quetes->width.'"';}?></td>
                                                <!--<td class="text-center"><span class="btn btn-warning btn-xs">Open</span></td>-->

                                                 <?php if($old_ven!=$new_ven): ?>

                                                 <td class="text-left width-e " rowspan="<?php echo e(count($rfq->rfq_quetes)); ?>">
                                                 <?php if(!empty($rfq_quotation_comment_exist)){?>
                                                 <?php if(!empty($rfq->rfq_comments) && count($rfq->rfq_comments)>0): ?>
                                                <?php                                        
                                                if(!empty($rfq->rfq_comments) && count($rfq->rfq_comments)>0)
                                                {   
                                                  if(count($rfq->rfq_comments[0]->comments)<=20)                                 
                                                     echo ($rfq->rfq_comments[0]->comments);
                                                  else
                                                   echo substr($rfq->rfq_comments[0]->comments,0,18).'..';  

                                                }
                                                ?>
                                                <a href="<?php echo e(route('admin-comments',$quetes->rfq_details_id)); ?>" class=" pull-right btn btn-warning btn-xs"><?php echo count($rfq->rfq_comments)?> Comment(s)</a>
                                                 <?php else: ?>
                                                	<a href="<?php echo e(route('admin-comments',$quetes->rfq_details_id)); ?>" class="btn btn-warning btn-xs pull-right"> Comment</a>
                                                 <?php endif; ?>
                                                 <?php }?>
                                                 </td>

                                                 <?php endif; ?>
                                            </tr>
                                            <?php  $old_ven=$new_ven;?>

                                             <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                        </tbody>
                                       </table>
                               			
                                   <?php else: ?>
                                   No Quotes Available!
                                   <?php endif; ?>
                               
                               </td>
                                       <td class="text-center">
                                        <?php if(!empty($rfq_request_notify_exist)){?>
                                        <?php if(empty($rfq->rfq_quetes) && $rfq->notify==0): ?>
                                         <a id="notify_vendor<?php echo $rfq->id.'_'.$rfq->vendor_id; ?>" onclick="return notifyRfq('<?php echo e($rfq->id); ?>','<?php echo e($rfq->vendor_id); ?>');" class="btn btn-danger btn-xs">Notify</a>
                                         <?php elseif(empty($rfq->rfq_quetes) && $rfq->notify==1): ?>
                                         <a class="btn btn-danger btn-xs">Notified</a>
                                        <?php endif; ?>
                                         <?php }?>
                                        </td>  
                                       <?php if($new_val!=$old_val){?>
                                        <td class="text-center"  rowspan="<?php echo e($rfq->rfq_count); ?>">
                                         <?php if(!empty($rfq_request_conclude_exist)){?>
                                       <a href="<?php echo e(route('close-quotation', $rfq->id)); ?>" onclick="return closeRfqRequest('<?php echo e($rfq->req_id); ?>');" class="btn btn-success btn-xs">Conclude</a>&nbsp;
                                       <?php } ?>
                                       <?php }?>
                                        </td>
                                    </tr>
                                    <?php $old_val=$new_val;?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                   <?php else: ?>
                                    <tr>
                                    	<td class="text-center" colspan="13">No Rfq Available!</td>
                                    </tr>
                                   <?php endif; ?>
                                </tbody>
                               </table>
                              </div>
                              <div class="row">
                              <div class="col-sm-6">
                              	<div class="dataTables_info" role="status" aria-live="polite">Showing <?php echo e($rfq_list->count()); ?> of <?php echo e($rfq_list->total()); ?> entries</div>
                              </div>
                              <div class="col-sm-6">
                                    <nav aria-label="Page navigation" class="pull-right">
                                       <?php echo e($rfq_list->links()); ?> 
                                    </nav>
                                  </div>
                              </div>
<!-- /#Hover Image -->
    <script type="text/javascript">$('img').on('mouseover', function() {
    var self = this,
        i = 0,
        images = $(this).data('mouseover').split(/\s+/);
    
    (function nextImage() {
        var next = images[i++ % images.length].split('#');
        $(self).data('timeout', setTimeout(function() {
            self.src = next[0];
            nextImage();
        }, next[1]));
    })();
    
}).on('mouseout', function() {
    clearTimeout($(this).data('timeout'));
    this.src = $(this).attr('src');
});
</script>