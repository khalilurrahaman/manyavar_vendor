<!--<h5>Vendor List Filter</h5>-->
        <div class="col-sm-2">
            <select class="select-drop" id="selectcat2" name="selectcat2">
            	<option value="">Select Colour</option>
                <?php if(!empty($cat2_list) && count($cat2_list)>0): ?>
                <?php $__currentLoopData = $cat2_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat2): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                	<option value="<?php echo e($cat2->cat2); ?>"><?php echo e($cat2->cat2); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                <?php endif; ?>
            </select>
        </div>
        <div class="col-sm-2">
            <select class="select-drop" id="selectcat3" name="selectcat3">
            	<option value="">Select Cat3</option>
                <?php if(!empty($cat3_list) && count($cat3_list)>0): ?>
                <?php $__currentLoopData = $cat3_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat3): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                	<option value="<?php echo e($cat3->cat3); ?>"><?php echo e($cat3->cat3); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                <?php endif; ?>
            </select>
        </div>
        <div class="col-sm-2">
            <select class="select-drop" id="selectcat4" name="selectcat4">
            	<option value="">Select Width/Size</option>
                <?php if(!empty($cat4_list) && count($cat4_list)>0): ?>
                <?php $__currentLoopData = $cat4_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat4): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                	<option value="<?php echo e($cat4->cat4); ?>"><?php echo e($cat4->cat4); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                <?php endif; ?>
            </select>
        </div>
        
        <div class="col-sm-2">
        	<a href="javascript:void(0)" class="btn btn-success selectSearchLive">Apply</a>
        </div>
        <script>
		$('.selectSearchLive').click(function(){
	     var selectcat1 = $('#search_exist_main').val();
		 var selectcat2 = $('#selectcat2').val();
		 var selectcat3 = $('#selectcat3').val();
		 var selectcat4 = $('#selectcat4').val();
		filterLive(selectcat1,selectcat2,selectcat3,selectcat4) ;
		});
		</script>
