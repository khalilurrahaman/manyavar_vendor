<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use DB;
use App\Models\GIT;

class markCompleteGit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:markCompleteGit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mark as Complete Git';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //$close_git=DB::table('git')->where('git.git_qty', '=','git.git_rcv')->get();
        $close_git=DB::table('git')->where('git_qty', '=', DB::raw('git_rcv'))->get();
        //dd($close_git);
        if($close_git){
            foreach ($close_git as $each_git) 
            {
                //dd($each_git->ORDCODE);
                $poStatus = DB::table('V_PURORDDET')->where('V_PURORDDET.ORDCODE', '=',$each_git->ORDCODE)->where('V_PURORDDET.ICODE', '=',$each_git->ICODE)->update(['STATUS'=>'CLOSED','updated_at'=>date('Y-m-d H:i:s')]);

                $isexist=DB::table('V_PURORDDET')->selectRaw('count(*) as num')->whereRaw('V_PURORDDET.ORDCODE = "'.$each_git->ORDCODE.'" AND V_PURORDDET.STATUS ="OPEN"')->groupBy('V_PURORDDET.ORDCODE')->first();

                if(!empty($isexist))
                {
                    $poStatus = DB::table('purchase_order')->where('purchase_order.ORDCODE', '=',$each_git->ORDCODE)->update(['status'=>'CLOSED','updated_at'=>date('Y-m-d H:i:s')]);
                }
                if(DB::table('git')->where('git.ORDCODE', '=',$each_git->ORDCODE)->where('git.ICODE', '=',$each_git->ICODE)->update(['status'=>'Close','updated_at'=>date('Y-m-d H:i:s')]))
                {
                    
                    $gitDtls = DB::table('git')->selectRaw('users.id,purchase_order.PONO')
                    ->leftJoin('V_PURORDDET', 'V_PURORDDET.id', '=', 'git.pd_id')
                    ->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
                    ->join('users', 'purchase_order.PCODE', '=', 'users.SLCODE')
                    ->where('git.id', '!=',$each_git->id)
                    ->first();              
                
                    $notify_array['message'] = 'Git For PO:'.$gitDtls->PONO.' has been Mark Completed';
                    $notify_array['type'] = 'RFQRequestConclude';
                    $notify_array['red_url'] = 'all-git-list';
                    $notify_array['receiver'] = $gitDtls->id;;
                    $notify_array['status'] = 'NEW';
                    $notify_array['table_name'] = 'git';
                    $notify_array['primary_key'] = 'id';
                    $notify_array['primary_key_val'] =$each_git->id;
                    $notify_array['created_at'] =date('Y-m-d H:i:s');   
                    DB::table('notification')->insert($notify_array);
                
                
                }
            }
        }

        echo 'markCompleteGit success';exit;
    }
}