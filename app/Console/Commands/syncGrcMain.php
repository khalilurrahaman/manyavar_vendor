<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use DB;

class syncGrcMain extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:syncGrcMain';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Grc Main Order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $three_month = date("d-M-Y",strtotime("-1 Months"));
        $vendor_arr = DB::table('users')->select('SLCODE')->where('status','=',1)->where('type','=','vendor')->get();
        
        $vendor_PCODE='';
        foreach($vendor_arr as $eachvendor_PCODE)
        {
            $vendor_PCODE .= "'".$eachvendor_PCODE->SLCODE."',";
        }

        $vendor_PCODE1 = rtrim($vendor_PCODE,',');
      
       
        $query_str="SELECT * from v_invgrcmain  where PCODE IN (".$vendor_PCODE1.") AND GRCDT>='".$three_month."'";
        
        $grclist=  User::oracleQuery($query_str);
        foreach ($grclist as $row) 
        {

            $isexist=$this->checkGrcDuplicate($row['GRCCODE']);
            if($isexist==0)
            {   
                $row['GRCDT']=(trim($row['GRCDT']))?date('Y-m-d',strtotime(trim($row['GRCDT']))):'';
                $row['TIME']=(trim($row['TIME']))?date('Y-m-d',strtotime(trim($row['TIME']))):'';
                $row['LAST_ACCESS_TIME']=(trim($row['LAST_ACCESS_TIME']))?date('Y-m-d',strtotime(trim($row['LAST_ACCESS_TIME']))):'';
                $row['created_at']=date('Y-m-d H:i:s');
                $row['updated_at']=date('Y-m-d H:i:s');
                //unset($row['RN']);             
                DB::table('v_invgrcmain')->insert($row); 
            }
            else
            {

                $row['GRCDT']=(trim($row['GRCDT']))?date('Y-m-d',strtotime(trim($row['GRCDT']))):'';
                $row['TIME']=(trim($row['TIME']))?date('Y-m-d',strtotime(trim($row['TIME']))):'';
                $row['LAST_ACCESS_TIME']=(trim($row['LAST_ACCESS_TIME']))?date('Y-m-d',strtotime(trim($row['LAST_ACCESS_TIME']))):'';
                $row['updated_at']=date('Y-m-d H:i:s');             
                DB::table('v_invgrcmain')->where('GRCCODE','=',$row['GRCCODE'])->update($row);
            }

        }

         echo 'success';exit;
         
    }

    public function checkGrcDuplicate($GRCCODE)
    {
        $order=DB::table('v_invgrcmain')->selectRaw('count(*) as num')->where('GRCCODE', '=',$GRCCODE)->first();
        return $order->num;
    }
}
