<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use DB;

class VendorSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:VendorSync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Vendor With Details';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $query_str="SELECT *  from V_FINSL WHERE SLCODE IN('1001262018','1001283428','1001251905','1000175886','1000000591','1001262272','1001280857','1001264610','1001263837','1001265293','1000976942','1001279341','1001281390','1001282014','1001278892','1001268317','1001270031','1001272540','1000000592','1000000885','1001281525','1001283765','1001283901')" ;//Keva Creation,P.C.Fabrics,Brilliant Packaging Pvt. Ltd. ,Arihant Agencies,Arihant - Traders,Premchand Pannachand,Hoshiyar Singh,Choubey Overseas Corporation, Rahul Traders
        $v_List=  User::oracleQuery($query_str);
        //echo '<pre>'; print_r($v_List);exit;

        //$list = array_chunk($v_List, 100);
        foreach ($v_List as $row) 
        {            

            $isexist=DB::table('users')->selectRaw('count(*) as num')->where('SLCODE','=',$row['SLCODE'])->first();
            if($isexist->num == 0)
            {
                $vendor_row=array();
                $vendor_row['type']         =   'vendor';
                $vendor_row['SLCODE']       =   trim($row['SLCODE']);
                $vendor_row['username']     =   trim($row['BEMAIL']);
                $vendor_row['email']        =   trim($row['BEMAIL']);
                $vendor_row['CSTNO']        =   trim($row['CSTNO']);
                $vendor_row['LSTNO']        =   trim($row['LSTNO']);
                $vendor_row['name']         =   trim($row['SLNAME']);
                $vendor_row['address']      =   trim($row['BADDR']);
                $vendor_row['mobile']       =   trim($row['BPH1']);
                $vendor_row['status']       =   1;
                $vendor_row['created_at']   =   date('Y-m-d H:i:s');
                $vendor_row['updated_at']   =   date('Y-m-d H:i:s');

                DB::table('users')->insert($vendor_row);                
            }
            else
            {
                $vendor_row=array();
                #$vendor_row['username']     =   trim($row['BEMAIL']);
                #$vendor_row['email']        =   trim($row['BEMAIL']);
                $vendor_row['CSTNO']        =   trim($row['CSTNO']);
                $vendor_row['LSTNO']        =   trim($row['LSTNO']);
                $vendor_row['name']         =   trim($row['SLNAME']);
                $vendor_row['address']      =   trim($row['BADDR']);
                $vendor_row['mobile']       =   trim($row['BPH1']);
                $vendor_row['updated_at']   =   date('Y-m-d H:i:s');

                DB::table('users')->where('SLCODE','=',$row['SLCODE'])->update($vendor_row);        
            }

        }

        echo 'success';exit;
    }
}
