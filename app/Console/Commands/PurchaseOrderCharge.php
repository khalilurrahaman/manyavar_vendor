<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use DB;

class PurchaseOrderCharge extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:PurchaseOrderCharge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Purchase Order Charge';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //$three_month = date("d-M-Y",strtotime("-1 Months"));
        $three_month = "01-Apr-2016";
        $vendor_arr = DB::table('users')->select('SLCODE')->where('status','=',1)->where('type','=','vendor')->get();
        $vendor_PCODE='';
        foreach($vendor_arr as $eachvendor_PCODE)
        {
            $vendor_PCODE .= "'".$eachvendor_PCODE->SLCODE."',";
        }

        $vendor_PCODE1 = rtrim($vendor_PCODE,',');
        //echo "<pre>";print_r($vendor_PCODE1); exit;
        $query_str="SELECT * from V_PURORDMAIN  where PCODE IN (".$vendor_PCODE1.") AND ORDDT>='".$three_month."'";
        $p_order=  User::oracleQuery($query_str);
        //echo "<pre>";print_r($p_order);  

        //$list = array_chunk($v_List, 100);
       # $abc =0;
        foreach ($p_order as $row) 
        {            
            $ORDCODE = $row['ORDCODE'];
            #echo $ORDCODE;
            //$isexist = $this->checkDuplicateOrder($row['ORDCODE']);
            $isexist=DB::table('v_purordchg')->selectRaw('count(*) as num')->where('ORDCODE','=',$ORDCODE)->first();
            $purord_chg="SELECT * from v_purordchg where ORDCODE = $ORDCODE";
            $purordchg=  User::oracleQuery($purord_chg);
            //echo "<pre>";print_r($isexist);
            if(count($purordchg)>0)
            {
                
                if(($isexist->num == 0) && !empty($purordchg[0]))
                {
                    $purordchg_row=array();
                    $purordchg_row['ORDCODE']  =   trim($purordchg[0]['ORDCODE']);
                    $purordchg_row['CHGCODE']  =   trim($purordchg[0]['CHGCODE']);
                    $purordchg_row['RATE']         =   trim($purordchg[0]['RATE']);
                    $purordchg_row['CODE']      =   trim($purordchg[0]['CODE']);
                    $purordchg_row['SOURCE']       =   trim($purordchg[0]['SOURCE']);
                    $purordchg_row['ISTAX']       =   trim($purordchg[0]['ISTAX']);
                    $purordchg_row['created_at']   =   date('Y-m-d H:i:s');
                    $purordchg_row['updated_at']   =   date('Y-m-d H:i:s');

                    DB::table('v_purordchg')->insert($purordchg_row);
                    $CHGCODE = $purordchg[0]['CHGCODE'];

                    $finchg="SELECT * from v_finchg where CHGCODE = $CHGCODE";
                    $fin_chg=  User::oracleQuery($finchg);
						if(!empty($fin_chg[0]))
						{
								$v_finchg = DB::table('V_Fincharge')->where('CHGCODE','=',$CHGCODE)->first();
								if(empty($v_finchg))
								{
									
									$finchg_row=array();
									$finchg_row['CHGCODE'] =   trim($fin_chg[0]['CHGCODE']);
									$finchg_row['CHGNAME'] =   trim($fin_chg[0]['CHGNAME']);
									$finchg_row['GLCODE']  =   trim($fin_chg[0]['GLCODE']);
									$finchg_row['RATE']    =   trim($fin_chg[0]['RATE']);
									$finchg_row['created_at'] =   date('Y-m-d H:i:s');
									$finchg_row['updated_at'] =   date('Y-m-d H:i:s');
		
									DB::table('V_Fincharge')->insert($finchg_row);
								}
								else
								{
									$finchg_row1=array();
									$finchg_row1['CHGCODE'] =   trim($fin_chg[0]['CHGCODE']);
									$finchg_row1['CHGNAME'] =   trim($fin_chg[0]['CHGNAME']);
									$finchg_row1['GLCODE']  =   trim($fin_chg[0]['GLCODE']);
									$finchg_row1['RATE']    =   trim($fin_chg[0]['RATE']);
									$finchg_row1['updated_at'] =   date('Y-m-d H:i:s');
		
									DB::table('V_Fincharge')->where('CHGCODE','=',$CHGCODE)->update($finchg_row1);
								}
						}
                }   
                else if(!empty($purordchg[0]))
				{
                    $purordchg_row1=array();
					$purordchg_row1['ORDCODE']  =   trim($purordchg[0]['ORDCODE']);
					$purordchg_row1['CHGCODE']  =   trim($purordchg[0]['CHGCODE']);
					$purordchg_row1['RATE']     =   trim($purordchg[0]['RATE']);
					$purordchg_row1['CODE']     =   trim($purordchg[0]['CODE']);
					$purordchg_row1['SOURCE']   =   trim($purordchg[0]['SOURCE']);
					$purordchg_row1['ISTAX']    =   trim($purordchg[0]['ISTAX']);
					$purordchg_row1['updated_at'] =   date('Y-m-d H:i:s');

					DB::table('v_purordchg')->where('ORDCODE','=',$ORDCODE)->update($purordchg_row1);
					$CHGCODE = $purordchg[0]['CHGCODE'];

					$finchg="SELECT * from v_finchg where CHGCODE = $CHGCODE";
					$fin_chg=  User::oracleQuery($finchg);
					if(!empty($fin_chg[0])){

						 $v_finchg = DB::table('V_Fincharge')->where('CHGCODE','=',$CHGCODE)->first();
						if(empty($v_finchg))
						{
							
							$finchg_row=array();
							$finchg_row['CHGCODE']  =   trim($fin_chg[0]['CHGCODE']);
							$finchg_row['CHGNAME']  =   trim($fin_chg[0]['CHGNAME']);
							$finchg_row['GLCODE']   =   trim($fin_chg[0]['GLCODE']);
							$finchg_row['RATE']     =   trim($fin_chg[0]['RATE']);
							$finchg_row['created_at'] =   date('Y-m-d H:i:s');
							$finchg_row['updated_at'] =   date('Y-m-d H:i:s');

							DB::table('V_Fincharge')->insert($finchg_row);
						}
						else{
							$finchg_row1=array();
							$finchg_row1['CHGCODE']  =   trim($fin_chg[0]['CHGCODE']);
							$finchg_row1['CHGNAME']  =   trim($fin_chg[0]['CHGNAME']);
							$finchg_row1['GLCODE']   =   trim($fin_chg[0]['GLCODE']);
							$finchg_row1['RATE']     =   trim($fin_chg[0]['RATE']);
							$finchg_row1['updated_at'] =   date('Y-m-d H:i:s');

							DB::table('V_Fincharge')->where('CHGCODE','=',$CHGCODE)->update($finchg_row1);
						}
					}
                }
            }
        }
		
	echo 'success';exit;
    }
}
