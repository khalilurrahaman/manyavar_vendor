<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use DB;

class PurchaseOrderSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:PurchaseOrderSync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Purchase Order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $three_month = date("d-M-Y",strtotime("-1 Months"));//24-Oct-2016
		#echo $three_month; exit;
        $vendor_arr = DB::table('users')->select('SLCODE')->where('status','=',1)->where('type','=','vendor')->get();
        $vendor_PCODE='';
        foreach($vendor_arr as $eachvendor_PCODE)
        {
            $vendor_PCODE .= "'".$eachvendor_PCODE->SLCODE."',";
        }

        $vendor_PCODE1 = rtrim($vendor_PCODE,',');
        //echo "<pre>";print_r($vendor_PCODE1); exit;
        $query_str="SELECT * from V_PURORDMAIN  where PCODE IN (".$vendor_PCODE1.") AND ORDDT>='".$three_month."'";
		#$query_str="SELECT * from V_PURORDMAIN  where PCODE IN (".$vendor_PCODE1.") AND ORDDT>='".$three_month."'";
		
        $p_order=  User::oracleQuery($query_str);
        //echo "<pre>";print_r($p_order); exit; 
        foreach ($p_order as $row) 
        {  

            $isexist=$this->checkOrderDuplicate($row['ORDCODE']);

            if($isexist==0)  //No Duplicate found in purchase_order Table
            {
                //dd("OrderDuplicate not exists");exit;
                $row['ORDDT']=(trim($row['ORDDT']))?date('Y-m-d',strtotime(trim($row['ORDDT']))):'';
                $row['TIME']=(trim($row['TIME']))?date('Y-m-d',strtotime(trim($row['TIME']))):'';
                $row['LAST_ACCESS_TIME']=(trim($row['LAST_ACCESS_TIME']))?date('Y-m-d',strtotime(trim($row['LAST_ACCESS_TIME']))):'';
                $row['created_at']=date('Y-m-d H:i:s');
                $row['updated_at']=date('Y-m-d H:i:s');
                $orderCode = $row['ORDCODE'];
                //$orderNo = $row['ORDNO'];
                $row['PONO'] =$row['SCHEME_DOCNO'];// $this->generatePOno($row['ORDNO']);

                DB::table('purchase_order')->insert($row);//insert into Purchase Order Table

                $PURORDDET="SELECT * from V_PURORDDET where ORDCODE = $orderCode";
                $p_order_det_item1=  User::oracleQuery($PURORDDET);
                //echo "NEW"; echo "<pre>";print_r($p_order_det_item1);
                foreach ($p_order_det_item1 as $row1) {
                    $isexist1=$this->checkOrderDetailsDuplicate($row1['ORDCODE'],$row1['ICODE']);
                    if($isexist1==0) // No Duplicate found in V_PURORDDET Table
                    {
                        //dd("OrderDetails Not Exists");exit;
                        $row1['created_at']=date('Y-m-d H:i:s');
                        $row1['updated_at']=date('Y-m-d H:i:s');
                        $PURORDDET_CODE = $row1['CODE'];

                        DB::table('V_PURORDDET')->insert($row1); //insert into V_PURORDDET Table

                        $PURORDDET_DELIVERY="SELECT * from V_PURORDDET_DELIVERY where PURORDDET_CODE = $PURORDDET_CODE";
                        $p_order_det_delvery=  User::oracleQuery($PURORDDET_DELIVERY);
                        foreach ($p_order_det_delvery as $row2) {
                            $isexist2 = $this->checkOrderDetailsDeliveryDuplicate($row1['CODE']);
                            if($isexist2 == 0)  // No Duplicate found in purchase_order_delivery Table
                            { 
                                $row2['DUEDATE']=(trim($row2['DUEDATE']))?date('Y-m-d',strtotime(trim($row2['DUEDATE']))):'';
                                $row2['created_at']=date('Y-m-d H:i:s');
                                $row2['updated_at']=date('Y-m-d H:i:s');
                                DB::table('purchase_order_delivery')->insert($row2);   
                            }
                            else{
                                $updateDelivery=array();
                                $updateDelivery['CODE']           =   $row2['CODE'];
                                $updateDelivery['PURORDDET_CODE'] =   $row2['PURORDDET_CODE'];
                                $updateDelivery['DUEDATE']        =   $row2['DUEDATE']=(trim($row2['DUEDATE']))?date('Y-m-d',strtotime(trim($row2['DUEDATE']))):'';
                                $updateDelivery['updated_at']     =   date('Y-m-d H:i:s');

                                DB::table('purchase_order_delivery')->where('PURORDDET_CODE','=',$row2['PURORDDET_CODE'])->update($updateDelivery);
                            }
                        }
                    }
                    else
                    {
                        $update=array();
                        $update['ORDQTY']         =   $row1['ORDQTY'];
                        $update['CNLQTY']         =   $row1['CNLQTY'];
                        $update['RATE']           =   $row1['RATE'];
                        $update['RCQTY']          =   $row1['RCQTY'];
                        $update['OQTY']           =   $row1['OQTY'];
                        $update['REM']            =   $row1['REM'];
                        $update['CODE']           =   $row1['CODE'];
                        $update['DISCOUNT']       =   $row1['DISCOUNT'];						
                        $update['updated_at']     =   date('Y-m-d H:i:s');

                        DB::table('V_PURORDDET')->where('ORDCODE','=',$row['ORDCODE'])->where('ICODE', '=',$row1['ICODE'])->update($update);

                        $PURORDDET_DELIVERY="SELECT * from V_PURORDDET_DELIVERY where PURORDDET_CODE = $PURORDDET_CODE";
                        $p_order_det_delvery=  User::oracleQuery($PURORDDET_DELIVERY);
                        foreach ($p_order_det_delvery as $row2) {
                            $isexist2 = $this->checkOrderDetailsDeliveryDuplicate($row1['CODE']);
                            if($isexist2 == 0)  // No Duplicate found in purchase_order_delivery Table
                            { 
                                $row2['DUEDATE']=(trim($row2['DUEDATE']))?date('Y-m-d',strtotime(trim($row2['DUEDATE']))):'';
                                $row2['created_at']=date('Y-m-d H:i:s');
                                $row2['updated_at']=date('Y-m-d H:i:s');
                                DB::table('purchase_order_delivery')->insert($row2);   
                            }
                            else{
                                $updateDelivery=array();
                                $updateDelivery['CODE']           =   $row2['CODE'];
                                $updateDelivery['PURORDDET_CODE'] =   $row2['PURORDDET_CODE'];
                                $updateDelivery['DUEDATE']        =   $row2['DUEDATE']=(trim($row2['DUEDATE']))?date('Y-m-d',strtotime(trim($row2['DUEDATE']))):'';
                                $updateDelivery['updated_at']     =   date('Y-m-d H:i:s');

                                DB::table('purchase_order_delivery')->where('PURORDDET_CODE','=',$row2['PURORDDET_CODE'])->update($updateDelivery);
                            }
                        }
                    }
                }
            }
            else
            {
                //dd("purchase order exist");exit;
                $update_arr=array();

                $update_arr['DOCNO']                =   $row['DOCNO'];
                $update_arr['STAT']                 =   $row['STAT'];
                $update_arr['REM']                  =   $row['REM'];
                $update_arr['TIME']                 =   $row['TIME'];
                $update_arr['YCODE']                =   $row['YCODE'];
                $update_arr['LAST_ACCESS_TIME']     =   $row['LAST_ACCESS_TIME'];
                $update_arr['SHIPSTATUS']           =   $row['SHIPSTATUS'];
                $update_arr['DESCCMPCODE']          =   $row['DESCCMPCODE'];
                $update_arr['ORDCMPCODE']           =   $row['ORDCMPCODE'];
                $update_arr['CREATORCMPCODE']       =   $row['CREATORCMPCODE'];
                $update_arr['EXRATE']               =   $row['EXRATE'];
                $update_arr['PONO']        		 =   $row['SCHEME_DOCNO'];
                $update_arr['GRSAMT']               =   $row['GRSAMT'];
                $update_arr['NETAMT']               =   $row['NETAMT'];
				$update_arr['CHGAMT']   		       =   $row['CHGAMT'];
                $update_arr['updated_at']           =   date('Y-m-d H:i:s');


                DB::table('purchase_order')->where('ORDCODE','=',$row['ORDCODE'])->update($update_arr);
                $orderCode = $row['ORDCODE'];
                $PURORDDET="SELECT * from V_PURORDDET where ORDCODE = $orderCode";

                $p_order_det_item2=  User::oracleQuery($PURORDDET);
                //echo "OLD"; echo "<pre>";print_r($p_order_det_item2);
                foreach ($p_order_det_item2 as $row1) 
                {
                    $isexist1=$this->checkOrderDetailsDuplicate($row1['ORDCODE'],$row1['ICODE']);
                    if($isexist1==0) // No Duplicate found in V_PURORDDET Table
                    {
                        //dd("OrderDetails not exists"); exit;
                        $row1['created_at']=date('Y-m-d H:i:s');
                        $row1['updated_at']=date('Y-m-d H:i:s');
                        $PURORDDET_CODE = $row1['CODE'];

                        DB::table('V_PURORDDET')->insert($row1); //insert into V_PURORDDET Table

                        $PURORDDET_DELIVERY="SELECT * from V_PURORDDET_DELIVERY where PURORDDET_CODE = $PURORDDET_CODE";
                        $p_order_det_delvery=  User::oracleQuery($PURORDDET_DELIVERY);
                        foreach ($p_order_det_delvery as $row2) {
                            $isexist2 = $this->checkOrderDetailsDeliveryDuplicate($row1['CODE']);
                            if($isexist2 == 0)  // No Duplicate found in purchase_order_delivery Table
                            { 
                                $row2['DUEDATE']=(trim($row2['DUEDATE']))?date('Y-m-d',strtotime(trim($row2['DUEDATE']))):'';
                                $row2['created_at']=date('Y-m-d H:i:s');
                                $row2['updated_at']=date('Y-m-d H:i:s');
                                DB::table('purchase_order_delivery')->insert($row2);   
                            }
                            else{
                                $updateDelivery=array();
                                $updateDelivery['CODE']           =   $row2['CODE'];
                                $updateDelivery['PURORDDET_CODE'] =   $row2['PURORDDET_CODE'];
                                $updateDelivery['DUEDATE']        =   $row2['DUEDATE']=(trim($row2['DUEDATE']))?date('Y-m-d',strtotime(trim($row2['DUEDATE']))):'';
                                $updateDelivery['updated_at']     =   date('Y-m-d H:i:s');

                                DB::table('purchase_order_delivery')->where('PURORDDET_CODE','=',$row2['PURORDDET_CODE'])->update($updateDelivery);
                            }
                        }
                    }
                    else
                    {
                        //dd("OrderDetails exists"); exit;
                        $update=array();
                        $update['ORDQTY']         =   $row1['ORDQTY'];
                        $update['CNLQTY']         =   $row1['CNLQTY'];
                        $update['RATE']           =   $row1['RATE'];
                        $update['RCQTY']          =   $row1['RCQTY'];
                        $update['OQTY']           =   $row1['OQTY'];
                        $update['REM']            =   $row1['REM'];
                        $update['CODE']           =   $row1['CODE'];
                        $update['DISCOUNT']       =   $row1['DISCOUNT'];
                        $update['updated_at']     =   date('Y-m-d H:i:s');

                        DB::table('V_PURORDDET')->where('ORDCODE','=',$row['ORDCODE'])->where('ICODE', '=',$row1['ICODE'])->update($update);
                        $PURORDDET_CODE = $row1['CODE'];
                        $PURORDDET_DELIVERY="SELECT * from V_PURORDDET_DELIVERY where PURORDDET_CODE = $PURORDDET_CODE";
                        $p_order_det_delvery=  User::oracleQuery($PURORDDET_DELIVERY);
                        foreach ($p_order_det_delvery as $row2) {
                            $isexist2 = $this->checkOrderDetailsDeliveryDuplicate($row1['CODE']);
                            if($isexist2 == 0)  // No Duplicate found in purchase_order_delivery Table
                            { 
                                //dd("DetailsDelivery not exists"); exit;
                                $row2['DUEDATE']=(trim($row2['DUEDATE']))?date('Y-m-d',strtotime(trim($row2['DUEDATE']))):'';
                                $row2['created_at']=date('Y-m-d H:i:s');
                                $row2['updated_at']=date('Y-m-d H:i:s');
                                
                                DB::table('purchase_order_delivery')->insert($row2);   
                            }
                            else{
                                //dd("DetailsDelivery exists"); exit;
                                $updateDelivery=array();
                                $updateDelivery['CODE']           =   $row2['CODE'];
                                $updateDelivery['PURORDDET_CODE'] =   $row2['PURORDDET_CODE'];
                                $updateDelivery['DUEDATE']        =   $row2['DUEDATE']=(trim($row2['DUEDATE']))?date('Y-m-d',strtotime(trim($row2['DUEDATE']))):'';
                                $updateDelivery['updated_at']     =   date('Y-m-d H:i:s');

                                DB::table('purchase_order_delivery')->where('PURORDDET_CODE','=',$row2['PURORDDET_CODE'])->update($updateDelivery);
                            }

                        }
                    }
                }
            }//echo "<pre>";print_r($row2);
        }

        echo 'success';exit;
         
    }

    public function checkOrderDuplicate($ORDCODE)
    {
        $order=DB::table('purchase_order')->selectRaw('count(*) as num')->where('ORDCODE', '=',$ORDCODE)->first();

        return $order->num;
    }

    public function checkOrderDetailsDuplicate($ORDCODE,$ICODE)
    {
        $order=DB::table('V_PURORDDET')->selectRaw('count(*) as num')->where('ORDCODE', '=',$ORDCODE)->where('ICODE', '=',$ICODE)->first();
        return $order->num;
    }

    public function checkOrderDetailsDeliveryDuplicate($PURORDDET_CODE)
    {
        $order=DB::table('purchase_order_delivery')->selectRaw('count(*) as num')->where('PURORDDET_CODE', '=',$PURORDDET_CODE)->first();

        return $order->num;
    }

    static function generatePOno($orderNo)
    {   
        $po_no=str_pad($orderNo, 6, '0', STR_PAD_LEFT);
        $po_id = 'PO/'.$po_no.'/'.date("y",strtotime("-1 year")).'-'.date("y");
        return $po_id;
    }
}
