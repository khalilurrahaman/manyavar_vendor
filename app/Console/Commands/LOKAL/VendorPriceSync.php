<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use DB;

class VendorPriceSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:VendorPriceSync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Price With Details';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $three_month = date("d-M-Y",strtotime("-1 Months"));
        $vendor_arr = DB::table('users')->select('SLCODE')->where('status','=',1)->where('type','=','vendor')->get();
        $vendor_PCODE='';
        foreach($vendor_arr as $eachvendor_PCODE)
        {
            $vendor_PCODE .= "'".$eachvendor_PCODE->SLCODE."',";
        }

        $vendor_PCODE1 = rtrim($vendor_PCODE,',');
        //echo "<pre>";print_r($vendor_PCODE1); exit;
        $query_str="SELECT * from V_PUR_PRICE_CHART  where PCODE IN (".$vendor_PCODE1.") AND EFFECTIVE_DATE>='".$three_month."'";
        $price_list=  User::oracleQuery($query_str);
            foreach ($price_list as $row) {
            if(!empty($row)){
                $isexist=DB::table('prices')->selectRaw('count(*) as num')->where('ICODE','=',$row['ICODE'])->where('PCODE','=',$row['PCODE'])->first();
                if($isexist==0){

                    $row['CODE'] = trim($row['CODE']);
                    $row['PCODE'] = trim($row['PCODE']);
                    $row['ICODE'] = trim($row['ICODE']);
                    $EFFECTIVE_DATE = strtotime(trim($row['EFFECTIVE_DATE']));
                    $row['EFFECTIVE_DATE'] = date('Y-m-d',$EFFECTIVE_DATE);
                    $row['RATE'] = trim($row['RATE']);
                    $row['DELIVERY_DAYS'] = trim($row['DELIVERY_DAYS']);
                    $row['EXTINCT'] = trim($row['EXTINCT']);
                    $row['ECODE'] = trim($row['ECODE']);
                    $row['TIME'] = date('Y-m-d',strtotime(trim($row['TIME'])));
                    $row['created_at'] =   date('Y-m-d H:i:s');
                    $row['updated_at'] =   date('Y-m-d H:i:s');
                    
                    DB::table('prices')->insert($row);
                }else{

                    $update_arr=array();
                    $update_arr['CODE'] = trim($row['CODE']);
                    $EFFECTIVE_DATE = strtotime(trim($row['EFFECTIVE_DATE']));
                    $update_arr['EFFECTIVE_DATE'] = date('Y-m-d',$EFFECTIVE_DATE);
                    $update_arr['RATE'] = trim($row['RATE']);
                    $update_arr['DELIVERY_DAYS'] = trim($row['DELIVERY_DAYS']);
                    $update_arr['EXTINCT'] = trim($row['EXTINCT']);
                    $update_arr['ECODE'] = trim($row['ECODE']);
                    $update_arr['TIME'] = date('Y-m-d',strtotime(trim($row['TIME'])));
                    $update_arr['updated_at']    =   date('Y-m-d H:i:s');


                    DB::table('prices')->where('PCODE','=',$row['PCODE'])->where('ICODE','=',$row['ICODE'])->update($update_arr);
                }
            }
        }
        echo "success";exit;
    }
}
