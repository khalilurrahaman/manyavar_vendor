<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use DB;

class VendorItemSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:VendorItemSync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Item With Details';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $query_str="SELECT * FROM (select ICODE, GRPCODE, DIVISION, SECTION, DEPARTMENT, CCODE1, CCODE2, CCODE3, CCODE4, CCODE5, CCODE6, CNAME1, CNAME2, CNAME3, CNAME4, CNAME5, CNAME6, RATE, MRP, BARRQ, BARUNIT, REM, PARTYCODE, PARTYNAME, GENERATED, LAST_CHANGED, EXPIRY_DATE, GRCCODE, LISTED_MRP, ITEM_NAME, DESC1, DESC2, DESC3, DESC4, DESC5, DESC6, NUM1, NUM2, NUM3, SEARCH_STRING, rownum rn FROM V_INVITEM where rownum<=85354) where rn BETWEEN 0 AND 10000";
        //$query_str="SELECT count(*) from V_INVITEM";
        $v_invitem=  User::oracleQuery($query_str);
        //dd($v_invitem);
        $list = array_chunk($v_invitem, 100);

        for($i=0; $i <count($list); $i++) {
            foreach ($list[$i] as $row) {
                echo "<pre>";print_r($row);
                if(!empty($row)){
                    $isexist=DB::table('V_INVITEM')->selectRaw('count(*) as num')->where('ICODE','=',$row['ICODE'])->first();
                    if($isexist==0){
                        /*$row['cat1'] = (trim($row['CNAME1']));
                        $row['cat2'] = (trim($row['CNAME2']));
                        $row['cat3'] = (trim($row['CNAME3']));
                        $row['cat4'] = (trim($row['CNAME4']));
                        unset($row['CNAME1']);
                        unset($row['CNAME2']);
                        unset($row['CNAME3']);
                        unset($row['CNAME4']);*/
                        unset($row['RN']);
                        $row['GENERATED']=(trim($row['GENERATED']))?date('Y-m-d',strtotime(trim($row['GENERATED']))):'';
                        $row['LAST_CHANGED']=(trim($row['LAST_CHANGED']))?date('Y-m-d',strtotime(trim($row['LAST_CHANGED']))):'';
                        $row['EXPIRY_DATE']=(trim($row['EXPIRY_DATE']))?date('Y-m-d',strtotime(trim($row['EXPIRY_DATE']))):'';
                        $row['created_at']=date('Y-m-d H:i:s');
                        $row['updated_at']=date('Y-m-d H:i:s');
                        //echo "<pre>";print_r($row);
                        DB::table('V_INVITEM')->insert($row);
                    }else{
                        $update_arr=array();
                        $update_arr['GRPCODE']       =   $row['GRPCODE'];
                        $update_arr['DIVISION']      =   $row['DIVISION'];
                        $update_arr['SECTION']       =   $row['SECTION'];
                        $update_arr['DEPARTMENT']    =   $row['DEPARTMENT'];
                        $update_arr['CCODE1']        =   $row['CCODE1'];
                        $update_arr['CCODE2']        =   $row['CCODE2'];
                        $update_arr['CCODE3']        =   $row['CCODE3'];
                        $update_arr['CCODE4']        =   $row['CCODE4'];
                        $update_arr['CCODE5']        =   $row['CCODE5'];
                        $update_arr['CCODE6']        =   $row['CCODE6'];
                        $update_arr['CNAME1']        =   $row['CNAME1'];
                        $update_arr['CNAME2']        =   $row['CNAME2'];
                        $update_arr['CNAME3']        =   $row['CNAME3'];
                        $update_arr['CNAME4']        =   $row['CNAME4'];
                        $update_arr['CNAME5']        =   $row['CNAME5'];
                        $update_arr['CNAME6']        =   $row['CNAME6'];
                        $update_arr['RATE']          =   $row['RATE'];
                        $update_arr['MRP']           =   $row['MRP'];
                        $update_arr['BARRQ']         =   $row['BARRQ'];
                        $update_arr['BARUNIT']       =   $row['BARUNIT'];
                        $update_arr['REM']           =   $row['REM'];
                        $update_arr['PARTYCODE']     =   $row['PARTYCODE'];
                        $update_arr['PARTYNAME']     =   $row['PARTYNAME'];
                        $update_arr['GENERATED']     =(trim($row['GENERATED']))?date('Y-m-d',strtotime(trim($row['GENERATED']))):'';
                        $update_arr['LAST_CHANGED']  =(trim($row['LAST_CHANGED']))?date('Y-m-d',strtotime(trim($row['LAST_CHANGED']))):'';
                        $update_arr['EXPIRY_DATE']   =(trim($row['EXPIRY_DATE']))?date('Y-m-d',strtotime(trim($row['EXPIRY_DATE']))):'';
                        $update_arr['GRCCODE']       =   $row['GRCCODE'];
                        $update_arr['LISTED_MRP']    =   $row['LISTED_MRP'];
                        $update_arr['ITEM_NAME']     =   $row['ITEM_NAME'];
                        $update_arr['DESC1']         =   $row['DESC1'];
                        $update_arr['DESC2']         =   $row['DESC2'];
                        $update_arr['DESC3']         =   $row['DESC3'];
                        $update_arr['DESC4']         =   $row['DESC4'];
                        $update_arr['DESC5']         =   $row['DESC5'];
                        $update_arr['DESC6']         =   $row['DESC6'];
                        $update_arr['NUM1']          =   $row['NUM1'];
                        $update_arr['NUM2']          =   $row['NUM2'];
                        $update_arr['NUM3']          =   $row['NUM3'];
                        $update_arr['SEARCH_STRING'] =   $row['SEARCH_STRING'];
                        $update_arr['updated_at']    =   date('Y-m-d H:i:s');


                        DB::table('V_INVITEM')->where('ICODE','=',$row['ICODE'])->update($update_arr);
                    }
                }
            }
        }
        echo "success";exit;
    }
}
