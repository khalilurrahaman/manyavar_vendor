<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use DB;

class syncGrc extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:syncGrc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Grc Order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $three_month = date("d-M-Y",strtotime("-1 Months"));
        $vendor_arr = DB::table('users')->select('SLCODE')->where('status','=',1)->where('type','=','vendor')->get();
        
        $vendor_PCODE='';
        foreach($vendor_arr as $eachvendor_PCODE)
        {
            $vendor_PCODE .= "'".$eachvendor_PCODE->SLCODE."',";
        }

        $vendor_PCODE1 = rtrim($vendor_PCODE,',');
      
       
        $query_str="SELECT * from v_invgrtmain  where PCODE IN (".$vendor_PCODE1.") AND GRTDT>='".$three_month."'";
        
        $grtlist=  User::oracleQuery($query_str);

        //echo count($grtlist);exit;
        foreach ($grtlist as $row) 
        {

            $row['GRTDT']=(trim($row['GRTDT']))?date('Y-m-d',strtotime(trim($row['GRTDT']))):'';
            $row['LAST_ACCESS_TIME']=(trim($row['LAST_ACCESS_TIME']))?date('Y-m-d',strtotime(trim($row['LAST_ACCESS_TIME']))):'';
            $row['created_at']=date('Y-m-d H:i:s');
            $row['updated_at']=date('Y-m-d H:i:s');
           

            $isexist=$this->checkGrtDuplicate($row['GRTNO']);

            if($isexist==0)
            {                
                DB::table('V_INVGRTMAIN')->insert($row);  
                $PURORDDET="SELECT * from v_invgrtdet where GRTNO = ".$row['GRTNO'];
                $p_order_det_item=  User::oracleQuery($PURORDDET);
                foreach ($p_order_det_item as $row1) 
                {
                    $row1['GRTDT']=(trim($row['GRTDT']))?date('Y-m-d',strtotime(trim($row['GRTDT']))):'';
                    $row1['created_at']=date('Y-m-d H:i:s');
                    $row1['updated_at']=date('Y-m-d H:i:s');
                    DB::table('V_INVGRTDET')->insert($row1);
                }             
            }
            else
            {
                $update_arr=array();

                $update_arr['SCHEME_DOCNO']         =   $row['SCHEME_DOCNO'];
                $update_arr['DOCCODE']              =   $row['DOCCODE'];
                $update_arr['REM']                  =   $row['REM'];
                $update_arr['GRSAMT']               =   $row['GRSAMT'];
                $update_arr['CHGAMT']               =   $row['CHGAMT'];
                $update_arr['NETAMT']               =   $row['NETAMT'];
                $update_arr['updated_at']           =   date('Y-m-d H:i:s');

                DB::table('V_INVGRTMAIN')->where('GRTNO','=',$row['GRTNO'])->update($update_arr);

                $grt_det="SELECT * from v_invgrtdet where GRTNO = ".$row['GRTNO'];

                $grt_det_item=  User::oracleQuery($grt_det);
                foreach ($grt_det_item as $row1) 
                { 
                    $row1['GRTDT']=(trim($row['GRTDT']))?date('Y-m-d',strtotime(trim($row['GRTDT']))):'';                  
                    $isexist1=$this->checkGrtDetailsDuplicate($row1['GRTNO'],$row1['ICODE']);
                    if($isexist1==0)
                    {
                        $row1['updated_at']=date('Y-m-d H:i:s');
                        DB::table('V_INVGRTDET')->insert($row1);
                    }
                    else
                    {
                        $update=array();
                        
                        $update['GRTDT']          =   $row1['GRTDT'];
                        $update['RTINVQTY']       =   $row1['RTINVQTY'];
                        $update['GRSAMT']         =   $row1['GRSAMT'];
                        $update['TAXAMT']         =   $row1['TAXAMT'];
                        $update['NETAMT']         =   $row1['NETAMT'];
                        $update['REM']            =   $row1['REM'];
                        $update['DISCOUNT']       =   $row1['DISCOUNT'];
                        $update['INVGRCDET_CODE'] =   $row1['INVGRCDET_CODE'];
                        $update['RSP']            =   $row1['RSP'];
                        
                        $update['updated_at']     =   date('Y-m-d H:i:s');

                        DB::table('V_INVGRTDET')->where('GRTNO','=',$row['GRTNO'])->where('ICODE', '=',$row1['ICODE'])->update($update);
                    }
                }
            }

        }

         echo 'success';exit;
         
    }

    public function checkGrtDuplicate($GRTNO)
    {
        $order=DB::table('V_INVGRTMAIN')->selectRaw('count(*) as num')->where('GRTNO', '=',$GRTNO)->first();

        return $order->num;
    }

    public function checkGrtDetailsDuplicate($GRTNO,$ICODE)
    {
        $order=DB::table('V_INVGRTDET')->selectRaw('count(*) as num')->where('GRTNO', '=',$GRTNO)->where('ICODE', '=',$ICODE)->first();

        return $order->num;
    }

    static function generatePOno($orderNo)
    {   
        $po_no=str_pad($orderNo, 6, '0', STR_PAD_LEFT);
        $po_id = 'PO/'.$po_no.'/'.date("y",strtotime("-1 year")).'-'.date("y");
        return $po_id;
    }
}
