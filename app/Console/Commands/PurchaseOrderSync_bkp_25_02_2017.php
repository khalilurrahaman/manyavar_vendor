<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use DB;

class PurchaseOrderSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:PurchaseOrderSync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Purchase Order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $three_month = date("d-M-Y",strtotime("-3 Months"));
        $vendor_arr = DB::table('users')->select('SLCODE')->where('status','=',1)->where('type','=','vendor')->get();
        
        $vendor_PCODE='';
        foreach($vendor_arr as $eachvendor_PCODE)
        {
            $vendor_PCODE .= "'".$eachvendor_PCODE->SLCODE."',";
        }

        $vendor_PCODE1 = rtrim($vendor_PCODE,',');
       
        $query_str="SELECT * from V_PURORDMAIN  where PCODE IN (".$vendor_PCODE1.") AND ORDDT>='".$three_month."'";
        $p_order=  User::oracleQuery($query_str);
        //echo "<pre>";print_r($p_order); exit; 
        foreach ($p_order as $row) 
        {
            $row['ORDDT']=(trim($row['ORDDT']))?date('Y-m-d',strtotime(trim($row['ORDDT']))):'';
            $row['TIME']=(trim($row['TIME']))?date('Y-m-d',strtotime(trim($row['TIME']))):'';
            $row['LAST_ACCESS_TIME']=(trim($row['LAST_ACCESS_TIME']))?date('Y-m-d',strtotime(trim($row['LAST_ACCESS_TIME']))):'';
            $row['created_at']=date('Y-m-d H:i:s');
            $row['updated_at']=date('Y-m-d H:i:s');
            $orderCode = $row['ORDCODE'];
            //$orderNo = $row['ORDNO'];
            $row['PONO'] = $this->generatePOno($row['ORDNO']);
           

            $isexist=$this->checkOrderDuplicate($row['ORDCODE']);

            if($isexist==0)
            {
                DB::table('purchase_order')->insert($row);

                $PURORDDET="SELECT * from V_PURORDDET where ORDCODE = $orderCode";
                $p_order_det_item=  User::oracleQuery($PURORDDET);
                foreach ($p_order_det_item as $row1) 
                {
                    $row1['created_at']=date('Y-m-d H:i:s');
                    $row1['updated_at']=date('Y-m-d H:i:s');
                    DB::table('V_PURORDDET')->insert($row1);
                }
            }
            else
            {
                $update_arr=array();

                $update_arr['DOCNO']                =   $row['DOCNO'];
                $update_arr['STAT']                 =   $row['STAT'];
                $update_arr['REM']                  =   $row['REM'];
                $update_arr['TIME']                 =   $row['TIME'];
                $update_arr['YCODE']                =   $row['YCODE'];
                $update_arr['LAST_ACCESS_TIME']     =   $row['LAST_ACCESS_TIME'];
                $update_arr['SHIPSTATUS']           =   $row['SHIPSTATUS'];
                $update_arr['DESCCMPCODE']          =   $row['DESCCMPCODE'];
                $update_arr['ORDCMPCODE']           =   $row['ORDCMPCODE'];
                $update_arr['CREATORCMPCODE']       =   $row['CREATORCMPCODE'];
                $update_arr['EXRATE']               =   $row['EXRATE'];
                $update_arr['updated_at']           =   date('Y-m-d H:i:s');


                DB::table('purchase_order')->where('ORDCODE','=',$row['ORDCODE'])->update($update_arr);

                $PURORDDET="SELECT * from V_PURORDDET where ORDCODE = $orderCode";

                $p_order_det_item=  User::oracleQuery($PURORDDET);
                foreach ($p_order_det_item as $row1) 
                {
                    $isexist1=$this->checkOrderDetailsDuplicate($row1['ORDCODE'],$row1['ICODE']);
                    if($isexist1==0)
                    {
                        $row1['updated_at']=date('Y-m-d H:i:s');
                        DB::table('V_PURORDDET')->insert($row1);
                    }
                    else
                    {
                        $update=array();
                        $update['ORDQTY']         =   $row1['ORDQTY'];
                        $update['CNLQTY']         =   $row1['CNLQTY'];
                        $update['RATE']           =   $row1['RATE'];
                        $update['RCQTY']          =   $row1['RCQTY'];
                        $update['OQTY']           =   $row1['OQTY'];
                        $update['REM']            =   $row1['REM'];
                        $update['CODE']           =   $row1['CODE'];
                        $update['DISCOUNT']       =   $row1['DISCOUNT'];
                        $update['updated_at']     =   date('Y-m-d H:i:s');


                        DB::table('V_PURORDDET')->where('ORDCODE','=',$row['ORDCODE'])->update($update);
                    }
                }
            }

        }

         echo 'success';exit;
         
    }

    public function checkOrderDuplicate($ORDCODE)
    {
        $order=DB::table('purchase_order')->selectRaw('count(*) as num')->where('ORDCODE', '=',$ORDCODE)->first();

        return $order->num;
    }

    public function checkOrderDetailsDuplicate($ORDCODE,$ICODE)
    {
        $order=DB::table('V_PURORDDET')->selectRaw('count(*) as num')->where('ORDCODE', '=',$ORDCODE)->where('ICODE', '=',$ICODE)->first();

        return $order->num;
    }

    static function generatePOno($orderNo)
    {   
        $po_no=str_pad($orderNo, 6, '0', STR_PAD_LEFT);
        $po_id = 'PO/'.$po_no.'/'.date("y",strtotime("-1 year")).'-'.date("y");
        return $po_id;
    }
}
