<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use DB;
use Carbon\Carbon;
use App\Models\Api;
use App\Models\UsersDevice;

class NotificationSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:NotificationSync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Expire PO Notification With Details';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $query_str_due_po=" purchase_order.status = 'OPEN' AND V_PURORDDET.STATUS = 'OPEN' AND  (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) > 0 ";
        $bet2="'".Carbon::now()->format('Y-m-d')."' AND '".Carbon::now()->addDay(15)->format('Y-m-d')."'";
            $query_str_due_po .=" AND purchase_order_delivery.DUEDATE BETWEEN ". $bet2;

        $expirein15daysPOList = DB::table('purchase_order')->select(['users.remember_token as device_token','usersdevice.device_type','usersdevice.token_id','usersdevice.device_id','purchase_order.PONO as PONO'])
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('purchase_order_delivery', 'purchase_order_delivery.PURORDDET_CODE', '=', 'V_PURORDDET.CODE')
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
        ->leftJoin('usersdevice', 'usersdevice.user_id', '=', 'users.id')  
        ->where('purchase_order.status', '=','OPEN')
        ->where('V_PURORDDET.status', '=','OPEN')
        ->whereRaw($query_str_due_po)
        ->orderBy('purchase_order_delivery.DUEDATE', 'asc')->get();
        //print_r($expirein15daysPOList);exit;
        if(!empty($expirein15daysPOList) && count($expirein15daysPOList)>0)
        {
            foreach ($expirein15daysPOList as $eachUser) 
            {
                $msg="Please take some action against the PO No:".$eachUser->PONO;
                $device_id=$eachUser->device_id;
                $device_token=$eachUser->device_token;
                $device_type=$eachUser->device_type;
                $token_id=$eachUser->token_id;
                $title='PO Expired in 15 Days';
                //dd($token_id);
                if(!empty($token_id))
                User::sendPush($device_token,$device_type,$device_id,$msg,$title,$token_id);
            }  
        } 


        echo 'success';exit;
    }
}
