<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use DB;

class VendorItemSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:VendorItemSync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Item With Details';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$icodelist=DB::table('V_PURORDDET')->select('ICODE')->groupBy('ICODE')->get();
		#echo "<pre>";print_r($icodelist);exit;
		if(!empty($icodelist))
		{
			foreach($icodelist as $icode)
			{	   
					$ICODE=$icode->ICODE;
					$query_str="SELECT * from V_INVITEM  where ICODE = '".$ICODE."'";
					$item_list=  User::oracleQuery($query_str);
					#echo "<pre>";print_r($item_list);
					if(!empty($item_list))
					{
						foreach ($item_list as $row) 
						{		
								if(!empty($row))
								{
										#$isexist=DB::table('V_INVITEM')->selectRaw('count(*) as num')->where('ICODE','=',$row['ICODE'])->first();
										$isexist=DB::table('products')->selectRaw('count(*) as num')->where('ICODE','=',$row['ICODE'])->first();
										
										if(empty($isexist->num))
										{
											//unset($row['RN']);
											
											
											$row['cat1']=(trim($row['CNAME1']));
											$row['cat2']=(trim($row['CNAME2']));
											$row['cat3']=(trim($row['CNAME3']));
											$row['cat4']=(trim($row['CNAME4']));
											unset($row['CNAME1']);
											unset($row['CNAME2']);
											unset($row['CNAME3']);
											unset($row['CNAME4']);
											$row['GENERATED']=(trim($row['GENERATED']))?date('Y-m-d',strtotime(trim($row['GENERATED']))):'';
											$row['LAST_CHANGED']=(trim($row['LAST_CHANGED']))?date('Y-m-d',strtotime(trim($row['LAST_CHANGED']))):'';
											$row['EXPIRY_DATE']=(trim($row['EXPIRY_DATE']))?date('Y-m-d',strtotime(trim($row['EXPIRY_DATE']))):'';
											$row['created_at']=date('Y-m-d H:i:s');
											$row['updated_at']=date('Y-m-d H:i:s');
											DB::table('products')->insert($row);
										}
										else
										{
											$UPDICODE=$row['ICODE'];
											$row['cat1']=(trim($row['CNAME1']));
											$row['cat2']=(trim($row['CNAME2']));
											$row['cat3']=(trim($row['CNAME3']));
											$row['cat4']=(trim($row['CNAME4']));
											unset($row['CNAME1']);
											unset($row['CNAME2']);
											unset($row['CNAME3']);
											unset($row['CNAME4']);
											$row['GENERATED']     =(trim($row['GENERATED']))?date('Y-m-d',strtotime(trim($row['GENERATED']))):'';
											$row['LAST_CHANGED']  =(trim($row['LAST_CHANGED']))?date('Y-m-d',strtotime(trim($row['LAST_CHANGED']))):'';
											$row['EXPIRY_DATE']   =(trim($row['EXPIRY_DATE']))?date('Y-m-d',strtotime(trim($row['EXPIRY_DATE']))):'';
											$row['updated_at']    =date('Y-m-d H:i:s');
											unset($row['id']);
											unset($row['ICODE']);
											unset($row['created_at']);
											
											DB::table('products')->where('ICODE','=',$UPDICODE)->update($row);
										}
								}
						}
					}
			}
		}
        echo "success";exit;
    }
}
