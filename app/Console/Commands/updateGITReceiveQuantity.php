<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use DB;

class updateGITReceiveQuantity extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:updateGITReceiveQuantity';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update GIT Receive Quantity';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $git_arr = DB::table('git')->select('git_no','ORDCODE')->where('status','!=','Rejected')->where('status','!=','Close')->get();

        if(!empty($git_arr) && count($git_arr)>0)
        {
            foreach($git_arr as $git)
            {
                $git_no=$git->git_no;
                $ORDCODE=$git->ORDCODE;
                $grclist = DB::table('v_invgrcmain')->select(DB::raw('sum(RCQTY) as RCQTY'))->where('ORDCODE','=',$ORDCODE)->where('REFERENCE_NO','=',$git_no)->first();        
                
                if(!empty($grclist) && count($grclist)>0)
                {
                    $row_update['git_rcv']=$grclist->RCQTY;           
                    DB::table('git')->where('git_no','=',$git_no)->where('ORDCODE','=',$ORDCODE)->update($row_update);
                }
            }
        }
        

        echo 'success';exit;
         
    }
}
