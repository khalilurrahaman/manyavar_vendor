<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\VendorSync',
        'App\Console\Commands\PurchaseOrderSync',
        'App\Console\Commands\syncGrc',
        'App\Console\Commands\PurchaseOrderCharge',
        'App\Console\Commands\VendorItemSync',
        'App\Console\Commands\VendorPriceSync',
        'App\Console\Commands\NotificationSync',
        'App\Console\Commands\syncGrcMain',
        'App\Console\Commands\updateGITReceiveQuantity',
        'App\Console\Commands\markCompleteGit',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
