<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Permission;
use App\Models\PermissionRole;
use App\User;
use Auth;
use Redirect;
use DB;

class roleController extends Controller
{
    public function index()
    {
    	$data['roles']=Role::where('status','=','1')->get();
    	
    	return view('admin.role-list',$data);
    }

    public function roleAdd(Request $request)
    {
    	$user=Auth::user();

    	$name=trim($request->name);
    	$slug=trim($request->slug);
    	$description=trim($request->description);

    	$role=new Role;
    	$role->name=$name;
    	$role->slug=$slug;
    	$role->description=$description;
    	$role->createdby=$user->id;
    	$role->modifiedby=$user->id;
    	$role->save();

    	return Redirect::route('role-listing');


    }
    public function addPermission($id)
    {
       $finalPermissions = array();
        $firstSelectBox= array();
        $assigned_permissions = array();

        $role_user=Role::findOrFail($id);
        $allPermissionByGroup = Permission::groupby('model')->distinct()->get();
        $allAssignedPermissions=PermissionRole::where('role_id','=',$role_user->id)->get();
        //dd($allAssignedPermissions)
        foreach ($allAssignedPermissions as $eachAssignedPermissions) {

            $permissions_details=Permission::where('id','=',$eachAssignedPermissions->permission_id)->first();
            if(isset($permissions_details->slug))
            {
                $assigned_permissions[] = $permissions_details->slug;
            }

        }
        foreach ($allPermissionByGroup as $key => $eachPermissionByGroup) {
            
            $allPermissions=Permission::where('model','=',$eachPermissionByGroup->model)->get();

                foreach ($allPermissions as $eachPermission) {

                        $finalPermissions[$eachPermission->model][] = $eachPermission; 
                   
                }
        }
        
        //$data['page']='Manage Permission';
        //dd($assigned_permissions);
        return view('admin.add-role-permission')->with('assigned_permissions',$assigned_permissions)->with('role_user',$role_user)->with('allPermission',$finalPermissions);
    }
    public function insertPermission(Request $request)
    {
        $role_id=$request->role_id;
        $rolePermission=PermissionRole::where('role_id','=',$role_id)->delete();
        if(!empty($request['permission'])){
            foreach ($request['permission'] as $val) {

                $PermissionRole= new PermissionRole;

                $PermissionRole->permission_id=$val;
                $PermissionRole->role_id=$request->role_id;
                $PermissionRole->save();

            }
        }
        return Redirect::back()->withErrors(['Permission added successfully']);

    }
    public function roleDelete($id)
    {
        $role_delete=Role::where('id','=',$id)->update(['status'=>0]);
        return \Redirect::back();
    }
    public function roleEdit(Request $request)
    {
        $id=$request->id;
        $role_edit=Role::where('id','=',$id)->first();
        return $role_edit;
    }
    public function roleUpdate(Request $request)
    {
        $role_id=$request->role_id;
        $name=$request->name;
        $slug=$request->slug;
        $description=$request->description;
       
        $role_update=Role::where('id','=',$role_id)->first();
        $role_update->name=$name;
        $role_update->slug=$slug;
        $role_update->description=$description;
        if($role_update->save())
        {
           return \Redirect::back();
        }
        

    }
    public function viewUser($id)
    {
        $user=DB::table('users')->join('roles','users.role_id','=','roles.id')->where('users.role_id','=',$id)->select('users.name','users.mobile','users.email','roles.name as role_name')->paginate(10);
        return view('admin.role-user-list')->with('user',$user);
    }
    
        
}