<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Rfq;
use App\Models\Quetes;
use App\Models\RfqDetails;
use App\Models\Product;
use App\Models\Comments;
use Redirect;
use Auth;
use DB;

class RequestQuotationController extends Controller
{
    public function index()
    {
    	$quoteLisr = Rfq::select('rfqs.id', 'rfqs.req_id', 'rfq_quetes.id as q_id','rfq_quetes.quete_id', 'rfq_quetes.price', 'rfq_quetes.width', 'rfq_quetes.created_at as quete_date','rfq_quetes.status as quete_status','users.id as vendor_id','users.first_name','users.last_name','products.cat1','products.cat2','products.cat3','products.cat4')
            ->join('rfq_quetes', 'rfqs.id', '=', 'rfq_quetes.rfq_id')
            ->join('users', 'rfq_quetes.vendor_id', '=','users.id')
            ->join('products','rfqs.product_id', '=', 'products.id')
            ->where('rfqs.status', '=',1)
            ->orderBy('rfq_quetes.created_at','desc')
            ->get();
                
            foreach ($quoteLisr as $key=>$value) {
                $quoteLisr[$key]['rfq_comments'] =Comments::select('comments')->where('quete_id', '=',$value->q_id)->orderBy('created_at','desc')->first();
            }

        return view('admin.vendor-all-rfq')->with('quoteLisr', $quoteLisr);
    }

    public function rfqList()
    {
        $rfqList = Rfq::select('rfqs.id', 'rfqs.status', 'rfqs.req_id', 'products.cat1','products.cat2','products.cat3', 'products.image')
                ->join('products','rfqs.product_id', '=', 'products.id')
                ->orderBy('rfqs.created_at','desc')
                ->get();
                
            foreach ($rfqList as $eachRfq) {
                $vendor_ids = RfqDetails::select('vendor_id')->where('rfq_id', '=', $eachRfq->id)->get();
            }
                dd($vendor_ids);
                /*if(!empty($vendor_ids)){
                    foreach ($vendor_ids as $key => $eachVendor) {
                        $rfqList[$key]['vendor_details'] = User::select('first_name', 'last_name')->where('id', '=', $eachVendor->vendor_id)->get();
                    }
                }else{
                    $rfqList[$key]['vendor_details'] = 0;
                }
            }*/
            dd($vendor_ids);
        return view('admin.rfq-list')->with('rfqList', $rfqList);
    }

    public function closeRequest($id)
    {
        $rfqs = Rfq::where('id','=', $id)->update(['status' =>0]);
        
        return Redirect::route('rfq-request');
    }

    public function getVendorList(Request $request)
    {
        $vendor_list = DB::table('rfq_quetes')
                    ->join('users', 'rfq_quetes.vendor_id', '=','users.id')
                    ->where('rfq_quetes.rfq_id', '=',$request->rfq_id)
                    ->groupBy('rfq_quetes.vendor_id')
                    ->select(['users.id', 'users.first_name', 'users.last_name'])->get();

        return $vendor_list;
    }

    public function closeRfq($id)
    {
        $rfqs = Rfq::where('id','=', $id)->update(['status' =>0]);

        return Redirect::route('rfq-list');
    }
     
    public function addNewItem(Request $request)
    {
        $file = $request->file('file');

        $imageName = time().'_'.$request->file('file')->getClientOriginalName();
        if($file->move(public_path('upload/product'), $imageName)){
            $product = new Product;
            $product->image = $imageName;
            $product->cat1 = $request->cat1;
            $product->cat2 = $request->cat2;
            $product->save();
        }
		$product_details = Product::where('id','=',$product->id)->first();
        echo $product_details; exit;
    }

    public function getAddrfq()
    {
    	$vendor_list = User::where('type','=','vendor')->where('status', '=', 1)->get();
    	$product_list = Product::where_not_null('cat1')->get();
		$sample_list = Product::whereNull('cat1')->get();
    	return view('admin.vendor-add-rfq')->with('allVendor', $vendor_list)->with('allProduct', $product_list)->with('sampleProduct', $sample_list);
    }
	
	public function searchItemByKeyword(Request $request)
    {
    	$product_list = Product::where('cat1', 'like', '%' . $request->search_str . '%')
    	->orWhere('cat2', 'like', '%' . $request->search_str . '%')
		->orWhere('cat3', 'like', '%' . $request->search_str . '%')
		->orWhere('cat4', 'like', '%' . $request->search_str . '%')
		->get();
		$html='';
		 if(!empty($product_list) && count($product_list)>0){
                                   foreach($allProduct as $ext){
                                    $html .='<tr>';
                                    	$html .='<td class="text-center">';
 										
                                            $e_image=URL::to('/')."/img/pre-order-img.jpg";
                                            if(!empty($ext->image) && file_exists(public_path().'/upload/product/'.$ext->image))
                                            {
                                               $e_image= URL::to('/') .'/upload/product/'.$ext->image;
                                            }
                                       
                                        $html .='<img src="'.$e_image.'" title="small image" width="56" class="selectProductExistClassImage'.$ext->id.'">';
                                        $html .='</td>';
                                        $html .='<td class="text-center selectProductExistClassCat1'.$ext->id.'">'.$ext->cat1.'</td>';
                                        $html .='<td class="text-center selectProductExistClassCat2'.$ext->id.'">'.$ext->cat2.'</td>';
                                        $html .='<td class="text-center selectProductExistClassCat3'.$ext->id.'">'.$ext->cat3.'</td>';
                                        $html .='<td class="text-center selectProductExistClassCat4'.$ext->id.'">'.$ext->cat4.'</td>';
                                        $html .='<td class="text-center">';
                                        $html .='<a href="javascript:void(0);" product_id="'.$ext->id.'" class="btn btn-success selectProductExist" style="width:56px; padding:3px 0; font-size:12px;">Select</a>';
                                        $html .='</td>';
                                    $html .='</tr>';
								   }
		 }
                                    else
									{
                                     $html .='<tr>';
                                    	$html .='<td class="text-center" colspan="6">Product Not Available!</td>';
                                    $html .='</tr>';
									}
                                    
    	return $html;
    }

    public function postAddrfq(Request $request)
    {
        $user = Auth::user();

        $req_id= $this->generateReq();
        $rfq = new Rfq;
        $rfq->req_id = $req_id;
        $rfq->product_id = $request->product_id;
        $rfq->createdby = $user->id;
        $rfq->modifiedby = $user->id;
        $rfq->status = 1;
        if($rfq->save()){
            $vendor_id = $request->vendor_id;
            $shipment_provider = $request->shipment_provider;
            $docket_no = $request->docket_no;
            foreach ($vendor_id as $key=>$eachVendor) {
                $rfq_details= new RfqDetails;
                $rfq_details->rfq_id = $rfq->id; 
                $rfq_details->vendor_id = $eachVendor; 
                $rfq_details->shipment_provider = $shipment_provider[$key];
                $rfq_details->docket_no = $docket_no[$key];
                $rfq_details->createdby = $user->id;
                $rfq_details->modifiedby = $user->id;
                $rfq_details->status = 1;
                $rfq_details->save();
            }
        }
        return Redirect::route('rfq-list');
    }

    static function generateReq()
    {
        $last_req = Rfq::selectRaw('max(id) as id')->first();
        $req_id=str_pad($last_req->id+1, 6, '0', STR_PAD_LEFT);
        $req_id = 'REQ/'.$req_id;
        return $req_id;
    }

    public function adminCommant($id)
    {
        //dd($id);
        $quoteComments = Comments::where('quete_id', '=', $id)->orderBy('id','asc')->get();
        //dd($quoteComments);
        return view('admin.vendor-comments')->with('comments_list',$quoteComments)->with('quotation_id', $id);
    }

    public function adminAddCommant(Request $request)
    {
        $return=0;
        
        $quotation['quete_id'] = $request->quotation_id;
        $quotation['user_id'] = Auth::user()->id;   
        $quotation['comments'] = $request->comments;
        $quotation['created_at'] = date('Y-m-d H:i:s');
        $quotation['updated_at'] = date('Y-m-d H:i:s');
                
        if(DB::table('rfq_comments')->insert($quotation))
        {
            $return=1;
        }
        echo $return;
    }
}
