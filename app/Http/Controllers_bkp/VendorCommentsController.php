<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VendorCommentsController extends Controller
{
    public function index()
    {
    	return view('admin.vendor-comments');
    }
}
