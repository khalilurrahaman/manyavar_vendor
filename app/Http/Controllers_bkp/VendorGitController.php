<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Barryvdh\DomPDF\Facade as PDF;
class VendorGitController extends Controller
{
    public function viewGit()
    {
    	return view('admin.vendor-all-git-list');
    }

    public function viewGrt()
    {
    	return view('admin.vendor-all-grt-list');
    }
	
	
    public function viewAllVendorGit()
    {
		
		$data['gitList'] = DB::table('git')->selectRaw('products.cat1,products.cat2,products.cat3,products.cat4,V_PURORDDET.ORDQTY,V_PURORDDET.RCQTY,V_PURORDDET.CNLQTY,V_PURORDDET.OQTY,git.*,(select count(*) from git g where g.challan_no=git.challan_no ) as rowspan')
		
    	->leftJoin('V_PURORDDET', 'git.ORDCODE', '=', 'V_PURORDDET.ORDCODE' ,'and','git.ICODE', '=', 'V_PURORDDET.ICODE')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
    	->orderBy('git.created_at', 'desc')
		->paginate(30);
		
		
		
		
		
		
		#echo "<pre>";print_r($data['gitList']);exit;
    	return view('vendor.vendor-all-git',$data);
    }
	
    public function viewAddVendorGit()
    {
		
		#echo Auth::user()->SLCODE;exit;
		$data['cat1_list'] = DB::table('products')->select('cat1')->groupBy('cat1')->orderBy('cat1','desc')->get();
		$data['cat2_list'] = DB::table('products')->select('cat2')->groupBy('cat2')->orderBy('cat2','desc')->get();
		$data['cat3_list'] = DB::table('products')->select('cat3')->groupBy('cat3')->orderBy('cat3','desc')->get();
		$data['cat4_list'] = DB::table('products')->select('cat4')->groupBy('cat4')->orderBy('cat4','desc')->get();
		
		$data['pending_qty_list'] =DB::table('purchase_order')->select(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY as p_qty'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->where('purchase_order.status', '=','OPEN')
        ->orderBy('p_qty')
		->distinct('p_qty')->get();
		
		$data['purchaseOrderList'] = DB::table('purchase_order')->select('purchase_order.ORDNO','products.image','products.cat1','products.cat2','products.cat3','products.cat4','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.ORDCODE','V_PURORDDET.ICODE','V_PURORDDET.CNLQTY','V_PURORDDET.OQTY',DB::raw('sum(git.git_qty) as git_qty'))
    	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->leftJoin('git', 'git.ORDCODE', '=', 'V_PURORDDET.ORDCODE' ,'and','git.ICODE', '=', 'V_PURORDDET.ICODE')
    	->where('purchase_order.status', '=','OPEN')
		->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
    	->orderBy('purchase_order.ORDDT', 'desc')
		->groupBy('V_PURORDDET.id')
		->get();
		#echo "<pre>";print_r($data['purchaseOrderList']);exit;
    	return view('vendor.vendor-add-git',$data);
    }
	
	public function filterAddGit(Request $request)
    {
		
		
		$pending_qty=$request->pending_qty;
		$category1=$request->category1;
		$category2=$request->category2;
		$category3=$request->category3;
		$category4=$request->category4;
		
		$query_str=" purchase_order.status = 'OPEN' ";
    	//if(!empty($pending_qty))
    		//$query_str .=" AND pending_qty.ORDNO = '".$pending_qty."' ";
    	if(!empty($category1))
    		$query_str .=" AND products.cat1 = '".$category1."' ";
		if(!empty($category2))
    		$query_str .=" AND products.cat2 = '".$category2."' ";
		if(!empty($category3))
    		$query_str .=" AND products.cat3 = '".$category3."' ";
		if(!empty($category4))
    		$query_str .=" AND products.cat4 = '".$category4."' ";
			
			
			
		$data['purchaseOrderList'] = DB::table('purchase_order')->select('purchase_order.ORDNO','products.image','products.cat1','products.cat2','products.cat3','products.cat4','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.ORDCODE','V_PURORDDET.ICODE','V_PURORDDET.CNLQTY','V_PURORDDET.OQTY',DB::raw('sum(git.git_qty) as git_qty'))
    	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->leftJoin('git', 'git.ORDCODE', '=', 'V_PURORDDET.ORDCODE' ,'and','git.ICODE', '=', 'V_PURORDDET.ICODE')
    	//->where('purchase_order.status', '=','OPEN')
		->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
		->whereRaw($query_str)
    	->orderBy('purchase_order.ORDDT', 'desc')
		->groupBy('V_PURORDDET.id')
		->get();
		#echo "<pre>";print_r($data['purchaseOrderList']);exit;
    	return view('vendor.vendor-add-git-filter',$data);
    }
	
	public function submitGitRecords(Request $request)
    {
		
		for($i=0;$i<count($request->git_qty);$i++)
		{
			if(!empty($request->git_qty[$i]))
			{
				$git['ORDCODE'] = $request->ORDCODE[$i];//
				$git['ICODE'] = $request->ICODE[$i];//
				$git['SLCODE'] = Auth::user()->SLCODE;
				$git['git_qty'] = $request->git_qty[$i];//
				$git['challan_no'] = $request->challan_no;
				$git['lorry_no'] = $request->lorry_no;
				$git['transport_no'] = $request->transport_no;
				$git['git_date'] = $request->date;
				$git['created_at'] = date('Y-m-d H:i:s');
				$git['updated_at'] = date('Y-m-d H:i:s');
				$git['status'] = 'Pending';
				DB::table('git')->insert($git);
			}
		}
		
		
		return view('vendor.success-git',$git);
		
    }	
	
	public function generateInvoice($data='')
    {
		echo 'Generate Invoice Successfulley';exit;
		 $content = 'hhh';
		// $admission_letter_css = public_path().'/css/letter/letter.css';
		 $mypdf = new mPDF();
		 //$mypdf->WriteHTML(file_get_contents($admission_letter_css), 1);
		 $mypdf->WriteHTML($content, 2);
		 $mypdf->Output();
 
    	/*$pdf = app('dompdf.wrapper');
		$pdf = $pdf->loadView('vendor.git-invoice', $data);
		dd($pdf);
		return $pdf->stream();*/
	}
	
	public function downloadInvoice($data='')
    {
		echo 'Download Invoice Successfulley';exit;
		$pdf = \PDF::loadView('vendor.git-invoice', $data);
		return $pdf->download('invoice.pdf');
	
	}
	
	public function updateGitRecords(Request $request)
    {
		$return=0;
		
        $git['lorry_no'] = $request->lorry_no;
        $git['transport_no'] = $request->transport_no;
        $git['updated_at'] = date('Y-m-d H:i:s');
                
        if(DB::table('git')->where('challan_no', $request->ch_no)->update($git))
        {
            $return=1;
        }
        echo $return;
	
	}
	
	public function downloadInvoiceByChallan(Request $request)
    {
		$challan=$request->challan;
		
		$data['gitList'] = DB::table('git')->selectRaw('products.cat1,products.cat2,products.cat3,products.cat4,V_PURORDDET.ORDQTY,V_PURORDDET.RCQTY,V_PURORDDET.CNLQTY,V_PURORDDET.OQTY,git.*,(select count(*) from git g where g.challan_no=git.challan_no ) as rowspan')
    	->leftJoin('V_PURORDDET', 'git.ORDCODE', '=', 'V_PURORDDET.ORDCODE' ,'and','git.ICODE', '=', 'V_PURORDDET.ICODE')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
		->where('git.challan_no', '=',$challan)
    	->orderBy('git.created_at', 'desc')
		->get();
		
		$this->downloadInvoice();
	
	}
	
	
	
	
}
