<?php


namespace App\Http\Controllers;
use App\User;
use App\Models\price;
use DB;

class VendorInformationController extends Controller
{

	public function syncVendorDetails()
    {
		echo 'Fetch Data From v_finsh table'.'<br>';
		
		$vendor_detalis=DB::table('v_finsh')->select(['SLCODE','SLNAME', 'BEMAIL'])->get();
		
		$vendor_list=array();
		foreach ($vendor_detalis as $value) {
			$vendor_list[] = $value;
		}
		//echo "<pre>";print_r($vendor_list); exit;
		$list = array_chunk($vendor_list, 100);
		$row = array();
		for($i=0; $i <count($list); $i++) { 
			foreach ($list[$i] as $row1) {
				$row['SLCODE'] = trim($row1->SLCODE);
				$row['name'] = trim($row1->SLNAME);
				$row['username'] = trim($row1->BEMAIL);
				$row['email'] = trim($row1->BEMAIL);
				$row['type'] = 'vendor';
				$row['created_at'] = date('Y-m-d H:i:s');
				$row['updated_at'] = date('Y-m-d H:i:s');
				DB::table('users_copy')->insert($row);
			}
		}
		echo "success";
	}
}
