<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Redirect;
class AdminGitController extends Controller
{
    public function viewGit()
    {

		$gitList = DB::table('git')->selectRaw('git.id,users.name,products.cat1,products.cat2,products.cat3,V_PURORDDET.ORDQTY,V_PURORDDET.RCQTY,V_PURORDDET.CNLQTY,V_PURORDDET.OQTY,git.git_qty,git.git_date,git.git_qty,git.updated_at,git.status,git.challan_no,(select count(*) from git g where g.challan_no=git.challan_no ) as rowspan')
    	->leftJoin('V_PURORDDET', 'git.ORDCODE', '=', 'V_PURORDDET.ORDCODE' ,'and','git.ICODE', '=', 'V_PURORDDET.ICODE')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
    	->join('users', 'purchase_order.PCODE', '=', 'users.SLCODE')
    	->where('git.status', '!=','Close')
    	->orderBy('git.updated_at', 'desc')
		->paginate(30);
		//dd($gitList);
    	return view('admin.vendor-all-git-list')->with('gitList',$gitList);
    }

    public function markCompleteGit()
    {

    	return Redirect::route('all-git-list');
    }
    public function viewAllVendorGit()
    {
    	return view('vendor.vendor-all-git');
    }
}
