<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Models\Devision;
use DB;
use Redirect;

class AdminDevisionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list=Devision::orderBy('id')->get();
        return view('admin.devision-list')->with('list',$list);
    }
    public function getEditData(Request $request)
    {
        $id=$request->id;
        $data=Devision::where('id','=', $id)->first();
        return $data;
    }
    
    public function addDevision(Request $request)
    {
        $user=\Auth::user();
        $this->validate($request,[
        'name'=>'required',        
        'description'=>'required'
        ]);

        $name=trim($request->name);        
        $description=$request->description;        
        /*$createdby=$user->id;
        $modifiedby=$user->id;*/
        $createdby=3;
        $modifiedby=3;


        $devision=new Devision;
        $devision->name=$name; 
        $devision->description=$description;      
        $devision->createdby=$createdby;
        $devision->modifiedby=$modifiedby;
        if($devision->save())
        {
            return redirect::route('division-listing');
                  
        }
        else
        {
            return redirect::route('division-listing');
        }

    }

    public function updateDevision(Request $request)
    {

        $user=\Auth::user();
        $this->validate($request,[
        'name'=>'required',        
        'description'=>'required'
        ]);
        $id=$request->id;
        $name=trim($request->name);        
        $description=$request->description;        
/*        $modifiedby=$user->id;
*/        $modifiedby=3;

        $data=Devision::where('id','=',$id)->first();
        $data->name=$name;
        $data->description=$description;
        $data->modifiedby=$modifiedby;
        if($data->save())
        {
            return redirect()->back();
        }
        else
        {
            return redirect()->back();
        }

        /*$data=\DB::table('departments')->where('id','=',$id)->update(
            array(
                'name' => $name,                
                'description' => $description,                
                'modifiedby' => $modifiedby          
               
                ));*/
        
        
    }
    public function deleteDevision(Request $request)
    {
        $id=$request->id;
        $data=Devision::where('id','=',$id);
        if($data->delete())
        {
            return redirect()->back();
        }
    }
}
