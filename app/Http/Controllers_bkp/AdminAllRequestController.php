<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class AdminAllRequestController extends Controller
{
    public function viewAllRequest()
    {
    	 $request_list = DB::table('po_action')        
        ->select('po_action.*')
        ->orderBy('created_at','desc')
        ->paginate(10);             
       
        //dd($request_list);
       $po_list = DB::table('po_action')        
        ->select('po_no')
        ->distinct('po_no')             
        ->get(); 

        if(!empty($request_list) && count($request_list)>0)
        {
            foreach($request_list as $reqst)
            {
                $req_comments =DB::table('request_comments')->where('action_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
                        if(!empty($req_comments) && count($req_comments)>0)
                        $reqst->req_comments=$req_comments;
                }
            }
//dd($po_list);
    	return view('admin.vendor-all-request-list')->with('request_list',$request_list)->with('po_list',$po_list);
    }
     public function filterAction(Request $request)
    {
    	/*--Filter By Type*/
      $user_type=Auth::user()->type;      
      $status=$request->status;
    	$type=$request->type;
    	$po_no=$request->po_no;
    	$query="action_type != ''";
    	if(!empty($type))
    		$query .=" AND action_type = '".$type."' ";
    	if(!empty($po_no))
    		$query .=" AND po_no = '".$po_no."' ";
      if(!empty($status))
        $query .=" AND status = '".$status."' ";
         if($user_type=='vendor'){
    	 		$data['request_list'] = DB::table('po_action')  
		        ->select('po_action.*')
            ->where('user_id', '=',Auth::user()->id) 
		        ->whereRaw($query)            
		        ->paginate(10);
        }
        else{
          $data['request_list'] = DB::table('po_action')  
            ->select('po_action.*')           
            ->whereRaw($query)            
            ->paginate(10);
        }
         if(!empty($data['request_list']) && count($data['request_list'])>0)
        {
            foreach($data['request_list'] as $reqst)
            {
                $req_comments =DB::table('request_comments')->where('action_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
                        if(!empty($req_comments) && count($req_comments)>0)
                        $reqst->req_comments=$req_comments;
                }
            }
        //dd($request_list);
  //       $html='';
  //       $inc=0;
  //       if(!empty($request_list) && count($request_list)){
  //       	foreach ($request_list as $req) {
  //       		 							$type='';
  //                                        if($req->action_type=='cutting')
  //                                         $type='Request For Cutting';
  //                                         elseif($req->action_type=='rate_change') 
  //                                          $type='Request For Rate Change';
  //                                         elseif($req->action_type=='width_change') 
  //                                          $type='Request For Width Change';
  //                                         elseif($req->action_type=='date_ext') 
  //                                          $type='Request For Date Extention';
  //                                         elseif($req->action_type=='cancel') 
  //                                          $type='Request For Cancellation';
  //                                         elseif($req->action_type=='others') 
  //                                          $type='Request For Others';
  //                       $inc++;

  //       								$html .=' <tr>
  //                                     <td class="text-center">'.$inc.'</td>
  //                                   	<td class="text-center">'.$req->po_no.'</td>
  //                                   	<td class="text-center">26/09/2016</td>
  //                                       <td class="text-center">'.$type.'</td>
  //                                       <td class="text-center">--</td>
  //                                       <td class="text-center">--</td>
  //                                       <td class="text-center">--</td>
  //                                       <td class="text-center"><span class="sm-btn btn-primary btn-xs">New</span></td>
  //                                       <td class="text-center">Lorem Ipsum... <a href="vendor-comments-2.php" class="btn btn-warning btn-xs">Details</a></td>
  //                                       <td class="text-center"><a href="javascript:void(0)" class="btn btn-success">Accpet</a></td>
  //                                   </tr>';

  //                               }
  //                           }
  //                           else{
  //                           	$html .=' <tr>
  //                                   	<td class="text-center" colspan="10">No Result Found</td>
  //                                   </tr>';


  //       }

  // echo $html;
    	
  //   }

  return view('admin.vendor-all-request-list-filter',$data); 
    }

     public function reqWiseComments($id)
    {
        //dd($id);
        $Comments = DB::table('request_comments')
                  ->select('*')
                  ->where('action_id', '=', $id)->orderBy('created_at','desc')->get();
        //dd($Comments);
        return view('admin.admin-requestwise-comments')->with('comments_list',$Comments)->with('action_id', $id);
       
    }
     public function addReqComments(Request $request)
    {
        $return=0;
        
        $action['action_id'] = $request->action_id;
        $action['user_id'] = Auth::user()->id;   
        $action['comments'] = $request->comments;
        $action['created_at'] = date('Y-m-d H:i:s');
        $action['updated_at'] = date('Y-m-d H:i:s');
                
        if(DB::table('request_comments')->insert($action))
        {
            $return=1;
        }
        echo $return;
    }
public function acceptRequest(Request $request)
    {
        $return=0;
        $quotation['admin_remarks'] = $request->reason;
        $quotation['status'] = 'accept'; 
        if($request->type=='rate_change' || $request->type=='width_change'){
        
        $quotation['accepted_rate'] = $request->accept_val;  
        }
       
       if($request->type=='sample'){
        
        $quotation['docket_no'] = $request->docket_no;
        $quotation['prn_no'] = $request->prn_no;
        $quotation['prn_date'] = $request->prn_date;
        $quotation['courier_no'] = $request->courier_no;  
        }
                
        if(DB::table('po_action')->where('id','=', $request->action_id)->update($quotation))
        {
            $return=1;
        }
        echo $return;
    }

    public function rejectRequest(Request $request)
    {
        $return=0;
        $quotation['admin_remarks'] = $request->reason;
        $quotation['status'] = 'reject';        
               
        if(DB::table('po_action')->where('id','=', $request->action_id)->update($quotation))
        {
            $return=1;
        }
        echo $return;
    }
}