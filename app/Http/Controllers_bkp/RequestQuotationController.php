<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Rfq;
use App\Models\Quetes;
use App\Models\RfqDetails;
use App\Models\Product;
use App\Models\Comments;
use Redirect;
use Auth;
use DB;
use URL;

class RequestQuotationController extends Controller
{
    public function index()
    {

        $rfq_list = DB::table('rfqs')   
        ->join('products','rfqs.product_id', '=', 'products.id')
        ->join('rfq_details', 'rfqs.id', '=', 'rfq_details.rfq_id') 
		->join('users', 'rfq_details.vendor_id', '=', 'users.id') 
        ->select('rfqs.*','rfq_details.vendor_id','rfq_details.notify','users.name','products.cat1','products.cat2')        
        ->where('rfqs.status','=',1)->orderBy('rfqs.req_id','desc')
        ->paginate(10);
        //dd($rfq_list);
		if(!empty($rfq_list) && count($rfq_list)>0)
		{
			foreach($rfq_list as $rfq)
			{
				$rfq_count =DB::table('rfq_details')->where('rfq_id', '=',$rfq->id)->selectRaw("count('*') as no")->first();
				if(!empty($rfq_count) && count($rfq_count)>0)
				$rfq->rfq_count=$rfq_count->no;
				
				$rfq_quetes =DB::table('rfq_quetes')->where('rfq_id', '=',$rfq->id)->where('vendor_id', '=',$rfq->vendor_id)->orderBy('created_at','desc')->get();
				if(!empty($rfq_quetes) && count($rfq_quetes)>0)
				$rfq->rfq_quetes=$rfq_quetes;
				
				if(!empty($rfq_quetes) && count($rfq_quetes)>0)
				{
					foreach($rfq_quetes as $quetes)
					{
						$rfq_comments =DB::table('rfq_comments')->where('quete_id', '=',$quetes->id)->orderBy('created_at','desc')->get();
						if(!empty($rfq_comments) && count($rfq_comments)>0)
						$quetes->rfq_comments=$rfq_comments;
					}
				}
				
		
			}
		}
		
         //Filter Data
		$data['req_id_list'] = DB::table('rfqs')->select('req_id')->where('status','=',1)->groupBy('req_id')->orderBy('req_id','desc')->get();
		$data['cat1_list'] = DB::table('products')->select('cat1')->groupBy('cat1')->orderBy('cat1','desc')->get();
		$data['cat2_list'] = DB::table('products')->select('cat2')->groupBy('cat2')->orderBy('cat2','desc')->get();
		$data['vendor_name_list'] =DB::table('rfqs')
        ->join('rfq_details', 'rfqs.id', '=', 'rfq_details.rfq_id') 
		->join('users', 'rfq_details.vendor_id', '=', 'users.id') 
        ->select('users.id','users.name')        
        ->where('users.status','=',1)->orderBy('users.id','desc')->groupBy('users.id')->get();
		
		$data['rate_list'] = DB::table('products')->select('RATE')->groupBy('RATE')->orderBy('RATE','desc')->get();
		$data['quete_list'] = DB::table('rfq_quetes')->select('quete_id')->groupBy('quete_id')->orderBy('quete_id','desc')->get();
		
        #dd($filter_data); 
		
        return view('admin.vendor-all-rfq',$data)->with('rfq_list',$rfq_list);
    }


	public function filterRfqRequest(Request $request)
    {
		$req_id=$request->req_id;
		$cat1=$request->cat1;
		$cat2=$request->cat2;
		$name=$request->name;
		//$quote_id=$request->quote_id;
		$date=$request->date;
		$price=$request->price;
		
		$query_str=" rfqs.status = '1' ";
    	if(!empty($req_id))
    		$query_str .=" AND rfqs.req_id = '".$req_id."' ";
    	if(!empty($cat1))
    		$query_str .=" AND products.cat1 = '".$cat1."' ";
		if(!empty($cat2))
    		$query_str .=" AND products.cat2 = '".$cat2."' ";
		if(!empty($name))
    		$query_str .=" AND rfq_details.vendor_id = '".$name."' ";
    	if(!empty($date))
    		$query_str .=" AND rfqs.created_at <= '".$date."' ";
		if(!empty($price))
    		$query_str .=" AND products.RATE = '".$price."' ";
     
				
				
		$data['rfq_list'] = DB::table('rfqs')   
        ->join('products','rfqs.product_id', '=', 'products.id')
        ->join('rfq_details', 'rfqs.id', '=', 'rfq_details.rfq_id') 
		->join('users', 'rfq_details.vendor_id', '=', 'users.id') 
        ->select('rfqs.*','rfq_details.vendor_id','rfq_details.notify','users.name','products.cat1','products.cat2')        
        ->whereRaw($query_str)
		->orderBy('rfqs.req_id','desc')
        ->paginate(10);
        //dd($rfq_list);
		if(!empty($data['rfq_list']) && count($data['rfq_list'])>0)
		{
			foreach($data['rfq_list'] as $rfq)
			{
				$rfq_count =DB::table('rfq_details')->where('rfq_id', '=',$rfq->id)->selectRaw("count('*') as no")->first();
				if(!empty($rfq_count) && count($rfq_count)>0)
				$rfq->rfq_count=$rfq_count->no;
				
				$rfq_quetes =DB::table('rfq_quetes')->where('rfq_id', '=',$rfq->id)->where('vendor_id', '=',$rfq->vendor_id)->orderBy('created_at','desc')->get();
				if(!empty($rfq_quetes) && count($rfq_quetes)>0)
				$rfq->rfq_quetes=$rfq_quetes;
				
				if(!empty($rfq_quetes) && count($rfq_quetes)>0)
				{
					foreach($rfq_quetes as $quetes)
					{
						$rfq_comments =DB::table('rfq_comments')->where('quete_id', '=',$quetes->id)->orderBy('created_at','desc')->get();
						if(!empty($rfq_comments) && count($rfq_comments)>0)
						$quetes->rfq_comments=$rfq_comments;
					}
				}
				
		
			}
		}
		//dd($data);
        return view('admin.vendor-all-rfq-filter',$data);
	}
	
	
    public function rfqList()
    {
        $rfqList = Rfq::select(['rfqs.id', 'rfqs.created_at', 'rfqs.status', 'rfqs.req_id', 'products.cat1','products.cat2','products.cat3', 'products.image'])
                ->join('products','rfqs.product_id', '=', 'products.id')
                ->orderBy('rfqs.status','desc')
                ->orderBy('rfqs.created_at','desc')
                ->get();
                
            foreach ($rfqList as $eachRfq) {
                $vendor_ids = RfqDetails::select(['vendor_id'])->where('rfq_id', '=', $eachRfq->id)->get();
                //dd($vendor_ids);
                if(!empty($vendor_ids)){
                    foreach ($vendor_ids as $key => $eachVendor) {
                        $rfqList[$key]['vendor_details'] = User::select(['name'])->where('id', '=', $eachVendor->vendor_id)->first();
                    }
                }else{
                    $rfqList[$key]['vendor_details'] = 0;
                }
            }
            //dd($rfqList);
        return view('admin.rfq-list')->with('rfqList', $rfqList);
    }

    public function closeRequest($id)
    {
        $rfqs = Rfq::where('id','=', $id)->update(['status' =>0]);
        
        return Redirect::route('rfq-request');
    }

    public function getVendorList(Request $request)
    {
        $vendor_list = DB::table('rfq_quetes')
                    ->join('users', 'rfq_quetes.vendor_id', '=','users.id')
                    ->where('rfq_quetes.rfq_id', '=',$request->rfq_id)
                    ->groupBy('rfq_quetes.vendor_id')
                    ->select(['users.id', 'users.name'])->get();

        return $vendor_list;
    }

    public function closeRfq($id)
    {
        $rfqs = Rfq::where('id','=', $id)->update(['status' =>0]);

        return Redirect::route('rfq-list');
    }

    public function notifyVendor(Request $request)
    {   
        $rfqs = RfqDetails::where('vendor_id','=', trim($request->vendor_id))->where('rfq_id','=', trim($request->rfq_id))->update(['notify' =>1]);

        echo 'success'; exit;
    }
     
    public function addNewItem(Request $request)
    {
        $file = $request->file('file');

        $imageName = time().'_'.$request->file('file')->getClientOriginalName();
        if($file->move(public_path('upload/product'), $imageName)){
            $product = new Product;
            $product->image = $imageName;
            $product->cat1 = $request->cat1;
            $product->cat2 = $request->cat2;
			$product->cat3 = $request->cat3;
            $product->cat4 = $request->cat4;
            $product->save();
        }
        $product_details = Product::where('id','=',$product->id)->first();
        return $product_details;
    }

    public function getAddrfq()
    {   
		$vendor_list = User::select('id','name')->where('type','=','vendor')->where('status', '=', 1)->get();
    	$product_list = Product::select('id','image','cat1','cat2','cat3','cat4')->where('cat1', '<>','')->inRandomOrder()->limit(10)->get();
		$sample_list = Product::select('id','image')->where('cat1', '=','')->get();
		
		$cat1_list = Product::where('cat1', '<>','')->groupBy('cat1')->pluck('cat1');
		$cat2_list = Product::where('cat2', '<>','')->groupBy('cat2')->pluck('cat2');
		$cat3_list = Product::where('cat3', '<>','')->groupBy('cat3')->pluck('cat3');
		$cat4_list = Product::where('cat4', '<>','')->groupBy('cat4')->pluck('cat4');;
		
		#echo "<pre>";print_r($cat1_list);exit;
		
    	return view('admin.vendor-add-rfq')->with('allVendor', $vendor_list)->with('allProduct', $product_list)->with('sampleProduct', $sample_list)->with('cat1_list', $cat1_list)->with('cat2_list', $cat2_list)->with('cat3_list', $cat3_list)->with('cat4_list', $cat4_list);
    }
	
	
	
	public function getOptionFilter(Request $request)
    {
		
		$data['cat2_list'] = Product::select('cat2')->distinct('cat2')->where('cat1', '=', $request->search_str)->get();
		$data['cat3_list'] = Product::select('cat3')->distinct('cat3')->where('cat1', '=', $request->search_str)->get();
		$data['cat4_list'] = Product::select('cat4')->distinct('cat4')->where('cat1', '=', $request->search_str)->get();
		
        $html = view('admin.get-option-filter',$data);  
		return  $html;
    }
	
	public function searchItemByKeyword(Request $request)
    {
		if(!empty($request->selectcat1) || !empty($request->selectcat2) || !empty($request->selectcat3) || !empty($request->selectcat4))
		{
					$conds="";
					$i=0;
					if(!empty($request->selectcat1) && $request->selectcat1!='Select Cat1')
					{
						$conds .=" `cat1` LIKE '%".$request->selectcat1."%' ";
						$i++;	
					}
					if(!empty($request->selectcat2) && $request->selectcat2!='Select Cat2')
					{
						if(empty($i))
							$conds .=" `cat2` LIKE '%".$request->selectcat2."%' ";
						else
							$conds .=" AND `cat2` LIKE '%".$request->selectcat2."%' ";
						$i++;	
					}
					if(!empty($request->selectcat3) && $request->selectcat3!='Select Cat3')
					{
						if(empty($i))
							$conds .=" `cat3` LIKE '%".$request->selectcat3."%' ";
						else
							$conds .=" AND `cat3` LIKE '%".$request->selectcat3."%' ";
						$i++;	
					}
					if(!empty($request->selectcat4) && $request->selectcat4!='Select Cat4')
					{
						if(empty($i))
							$conds .=" `cat4` LIKE '%".$request->selectcat4."%' ";
						else
							$conds .=" AND `cat4` LIKE '%".$request->selectcat4."%' ";
						$i++;	
					}
					
					$product_list=Product::whereRaw($conds)->groupBy('cat1')->get();
		}elseif(!empty($request->search_str))
		{
			$product_list = Product::where('cat1', 'like', '%' . $request->search_str . '%')
			->orWhere('cat2', 'like', '%' . $request->search_str . '%')
			->orWhere('cat3', 'like', '%' . $request->search_str . '%')
			->orWhere('cat4', 'like', '%' . $request->search_str . '%')
			->groupBy('cat1')
			->get();
		}
		else
		{
			$product_list = Product::where('cat1', '<>','')->groupBy('cat1')->inRandomOrder()->limit(10)->get();
		}
		
		
		$html='';
		 if(!empty($product_list) && count($product_list)>0){
                                   foreach($product_list as $ext){
                                    $html .='<tr>';
                                    	$html .='<td class="text-center col-xs-2">';
 										
                                            $e_image=URL::to('/')."/img/pre-order-img.jpg";
                                            if(!empty($ext->image) && file_exists(public_path().'/upload/product/'.$ext->image))
                                            {
                                               $e_image= URL::to('/') .'/upload/product/'.$ext->image;
                                            }
                                       
                                        $html .='<img src="'.$e_image.'" title="small image" width="56" class="selectProductExistClassImage'.$ext->id.'">';
                                        $html .='</td>';
                                        $html .='<td class="text-center col-xs-2 selectProductExistClassCat1'.$ext->id.'">'.$ext->cat1.'</td>';
                                        $html .='<td class="text-center col-xs-2 selectProductExistClassCat2'.$ext->id.'">'.$ext->cat2.'</td>';
                                        $html .='<td class="text-center col-xs-2 selectProductExistClassCat3'.$ext->id.'">'.$ext->cat3.'</td>';
                                        $html .='<td class="text-center col-xs-2 selectProductExistClassCat4'.$ext->id.'">'.$ext->cat4.'</td>';
                                        $html .='<td class="text-center col-xs-2">';
                                        $html .='<a href="javascript:void(0);" product_id="'.$ext->id.'" class="btn btn-success selectProductExist" style="width:56px; padding:3px 0; font-size:12px;">Select</a>';
                                        $html .='</td>';
                                    $html .='</tr>';
								   }
		 }
                                    else
									{
                                     $html .='<tr>';
                                    	$html .='<td class="text-center col-xs-12" colspan="6">Product Not Available!</td>';
                                    $html .='</tr>';
									}
                                    
    	return $html;
    }
    public function postAddrfq(Request $request)
    {
        $user = Auth::user();

        $req_id= $this->generateReq();
        $rfq = new Rfq;
        $rfq->req_id = $req_id;
        $rfq->product_id = $request->product_id;
		$rfq->remarks = $request->comment;
		
        $rfq->createdby = $user->id;
        $rfq->modifiedby = $user->id;
        $rfq->status = 1;
        if($rfq->save()){
            $vendor_id = $request->vendor_id;
            $shipment_provider = $request->shipment_provider;
			$remarks_indv = $request->remarks_indv;
            $docket_no = $request->docket_no;
            foreach ($vendor_id as $key=>$eachVendor) {
                $rfq_details= new RfqDetails;
                $rfq_details->rfq_id = $rfq->id; 
                $rfq_details->vendor_id = $eachVendor; 
                $rfq_details->shipment_provider = $shipment_provider[$key];
                $rfq_details->docket_no = $docket_no[$key];
				$rfq_details->shipment_provider = $shipment_provider[$key];
				$rfq_details->indv_remarks = $remarks_indv[$key];
                $rfq_details->createdby = $user->id;
                $rfq_details->modifiedby = $user->id;
                $rfq_details->status = 1;
                $rfq_details->save();
            }
        }
        return Redirect::route('rfq-request');
    }

    static function generateReq()
    {
        $last_req = Rfq::selectRaw('max(id) as id')->first();
        $req_id=str_pad($last_req->id+1, 6, '0', STR_PAD_LEFT);
        $req_id = 'REQ/'.$req_id;
        return $req_id;
    }

    public function adminCommant($id)
    {
        //dd($id);
        $quoteComments = Comments::where('quete_id', '=', $id)->orderBy('id','asc')->get();
        //dd($quoteComments);
        return view('admin.vendor-comments')->with('comments_list',$quoteComments)->with('quotation_id', $id);
    }

    public function adminAddCommant(Request $request)
    {
        $return=0;
        
        $quotation['quete_id'] = $request->quotation_id;
        $quotation['user_id'] = Auth::user()->id;   
        $quotation['comments'] = $request->comments;
        $quotation['created_at'] = date('Y-m-d H:i:s');
        $quotation['updated_at'] = date('Y-m-d H:i:s');
                
        if(DB::table('rfq_comments')->insert($quotation))
        {
            $return=1;
        }
        echo $return;
    }

     

}
