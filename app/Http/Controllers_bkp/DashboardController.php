<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use auth;
use DB;
class DashboardController extends Controller
{
    public function adminDashboard()
    {
    	$user=auth::user();
    	if($user)
    	{	    	
	        return view('admin.dashboard');	    	
	    }else
	    {
	    	return Redirect('/');
	    }
    }

    public function vendorDashboard() //vendor.dashboard
    {
		
		//$this->getInvItem()//Fetch Data From V_INVITEM table
		//$this->getPriceList()//Fetch Data From V_PUR_PRICE_CHART table
		//$this->getFinsl()//Fetch Data From V_FINSL table
		//$this->getPurOrdMain()//Fetch Data From V_PURORDMAIN table
		//$this->getInvItem()getPurOrdChg()//Fetch Data From V_PURORDCHG table
		//$this->getPurOrdDetDelivery()//Fetch Data From V_PURORDDET_DELIVERY table
		//$this->getPurRtMain()//Fetch Data From V_PURRTMAIN table
		//$this->getPurRtChg()//Fetch Data From V_PURRTCHG table
		//$this->getPurRtChgItem ()//Fetch Data From V_PURRTCHG_ITEM  table
		
		
    	$user=auth::user();
    	if($user)
    	{
			 	
	        return view('vendor.dashboard');   	
	    }else
	    {
	    	return Redirect('/');
	    }
    }

    

   
}
