<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Redirect;

class VendorRequestController extends Controller
{
    public function viewAllRequest()
    {
        $request_list = DB::table('po_action')        
        ->select('po_action.*')
        ->where('po_action.user_id', '=',Auth::user()->id)
        ->orderBy('priority','asc') 
        ->orderBy('created_at','desc')         
        ->paginate(10);
        $po_list = DB::table('po_action')        
        ->select('po_no')
        ->distinct('po_no') 
        ->where('po_action.user_id', '=',Auth::user()->id)               
        ->get(); 
      if(!empty($request_list) && count($request_list)>0)
        {
            foreach($request_list as $reqst)
            {
                $req_comments =DB::table('request_comments')->where('action_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
                        if(!empty($req_comments) && count($req_comments)>0)
                        $reqst->req_comments=$req_comments;
                }
            }
       // dd($request_list);
    	return view('vendor.vendor-all-request')->with('request_list',$request_list)->with('po_list',$po_list);
    }
     public function sampleRequest()
    {
        $user = Auth::user();
        $purchaseOrderList = DB::table('purchase_order')->select(['purchase_order.ORDNO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY'])
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
        ->orderBy('purchase_order.ORDDT', 'desc')->paginate(30);
		
		//Filter Data PO
		$data['po_number_list'] =DB::table('purchase_order')->select('purchase_order.ORDNO')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->groupBy('purchase_order.ORDNO')
        ->orderBy('purchase_order.ORDNO', 'desc')->get(); 
		
		//Filter Data Cat1
		$data['cat1_list'] =DB::table('purchase_order')->select('products.cat1')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->groupBy('products.cat1')
        ->orderBy('products.cat1', 'desc')->get(); 
		
		//Filter Data Cat1
		$data['cat2_list'] =DB::table('purchase_order')->select('products.cat2')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->groupBy('products.cat2')
        ->orderBy('products.cat2', 'desc')->get(); 
		
		//Filter Data Ordr Qty
		$data['order_qty_list'] =DB::table('purchase_order')->select('V_PURORDDET.ORDQTY')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->groupBy('V_PURORDDET.ORDQTY')
        ->orderBy('V_PURORDDET.ORDQTY', 'asc')->get(); 
		
		
        return view('vendor.vendor-sample-request',$data)->with('purchaseOrderList', $purchaseOrderList);
    }
	
	
	public function filterSampleRequest(Request $request)
    {
		
		$po_number=$request->po_number;
		$category1=$request->category1;
		$category2=$request->category2;
		$order_qty=$request->order_qty;
		
		$query_str=" purchase_order.status = 'OPEN' ";
    	if(!empty($po_number))
    		$query_str .=" AND purchase_order.ORDNO = '".$po_number."' ";
    	if(!empty($category1))
    		$query_str .=" AND products.cat1 = '".$category1."' ";
		if(!empty($category2))
    		$query_str .=" AND products.cat2 = '".$category2."' ";
		if(!empty($order_qty))
    		$query_str .=" AND V_PURORDDET.ORDQTY = '".$order_qty."' ";
			
			
        $user = Auth::user();
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
       // ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->whereRaw($query_str)
        ->orderBy('purchase_order.ORDDT', 'desc')->paginate(30);		
		
        return view('vendor.vendor-sample-request-filter')->with('purchaseOrderList', $purchaseOrderList);
    }
	
	
	
    public function viewRateChange()
    {
        $user = Auth::user();
        $purchaseOrderList = DB::table('purchase_order')->select(['purchase_order.ORDNO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY'])
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
        ->orderBy('purchase_order.ORDDT', 'desc')->paginate(30);
            //dd($purchaseOrderList);
			
		//Filter Data PO
		$data['po_number_list'] =DB::table('purchase_order')->select('purchase_order.ORDNO')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->groupBy('purchase_order.ORDNO')
        ->orderBy('purchase_order.ORDNO', 'desc')->get(); 
		
		//Filter Data Cat1
		$data['cat1_list'] =DB::table('purchase_order')->select('products.cat1')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->groupBy('products.cat1')
        ->orderBy('products.cat1', 'desc')->get(); 
		
		//Filter Data Cat1
		$data['cat2_list'] =DB::table('purchase_order')->select('products.cat2')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->groupBy('products.cat2')
        ->orderBy('products.cat2', 'desc')->get(); 
		
		//Filter Data Ordr Qty
		$data['order_qty_list'] =DB::table('purchase_order')->select('V_PURORDDET.ORDQTY')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->groupBy('V_PURORDDET.ORDQTY')
        ->orderBy('V_PURORDDET.ORDQTY', 'asc')->get(); 
		
    	return view('vendor.vendor-request-rate-change',$data)->with('purchaseOrderList', $purchaseOrderList);
    }
	
	
	
	
	public function filterRateChange(Request $request)
    {
		
		$po_number=$request->po_number;
		$category1=$request->category1;
		$category2=$request->category2;
		$order_qty=$request->order_qty;
		
		$query_str=" purchase_order.status = 'OPEN' ";
    	if(!empty($po_number))
    		$query_str .=" AND purchase_order.ORDNO = '".$po_number."' ";
    	if(!empty($category1))
    		$query_str .=" AND products.cat1 = '".$category1."' ";
		if(!empty($category2))
    		$query_str .=" AND products.cat2 = '".$category2."' ";
		if(!empty($order_qty))
    		$query_str .=" AND V_PURORDDET.ORDQTY = '".$order_qty."' ";
			
			
        $user = Auth::user();
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
       // ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->whereRaw($query_str)
        ->orderBy('purchase_order.ORDDT', 'desc')->paginate(30);		
		
        return view('vendor.vendor-request-rate-change-filter')->with('purchaseOrderList', $purchaseOrderList);
    }
	
    public function viewRequestCutting()
    {
        $user = Auth::user();
        $purchaseOrderList = DB::table('purchase_order')->select(['purchase_order.ORDNO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY'])
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
        ->orderBy('purchase_order.ORDDT', 'desc')->paginate(30);
        
		//Filter Data PO
		$data['po_number_list'] =DB::table('purchase_order')->select('purchase_order.ORDNO')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->groupBy('purchase_order.ORDNO')
        ->orderBy('purchase_order.ORDNO', 'desc')->get(); 
		
		//Filter Data Cat1
		$data['cat1_list'] =DB::table('purchase_order')->select('products.cat1')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->groupBy('products.cat1')
        ->orderBy('products.cat1', 'desc')->get(); 
		
		//Filter Data Cat1
		$data['cat2_list'] =DB::table('purchase_order')->select('products.cat2')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->groupBy('products.cat2')
        ->orderBy('products.cat2', 'desc')->get(); 
		
		//Filter Data Ordr Qty
		$data['order_qty_list'] =DB::table('purchase_order')->select('V_PURORDDET.ORDQTY')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->groupBy('V_PURORDDET.ORDQTY')
        ->orderBy('V_PURORDDET.ORDQTY', 'asc')->get(); 
		
		
    	return view('vendor.vendor-cutting-request',$data)->with('purchaseOrderList', $purchaseOrderList);
    }
	
	public function filterRequestCutting(Request $request)
    {
		
		$po_number=$request->po_number;
		$category1=$request->category1;
		$category2=$request->category2;
		$order_qty=$request->order_qty;
		
		$query_str=" purchase_order.status = 'OPEN' ";
    	if(!empty($po_number))
    		$query_str .=" AND purchase_order.ORDNO = '".$po_number."' ";
    	if(!empty($category1))
    		$query_str .=" AND products.cat1 = '".$category1."' ";
		if(!empty($category2))
    		$query_str .=" AND products.cat2 = '".$category2."' ";
		if(!empty($order_qty))
    		$query_str .=" AND V_PURORDDET.ORDQTY = '".$order_qty."' ";
			
			
        $user = Auth::user();
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
       // ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->whereRaw($query_str)
        ->orderBy('purchase_order.ORDDT', 'desc')->paginate(30);		
		
        return view('vendor.vendor-cutting-request-filter')->with('purchaseOrderList', $purchaseOrderList);
    }
	
    public function viewWidthChanges()
    {
         $user = Auth::user();
        $purchaseOrderList = DB::table('purchase_order')->select(['purchase_order.ORDNO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY'])
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
        ->orderBy('purchase_order.ORDDT', 'desc')->paginate(30);
            //dd($purchaseOrderList);
			
		//Filter Data PO
		$data['po_number_list'] =DB::table('purchase_order')->select('purchase_order.ORDNO')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->groupBy('purchase_order.ORDNO')
        ->orderBy('purchase_order.ORDNO', 'desc')->get(); 
		
		//Filter Data Cat1
		$data['cat1_list'] =DB::table('purchase_order')->select('products.cat1')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->groupBy('products.cat1')
        ->orderBy('products.cat1', 'desc')->get(); 
		
		//Filter Data Cat1
		$data['cat2_list'] =DB::table('purchase_order')->select('products.cat2')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->groupBy('products.cat2')
        ->orderBy('products.cat2', 'desc')->get(); 
		
		//Filter Data Ordr Qty
		$data['order_qty_list'] =DB::table('purchase_order')->select('V_PURORDDET.ORDQTY')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->groupBy('V_PURORDDET.ORDQTY')
        ->orderBy('V_PURORDDET.ORDQTY', 'asc')->get(); 
		
    	return view('vendor.vendor-width-change',$data)->with('purchaseOrderList', $purchaseOrderList);
    }
	
	public function filterWidthChanges(Request $request)
    {
		
		$po_number=$request->po_number;
		$category1=$request->category1;
		$category2=$request->category2;
		$order_qty=$request->order_qty;
		
		$query_str=" purchase_order.status = 'OPEN' ";
    	if(!empty($po_number))
    		$query_str .=" AND purchase_order.ORDNO = '".$po_number."' ";
    	if(!empty($category1))
    		$query_str .=" AND products.cat1 = '".$category1."' ";
		if(!empty($category2))
    		$query_str .=" AND products.cat2 = '".$category2."' ";
		if(!empty($order_qty))
    		$query_str .=" AND V_PURORDDET.ORDQTY = '".$order_qty."' ";
			
			
        $user = Auth::user();
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
       // ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->whereRaw($query_str)
        ->orderBy('purchase_order.ORDDT', 'desc')->paginate(30);		
		
        return view('vendor.vendor-width-change-filter')->with('purchaseOrderList', $purchaseOrderList);
    }
	
    public function viewVendorDateExt()
    {
        $user = Auth::user();
        $purchaseOrderList = DB::table('purchase_order')->select(['purchase_order.ORDNO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY'])
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
        ->orderBy('purchase_order.ORDDT', 'desc')->paginate(30);
		
		//Filter Data PO
		$data['po_number_list'] =DB::table('purchase_order')->select('purchase_order.ORDNO')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->groupBy('purchase_order.ORDNO')
        ->orderBy('purchase_order.ORDNO', 'desc')->get(); 
		
		//Filter Data Cat1
		$data['cat1_list'] =DB::table('purchase_order')->select('products.cat1')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->groupBy('products.cat1')
        ->orderBy('products.cat1', 'desc')->get(); 
		
		//Filter Data Cat1
		$data['cat2_list'] =DB::table('purchase_order')->select('products.cat2')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->groupBy('products.cat2')
        ->orderBy('products.cat2', 'desc')->get(); 
		
		//Filter Data Ordr Qty
		$data['order_qty_list'] =DB::table('purchase_order')->select('V_PURORDDET.ORDQTY')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->groupBy('V_PURORDDET.ORDQTY')
        ->orderBy('V_PURORDDET.ORDQTY', 'asc')->get(); 
		
        return view('vendor.vendor-date-extention',$data)->with('purchaseOrderList', $purchaseOrderList);
    }
	
	public function filterVendorDateExt(Request $request)
    {
		
		$po_number=$request->po_number;
		$category1=$request->category1;
		$category2=$request->category2;
		$order_qty=$request->order_qty;
		
		$query_str=" purchase_order.status = 'OPEN' ";
    	if(!empty($po_number))
    		$query_str .=" AND purchase_order.ORDNO = '".$po_number."' ";
    	if(!empty($category1))
    		$query_str .=" AND products.cat1 = '".$category1."' ";
		if(!empty($category2))
    		$query_str .=" AND products.cat2 = '".$category2."' ";
		if(!empty($order_qty))
    		$query_str .=" AND V_PURORDDET.ORDQTY = '".$order_qty."' ";
			
			
        $user = Auth::user();
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
       // ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->whereRaw($query_str)
        ->orderBy('purchase_order.ORDDT', 'desc')->paginate(30);		
		
        return view('vendor.vendor-date-extention-filter')->with('purchaseOrderList', $purchaseOrderList);
    }
	
	
    public function viewVendorCalcellation()
    {
        $user = Auth::user();
        $purchaseOrderList = DB::table('purchase_order')->select(['purchase_order.ORDNO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY'])
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
        ->orderBy('purchase_order.ORDDT', 'desc')->paginate(30);
		
		//Filter Data PO
		$data['po_number_list'] =DB::table('purchase_order')->select('purchase_order.ORDNO')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->groupBy('purchase_order.ORDNO')
        ->orderBy('purchase_order.ORDNO', 'desc')->get(); 
		
		//Filter Data Cat1
		$data['cat1_list'] =DB::table('purchase_order')->select('products.cat1')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->groupBy('products.cat1')
        ->orderBy('products.cat1', 'desc')->get(); 
		
		//Filter Data Cat1
		$data['cat2_list'] =DB::table('purchase_order')->select('products.cat2')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->groupBy('products.cat2')
        ->orderBy('products.cat2', 'desc')->get(); 
		
		//Filter Data Ordr Qty
		$data['order_qty_list'] =DB::table('purchase_order')->select('V_PURORDDET.ORDQTY')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->groupBy('V_PURORDDET.ORDQTY')
        ->orderBy('V_PURORDDET.ORDQTY', 'asc')->get(); 

        return view('vendor.vendor-calcellation',$data)->with('purchaseOrderList', $purchaseOrderList);
    }
	
	public function filterVendorCalcellation(Request $request)
    {
		
		$po_number=$request->po_number;
		$category1=$request->category1;
		$category2=$request->category2;
		$order_qty=$request->order_qty;
		
		$query_str=" purchase_order.status = 'OPEN' ";
    	if(!empty($po_number))
    		$query_str .=" AND purchase_order.ORDNO = '".$po_number."' ";
    	if(!empty($category1))
    		$query_str .=" AND products.cat1 = '".$category1."' ";
		if(!empty($category2))
    		$query_str .=" AND products.cat2 = '".$category2."' ";
		if(!empty($order_qty))
    		$query_str .=" AND V_PURORDDET.ORDQTY = '".$order_qty."' ";
			
			
        $user = Auth::user();
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
       // ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->whereRaw($query_str)
        ->orderBy('purchase_order.ORDDT', 'desc')->paginate(30);		
		
        return view('vendor.vendor-calcellation-filter')->with('purchaseOrderList', $purchaseOrderList);
    }
    public function viewVendorcreateSupportTicket()
    {
        $po_list = DB::table('purchase_order')
        ->select('id','ORDCODE')      
        
        ->get();
        //dd($po_list);
        return view('vendor.vendor-create-support-ticket')->with('po_list',$po_list);
    }
    public function addSupportRequest(Request $request)
    {
        $return=0;
        
        
        $support['user_id'] = Auth::user()->id;
        $support['po_no'] = $request->order_id;   
        $support['action_type'] = 'others';
        $support['remarks'] = $request->remks;   
        $support['created_at'] = date('Y-m-d H:i:s');
        $support['updated_at'] = date('Y-m-d H:i:s');
                
        if(DB::table('po_action')->insert($support))
        {
            $return=1;
        }
        echo $return;
       
    }

     public function filterAction(Request $request)
    {
        /*--Filter By Type*/
      $user_type=Auth::user()->type;      
      $status=$request->status;
        $type=$request->type;
        $po_no=$request->po_no;
        $query="action_type != ''";
        if(!empty($type))
            $query .=" AND action_type = '".$type."' ";
        if(!empty($po_no))
            $query .=" AND po_no = '".$po_no."' ";
      if(!empty($status))
        $query .=" AND status = '".$status."' ";
         if($user_type=='vendor'){
                $data['request_list'] = DB::table('po_action')  
                ->select('po_action.*')
                ->where('user_id', '=',Auth::user()->id) 
                ->whereRaw($query)
                ->orderBy('priority','asc') 
                ->orderBy('created_at','desc')              
                ->paginate(10);
        }
        else{
          $data['request_list'] = DB::table('po_action')  
            ->select('po_action.*')           
            ->whereRaw($query)
            ->orderBy('priority','asc') 
            ->orderBy('created_at','desc')              
            ->paginate(10);
        }
         if(!empty($data['request_list']) && count($data['request_list'])>0)
        {
            foreach($data['request_list'] as $reqst)
            {
                $req_comments =DB::table('request_comments')->where('action_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
                        if(!empty($req_comments) && count($req_comments)>0)
                        $reqst->req_comments=$req_comments;
                }
            }
        //dd($request_list);
  //       $html='';
  //       $inc=0;
  //       if(!empty($request_list) && count($request_list)){
  //        foreach ($request_list as $req) {
  //                                        $type='';
  //                                        if($req->action_type=='cutting')
  //                                         $type='Request For Cutting';
  //                                         elseif($req->action_type=='rate_change') 
  //                                          $type='Request For Rate Change';
  //                                         elseif($req->action_type=='width_change') 
  //                                          $type='Request For Width Change';
  //                                         elseif($req->action_type=='date_ext') 
  //                                          $type='Request For Date Extention';
  //                                         elseif($req->action_type=='cancel') 
  //                                          $type='Request For Cancellation';
  //                                         elseif($req->action_type=='others') 
  //                                          $type='Request For Others';
  //                       $inc++;

  //                                    $html .=' <tr>
  //                                     <td class="text-center">'.$inc.'</td>
  //                                    <td class="text-center">'.$req->po_no.'</td>
  //                                    <td class="text-center">26/09/2016</td>
  //                                       <td class="text-center">'.$type.'</td>
  //                                       <td class="text-center">--</td>
  //                                       <td class="text-center">--</td>
  //                                       <td class="text-center">--</td>
  //                                       <td class="text-center"><span class="sm-btn btn-primary btn-xs">New</span></td>
  //                                       <td class="text-center">Lorem Ipsum... <a href="vendor-comments-2.php" class="btn btn-warning btn-xs">Details</a></td>
  //                                       <td class="text-center"><a href="javascript:void(0)" class="btn btn-success">Accpet</a></td>
  //                                   </tr>';

  //                               }
  //                           }
  //                           else{
  //                            $html .=' <tr>
  //                                    <td class="text-center" colspan="10">No Result Found</td>
  //                                   </tr>';


  //       }

  // echo $html;
        
  //   }

  return view('vendor.vendor-all-request-filter',$data); 
    }
     public function requestWiseComments($id)
    {
        //dd($id);
        $Comments = DB::table('request_comments')
                  ->select('*')
                  ->where('action_id', '=', $id)->orderBy('created_at')->get();
        //dd($Comments);
        return view('vendor.requestwise-comments')->with('comments_list',$Comments)->with('action_id', $id);
       
    }
     public function addRequestComments(Request $request)
    {
        $return=0;
        
        $action['action_id'] = $request->action_id;
        $action['user_id'] = Auth::user()->id;   
        $action['comments'] = $request->comments;
        $action['created_at'] = date('Y-m-d H:i:s');
        $action['updated_at'] = date('Y-m-d H:i:s');
                
        if(DB::table('request_comments')->insert($action))
        {
            $return=1;
        }
        echo $return;
    }
    public function acceptRequest($id)
    {
        // $reqs = DB::table('po_action')->where('id','=', $id)->update(['status' =>'close']);

        // return Redirect::route('all-requst-vendor');

        $return=0;
       
        $quotation['status'] = 'close';
        $quotation['priority'] = '4';        
               
        if(DB::table('po_action')->where('id','=', $id)->update($quotation))
        {
            return Redirect::route('all-requst-vendor');
        }
        echo $return;
    }

   public function rejectRequest($id)
    {
        $reqs = DB::table('po_action')->where('id','=', $id)->update(['status' =>'reject']);

        return Redirect::route('all-requst-vendor');
    }

}
