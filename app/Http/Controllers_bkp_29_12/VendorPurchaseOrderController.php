<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

class VendorPurchaseOrderController extends Controller
{
    public function index()
    {
    	return view('admin.purchase-order-list');
    }
	
	
    public function purchaseOrderlistview()//Vendor Purchase Order Listing Section
    {   
        $user = Auth::user();
		$purchaseOrderList = DB::table('purchase_order')->select(['purchase_order.ORDNO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY'])
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
        ->orderBy('purchase_order.ORDDT', 'desc')->paginate(30);
		
		//Filter Data
		$data['po_number_list'] = DB::table('purchase_order')->select('ORDNO')->where('status','=','OPEN')->where('PCODE','=',$user->SLCODE)->groupBy('ORDNO')->orderBy('ORDNO','desc')->get();

        $data['cat1_list']=DB::table('purchase_order')->select('products.cat1')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
        ->groupBy('products.cat1')
        ->orderBy('products.cat1', 'desc')->get();

        $data['cat2_list']=DB::table('purchase_order')->select('products.cat2')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
        ->groupBy('products.cat2')
        ->orderBy('products.cat2', 'desc')->get();


		//$data['cat1_list'] = DB::table('products')->select('cat1')->groupBy('cat1')->orderBy('cat1','desc')->get();


		//$data['cat2_list'] = DB::table('products')->select('cat2')->groupBy('cat2')->orderBy('cat2','desc')->get();
		
    	return view('vendor.vendor-purchase-order-list',$data)->with('purchaseOrderList', $purchaseOrderList);
    }
	
	 public function filterPurchaseOrder(Request $request)
    {
    	$user = Auth::user();
		$po_number=$request->po_number;
		$category1=$request->category1;
		$category2=$request->category2;
		$due_from=$request->due_from;
		$due_to=$request->due_to;
		
		$query_str=" purchase_order.status = 'OPEN' ";
    	if(!empty($po_number))
    		$query_str .=" AND purchase_order.ORDNO = '".$po_number."' ";
    	if(!empty($category1))
    		$query_str .=" AND products.cat1 = '".$category1."' ";
		if(!empty($category2))
    		$query_str .=" AND products.cat2 = '".$category2."' ";
		if(!empty($due_from))
    		$query_str .=" AND purchase_order.ORDDT >= '".$due_from."' ";
    	if(!empty($due_to))
    		$query_str .=" AND purchase_order.ORDDT <= '".$due_to."' ";
			
    	$purchaseOrderList = DB::table('purchase_order')->select(['purchase_order.ORDNO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY'])
    	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
    	->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
		->whereRaw($query_str)
    	->orderBy('purchase_order.ORDDT', 'desc')->paginate(30);
		
	
    	return view('vendor.vendor-purchase-order-list-filter')->with('purchaseOrderList', $purchaseOrderList);
    }
    public function purchaseOrderRequest(Request $request)
    {
        //dd($request);
        $return=0;
        
        $porequest['user_id']=Auth::user()->id;  
        $porequest['po_no']= $request->po_no;
        $porequest['created_at']= date('Y-m-d H:i:s');
        $porequest['updated_at']= date('Y-m-d H:i:s');
        $porequest['status']= 'new';
		
        if($request->type=='cutting'){        
        $porequest['action_type'] = $request->type;
        $porequest['remarks'] = $request->remarks;
        }

        if($request->type=='rate_change'){
        $porequest['old_value'] = $request->old_rate;        
        $porequest['action_type'] = $request->type;
        $porequest['Reason'] = $request->reason;
        $porequest['new_rate'] = $request->new_rate;
        }

        if($request->type=='width_change'){ 
        $porequest['old_value'] = $request->old_rate;       
        $porequest['action_type'] = $request->type;
        $porequest['Reason'] = $request->reason;
        $porequest['new_rate'] = $request->new_rate;
        $porequest['new_width'] = $request->new_width;
        }

        if($request->type=='date_ext'){        
        $porequest['action_type'] = $request->type;
        $porequest['Reason'] = $request->reason;
        $porequest['ext_date'] = $request->ext_date;       
        }

        if($request->type=='cancel'){        
        $porequest['action_type'] = $request->type;
        $porequest['Reason'] = $request->reason;
        $porequest['cancel_qty'] = $request->can_qty;       
        }

        if($request->type=='others'){        
        $porequest['action_type'] = $request->type;
        $porequest['Reason'] = $request->reason;               
        }

        if($request->type=='sample'){        
        $porequest['action_type'] = $request->type;
        $porequest['Reason'] = $request->reason;               
        }


        if(DB::table('po_action')->insert($porequest))
        {
            $return=1;
        }
        echo $return;
    }
}
