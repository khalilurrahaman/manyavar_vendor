<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Models\Department;
use DB;
use Redirect;

class AdminDepartmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list=Department::orderBy('id')->get();
        return view('admin.departmen-list')->with('list',$list);
    }
    public function getEditData(Request $request)
    {
        $id=$request->id;
        $data=Department::where('id','=', $id)->first();
        return $data;
    }
    
    public function addDepartment(Request $request)
    {
        $user=\Auth::user();
        $this->validate($request,[
        'name'=>'required',        
        'description'=>'required'
        ]);

        $name=trim($request->name);        
        $description=$request->description;        
        /*$createdby=$user->id;
        $modifiedby=$user->id;*/
        $createdby=3;
        $modifiedby=3;


        $department=new Department;
        $department->name=$name; 
        $department->description=$description;      
        $department->createdby=$createdby;
        $department->modifiedby=$modifiedby;
        if($department->save())
        {
            return redirect::route('departmnt-lising');
                  
        }
        else
        {
            return redirect::route('departmnt-lising');
        }

    }

    public function updateDepartment(Request $request)
    {

        $user=\Auth::user();
        $this->validate($request,[
        'name'=>'required',        
        'description'=>'required'
        ]);
        $id=$request->id;
        $name=trim($request->name);        
        $description=$request->description;        
        //$modifiedby=$user->id;
        $modifiedby=3;

        $data=Department::where('id','=',$id)->first();
        $data->name=$name;
        $data->description=$description;
        $data->modifiedby=$modifiedby;
        if($data->save())
        {
            return redirect()->back();
        }
        else
        {
            return redirect()->back();
        }

        /*$data=\DB::table('departments')->where('id','=',$id)->update(
            array(
                'name' => $name,                
                'description' => $description,                
                'modifiedby' => $modifiedby          
               
                ));*/
        
        
    }
    public function deleteDepartment(Request $request)
    {
        $id=$request->id;
        $data=Department::where('id','=',$id);
        if($data->delete())
        {
            return redirect()->back();
        }
    }
}
