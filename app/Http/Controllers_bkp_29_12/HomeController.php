<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function userLogin()
    {
        if(auth::user())
        {
            $user=auth::user();
           
            if($user->type == "vendor")
            {
                 return redirect('vendor/dashboard');
            }
            else {
                 return redirect('admin/dashboard');
            }
           
        }
        else        
            return view('auth.login');
    }

    public function userLogout()
    {
        if(auth::user())
        {
            auth::logout();
        }
        return redirect('/');
    }



    
}
