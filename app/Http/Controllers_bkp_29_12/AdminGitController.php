<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Redirect;
class AdminGitController extends Controller
{
    public function viewGit()
    {

		$gitList = DB::table('git')->selectRaw('git.id,users.name,products.cat1,products.cat2,products.cat3,V_PURORDDET.ORDQTY,V_PURORDDET.RCQTY,V_PURORDDET.CNLQTY,V_PURORDDET.OQTY,git.git_qty,git.git_date,git.git_qty,git.updated_at,git.status,git.challan_no,(select count(*) from git g where g.challan_no=git.challan_no ) as rowspan')
    	->leftJoin('V_PURORDDET', 'git.ORDCODE', '=', 'V_PURORDDET.ORDCODE' ,'and','git.ICODE', '=', 'V_PURORDDET.ICODE')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
    	->join('users', 'purchase_order.PCODE', '=', 'users.SLCODE')
    	->where('git.status', '!=','Close')
    	->orderBy('git.updated_at', 'desc')
		->paginate(30);
        

        if(!empty($gitList) && count($gitList)>0)
        {
            foreach($gitList as $reqst)
            {
                $git_comments =DB::table('git_comments')->where('git_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
                        if(!empty($git_comments) && count($git_comments)>0)
                        $reqst->git_comments=$git_comments;
                }
            }

		//dd($gitList);
    	return view('admin.vendor-all-git-list')->with('gitList',$gitList);
    }

    public function markCompleteGit()
    {

    	return Redirect::route('all-git-list');
    }
    public function viewAllVendorGit()
    {
    	return view('vendor.vendor-all-git');
    }

    public function adminGitComment($id)
    {
       echo 1; exit;
        $Comments = DB::table('git_comments')
                  ->select('*')
                  ->where('git_id', '=', $id)->orderBy('created_at','desc')->get();
        //dd($Comments);
        return view('admin.admin-git-comments')->with('comments_list',$Comments)->with('git_id', $id);
       
    }
     public function addGitComment(Request $request)
    {
        $return=0;
        
        $action['git_id'] = $request->git_id;
        $action['user_id'] = Auth::user()->id;   
        $action['comments'] = $request->comments;
        $action['created_at'] = date('Y-m-d H:i:s');
        $action['updated_at'] = date('Y-m-d H:i:s');
                
        if(DB::table('git_comments')->insert($action))
        {
            $return=1;
        }
        echo $return;
    }
}
