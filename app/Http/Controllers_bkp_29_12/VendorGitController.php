<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use mpdf;
class VendorGitController extends Controller
{
    public function viewGit()
    {
    	return view('admin.vendor-all-git-list');
    }

    public function viewGrt()
    {
    	return view('admin.vendor-all-grt-list');
    }
	
	
    public function viewAllVendorGit()
    {
		
		$data['gitList'] = DB::table('git')->selectRaw('products.cat1,products.cat2,products.cat3,products.cat4,V_PURORDDET.ORDQTY,V_PURORDDET.RCQTY,V_PURORDDET.CNLQTY,V_PURORDDET.OQTY,git.*,(select count(*) from git g where g.per_git_id=git.per_git_id ) as rowspan')
		
    	->leftJoin('V_PURORDDET', 'git.ORDCODE', '=', 'V_PURORDDET.ORDCODE' ,'and','git.ICODE', '=', 'V_PURORDDET.ICODE')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
    	->orderBy('git.created_at', 'desc')
		->paginate(30);
		
		if(!empty($data['gitList']) && count($data['gitList'])>0)
        {
            foreach($data['gitList'] as $reqst)
            {
                $git_comments =DB::table('git_comments')->where('git_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
                        if(!empty($git_comments) && count($git_comments)>0)
                        $reqst->git_comments=$git_comments;
                }
            }
		
		
		
		//dd($data['gitList']);
		#echo "<pre>";print_r($data['gitList']);exit;
    	return view('vendor.vendor-all-git',$data);
    }
	
    public function viewAddVendorGit()
    {
		
		#echo Auth::user()->SLCODE;exit;
		
		
		$data['purchaseOrderList'] = DB::table('purchase_order')->select('purchase_order.ORDNO','products.image','products.cat1','products.cat2','products.cat3','products.cat4','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.ORDCODE','V_PURORDDET.ICODE','V_PURORDDET.CNLQTY','V_PURORDDET.OQTY',DB::raw('sum(git.git_qty) as git_qty'))
    	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->leftJoin('git', 'git.ORDCODE', '=', 'V_PURORDDET.ORDCODE' ,'and','git.ICODE', '=', 'V_PURORDDET.ICODE')
    	->where('purchase_order.status', '=','OPEN')
		->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
    	->orderBy('purchase_order.ORDDT', 'desc')
		->groupBy('V_PURORDDET.id')
		->get();
		
		
		$data['cat1_list'] = DB::table('purchase_order')->select('products.cat1')
    	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->leftJoin('git', 'git.ORDCODE', '=', 'V_PURORDDET.ORDCODE' ,'and','git.ICODE', '=', 'V_PURORDDET.ICODE')
    	->where('purchase_order.status', '=','OPEN')
		->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
    	->orderBy('products.cat1', 'desc')
		->groupBy('products.cat1')
		->get();
		
		$data['cat2_list'] = DB::table('purchase_order')->select('products.cat2')
    	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->leftJoin('git', 'git.ORDCODE', '=', 'V_PURORDDET.ORDCODE' ,'and','git.ICODE', '=', 'V_PURORDDET.ICODE')
    	->where('purchase_order.status', '=','OPEN')
		->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
    	->orderBy('products.cat2', 'desc')
		->groupBy('products.cat2')
		->get();
		
		$data['cat3_list'] = DB::table('purchase_order')->select('products.cat3')
    	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->leftJoin('git', 'git.ORDCODE', '=', 'V_PURORDDET.ORDCODE' ,'and','git.ICODE', '=', 'V_PURORDDET.ICODE')
    	->where('purchase_order.status', '=','OPEN')
		->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
    	->orderBy('products.cat3', 'desc')
		->groupBy('products.cat3')
		->get();
		
		$data['cat4_list'] = DB::table('purchase_order')->select('products.cat4')
    	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->leftJoin('git', 'git.ORDCODE', '=', 'V_PURORDDET.ORDCODE' ,'and','git.ICODE', '=', 'V_PURORDDET.ICODE')
    	->where('purchase_order.status', '=','OPEN')
		->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
    	->orderBy('products.cat4', 'desc')
		->groupBy('products.cat4')
		->get();
		
		
		
		/*$data['pending_qty_list'] =DB::table('purchase_order')->select(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY as p_qty'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->where('purchase_order.status', '=','OPEN')
        ->orderBy('p_qty')
		->distinct('p_qty')->get();*/
		#echo "<pre>";print_r($data['purchaseOrderList']);exit;
    	return view('vendor.vendor-add-git',$data);
    }
	
	public function filterAddGit(Request $request)
    {
		
		
		//$pending_qty=$request->pending_qty;
		$category1=$request->category1;
		$category2=$request->category2;
		$category3=$request->category3;
		$category4=$request->category4;
		
		$query_str=" purchase_order.status = 'OPEN' ";
    	//if(!empty($pending_qty))
    		//$query_str .=" AND pending_qty.ORDNO = '".$pending_qty."' ";
    	if(!empty($category1))
    		$query_str .=" AND products.cat1 = '".$category1."' ";
		if(!empty($category2))
    		$query_str .=" AND products.cat2 = '".$category2."' ";
		if(!empty($category3))
    		$query_str .=" AND products.cat3 = '".$category3."' ";
		if(!empty($category4))
    		$query_str .=" AND products.cat4 = '".$category4."' ";
			
			
			
		$data['purchaseOrderList'] = DB::table('purchase_order')->select('purchase_order.ORDNO','products.image','products.cat1','products.cat2','products.cat3','products.cat4','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.ORDCODE','V_PURORDDET.ICODE','V_PURORDDET.CNLQTY','V_PURORDDET.OQTY',DB::raw('sum(git.git_qty) as git_qty'))
    	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->leftJoin('git', 'git.ORDCODE', '=', 'V_PURORDDET.ORDCODE' ,'and','git.ICODE', '=', 'V_PURORDDET.ICODE')
    	//->where('purchase_order.status', '=','OPEN')
		->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
		->whereRaw($query_str)
    	->orderBy('purchase_order.ORDDT', 'desc')
		->groupBy('V_PURORDDET.id')
		->get();
		#echo "<pre>";print_r($data['purchaseOrderList']);exit;
    	return view('vendor.vendor-add-git-filter',$data);
    }
	
	public function submitGitRecords(Request $request)
    {
		
		$per_git_id= $this->generatePerGitId();
		
		for($i=0;$i<count($request->git_qty);$i++)
		{
			if(!empty($request->git_qty[$i]))
			{
				$git['per_git_id'] = $per_git_id;
				$git['ORDCODE'] = $request->ORDCODE[$i];//
				$git['ICODE'] = $request->ICODE[$i];//
				$git['SLCODE'] = Auth::user()->SLCODE;
				$git['git_qty'] = $request->git_qty[$i];//
				$git['challan_no'] = $request->challan_no;
				$git['lorry_no'] = $request->lorry_no;
				$git['transport_no'] = $request->transport_no;
				$git['git_date'] = $request->date;
				$git['created_at'] = date('Y-m-d H:i:s');
				$git['updated_at'] = date('Y-m-d H:i:s');
				$git['status'] = 'Pending';
				DB::table('git')->insert($git);
			}
		}
		
		
		return view('vendor.success-git')->with('per_git_id',$per_git_id);
		
    }	
	
	
	static function generatePerGitId()
    {
		$max_id=1;
        $row_result = DB::table('git')->selectRaw('max(per_git_id) as per_git_id')->first();
		if(!empty($row_result) && count($row_result)>0)
        $max_id=$row_result->per_git_id+1;
		
        return $max_id;
    }
	
	
	public function generateInvoice($per_git_id='')
    {
		
		$data['gitList'] = DB::table('git')->selectRaw('products.cat1,products.cat2,products.cat3,products.cat4,V_PURORDDET.ORDQTY,V_PURORDDET.RCQTY,V_PURORDDET.CNLQTY,V_PURORDDET.OQTY,git.*,(select count(*) from git g where g.challan_no=git.challan_no ) as rowspan')
    	->leftJoin('V_PURORDDET', 'git.ORDCODE', '=', 'V_PURORDDET.ORDCODE' ,'and','git.ICODE', '=', 'V_PURORDDET.ICODE')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
		->where('git.per_git_id', '=',$per_git_id)
    	->orderBy('git.created_at', 'desc')
		->get();
		
		$html='<!doctype html>
<html>
<body>
<div class="invoice-box">
  <table>
    <tr>
      <td class="title"><img src="'.asset('img/mmlsmall.png').'" style="width:100%; max-width:300px;"><br/>
        Lorem Ipsum simple dummy<br/>
        text Lorem Ipsum<br/>
        simple dummy text </td>
      <td><strong>Goods Return Note</strong></td>
    </tr>
  </table>
  <hr/>
  <table>
    <tr>
      <td class="title"> Lorem Ipsum simple dummy<br/>
        text Lorem Ipsum<br/>
        simple dummy text </td>
      <td>Created: January 1, 2015<br>
        Due: December 6, 2016</td>
    </tr>
  </table>
  <table cellpadding="0" cellspacing="0">
    <tr class="heading">
      <td>Item </td>
      <td>Name</td>
      <td>Price</td>
      <td>Qty.</td>
      <td>Amount</td>
      <td>Total</td>
    </tr>
    <tr class="item">
      <td><strong>FG245 - Lorem Ipaum Simple dummy Text...</strong></td>
      <td>Lorem Ipsum</td>
      <td>230.00</td>
      <td>46</td>
      <td>5698.00</td>
      <td>5689978.00</td>
    </tr>
    <tr class="item">
      <td><strong>F00245 - Lorem Ipaum Simple dummy Text...</strong></td>
      <td>Lorem Ipsum</td>
      <td>230.00</td>
      <td>46</td>
      <td>5698.00</td>
      <td>5689978.00</td>
    </tr>
    <tr class="item">
      <td>&nbsp;</td>
      <td><strong>Total :</strong></td>
      <td>&nbsp;</td>
      <td><strong>46</strong></td>
      <td><strong>123456</strong></td>
      <td><strong>385.00</strong></td>
    </tr>
    <tr class="item">
      <td>&nbsp;</td>
      <td><strong>Discount :</strong></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><strong>(10%)</strong></td>
      <td><strong>- 385.00</strong></td>
    </tr>
    <tr class="total">
      <td style="text-align:right;"><strong>Total Amount :</strong></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><strong>60000.00</strong></td>
    </tr>
  </table>
  <br/>
  <p><strong>Remarks :</strong> Lorem Ipsum simple dummy text.</p>
  <br/>
  <br/>
  <table>
    <tr>
      <td style="padding-left:0;"><strong>Lorem Ipsum : </strong>simple dummy text Lorem Ipsum<br/>
        <strong>Lorem Ipsum : </strong> simple dummy text Lorem Ipsum </td>
      <td style="text-align:center;"><hr/>
        Lorem Ipsum</td>
      <td style="text-align:center;"><hr/>
        Lorem Ipsum</td>
    </tr>
  </table>
</div>
</body>
</html>';

		$stylesheet = file_get_contents(asset('css/invoice.css'));	
		require_once base_path('vendor/mpdf/mpdf.php');
		$mpdf=new mPDF();
		$mpdf->WriteHTML($stylesheet,1);
		$mpdf->WriteHTML($html,2);
		$mpdf->Output('git-invoice.pdf','I');
		exit;
	}
	
	public function downloadInvoice($per_git_id='')
    {
		$html='<!doctype html>
<html>
<body>
<div class="invoice-box">
  <table>
    <tr>
      <td class="title"><img src="'.asset('img/mmlsmall.png').'" style="width:100%; max-width:300px;"><br/>
        Lorem Ipsum simple dummy<br/>
        text Lorem Ipsum<br/>
        simple dummy text </td>
      <td><strong>Goods Return Note</strong></td>
    </tr>
  </table>
  <hr/>
  <table>
    <tr>
      <td class="title"> Lorem Ipsum simple dummy<br/>
        text Lorem Ipsum<br/>
        simple dummy text </td>
      <td>Created: January 1, 2015<br>
        Due: December 6, 2016</td>
    </tr>
  </table>
  <table cellpadding="0" cellspacing="0">
    <tr class="heading">
      <td>Item </td>
      <td>Name</td>
      <td>Price</td>
      <td>Qty.</td>
      <td>Amount</td>
      <td>Total</td>
    </tr>
    <tr class="item">
      <td><strong>FG245 - Lorem Ipaum Simple dummy Text...</strong></td>
      <td>Lorem Ipsum</td>
      <td>230.00</td>
      <td>46</td>
      <td>5698.00</td>
      <td>5689978.00</td>
    </tr>
    <tr class="item">
      <td><strong>F00245 - Lorem Ipaum Simple dummy Text...</strong></td>
      <td>Lorem Ipsum</td>
      <td>230.00</td>
      <td>46</td>
      <td>5698.00</td>
      <td>5689978.00</td>
    </tr>
    <tr class="item">
      <td>&nbsp;</td>
      <td><strong>Total :</strong></td>
      <td>&nbsp;</td>
      <td><strong>46</strong></td>
      <td><strong>123456</strong></td>
      <td><strong>385.00</strong></td>
    </tr>
    <tr class="item">
      <td>&nbsp;</td>
      <td><strong>Discount :</strong></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><strong>(10%)</strong></td>
      <td><strong>- 385.00</strong></td>
    </tr>
    <tr class="total">
      <td style="text-align:right;"><strong>Total Amount :</strong></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><strong>60000.00</strong></td>
    </tr>
  </table>
  <br/>
  <p><strong>Remarks :</strong> Lorem Ipsum simple dummy text.</p>
  <br/>
  <br/>
  <table>
    <tr>
      <td style="padding-left:0;"><strong>Lorem Ipsum : </strong>simple dummy text Lorem Ipsum<br/>
        <strong>Lorem Ipsum : </strong> simple dummy text Lorem Ipsum </td>
      <td style="text-align:center;"><hr/>
        Lorem Ipsum</td>
      <td style="text-align:center;"><hr/>
        Lorem Ipsum</td>
    </tr>
  </table>
</div>
</body>
</html>';

		$stylesheet = file_get_contents(asset('css/invoice.css'));	
		require_once base_path('vendor/mpdf/mpdf.php');
		$mpdf=new mPDF();
		$mpdf->WriteHTML($stylesheet,1);
		$mpdf->WriteHTML($html,2);
		$mpdf->Output('git-invoice'.date('Y-m-d-H:i:s').'.pdf','D');
		exit;
	
	}
	
	public function updateGitRecords(Request $request)
    {
		$return=0;
		
        $git['lorry_no'] = $request->lorry_no;
        $git['transport_no'] = $request->transport_no;
        $git['updated_at'] = date('Y-m-d H:i:s');
                
        if(DB::table('git')->where('challan_no', $request->ch_no)->update($git))
        {
            $return=1;
        }
        echo $return;
	
	}
	
	
	
	public function gitWiseComments($id)
    {
        //dd($id);
        $Comments = DB::table('git_comments')
                  ->select('*')
                  ->where('git_id', '=', $id)->orderBy('created_at','desc')->get();
        //dd($Comments);
        return view('vendor.gitwise-comments')->with('comments_list',$Comments)->with('git_id', $id);
       
    }
     public function addGitComments(Request $request)
    {
        $return=0;
        
        $action['git_id'] = $request->git_id;
        $action['user_id'] = Auth::user()->id;   
        $action['comments'] = $request->comments;
        $action['created_at'] = date('Y-m-d H:i:s');
        $action['updated_at'] = date('Y-m-d H:i:s');
                
        if(DB::table('git_comments')->insert($action))
        {
            $return=1;
        }
        echo $return;
    }
	
	
}
