<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\Rfq;
use App\Models\Quetes;
use DB;
Use Auth;

class VendorRfqRequestController extends Controller
{
    public function index()
    {
		
		$rfq_list = DB::table('rfqs')
		->join('rfq_details', 'rfqs.id', '=', 'rfq_details.rfq_id')
		->select('rfqs.*')
		->where('rfq_details.vendor_id', '=',Auth::user()->id)
		->where('rfqs.status','=',1)->orderBy('rfqs.created_at','desc')
		->get();
		if(!empty($rfq_list))
		{
			foreach($rfq_list as $rfq)
			{
				$product_details =DB::table('products')->where('id', '=',$rfq->product_id)->first();

				$rfq_details =DB::table('rfq_details')->where('vendor_id', '=',Auth::user()->id)->where('rfq_id', '=',$rfq->id)->first();
				if(!empty($product_details)){
					$rfq->product_details=$product_details;

				}
				if(!empty($rfq_details))
				{
					
					$rfq->rfq_details=$rfq_details;
					$rfq_quetes =DB::table('rfq_quetes')->where('vendor_id', '=',Auth::user()->id)->where('rfq_id', '=',$rfq_details->rfq_id)->orderBy('created_at','desc')->get();
					if(!empty($rfq_quetes))
					{
						$rfq->rfq_quetes=$rfq_quetes;
						foreach($rfq_quetes as $qts)
						{
							$rfq_comments =DB::table('rfq_comments')->where('quete_id', '=',$qts->id)->orderBy('created_at','desc')->get();
							if(!empty($rfq_comments))
							{
								$qts->rfq_comments=$rfq_comments;
							}
							
						}
						
						
					}
					
				}
				
			}
		}
	
		//dd($rfq_list);
    	return view('vendor.vendor-rfq-request-list')->with('rfq_list',$rfq_list);
    }
	
	public function addRfqQuotation(Request $request)
    {
		
		$return=0;
        $quetes = new Quetes;
		$quetes->quete_id =  $this->generateQU();
        $quetes->rfq_id = $request->rfq_id;
		$quetes->vendor_id = Auth::user()->id;	
        $quetes->price = $request->rfq_price;
		$quetes->width = $request->rfq_width;
        $quetes->status = 1;
        $quetes->created_at = date('Y-m-d H:i:s');
        $quetes->updated_at = date('Y-m-d H:i:s');
        if($quetes->save())
		{
			$return=1;
		}
		echo $return;
    }
	static function generateQU()
    {
        $last_req = Quetes::selectRaw('max(id) as id')->first();
        $req_id=str_pad($last_req->id+1, 6, '0', STR_PAD_LEFT);
        $req_id = 'QU/'.$req_id;
        return $req_id;
    }
	
	
	public function quotationWiseComments(Request $request)
    {
		$comments_list = DB::table('rfq_comments')
		->select('*')
		->where('quete_id', '=',$request->quotation_id)
		->orderBy('id','asc')
		->get();
		#dd($comments_list);
        return view('vendor.quotationwise-comments')->with('comments_list',$comments_list)->with('quotation_id',$request->quotation_id);
    }
	
	public function addQuotationComments(Request $request)
    {
		
		$return=0;
		
        $quotation['quete_id'] = $request->quotation_id;
		$quotation['user_id'] = Auth::user()->id;	
        $quotation['comments'] = $request->comments;
        $quotation['created_at'] = date('Y-m-d H:i:s');
        $quotation['updated_at'] = date('Y-m-d H:i:s');
				
        if(DB::table('rfq_comments')->insert($quotation))
		{
			$return=1;
		}
		echo $return;
    }
     public function rejectedrfq()
    {
		$rfq_rejected_list = DB::table('rfqs')
		->join('rfq_details', 'rfqs.id', '=', 'rfq_details.rfq_id')
		->select('rfqs.*')
		->where('rfq_details.vendor_id', '=',Auth::user()->id)
		->where('rfqs.status', '=',0)
		->orderBy('rfqs.status','desc')->orderBy('rfqs.created_at','desc')
		->get();

		if(!empty($rfq_rejected_list))
		{
			foreach($rfq_rejected_list as $rfq)
			{
				$product_details =DB::table('products')->where('id', '=',$rfq->product_id)->first();

				$rfq_details =DB::table('rfq_details')->where('vendor_id', '=',Auth::user()->id)->where('rfq_id', '=',$rfq->id)->first();
				if(!empty($product_details)){
					$rfq->product_details=$product_details;

				}
				if(!empty($rfq_details)){
					$rfq->rfq_details=$rfq_details;

				}
			}
		}		
	//dd($rfq_rejected_list);
    	return view('vendor.vendor-rejected-rfq')->with('rfq_rejected_list',$rfq_rejected_list);
    }
}
