<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Models\UserDetails;
use App\Models\Department;
use App\Models\Devision;
use App\Models\Section;
use DB;
use Auth;
use Redirect;

class AdminVendorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vendor_list=DB::table('users')->join('v_finsh','users.SLCODE','=','v_finsh.SLCODE')->select('users.*','v_finsh.BPH1 as mobile','v_finsh.BADDR as address')->where('users.type','!=','admin')->paginate(10);
       /* $vendor_list=DB::table('users')->join('departments','departments.id','=','users.department_id')->join('sections','sections.id','=','users.section_id')->join('devision','devision.id','=','users.devision_id')->join('userdetails','userdetails.userid','=','users.id')->select('users.*','departments.name AS Dname','sections.name AS Sname','devision.name AS DVname','userdetails.mobile','userdetails.address')->where('users.type','!=','admin')->paginate(10);*/
        //dd($vendor_list);
        return view('admin.vendor_list')->with('vendor_list',$vendor_list);
    }
    public function addVendor()
    {
        $Department=Department::where('status','=',1)->get();
        $Section=Section::where('status','=',1)->get();
        $Devision=Devision::where('status','=',1)->get();
        return view('admin.add-vendor')->with('Department',$Department)->with('Section',$Section)->with('Devision',$Devision);
    }
    public function insertVendor(Request $request)
    {
        Validator::extend('alpha_spaces', function($attribute, $value)
        {
            return preg_match('/^[\pL\s]+$/u',$value);
        });
        $messages = [
        'name.alpha_spaces' => 'The Name may only contain letters and spaces.',     
        ];

        $this->validate($request,[
        'first_name'=>'required|alpha_spaces',
        'last_name'=>'required|alpha_spaces',
        'username'=>'required',
        'email'=>'required|email|unique:users',
        //'password'=>'required',        
        //'confirm_password'=>'required|same:password',
        'mobile'=>'required',
        'adderess'=>'required',
        'department'=>'required',
        'section'=>'required',
        'devision'=>'required'
        ]);

        $first_name=trim($request->first_name);
        $last_name=trim($request->last_name);
        $username=trim($request->username);
        $email=trim($request->email);
        $password=md5($request->username);
        $mobile=$request->mobile;
        $department=$request->department;
        $section=$request->section;
        $devision=$request->devision;
        $adderess=$request->adderess;
        $type='vendor';
        $createdby=1;
        $modifiedby=1;







        $venor=new User;
        $venor->type=$type;
        $venor->first_name=$first_name;
        $venor->last_name=$last_name;
        $venor->username=$username;
        $venor->email=$email;
        $venor->password=$password;
        $venor->createdby=$createdby;
        $venor->modifiedby=$modifiedby;
        $venor->department_id=$department;
        $venor->section_id=$section;
        $venor->devision_id=$devision;
        if($venor->save())
        {
            $vendor_details=new UserDetails;
            $vendor_details->userid=$venor->id;
            $vendor_details->mobile=$mobile;
            $vendor_details->address=$adderess;
            if($vendor_details->save())
            {
                return redirect::route('ven-listing');
            }            
        }
        else
        {
            return view('admin.add-vendor');
        }

    }

    public function editVendor($id)
    {
        $userId=$id;
        $Department=Department::where('status','=',1)->get();
        $Section=Section::where('status','=',1)->get();
        $Devision=Devision::where('status','=',1)->get();
        $editVendor=DB::table('users')->join('userdetails','users.id','=','userdetails.userid')->where('users.type','!=','admin')->where('users.id','=',$userId)->select('users.id','users.username','users.first_name','users.last_name','users.email','users.department_id','users.section_id','users.devision_id','userdetails.mobile','userdetails.address')->first();
        return view('admin.edit-vendor')->with('vendor_details',$editVendor)->with('Department',$Department)->with('Section',$Section)->with('Devision',$Devision);
    }
    public function updateVendor(Request $request)
    {
        //dd($request->email);
        Validator::extend('alpha_spaces', function($attribute, $value)
        {
            return preg_match('/^[\pL\s]+$/u',$value);
        });
        $messages = [
        'name.alpha_spaces' => 'The Name may only contain letters and spaces.',     
        ];

        $this->validate($request,[
        'first_name'=>'required|alpha_spaces',
        'last_name'=>'required|alpha_spaces',
        'username'=>'required',
        'email'=>'required|email|unique:users,id,'.$request->email,
        
        'mobile'=>'required',
        'adderess'=>'required',
        'department'=>'required',
        'section'=>'required',
        'devision'=>'required'
        ]);
        $id=$request->id;
        $first_name=trim($request->first_name);
        $last_name=trim($request->last_name);
        $username=trim($request->username);
        $email=trim($request->email);        
        $department=$request->department;
        $section=$request->section;
        $devision=$request->devision;
        $mobile=$request->mobile;
        $adderess=$request->adderess;
        $modifiedby=1;
        $vendor_edit=User::where('id','=',$id)->first();
        $vendor_edit->first_name=$first_name;
        $vendor_edit->last_name=$last_name;
        $vendor_edit->username=$username;
        $vendor_edit->email=$email;
        $vendor_edit->department_id=$department;
        $vendor_edit->section_id=$section;
        $vendor_edit->devision_id=$devision;
        if($vendor_edit->save())
        {
            $vendor_details_edit=UserDetails::where('userid','=',$id)->first();
            $vendor_details_edit->mobile=$mobile;
            $vendor_details_edit->address=$adderess;
            if($vendor_details_edit->save())
            {
                return redirect::route('ven-listing');

            }
        }

    }
    public function statusActive($id)
    {
        $vendor=User::where('id','=',$id)->first();
        $vendor->status=1;
        if($vendor->save())
        {
           return redirect::route('ven-listing');
        }
        else
        {
            return redirect::route('ven-listing');
        }

    }
    public function statusDeactive($id)
    {
        $vendor=User::where('id','=',$id)->first();
        $vendor->status=0;
        if($vendor->save())
        {
           return redirect::route('ven-listing');
        }
        else
        {
            return redirect::route('ven-listing');
        }

    }

   public function editProfile()
        {
            $user_id=Auth::User()->id;
            
            $data['userdata']=User::where('id','=',$user_id)->first();
            $data['user_data']=UserDetails::where('userid','=',$user_id)->first();
            //dd($userdata);
            return view('vendor.edit-profile',$data);
        }
    public function updateProfile(Request $request)
    {
        //dd($request);
        Validator::extend('alpha_spaces', function($attribute, $value)
        {
            return preg_match('/^[\pL\s]+$/u',$value);
        });
        $messages = [
        'name.alpha_spaces' => 'The Name may only contain letters and spaces.',     
        ];

        $this->validate($request,[
        'name'=>'required|alpha_spaces',
        'username'=>'required',
        'email'=>'required|email|unique:users,id,'.$request->email, 

        'mobile'=>'required',
        'address'=>'required',              
        ],$messages);

        $id=$request->id;
        $name=trim($request->name);
        $username=trim($request->username);
        $email=trim($request->email);        
        $mobile=$request->mobile;
        $address=$request->address;

        $profile_edit=User::where('id','=',$id)->first();
        $profile_edit->name=$name;
        $profile_edit->username=$username;
        $profile_edit->email=$email;
        $profile_edit->mobile=$mobile;
        $profile_edit->address=$address;
        
        if($profile_edit->save())
        {
            /*//dd($id);
            $vendor_details=UserDetails::where('userid','=',$id)->first();
            $vendor_details->mobile=$mobile;
            $vendor_details->address=$address;
            if($vendor_details->save())
            {*/
                return redirect::back()->withErrors('Profile Updated Successfully');

            /*}*/
        }
    
    }
    public function editPassword()
    {
         $user=Auth::User()->id;
         //dd($user);
         return view('vendor.edit-password')->with('userid',$user);
    }
    public function updatePassword(Request $request)
    {
        //dd($request); 
        $this->validate($request,[
        
        'old_password'=>'required',        
        'new_password'=>'required|min:6',        
        'confirm_new_password'=>'required|min:6|same:new_password'
        
        ]);  
        $id=$request->id;     
        $pass=Auth::User()->password;        
        $new_password=md5($request->new_password); 
        $old_password=md5($request->old_password);
		if(empty($request->new_password) || empty($request->confirm_new_password))
              return redirect::back()->withErrors(['Please Provide your desire password!!']);  
			  
		if($request->new_password!=$request->confirm_new_password)
              return redirect::back()->withErrors(['New password and Confirm New Password Mismatch!!']);  
			          
        if($old_password==$pass){
          $pass_edit=User::findOrFail($id);
          $pass_edit->password=$new_password;
          
        try{
            $pass_edit->save();
            return redirect::back()->withErrors('Password Changed Successfully');
          
            }
        catch(Exception $e){}
          }
          else{
               return redirect::back()->withErrors(['Old password does not matched']);
;
          }      

    }

    public function vendorpass($id)
    {   
        //dd($id);
        $data['username']=User::where('id','=',$id)->first();         
        return view('admin.set-password',$data);
    }
    public function setpassword(Request $request)
    {
         //dd($request);
        Validator::extend('alpha_spaces', function($attribute, $value)
        {
            return preg_match('/^[\pL\s]+$/u',$value);
        });
        $messages = [
        'pass.required' => 'Password field can not be blank.', 
        'con_pass.required' => 'Confirm Password field can not be blank.',   
        ];

        $this->validate($request,[        

        'pass'=>'required',
        'con_pass'=>'required',              
        ],$messages);

        $id=$request->id;
        $password=md5($request->pass);
        if($request->pass!=$request->con_pass)
              return redirect::back()->withErrors(['Password and Confirm Password Mismatch!!']); 

        $set_pass=User::findOrFail($id);
        $set_pass->password=$password;

        try{
            $set_pass->save();
            return redirect::route('ven-listing');
          
            }
        catch(Exception $e){}
    }
  
}
