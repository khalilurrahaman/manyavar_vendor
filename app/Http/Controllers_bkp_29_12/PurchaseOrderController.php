<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\purchesOrder;
use Auth;
use Redirect;
use DB;

class PurchaseOrderController extends Controller
{
    public function index()
    {
    	
    	//$last_three_month = date('Y-m-d',strtotime("-3 month"));
    	//dd($last_three_month);
    	$purchaseOrderList = DB::table('purchase_order')->select(['purchase_order.ORDNO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY'])
    	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
    	->where('purchase_order.status', '=','OPEN')
    	->orderBy('purchase_order.ORDDT', 'desc')->paginate(30);
		
		//Filter Data
		$data['po_number_list'] = DB::table('purchase_order')->select('purchase_order.ORDNO')
    	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
    	->where('purchase_order.status', '=','OPEN')
		->groupBy('purchase_order.ORDNO')
    	->orderBy('purchase_order.ORDDT', 'desc')
		->get();
		
		$data['cat1_list'] = DB::table('purchase_order')->select('products.cat1')
    	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
    	->where('purchase_order.status', '=','OPEN')
		->groupBy('products.cat1')
    	->orderBy('products.cat1', 'desc')
		->get();
		
		
		$data['cat2_list'] = DB::table('purchase_order')->select('products.cat2')
    	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
    	->where('purchase_order.status', '=','OPEN')
		->groupBy('products.cat2')
    	->orderBy('products.cat2', 'desc')
		->get();
		
    	//dd($purchaseOrderList);
    	return view('admin.purchase-order-list',$data)->with('purchaseOrderList', $purchaseOrderList);
    }

    
	 public function filterPurchaseOrder(Request $request)
    {
    	
		$po_number=$request->po_number;
		$category1=$request->category1;
		$category2=$request->category2;
		$due_from=$request->due_from;
		$due_to=$request->due_to;
		
		$query_str=" purchase_order.status = 'OPEN' ";
    	if(!empty($po_number))
    		$query_str .=" AND purchase_order.ORDNO = '".$po_number."' ";
    	if(!empty($category1))
    		$query_str .=" AND products.cat1 = '".$category1."' ";
		if(!empty($category2))
    		$query_str .=" AND products.cat2 = '".$category2."' ";
		if(!empty($due_from))
    		$query_str .=" AND purchase_order.ORDDT >= '".$due_from."' ";
    	if(!empty($due_to))
    		$query_str .=" AND purchase_order.ORDDT <= '".$due_to."' ";
			
    	$purchaseOrderList = DB::table('purchase_order')->select(['purchase_order.ORDNO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY'])
    	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
    	->where('purchase_order.status', '=','OPEN')
		->whereRaw($query_str)
    	->orderBy('purchase_order.ORDDT', 'desc')->paginate(30);
		
	
    	return view('admin.purchase-order-list-filter')->with('purchaseOrderList', $purchaseOrderList);
    }

}
