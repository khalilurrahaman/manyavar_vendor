<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Models\Section;
use DB;
use Redirect;

class AdminSectionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list=Section::orderBy('id')->paginate(10);
        return view('admin.section-list')->with('list',$list);
    }
    public function getEditData(Request $request)
    {
        $id=$request->id;
        $data=Section::where('id','=', $id)->first();
        return $data;
    }
    
    public function addSection(Request $request)
    {
        $user=\Auth::user();
        $this->validate($request,[
        'name'=>'required',        
        'description'=>'required'
        ]);

        $name=trim($request->name);        
        $description=$request->description;        
        /*$createdby=$user->id;
        $modifiedby=$user->id;*/
        $createdby=3;
        $modifiedby=3;


        $section=new Section;
        $section->name=$name; 
        $section->description=$description;      
        $section->createdby=$createdby;
        $section->modifiedby=$modifiedby;
        if($section->save())
        {
            return redirect::route('section-listing');
                  
        }
        else
        {
            return redirect::route('section-listing');
        }

    }

    public function updateSection(Request $request)
    {

        $user=\Auth::user();
        $this->validate($request,[
        'name'=>'required',        
        'description'=>'required'
        ]);
        $id=$request->id;
        $name=trim($request->name);        
        $description=$request->description;        
/*        $modifiedby=$user->id;
*/        $modifiedby=3;

        $data=Section::where('id','=',$id)->first();
        $data->name=$name;
        $data->description=$description;
        $data->modifiedby=$modifiedby;
        if($data->save())
        {
            return redirect()->back();
        }
        else
        {
            return redirect()->back();
        }

        /*$data=\DB::table('departments')->where('id','=',$id)->update(
            array(
                'name' => $name,                
                'description' => $description,                
                'modifiedby' => $modifiedby          
               
                ));*/
        
        
    }
    public function deleteSection(Request $request)
    {
        $id=$request->id;
        $data=Section::where('id','=',$id);
        if($data->delete())
        {
            return redirect()->back();
        }
    }
}
