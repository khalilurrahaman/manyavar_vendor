<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
		
        //Start If the status is not approved redirect to login//Khalilur 22/02/2017 
		$response = $next($request);
        if(Auth::check() && Auth::user()->status == 0){
            Auth::logout();
			return redirect('/')->withErrors(['loginmsg' => 'Your account has been inactivated!']);
            //return redirect('/login')->withErrors('loginmsg', 'Your error text');
        }
		//End If the status is not approved redirect to login//Khalilur 22/02/2017 
		
        if (Auth::guard($guard)->check()) {
            return redirect('/');
        }

        return $next($request);
    }
}
