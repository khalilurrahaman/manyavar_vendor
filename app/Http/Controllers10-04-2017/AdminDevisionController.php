<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Models\Devision;
use DB;
use Redirect;

class AdminDevisionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($this->userAccessibilityCheck('division-list'))
		{
			$list=Devision::orderBy('id')->get();
        	return view('admin.devision-list')->with('list',$list);
		}
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);
		    
			
    }
    public function getEditData(Request $request)
    {
		    
        $id=$request->id;
        $data=Devision::where('id','=', $id)->first();
        return $data;
    }
    
    public function addDevision(Request $request)
    {
        $user=\Auth::user();
        $this->validate($request,[
        'name'=>'required|unique:devision',        
        'description'=>'required'
        ]);

        $name=trim($request->name);        
        $description=$request->description;        
        $createdby=$user->id;
        $modifiedby=$user->id;
        


        $devision=new Devision;
        $devision->name=$name; 
        $devision->description=$description;      
        $devision->createdby=$createdby;
        $devision->modifiedby=$modifiedby;
        if($devision->save())
        {
            return redirect::route('division-listing')->withErrors(["New Devision Added Successfully"]);
                  
        }
        else
        {
            return redirect::route('division-listing')->withErrors(["Error!!"]);
        }

    }

    public function updateDevision(Request $request)
    {

        $user=\Auth::user();
        $id=$request->id;
        $name=trim($request->name);        
        $description=$request->description;        
        $modifiedby=$user->id;
        $this->validate($request,[
        'name'=>'required|unique:devision,name,'.$id,        
        'description'=>'required'
        ]);
        
        //$modifiedby=3;

        $data=Devision::where('id','=',$id)->first();
        $data->name=$name;
        $data->description=$description;
        $data->modifiedby=$modifiedby;
        if($data->save())
        {
            return Redirect::route('division-listing')->withErrors(["Devision Updated Successfully"]);
        }
        else
        {
            return redirect()->back();
        }

        /*$data=\DB::table('departments')->where('id','=',$id)->update(
            array(
                'name' => $name,                
                'description' => $description,                
                'modifiedby' => $modifiedby          
               
                ));*/
        
        
    }
    public function deleteDevision(Request $request)
    {
        if($this->userAccessibilityCheck('division-delete'))
		{
			$id=$request->id;
			$data=Devision::where('id','=',$id);
			if($data->delete())
			{
				return redirect()->back()->withErrors(["Devision Deleted Successfully"]);
			}
		}
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);
			
    }
    public function addDevisionPage()
    {
        if($this->userAccessibilityCheck('division-add'))
		 	return view('admin.add-division');
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);
       
        
    }
    public function editDevisionPage($id)
    {
		if($this->userAccessibilityCheck('division-edit'))
		{
			$devision=Devision::where('id','=',$id)->first();
        	return view('admin.edit-division')->with('devision',$devision);
		}
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);
        
        
    }


}
