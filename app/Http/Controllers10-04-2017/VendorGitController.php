<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Redirect;
use mpdf;
use URL;
class VendorGitController extends Controller
{
    public function viewGit()
    {
    	return view('admin.vendor-all-git-list');
    }

    public function viewGrt()
    {
    	
		if($this->userAccessibilityCheck('all-grt-list'))
		{
			$data['grt_list'] = DB::table('V_INVGRTMAIN')->selectRaw('users.name,V_INVGRTMAIN.id,V_INVGRTMAIN.GRTNO,V_INVGRTMAIN.GRTDT,V_INVGRTDET.QTY,V_INVGRTMAIN.GRCCODE,V_INVGRTDET.RTINVQTY')
			->join('V_INVGRTDET','V_INVGRTMAIN.GRTNO', '=', 'V_INVGRTDET.GRTNO')
			->join('users', 'V_INVGRTMAIN.PCODE', '=', 'users.SLCODE')
			->where('V_INVGRTMAIN.status', '=','1')
			->orderBy('V_INVGRTMAIN.GRTDT', 'desc')
			->paginate(20);
			//dd($data['grt_list']);
			return view('admin.vendor-all-grt-list',$data);
		}
		else
		{
			return Redirect::route('dashboard')->withErrors(['You have no access!']);
		}
    }
	
	public function resolveGitRecordsAdmin(Request $request)
    {
		$return=0;
        $file = $request->file('file');
        $fileName = time().'_'.$request->file('file')->getClientOriginalName();
        if($file->move(public_path('upload/grcdoc'), $fileName))
		{
			//$git['grc_doc'] =$fileName;
			//$git['git_id'] = $request->git_id;
			$grc['updated_at'] = date('Y-m-d H:i:s');
			#print_r($git);exit;		
			if(DB::table('V_INVGRTMAIN')->where('id', $request->grc_id)->update($grc))
			{

				$return=1;
			}
        }
        echo $return;
    }


    public function viewAllVendorGit()
    {		
    	return view('vendor.vendor-all-git');
    }
	
    public function viewAddVendorGit()
    {						        
    	   return view('vendor.vendor-add-git');
    }
	
	  public function filterAddGit(Request $request)
    {
		
		
  		//$pending_qty=$request->pending_qty;
  		$category1=$request->category1;
  		$category2=$request->category2;
  		$category3=$request->category3;
  		$category4=$request->category4;
  		
  		$query_str=" purchase_order.status = 'OPEN' AND  (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) > 0 ";
      	//if(!empty($pending_qty))
      		//$query_str .=" AND pending_qty.ORDNO = '".$pending_qty."' ";
      	if(!empty($category1))
      		$query_str .=" AND products.cat1 = '".$category1."' ";
  		if(!empty($category2))
      		$query_str .=" AND products.cat2 = '".$category2."' ";
  		if(!empty($category3))
          $query_str .=" AND products.CCODE3 = '".$category3."' ";
      if(!empty($category4))
          $query_str .=" AND products.CCODE4 = '".$category4."' ";
  			
  			
  			
  		$data['purchaseOrderList'] = DB::table('purchase_order')->select('purchase_order.ORDNO','products.id as product_id','products.image','products.cat1','products.cat2','products.cat3','products.cat4','V_PURORDDET.id as pd_id','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.ORDCODE','V_PURORDDET.ICODE','V_PURORDDET.CNLQTY','V_PURORDDET.OQTY',DB::raw('sum(git.git_qty) as git_qty'))
      	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
      	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
  		//->leftJoin('git', 'git.ORDCODE', '=', 'V_PURORDDET.ORDCODE' ,'and','git.ICODE', '=', 'V_PURORDDET.ICODE')
  		  ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
      	->where('purchase_order.status', '=','OPEN')
    		->where('V_PURORDDET.status', '=','OPEN')
    		->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
    		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
    		->whereRaw($query_str)
      	->orderBy('purchase_order.ORDDT', 'desc')
    		->groupBy('V_PURORDDET.id')
    		->get();
  		
      	return view('vendor.vendor-add-git-filter',$data);
    }
	
	public function submitGitRecords(Request $request)
    {
		
		$per_git_id= $this->generatePerGitId();
		$git_no= $this->generateGit($per_git_id);
		#$challan_no= $this->generateChallan($per_git_id);
		for($i=0;$i<count($request->git_qty);$i++)
		{
			if(!empty($request->git_qty[$i]))
			{
				$git['per_git_id'] = $per_git_id;
				$git['git_no'] = $git_no;
				$git['ORDCODE'] = $request->ORDCODE[$i];//
				$git['ICODE'] = $request->ICODE[$i];//
				$git['pd_id'] = $request->pd_id[$i];//
				$git['SLCODE'] = Auth::user()->SLCODE;
				$git['git_qty'] = $request->git_qty[$i];//
				$git['challan_no'] = $request->challan_no;
				$git['lorry_no'] = $request->lorry_no;
				$git['transport_no'] = $request->transport_no;
				$git['git_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $request->date)));
				$git['created_at'] = date('Y-m-d H:i:s');
				$git['updated_at'] = date('Y-m-d H:i:s');
				$git['status'] = 'Pending';
				if(DB::table('git')->insert($git))
				{
					/*$row_result = DB::table('git')->selectRaw('max(id) as id')->first();
					$notify_array['message'] = $git_no.' has been Added';
					$notify_array['type'] = 'GitAdded';
					$notify_array['red_url'] = 'all-git-list';
					$notify_array['sender'] = Auth::user()->id;
					$notify_array['receiver'] = 1;
					$notify_array['status'] = 'NEW';
					$notify_array['table_name'] = 'git';
					$notify_array['primary_key'] = 'id';
					$notify_array['primary_key_val'] =$row_result->id;
					$notify_array['created_at'] =date('Y-m-d H:i:s');	
					DB::table('notification')->insert($notify_array);*/
					
				}
			}
		}
		
		return Redirect::route('view-git-invoice',$per_git_id);		
		//return view('vendor.success-git')->with('per_git_id',$per_git_id);
		
    }	
	
	
	static function generatePerGitId()
    {
		$max_id=100001;
        $row_result = DB::table('git')->selectRaw('max(per_git_id) as per_git_id')->first();
		if(!empty($row_result) && count($row_result) && $row_result->per_git_id!=null)
        $max_id=$row_result->per_git_id+1;
        return $max_id;
    }
	
	
	public function generateInvoice($per_git_id='')
    {
		
		
		$vDtls = DB::table('v_finsh')->selectRaw('*')->where('SLCODE','=',Auth::user()->SLCODE)->first();
		$CSTNO='Not specified';
    	$LSTNO='Not specified';
    	if(!empty($vDtls))
    	{
    	$CSTNO=$vDtls->CSTNO;
    	$LSTNO=$vDtls->LSTNO;
    	}
		$gitList = DB::table('git')->selectRaw('purchase_order.PONO,products.cat1,products.cat2,products.cat3,products.cat4,products.ITEM_NAME,V_PURORDDET.RATE,git.git_qty,git.git_no,git.challan_no,git.created_at,git.git_date')
    	->leftJoin('V_PURORDDET', 'git.pd_id', '=', 'V_PURORDDET.id')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
		->where('git.per_git_id', '=',$per_git_id)
    	->orderBy('git.created_at', 'desc')
		->get();
		#dd($gitList);
		$git_no='NA';
		if(!empty($gitList[0]))
		{
			$git_no=$gitList[0]->git_no;
			$challan_no=$gitList[0]->challan_no;
			$created_at=date('d/m/Y',strtotime($gitList[0]->created_at));
			$git_date=date('d/m/Y',strtotime($gitList[0]->git_date));
		}
		$html='<!doctype html>
<html>';
$html2='<head>
<meta charset="utf-8">
<title>invoice :: Print</title>
</head>';

$html .='<body>
<div class="invoice-box">
  <table>
    <tr>
     
      <td class="title" style="text-align:left;"><strong>Supplier :</strong> '.Auth::user()->name.'<br/>
        <strong>Address :</strong> '.Auth::user()->address.' <br/>
        <strong>CST No :</strong>'.$CSTNO.'<br/>
        <strong>VAT No :</strong> '.$LSTNO.'
		</td>
      <td><strong>DELIVERY CHALLAN</strong><br/>
        <br/></td>
    </tr>
  </table>
  <hr/>
  <table>
    <tr>
	 <td class="title" style="text-align:left;"><h3>VEDANT FASHIONS PVT. LTD.</h3>
        <p style="font-size:13px !important;"><strong>PAN :</strong> AABCV4646B, <strong>CIN :</strong> U51311WB2002PTC094677, <br/>
          <strong>Regd. Office :</strong> Paridhan Garment Park, SDF-1,A-501-502, 4th Floor, 19, Canal <br/>
          South Road, Kolkata, West Bengal, 700015 <br/>
          <strong>Phone :</strong> 33685353, <strong>Fax :</strong> 23232069, <strong>E-Mail : </strong> info@manyavar.com</p></td>
      <td style="text-align:left;"><strong>No. :</strong>'.$git_no.'<br/>
        <strong>Date :</strong>'.$created_at.'<br/>
        <strong>Challan No. :</strong> '.$challan_no.' <br/>
        <strong>Currency :</strong> Rupees<br/>
        <strong>Challan Date. :</strong>'.$git_date.'</td>
    </tr>
  </table>
  <br/>
  <table cellpadding="0" cellspacing="0">
    <tr class="heading">
      <td>Design</td>
      <td>Colour</td>
      <td>Cat 3</td>
      <td>Width/Size</td>
      <td>Item Tax Group</td>
      <td>Qty.</td>
    </tr>';
    $t_qty=0;
	if(!empty($gitList))
	{
	foreach($gitList as $git)
	{
	$t_qty +=$git->git_qty;
    $html .='<tr class="item">
      <td>'.$git->cat1.'</td>
      <td>'.$git->cat2.'</td>
      <td>'.$git->cat3.'</td>
      <td>'.$git->cat4.'</td>
      <td>Non Taxable</td>
      <td>'.$git->git_qty.'</td>
    </tr>';
	}
	}
    $html .='<tr class="item">
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><strong>Total :</strong></td>
      <td>'.$t_qty.'</td>
    </tr>
  </table>
</div>
</body>
</html>';

		$stylesheet = file_get_contents(public_path('css/invoice.css'));	
		require_once base_path('vendor/mpdf/mpdf.php');
		$mpdf=new mPDF();
		$mpdf->WriteHTML($stylesheet,1);
		$mpdf->WriteHTML($html,2);
		$mpdf->Output('git-invoice.pdf','I');
		exit;
	}
	
	public function downloadInvoice($per_git_id='')
    {
    	$vDtls = DB::table('v_finsh')->selectRaw('*')->where('SLCODE','=',Auth::user()->SLCODE)->first();
    	$CSTNO='Not specified';
    	$LSTNO='Not specified';
    	if(!empty($vDtls))
    	{
    	$CSTNO=$vDtls->CSTNO;
    	$LSTNO=$vDtls->LSTNO;
    	}
		$gitList = DB::table('git')->selectRaw('products.cat1,products.cat2,products.cat3,products.cat4,products.ITEM_NAME,V_PURORDDET.RATE,git.git_qty,git.git_no,git.challan_no,git.created_at,git.git_date')
    	->leftJoin('V_PURORDDET', 'git.pd_id', '=', 'V_PURORDDET.id')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
		->where('git.per_git_id', '=',$per_git_id)
    	->orderBy('git.created_at', 'desc')
		->get();
		//dd($gitList);
		$git_no='NA';
		if(!empty($gitList[0]))
		{
			$git_no=$gitList[0]->git_no;
			$challan_no=$gitList[0]->challan_no;
			$created_at=date('d/m/Y',strtotime($gitList[0]->created_at));
			$git_date=date('d/m/Y',strtotime($gitList[0]->git_date));
			
		}
		
		$html='<!doctype html>
<html>';
$html2='<head>
<meta charset="utf-8">
<title>invoice :: Print</title>
</head>';

$html .='<body>
<div class="invoice-box">
  <table>
    <tr>
     
      <td class="title" style="text-align:left;"><strong>Supplier :</strong> '.Auth::user()->name.'<br/>
        <strong>Address :</strong> '.Auth::user()->address.' <br/>
        <strong>CST No :</strong>'.$CSTNO.'<br/>
        <strong>VAT No :</strong>'.$LSTNO.'</td>
      <td><strong>DELIVERY CHALLAN</strong><br/>
        <br/></td>
    </tr>
  </table>
  <hr/>
  <table>
    <tr>
	 <td class="title" style="text-align:left;"><h3>VEDANT FASHIONS PVT. LTD.</h3>
        <p style="font-size:13px !important;"><strong>PAN :</strong> AABCV4646B, <strong>CIN :</strong> U51311WB2002PTC094677, <br/>
          <strong>Regd. Office :</strong> Paridhan Garment Park, SDF-1,A-501-502, 4th Floor, 19, Canal <br/>
          South Road, Kolkata, West Bengal, 700015 <br/>
          <strong>Phone :</strong> 33685353, <strong>Fax :</strong> 23232069, <strong>E-Mail : </strong> info@manyavar.com</p></td>
      <td style="text-align:left;"><strong>No. :</strong>'.$git_no.'<br/>
        <strong>Date :</strong>'.$created_at.'<br/>
        <strong>Challan No. :</strong> '.$challan_no.' <br/>
        <strong>Currency :</strong> Rupees<br/>  
		<strong>Challan Date. :</strong>'.$git_date.'
		</td>
    </tr>
  </table>
  <br/>
  <table cellpadding="0" cellspacing="0">
    <tr class="heading">
      <td>Design</td>
      <td>Colour</td>
      <td>Cat 3</td>
      <td>Width/Size</td>
      <td>Item Tax Group</td>
      <td>Qty.</td>
    </tr>';
    $t_qty=0;

	if(!empty($gitList))
	{
	foreach($gitList as $git)
	{
	$t_qty +=$git->git_qty;
    $html .='<tr class="item">
       <td>'.$git->cat1.'</td>
      <td>'.$git->cat2.'</td>
      <td>'.$git->cat3.'</td>
      <td>'.$git->cat4.'</td>
      <td>Non Taxable</td>
      <td>'.$git->git_qty.'</td>
    </tr>';
	}
	}
 	
    $html .='<tr class="item">
       <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><strong>Total :</strong></td>
      <td>'.$t_qty.'</td>
    </tr>
  </table>
</div>
</body>
</html>';

		$stylesheet = file_get_contents(public_path('css/invoice.css'));	
		require_once base_path('vendor/mpdf/mpdf.php');
		$mpdf=new mPDF();
		$mpdf->WriteHTML($stylesheet,1);
		$mpdf->WriteHTML($html,2);
		$mpdf->Output('git-invoice'.date('Y-m-d-H:i:s').'.pdf','D');
		exit;
	
	}
	
	public function updateGitRecords(Request $request)
    {
		$return=0;
		
        $git['lorry_no'] = $request->lorry_no;
        $git['transport_no'] = $request->transport_no;
        $git['updated_at'] = date('Y-m-d H:i:s');
                
        if(DB::table('git')->where('challan_no', $request->ch_no)->update($git))
        {
            $return=1;
        }
        echo $return;
	
	}
	
	public function fetchGitRecords(Request $request)
    {
		$git_no=$request->git_no;
		$type=$request->type;
		$data['cnList'] = DB::table('git')->selectRaw('products.ITEM_NAME,V_PURORDDET.RATE,git.git_qty,(git.git_qty - git.git_rcv) as cn_qty,v_purordchg.ISTAX,v_purordchg.RATE as disc_rate,V_Fincharge.CHGNAME,purchase_order.GRSAMT,purchase_order.CHGAMT,purchase_order.NETAMT')
    	->leftJoin('V_PURORDDET', 'git.pd_id', '=', 'V_PURORDDET.id')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->leftJoin('v_purordchg', 'purchase_order.ORDCODE', '=', 'v_purordchg.ORDCODE')
		->leftJoin('V_Fincharge', 'v_purordchg.CHGCODE', '=', 'V_Fincharge.CHGCODE')		
		//->whereRaw('purchase_order.PCODE ='.Auth::user()->SLCODE)
		->where('git.git_no', '=',$git_no)
		->where('git.git_rcv', '>',0)
		->where(DB::raw('git.git_rcv - git.git_qty'),'<',0)
		->orderBy('git.git_no', 'desc')	
		->orderBy('git.status', 'asc')
    	->orderBy('git.updated_at', 'desc')
		->get();
		 
		 return $data;
	
	}
	
	public function resolveGitRecords(Request $request)
    {
		$return=0;
		//$id=$request->id;
        $file = $request->file('file');
        $fileName = time().'_'.$request->file('file')->getClientOriginalName();
        if($file->move(public_path('upload/gitdoc'), $fileName))
		{
        
			if($request->type=='Attach CN')
			{
				$git['cn_doc'] =$fileName;
				$git['vendor_cn_resolve_remarks'] = $request->resolve_remarks;
			}
			else
			{
				$git['sb_doc'] =$fileName;
				$git['vendor_sb_resolve_remarks'] = $request->resolve_remarks;
			}
				
			
			$git['vendor_is_resolve'] = 1;
			$git['updated_at'] = date('Y-m-d H:i:s');
			#print_r($git);exit;		
			if(DB::table('git')->where('per_git_id', $request->per_git_id)->update($git))
			{
				/*$mark_complete['is_mark_complete'] =1;
				if(DB::table('git')->where('pd_id', $request->pd_id)->update($mark_complete))
				{*/
					/*$git_row=DB::table('git')->selectRaw('git_no,request_type')->where('per_git_id', $request->per_git_id)->first();

					$git_no=$git_row->git_no;			
					$type=$git_row->request_type;

					$notify_array['type'] = $type;
					$notify_array['message'] = 'CN/SB has been attached for '. $git_no;
										
					
					$notify_array['red_url'] = 'all-git-list';
					$notify_array['sender'] = Auth::user()->id;
					$notify_array['receiver'] = 1;
					$notify_array['status'] = 'NEW';
					$notify_array['table_name'] = 'git';
					$notify_array['primary_key'] = 'per_git_id';
					$notify_array['primary_key_val'] =$request->per_git_id;
					$notify_array['created_at'] =date('Y-m-d H:i:s');	
					DB::table('notification')->insert($notify_array);*/
				//}
			

				$return=1;
			}
        }
        echo $return;
    }
	
	
	public function gitWiseComments($id)
    {
        //dd($id);
        $Comments = DB::table('git_comments')
                  ->select('*')
                  ->where('git_id', '=', $id)->orderBy('created_at','desc')->get();
        //dd($Comments);
        return view('vendor.gitwise-comments')->with('comments_list',$Comments)->with('git_id', $id);
       
    }
     public function addGitComments(Request $request)
    {
        $return=0;
        
        $action['git_id'] = $request->git_id;
        $action['user_id'] = Auth::user()->id;   
        $action['comments'] = $request->comments;
        $action['created_at'] = date('Y-m-d H:i:s');
        $action['updated_at'] = date('Y-m-d H:i:s');
                
        if(DB::table('git_comments')->insert($action))
        {
            $return=1;
        }
        echo $return;
    }
	
	static function generateGit($git_no)
    {
		if(strlen($git_no)<=6)
        $git_no=str_pad($git_no,7, '0', STR_PAD_LEFT);
		$month=date('m');		
		$year=date('y');
		if($month<04)
		$year=$year-1;
		
		$year_plus=$year+1;
        $git_no = 'GIT/'.$git_no.'/'.$year.'-'.$year_plus;
        return $git_no;
    }
	static function generateChallan($git_no)
    {
		if(strlen($git_no)<=6)
        $git_no=str_pad($git_no,7, '0', STR_PAD_LEFT);
		
        $git_no = 'CH-'.$git_no.'-VN-'.Auth::user()->SLCODE;
        return $git_no;
    }
	
	 public function getVendorCat1(Request $request)
    {
        $user = Auth::user();
        $status=$request->value;
        $cat1_list =  DB::table('git')->selectRaw('products.cat1')
		
    	->leftJoin('V_PURORDDET', 'git.pd_id', '=', 'V_PURORDDET.id')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')		
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
		//->whereRaw('git.ICODE = V_PURORDDET.ICODE')
    	->orderBy('git.created_at', 'desc')
    	->groupBy('products.cat1')
		->get();

        $cat1_html='<option value="">Select Design</option>';
        foreach ($cat1_list as $cat1_html_value)
        {
            $cat1_html=$cat1_html.'<option value="'.$cat1_html_value->cat1.'">'.$cat1_html_value->cat1.'</option>';
        }
        $data['cat1']=$cat1_html;
        return $data;
    }
	
	public function getVendorCat2(Request $request)
    {
        $user = Auth::user();
        $status=$request->value;
        $cat2_list =DB::table('git')->selectRaw('products.cat2')
		
    	->leftJoin('V_PURORDDET', 'git.pd_id', '=', 'V_PURORDDET.id')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')		
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
		//->whereRaw('git.ICODE = V_PURORDDET.ICODE')
    	->orderBy('git.created_at', 'desc')
    	->groupBy('products.cat2')
		->get();

        $cat2_html='<option value="">Select Colour</option>';
        foreach ($cat2_list as $cat2_html_value)
        {
            $cat2_html=$cat2_html.'<option value="'.$cat2_html_value->cat2.'">'.$cat2_html_value->cat2.'</option>';
        }
        $data['cat2']=$cat2_html;
        return $data;
    }

	public function getVendorCat3(Request $request)
    {
        $user = Auth::user();
        $status=$request->value;
        $cat3_list = DB::table('git')->selectRaw('products.cat3')
		
    	->leftJoin('V_PURORDDET', 'git.pd_id', '=', 'V_PURORDDET.id')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')		
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
		//->whereRaw('git.ICODE = V_PURORDDET.ICODE')
    	->orderBy('git.created_at', 'desc')
    	->groupBy('products.cat3')
		->get();

        $cat3_html='<option value="">Select Cat3</option>';
        foreach ($cat3_list as $cat3_html_value)
        {
            $cat3_html=$cat3_html.'<option value="'.$cat3_html_value->cat3.'">'.$cat3_html_value->cat3.'</option>';
        }
        $data['cat3']=$cat3_html;
        return $data;
    }
	  public function getVendorCat4(Request $request)
    {
        $user = Auth::user();
        $status=$request->value;
        $cat4_list = DB::table('git')->selectRaw('products.cat4')
		
    	->leftJoin('V_PURORDDET', 'git.pd_id', '=', 'V_PURORDDET.id')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')		
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
		//->whereRaw('git.ICODE = V_PURORDDET.ICODE')
    	->orderBy('git.created_at', 'desc')
    	->groupBy('products.cat4')
		->get();

        $cat4_html='<option value="">Select Width/Size</option>';
        foreach ($cat4_list as $cat4_html_value)
        {
            $cat4_html=$cat4_html.'<option value="'.$cat4_html_value->cat4.'">'.$cat4_html_value->cat4.'</option>';
        }
        $data['cat4']=$cat4_html;
        return $data;
    }

    public function filterVendorGit(Request $request)
    {
        
        $user = Auth::user();
        $category1=$request->category1;
        $category2=$request->category2;
        $category3=$request->category3;
        $category4=$request->category4;
        $due_from=$request->due_from;
       
        $due_to=$request->due_to;
        
        //$due_from=$request->due_from;
        
        #$query_str=" (git.status = 'Close' OR git.status = 'Pending' OR git.status = 'Rejected' OR git.status = 'Partial Pending' )";
		$query_str=" (git.status = 'Pending' OR git.status = 'Rejected' OR git.status = 'Partial Pending' )";
        
        if(!empty($category1))
            $query_str .=" AND products.cat1 = '".$category1."' ";
        if(!empty($category2))
            $query_str .=" AND products.cat2 = '".$category2."' ";
        if(!empty($category3))
            $query_str .=" AND products.cat3 = '".$category3."' ";
        if(!empty($category4))
            $query_str .=" AND products.cat4 LIKE '".$category4."%' ";

        if(!empty($due_from))
        {

        	$due_from1=explode('/',$due_from);
        	$due_from1=$due_from1[2].'-'.$due_from1[1].'-'.$due_from1[0];
        	
            $query_str .=" AND DATE(git.git_date) >= '".$due_from1."' ";
        }
        if(!empty($due_to))
        {        	
        	$due_to1=explode('/',$due_to);
        	$due_to1=$due_to1[2].'-'.$due_to1[1].'-'.$due_to1[0];    
        	
            $query_str .=" AND DATE(git.git_date) <= '".$due_to1."' ";
        }

        	
        /*if(!empty($due_from))
            $query_str .=" AND products.git_date = '".$due_from."' ";*/
        
     
        // dd(Auth::user()->SLCODE);   
                
        $data['gitList'] = DB::table('git')->selectRaw('products.cat1,products.cat2,products.cat3,products.cat4,V_PURORDDET.created_at,V_PURORDDET.ORDQTY,V_PURORDDET.RCQTY,V_PURORDDET.CNLQTY,V_PURORDDET.OQTY,git.*,(select count(*) from git g where g.git_no=git.git_no) as rowspan')
		
    	->leftJoin('V_PURORDDET', 'git.pd_id', '=', 'V_PURORDDET.id')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')		
		->whereRaw('purchase_order.PCODE ='.Auth::user()->SLCODE)
		->whereRaw($query_str)
		//->whereRaw('git.ICODE = V_PURORDDET.ICODE')
		->orderBy('git.git_no', 'desc')	
		->orderBy('git.status', 'asc')
    	->orderBy('git.updated_at', 'desc')
		->paginate(20);
		if(!empty($data['gitList']) && count($data['gitList'])>0)
        {
            foreach($data['gitList'] as $reqst)
            {
				//-------------------GIT Comments-------------		
                $git_comments =DB::table('git_comments')->where('git_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
				if(!empty($git_comments) && count($git_comments)>0)
				$reqst->git_comments=$git_comments;
				
				//-------------------Prev GIT Qty-------------	
				/*$prev_gitqty = DB::table('git')->select(DB::raw('sum(git.git_qty) as pgit_qty'))
				->where('git.ORDCODE', '=',$reqst->ORDCODE)
				->where('git.ICODE', '=',$reqst->ICODE)
				->where('git.SLCODE','=',Auth::user()->SLCODE)
				->where('git.created_at','<=',$reqst->created_at)
				->get();
                $reqst->prev_git_qty=$prev_gitqty;	*/
				
				//------------------SB Note-------------
				$sb = DB::table('git')->select(DB::raw('count(git.id) as cn_count'))
				->where('git.git_no', '=',$reqst->git_no)
				->where('git.git_rcv', '>',0)
				->where(DB::raw('git.git_rcv - git.git_qty'),'<',0)
				->first();
				$sbc=0;
				if(!empty($sb->cn_count) && (count($sb->cn_count)>0))
				$sbc=1;
				$reqst->is_cn=$sbc;
						
            }
        }

        


        return view('vendor.vendor-all-git-filter',$data);
    }
	
	public function chalanNoAvailability(Request $request)
    {
        $SLCODE = Auth::user()->SLCODE;
        $challan_no=$request->challan_no;
		$return=1;
        $challan_no_availability =DB::table('git')->select('git.id')
    	->leftJoin('V_PURORDDET', 'git.pd_id', '=', 'V_PURORDDET.id')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
		->where('purchase_order.PCODE','=',$SLCODE)
		->where('git.challan_no','=',$challan_no)
		->get();
		if(!empty($challan_no_availability) && count($challan_no_availability)>0)
			$return=0;
		
        return $return;
    }

     public function viewArchiveVendorGit()
    {		
    	return view('vendor.vendor-git-archive');
    }

    public function filterArchiveVendorGit(Request $request)
    {
        
        $user = Auth::user();
        $category1=$request->category1;
        $category2=$request->category2;
        $category3=$request->category3;
        $category4=$request->category4;
        $due_from=$request->due_from;
       
        $due_to=$request->due_to;
        
        //$due_from=$request->due_from;
        
        #$query_str=" (git.status = 'Close' OR git.status = 'Pending' OR git.status = 'Rejected' OR git.status = 'Partial Pending' )";
		$query_str=" (git.status = 'Rejected' OR git.status = 'Close' )";
        
        if(!empty($category1))
            $query_str .=" AND products.cat1 = '".$category1."' ";
        if(!empty($category2))
            $query_str .=" AND products.cat2 = '".$category2."' ";
        if(!empty($category3))
            $query_str .=" AND products.cat3 = '".$category3."' ";
        if(!empty($category4))
            $query_str .=" AND products.cat4 LIKE '".$category4."%' ";

        if(!empty($due_from))
        {

        	$due_from1=explode('/',$due_from);
        	$due_from1=$due_from1[2].'-'.$due_from1[1].'-'.$due_from1[0];
        	
            $query_str .=" AND DATE(git.git_date) >= '".$due_from1."' ";
        }
        if(!empty($due_to))
        {        	
        	$due_to1=explode('/',$due_to);
        	$due_to1=$due_to1[2].'-'.$due_to1[1].'-'.$due_to1[0];    
        	
            $query_str .=" AND DATE(git.git_date) <= '".$due_to1."' ";
        }

        	
        /*if(!empty($due_from))
            $query_str .=" AND products.git_date = '".$due_from."' ";*/
        
     
        // dd(Auth::user()->SLCODE);   
                
        $data['gitList'] = DB::table('git')->selectRaw('products.cat1,products.cat2,products.cat3,products.cat4,V_PURORDDET.created_at,V_PURORDDET.ORDQTY,V_PURORDDET.RCQTY,V_PURORDDET.CNLQTY,V_PURORDDET.OQTY,git.*,(select count(*) from git g where g.git_no=git.git_no) as rowspan')
		
    	->leftJoin('V_PURORDDET', 'git.pd_id', '=', 'V_PURORDDET.id')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')		
		->whereRaw('purchase_order.PCODE ='.Auth::user()->SLCODE)
		->whereRaw($query_str)
		//->whereRaw('git.ICODE = V_PURORDDET.ICODE')
		->orderBy('git.git_no', 'desc')	
		->orderBy('git.status', 'asc')
    	->orderBy('git.updated_at', 'desc')
		->paginate(20);
		if(!empty($data['gitList']) && count($data['gitList'])>0)
        {
            foreach($data['gitList'] as $reqst)
            {
				//-------------------GIT Comments-------------		
                $git_comments =DB::table('git_comments')->where('git_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
				if(!empty($git_comments) && count($git_comments)>0)
				$reqst->git_comments=$git_comments;
				
				//-------------------Prev GIT Qty-------------	
				/*$prev_gitqty = DB::table('git')->select(DB::raw('sum(git.git_qty) as pgit_qty'))
				->where('git.ORDCODE', '=',$reqst->ORDCODE)
				->where('git.ICODE', '=',$reqst->ICODE)
				->where('git.SLCODE','=',Auth::user()->SLCODE)
				->where('git.created_at','<=',$reqst->created_at)
				->get();
                $reqst->prev_git_qty=$prev_gitqty;	*/
				
				//------------------SB Note-------------
				$sb = DB::table('git')->select(DB::raw('count(git.id) as cn_count'))
				->where('git.git_no', '=',$reqst->git_no)
				->where('git.git_rcv', '>',0)
				->where(DB::raw('git.git_rcv - git.git_qty'),'<',0)
				->first();
				$sbc=0;
				if(!empty($sb->cn_count) && (count($sb->cn_count)>0))
				$sbc=1;
				$reqst->is_cn=$sbc;
						
            }
        }

        


        return view('vendor.vendor-git-archive-filter',$data);
    }
    public function getVendorArchiveCat1(Request $request)
    {
        $user = Auth::user();
        $status=$request->value;
        $cat1_list =  DB::table('git')->selectRaw('products.cat1')
		
    	->leftJoin('V_PURORDDET', 'git.pd_id', '=', 'V_PURORDDET.id')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')		
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
		->where('git.status','=','Close')
		//->whereRaw('git.ICODE = V_PURORDDET.ICODE')
    	->orderBy('git.created_at', 'desc')
    	->groupBy('products.cat1')
		->get();

        $cat1_html='<option value="">Select Design</option>';
        foreach ($cat1_list as $cat1_html_value)
        {
            $cat1_html=$cat1_html.'<option value="'.$cat1_html_value->cat1.'">'.$cat1_html_value->cat1.'</option>';
        }
        $data['cat1']=$cat1_html;
        return $data;
    }

    public function getVendorArchiveCat2(Request $request)
    {
        $user = Auth::user();
        $status=$request->value;
        $cat2_list =DB::table('git')->selectRaw('products.cat2')
		
    	->leftJoin('V_PURORDDET', 'git.pd_id', '=', 'V_PURORDDET.id')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')		
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
		->where('git.status','=','Close')
		//->whereRaw('git.ICODE = V_PURORDDET.ICODE')
    	->orderBy('git.created_at', 'desc')
    	->groupBy('products.cat2')
		->get();

        $cat2_html='<option value="">Select Colour</option>';
        foreach ($cat2_list as $cat2_html_value)
        {
            $cat2_html=$cat2_html.'<option value="'.$cat2_html_value->cat2.'">'.$cat2_html_value->cat2.'</option>';
        }
        $data['cat2']=$cat2_html;
        return $data;
    }

	public function getVendorArchiveCat3(Request $request)
    {
        $user = Auth::user();
        $status=$request->value;
        $cat3_list = DB::table('git')->selectRaw('products.cat3')
		
    	->leftJoin('V_PURORDDET', 'git.pd_id', '=', 'V_PURORDDET.id')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')		
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
		->where('git.status','=','Close')
		//->whereRaw('git.ICODE = V_PURORDDET.ICODE')
    	->orderBy('git.created_at', 'desc')
    	->groupBy('products.cat3')
		->get();

        $cat3_html='<option value="">Select Cat3</option>';
        foreach ($cat3_list as $cat3_html_value)
        {
            $cat3_html=$cat3_html.'<option value="'.$cat3_html_value->cat3.'">'.$cat3_html_value->cat3.'</option>';
        }
        $data['cat3']=$cat3_html;
        return $data;
    }
	  public function getVendorArchiveCat4(Request $request)
    {
        $user = Auth::user();
        $status=$request->value;
        $cat4_list = DB::table('git')->selectRaw('products.cat4')
		
    	->leftJoin('V_PURORDDET', 'git.pd_id', '=', 'V_PURORDDET.id')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')		
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
		->where('git.status','=','Close')
		//->whereRaw('git.ICODE = V_PURORDDET.ICODE')
    	->orderBy('git.created_at', 'desc')
    	->groupBy('products.cat4')
		->get();

        $cat4_html='<option value="">Select Width/Size</option>';
        foreach ($cat4_list as $cat4_html_value)
        {
            $cat4_html=$cat4_html.'<option value="'.$cat4_html_value->cat4.'">'.$cat4_html_value->cat4.'</option>';
        }
        $data['cat4']=$cat4_html;
        return $data;
    }
}
