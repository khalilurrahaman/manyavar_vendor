<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\Rfq;
use App\Models\Quetes;
use DB;
Use Auth;

class VendorRfqRequestController extends Controller
{
    public function index()
    {
		
		$rfq_list = DB::table('rfqs')
		->join('rfq_details', 'rfqs.id', '=', 'rfq_details.rfq_id')
		->selectRaw('rfqs.*,rfq_details.id as rfq_details_id')
		->where('rfq_details.vendor_id', '=',Auth::user()->id)
		->where('rfqs.status','=',1)->orderBy('rfqs.created_at','desc')
		->get();
		if(!empty($rfq_list))
		{
			foreach($rfq_list as $rfq)
			{
				$product_details =DB::table('products')->where('id', '=',$rfq->product_id)->first();
				if(!empty($product_details))
					$rfq->product_details=$product_details;				

				$rfq_details =DB::table('rfq_details')->where('vendor_id', '=',Auth::user()->id)->where('rfq_id', '=',$rfq->id)->first();		
				if(!empty($rfq_details))
					$rfq->rfq_details=$rfq_details;

				$rfq_quetes =DB::table('rfq_quetes')->where('vendor_id', '=',Auth::user()->id)->where('rfq_id', '=',$rfq_details->rfq_id)->orderBy('created_at','desc')->get();
				if(!empty($rfq_quetes))
					$rfq->rfq_quetes=$rfq_quetes;

				$rfq_comments =DB::table('rfq_comments')->where('rfq_details_id', '=',$rfq->rfq_details_id)->orderBy('created_at','desc')->get();
				if(!empty($rfq_comments) && count($rfq_comments)>0)
					$rfq->rfq_comments=$rfq_comments;	
				
				
			}
		}
	
		//dd($rfq_list);
    	return view('vendor.vendor-rfq-request-list')->with('rfq_list',$rfq_list);
    }
	
	public function addRfqQuotation(Request $request)
    {

    	$rfq_data=Rfq::select('req_id')->where('id','=',$request->rfq_id)->first();

		$return=0;
        $quetes = new Quetes;
		$quetes->quete_id =  $this->generateQU();
        $quetes->rfq_id = $request->rfq_id;
        $quetes->rfq_details_id = $request->rfq_details_id;
		$quetes->vendor_id = Auth::user()->id;	
        $quetes->price = $request->rfq_price;
		$quetes->width = $request->rfq_width;
        $quetes->status = 1;
        $quetes->created_at = date('Y-m-d H:i:s');
        $quetes->updated_at = date('Y-m-d H:i:s');
        if($quetes->save())
		{
			$notify_array['message'] = 'A Quotation has been Added for :'.$rfq_data->req_id;
			$notify_array['type'] = ucfirst($request->rfq_id).'QuotationAdded';
			$notify_array['red_url'] = 'all-rfq-request';
			$notify_array['sender'] = Auth::user()->id;
			$notify_array['receiver'] =1;
			$notify_array['status'] = 'NEW';
			$notify_array['table_name'] = 'rfqs';
			$notify_array['primary_key'] = 'id';
			$notify_array['primary_key_val'] =$request->rfq_id;
			$notify_array['created_at'] =date('Y-m-d H:i:s');	
			DB::table('notification')->insert($notify_array);
			$return=1;
		}
		echo $return;
    }
	static function generateQU()
    {
        $last_req = Quetes::selectRaw('max(id) as id')->first();
        $req_id=str_pad($last_req->id+1, 6, '0', STR_PAD_LEFT);
        $req_id = 'QU/'.$req_id;
        return $req_id;
    }
	
	
	public function quotationWiseComments(Request $request)
    {    	
    	$rfq=DB::table('rfqs')
    	->leftJoin('rfq_details', 'rfqs.id', '=', 'rfq_details.rfq_id')
		->select('rfqs.status')
		->where('rfq_details.id', '=',$request->quotation_id)
		->first();
	
		$data['status']=$rfq->status;

		$data['comments_list'] = DB::table('rfq_comments')
		->select('*')
		->where('rfq_details_id', '=',$request->quotation_id)
		->orderBy('id','asc')
		->get();



		$data['quotation_id']=$request->quotation_id;

		//dd($comments_list);
        return view('vendor.quotationwise-comments',$data);
    }
	
	public function addQuotationComments(Request $request)
    {
		
		$return=0;

		$quote=Quetes::select('id')->where('rfq_details_id', '=', $request->quotation_id)->orderBy('id','asc')->first();
		if(count($quote)>0)
        {
	        $quotation['quete_id'] = $quote->id;
	        $quotation['rfq_details_id'] = $request->quotation_id;
			$quotation['user_id'] = Auth::user()->id;	
	        $quotation['comments'] = $request->comments;
	        $quotation['created_at'] = date('Y-m-d H:i:s');
	        $quotation['updated_at'] = date('Y-m-d H:i:s');
					
	        if(DB::table('rfq_comments')->insert($quotation))
			{
				$return=1;
			}
		}
		echo $return;
    }
     public function rejectedrfq()
    {
		$rfq_rejected_list = DB::table('rfqs')
		->join('rfq_details', 'rfqs.id', '=', 'rfq_details.rfq_id')
		->selectRaw('rfqs.*,rfq_details.id as rfq_details_id')
		->where('rfq_details.vendor_id', '=',Auth::user()->id)
		->where('rfqs.status', '=',0)
		->orderBy('rfqs.status','desc')->orderBy('rfqs.created_at','desc')
		->get();

		if(!empty($rfq_rejected_list))
		{
			foreach($rfq_rejected_list as $rfq)
			{
				$product_details =DB::table('products')->where('id', '=',$rfq->product_id)->first();
				if(!empty($product_details))
					$rfq->product_details=$product_details;

				$rfq_details =DB::table('rfq_details')->where('vendor_id', '=',Auth::user()->id)->where('rfq_id', '=',$rfq->id)->first();							
				if(!empty($rfq_details))
					$rfq->rfq_details=$rfq_details;
				

				$rfq_quetes =DB::table('rfq_quetes')->where('vendor_id', '=',Auth::user()->id)->where('rfq_id', '=',$rfq_details->rfq_id)->orderBy('created_at','desc')->get();
				if(!empty($rfq_quetes))
					$rfq->rfq_quetes=$rfq_quetes;

				$rfq_comments =DB::table('rfq_comments')->where('rfq_details_id', '=',$rfq->rfq_details_id)->orderBy('created_at','desc')->get();
				if(!empty($rfq_comments) && count($rfq_comments)>0)
					$rfq->rfq_comments=$rfq_comments;	

			}
		}		
		
    	return view('vendor.vendor-rejected-rfq')->with('rfq_rejected_list',$rfq_rejected_list);
    }
}
