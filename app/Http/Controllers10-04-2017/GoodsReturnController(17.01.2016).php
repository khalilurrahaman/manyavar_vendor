<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Product;
use App\Models\GoodsReturns;
use DB;
use Auth;
use Redirect;


class GoodsReturnController extends Controller
{
    public function viewList()
    {
        $return_list = DB::table('goods_returns')
            
            ->join('users', 'users.id', '=', 'goods_returns.vendor_id')->where('goods_returns.status','=',1)
            ->orderBy('goods_returns.id','desc')
            ->select('goods_returns.id','goods_returns.category1','goods_returns.category2','goods_returns.comments','goods_returns.return_quantity','goods_returns.product_id','goods_returns.docket_no','goods_returns.prn_no','goods_returns.prn_date','goods_returns.courier_name','goods_returns.vendor_comments','goods_returns.status','users.name','goods_returns.image','goods_returns.reason')->paginate(20);
        //dd($return_list);



            $data['cat1_list'] = DB::table('goods_returns')   
        ->select('goods_returns.category1')        
        ->where('goods_returns.status','=',1)
        ->groupBy('goods_returns.category1') 
        ->orderBy('goods_returns.category1','desc')
        ->get();
        
        
        $data['cat2_list'] = DB::table('goods_returns')   
       
        ->select('goods_returns.category2')        
        ->where('goods_returns.status','=',1)
        ->groupBy('goods_returns.category2') 
        ->orderBy('goods_returns.category2','desc')
        ->get();
        
        
        $data['vendor_name_list'] = DB::table('goods_returns')   
        
        ->join('users', 'goods_returns.vendor_id', '=', 'users.id') 
        ->select('users.id','users.name')        
        ->where('goods_returns.status','=',1)
        ->groupBy('users.id') 
        ->orderBy('users.id','desc')
        ->get();
        




        /*$cat1_list = Product::where('cat1', '<>','')->groupBy('cat1')->pluck('cat1');
        $cat2_list = Product::where('cat2', '<>','')->groupBy('cat2')->pluck('cat2');
        dd($cat1_list);*/
        return view('admin.googs-return-list',$data)->with('returnList',$return_list);
    }
	public function closedReturn()
    {
        $return_list = DB::table('goods_returns')
        	
			->join('users', 'users.id', '=', 'goods_returns.vendor_id')->where('goods_returns.status','=',0)
            ->orderBy('goods_returns.id','desc')
        	->select('goods_returns.id','goods_returns.category1','goods_returns.category2','goods_returns.comments','goods_returns.return_quantity','goods_returns.product_id','goods_returns.docket_no','goods_returns.prn_no','goods_returns.prn_date','goods_returns.courier_name','goods_returns.vendor_comments','goods_returns.status','users.name','goods_returns.image','goods_returns.reason')->paginate(20);
        //dd($return_list);
        return view('admin.goods-return-closed')->with('returnList',$return_list);
    }

    public function viewAddform()
    {
    	$vendor_list = User::select('id','name')->where('type','=','vendor')->where('status', '=', 1)->get();
    	$product_list = Product::select('id','image','cat1','cat2','cat3','cat4')->where('cat1', '<>','')->inRandomOrder()->limit(10)->get();
		$cat1_list = Product::select('id','cat1')->where('cat1', '<>','')->groupBy('cat1')->get();
		$cat2_list = Product::select('id','cat2')->where('cat2', '<>','')->groupBy('cat2')->get();
		$cat3_list = Product::select('id','cat3')->where('cat3', '<>','')->groupBy('cat3')->get();
		$cat4_list = Product::select('id','cat4')->where('cat4', '<>','')->groupBy('cat4')->get();
		
		
		$cat1_list1 = Product::where('cat1', '<>','')->groupBy('cat1')->pluck('cat1');
        $cat2_list2 = Product::where('cat2', '<>','')->groupBy('cat2')->pluck('cat2');
    	return view('admin.googs-return-add')->with('allVendor', $vendor_list)->with('allProduct', $product_list)->with('cat1_list', $cat1_list)->with('cat2_list', $cat2_list)->with('cat3_list', $cat3_list)->with('cat4_list', $cat4_list)->with('cat1_list1', $cat1_list1)->with('cat2_list2', $cat2_list2);
    }

    public function postAddreturn(Request $request)
    {
        $user = Auth::user();

        $rfq_return = new GoodsReturns;
        //$rfq_return->product_id = trim($request->product_id);
		$rfq_return->vendor_id = trim($request->vendor_id);
		$rfq_return->docket_no = trim($request->docket_no);
		$rfq_return->prn_no = trim($request->prn_no);
		$rfq_return->prn_date =date('Y-m-d', strtotime(str_replace('/', '-', $request->prn_date)));
		$rfq_return->courier_name = trim($request->courier_name);
        $rfq_return->comments = trim($request->comment);
        $rfq_return->image = trim($request->image);
        $rfq_return->category1 = trim($request->cat_1);
        $rfq_return->category2 = trim($request->cat_2);
        $rfq_return->reason = trim($request->remarks);
		$rfq_return->return_quantity = trim($request->quantity);
		
        $rfq_return->createdby = $user->id;
        $rfq_return->modifiedby = $user->id;
        $rfq_return->status = 1;
        if($rfq_return->save())
        {
            echo "s";
        }
        else
        {
            echo "ds";
        }



        return Redirect::route('goods-returns-list')->withErrors(['Goods has been added successfully for return']);
    }

    public function goodsReceiveList()
    {
        $user = Auth::user();
        $receive_list = DB::table('goods_returns')
            ->join('users', 'users.id', '=', 'goods_returns.vendor_id')->where('goods_returns.vendor_id', '=', $user->id)
            ->orderBy('goods_returns.status','desc')->orderBy('goods_returns.id','desc')
            ->select('goods_returns.id','goods_returns.category1','goods_returns.category2','goods_returns.comments','goods_returns.return_quantity','goods_returns.product_id','goods_returns.docket_no','goods_returns.prn_no','goods_returns.prn_date','goods_returns.courier_name','goods_returns.vendor_comments','goods_returns.status','users.name','goods_returns.image','goods_returns.reason')->paginate(20);
        //dd($receive_list);
        return view('vendor.goods-received-list')->with('receiveList',$receive_list);
    }

    public function goodsReceiveAccept(Request $request)
    {
        $rfqs = GoodsReturns::where('id','=', $request->returntId)->update(['status' =>0,'vendor_comments'=>trim($request->vendor_comment)]);

        return Redirect::route('goods-receive-list')->withErrors(['Return request has been accepted successfully']);
    }
    public function addGoodsFile(Request $request)
    {
         $file = $request->file('file');

        $imageName = time().'_'.$request->file('file')->getClientOriginalName();
        if($file->move(public_path('upload/temp'), $imageName)){
            $data['file_name']=$imageName;
            return $data;
        }
    }
    public function filterGoodsReturn(Request $request)
    {
        //dd(11);
        $cat1=$request->cat1;
        $cat2=$request->cat2;
        $name=$request->name;
        
        $query_str=" goods_returns.status = '1' ";
        
        if(!empty($cat1))
            $query_str .=" AND goods_returns.category1 = '".$cat1."' ";
        if(!empty($cat2))
            $query_str .=" AND goods_returns.category2 = '".$cat2."' ";
        if(!empty($name))
            $query_str .=" AND goods_returns.vendor_id = '".$name."' ";
        
     
                
                
        $return_list = DB::table('goods_returns')
            
            ->join('users', 'users.id', '=', 'goods_returns.vendor_id')->whereRaw($query_str)
            ->orderBy('goods_returns.id','desc')
            ->select('goods_returns.id','goods_returns.category1','goods_returns.category2','goods_returns.comments','goods_returns.return_quantity','goods_returns.product_id','goods_returns.docket_no','goods_returns.prn_no','goods_returns.prn_date','goods_returns.courier_name','goods_returns.vendor_comments','goods_returns.status','users.name','goods_returns.image','goods_returns.reason')->paginate(20);
            return view('admin.goods-return-filter')->with('returnList',$return_list);
    }
}