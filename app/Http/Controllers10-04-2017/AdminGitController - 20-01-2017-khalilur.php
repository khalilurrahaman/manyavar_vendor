<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Redirect;
use App\Models\GIT;
use App\Models\purchesOrder;
class AdminGitController extends Controller
{
    public function viewGit()
    {
		$gitList = DB::table('git')->selectRaw('git.id,git.git_no,users.name,products.cat1,products.cat2,git.ORDCODE,git.ICODE,git.created_at,products.cat3,V_PURORDDET.ORDQTY,V_PURORDDET.RCQTY,V_PURORDDET.CNLQTY,V_PURORDDET.OQTY,git.*,(select count(*) from git g where g.challan_no=git.challan_no ) as rowspan')
    	->leftJoin('V_PURORDDET', 'V_PURORDDET.id', '=', 'git.pd_id')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
    	->join('users', 'purchase_order.PCODE', '=', 'users.SLCODE')		
		//->whereRaw('git.ICODE = V_PURORDDET.ICODE')
    	->where('git.status', '!=','Close')
    	->orderBy('git.updated_at', 'desc')
		->paginate(30);
        

        if(!empty($gitList) && count($gitList)>0)
        {
            foreach($gitList as $reqst)
            {
                $git_comments =DB::table('git_comments')->where('git_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
                        if(!empty($git_comments) && count($git_comments)>0)
                        $reqst->git_comments=$git_comments;
						
				$prev_gitqty = DB::table('git')->select(DB::raw('sum(git.git_qty) as pgit_qty'))
				->where('git.ORDCODE', '=',$reqst->ORDCODE)
				->where('git.ICODE', '=',$reqst->ICODE)
				//->where('git.SLCODE','=',Auth::user()->SLCODE)
				->where('git.created_at','<=',$reqst->created_at)
				->get();
                $reqst->prev_git_qty=$prev_gitqty;
				
                }
            }

		#dd($gitList);
    	return view('admin.vendor-all-git-list')->with('gitList',$gitList);
    }

    public function markCompleteGit($id)
    {
		
        $close_git=GIT::where('id','=',$id)->first();

        if($close_git)
        {
            $poStatus = DB::table('V_PURORDDET')->where('V_PURORDDET.ORDCODE', '=',$close_git->ORDCODE)->where('V_PURORDDET.ICODE', '=',$close_git->ICODE)->update(['STATUS'=>'CLOSED','updated_at'=>date('Y-m-d H:i:s')]);

            $isexist=DB::table('V_PURORDDET')->selectRaw('count(*) as num')->whereRaw('V_PURORDDET.ORDCODE = "'.$close_git->ORDCODE.'" AND V_PURORDDET.STATUS ="OPEN"')->groupBy('V_PURORDDET.ORDCODE')->first();

            if($isexist->num==0)
            {
                $poStatus = DB::table('purchase_order')->where('purchase_order.ORDCODE', '=',$close_git->ORDCODE)->update(['status'=>'CLOSED','updated_at'=>date('Y-m-d H:i:s')]);
            }

            if(DB::table('git')->where('git.ORDCODE', '=',$close_git->ORDCODE)->where('git.ICODE', '=',$close_git->ICODE)->update(['status'=>'Close','updated_at'=>date('Y-m-d H:i:s')]))
			{
				
					$gitDtls = DB::table('git')->selectRaw('users.id')
					->leftJoin('V_PURORDDET', 'V_PURORDDET.id', '=', 'git.pd_id')
					->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
					->join('users', 'purchase_order.PCODE', '=', 'users.SLCODE')
					->where('git.id', '!=',$id)
					->first();
		
		
					$notify_array['message'] = 'Git has been Mark Completed';
					$notify_array['type'] = 'GitMarkCompleted';
					$notify_array['sender'] = Auth::user()->id;
					$notify_array['receiver'] = $gitDtls->id;
					$notify_array['status'] = 'NEW';
					DB::table('notification')->insert($notify_array);
			}



            return Redirect::route('all_git_list');
        }
        else
        {
            return Redirect::route('all_git_list');
        }
    	return Redirect::route('all_git_list');
    }
	
	
	public function resolveGitRecords(Request $request)
    {
		$return=0;
        $git['per_git_id'] = $request->per_git_id;
        $git['admin_resolve_remarks'] = $request->resolve_remarks;
        $git['admin_is_resolve'] = 1;
                
        if(DB::table('git')->where('per_git_id', $request->per_git_id)->update($git))
        {
            $return=1;
        }
        echo $return;
	
	}
	
	
    public function viewAllVendorGit()
    {
    	return view('vendor.vendor-all-git');
    }

    public function adminGitComment($id)
    {
        //dd($id);
        $Comments = DB::table('git_comments')
                  ->select('*')
                  ->where('git_id', '=', $id)->orderBy('created_at','desc')->get();
        //dd($Comments);
        return view('admin.admin-git-comments')->with('comments_list',$Comments)->with('git_id', $id);
       
    }
     public function addGitComment(Request $request)
    {
        $return=0;
        
        $action['git_id'] = $request->git_id;
        $action['user_id'] = Auth::user()->id;   
        $action['comments'] = $request->comments;
        $action['created_at'] = date('Y-m-d H:i:s');
        $action['updated_at'] = date('Y-m-d H:i:s');
                
        if(DB::table('git_comments')->insert($action))
        {
            $return=1;
        }
        echo $return;
    }
}
