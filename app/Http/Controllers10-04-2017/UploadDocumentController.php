<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use auth;
use App\User;
use App\Models\DocumentType;
use App\Models\Document;
use App\Models\DocumentFiles;
use Redirect;
use DB;

class UploadDocumentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data=DocumentType::where('status','=',1)->get();
        $user=Auth::user();
        return view('vendor.add-document')->with('data',$data);
    }
    public function documentTypeList()
    {
         if($this->userAccessibilityCheck('document-type-list'))
	{
        $data=DocumentType::where('status','=',1)->orderBy('id')->get();
        $user=Auth::user();
        return view('admin.document_type_list')->with('document_type',$data);
    }else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);
		    
       
    }
    public function documentType()
    {
        
        return view('admin.add-document-type');
    }
    public function documentList()
    {
		if($this->userAccessibilityCheck('document-list'))
		{
			$document_list=Document::where('status','=',1)->orderBy('id')->paginate(10);
			$user=Auth::user();
			#dd($document_list);
			return view('admin.document_list')->with('document_list',$document_list);
		}
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);    
        
    }
	
	public function statusDeactive($id)
    {
        $document=Document::where('id','=',$id)->first();
        $document->status=0;
        if($document->save())
        {
           return redirect::route('document-list')->withErrors(["Documents Archieved Successfully"]);
        }
        else
        {
            return redirect::route('document-list');
        }

    }
	
    public function vendorDocumentList()
    {
        $user=Auth::user();
        $vendor_id=$user->id;
        $document_list=Document::where('vendor_id','=',$vendor_id)->orderBy('id')->paginate(10);
       

        $user=Auth::user();

        return view('vendor.document_list')->with('document_list',$document_list);
    }
    public function insertDocument(Request $request)
    {
        $user=Auth::user();
        $vendor_id=$user->id;
        $type=$request->type;
        $month=$request->month;
        $remarks=$request->remarks;
        $created_by=$vendor_id;
        $status=1;

        $document=new Document;
        $document->doc_type=$type;
        $document->vendor_id=$vendor_id;
        $document->month=$month;
        $document->remarks=$remarks;            
        $document->created_by=$created_by;
        if($document->save())
        {
            $file=$request->file;
            $j=count($file);
            for($i=0;$i<$j;$i++)
            {
                if($file[$i]->getClientSize()>3145728)
                {
                    $delete_doc=Document::where('id','=',$document->id);
                    $delete_doc->delete(); 
                    $delete_doc_file=DocumentFiles::where('document_id','=',$document->id);
                    $delete_doc_file->delete();
                    return redirect()->back()->withErrors(['File size greater than 3MB ! ']);
                }
                else
                {
                    $DocumentFiles=new DocumentFiles;
                    $DocumentFiles->document_id=$document->id;
                    $file[$i]->move(base_path() . '/public/Document_upload/',time().$file[$i]->getClientOriginalName() );                      
                    $DocumentFiles->document_file=time().$file[$i]->getClientOriginalName();
                    $DocumentFiles->save(); 
                }

                
            }
        }
        return Redirect('vendor/document-listing')->withErrors(["Document Added Successfully"]);
    }
    public function insertDocumentType(Request $request)
    {

        $this->validate($request,[
        'type_name'=>'required|unique:document_type_master',        
        'description'=>'required'
        ]);
        $user=Auth::user();
        $type=$request->type_name;
        $description=$request->description;
        $status=1;
        $user=Auth::user();
        $created_by=$user->id;

        $document=new DocumentType;
        $document->type_name=$type;
        $document->description=$description;
                    
        $document->created_by=$created_by;
        if($document->save())
        {
           return Redirect('admin/document-type-list')->withErrors(["Document Type Added Successfully"]);
        }
        
    }
    public function deleteDocumentType(Request $request)
    {
        $id=$request->id;
        $status=0;
        $user=Auth::user();
        $updated_by=$user->id;

        $document=DocumentType::where('id','=',$id)->first();

        $document->status=$status;                    
        $document->updated_by=$updated_by;
        if($document->save())
        {
           return Redirect('admin/document-type-list')->withErrors(["Document Type Deleted Successfully"]);
        }
    }

    public function editDocumentType(Request $request)
    {
        $id=$request->id;
        $document=DocumentType::where('id','=',$id)->first();
        return view('admin.edit-document-type')->with('data',$document);
    }
    public function updateDocumentType(Request $request)
    {
        $this->validate($request,[
        'type_name'=>'required|unique:document_type_master,type_name,'.$request->id,        
        'description'=>'required'
        ]);
        

        $id=$request->id;
        $type=$request->type_name;
        $description=$request->description;
        $user=Auth::user();
        $updated_by=$user->id;

        $document=DocumentType::where('id','=',$id)->first();
        $document->type_name=$type;
        $document->description=$description;
        $document->updated_by=$updated_by;
        if($document->save())
        {
           return Redirect('admin/document-type-list')->withErrors(["Document Type Updated Successfully"]);
        }
    }

    public function archieveDocumentList()
    {
		
		if($this->userAccessibilityCheck('archieve-document-list'))
		{
			$document_list=Document::where('status','=',0)->orderBy('id')->paginate(10);
			$user=Auth::user();
			#dd($document_list);
			return view('admin.document_archieve_list')->with('document_list',$document_list);
		}
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);    
        
    }
    


    
}
