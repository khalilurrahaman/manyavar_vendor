<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use auth;
use App\User;
use App\Models\Forgot;
use Redirect;
class ForgotController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if(!empty((Auth::user())))
        {
           return Redirect::route('login');
        }
       
        return view('auth.passwords.forgot-password');
    }
    public function checkUser(Request $request)
    {
        $this->validate($request,[
        'username'=>'required'
        ]);
        $username=$request->username;
        $user=User::where('username','=',$username)->first();
        if($user)
        {
            $delete_forgot=Forgot::where('email','=',$user['email'])->delete();

            $forgot=new Forgot;
            $forgot->email=$user['email'];
            $forgot->old_password=$user['password'];
            
            //$token=md5(time());
            //$forgot->token=$token;

            $token=$this->generateRandomString(50);
			#echo $token;exit;
            $forgot->token=$token;
            $forgot->save();

            $email_encode=base64_encode($user['email']);


           $message ="<html>
                    <head>
                    <title>Reset Password</title>
                    </head>
                    <body>
                    <a href=".url('/reset-password?em='.$email_encode.'&tk='.$token ).">Clik here for reset your password.</a>
                    </body>
                    </html>";
                    
        
            $this->actionSendEmail($user['email'],'Manyavar Vendor Forgot Password',$message);


             return Redirect::route('forgot-password')->withErrors(['Please check your email to reset password']);
/*             return Redirect::route('reset-password?em='.$email_encode.'&tk='.$token);
*/        }
        else
        {
            return Redirect::route('forgot-password')->withErrors(['Invalid Username']);
        }
    }

	public function actionSendEmail($to_email,$subject,$message){

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <skkhalilur.rahaman@bluehorse.in>' . "\r\n";
        $headers .= 'Cc: skkhalilur.rahaman@bluehorse.in' . "\r\n";
        mail($to_email,$subject,$message,$headers);
    }

	public function generateRandomString($length = 10) 
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		
		for ($i = 0; $i < $length; $i++) 
		{
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	
    public function resetPassword(Request $request)
    {
   /* public function resetPassword($email)
    {
      */  
        $email=base64_decode($request->em);
        $token=$request->tk;
        $user=Forgot::where('email','=',$email)->where('token','=',$token)->first();
        if($user)
        {

            return view('auth.passwords.reset-password')->with('user',$user);
        }
        else
        {
            return view('auth.passwords.reset-password-msg')->with('success',2)->withErrors('Password reset failed !');
        }
        

    }
    public function submitResetPassword(Request $request)
    {
        $this->validate($request,[
        'new_password'=>'required',
        'confirm_password'=>'required|same:new_password'
        ]);

        $user=User::where('email','=',$request->email)->first();
        if($user)
        {
            $user->password=md5($request->new_password);

            if($user->save())
            {
                $forgot=Forgot::where('email','=',$request->email)->first();
                $forgot->token=0;
                $forgot->save();
                return Redirect::route('reset-successfull');
            }
            else
            {
                return view('auth.passwords.reset-password-msg')->with('success',2)->withErrors('Password reset failed !');
            }
        }
        else
        {
            return view('auth.passwords.reset-password-msg')->with('success',2)->withErrors('Password reset failed !');
        }



    }

    public function resetSuccess()
    {
        return view('auth.passwords.reset-password-msg')->with('success',1)->withErrors('You have successfully reset your password');
    }


    


    
}
