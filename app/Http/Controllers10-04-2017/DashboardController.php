<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use auth;
use DB;
use Carbon\Carbon;
class DashboardController extends Controller
{
    public function adminDashboard()
    {
    	$user=auth::user();
		#dd($user);
    	if($user)
    	{	    		    	
	        $request_list = DB::table('po_action')        
	        ->select('po_action.*')
	        ->where('status','=','new')
	        ->orderBy('priority','asc') 
	        ->orderBy('created_at','desc')         
	        ->paginate(10);	  

	        // $select_new_po = DB::table('purchase_order')->select('purchase_order.PONO')
	        // ->where('purchase_order.status', '=','OPEN')
	        // ->orderBy('purchase_order.ORDDT', 'desc')
	        // ->limit(50)
	        // ->get();

	        if(!empty($request_list) && count($request_list)>0)
	        {
	            foreach($request_list as $reqst)
	            {	                	                
	                $ven_name =DB::table('users')->select('name')->where('id', '=',$reqst->user_id)->first();
                    if(!empty($ven_name))
                    	$reqst->ven_name=$ven_name->name;	                

                }

                /*foreach($request_list as $reqst)
	            {	                	                
	                $ven_name =DB::table('purchase_order')->select('ORDDT')->where('PONO', '=',$reqst->po_no)->first();
                    if(!empty($ven_name))
                    	$reqst->ORDDT=$ven_name->ORDDT;	                

                }*/

            }	        


	        $data['request_list']=$request_list;

	        $where='notification.status="NEW" AND notification.receiver="'.Auth::user()->id.'" AND DATE(notification.created_at) > (NOW() - INTERVAL 7 DAY)';

			$notificationList = DB::table('notification')->select('notification.*','users.name as sender_name')
			->leftJoin('users','users.id', '=', 'notification.sender')
	        ->whereRaw($where)
	        ->orderBy('notification.created_at', 'desc')
	        ->take(8)
	        ->get();
			##dd($notificationList);
	        $total = DB::table('notification')->selectRaw('count(*) as num')
	        ->whereRaw($where)
	        ->first();        
	        if(count($total)>0)
	            $notificationList->tot=$total->num;

	        $data['notificationList']=$notificationList;	        
			
	        return view('admin.dashboard',$data); 
	    }else
	    {
	    	return Redirect('/');
	    }
    }

    public function notification()
    {
    	$where='notification.receiver="'.Auth::user()->id.'" AND DATE(notification.created_at) > (NOW() - INTERVAL 7 DAY)';
		$notificationList = DB::table('notification')->select('notification.*','users.name as sender_name')
		->leftJoin('users','users.id', '=', 'notification.sender')
        ->whereRaw($where)       
        ->orderBy('notification.created_at', 'desc')
        ->get();

        $unread = DB::table('notification')->selectRaw('count(*) as num')
        ->where('notification.status', '=', 'NEW')
        ->whereRaw($where)
        ->first();

        $notificationList->unread= $unread->num;
		//dd($notificationList);
		
        return view('admin.all-notification')->with('notificationList',$notificationList);
    }
	
	public function readNotification(Request $request)
    {
		$return_val=0;
		if(DB::table('notification')->where('notification.id', '=',$request->id)->update(['status'=>'READ']))
		{
			$return_val=1;
		}
		echo $return_val;

    }

   

    

   
}
