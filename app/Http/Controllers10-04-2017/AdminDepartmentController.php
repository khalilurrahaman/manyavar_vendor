<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Models\Department;
use DB;
use Redirect;

class AdminDepartmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         if($this->userAccessibilityCheck('department-list'))
	{ 
        $list=Department::orderBy('id')->get();
        return view('admin.departmen-list')->with('list',$list);
    }
      else
	return Redirect::route('dashboard')->withErrors(['You have no access!']);
		    
       
    }
    public function getEditData(Request $request)
    {
        $id=$request->id;
        $data=Department::where('id','=', $id)->first();
        return $data;
    }
    
    public function addDepartment(Request $request)
    {
        $user=\Auth::user();
        $this->validate($request,[
        'name'=>'required|unique:departments',        
        'description'=>'required'
        ]);

        $name=trim($request->name);        
        $description=$request->description;        
        /*$createdby=$user->id;
        $modifiedby=$user->id;*/
        $createdby=3;
        $modifiedby=3;


        $department=new Department;
        $department->name=$name; 
        $department->description=$description;      
        $department->createdby=$createdby;
        $department->modifiedby=$modifiedby;
        if($department->save())
        {
            return redirect::route('departmnt-lising')->withErrors(["New Department Added Successfully"]);
                  
        }
        else
        {
            return redirect::route('departmnt-lising')->withErrors(["Error !!"]);
        }

    }

    public function updateDepartment(Request $request)
    {

        $user=\Auth::user();
        $this->validate($request,[
        'name'=>'required|unique:departments,name,'.$request->id,        
        'description'=>'required'
        ]);
        $id=$request->id;
        $name=trim($request->name);        
        $description=$request->description;        
        //$modifiedby=$user->id;
        $modifiedby=3;

        $data=Department::where('id','=',$id)->first();
        $data->name=$name;
        $data->description=$description;
        $data->modifiedby=$modifiedby;
        if($data->save())
        {
            return redirect::route('departmnt-lising')->withErrors(["Department Updated Successfully"]);
        }
        else
        {
            return redirect::route('departmnt-lising')->withErrors(["Error !!"]);
        }

        /*$data=\DB::table('departments')->where('id','=',$id)->update(
            array(
                'name' => $name,                
                'description' => $description,                
                'modifiedby' => $modifiedby          
               
                ));*/
        
        
    }
    public function deleteDepartment(Request $request)
    {
        $id=$request->id;
        $data=Department::where('id','=',$id);
        if($data->delete())
        {
            return redirect()->back()->withErrors(["Department Deleted Successfully"]);
        }
    }
    public function addDepartmentPage()
    {
        
        return view('admin.add-department');
        
    }
    public function editDepartmentPage($id)
    {
        $department=Department::where('id','=',$id)->first();
        return view('admin.edit-department')->with('department',$department);
        
    }
}
