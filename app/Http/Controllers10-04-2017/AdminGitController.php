<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Redirect;
use App\Models\GIT;
use App\Models\purchesOrder;
class AdminGitController extends Controller
{
    public function viewGit()
    {   
	
		if($this->userAccessibilityCheck('all-git-list'))
			return view('admin.vendor-all-git-list');
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);
		    
    	
    }

    public function markCompleteGit($id)
    {
		
        $close_git=GIT::where('id','=',$id)->first();
		
        if($close_git)
        {
            $poStatus = DB::table('V_PURORDDET')->where('V_PURORDDET.ORDCODE', '=',$close_git->ORDCODE)->where('V_PURORDDET.ICODE', '=',$close_git->ICODE)->update(['STATUS'=>'CLOSED','updated_at'=>date('Y-m-d H:i:s')]);

            $isexist=DB::table('V_PURORDDET')->selectRaw('count(*) as num')->whereRaw('V_PURORDDET.ORDCODE = "'.$close_git->ORDCODE.'" AND V_PURORDDET.STATUS ="OPEN"')->groupBy('V_PURORDDET.ORDCODE')->first();

            if(!empty($isexist))
            {
                $poStatus = DB::table('purchase_order')->where('purchase_order.ORDCODE', '=',$close_git->ORDCODE)->update(['status'=>'CLOSED','updated_at'=>date('Y-m-d H:i:s')]);
            }

            if(DB::table('git')->where('git.ORDCODE', '=',$close_git->ORDCODE)->where('git.ICODE', '=',$close_git->ICODE)->update(['status'=>'Close','updated_at'=>date('Y-m-d H:i:s')]))
			{
				
					$gitDtls = DB::table('git')->selectRaw('users.id,purchase_order.PONO')
					->leftJoin('V_PURORDDET', 'V_PURORDDET.id', '=', 'git.pd_id')
					->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
					->join('users', 'purchase_order.PCODE', '=', 'users.SLCODE')
					->where('git.id', '!=',$id)
					->first();				
				
					$notify_array['message'] = 'Git For PO:'.$gitDtls->PONO.' has been Mark Completed';
					$notify_array['type'] = 'RFQRequestConclude';
					$notify_array['red_url'] = 'all-git-list';
					$notify_array['sender'] = Auth::user()->id;
					$notify_array['receiver'] = $gitDtls->id;;
					$notify_array['status'] = 'NEW';
					$notify_array['table_name'] = 'git';
					$notify_array['primary_key'] = 'id';
					$notify_array['primary_key_val'] =$id;
					$notify_array['created_at'] =date('Y-m-d H:i:s');	
					DB::table('notification')->insert($notify_array);
			
			
			}



            return Redirect::route('all_git_list');
        }
        else
        {
            return Redirect::route('all_git_list');
        }
    	return Redirect::route('all_git_list');
    }
	
	
	
	public function markCompleteGitPost(Request $request)
    {
		
			$mark_complete_remarks=$request->mark_complete_remarks;
       
            if(DB::table('git')->where('git_no','=',$request->mark_complete_git_id)->update(['mark_complete_remarks'=>$mark_complete_remarks,'status'=>'Close','updated_at'=>date('Y-m-d H:i:s')]))
			{
				
					$gitDtls = DB::table('git')->selectRaw('users.id,purchase_order.PONO')
					->leftJoin('V_PURORDDET', 'V_PURORDDET.id', '=', 'git.pd_id')
					->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
					->join('users', 'purchase_order.PCODE', '=', 'users.SLCODE')
					->where('git.git_no', '!=',$request->mark_complete_git_id)
					->first();				
				
					$notify_array['message'] = $request->mark_complete_git_id.' has been Mark Completed';
					$notify_array['type'] = 'RFQRequestConclude';
					$notify_array['red_url'] = 'all-git-list';
					$notify_array['sender'] = Auth::user()->id;
					$notify_array['receiver'] = $gitDtls->id;;
					$notify_array['status'] = 'NEW';
					$notify_array['table_name'] = 'git';
					$notify_array['primary_key'] = 'git_no';
					$notify_array['primary_key_val'] =$request->mark_complete_git_id;
					$notify_array['created_at'] =date('Y-m-d H:i:s');	
					DB::table('notification')->insert($notify_array);
					
				 	$request->session()->flash('alert-success', 'Mark completed successfully!');
			
			}
			else
			{
				$request->session()->flash('alert-danger', 'Try Again!');
			}
    	return Redirect::route('all_git_list');
    }
	
	
	
	public function resolveGitRecords(Request $request)
    {
		$return=0;
        $git['admin_sb_resolve_remarks'] = $request->resolve_remarks;
		$git['request_type'] = $request->request_type;		
        $git['admin_is_resolve'] = 1;  
		$git['updated_at'] =date('Y-m-d H:i:s');	   
        if(DB::table('git')->where('git_no', $request->git_no)->update($git))
        {
			$row_result = DB::table('git')->select('users.id as uid')
			->join('users', 'users.SLCODE', '=', 'git.SLCODE')
			->where('git.git_no', $request->git_no)->first();
			
			
			$type=$request->request_type;
			$notify_array['type'] = $type;
			$notify_array['message'] = 'Request for CN/SB has been sent for '. $request->git_no;
			
					
			$notify_array['red_url'] = 'all-git-list';
			$notify_array['sender'] =1;
			$notify_array['receiver'] = $row_result->uid;
			$notify_array['status'] = 'NEW';
			$notify_array['table_name'] = 'git';
			$notify_array['primary_key'] = 'git_no';
			$notify_array['primary_key_val'] =$request->git_no;
			$notify_array['created_at'] =date('Y-m-d H:i:s');	
			DB::table('notification')->insert($notify_array);
					
            $return=1;
        }
        echo $return;
	
	}
	
	
    public function viewAllVendorGit()
    {
    	return view('vendor.vendor-all-git');
    }

    public function adminGitComment($id)
    {
        //dd($id);
        $Comments = DB::table('git_comments')
                  ->select('*')
                  ->where('git_id', '=', $id)->orderBy('created_at','desc')->get();
        //dd($Comments);
        return view('admin.admin-git-comments')->with('comments_list',$Comments)->with('git_id', $id);
       
    }
     public function addGitComment(Request $request)
    {
        $return=0;
        
        $action['git_id'] = $request->git_id;
        $action['user_id'] = Auth::user()->id;   
        $action['comments'] = $request->comments;
        $action['created_at'] = date('Y-m-d H:i:s');
        $action['updated_at'] = date('Y-m-d H:i:s');
                
        if(DB::table('git_comments')->insert($action))
        {
            $return=1;
        }
        echo $return;
    }

     public function getAdminVendor()
    {
    	/*$user = Auth::user();
        $status=$request->value;*/
        $cat1_list =  DB::table('git')->selectRaw('users.id,users.name')
		
    	->leftJoin('V_PURORDDET', 'git.pd_id', '=', 'V_PURORDDET.id')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')		
		->join('users', 'purchase_order.PCODE', '=', 'users.SLCODE')
		->whereRaw("git.status!='Close'")
    	->orderBy('git.created_at', 'desc')
    	->groupBy('users.id')
		->get();

        $cat1_html='<option value="">Select Vendor</option>';
        foreach ($cat1_list as $cat1_html_value)
        {
            $cat1_html=$cat1_html.'<option value="'.$cat1_html_value->id.'">'.$cat1_html_value->name.'</option>';
        }
        $data['vendor']=$cat1_html;
        return $data;
    }
     public function getAdminCat1()
    {
        /*$user = Auth::user();
        $status=$request->value;*/
        $cat1_list =  DB::table('git')->selectRaw('products.cat1')
		
    	->leftJoin('V_PURORDDET', 'git.pd_id', '=', 'V_PURORDDET.id')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')		
		->whereRaw("git.status!='Close'")
		//->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
    	->orderBy('git.created_at', 'desc')
    	->groupBy('products.cat1')
		->get();

        $cat1_html='<option value="">Select Design</option>';
        foreach ($cat1_list as $cat1_html_value)
        {
            $cat1_html=$cat1_html.'<option value="'.$cat1_html_value->cat1.'">'.$cat1_html_value->cat1.'</option>';
        }
        $data['cat1']=$cat1_html;
        return $data;
    }
	
	public function getAdminCat2()
    {
       /*$user = Auth::user();
        $status=$request->value;*/
        $cat2_list =DB::table('git')->selectRaw('products.cat2')
		
    	->leftJoin('V_PURORDDET', 'git.pd_id', '=', 'V_PURORDDET.id')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->whereRaw("git.status!='Close'")
		//->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
    	->orderBy('git.created_at', 'desc')
    	->groupBy('products.cat2')
		->get();

        $cat2_html='<option value="">Select Colour</option>';
        foreach ($cat2_list as $cat2_html_value)
        {
            $cat2_html=$cat2_html.'<option value="'.$cat2_html_value->cat2.'">'.$cat2_html_value->cat2.'</option>';
        }
        $data['cat2']=$cat2_html;
        return $data;
    }

    public function filterAdminGit(Request $request)
    {

        $user = Auth::user();
        $category1=$request->category1;
        $category2=$request->category2;
        $vendor=$request->vendor;
        /*$category3=$request->category3;
        $category4=$request->category4;*/
        $due_from=$request->due_from;
       
        $due_to=$request->due_to;
        
        //$due_from=$request->due_from;
        
        $query_str=" (git.status != 'Close' )";
        
        if(!empty($category1))
            $query_str .=" AND products.cat1 = '".$category1."' ";
        if(!empty($category2))
            $query_str .=" AND products.cat2 = '".$category2."' ";
        if(!empty($vendor))
            $query_str .=" AND users.id = '".$vendor."' ";
        /*if(!empty($category4))
            $query_str .=" AND products.cat4 LIKE '".$category4."%' ";*/

        if(!empty($due_from))
        {

        	$due_from1=explode('/',$due_from);
        	$due_from1=$due_from1[2].'-'.$due_from1[1].'-'.$due_from1[0];
        	
            $query_str .=" AND DATE(git.git_date) >= '".$due_from1."' ";
        }
        if(!empty($due_to))
        {        	
        	$due_to1=explode('/',$due_to);
        	$due_to1=$due_to1[2].'-'.$due_to1[1].'-'.$due_to1[0];    
        	
            $query_str .=" AND DATE(git.git_date) <= '".$due_to1."' ";
        }

        	
        /*if(!empty($due_from))
            $query_str .=" AND products.git_date = '".$due_from."' ";*/
        
     
        // dd(Auth::user()->SLCODE);   
                
        
		$gitList = DB::table('git')->selectRaw('git.*,(select count(*) from git g where g.git_no=git.git_no) as rowspan,users.name,products.cat1,products.cat2,git.ORDCODE,git.ICODE,products.cat3,V_PURORDDET.ORDQTY,V_PURORDDET.RCQTY,V_PURORDDET.CNLQTY,V_PURORDDET.OQTY')
    	->leftJoin('V_PURORDDET', 'V_PURORDDET.id', '=', 'git.pd_id')
		->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
    	->join('users', 'purchase_order.PCODE', '=', 'users.SLCODE')
    	->whereRaw($query_str)		
		//->whereRaw('git.ICODE = V_PURORDDET.ICODE')
    	#->where('git.status', '!=','Close')
		->orderBy('git.git_no', 'desc')		
    	->orderBy('git.updated_at', 'desc')
		->paginate(30);
        #dd($gitList);

        if(!empty($gitList) && count($gitList)>0)
        {
            foreach($gitList as $reqst)
            {
				//-------------------GIT Comments-------------		
                $git_comments =DB::table('git_comments')->where('git_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
                if(!empty($git_comments) && count($git_comments)>0)
                $reqst->git_comments=$git_comments;
				
				//-------------------Prev GIT Qty-------------		
				// $prev_gitqty = DB::table('git')->select(DB::raw('sum(git.git_qty) as pgit_qty'))
				// ->where('git.ORDCODE', '=',$reqst->ORDCODE)
				// ->where('git.ICODE', '=',$reqst->ICODE)
				// //->where('git.SLCODE','=',Auth::user()->SLCODE)
				// ->where('git.created_at','<=',$reqst->created_at)
				// ->get();				
    			// $reqst->prev_git_qty=$prev_gitqty;
				
				
				//-------------------Mark Complete-------------
				//$total_git = DB::table('git')->select(DB::raw('count(git.id) as t_count'))
				//->where('git.pd_id', '=',$reqst->pd_id)
				//->first();
				//#echo $total_git->t_count;
				//$ptotal_git = DB::table('git')->select(DB::raw('count(git.id) as p_count'))
				//->where('git.pd_id', '=',$reqst->pd_id)
				//->where('git.vendor_is_resolve', '=',1)
				//->first();
				//#echo $ptotal_git->p_count;
				//$mc=0;
				//if(!empty($total_git->t_count) && !empty($ptotal_git->p_count) && (count($ptotal_git->p_count)>0) && ($total_git->t_count==$ptotal_git->p_count))
				//$mc=1;
				//$reqst->mark_complete=$mc;
				
				
				//------------------SB Note-------------
				$sb = DB::table('git')->select(DB::raw('count(git.id) as sb_count'))
				->where('git.git_no', '=',$reqst->git_no)
				->where('git.git_rcv', '>',0)
				->where(DB::raw('git.git_rcv - git.git_qty'),'>',0)
				->first();
				$sbc=0;
				if(!empty($sb->sb_count) && (count($sb->sb_count)>0))
				$sbc=1;
				$reqst->is_sbc=$sbc;
				
                }
            }
        //dd($gitList);
        return view('admin.vendor-all-git-filter')->with('gitList',$gitList);
    }

    public function viewArchiveGit()
    {   
		if($this->userAccessibilityCheck('all-archive-git-list'))
			return view('admin.archive-git-list');
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);    
        
    }

    public function filterArchiveGit(Request $request)
    {

        $user = Auth::user();
        $category1=$request->category1;
        $category2=$request->category2;
        $vendor=$request->vendor;
        /*$category3=$request->category3;
        $category4=$request->category4;*/
        $due_from=$request->due_from;
       
        $due_to=$request->due_to;
        
        //$due_from=$request->due_from;
        
        $query_str=" (git.status = 'Close' )";
        
        if(!empty($category1))
            $query_str .=" AND products.cat1 = '".$category1."' ";
        if(!empty($category2))
            $query_str .=" AND products.cat2 = '".$category2."' ";
        if(!empty($vendor))
            $query_str .=" AND users.id = '".$vendor."' ";
        /*if(!empty($category4))
            $query_str .=" AND products.cat4 LIKE '".$category4."%' ";*/

        if(!empty($due_from))
        {

            $due_from1=explode('/',$due_from);
            $due_from1=$due_from1[2].'-'.$due_from1[1].'-'.$due_from1[0];
            
            $query_str .=" AND DATE(git.git_date) >= '".$due_from1."' ";
        }
        if(!empty($due_to))
        {           
            $due_to1=explode('/',$due_to);
            $due_to1=$due_to1[2].'-'.$due_to1[1].'-'.$due_to1[0];    
            
            $query_str .=" AND DATE(git.git_date) <= '".$due_to1."' ";
        }

            
        /*if(!empty($due_from))
            $query_str .=" AND products.git_date = '".$due_from."' ";*/
        
     
        // dd(Auth::user()->SLCODE);   
                
        
        $gitList = DB::table('git')->selectRaw('git.*,(select count(*) from git g where g.git_no=git.git_no) as rowspan,users.name,products.cat1,products.cat2,git.ORDCODE,git.ICODE,products.cat3,V_PURORDDET.ORDQTY,V_PURORDDET.RCQTY,V_PURORDDET.CNLQTY,V_PURORDDET.OQTY')
        ->leftJoin('V_PURORDDET', 'V_PURORDDET.id', '=', 'git.pd_id')
        ->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->join('users', 'purchase_order.PCODE', '=', 'users.SLCODE')
        ->whereRaw($query_str)      
        //->whereRaw('git.ICODE = V_PURORDDET.ICODE')
        #->where('git.status', '!=','Close')
        ->orderBy('git.git_no', 'desc')     
        ->orderBy('git.updated_at', 'desc')
        ->paginate(30);
        #dd($gitList);

        if(!empty($gitList) && count($gitList)>0)
        {
            foreach($gitList as $reqst)
            {
                //-------------------GIT Comments-------------      
                $git_comments =DB::table('git_comments')->where('git_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
                if(!empty($git_comments) && count($git_comments)>0)
                $reqst->git_comments=$git_comments;
                
                //-------------------Prev GIT Qty-------------      
                // $prev_gitqty = DB::table('git')->select(DB::raw('sum(git.git_qty) as pgit_qty'))
                // ->where('git.ORDCODE', '=',$reqst->ORDCODE)
                // ->where('git.ICODE', '=',$reqst->ICODE)
                // //->where('git.SLCODE','=',Auth::user()->SLCODE)
                // ->where('git.created_at','<=',$reqst->created_at)
                // ->get();             
                // $reqst->prev_git_qty=$prev_gitqty;
                
                
                //-------------------Mark Complete-------------
                //$total_git = DB::table('git')->select(DB::raw('count(git.id) as t_count'))
                //->where('git.pd_id', '=',$reqst->pd_id)
                //->first();
                //#echo $total_git->t_count;
                //$ptotal_git = DB::table('git')->select(DB::raw('count(git.id) as p_count'))
                //->where('git.pd_id', '=',$reqst->pd_id)
                //->where('git.vendor_is_resolve', '=',1)
                //->first();
                //#echo $ptotal_git->p_count;
                //$mc=0;
                //if(!empty($total_git->t_count) && !empty($ptotal_git->p_count) && (count($ptotal_git->p_count)>0) && ($total_git->t_count==$ptotal_git->p_count))
                //$mc=1;
                //$reqst->mark_complete=$mc;
                
                
                //------------------SB Note-------------
                $sb = DB::table('git')->select(DB::raw('count(git.id) as sb_count'))
                ->where('git.git_no', '=',$reqst->git_no)
                ->where('git.git_rcv', '>',0)
                ->where(DB::raw('git.git_rcv - git.git_qty'),'>',0)
                ->first();
                $sbc=0;
                if(!empty($sb->sb_count) && (count($sb->sb_count)>0))
                $sbc=1;
                $reqst->is_sbc=$sbc;
                
                }
            }
        //dd($gitList);
        return view('admin.archive-git-filter')->with('gitList',$gitList);
    }

    public function getArchiveVendor()
    {
        /*$user = Auth::user();
        $status=$request->value;*/
        $cat1_list =  DB::table('git')->selectRaw('users.id,users.name')
        
        ->leftJoin('V_PURORDDET', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')      
        ->join('users', 'purchase_order.PCODE', '=', 'users.SLCODE')
        ->whereRaw("git.status='Close'")
        ->orderBy('git.created_at', 'desc')
        ->groupBy('users.id')
        ->get();

        $cat1_html='<option value="">Select Vendor</option>';
        foreach ($cat1_list as $cat1_html_value)
        {
            $cat1_html=$cat1_html.'<option value="'.$cat1_html_value->id.'">'.$cat1_html_value->name.'</option>';
        }
        $data['vendor']=$cat1_html;
        return $data;
    }
     public function getArchiveCat1()
    {
        /*$user = Auth::user();
        $status=$request->value;*/
        $cat1_list =  DB::table('git')->selectRaw('products.cat1')
        
        ->leftJoin('V_PURORDDET', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')      
        ->whereRaw("git.status='Close'")
        //->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
        ->orderBy('git.created_at', 'desc')
        ->groupBy('products.cat1')
        ->get();

        $cat1_html='<option value="">Select Design</option>';
        foreach ($cat1_list as $cat1_html_value)
        {
            $cat1_html=$cat1_html.'<option value="'.$cat1_html_value->cat1.'">'.$cat1_html_value->cat1.'</option>';
        }
        $data['cat1']=$cat1_html;
        return $data;
    }
    
    public function getArchiveCat2()
    {
       /*$user = Auth::user();
        $status=$request->value;*/
        $cat2_list =DB::table('git')->selectRaw('products.cat2')
        
        ->leftJoin('V_PURORDDET', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('purchase_order','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->whereRaw("git.status='Close'")
        //->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
        ->orderBy('git.created_at', 'desc')
        ->groupBy('products.cat2')
        ->get();

        $cat2_html='<option value="">Select Colour</option>';
        foreach ($cat2_list as $cat2_html_value)
        {
            $cat2_html=$cat2_html.'<option value="'.$cat2_html_value->cat2.'">'.$cat2_html_value->cat2.'</option>';
        }
        $data['cat2']=$cat2_html;
        return $data;
    }
}
