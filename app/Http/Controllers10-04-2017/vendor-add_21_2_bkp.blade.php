@extends('layouts.admin.app')

@section('extra_css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
  <style type="text/css">
    .btn-custom-css { padding: 2px 10px; border-radius: 2px;}
    .entry-area {width: 79%;}
    .modal-dialog {
      width: 350px;
     }
      .err_msg{ text-align: center; display: block;padding:4px 0 16px 0px;color:#fd3e00; font-size: 12px; }

     #map {
      height: 250px;
      width: 100%;
    }
  </style>

@endsection

@section('content')
 
  <div id="page-wrapper">
    <div class="container-fluid"> 
      <br/>
      <div class="row">
        <div class="col-sm-12">
        @if($errors->any())
          <div id="alert-dismissable" class="alert alert-danger alert-dismissable">
            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
            <strong> Opps!</strong>Form Submition Failed!! Please try again!
          </div>
        @endif
          <div class="panel panel-default">
            <div class="panel-heading">
              <div class="row">
                <div class="col-sm-9"><h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Vendor Add</h3></div>
                <div class="col-sm-3">
                    
                </div>
              </div>
            </div>
            <div class="panel-body edit-tab">
                      <div class="process">
                       <div class="process-row nav nav-tabs">
                        <div class="process-step">
                         <button type="button" class="btn btn-warning btn-circle" data-toggle="tab" href="#menu1"><i class="fa fa-info fa-3x"></i></button>
                         <p>General Info</p>
                        </div>
                        <div class="process-step">
                         <button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu2"><i class="fa fa-phone fa-3x"></i></button>
                         <p>Contact Info</p>
                        </div>
                        <div class="process-step">
                         <button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu3"><i class="fa fa-map-marker fa-3x"></i></button>
                         <p>Location</p>
                        </div>
                     
                       
                        <div class="process-step">
                         <button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu6"><i class="fa fa-calculator fa-3x"></i></button>
                         <p>Fees</p>
                        </div>
                       </div>
                      </div>
                       <form  onsubmit="return feesForm();" autocomplete="off" enctype="multipart/form-data" method="POST" action="{{route('add.vendor')}}"  >
                       {{ csrf_field() }}

                       <input type="hidden" name="ekplate_id"  value="{{App\Model\VendorDetail::getEkplateData(\Session::get('vendor_sync_data'),'id')}}"  />

                      <div class="tab-content">
                      <div id="menu1" class="tab-pane fade active in">
                        
                        <div class="col-sm-12 form-group margin-center">
                          <div class="col-sm-4"><label>Vendor-Customer Contact no<em>*</em> :</label></div>
                          <div class="col-sm-4"><input @if(isset($_GET['flag']) && $_GET['flag']==1) value="{{$_GET['customer_phone']}}"   @endif onkeypress="javascript:return validateNumber(event)" maxlength="10" type="text" id="customer_mobile_autocomplete" class="form-control" placeholder="Type mobile No" name="vendor-customer_contact_no">
                           @if ($errors->has('vendor-customer_contact_no')) 
                           <p class="help-block" style="color:red">{{ $errors->first('vendor-customer_contact_no') }}</p> 
                           @endif
                           <div class="clearfix"></div>
                           <div style="display: none;" id="customerVerified" >Customer phone number is not verified please <a href="#" onclick="customerotp($('#customer_mobile_autocomplete').val())">verify</a>  </div>

                           @if(\Session::has('not_phone_verified'))
                              <div id="contphverified" style="color:red;">Customer phone number is not verified please verify.</div>
                           @endif

                           <div id="sucmobverified" style="display: none;color:green">Mobile number is successfully verified.</div>
                          </div>

                           <div id="offcusname" class="col-sm-3"><label>@if(isset($_GET['flag']) && $_GET['flag']==1) {{$_GET['customer_name']}}   @endif </label></div>
                          <div id="customerName" class="col-sm-3"></div>
                          
                           
                          <div style="display: none;" id="customeraddMessage" class="col-sm-3">No customer found Please <a style="text-decoration: none;" href="#">Add Customer</a></div>
                          <div class="clearfix"></div>
                        </div>
                        <hr/>
                        <div class="clearfix"></div>
                        <div class="col-sm-12 form-group margin-center">
                          <div class="col-sm-3">
                          
                           <img src="{{url('img/avatar.png')}}" alt="" class="img-thumbnail">
                          
                           <div class="clearfix"></div><br/>
                          
                           <input type="file" name="image" class="form-control"/>
                          </div>
                          <div class="col-sm-9 form-group">
                            <div class="entry-area">
                              <div class="col-sm-4"><label>Entity Name :</label></div>
                              <div class="col-sm-6">
                              <input type="text" name="entity_name" value="{{ App\Model\VendorDetail::getEkplateData(\Session::get('vendor_sync_data'),'name')}}" class="form-control" >

                               @if ($errors->has('entity_name')) <p class="help-block" style="color:red">{{ $errors->first('entity_name') }}</p> @endif
                              <input type="hidden" name="customer_id" @if(isset($_GET['flag']) && $_GET['flag']==1) value="{{$_GET['customer_id']}}" @endif  id="customer_id" />
                              </div>
                               <div class="col-sm-2">
                                  @if ($errors->has('entity_name'))<a href="javascript:void(0)" class="close-btn"><i class="fa fa-times" aria-hidden="true"></i></a> @endif 
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="entry-area">
                              <div class="col-sm-4"><label>Shop Name<em>*</em> :</label></div>
                              <div class="col-sm-6"><input class="form-control"  value="{{ App\Model\VendorDetail::getEkplateData(\Session::get('vendor_sync_data'),'shop_name')}}"  type="text" name="shop_name" >

                               @if ($errors->has('shop_name')) <p class="help-block" style="color:red">{{ $errors->first('shop_name') }}</p> @endif
                              </div>
                               <div class="col-sm-2">
                                  @if ($errors->has('shop_name'))<a href="javascript:void(0)" class="close-btn"><i class="fa fa-times" aria-hidden="true"></i></a> @endif 
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="entry-area">
                              <div class="col-sm-4"><label>Authorised Name :</label></div>
                              <div class="col-sm-6"><input class="form-control" type="text" name="authorised_person"  value="{{ App\Model\VendorDetail::getEkplateData(\Session::get('vendor_sync_data'),'contact_person')}}" >

                              @if ($errors->has('authorised_person')) <p class="help-block" style="color:red">{{ $errors->first('authorised_person') }}</p> @endif
                              </div>
                               <div class="col-sm-2">
                                  @if ($errors->has('authorised_person'))<a href="javascript:void(0)" class="close-btn"><i class="fa fa-times" aria-hidden="true"></i></a> @endif 
                                </div>
                                <div class="clearfix"></div>
                            </div>
                              <div class="entry-area">
                              <div class="col-sm-4"><label>Pan Number :</label></div>
                              <div class="col-sm-6"><input class="form-control" type="text" name="pan_no" value="">
                               @if ($errors->has('pan_no')) <p class="help-block" style="color:red">{{ $errors->first('pan_no') }}</p> @endif
                              </div>
                                <div class="col-sm-2">
                                  @if ($errors->has('pan_no'))<a href="javascript:void(0)" class="close-btn"><i class="fa fa-times" aria-hidden="true"></i></a> @endif 
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="entry-area">
                              <div class="col-sm-10"><label>Note :</label>
                                <textarea  name="note" class="form-control"> {{ App\Model\VendorDetail::getEkplateData(\Session::get('vendor_sync_data'),'additional_info')}}</textarea>
                                </div>
                                <div class="clearfix"></div>
                                <!--<button type="button" class="center-btn btn btn-warning">Submit</button>-->
                            </div>
                            <div class="clearfix"></div>
                          </div>
                          <div class="clearfix"></div>
                        </div>

                       <div class="clearfix"></div>
                        <ul class="list-unstyled list-inline pull-right" style="margin:0;">
                       <!--  <li><button type="submit" class="btn btn-success next-step">submit <i class="fa fa-refresh"></i></button></li> -->
                         <li><button type="button" class="btn btn-info next-step">Next <i class="fa fa-chevron-right"></i></button></li>
                        </ul>
                       
                      </div>

                      <div id="menu2" class="tab-pane fade">
                        
                        <div class="col-sm-7 form-group margin-center">
                          <!-- <div class="full-input">
                              <div class="col-sm-4"><label>Email ID :</label></div>
                              <div class="col-sm-6"><input type="text" class="form-control" placeholder=""></div>
                                <div class="col-sm-2">
                                  <a href="javascript:void(0)" class="close-btn"><i class="fa fa-times" aria-hidden="true"></i></a>
                                  <a href="javascript:void(0)" class="right-btn"><i class="fa fa-check" aria-hidden="true"></i></a>
                                </div>
                                <div class="clearfix"></div>
                          </div>  -->
                            <div class="full-input">
                              <div class="col-sm-4"><label>Primary Mobile No<em>*</em> :</label></div>
                              <div class="col-sm-6"><input type="text" id="Primary_no" maxlength="10" name="phone[]" class="form-control" placeholder=""  value="{{ App\Model\VendorDetail::getEkplateData(\Session::get('vendor_sync_data'),'mobile')}}" >

                                @if ($errors->has('phone.0')) <p class="help-block" style="color:red">{{ $errors->first('phone.0') }}</p> @endif
                              </div>
                                 <div class="col-sm-2">
                                  @if ($errors->has('phone.0'))<a href="javascript:void(0)" class="close-btn"><i class="fa fa-times" aria-hidden="true"></i></a> @endif 
                                </div>
                                <div class="clearfix"></div>
                          </div> 
                            <div class="full-input">
                              <div class="col-sm-4"><label>Secondary Mobile No :</label></div>
                              <div class="col-sm-6"><input type="text" id="Secondary_no" maxlength="10" name="phone[]" class="form-control" placeholder="">
                                <span id="mobile_msg" ></span>
                               @if ($errors->has('phone.1')) <p class="help-block" style="color:red">{{ $errors->first('phone.1') }}</p> @endif
                               </div>
                               <div class="col-sm-2">
                                  @if ($errors->has('phone.1'))<a href="javascript:void(0)" class="close-btn"><i class="fa fa-times" aria-hidden="true"></i></a> @endif 
                                </div>
                                
                                <div class="clearfix"></div>
                          </div> 
                            <!-- <div class="full-input">
                              <div class="col-sm-4"><label>&nbsp;</label></div>
                              <div class="col-sm-6"><input type="text" class="form-control" placeholder=""></div>
                                <div class="col-sm-2"><a href="javascript:void(0)" class="close-btn"><i class="fa fa-minus" aria-hidden="true"></i></a></div>
                                <div class="clearfix"></div>
                            </div> -->
                          <div class="clearfix"></div>
                                <!--<button type="button" class="center-btn btn btn-warning">Submit</button>
                                <div class="clearfix"></div>-->
                        </div>
                       <div class="clearfix"></div>
                       <hr/>
                        <ul class="list-unstyled list-inline pull-right">
                            <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
                            <!-- <li><button type="submit" class="btn btn-success next-step">Submit <i class="fa fa-refresh"></i></button></li> -->
                            <li><button type="button" class="btn btn-info next-step" onclick="return validateMobileNumber()">Next <i class="fa fa-chevron-right"></i></button></li>
                        </ul>
                       
                      </div>

                      <div id="menu3" class="tab-pane fade">
                    
                        <div class="col-sm-8 form-group">
                          <div class="row">
                                
                                <div class="col-sm-6">
                                    <div class="full-input row">
                                    <div class="col-sm-4"><label>Country<em>*</em> :</label></div>
                                    <div class="col-sm-8">
                                  
                                    <select class="form-control"  id="geo-country" name="country_id" placeholder="">
                                      <option value="">Select Country</option>
                                       @foreach($countrylist as $val)
                                        <option @if(ucfirst($val->name)=='India') selected @endif value="{{ $val->id }}">{{ ucfirst($val->name)}}</option>
                                       @endforeach
                                    </select>

                                    @if ($errors->has('country_id')) <p class="help-block" style="color:red">{{ $errors->first('country_id') }}</p> @endif
                                   <!--  <input type="hidden" name="country_id"  id="country_id" /> -->
                                    </div>
                                    <div class="clearfix"></div>
                                    </div>
                                </div>
                              <div class="col-sm-6">
                                <div class="full-input row">
                                    <div class="col-sm-4"><label>State<em>*</em> :</label></div>
                                    <div class="col-sm-8">
                                   
                                     <select class="form-control"  id="geo-state" name="state_id" placeholder="">
                                      <option value="">Select State</option>
                                       @foreach($statelist as $val)
                                      <option value="{{ $val->id }}">{{ ucfirst($val->name)}}</option>
                                      @endforeach
                                    </select>

                                    @if ($errors->has('state_id')) <p class="help-block" style="color:red">{{ $errors->first('state_id') }}</p> @endif
                                    <!-- <input type="hidden" name="state_id"  id="state_id" /> -->
                                    </div>
                                    <div class="clearfix"></div>
                                    </div>
                                </div>
                                
                                <div class="col-sm-6">
                                    <div class="full-input row">
                                    <div class="col-sm-4"><label>City<em>*</em> :</label></div>
                                    <div class="col-sm-8">
                                   
                                   <select class="form-control"  id="geo-city" name="city_id" placeholder="" >
                                      <option value="">Select City</option>
                                      @foreach($citylist as $val)
                                      <option @if(App\Model\VendorDetail::getEkplateData(\Session::get('vendor_sync_data'),'city_id')==ucfirst($val->name)) selected @endif value="{{ $val->id }}">{{ ucfirst($val->name) }}</option>
                                      @endforeach
                                    </select>

                                    @if ($errors->has('city_id')) <p class="help-block" style="color:red">{{ $errors->first('city_id') }}</p> @endif
                                    <!--  <input type="hidden" name="city_id"  id="city_id" /> -->
                                    </div>
                                    <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                <div class="full-input row">
                                    <div class="col-sm-4"><label>Location<em>*</em> :</label></div>
                                    <div class="col-sm-8"><input disabled="disabled"  id="geo-location" type="text" class="form-control" name="location" placeholder="" value="{{ App\Model\VendorDetail::getEkplateData(\Session::get('vendor_sync_data'),'area_name')}}" onkeyup="updatePosition('loc')">
                                     @if ($errors->has('location')) <p class="help-block" style="color:red">{{ $errors->first('location') }}</p> @endif
                                    </div>
                                    <div class="clearfix"></div>
                                    </div>
                                </div>
                              <div class="col-sm-6">
                                    <div class="full-input row">
                                    <div class="col-sm-4"><label>Latitude<em>*</em> :</label></div>
                                    <div class="col-sm-8">
                                    <input id="geo-lat" disabled="disabled" type="text"  value="{{ App\Model\VendorDetail::getEkplateData(\Session::get('vendor_sync_data'),'lat')}}"  name="latitude" class="form-control" placeholder="" onkeyup="updatePosition('lat')">

                                      @if ($errors->has('latitude')) 
                                      <p class="help-block" style="color:red">
                                        {{ $errors->first('latitude') }}
                                      </p> 
                                      @endif
                                 
                                    </div>
                                   
                                    <div class="clearfix"></div>
                                    </div>
                                </div>
                              <div class="col-sm-6">
                                <div class="full-input row">
                                    <div class="col-sm-4"><label>Longitude<em>*</em> :</label></div>
                                    <div class="col-sm-8"><input id="geo-long" disabled="disabled" type="text" name="longitude"  value="{{ App\Model\VendorDetail::getEkplateData(\Session::get('vendor_sync_data'),'long')}}" class="form-control" placeholder="" onkeyup="updatePosition('lat')">

                                      @if ($errors->has('longitude')) <p class="help-block" style="color:red">{{ $errors->first('longitude') }}</p> @endif
                                  
                                    </div>
                                   
                                    <div class="clearfix"></div>
                                    </div>
                                </div>
                              
                                <div class="col-sm-12">
                                <div class="full-input row">
                                    <div class="col-sm-12">
                                    <label>Full Address<em>*</em> :</label>
                                <textarea id="geo-full-address"  class="form-control" name="fulladdess" style="height:79px;"> {{ App\Model\VendorDetail::getEkplateData(\Session::get('vendor_sync_data'),'address')}}</textarea>
                                
                                @if ($errors->has('fulladdess')) <p class="help-block" style="color:red">{{ $errors->first('fulladdess') }}</p> @endif
                                    </div>
                                    <div class="clearfix"></div>
                                    </div>
                                </div>
                                
                                
                                
                                
                            </div>
                          <div class="clearfix"></div>
                                <!--<button type="button" class="center-btn btn btn-warning">Submit</button>
                                <div class="clearfix"></div>-->
                        </div>
                        <div class="col-sm-4 form-group">
                        
                          <div id="map" class="loction-map">
                            <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14735.588164031247!2d88.48397849999999!3d22.582953800000002!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1482310147737" width="100%" height="259" frameborder="0" style="border:0" allowfullscreen></iframe> -->
                            </div>
                            
                          <div class="clearfix"></div>
                                <!--<button type="button" class="center-btn btn btn-warning">Submit</button>
                                <div class="clearfix"></div>-->
                        </div>
                        
                        
                        <div class="clearfix"></div>
                        <ul class="list-unstyled list-inline pull-right">
                         <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
                         <!-- <li><button type="submit" class="btn btn-success next-step">Submit <i class="fa fa-refresh"></i></button></li> -->
                         <li><button type="button" class="btn btn-info next-step">Next <i class="fa fa-chevron-right"></i></button></li>
                        </ul>
                     
                    </div>

               
                
                       <div id="menu6" class="tab-pane fade">
                    
                        <div class="col-sm-7 form-group margin-center">
                          <!-- <div class="full-input">
                              <div class="col-sm-5"><label>Pg<em>*</em> :</label></div>
                              <div class="col-sm-7">

                               <select class="form-control"  name="pg_id" placeholder="">
                                      <option value="">Select Pg</option>
                                       @foreach($pglist as $val)
                                        <option  value="{{ $val->id }}">{{ ucfirst($val->gateway_name)}}</option>
                                       @endforeach
                              </select>
                              @if ($errors->has('pg_id')) <p class="help-block" style="color:red">{{ $errors->first('pg_id') }}</p> @endif
                              </div>
                              
                                <div class="clearfix"></div>
                          </div> -->
                          <div class="full-input">
                              <div class="col-sm-5"><label>Default Fixed Fees :</label></div>
                              <div class="col-sm-7"><input value="0" type="text" name="fixed_fee" class="form-control" placeholder="">
                              @if ($errors->has('fixed_fee')) <p class="help-block" style="color:red">{{ $errors->first('fixed_fee') }}</p> @endif
                              </div>
                              
                                <div class="clearfix"></div>
                          </div> 
                            <div class="full-input">
                              <div class="col-sm-5"><label>Default Negotiate Fees :</label></div>
                              <div class="col-sm-6"><input value="0" type="text"  name="negotiated_value" class="form-control" placeholder="">
                                 @if ($errors->has('negotiated_value')) <p class="help-block" style="color:red">{{ $errors->first('negotiated_value') }}</p> @endif
                              </div>
                                <div class="col-sm-1"><span class="percent">%</span></div>
                                
                                <div class="clearfix"></div>
                          </div> 
                            <div class="full-input">
                              <div class="col-sm-5"><label>Upper Limit :</label></div>
                              <div class="col-sm-7"><input value="0" id="upp_limit" type="text" name="upper_limit" class="form-control" placeholder="">
                               @if ($errors->has('upper_limit')) <p class="help-block" style="color:red">{{ $errors->first('upper_limit') }}</p> @endif
                              </div>
                               
                                <div class="clearfix"></div>
                          </div> 
                            <div class="full-input">
                              <div class="col-sm-5"><label>Lower Limit :</label></div>
                              <div class="col-sm-7"><input value="0" id="low_limit" type="text" name="lower_limit" class="form-control" placeholder="">
                               @if ($errors->has('lower_limit')) <p class="help-block" style="color:red">{{ $errors->first('lower_limit') }}</p> @endif
                              </div>
                               
                                <div class="clearfix"></div>
                          </div>
                          <div class="clearfix"></div>
                                <!--<button type="button" class="center-btn btn btn-warning">Submit</button>
                                <div class="clearfix"></div>-->
                        </div>
                        <div class="clearfix"></div>
                          <hr/>
                        <ul class="list-unstyled list-inline pull-right">
                         <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
                         <li><button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button></li>
                        </ul>
                        
                      </div>
                       
                       <div class="clearfix"></div>
                      </div>
                      </form>
            </div>
          </div>
        </div>
        
      </div>
      <!-- /.row --> 
      
    </div>
    <!-- /.container-fluid --> 
    
  </div>
  <!-- /#page-wrapper --> 

   <!-- Modal -->
<div id="addphoneverifiedmodal" class="modal fade new-modal" role="dialog">
  <div class="modal-dialog">

    <!--edit customer doc Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Otp Verification</h4>
      </div>
      <div class="modal-body">
        <div class="col-sm-12 form-group margin-center add-modal">
          <div class="row">
             <form autocomplete="off" id="otpvendorform" method="POST" action="#" >
              {{ csrf_field() }}
           
                <!--  <input id="cusdocid-field" name="id" type="hidden" class="form-control id-field" value="" placeholder=""> -->
                <div class="col-sm-12">
                  <div class="row input-area">
                          <div class="col-sm-10">
                          <label>Enter Otp :</label>
                            <input type="text"  name="otp"  id="reg_otp_cus" class="form-control" maxlength="4" placeholder="">
                             
                              <div id="otp_mis" style="color:red;">OTP not matched!</div>
                              <div id="requiredotp" style="display:none;color:red;">OTP can not be blank!</div>

                             <input type="hidden" name="customerMob"  id="customerMob" />
                          </div>
                        </div>
                </div>
           
                 <div class="clearfix"></div>
                        <hr/>
                <div class="col-sm-12">
                <div class="submit-area">
                        <button type="submit" class="btn btn-success" >Submit</button>
                     
                        </div>  
                </div>
        
              </form>
        
              </div>   
                        
                        </div>
                        <div class="clearfix"></div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>

@endsection

 @section('extra_js')
 <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script> 


    

 <script type="text/javascript">

 function feesForm(){  

      var low_limit = $('#low_limit').val();
      var upp_limit = $('#upp_limit').val();

      if(upp_limit <= low_limit){
          
         alert('Lower limit can not be greater than or equal to upper limit');
         return false;
          
      }
      
  }

 function customerotp(mob) {//alert(mob);

     $("#otp_mis").hide();
     $("#reg_otp_cus").val('');

      $.ajax({

        'url':"{{url('admin/vendor/ajx/vendorotp')}}",
        'data':{'mob':mob,"_token":"{{csrf_token()}}" },
        'type':"POST",
        'beforeSubmit':$('.page-load').css('display','block'),
         success:function(result){

          if(result.status=="ok"){
            $('.page-load').css('display','none');
            $('#addphoneverifiedmodal').modal('show');
            $('#customerMob').val(result.mob);
          }
         } 
      })  
    }

   $(function() {

      $("#otpvendorform").submit(function(e) {

          var otp_val = $('#reg_otp_cus').val();

          if(otp_val==''){

            $('#requiredotp').show();
            $("#otp_mis").hide();
          }

          if(!(otp_val!='' && otp_val.length==4)){

              $('.err_msg').html('Please Enter Four Digit!');

          }else{

              var url = "{{ url('admin/vendor/ajx/savevendorcustomerotp') }}"; // the script where you handle the form input.

              $.ajax({

                   type: "POST",
                   url: url,
                   data: $("#otpvendorform").serialize(), // serializes the form's elements.
                   success: function(data) {
                       //console.log(data);
                      if(data=="success"){
                        $('#addphoneverifiedmodal').modal('hide');
                        $("#customerVerified").hide();
                        $("#sucmobverified").show();
                        setTimeout(function() {
                            $('#sucmobverified').hide();
                        }, 10000)
                      }else{
                        $("#otp_mis").show();
                        $("#customerVerified").hide();
                        $("#requiredotp").hide();
                      }
                   }
              });
          }

          e.preventDefault(); // avoid to execute the actual submit of the form.
      });
        
    });


  $(function(){ 

      $("#geo-country").change(function () { 

        var country_id=this.value;
        $("#geo-state").html('<option value="">Select State</option>');
        $("#geo-city").html('<option value="">Select City</option>');
        //alert(country_id);
        $.ajax({

            type: "POST",
            url: '{{ route('not-show.search.autocomplete.country')}}',
            data: { id:country_id , _token:'{{csrf_token()}}' }, 
            beforeSubmit : $('.page-load').css('display','block'),
            success: function(data) {//console.log(data);

              $('.page-load').css('display','none');
              $("#geo-state").html(data);

            }
        });

      });  
  })

  $(function() {

      $("#geo-state").change(function () { 

        var state_id=this.value;
        $("#geo-city").html('<option value="">Select City</option>');
        $.ajax({

           type: "POST",
           url: '{{ route('not-show.search.autocomplete.state')}}',
           data: { id:state_id , _token:'{{csrf_token()}}' }, 
           beforeSubmit: $('.page-load').css('display','block'),
           success: function(data) {
              $('.page-load').css('display','none');
              $("#geo-city").html(data);

           }
        });

      });
      $("#geo-city").change(function () { 

          var city = $("#geo-city option:selected").text(); 
          //console.log(city);
          if(city.indexOf( 'Select' )>=0){
            $("#geo-location").val('');
            $("#geo-location").prop('disabled',true);
            $("#geo-lat").val('');
            $("#geo-lat").prop('disabled',true);
            $("#geo-long").val('');
            $("#geo-long").prop('disabled',true);
          }else{
            $("#geo-location").prop('disabled',false);
            $("#geo-lat").prop('disabled',false);
            $("#geo-long").prop('disabled',false);
          }
      });
  })      


  $(function() {

      //$("#customeraddMessage").hide(); 
      $("#customerVerified").hide();     
      $( "#customer_mobile_autocomplete" ).autocomplete({

        source: "{{route('not-show.search.autocomplete.customer.phone')}}",
        minLength: 2,

          select: function(event, ui) { 
            $('#customer_mobile_autocomplete').val(ui.item.value);
            $('#customer_id').val(ui.item.id);
            $('#customerName').html('<label>'+ui.item.name+'</label>');
            //alert( verified);
            if(ui.item.verified==0){
             $("#customerVerified").show();
             $("#customerName").show();
             $("#offcusname").hide();
            }else{
              $("#customerVerified").hide();
               $("#customerName").show();
               $("#offcusname").hide();
            }


          },
          response: function(event, ui) {
                // ui.content is the array that's about to be sent to the response callback.
                if (ui.content.length === 0) {

                    $("#customerVerified").hide();
                    $("#customerName").hide();
                    $("#customeraddMessage").show();

                    var cus_number = document.getElementById('customer_mobile_autocomplete').value;

                     var url = '{{url("admin/customer/add")}}?flag=true&customer='+cus_number;

                     $("#customeraddMessage>a").attr('href',url);

                     $("#offcusname").hide();

                  } else {
                    $("#customeraddMessage").hide();
                }
          },
          create: function () {

              $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
                   // $("#customeraddMessage").hide(); 
                return $("<li></li>")
                   //.data("item.autocomplete", item)
                   .append($("<p style='padding:6px; margin-bottom:4px;'></p>").html(item.label)) 
                   .appendTo(ul);
              };
          }

      });

    });

    $(document).ready( function() {

      setTimeout(function(){
        $('#alert-dismissable').hide();
      
      },5000)

      setTimeout(function() {
          $('#contphverified').hide();
      }, 5000);

    })

    

    function validatePrimaryNumber
    (evt) {
      var theEvent = evt || window.event;
      var key = theEvent.keyCode || theEvent.which;
      key = String.fromCharCode( key );
      var regex = /[0-9]|\./;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
      }
    }

    $('#Primary_no').bind('keyup keydown blur', function(e) {        
      if (e.which === 32 && !this.value.length)
          e.preventDefault();
      $(this).val(function(i, val) {        
         return val.replace(/[^0-9\s]/gi,''); 
      });       
    });

    $('#Secondary_no').bind('keyup keydown blur', function(e) {        
      if (e.which === 32 && !this.value.length)
          e.preventDefault();
      $(this).val(function(i, val) {        
         return val.replace(/[^0-9\s]/gi,''); 
      });       
    });

    function validateSecondaeryNumber(evt) {
      var theEvent = evt || window.event;
      var key = theEvent.keyCode || theEvent.which;
      key = String.fromCharCode( key );
      var regex = /[0-9]|\./;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
      }
    }

    function validateMobileNumber() {
      var primary_number = $('#Primary_no').val();
      console.log(primary_number);
      var secondary_number = $('#Secondary_no').val();
      console.log(secondary_number);
      if(primary_number==secondary_number){
        $('#Secondary_no').val(null);
        $("#mobile_msg").text('Can not use same no').css('color', 'red').show();
        return false;
      }
    }
    
 </script>


  <script type="text/javascript">

      function setPositionByMap(marker,geocoder,infowindow,type) {

        //google.maps.event.addListener(marker, 'dragend', function (event) { 

            //console.log(this.getPosition());
            //document.getElementById("lat").value = this.getPosition().lat();
            //document.getElementById("long").value = this.getPosition().lng();
            var locality = "";
            /*$("#geo-country :selected").prop("selected", false);
            $("#geo-city :selected").prop("selected", false);
            $("#geo-state :selected").prop("selected", false);*/
            $('#geo-location').val('');

            geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
              
              if (status == google.maps.GeocoderStatus.OK) {

                if (results[0]) {

                  for (var i = 0; i < results[0].address_components.length; i++) {

                    if(type=="create") {

              if(results[0].address_components[i].types[0]=='country') {

                var geo_country = $.trim(results[0].address_components[i].long_name);
                $("#geo-country option:contains(" + geo_country + ")").prop('selected', true);
              }

              if(results[0].address_components[i].types[0]=="administrative_area_level_1") {

                $("#geo-state option:contains(" + results[0].address_components[i].long_name + ")").prop('selected', true);
              }

              if(results[0].address_components[i].types[0]=='administrative_area_level_2') {

                $("#geo-city option:contains(" + results[0].address_components[i].long_name + ")").prop('selected', true);
              }
            }

              if(results[0].address_components[i].types[0]=='route') {

                locality = results[0].address_components[i].long_name ;
              }

              if(results[0].address_components[i].types[0]=='political') {

                locality = locality +","+results[0].address_components[i].long_name;
              }

                        $('#geo-location').val(locality.replace(/[, ]+/g, " ").trim());

                  }
                 
                  $('#geo-full-address').val(results[0].formatted_address);
                  $('#geo-lat').val(marker.getPosition().lat());
                  $('#geo-long').val(marker.getPosition().lng());
                  
                  infowindow.setContent(results[0].formatted_address);
                  infowindow.open(map, marker);

                }
              }
            });

        //});
      }
 
        function initMap() {

           // var  geocoder = new google.maps.Geocoder();

            var pos = {lat: 22.57684131274138, lng: 88.47954856931153};
            var geocoder = new google.maps.Geocoder();
            var infowindow = new google.maps.InfoWindow();

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 10,
                center: pos,
                mapTypeControl: true,
                navigationControlOptions: {style:google.maps.NavigationControlStyle.SMALL}
            });
            
            var infoWindow = new google.maps.InfoWindow({map: map});

            // Try HTML5 geolocation.
            if (navigator.geolocation) {

                navigator.geolocation.getCurrentPosition(function(position) {
                  
                  //console.log(position);
                  pos = {
                      lat: position.coords.latitude,
                      lng: position.coords.longitude
                  };
                    
                  map = new google.maps.Map(document.getElementById('map'), {
                      zoom: 15,
                      center: pos,
                      mapTypeControl: true,
                      navigationControlOptions: {style:google.maps.NavigationControlStyle.SMALL}
                  });
              
                    var marker = new google.maps.Marker({

                      position: pos,
                      map: map,
                      draggable: true,

                    });

              google.maps.event.addListener(marker, 'dragend', function (event) { 

                setPositionByMap(marker,geocoder,infowindow,'create');
              });
                  //infoWindow.setPosition(pos);
                  //infoWindow.setContent('Location found.');
                  //map.setCenter(pos);
                }, function() {
                  
                    handleLocationError(true, infoWindow, map.getCenter());
                },{ enableHighAccuracy: false, maximumAge:10000, timeout:5000 });

            } else {
              // Browser doesn't support Geolocation
              handleLocationError(false, infoWindow, map.getCenter());
            }

            var marker = new google.maps.Marker({

              position: pos,
              map: map,
              draggable: true,

            });

              google.maps.event.addListener(marker, 'dragend', function (event) { 

                setPositionByMap(marker,geocoder,infowindow,'create');
              });
            
        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
          
          infoWindow.setPosition(pos);
          infoWindow.setContent(browserHasGeolocation ?
                                'Error: The Geolocation service failed.' :
                                'Error: Your browser doesn\'t support geolocation.');
        }

    function setMapOnChange(newPosition,geocoder,infowindow) {

        var myOptions = {

          center: newPosition,
          zoom: 15,
          mapTypeControl: true,
          navigationControlOptions: {style:google.maps.NavigationControlStyle.SMALL}
        }

        var map = new google.maps.Map(document.getElementById("map"), myOptions);

        var marker = new google.maps.Marker({

        position: newPosition,
        map: map,
        draggable: true,

    });

    google.maps.event.addListener(marker, 'dragend', function (event) { 

          setPositionByMap(marker,geocoder,infowindow,'update');

    });
    }

    function updatePosition(type) {

          var str = "";
          var lat = parseFloat(document.getElementById("geo-lat").value);
          var lon = parseFloat(document.getElementById("geo-long").value);
          var location= $('#geo-location').val();
          var country= $("#geo-country option:selected").text();
          var state= $("#geo-state option:selected").text();
          var city= $("#geo-city option:selected").text();

          if ((lat && lon) && type=="lat") {

              var newPosition = new google.maps.LatLng(lat,lon);
              var geocoder =  new google.maps.Geocoder();
              var infowindow = new google.maps.InfoWindow();
              var locality = "";

                geocoder.geocode({'location': newPosition}, function(results, status) {

                    if (status == google.maps.GeocoderStatus.OK ) {
                        
                        for (var i = 0; i < results[0].address_components.length; i++) {

                  if(results[0].address_components[i].types[0]=='route') {

                    locality = results[0].address_components[i].long_name ;
                  }

                  if(results[0].address_components[i].types[0]=='political') {

                    locality = locality +","+results[0].address_components[i].long_name;
                  }

                          $('#geo-location').val(locality.replace(/[, ]+/g, " ").trim());

                      } 

                      $('#geo-full-address').val(results[0].formatted_address);
                      infowindow.setContent(results[0].formatted_address);

                    } else {
                      window.alert('Geocoder failed due to: ' + status);
                    }
                });
               

             setMapOnChange(newPosition,geocoder,infowindow);

          }else if(location!='' && type=="loc") {

              if(city.indexOf( 'Select' )>=0){
                 city ='';
                 
              }else{
                str += city+","; 
              }

              if(state.indexOf( 'Select' )>=0){
                 state ='';

              }else{
                str += state+","; 

              }
              if(country.indexOf( 'Select' )>=0){
                 country ='';
              }else{
                //alert(1);
                str += country+","; 

              }

              str = location+","+str; 

              var fulladdress=str.trim().replace(/,{1,}$/, '');
              // $("#geo-state").val(state);
              //alert(fulladdress);
              var geocoder =  new google.maps.Geocoder();
              var infowindow = new google.maps.InfoWindow();

              geocoder.geocode( { 'address': fulladdress}, function(results, status) {

                  if (status == google.maps.GeocoderStatus.OK) {

                      lat = results[0].geometry.location.lat();
                      lon = results[0].geometry.location.lng();
                        var newPosition = new google.maps.LatLng(lat,lon);

                        if (status == google.maps.GeocoderStatus.OK) {

                            if (results[0]) {
                             
                              $('#geo-full-address').val(results[0].formatted_address);
                              $('#geo-lat').val(lat);
                              $('#geo-long').val(lon);
                                
                              infowindow.setContent(results[0].formatted_address);
                              //infowindow.open(map, marker);

                            }else{
                              window.alert('Geocoder failed due to: ' + status);
                            }
                        }

                      setMapOnChange(newPosition,geocoder,infowindow);

                  } 

              });  

          }

    }
    
  </script>

  <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQYS9Yyb7WJfBPzsmIoyak7MZxbtZoOcA&callback=initMap">
  </script>
@endsection 


 


