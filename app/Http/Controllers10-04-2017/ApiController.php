<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use App\Models\Api;
use App\Models\UsersDevice;
use App\User;
use auth;
use Response;

class ApiController extends Controller
{
    //Set currentAppVersion test
    protected $currentAppVersion = '1.0';
    /*
      To check app version
    */
    public function versionCheck(Request $post_data){

        //dd($post_data);
        $postRequest = json_decode($post_data->getContent());
        $requiredParams = ['data','version'];
        $isRequestJsonValide = Api::is_request_json_valide($requiredParams,$postRequest);
        $response['data'] = [];
        if($isRequestJsonValide){
            $notEmptyParams = ['version'];
            $version = $postRequest->data->version;
            if($version){
                if(is_string($version) == false){
                    $response['data']['version'] = $version;
                    $response['data']['verified'] = false;
                    $response['errNode']['errMsg']='Invalid version field value.';
                    $response['errNode']['errCode']= "1";  
                    return $response;
                }
            }
            $isNotEmptyParamsValide = Api::check_empty($notEmptyParams,$postRequest);
            if($isNotEmptyParamsValide['status']){
                
                if($version == $this->currentAppVersion){
                    $response['data']['version'] = $version;
                    $response['data']['verified'] = true;
                    $response['errNode']['errMsg']='no error';
                    $response['errNode']['errCode']= "0";
                }
                else{
                    $response['data']['verified'] = false;
                    $response['errNode']['errMsg']='Corrent version not match';
                    $response['errNode']['errCode']= "1";
                }
            }
            else{
                $response['data']['success'] = false;
                $response['errNode']['errMsg']=$isNotEmptyParamsValide['msg'];
                $response['errNode']['errCode']= "1"; 
            }          
        }
        else{
            $response['data']['success'] = false;
            $response['errNode']['errMsg']='Invalid Json Request';
            $response['errNode']['errCode']= "1";
        }
        return Response::json($response);
    }
    /*
      Login webservie 
    */
    public function login(Request $post_data){
        //dd($post_data);
        $postRequest = json_decode($post_data->getContent());
        $requiredParams = ['email','password','deviceId','deviceType'];
        //dd($requiredParams);
        $isRequestJsonValide = Api::is_request_json_valide($requiredParams,$postRequest);
        $response['data'] = [];
        if($isRequestJsonValide){
            $notEmptyParams = ['email','password','deviceId','deviceType'];
            $isNotEmptyParamsValide = Api::check_empty($notEmptyParams,$postRequest);
            if($isNotEmptyParamsValide['status']){
                
              //Main functionality
                $deviceId = $postRequest->data->deviceId;
                $deviceType = $postRequest->data->deviceType;
                //Check device type is valide or not
                if($deviceType){
                    if(Api::is_device_type_valide($deviceType) == false){
                        $response['errNode']['errMsg']='Invalid deviceType field value.';
                        $response['errNode']['errCode']= "1";  
                        return $response;
                    }
                }  
                //Check email id           
                $email = $postRequest->data->email;
                $password = $postRequest->data->password;
                if(Api::is_email_valide($email)){
                    //$credential['status'] = 1;
                    $credential = array('email' => $email, 'password' =>$password);
                    if(User::user_authentication($credential)){
                       $user_id = User::get_user_id($email);
                       //dd($user_id);
                       if($user_id != 0){
                        //If new device id than add new device id
                        if(!UsersDevice::findDeviceId($user_id)){
                            UsersDevice::addDevice($user_id,$deviceId,$deviceType);
                        }                        
                         //Update Last Login of that device
                          if (UsersDevice::updateLoginInfo($user_id,$deviceId,$deviceType) == true && User::update_user($user_id)) {
                              $response['data']['success'] = true;
                              $response['data']['msg'] = "login successfully";
                              $response['data']['userInfo'] =User::get_user_data($email);
                          }
                          else{
                               $response['data']['success'] = false;
                               $response['data']['userInfo'] =[];
                          }
                            $response['errNode']['errMsg']='no error';
                            $response['errNode']['errCode']= "0";                          
                       }
                       else{
                            $response['data']['success'] = false;
                            $response['errNode']['errMsg']='User Not found';
                            $response['errNode']['errCode']= "1";                        
                        }
                    }
                    else{
                        $response['data']['success'] = false;
                        $response['errNode']['errMsg']='User credential not match';
                        $response['errNode']['errCode']= "1"; 
                    }                  
                }
                else{
                    $response['data']['success'] = false;
                    $response['errNode']['errMsg']='Invalid Email Format';
                    $response['errNode']['errCode']= "1";
                }
            }
            else{
                $response['data']['success'] = false;
                $response['errNode']['errMsg']=$isNotEmptyParamsValide['msg'];
                $response['errNode']['errCode']= "1"; 
            }          
        }
        else{
            $response['data']['success'] = false;
            $response['errNode']['errMsg']='Invalid Json Request';
            $response['errNode']['errCode']= "1";
        }
        return Response::json($response);
    }

    /*
      Logout web service
    */ 
    public function logout(Request $post_data){
        $postRequest = json_decode($post_data->getContent());
        $requiredParams = ['data','access_token'];
        $isRequestJsonValide = Api::is_request_json_valide($requiredParams,$postRequest);
        $response['data'] = [];
        if($isRequestJsonValide){
            $notEmptyParams = ['access_token'];
            $isNotEmptyParamsValide = Api::check_empty($notEmptyParams,$postRequest);
            if($isNotEmptyParamsValide['status']){ 
                //Check User exist
                $accessToken = trim($postRequest->data->access_token);
                if(User::check_access_token($accessToken)){  
                    auth::logout();
                    $response['data']['success'] = true;
                    $response['data']['msg'] = "Successfully Logout";
                    $response['errNode']['errMsg']='no error';
                    $response['errNode']['errCode']= "0"; 
                }
                else{
                    $response['data']['success'] = false;
                    $response['errNode']['errMsg']='Invalid Access Token';
                    $response['errNode']['errCode']= "1";                        
                }
            }
            else{
                $response['data']['success'] = false;
                $response['errNode']['errMsg']=$isNotEmptyParamsValide['msg'];
                $response['errNode']['errCode']= "1"; 
            }          
        }
        else{
            $response['errNode']['errMsg']='Invalid Json Request';
            $response['errNode']['errCode']= "1";
        }
        return Response::json($response);
    }

    /*
      Change Password web service
    */ 
    public function changePassword(Request $post_data){
        $postRequest = json_decode($post_data->getContent());
        $requiredParams = ['data','access_token','password'];
        $isRequestJsonValide = Api::is_request_json_valide($requiredParams,$postRequest);
        $response['data'] = [];
        if($isRequestJsonValide){
            $notEmptyParams = ['access_token','password'];
            $isNotEmptyParamsValide = Api::check_empty($notEmptyParams,$postRequest);
            if($isNotEmptyParamsValide['status']){ 
                //Check User exist
                $accessToken = trim($postRequest->data->access_token);
                $password = trim($postRequest->data->password);
                if(User::check_access_token($accessToken)){ 
                    $user = User::update_userPass($accessToken,$password);
                    $response['data']['success'] = true;
                    $response['data']['msg'] = "Password changed sucessfully.";
                    $response['errNode']['errMsg']='no error';
                    $response['errNode']['errCode']= "0"; 
                }
                else{
                    $response['data']['success'] = false;
                    $response['errNode']['errMsg']='Invalid Access Token.';
                    $response['errNode']['errCode']= "1";                        
                }
            }
            else{
                $response['data']['success'] = false;
                $response['errNode']['errMsg']=$isNotEmptyParamsValide['msg'];
                $response['errNode']['errCode']= "1"; 
            }          
        }
        else{
            $response['data']['success'] = false;
            $response['errNode']['errMsg']='Invalid Json Request.';
            $response['errNode']['errCode']= "1";
        }
        return Response::json($response);
    }

    /*
      Po list web service
    */ 
    public function getPolist(Request $post_data){
        $postRequest = json_decode($post_data->getContent());
        $requiredParams = ['data','access_token','page_no'];
        $isRequestJsonValide = Api::is_request_json_valide($requiredParams,$postRequest);
        $response['data'] = [];
        if($isRequestJsonValide){
            $notEmptyParams = ['access_token','page_no'];
            $isNotEmptyParamsValide = Api::check_empty($notEmptyParams,$postRequest);
            if($isNotEmptyParamsValide['status']){ 
                //Check User exist
                $accessToken = trim($postRequest->data->access_token);

                if(User::check_access_token($accessToken)){
                    $userId = User::get_user_id_from_accesstoken($accessToken);
                    $currentPage = trim($postRequest->data->page_no);
                    if(count(User::new_poList($userId,$currentPage))>0){

                        $response['data']['success'] = true;
                        $response['data']['totalPage'] = User::totalPo_count($userId,$currentPage);
                        $response['data']['poList'] = User::new_poList($userId,$currentPage);
                    }
                    else{
                        $response['data']['success'] = false;
                        $response['data']['poList'] = [];
                    }
                    $response['errNode']['errMsg']='no error';
                    $response['errNode']['errCode']= "0"; 
                }
                else{
                    $response['data']['success'] = false;
                    $response['errNode']['errMsg']='Invalid Access Token.';
                    $response['errNode']['errCode']= "1";                        
                }
            }
            else{
                $response['errNode']['errMsg']=$isNotEmptyParamsValide['msg'];
                $response['errNode']['errCode']= "1"; 
            }          
        }
        else{
            $response['errNode']['errMsg']='Invalid Json Request.';
            $response['errNode']['errCode']= "1";
        }
        return Response::json($response);
    }
	
	
	/*Accept Po List web service*/ 
    public function poStatus(Request $post_data){
        $postRequest = json_decode($post_data->getContent());
        $requiredParams = ['data','access_token','poId','accepted','remarks'];
        $isRequestJsonValide = Api::is_request_json_valide($requiredParams,$postRequest);
        $response['data'] = [];
        if($isRequestJsonValide){
            $notEmptyParams = ['access_token','poId','accepted'];
            $isNotEmptyParamsValide = Api::check_empty($notEmptyParams,$postRequest);
            if($isNotEmptyParamsValide['status']){ 
                //Check User exist
                $accessToken = trim($postRequest->data->access_token);
 				$poId = trim($postRequest->data->poId);
 				$accepted = trim($postRequest->data->accepted);
 				$remarks = trim($postRequest->data->remarks);
                if(User::check_access_token($accessToken)){ 
                //dd(User::poStatus($poId,$accepted,$remarks));       
                    if(User::poStatus($poId,$accepted,$remarks))
                    {
                        $response['data']['success'] = true;
                        $response['data']['msg'] = ($accepted) ? 'Accepted Successfully' : 'Rejected Successfully';
                    }
                    else
                    {
                        $response['data']['success'] = false;
                        $response['data']['msg'] = 'Error Occured!';
                    }
                    $response['errNode']['errMsg']='no error';
                    $response['errNode']['errCode']= "0"; 
                }
                else{
                    $response['data']['success'] = false;
                    $response['errNode']['errMsg']='Invalid Access Token.';
                    $response['errNode']['errCode']= "1";                        
                }
            }
            else{
                $response['errNode']['errMsg']=$isNotEmptyParamsValide['msg'];
                $response['errNode']['errCode']= "1"; 
            }          
        }
        else{
            $response['errNode']['errMsg']='Invalid Json Request.';
            $response['errNode']['errCode']= "1";
        }
        return Response::json($response);
    }            
}
