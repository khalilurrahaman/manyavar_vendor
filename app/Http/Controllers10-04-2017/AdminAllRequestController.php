<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use DB;
use Auth;

class AdminAllRequestController extends Controller
{
//     public function viewAllRequest()
//     {
//     	 $request_list = DB::table('po_action')        
//         ->select('po_action.*')        
//         ->orderBy('priority','asc')
//         ->orderBy('created_at','desc')
//         ->paginate(20);             
       
//         //dd($request_list);
//        $po_list = DB::table('po_action')        
//         ->select('po_no')
//         ->distinct('po_no')             
//         ->get(); 

//         if(!empty($request_list) && count($request_list)>0)
//         {
//             foreach($request_list as $reqst)
//             {
//                 $req_comments =DB::table('request_comments')->where('action_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
//                         if(!empty($req_comments) && count($req_comments)>0)
//                         $reqst->req_comments=$req_comments;
                
//                 $ven_name =DB::table('users')->select('name')->where('id', '=',$reqst->user_id)->first();
//                         if(!empty($ven_name))
//                         $reqst->ven_name=$ven_name->name;

//                 }
//             }
        
// //dd($po_list);
//     	return view('admin.vendor-all-request-list')->with('request_list',$request_list)->with('po_list',$po_list);
//     }


    public function viewAllcuttingsapmleothersRequest()
    {
       if($this->userAccessibilityCheck('allcuttingsampleothers-request'))
	{
       $request_list = DB::table('po_action')        
        ->select('po_action.*')  
        ->whereIn('action_type',array('cutting', 'cancel', 'others','sample'))      
        ->orderBy('priority','asc')
        ->orderBy('notify','desc')
        ->orderBy('updated_at','desc')
        ->paginate(20);             
       
        //dd($request_list);
       $po_list = DB::table('po_action')        
        ->select('po_no')
        ->distinct('po_no')
        ->whereIn('action_type',array('cutting', 'cancel', 'others','sample'))             
        ->get(); 

        if(!empty($request_list) && count($request_list)>0)
        {
            foreach($request_list as $reqst)
            {
                $req_comments =DB::table('request_comments')->where('action_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
                        if(!empty($req_comments) && count($req_comments)>0)
                        $reqst->req_comments=$req_comments;
                
                $ven_dtls =DB::table('users')->select('name','address')->where('id', '=',$reqst->user_id)->first();
                        if(!empty($ven_dtls))
                        $reqst->ven_dtls=$ven_dtls;

                }
            }
        
//dd($po_list);
      return view('admin.cuttingsamothers-request-list')->with('request_list',$request_list)->with('po_list',$po_list);
    }
    else
      return Redirect::route('dashboard')->withErrors(['You have no access!']);
}
     public function filterAction(Request $request)
    {
    	/*--Filter By Type*/
      $user_type=Auth::user()->type;      
      $status=$request->status;
    	$type=$request->type;
    	$po_no=$request->po_no;
    	$query="action_type != ''";
    	if(!empty($type))
    		$query .=" AND action_type = '".$type."' ";
    	if(!empty($po_no))
    		$query .=" AND po_no = '".$po_no."' ";
      if(!empty($status))
        $query .=" AND status = '".$status."' ";
         if($user_type=='vendor'){
    	 		$data['request_list'] = DB::table('po_action')  
		        ->select('po_action.*')
            ->where('user_id', '=',Auth::user()->id)
            ->whereIn('action_type',array('cutting', 'cancel', 'others','sample')) 
		        ->whereRaw($query)
            ->orderBy('priority','asc')
            ->orderBy('created_at','desc')   
            ->orderBy('updated_at','desc')         
		        ->paginate(20);
        }
        else{
          $data['request_list'] = DB::table('po_action')  
            ->select('po_action.*') 
            ->whereIn('action_type',array('cutting', 'cancel', 'others','sample'))          
            ->whereRaw($query)
            ->orderBy('priority','asc')
            ->orderBy('created_at','desc')
            ->orderBy('updated_at','desc')
            ->orderBy('notify','desc')            
            ->paginate(20);
        }
         if(!empty($data['request_list']) && count($data['request_list'])>0)
        {
            foreach($data['request_list'] as $reqst)
            {
                $req_comments =DB::table('request_comments')->where('action_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
                        if(!empty($req_comments) && count($req_comments)>0)
                        $reqst->req_comments=$req_comments;

                 $ven_dtls =DB::table('users')->select('name','address')->where('id', '=',$reqst->user_id)->first();
                        if(!empty($ven_dtls))
                        $reqst->ven_dtls=$ven_dtls;    
                }
            }
        //dd($request_list);
  //       $html='';
  //       $inc=0;
  //       if(!empty($request_list) && count($request_list)){
  //       	foreach ($request_list as $req) {
  //       		 							$type='';
  //                                        if($req->action_type=='cutting')
  //                                         $type='Request For Cutting';
  //                                         elseif($req->action_type=='rate_change') 
  //                                          $type='Request For Rate Change';
  //                                         elseif($req->action_type=='width_change') 
  //                                          $type='Request For Width Change';
  //                                         elseif($req->action_type=='date_ext') 
  //                                          $type='Request For Date Extention';
  //                                         elseif($req->action_type=='cancel') 
  //                                          $type='Request For Cancellation';
  //                                         elseif($req->action_type=='others') 
  //                                          $type='Request For Others';
  //                       $inc++;

  //       								$html .=' <tr>
  //                                     <td class="text-center">'.$inc.'</td>
  //                                   	<td class="text-center">'.$req->po_no.'</td>
  //                                   	<td class="text-center">26/09/2016</td>
  //                                       <td class="text-center">'.$type.'</td>
  //                                       <td class="text-center">--</td>
  //                                       <td class="text-center">--</td>
  //                                       <td class="text-center">--</td>
  //                                       <td class="text-center"><span class="sm-btn btn-primary btn-xs">New</span></td>
  //                                       <td class="text-center">Lorem Ipsum... <a href="vendor-comments-2.php" class="btn btn-warning btn-xs">Details</a></td>
  //                                       <td class="text-center"><a href="javascript:void(0)" class="btn btn-success">Accpet</a></td>
  //                                   </tr>';

  //                               }
  //                           }
  //                           else{
  //                           	$html .=' <tr>
  //                                   	<td class="text-center" colspan="10">No Result Found</td>
  //                                   </tr>';


  //       }

  // echo $html;
    	
  //   }


  return view('admin.cuttingsamothers-request-list-filter',$data); 
    }
    

    public function viewAllwidthrateRequest()
    {
		if($this->userAccessibilityCheck('allwidthratechange-request'))
		{
			$request_list = DB::table('po_action')        
			->select('po_action.*')  
			->whereIn('action_type',array('rate_change', 'width_change'))      
			->orderBy('priority','asc')
			->orderBy('notify','desc')
			->orderBy('created_at','desc')
			->paginate(20);             
		   
			//dd($request_list);
		   $po_list = DB::table('po_action')        
			->select('po_no')
			->distinct('po_no')
			->whereIn('action_type',array('rate_change', 'width_change'))             
			->get(); 
	
			// $select_new_po = DB::table('purchase_order')->select('purchase_order.PONO')
			// ->where('purchase_order.status', '=','OPEN')
			// ->orderBy('purchase_order.ORDDT', 'desc')
			// ->limit(50)
			// ->get();
	
		   //dd($select_new_po);
			if(!empty($request_list) && count($request_list)>0)
			{
				foreach($request_list as $reqst)
				{
					$req_comments =DB::table('request_comments')->where('action_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
							if(!empty($req_comments) && count($req_comments)>0)
							$reqst->req_comments=$req_comments;
					
					$ven_dtls =DB::table('users')->select('name','address')->where('id', '=',$reqst->user_id)->first();
							if(!empty($ven_dtls))
							$reqst->ven_dtls=$ven_dtls;
	
					}
				}
			
	//dd($request_list);
		  return view('admin.widthrate-request-list')->with('request_list',$request_list)->with('po_list',$po_list);
		}
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);
		    
       
    }

     public function filterAllwidthratechange(Request $request)
            {
              /*--Filter By Type*/
              $user_type=Auth::user()->type;      
              $status=$request->status;
              $type=$request->type;
              $po_no=$request->po_no;
              $query="action_type != ''";
              if(!empty($type))
                $query .=" AND action_type = '".$type."' ";
              if(!empty($po_no))
                $query .=" AND po_no = '".$po_no."' ";
              if(!empty($status))
                $query .=" AND status = '".$status."' ";
                 if($user_type=='vendor'){
                  $data['request_list'] = DB::table('po_action')  
                    ->select('po_action.*')
                    ->where('user_id', '=',Auth::user()->id)
                    ->whereIn('action_type',array('rate_change', 'width_change')) 
                    ->whereRaw($query)
                    ->orderBy('priority','asc')
                    ->orderBy('created_at','desc') 
                    ->orderBy('notify','desc')           
                    ->paginate(20);
                }
                else{
                  $data['request_list'] = DB::table('po_action')  
                    ->select('po_action.*') 
                    ->whereIn('action_type',array('rate_change', 'width_change'))          
                    ->whereRaw($query)
                    ->orderBy('priority','asc')
                    ->orderBy('created_at','desc') 
                    ->orderBy('notify','desc')          
                    ->paginate(20);
                }
                 if(!empty($data['request_list']) && count($data['request_list'])>0)
                {
                    foreach($data['request_list'] as $reqst)
                    {
                        $req_comments =DB::table('request_comments')->where('action_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
                                if(!empty($req_comments) && count($req_comments)>0)
                                $reqst->req_comments=$req_comments;

                        $ven_dtls =DB::table('users')->select('name','address')->where('id', '=',$reqst->user_id)->first();
                               if(!empty($ven_dtls))
                                $reqst->ven_dtls=$ven_dtls;     
                        }
                    }
             
             $select_new_po = DB::table('purchase_order')->select('purchase_order.PONO')
              ->where('purchase_order.status', '=','OPEN')
              ->orderBy('purchase_order.ORDDT', 'desc')
              ->limit(50)
              ->get();  


          return view('admin.widthrate-request-list-filter',$data)->with('select_new_po',$select_new_po); 
            }
    
    

    public function viewAlldateextRequest()
    {
        if($this->userAccessibilityCheck('alldateext-request'))
	{
       $request_list = DB::table('po_action')        
        ->select('po_action.*')  
        ->where('action_type','=','date_ext')    
        ->orderBy('priority','asc')
        ->orderBy('created_at','desc')
        ->orderBy('updated_at','desc')
        ->orderBy('notify','desc')
        ->paginate(20);             
       
        //dd($request_list);
       $po_list = DB::table('po_action')        
        ->select('po_no')
        ->distinct('po_no')
        ->where('action_type','=','date_ext')            
        ->get(); 

        if(!empty($request_list) && count($request_list)>0)
        {
            foreach($request_list as $reqst)
            {
                $req_comments =DB::table('request_comments')->where('action_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
                        if(!empty($req_comments) && count($req_comments)>0)
                        $reqst->req_comments=$req_comments;
                
                $ven_dtls =DB::table('users')->select('name','address')->where('id', '=',$reqst->user_id)->first();
                        if(!empty($ven_dtls))
                        $reqst->ven_dtls=$ven_dtls; 

                $extended_no =DB::table('po_action')->select(DB::raw('count(id) as total_ext'))->where('status', '=','accept')->where('po_no','=',$reqst->po_no)->where('ICODE','=',$reqst->ICODE)->first();
                    $reqst->extended_no=$extended_no->total_ext;          

                }
            }
        
//dd($po_list);
      return view('admin.dateext-request-list')->with('request_list',$request_list)->with('po_list',$po_list);
    }
else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);
		    
       
    }
     public function filterAlldateext(Request $request)
            {
              /*--Filter By Type*/
              $user_type=Auth::user()->type;      
              $status=$request->status;
              $type=$request->type;
              $po_no=$request->po_no;
              $query="action_type = 'date_ext'";
              
              if(!empty($po_no))
                $query .=" AND po_no = '".$po_no."' ";
              if(!empty($status))
                $query .=" AND status = '".$status."' ";
                 if($user_type=='vendor'){
                  $data['request_list'] = DB::table('po_action')  
                    ->select('po_action.*')
                    ->where('user_id', '=',Auth::user()->id)
                    ->whereRaw($query)
                    ->orderBy('priority','asc')
                    ->orderBy('created_at','desc')
                    ->orderBy('updated_at','desc')
                    ->orderBy('notify','desc')            
                    ->paginate(20);
                }
                else{
                  $data['request_list'] = DB::table('po_action')  
                    ->select('po_action.*')        
                    ->whereRaw($query)
                    ->orderBy('priority','asc')
                    ->orderBy('created_at','desc')
                    ->orderBy('updated_at','desc')
                    ->orderBy('notify','desc')            
                    ->paginate(20);
                }
                 if(!empty($data['request_list']) && count($data['request_list'])>0)
                {
                    foreach($data['request_list'] as $reqst)
                    {
                        $req_comments =DB::table('request_comments')->where('action_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
                                if(!empty($req_comments) && count($req_comments)>0)
                                $reqst->req_comments=$req_comments;

                         $ven_dtls =DB::table('users')->select('name','address')->where('id', '=',$reqst->user_id)->first();
                        if(!empty($ven_dtls))
                        $reqst->ven_dtls=$ven_dtls;

                        $extended_no =DB::table('po_action')->select(DB::raw('count(id) as total_ext'))->where('status', '=','accept')->where('po_no','=',$reqst->po_no)->where('ICODE','=',$reqst->ICODE)->first();
                         $reqst->extended_no=$extended_no->total_ext; 
           
                        }
                    }
               


          return view('admin.dateext-request-list-filter',$data); 
            }

     public function reqWiseComments($id)
    {
        //dd($id);
        $poactiondetails = DB::table('po_action')
                  ->select('*')
                  ->where('id', '=', $id)->first();
         //dd($poactiondetails);
        $Comments = DB::table('request_comments')
                  ->select('*')
                  ->where('action_id', '=', $id)->orderBy('created_at')->get();

        $extended_no =DB::table('po_action')->select(DB::raw('count(id) as total_ext'))->where('status', '=','accept')->where('po_no','=',$poactiondetails->po_no)->where('ICODE','=',$poactiondetails->ICODE)->first();          
        //dd($extended_no);
        return view('admin.admin-requestwise-comments')->with('comments_list',$Comments)->with('action_id', $id)->with('poactiondetails',$poactiondetails)->with('extended_no',$extended_no);
       
    }
    public function addReqComments(Request $request)
    {
        $return=0;
        
        $action['action_id'] = $request->action_id;
        $action['user_id'] = Auth::user()->id;   
        $action['comments'] = $request->comments;
        $action['created_at'] = date('Y-m-d H:i:s');
        $action['updated_at'] = date('Y-m-d H:i:s');
                
        if(DB::table('request_comments')->insert($action))
        {
            $return=1;
        }
        echo $return;
    }

    public function acceptRequest(Request $request)
    {
        $return=0;
        $quotation['updated_at'] = date('Y-m-d H:i:s');
        $quotation['admin_remarks'] = $request->reason;
        if($request->type=='rate_change' || $request->type=='width_change'){
          //echo $request->reqst_val.'-'.$request->accept_val;exit;
            if($request->reqst_val==$request->accept_val){
               
            $quotation['status'] = 'accept';
            $quotation['priority'] = '2'; 
            $quotation['accepted_rate'] = $request->accept_val;  
           }
           elseif($request->reqst_val!=$request->accept_val){
            $quotation['status'] = 'inprocess';
            $quotation['priority'] = '0';
            $quotation['negotiated_rate'] = $request->accept_val;     
           }
         } 
        if($request->type!='rate_change' && $request->type!='width_change'){
            $quotation['status'] = 'accept';
            $quotation['priority'] = '2';  

           }
       
       if($request->type=='cutting'){
        $quotation['new_po'] = $request->new_po;
        $quotation['docket_no'] = $request->docket_no;
        $quotation['prn_no'] = $request->prn_no;
        $quotation['prn_date'] = $request->prn_date;
        $quotation['courier_no'] = $request->courier_no;  
        }
                
      if(DB::table('po_action')->where('id','=', $request->action_id)->update($quotation))
        {
			$acDtls = DB::table('po_action')->where('id','=', $request->action_id)->first();				
		
			$notify_array['message'] = ucfirst($request->type).' Request for PO:'.$acDtls->po_no.' has been Accepted';
			$notify_array['type'] = ucfirst($request->type).'RequestAccepted';
     
      if($acDtls->action_type=='date_ext')
      {         
        $notify_array['red_url'] = 'all-dateext-request';
      }
      if($acDtls->action_type=='rate_change' || $acDtls->action_type=='width_change')
      {               
        $notify_array['red_url'] = 'all-widthratechange-request';
      }
      if($acDtls->action_type=='sample' || $acDtls->action_type=='others' || $acDtls->action_type=='cutting' || $acDtls->action_type=='cancel')
      {               
        $notify_array['red_url'] = 'all-widthsampleothers-request';
      }
			$notify_array['sender'] = Auth::user()->id;
			$notify_array['receiver'] = $acDtls->user_id;
			$notify_array['status'] = 'NEW';
			$notify_array['table_name'] = 'po_action';
			$notify_array['primary_key'] = 'id';
			$notify_array['primary_key_val'] =$request->action_id;
			$notify_array['created_at'] =date('Y-m-d H:i:s');	
			DB::table('notification')->insert($notify_array);
					
            $return=1;
        }
        echo $return;
    }

    public function rejectRequest(Request $request)
    {
        $return=0;
        $quotation['admin_remarks'] = $request->reason;
        $quotation['status'] = 'reject';
        $quotation['priority'] = '4';   
        $quotation['updated_at'] = date('Y-m-d H:i:s');     
               
        if(DB::table('po_action')->where('id','=', $request->action_id)->update($quotation))
        {
			$acDtls = DB::table('po_action')->where('id','=', $request->action_id)->first();				
		
			$notify_array['message'] = ucfirst($acDtls->action_type).' Request for PO:'.$acDtls->po_no.' has been Rejected';
			$notify_array['type'] = ucfirst($request->type).'RejectedRequest';
      if($acDtls->action_type=='date_ext')
      {         
        $notify_array['red_url'] = 'all-dateext-request';
      }
      if($acDtls->action_type=='rate_change' || $acDtls->action_type=='width_change')
      {               
        $notify_array['red_url'] = 'all-widthratechange-request';
      }
      if($acDtls->action_type=='sample' || $acDtls->action_type=='others' || $acDtls->action_type=='cutting' || $acDtls->action_type=='cancel')
      {               
        $notify_array['red_url'] = 'all-widthsampleothers-request';
      }
			$notify_array['sender'] = Auth::user()->id;
			$notify_array['receiver'] = $acDtls->user_id;;
			$notify_array['status'] = 'NEW';
			$notify_array['table_name'] = 'po_action';
			$notify_array['primary_key'] = 'id';
			$notify_array['primary_key_val'] =$request->action_id;
			$notify_array['created_at'] =date('Y-m-d H:i:s');	
			DB::table('notification')->insert($notify_array);
			
            $return=1;
        }
        echo $return;
    }


    public function closeRequest(Request $request)
    {
        $return=0;
        $quotation['updated_at'] = date('Y-m-d H:i:s');
        $quotation['status'] = 'close';
        $quotation['priority'] = '3';
        if(!empty($request->new_po)){
        $quotation['new_po'] = $request->new_po;
        }

        
        if(DB::table('po_action')->where('id','=', $request->action_id)->update($quotation))
        {
          
            $return=1;
        }
       
        echo $return;
    }

     public function acceptOthersRequest($id)
    {
       
        $quotation['updated_at'] = date('Y-m-d H:i:s');
        $quotation['status'] = 'close';
        $quotation['priority'] = '3';
        
        if(DB::table('po_action')->where('id','=', $id)->update($quotation))
        {
          
            return redirect::route('ad-allcuttingsampleothers-request');
        }
    }

    public function getnewselectPoList(Request $request)
    {
    
    $select_new_po = DB::table('purchase_order')->select('purchase_order.PONO')
        
        ->join('users','purchase_order.PCODE', '=', 'users.SLCODE')
        ->where('purchase_order.status', '=','NEW')
        ->where('users.id', '=',$request->id)
        ->orderBy('purchase_order.ORDDT', 'desc')
        ->limit(50)
        ->get();
    

        $po_list_html='<option value="">Select New PO</option>';
        foreach($select_new_po as $row)
        {
            $po_list_html .='<option value="'.$row->PONO.'">'.$row->PONO.'</option>';
        }

        return $po_list_html;
    }
}
