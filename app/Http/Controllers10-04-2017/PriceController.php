<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Models\Product;
use App\Models\price;

use DB;
use Auth;
use Redirect;
use Image;


class PriceController extends Controller
{
    //----Price List For Admin----// 
    public function index()
    {
        if($this->userAccessibilityCheck('price-list'))
    	{
            //$data=price::orderBy('request_status','DESC')->paginate(25);
            $data = DB::table('prices')->select('prices.*','products.image','products.cat1','products.cat2','products.cat3','products.cat4','products.CNAME6','users.name')
            ->leftJoin('products', 'prices.ICODE', '=', 'products.ICODE')
            ->leftJoin('users', 'prices.PCODE', '=', 'users.SLCODE')
            ->orderBy('prices.request_status', 'DESC')
            ->paginate(25);
            //dd($data);
            return view('admin.price-list')->with('data',$data);
        }
       else
	       return Redirect::route('dashboard')->withErrors(['You have no access!']);
		    
       
    }
    
	public function editPrice($id)
	    {
	        $data=price::where('id','=',$id)->first();
	        return view('admin.edit-price')->with('data',$data);
	    }
	public function updatePrice(Request $request)
    {
    	//dd($request);
        $id=$request->id;
        $old_rate=$request->old_rate;
        $price=$request->price;


        $priceMaster=price::where('id','=',$id)->first();
        $priceMaster->RATE=$price;
        $priceMaster->updated_at= date('Y-m-d H:i:s');
        $priceMaster->old_rate=$old_rate;
        $priceMaster->request_status= 2;
        
        if($priceMaster->save())
        {
            return Redirect::route('price-list')->withErrors(["Price Updated Successfully"]);
        }
        else
        {
            return Redirect::route('price-list');
        }
    } 
   //------For Vendor Price Listing----------// 
    public function vendorPriceList()
    { 
        $user = Auth::user();
        // $data=price::orderBy('request_status','DESC')
        // ->where('PCODE','=',$user->SLCODE)
        // ->paginate(25);
        $data = DB::table('prices')->select('prices.*','products.image','products.cat1','products.cat2','products.cat3','products.cat4','products.CNAME6')
            ->leftJoin('products', 'prices.ICODE', '=', 'products.ICODE')
            ->where('prices.PCODE','=',$user->SLCODE)
            ->orderBy('prices.request_status', 'DESC')
            ->paginate(25);

      // dd($data);
        return view('vendor.price-list')->with('data',$data);
    }
     

    public function requestforPricechangeToAdmin($id)
    {
        if(DB::table('prices')->where('id','=', $id)->update(['request_status' =>1]))
        {
        return Redirect::back();
        }
    }

}

