<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\purchesOrder;
use Auth;
use Redirect;
use DB;

class PurchaseOrderController extends Controller
{


    //========Start Po Functionality=============//
    public function index()
    {             
    	return view('admin.purchase-order-list');
    }
    
    public function getPoList()
    {
        $po_number_list = DB::table('purchase_order')->select('purchase_order.ORDNO','purchase_order.PONO')
        ->where('purchase_order.status', '=','OPEN')
        ->orderBy('purchase_order.ORDDT', 'desc')
        ->get();

        $po_list_html='<option value="">PO Number</option>';
        foreach($po_number_list as $row)
        {
            $po_list_html .='<option value="'.$row->PONO.'">'.$row->PONO.'</option>';
        }

        $return_data=array();
        $return_data['po']=$po_list_html;

        return $return_data;exit;
    }

    public function getVendorList()
    {        
        $vendore_list  = DB::table('purchase_order')->select('users.name','users.id')  
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
        ->where('purchase_order.status', '=','OPEN')
        ->orderBy('users.name', 'asc')
        ->groupBy('users.name')
        ->get();

        $vendor_list_html='<option value="">Vendor Number</option>';
        foreach($vendore_list as $row)
        {
            $vendor_list_html .='<option value="'.$row->id.'">'.$row->name.'</option>';
        }

        $return_data=array();
        $return_data['vendor']=$vendor_list_html;

        return $return_data;exit;
    }


    public function getDesignList()
    {
        
       $design_list = DB::table('V_PURORDDET')->select('products.cat1')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('V_PURORDDET.STATUS', '=','OPEN')
        ->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
        ->groupBy('products.cat1')
        ->orderBy('products.cat1', 'desc')
        ->get();

        $design_list_html='<option value="">Design</option>';
        foreach($design_list as $row)
        {
            $design_list_html .='<option value="'.$row->cat1.'">'.$row->cat1.'</option>';
        }

        $return_data=array();
        $return_data['design']=$design_list_html;

        return $return_data;exit;
    }

    public function getColorList()
    {               
        $cat2_list = DB::table('V_PURORDDET')->select('products.cat2')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('V_PURORDDET.STATUS', '=','OPEN')
        ->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
        ->groupBy('products.cat2')
        ->orderBy('products.cat2', 'desc')
        ->get();

        $color_list_html='<option value="">Color</option>';
        foreach($cat2_list as $row)
        {
            $color_list_html .='<option value="'.$row->cat2.'">'.$row->cat2.'</option>';
        }

        $return_data=array();
        $return_data['color']=$color_list_html;

        return $return_data;exit;
    }
       
    
	public function filterPurchaseOrder(Request $request)
    {    	
		$po_number=$request->po_number;
		$category1=$request->category1;
		$category2=$request->category2;
		$due_from=$request->due_from;
		$due_to=$request->due_to;
		$vendore_id=$request->vendore_name;
		
		
		$query_str=" purchase_order.status = 'OPEN' AND V_PURORDDET.STATUS = 'OPEN' AND 
        (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) > 0 ";
    	if(!empty($po_number))
    		$query_str .=" AND purchase_order.PONO = '".$po_number."' ";
    	if(!empty($category1))
    		$query_str .=" AND products.cat1 = '".$category1."' ";
		if(!empty($category2))
    		$query_str .=" AND products.cat2 = '".$category2."' ";
			
		if(!empty($due_from))
    		$query_str .=" AND DATE(purchase_order.ORDDT) >= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_from)))."' ";
    	if(!empty($due_to))
    		$query_str .=" AND DATE(purchase_order.ORDDT) <= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_to)))."' ";
		
		if(!empty($vendore_id))
    		$query_str .=" AND users.id = '".$vendore_id."' ";
			
    	$purchaseOrderList = DB::table('purchase_order')->select(['purchase_order.ORDNO', 'purchase_order.PONO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE', 'products.ICODE', 'V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY','users.name',DB::raw('sum(git.git_qty) as git_qty')])
    	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
		->whereRaw($query_str)
    	->orderBy('purchase_order.ORDDT', 'desc')
        ->groupBy('V_PURORDDET.id')
        ->paginate(30);
		
	
    	return view('admin.purchase-order-list-filter')->with('purchaseOrderList', $purchaseOrderList);
    }
     //=========End Po Functionality=============//

    //==========Start Archive Po Functionality=============//
    public function archievePOview()
    {				
        return view('admin.purchase-order-archieve-list');
    }

    public function getArchivePoList()
    {
        $query_str=" (((V_PURORDDET.STATUS = 'OPEN') AND (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY<=0)) OR (V_PURORDDET.STATUS ='CLOSED'))";

        $po_number_list = DB::table('purchase_order')->select('purchase_order.ORDNO','purchase_order.PONO')
        ->whereRaw($query_str)
        ->orderBy('purchase_order.ORDDT', 'desc')
        ->get();

        $po_list_html='<option value="">PO Number</option>';
        foreach($po_number_list as $row)
        {
            $po_list_html .='<option value="'.$row->PONO.'">'.$row->PONO.'</option>';
        }

        $return_data=array();
        $return_data['po']=$po_list_html;

        return $return_data;exit;
    }

    public function getArchiveVendorList()
    {        
        $query_str=" (((V_PURORDDET.STATUS = 'OPEN') AND (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY<=0)) OR (V_PURORDDET.STATUS ='CLOSED'))";

        $vendore_list  = DB::table('purchase_order')->select('users.name','users.id')  
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
        ->whereRaw($query_str)
        ->orderBy('users.name', 'asc')
        ->groupBy('users.name')
        ->get();

        $vendor_list_html='<option value="">Vendor Number</option>';
        foreach($vendore_list as $row)
        {
            $vendor_list_html .='<option value="'.$row->id.'">'.$row->name.'</option>';
        }

        $return_data=array();
        $return_data['vendor']=$vendor_list_html;

        return $return_data;exit;
    }


    public function getArchiveDesignList()
    {
        $query_str=" (((V_PURORDDET.STATUS = 'OPEN') AND (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY<=0)) OR (V_PURORDDET.STATUS ='CLOSED'))";
        
       $design_list = DB::table('V_PURORDDET')->select('products.cat1')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->whereRaw($query_str)
        ->groupBy('products.cat1')
        ->orderBy('products.cat1', 'desc')
        ->get();

        $design_list_html='<option value="">Design</option>';
        foreach($design_list as $row)
        {
            $design_list_html .='<option value="'.$row->cat1.'">'.$row->cat1.'</option>';
        }

        $return_data=array();
        $return_data['design']=$design_list_html;

        return $return_data;exit;
    }

    public function getArchiveColorList()
    { 
        $query_str=" (((V_PURORDDET.STATUS = 'OPEN') AND (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY<=0)) OR (V_PURORDDET.STATUS ='CLOSED'))";

        $cat2_list = DB::table('V_PURORDDET')->select('products.cat2')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->whereRaw($query_str)
        ->groupBy('products.cat2')
        ->orderBy('products.cat2', 'desc')
        ->get();

        $color_list_html='<option value="">Color</option>';
        foreach($cat2_list as $row)
        {
            $color_list_html .='<option value="'.$row->cat2.'">'.$row->cat2.'</option>';
        }

        $return_data=array();
        $return_data['color']=$color_list_html;

        return $return_data;exit;
    }
       

    public function filterArchievePurchaseOrder(Request $request)
    {
        $po_number=$request->po_number;
        $category1=$request->category1;
        $category2=$request->category2;
        $due_from=$request->due_from;
        $due_to=$request->due_to;
        $vendore_id=$request->vendore_name;
        
        
        $query_str=" (((purchase_order.status = 'OPEN') AND (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY<=0)) OR (V_PURORDDET.STATUS ='CLOSED'))";
        if(!empty($po_number))
            $query_str .=" AND purchase_order.PONO = '".$po_number."' ";
        if(!empty($category1))
            $query_str .=" AND products.cat1 = '".$category1."' ";
        if(!empty($category2))
            $query_str .=" AND products.cat2 = '".$category2."' ";
            
        if(!empty($due_from))
            $query_str .=" AND DATE(purchase_order.ORDDT) >= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_from)))."' ";
        if(!empty($due_to))
            $query_str .=" AND DATE(purchase_order.ORDDT) <= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_to)))."' ";
        
        if(!empty($vendore_id))
            $query_str .=" AND users.id = '".$vendore_id."' ";
        //dd($query_str);
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.PONO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE', 'products.ICODE', 'V_PURORDDET.id', 'V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY', 'users.name',DB::raw('sum(git.git_qty) as git_qty'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
        
        ->whereRaw($query_str)
        ->orderBy('V_PURORDDET.updated_at', 'desc')
        ->groupBy('V_PURORDDET.id')
        ->paginate(30);
        //->toSql();
        
        //dd($purchaseOrderList);
        return view('admin.purchase-order-archieve-filter')->with('purchaseOrderList', $purchaseOrderList);
    }

    //==========End Archive Po Functionality=============//

    //==========Start Rejected Po Functionality=============//

    public function rejectPOview()
    {                
        return view('admin.purchase-order-reject-list');
    }

    public function getRejectPoList()
    {
        $po_number_list = DB::table('purchase_order')->select('purchase_order.ORDNO','purchase_order.PONO')
        ->where('purchase_order.status', '=','REJECT')
        ->orderBy('purchase_order.ORDDT', 'desc')
        ->get();

        $po_list_html='<option value="">PO Number</option>';
        foreach($po_number_list as $row)
        {
            $po_list_html .='<option value="'.$row->PONO.'">'.$row->PONO.'</option>';
        }

        $return_data=array();
        $return_data['po']=$po_list_html;

        return $return_data;exit;
    }

    public function getRejectVendorList()
    {        
        $vendore_list  = DB::table('purchase_order')->select('users.name','users.id')  
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
        ->where('purchase_order.status', '=','REJECT')
        ->orderBy('users.name', 'asc')
        ->groupBy('users.name')
        ->get();

        $vendor_list_html='<option value="">Vendor Number</option>';
        foreach($vendore_list as $row)
        {
            $vendor_list_html .='<option value="'.$row->id.'">'.$row->name.'</option>';
        }

        $return_data=array();
        $return_data['vendor']=$vendor_list_html;

        return $return_data;exit;
    }


    public function getRejectDesignList()
    {
        
       $design_list = DB::table('V_PURORDDET')->select('products.cat1')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('V_PURORDDET.STATUS', '=','REJECT')
        ->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
        ->groupBy('products.cat1')
        ->orderBy('products.cat1', 'desc')
        ->get();

        $design_list_html='<option value="">Design</option>';
        foreach($design_list as $row)
        {
            $design_list_html .='<option value="'.$row->cat1.'">'.$row->cat1.'</option>';
        }

        $return_data=array();
        $return_data['design']=$design_list_html;

        return $return_data;exit;
    }

    public function getRejectColorList()
    {               
        $cat2_list = DB::table('V_PURORDDET')->select('products.cat2')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('V_PURORDDET.STATUS', '=','REJECT')
        ->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
        ->groupBy('products.cat2')
        ->orderBy('products.cat2', 'desc')
        ->get();

        $color_list_html='<option value="">Color</option>';
        foreach($cat2_list as $row)
        {
            $color_list_html .='<option value="'.$row->cat2.'">'.$row->cat2.'</option>';
        }

        $return_data=array();
        $return_data['color']=$color_list_html;

        return $return_data;exit;
    }

    public function filterRejectPurchaseOrder(Request $request)
    {
       
        $po_number=$request->po_number;
        $category1=$request->category1;
        $category2=$request->category2;
        $due_from=$request->due_from;
        $due_to=$request->due_to;
        $vendore_id=$request->vendore_name;
        
        
        $query_str=" purchase_order.status = 'REJECT' AND V_PURORDDET.STATUS ='REJECT' ";
        if(!empty($po_number))
            $query_str .=" AND purchase_order.PONO = '".$po_number."' ";
        if(!empty($category1))
            $query_str .=" AND products.cat1 = '".$category1."' ";
        if(!empty($category2))
            $query_str .=" AND products.cat2 = '".$category2."' ";
            
        if(!empty($due_from))
            $query_str .=" AND DATE(purchase_order.ORDDT) >= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_from)))."' ";
        if(!empty($due_to))
            $query_str .=" AND DATE(purchase_order.ORDDT) <= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_to)))."' ";
        
        if(!empty($vendore_id))
            $query_str .=" AND users.id = '".$vendore_id."' ";
        
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.PONO', 'purchase_order.ORDDT','purchase_order.TIME', 'purchase_order.remarks', 'products.cat1','products.cat2','products.cat3','products.cat4','products.RATE', 'products.ICODE','V_PURORDDET.ORDQTY', 'V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY', 'users.name', DB::raw('sum(git.git_qty) as git_qty'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
        ->whereRaw($query_str)
        ->orderBy('purchase_order.updated_at', 'desc')
        ->groupBy('V_PURORDDET.id')
        ->paginate(30);
        
        //dd($purchaseOrderList);
        return view('admin.purchase-order-reject-filter')->with('purchaseOrderList', $purchaseOrderList);
    }

    //==========End Rejected Po Functionality=============//

}
