<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Models\Product;
use DB;
use Auth;
use Redirect;
use Image;


class ItemMasterController extends Controller
{
    public function index()
    {
        $data=Product::orderBy('id','DESC')->paginate(30);
        return view('admin.item-master-list')->with('data',$data);
    }
    public function addItem(Request $request)
    {
        $user = Auth::user();

        

        $category1=$request->category1;
        $category2=$request->category2;
        $category3=$request->category3;
        $category4=$request->category4;

        $this->validate($request,[
            'category1'=>'required',
            'category2'=>'required',
            'image'=>'image'
            ]);
        $itemMaster=new Product;
        $itemMaster->cat1=$category1;
        $itemMaster->cat2=$category2;
        $itemMaster->cat3=$category3;
        $itemMaster->cat4=$category4;
        $itemMaster->createdby=$user->id;
        $itemMaster->modifiedby=$user->id;
        if($request->hasFile('image'))
        {
            $file_name=time().$request->file('image')->getClientOriginalName();
            /*Image::make(Input::file('image')->getRealPath())->save(base_path().'/public/upload/product/'.$file_name);*/

            $request->file('image')->move(base_path() . '/public/upload/product/', $file_name );
            $itemMaster->image=$file_name;

        }
        else
        {
            $itemMaster->image='';
        }
        if($itemMaster->save())
        {
            return Redirect('admin/item-master-list');
        }
        else
        {
            return Redirect('admin/item-master');
        }
    }
    public function editItem($id)
    {
        $data=Product::where('id','=',$id)->first();
        return view('admin.edit-item')->with('data',$data);
    }
    public function itemMaster()
    {
        return view('admin.add-item');
    }
    public function updateItem(Request $request)
    {
        $user = Auth::user();

        
        $id=$request->id;
        $category1=$request->category1;
        $category2=$request->category2;
        $category3=$request->category3;
        $category4=$request->category4;

        $this->validate($request,[
            'category1'=>'required',
            'category2'=>'required',
            'image'=>'image'
            ]);
        $itemMaster=Product::where('id','=',$id)->first();
        $itemMaster->cat1=$category1;
        $itemMaster->cat2=$category2;
        $itemMaster->cat3=$category3;
        $itemMaster->cat4=$category4;
        $itemMaster->createdby=$user->id;
        $itemMaster->modifiedby=$user->id;
        if($request->hasFile('image'))
        {
            $file_name=time().$request->file('image')->getClientOriginalName();
            /*Image::make(Input::file('image')->getRealPath())->save(base_path().'/public/upload/product/'.$file_name);*/

            $request->file('image')->move(base_path() . '/public/upload/product/', $file_name );
            $itemMaster->image=$file_name;

        }
        
        if($itemMaster->save())
        {
            return Redirect('admin/item-master-list');
        }
        else
        {
            return Redirect('admin/item-master');
        }
    }
    public function deleteItem($id)
    {
        $data=Product::where('id','=',$id);
        if($data->delete())
        {
            return redirect()->back();
        }

    }
  
}
