<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Carbon\Carbon;

class VendorPurchaseOrderController extends Controller
{
    public function index()
    {
    	return view('admin.purchase-order-list');
    }
	
	
    public function purchaseOrderlistview()//Vendor Purchase Order Listing Section
    {   
        
    	return view('vendor.vendor-purchase-order-list');
    }


    public function getPoList()
    {
		$query_str=" purchase_order.status = 'OPEN' AND V_PURORDDET.STATUS = 'OPEN' AND purchase_order.PCODE = '".Auth::user()->SLCODE."' AND  (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) > 0 ";
    	$po_number_list = DB::table('purchase_order')->select('purchase_order.ORDNO','purchase_order.PONO as PONO')
		->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
		->whereRaw($query_str)
    	->groupBy('purchase_order.ORDNO')->orderBy('purchase_order.ORDNO','desc')->get();
		
		

        $po_list_html='<option value="">PO Number</option>';
        foreach($po_number_list as $row)
        {
            $po_list_html .='<option value="'.$row->PONO.'">'.$row->PONO.'</option>';
        }

        $return_data=array();
        $return_data['po']=$po_list_html;

        return $return_data;exit;
    }

    public function getDesignList()
    {
		$query_str=" purchase_order.status = 'OPEN' AND V_PURORDDET.STATUS = 'OPEN' AND purchase_order.PCODE = '".Auth::user()->SLCODE."' AND  (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) > 0 ";
    	$design_list = DB::table('purchase_order')->select('products.cat1')
		->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
		->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->whereRaw($query_str)
    	->groupBy('products.cat1')
        ->orderBy('products.cat1', 'desc')->get();
		

        $design_list_html='<option value="">Design</option>';
        foreach($design_list as $row)
        {
            $design_list_html .='<option value="'.$row->cat1.'">'.$row->cat1.'</option>';
        }

        $return_data=array();
        $return_data['design']=$design_list_html;

        return $return_data;exit;
    }

    public function getColorList()
    {           
      
		$query_str=" purchase_order.status = 'OPEN' AND V_PURORDDET.STATUS = 'OPEN' AND purchase_order.PCODE = '".Auth::user()->SLCODE."' AND  (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) > 0 ";
    	$cat2_list = DB::table('purchase_order')->select('products.cat2')
		->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
		->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->whereRaw($query_str)
    	->groupBy('products.cat2')
        ->orderBy('products.cat2', 'desc')->get();

        $color_list_html='<option value="">Color</option>';
        foreach($cat2_list as $row)
        {
            $color_list_html .='<option value="'.$row->cat2.'">'.$row->cat2.'</option>';
        }

        $return_data=array();
        $return_data['color']=$color_list_html;

        return $return_data;exit;
    }

	 public function getCategory3List()
    {           
       $query_str=" purchase_order.status = 'OPEN' AND V_PURORDDET.STATUS = 'OPEN' AND purchase_order.PCODE = '".Auth::user()->SLCODE."' AND  (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) > 0 ";
    	$cat3_list = DB::table('purchase_order')->select('products.cat3','products.CCODE3')
		->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
		->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->whereRaw($query_str)
    	->groupBy('products.cat3')
        ->orderBy('products.cat3', 'desc')->get();
		
        $cat3_list_html='<option value="">Cat 3</option>';
        foreach($cat3_list as $row)
        {
            $cat3_list_html .='<option value="'.$row->CCODE3.'">'.$row->cat3.'</option>';
        }

        $return_data=array();
        $return_data['category3']=$cat3_list_html;

        return $return_data;exit;
    }
	
	 public function getWidthSizeList()
    {           
       $query_str=" purchase_order.status = 'OPEN' AND V_PURORDDET.STATUS = 'OPEN' AND purchase_order.PCODE = '".Auth::user()->SLCODE."' AND  (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) > 0 ";
    	$cat4_list = DB::table('purchase_order')->select('products.cat4','products.CCODE4')
		->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
		->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->whereRaw($query_str)
    	->groupBy('products.cat4')
        ->orderBy('products.cat4', 'desc')->get();

        $width_size_list_html='<option value="">Width / Size</option>';
        foreach($cat4_list as $row)
        {
            $width_size_list_html .='<option value="'.$row->CCODE4.'">'.$row->cat4.'</option>';
        }

        $return_data=array();
        $return_data['width_size']=$width_size_list_html;

        return $return_data;exit;
    }
	public function filterPurchaseOrder(Request $request)
    {
    	$user = Auth::user();
		$po_number=trim($request->po_number);
		$category1=trim($request->category1);
		$category2=trim($request->category2);
		$category4=trim($request->category4);
        $category6=trim($request->category6);
		$due_from=trim($request->due_from);
		$due_to=trim($request->due_to);

        $next15=trim($request->next15);
        $prev15=trim($request->prev15);
		
		$query_str=" purchase_order.status = 'OPEN' AND V_PURORDDET.STATUS = 'OPEN' AND purchase_order.PCODE = '".Auth::user()->SLCODE."' AND  (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) > 0 ";

    	if(!empty($po_number))
    		$query_str .=" AND purchase_order.PONO like '%".$po_number."%' ";
			
    	if(!empty($category1))
    		$query_str .=" AND products.cat1 like '%".$category1."%' ";
		if(!empty($category2))
    		$query_str .=" AND products.cat2 like '%".$category2."%' ";
		
		if(!empty($category4))
    		$query_str .=" AND products.cat4 like '%".$category4."%' ";

        if(!empty($category6))
            $query_str .=" AND products.CNAME6 like '%".$category6."%' ";
			
		if(!empty($due_from))
    		$query_str .=" AND DATE(purchase_order.ORDDT) >= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_from)))."' ";
    	if(!empty($due_to))
    		$query_str .=" AND DATE(purchase_order.ORDDT) <= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_to)))."' ";

        if(!empty($prev15)){
            $bet1="'".Carbon::now()->subDay(45)->format('Y-m-d')."' AND '".Carbon::now()->format('Y-m-d')."'";
            $query_str .=" AND purchase_order_delivery.DUEDATE BETWEEN ". $bet1;
        }

        if(!empty($next15)){
            $bet2="'".Carbon::now()->format('Y-m-d')."' AND '".Carbon::now()->addDay(15)->format('Y-m-d')."'";
            $query_str .=" AND purchase_order_delivery.DUEDATE BETWEEN ". $bet2;
        }

    	$purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.PONO as PONO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.CNAME5','products.CNAME6','V_PURORDDET.RATE', 'products.ICODE','purchase_order_delivery.DUEDATE', 'V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY',DB::raw('sum(git.git_qty) as git_qty'))
		
		->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id') 
        ->leftJoin('purchase_order_delivery', 'purchase_order_delivery.PURORDDET_CODE', '=', 'V_PURORDDET.CODE')   			
		->whereRaw($query_str)
    	->orderBy('purchase_order_delivery.DUEDATE', 'asc')
		->groupBy('V_PURORDDET.id')
		->paginate(30);
		
        if(!empty($purchaseOrderList) && count($purchaseOrderList)>0)
        {
            foreach($purchaseOrderList as $reqst)
            {
                $actiondtls =DB::table('po_action')->select('action_type','status','ICODE')->where('po_no', '=',$reqst->PONO)->whereIn('status',array('new', 'inprocess'))->orderBy('created_at','desc')->get();
                if(!empty($actiondtls) && count($actiondtls)>0)
                    $reqst->actiondtls=$actiondtls;
            }
        }
	    //dd($purchaseOrderList);
    	return view('vendor.vendor-purchase-order-list-filter')->with('purchaseOrderList', $purchaseOrderList);
    }

    public function purchaseOrderRequest(Request $request)
    {
        //dd($request);
        $return=1;
        
        $porequest['user_id']=Auth::user()->id;  
        $porequest['po_no']= $request->po_no;
        $porequest['ICODE']= $request->Icode;
        $porequest['order_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $request->order_date)));
        $porequest['cat1'] = $request->cat_1;
        $porequest['cat2'] = $request->cat_2;   
        $porequest['old_width'] = $request->old_width;
        $porequest['created_at']= date('Y-m-d H:i:s');
        $porequest['updated_at']= date('Y-m-d H:i:s');
        $porequest['status']= 'new';
		
        if($request->type=='cutting'){        
            $porequest['action_type'] = $request->type;
            //$porequest['new_po'] = $request->new_po;
            $porequest['remarks'] = $request->remarks;
        }

        if($request->type=='rate_change'){
         
            $porequest['old_value'] = $request->old_rate;        
            $porequest['action_type'] = $request->type;        
            $porequest['Reason'] = $request->reason;
            $porequest['new_rate'] = $request->new_rate;
          
        }

        if($request->type=='width_change'){ 
               
            $porequest['old_value'] = $request->old_rate;       
            $porequest['action_type'] = $request->type;
            $porequest['Reason'] = $request->reason;        
            $porequest['new_rate'] = $request->new_rate;
            $porequest['new_width'] = $request->new_width;
         
        }

        if($request->type=='date_ext'){        
            $porequest['action_type'] = $request->type;
            $porequest['Reason'] = $request->reason; 
            $porequest['po_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $request->del_date)));       
            $porequest['ext_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $request->ext_date)));        
        }

        if($request->type=='cancel'){  
            $porequest['pending_qty'] = $request->pen_qty;  
            $porequest['action_type'] = $request->type;
            $porequest['Reason'] = $request->reason;
            $porequest['cancel_qty'] = $request->can_qty; 
            $porequest['po_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $request->del_date)));         
        }

        if($request->type=='others'){        
            $porequest['action_type'] = $request->type;
            $porequest['remarks'] = $request->reason;  
                   
        }

        if($request->type=='sample'){        
            $porequest['action_type'] = $request->type;
            $porequest['remarks'] = $request->reason;        
            $porequest['docket_no'] = $request->docket_no;
            //$porequest['prn_no'] = $request->prn_no;
            //$porequest['prn_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $request->prn_date)));
            $porequest['courier_no'] = $request->courier_no;                
        }
       
        
        $where="po_no='".trim($request->po_no)."' AND user_id = '".Auth::user()->id."' AND status = 'new' AND ICODE = '".($request->Icode)."' AND action_type='".($request->type)."'";
       
        $isexist = DB::table('po_action')->whereRaw($where)->first();
        
       
        if(count($isexist)==0)
        {
           
		    $l_id=DB::table('po_action')->insertGetId($porequest);
            //dd($l_id);

            if(!empty($l_id))
            {
    			
    			$acDtls = DB::table('po_action')->where('id','=', $l_id)->first();				
    		
    			$notify_array['message'] = ucfirst($acDtls->action_type).' Request for '.$acDtls->po_no.' has been Send';
                $notify_array['type'] = ucfirst($acDtls->action_type).'RequestSend';
                if($acDtls->action_type=='date_ext')
                {    			
                    $notify_array['red_url'] = 'alldateext-request';
                }
                if($acDtls->action_type=='rate_change' || $acDtls->action_type=='width_change')
                {               
                    $notify_array['red_url'] = 'allwidthratechange-request';
                }
                if($acDtls->action_type=='sample' || $acDtls->action_type=='others' || $acDtls->action_type=='cutting' || $acDtls->action_type=='cancel')
                {               
                    $notify_array['red_url'] = 'allcuttingsampleothers-request';
                }

    			$notify_array['sender'] = Auth::user()->id;
    			$notify_array['receiver'] =1;
    			$notify_array['status'] = 'NEW';
    			$notify_array['table_name'] = 'po_action';
    			$notify_array['primary_key'] = 'id';
    			$notify_array['primary_key_val'] =$l_id;
    			$notify_array['created_at'] =date('Y-m-d H:i:s');	
    			DB::table('notification')->insert($notify_array);
    			
                $return=1;
            }
        }
        echo $return;
    }


    //Vendor Archieve Purchase Order Listing Section


    public function archievePOlistview()
    {          
        return view('vendor.vendor-purchase-order-archieve');
    }

    public function getArchvePoList()
    {
        $user = Auth::user();
        $query_str= "((purchase_order.status = 'CLOSED') OR (purchase_order.status = 'REJECT') OR (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) <= 0) AND PCODE='".$user->SLCODE."'";

       
        $po_number_list = DB::table('purchase_order')->select('purchase_order.ORDNO','purchase_order.PONO as PONO')
                        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
						->where('purchase_order.status', '!=', 'NEW')//21-02-2017 for New item Not show in the List
                        ->whereRaw($query_str)
                        ->groupBy('purchase_order.ORDNO')
                        ->orderBy('purchase_order.ORDNO','desc')
                        ->get();
        
        $po_list_html='<option value="">PO Number</option>';
        foreach($po_number_list as $row)
        {
            $po_list_html .='<option value="'.$row->PONO.'">'.$row->PONO.'</option>';
        }
        
        $return_data=array();
        $return_data['po']=$po_list_html;
        
        return $return_data;exit;
    }

    public function getArchveDesignList()
    {
        $user = Auth::user();
        $query_str= "((purchase_order.status = 'CLOSED') OR (purchase_order.status = 'REJECT') OR (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) <= 0) AND PCODE='".$user->SLCODE."'";
        $design_list=DB::table('purchase_order')->select('products.cat1')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->where('purchase_order.status', '!=', 'NEW')//21-02-2017 for New item Not show in the List
        ->whereRaw($query_str)
        ->groupBy('products.cat1')
        ->orderBy('products.cat1', 'desc')->get();

        $design_list_html='<option value="">Design</option>';
        foreach($design_list as $row)
        {
            $design_list_html .='<option value="'.$row->cat1.'">'.$row->cat1.'</option>';
        }

        $return_data=array();
        $return_data['design']=$design_list_html;

        return $return_data;exit;
    }

    public function getArchveColorList()
    {           
        $user = Auth::user();
        $query_str= "((purchase_order.status = 'CLOSED') OR (purchase_order.status = 'REJECT') OR (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) <= 0) AND PCODE='".$user->SLCODE."'";
        $cat2_list=DB::table('purchase_order')->select('products.cat2')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->where('purchase_order.status', '!=', 'NEW')//21-02-2017 for New item Not show in the List
        ->whereRaw($query_str)
        ->groupBy('products.cat2')
        ->orderBy('products.cat2', 'desc')->get();

        $color_list_html='<option value="">Color</option>';
        foreach($cat2_list as $row)
        {
            $color_list_html .='<option value="'.$row->cat2.'">'.$row->cat2.'</option>';
        }

        $return_data=array();
        $return_data['color']=$color_list_html;

        return $return_data;exit;
    }
    
     public function getArchveCategory3List()
    {           
        $user = Auth::user();
        $query_str= "((purchase_order.status = 'CLOSED') OR (purchase_order.status = 'REJECT') OR (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) <= 0) AND PCODE='".$user->SLCODE."'";
        $cat3_list=DB::table('purchase_order')->select('products.cat3','products.CCODE3')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->where('purchase_order.status', '!=', 'NEW')//21-02-2017 for New item Not show in the List
        ->whereRaw($query_str)
        ->groupBy('products.cat3')
        ->orderBy('products.cat3', 'desc')->get();

        $cat3_list_html='<option value="">Cat 3</option>';
        foreach($cat3_list as $row)
        {
            $cat3_list_html .='<option value="'.$row->CCODE3.'">'.$row->cat3.'</option>';
        }

        $return_data=array();
        $return_data['category3']=$cat3_list_html;

        return $return_data;exit;
    }



     public function getArchveWidthSizeList()
    {           
        $user = Auth::user();
        $query_str= "((purchase_order.status = 'CLOSED') OR (purchase_order.status = 'REJECT') OR (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) <= 0) AND PCODE='".$user->SLCODE."'";
        $cat4_list=DB::table('purchase_order')->select('products.cat4','products.CCODE4')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->where('purchase_order.status', '!=', 'NEW')//21-02-2017 for New item Not show in the List
        ->whereRaw($query_str)
        ->groupBy('products.cat4')
        ->orderBy('products.cat4', 'desc')->get();

        $cat4_list_html='<option value="">Width / Size</option>';
        foreach($cat4_list as $row)
        {
            $cat4_list_html .='<option value="'.$row->CCODE4.'">'.$row->cat4.'</option>';
        }

        $return_data=array();
        $return_data['width_size']=$cat4_list_html;

        return $return_data;exit;
    }


    public function filterArchievePurchaseOrder(Request $request)
    {
        $user = Auth::user();
        $po_number=trim($request->po_number);
        $category1=trim($request->category1);
        $category2=trim($request->category2);
        $category4=trim($request->category4);
        $category6=trim($request->category6);
        $due_from=trim($request->due_from);
        $due_to=trim($request->due_to);
        
        #$query_str= "((purchase_order.status = 'CLOSED') OR (purchase_order.status = 'REJECT') OR (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) <= 0) AND (purchase_order.PCODE = '".Auth::user()->SLCODE."')";
		
		#$query_str= "((purchase_order.status = 'CLOSED') OR (purchase_order.status = 'REJECT') OR (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY <= 0)) AND (purchase_order.PCODE = '".Auth::user()->SLCODE."') AND (V_PURORDDET.RCQTY>0 OR V_PURORDDET.CNLQTY>0)";
		
		$query_str= "((purchase_order.status = 'CLOSED') OR (purchase_order.status = 'REJECT') OR (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY <= 0)) AND (purchase_order.PCODE = '".Auth::user()->SLCODE."')";
		 
        if(!empty($po_number))
            $query_str .=" AND purchase_order.PONO like '%".$po_number."%' ";
        if(!empty($category1))
            $query_str .=" AND products.cat1 like '%".$category1."%' ";
        if(!empty($category2))
            $query_str .=" AND products.cat2 like '%".$category2."%' ";
        if(!empty($category4))
            $query_str .=" AND products.cat4 like '%".$category4."%' ";
         if(!empty($category6))
            $query_str .=" AND products.CNAME6 like '%".$category6."%' ";
            
        if(!empty($due_from))
            $query_str .=" AND DATE(purchase_order.ORDDT) >= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_from)))."' ";
        if(!empty($due_to))
            $query_str .=" AND DATE(purchase_order.ORDDT) <= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_to)))."' ";
            
        $user = Auth::user();
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.PONO as PONO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.CNAME5','products.CNAME6','V_PURORDDET.RATE', 'products.ICODE','purchase_order_delivery.DUEDATE', 'V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY',DB::raw('sum(git.git_qty) as git_qty'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('purchase_order_delivery', 'purchase_order_delivery.PURORDDET_CODE', '=', 'V_PURORDDET.CODE')
	    ->where('purchase_order.status', '!=', 'NEW')//21-02-2017 for New item Not show in the List
        ->whereRaw($query_str)
        ->orderBy('purchase_order.ORDDT', 'desc')
        ->groupBy('V_PURORDDET.id')
        ->paginate(30);


        //dd($purchaseOrderList);
        return view('vendor.vendor-purchase-order-archieve-filter')->with('purchaseOrderList', $purchaseOrderList);
    }
}
