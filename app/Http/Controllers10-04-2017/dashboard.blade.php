@include('layouts.vendor_header');
        <div id="page-wrapper">
            <div class="container-fluid inner-body-area">
                <!--<div class="row">
                    <div class="col-lg-12 title-top">
                    <div class="row page-header">
                    	<div class="col-sm-6">
                        	<h1>Vendor <small>Dashboard</small></h1>
                        </div>
                        <div class="col-sm-6">
                        	<a href="javascript:void(0)" class="btn btn-success">Expand</a>
                        </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-lg-9 dash-left">
                    <div class="row">
                    	<div class="col-sm-5">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title pull-left"><i class="fa fa-cubes" aria-hidden="true"></i> New PO Creating </h3>
                                    <h5 class="pull-right">Urgent Action Required</h5>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="panel-body pre-order">
                                    <ul>@if(count($POList)>0)
                                		@foreach($POList as $eachPOList)
                                    	<li>
                                            <div class="row">
                                                <div class="col-sm-9 pre-details">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="row">
                                                                <div class="col-sm-9 details-dash">
                                                                    <h4>PO No : &nbsp; <a href="javascript:void(0)" onclick="poDetails({{$eachPOList->poId}})" data-toggle="modal" data-target="#details-pop"><strong>{{$eachPOList->odrNo}}</strong>
                                                                    <span class="btn btn-primary btn-xs">Details</span></a></h4>
                                                                    <h4>Date : &nbsp; <strong>{{ date('d/m/Y',strtotime($eachPOList->l_time)) }}</strong></h4>
                                                                    <h4>PO Value : &nbsp; <strong>{{$eachPOList->total}}/-</strong></h4>
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 accept-area">
                                                <a href="javascript:void(0)"class="btn btn-success" onclick="acceptPO({{$eachPOList->poId}})" data-toggle="modal" data-target="#updatePO-modal-body">Accept</a>
                                                <a href="javascript:void(0)" po_id="{{$eachPOList->poId}}" po_no="{{$eachPOList->odrNo}}"  data-toggle="modal" data-target="#rejectpop" class="btn btn-danger reject-po">Reject</a>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </li>
                                        @endforeach
	                                    @else
	                                    <li class="text-center">No Records Found!</li>
	                                    @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-7 paddingL0">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title pull-left"><i class="fa fa-calendar" aria-hidden="true"></i> {{count($acceptedPOList)}} PO Expired in 15 Days </h3>
                                    <h5 class="pull-right">Urgent Action Required</h5>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="panel-body pre-order custome-table">
                                    <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" style="margin-bottom:0;">
                                        <thead>
                                            <tr>
                                                <th class="text-center">PO No</th>
                                                <th class="text-center">Cat 1</th>
                                                <th class="text-center">Cat 2</th>
                                                <th class="text-center">Pending Qty</th>
                                                <th class="text-center dash-last">Action</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    @if(count($acceptedPOList)>0)
                                    @foreach($acceptedPOList as $echAcceptedList)
		                                <tr>
		                                	<td class="text-center">{{$echAcceptedList->ORDNO}}</td>
		                                    <td class="text-center">{{$echAcceptedList->cat1}}</td>
		                                    <td class="text-center">{{$echAcceptedList->cat2}}</td>
		                                    <td class="text-center">{{$echAcceptedList->ORDQTY - ($echAcceptedList->RCQTY+$echAcceptedList->CNLQTY)}}</td>
		                                    <td class="text-center dash-last">
		                                    	<span data-toggle="modal" data-target="#changedate"><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Request for Date Extension" class="btn btn-success">Date Ext</a></span>
		                                            <a href="vendor-add-git.php" data-toggle="tooltip" data-placement="top" title="Request for Add GIT" class="btn btn-info">Add GIT</a>
		                                            <span data-toggle="modal" data-target="#cancellation"><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Request for Cancellation" class="btn btn-danger">Cancel</a></span>
		                                    </td>
		                                </tr>
                          			@endforeach
                          			@else
                                    <tr>
                                    <td class="text-center" colspan="5">No Records Found!</td>
                                    </tr>
	                                @endif
                                    </tbody>
                               </table>
                              </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>  
                    <div class="row">
                    	<div class="col-sm-12">
                    		<div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title" style="padding-top:6px; padding-bottom:6px;">
                                    <i class="fa fa-list-alt" aria-hidden="true"></i> REQ Summary </h3>
                                </div>
                                <div class="clearfix"></div>
                                <div class="panel-body pre-order custome-table">
                                    <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" style="margin-bottom:0;">
                                        <thead>
                                            <tr>
                                                <th class="text-center">REQ ID</th>
                                            	<th class="text-center">Image</th>
                                                <th class="text-center">Cat 1</th>
                                                <th class="text-center">Cat 2</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Cat 4</th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                    @if(!empty($rfq_list) && count($rfq_list)>0)
                                     @foreach($rfq_list as $rfq)
                                    <tr>
                                        <td class="text-center">{{$rfq->req_id}}</td>
                                    	<td class="text-center">
                                            @if($rfq->image!='')
                                            
                                                @if(file_exists('upload/product/'.$rfq->image))
                                                
                                                <img src="{{URL::to('/upload/product').'/'.$rfq->image }}" style="height:60px;width: 60px;">
                                                @else
                                                
                                                <img src="{{URL::to('/img').'/pre-order-img.jpg' }}" style="height:60px;width: 60px;">
                                                @endif
                                            @else
                                            <img src="{{URL::to('/img').'/pre-order-img.jpg' }}" style="height:60px;width: 60px;">
                                            @endif
                                        </td>
                                        <td class="text-center">{{$rfq->cat1}}</td>
                                        <td class="text-center">{{$rfq->cat2}}</td>
                                        <td class="text-center">{{$rfq->cat3}}</td>
                                        <td class="text-center">{{$rfq->cat4}}</td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                    <td class="text-center" colspan="5">No Records Found!</td>
                                    </tr>
                                    @endif
                                </tbody>
                               </table>
                              </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-bell" aria-hidden="true"></i> Notification</h3>
                            </div>
                           <div class="panel-body">
                                <div class="list-group">
                                    <a href="javascript:void(0)" class="list-group-item">
                                        <span class="badge">just now</span>
                                        <i class="fa fa-fw fa-calendar"></i> Calendar updated
                                    </a>
                                    <a href="javascript:void(0)" class="list-group-item">
                                        <span class="badge">4 minutes ago</span>
                                        <i class="fa fa-fw fa-comment"></i> Commented on a post
                                    </a>
                                    <a href="javascript:void(0)" class="list-group-item">
                                        <span class="badge">23 minutes ago</span>
                                        <i class="fa fa-fw fa-truck"></i> Order 392 shipped
                                    </a>
                                    <a href="javascript:void(0)" class="list-group-item">
                                        <span class="badge">46 minutes ago</span>
                                        <i class="fa fa-fw fa-money"></i> Invoice 653 has been paid
                                    </a>
                                    <a href="javascript:void(0)" class="list-group-item">
                                        <span class="badge">1 hour ago</span>
                                        <i class="fa fa-fw fa-user"></i> A new user has been added
                                    </a>
                                    <a href="javascript:void(0)" class="list-group-item">
                                        <span class="badge">2 hours ago</span>
                                        <i class="fa fa-fw fa-check"></i> Completed task: "pick up dry cleaning"
                                    </a>
                                    <a href="javascript:void(0)" class="list-group-item">
                                        <span class="badge">yesterday</span>
                                        <i class="fa fa-fw fa-globe"></i> Saved the world
                                    </a>
                                    <a href="javascript:void(0)" class="list-group-item">
                                        <span class="badge">two days ago</span>
                                        <i class="fa fa-fw fa-check"></i> Completed task: "fix error on sales"
                                    </a>
                                </div>
                                <div class="text-right">
                                    <a href="javascript:void(0)">View All Notification &nbsp; <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@include('layouts.footer'); 

<!-- Modal Start-->
<div id="details-pop" class="modal fade quotation-pop details-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title po_ORDNO">PO/001005/16-17</h4>
      </div>
      <div class="modal-body">
        <div class="row shorting-area pricechange">
        	<div class="col-sm-3"><h5>PO No. : <strong class=" po_ORDNO"></strong></h5></div>
            <div class="col-sm-3"><h5>PO Date : <strong class="po_ORDDT"></strong></h5></div>
            <div class="col-sm-3"><h5>Document No : <strong>----</strong></h5></div>
            <div class="col-sm-3"><h5>ValidIty Period : <strong class="po_TIME"></strong></h5></div>
            <div class="col-sm-3"><h5>To : <strong class="TIME"></strong></h5></div>
             <div class="col-sm-3"><h5>Credit Days : <strong>30</strong></h5></div>
             <!--<div class="col-sm-3"><h5>Form Name : <strong>C Form</strong></h5></div>-->
            <div class="col-sm-3"><h5>Currency : <strong>Rupees</strong></h5></div>
            <div class="col-sm-3"><h5>Exchange Rate : <strong>1000</strong></h5></div>
            <div class="clearfix"></div>
            <hr>
        <div class="clearfix"></div>
        	<div class="custome-table">
            <div class="table-responsive ">
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                            	<th class="text-center">SL No.</th>
                                                <th class="text-center">Cat 1</th>
                                                <th class="text-center">Cat 2</th>
                                                <th class="text-center">Cat 3</th>
                                                <th class="text-center">Cat 4</th>
                                                <th class="text-center">Due Date</th>
                                                <th class="text-center">Qty</th>
                                                <th class="text-center">UOM</th>
                                                <th class="text-center">Rate</th>
                                                <th class="text-center">Amount</th>
                                                <th class="text-center">Basic</th>
                                            </tr>
                                        </thead>
                                    <tbody id = 'tab_data'>
                                   
                                    
                                </tbody>
                               </table>
                              </div>
            	<div class="col-sm-2 accept-area">
                    <a href="javascript:void(0)" id="acceptButton" class="btn btn-success">Accept</a>
                </div>
                <div class="col-sm-2 accept-area">
                    <a href="javascript:void(0)" id="rejectButton" class="btn btn-danger">Reject</a>
                </div>
            </div>
            
            
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!-- Modal End-->

<!--reject popup Modal Start-->
<div id="rejectpop" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Reason for Reject </h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="po_id" id="po_id" value="">
        <div class="row shorting-area pricechange">
            <div class="col-sm-12 text-center"><h4>PO. No : <strong class="po-no"></strong> <br/>
            </h4></div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <h5>Reason :</h5>
            <textarea class="form-control" id="vendorReject_remarks"></textarea>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <a href="javascript:void(0)" onclick="rejectPO()" class="pup-btn">Request Submit</a>
            </div>
            <div class="col-sm-6">
                <a href="javascript:void(0)" data-dismiss="modal" class="pup-btn">Cancel
                </a>
            </div>
        <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--reject popup  Modal End-->

<!--Accept and Reject Success popup Modal Start-->
<div id="updatePO-modal-body" class="modal fade quotation-pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="updatePO-modal-body">
        
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>
<!--Accept and Reject Success popup  Modal End-->
<script src="{{asset('js/custom/purchase_order.js')}}"></script>
<script>
    $('.reject-po').click(function(){
    
         var po_no = $(this).attr('po_no');
         var po_id = $(this).attr('po_id');
        $('.po-no').text(po_no);
        $('#po_id').val(po_id);
    });
    function acceptPO(id) {
        var po = confirm('Do you want to accept this PO?');
        if(po == true){
            $.ajax({
            type: 'GET', 
            url: '{{route("vendor-po-accept")}}',
            data: {id:id},
            success:function(data)
            {
                if(data==1)
                {
                    $('.updatePO-modal-body').html('PO accepted successfully!');
                    window.location.reload();
                }
                else
                    $('.updatePO-clearfix').text('Please Try again!');
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
        }else{
            window.location.reload();
        }
    }

    function rejectPO() {
        var remarks=$("#vendorReject_remarks").val();
        var po_id=$("#po_id").val();
        var ptoken = '{{csrf_token()}}';
        $.ajax({
            type: 'POST', 
            url: '{{route("vendor-po-reject")}}',
            data: {id:po_id,remarks:remarks,_token:ptoken},
            success:function(data)
            {
                if(data==1)
                {
                    $('.updatePO-modal-body').html('PO rejected successfully!');
                    window.location.reload();
                }
                else
                    $('.updatePO-clearfix').text('Please Try again!');
            },
            error: function (error) 
            {
                
                alert('Error Occured Please Try Again');
            }
        });
    }

    function poDetails(id) {
        $.ajax({
            type: 'GET',
            url: '{{route("vendor-po-details")}}',
            data: {id:id},
            success:function(data){
                var poId = id;
                var po_ORDNO = data[0].ORDNO;
                var po_ORDDT = data[0].ORDDT;
                var po_TIME = data[0].TIME;
                $('.po_ORDNO').text(po_ORDNO);
                $('.po_ORDDT').text(po_ORDDT);
                $('.po_TIME').text(po_TIME);
                var details_html = '';
                var total_ORDQTY = 0;
                var total_amount = 0;
                var total_basic = 0;
                for (i = 0; i < data.length; i++) {
                    
                    var total_ORDQTY =total_ORDQTY + data[i].ORDQTY;
                    var total_amount = total_amount+data[i].amount;
                    var total_basic =total_basic+ data[i].amount;

                    details_html +='<tr><td class="text-center">'+(i+1)+'</td>';
                    details_html +='<td class="text-center">'+data[i].cat1+'</td>';
                    details_html +='<td class="text-center">'+data[i].cat2+'</td>';
                    details_html +='<td class="text-center">'+data[i].cat3+'</td>';
                    details_html +='<td class="text-center">'+data[i].cat4+'</td>';
                    details_html +='<td class="text-center">'+data[i].ORDDT+'</td>';
                    details_html +='<td class="text-center">'+data[i].ORDQTY+'</td>';
                    details_html +='<td class="text-center">'+"N"+'</td>';
                    details_html +='<td class="text-center">'+data[i].RATE+'</td>';
                    details_html +='<td class="text-center">'+data[i].amount+'</td>';
                    details_html +='<td class="text-center">'+data[i].amount+'</td>';
                    details_html +='</tr>';
                }
                details_html +='<tr><td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+"Total :"+'</strong></td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+total_ORDQTY+'</strong></td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+total_amount+'</strong></td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+total_basic+'</strong></td>';
                    details_html +='</tr>';
                    details_html +='<tr><td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+"CST Purchase"+'</strong></td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>&nbsp;</strong></td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+"2 % on 136670"+'</strong></td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+27334.00+'</strong></td>';
                    details_html +='</tr>';
                    details_html +='<tr><td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;">&nbsp;</td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+"Net Amount"+'</strong></td>';
                    details_html +='<td class="text-center" style="border:0;"><strong>'+1394034.00+'</strong></td>';
                    details_html +='</tr>';
                $("#tab_data").html(details_html);

                $('#acceptButton').attr('onclick','"acceptPO('+poId+');"');
                
                $('#rejectButton').attr('onclick','"rejectPO('+poId+');"');

            }
        });
    }
</script>