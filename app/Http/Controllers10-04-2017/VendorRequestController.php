<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Redirect;

class VendorRequestController extends Controller
{
    // public function viewAllRequest()
    // {
    //     $request_list = DB::table('po_action')        
    //     ->select('po_action.*')
    //     ->where('po_action.user_id', '=',Auth::user()->id)
    //     ->orderBy('priority','asc') 
    //     ->orderBy('created_at','desc')         
    //     ->paginate(20);
    //     $po_list = DB::table('po_action')        
    //     ->select('po_no')
    //     ->distinct('po_no') 
    //     ->where('po_action.user_id', '=',Auth::user()->id)               
    //     ->get(); 
    //   if(!empty($request_list) && count($request_list)>0)
    //     {
    //         foreach($request_list as $reqst)
    //         {
    //             $req_comments =DB::table('request_comments')->where('action_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
    //                     if(!empty($req_comments) && count($req_comments)>0)
    //                     $reqst->req_comments=$req_comments;
    //             }
    //         }

    //    // dd($request_list);
    // 	return view('vendor.vendor-all-request')->with('request_list',$request_list)->with('po_list',$po_list);
    // }

    public function viewAllcuttingsampleothersRequest()
    {
        $request_list = DB::table('po_action')        
        ->select('po_action.*')
        ->where('po_action.user_id', '=',Auth::user()->id)
        ->whereIn('action_type',array('cutting', 'cancel', 'others','sample'))
        ->orderBy('priority','asc') 
        ->orderBy('created_at','desc') 
        ->orderBy('updated_at','desc')        
        ->paginate(20);
        $po_list = DB::table('po_action')        
        ->select('po_no')
        ->whereIn('action_type',array('cutting', 'cancel', 'others','sample'))
        ->distinct('po_no') 
        ->where('po_action.user_id', '=',Auth::user()->id)               
        ->get(); 
      if(!empty($request_list) && count($request_list)>0)
        {
            foreach($request_list as $reqst)
            {
                $req_comments =DB::table('request_comments')->where('action_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
                        if(!empty($req_comments) && count($req_comments)>0)
                        $reqst->req_comments=$req_comments;
                }
            }

        //dd($request_list);
        return view('vendor.CuttingSampleOthers-request')->with('request_list',$request_list)->with('po_list',$po_list);
    }

    public function filterAction(Request $request)
    {
        /*--Filter of viewAllcuttingsampleothersRequest By Type*/
      $user_type=Auth::user()->type;      
      $status=$request->status;
        $type=$request->type;
        $po_no=$request->po_no;
        $query="action_type != ''";
        if(!empty($type))
            $query .=" AND action_type = '".$type."' ";
        if(!empty($po_no))
            $query .=" AND po_no = '".$po_no."' ";
      if(!empty($status))
        $query .=" AND status = '".$status."' ";
         if($user_type=='vendor'){
                $data['request_list'] = DB::table('po_action')  
                ->select('po_action.*')
                ->where('user_id', '=',Auth::user()->id) 
                ->whereIn('action_type',array('cutting', 'cancel', 'others','sample'))
                ->whereRaw($query)
                ->orderBy('priority','asc') 
                ->orderBy('created_at','desc') 
                ->orderBy('updated_at','desc')             
                ->paginate(20);
        }
        else{
          $data['request_list'] = DB::table('po_action')  
            ->select('po_action.*')  
            ->whereIn('action_type',array('cutting', 'cancel', 'others','sample'))         
            ->whereRaw($query)
            ->orderBy('priority','asc') 
            ->orderBy('created_at','desc')
            ->orderBy('updated_at','desc')              
            ->paginate(20);
        }
         if(!empty($data['request_list']) && count($data['request_list'])>0)
        {
            foreach($data['request_list'] as $reqst)
            {
                $req_comments =DB::table('request_comments')->where('action_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
                        if(!empty($req_comments) && count($req_comments)>0)
                        $reqst->req_comments=$req_comments;
                }
            }
       

  return view('vendor.CuttingSampleOthers-request-filter',$data); 
    }
    

    public function viewAllwidthrateRequest()
    {
        $request_list = DB::table('po_action')        
        ->select('po_action.*')
        ->where('po_action.user_id', '=',Auth::user()->id)
        ->whereIn('action_type',array('width_change', 'rate_change'))
        ->orderBy('priority','asc') 
        ->orderBy('created_at','desc')       
        ->paginate(20);
        $po_list = DB::table('po_action')        
        ->select('po_no')
        ->whereIn('action_type',array('width_change', 'rate_change'))
        ->distinct('po_no') 
        ->where('po_action.user_id', '=',Auth::user()->id)               
        ->get(); 
      if(!empty($request_list) && count($request_list)>0)
        {
            foreach($request_list as $reqst)
            {
                $req_comments =DB::table('request_comments')->where('action_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
                        if(!empty($req_comments) && count($req_comments)>0)
                        $reqst->req_comments=$req_comments;
                }
            }

        //dd($request_list);
        return view('vendor.widthratechange-request')->with('request_list',$request_list)->with('po_list',$po_list);
    }
    

    public function widthrateRequestFilter(Request $request)
    {
        /*--Filter of viewAllwidthrateRequest By Type*/
      $user_type=Auth::user()->type;      
      $status=$request->status;
        $type=$request->type;
        $po_no=$request->po_no;
        $query="action_type != ''";
        if(!empty($type))
            $query .=" AND action_type = '".$type."' ";
        if(!empty($po_no))
            $query .=" AND po_no = '".$po_no."' ";
      if(!empty($status))
        $query .=" AND status = '".$status."' ";
         if($user_type=='vendor'){
                $data['request_list'] = DB::table('po_action')  
                ->select('po_action.*')
                ->where('user_id', '=',Auth::user()->id) 
                ->whereIn('action_type',array('width_change', 'rate_change'))
                ->whereRaw($query)
                ->orderBy('priority','asc') 
                ->orderBy('created_at','desc')            
                ->paginate(20);
        }
        else{
          $data['request_list'] = DB::table('po_action')  
            ->select('po_action.*')  
            ->whereIn('action_type',array('width_change', 'rate_change'))      
            ->whereRaw($query)
            ->orderBy('priority','asc') 
            ->orderBy('created_at','desc')            
            ->paginate(20);
        }
         if(!empty($data['request_list']) && count($data['request_list'])>0)
        {
            foreach($data['request_list'] as $reqst)
            {
                $req_comments =DB::table('request_comments')->where('action_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
                        if(!empty($req_comments) && count($req_comments)>0)
                        $reqst->req_comments=$req_comments;
                }
            }
       

  return view('vendor.widthratechange-request-filter',$data); 
    }
    

    public function viewAlldateextRequest()
    {
        $request_list = DB::table('po_action')        
        ->select('po_action.*')
        ->where('po_action.user_id', '=',Auth::user()->id)
        ->where('action_type','=','date_ext')
        ->orderBy('priority','asc') 
        ->orderBy('created_at','desc') 
        ->orderBy('updated_at','desc')        
        ->paginate(20);
        $po_list = DB::table('po_action')        
        ->select('po_no')
        ->where('action_type','=','date_ext')
        ->distinct('po_no') 
        ->where('po_action.user_id', '=',Auth::user()->id)               
        ->get(); 
      if(!empty($request_list) && count($request_list)>0)
        {
            foreach($request_list as $reqst)
            {
                $req_comments =DB::table('request_comments')->where('action_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
                        if(!empty($req_comments) && count($req_comments)>0)
                        $reqst->req_comments=$req_comments;

               
                $extended_no =DB::table('po_action')->select(DB::raw('count(id) as total_ext'))->where('status', '=','accept')->where('po_no','=',$reqst->po_no)->where('ICODE','=',$reqst->ICODE)->first();
                    $reqst->extended_no=$extended_no->total_ext;
                }
            }

        //dd($request_list);
        return view('vendor.alldateext-request')->with('request_list',$request_list)->with('po_list',$po_list);
    }
    

    public function dateextRequestFilter(Request $request)
    {
        /*--Filter of viewAlldateextRequest By Type*/
      $user_type=Auth::user()->type;      
      $status=$request->status;       
      $po_no=$request->po_no;
      $query="action_type = 'date_ext'";
       //echo Auth::user()->id; 
        if(!empty($po_no))
            $query .=" AND po_no = '".$po_no."' ";
        if(!empty($status))
           $query .=" AND status = '".$status."' ";
         if($user_type=='vendor'){
                $data['request_list'] = DB::table('po_action')  
                ->select('po_action.*')
                ->where('user_id', '=',Auth::user()->id) 
                //->where('action_type','=','date_ext')
                ->whereRaw($query)
                ->orderBy('priority','asc') 
                ->orderBy('created_at','desc') 
                ->orderBy('updated_at','desc')
                //->toSql();             
                ->paginate(20);
        }
        else{
          $data['request_list'] = DB::table('po_action')  
            ->select('po_action.*')  
            //->where('action_type','=','date_ext')     
            ->whereRaw($querys)
            ->orderBy('priority','asc') 
            ->orderBy('created_at','desc') 
            ->orderBy('updated_at','desc')             
           //->toSql();             
                ->paginate(20);
        }
        //dd($data['request_list']);exit;
         if(!empty($data['request_list']) && count($data['request_list'])>0)
        {
            foreach($data['request_list'] as $reqst)
            {
                $req_comments =DB::table('request_comments')->where('action_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
                        if(!empty($req_comments) && count($req_comments)>0)
                        $reqst->req_comments=$req_comments;

                $extended_no =DB::table('po_action')->select(DB::raw('count(id) as total_ext'))->where('status', '=','accept')->where('po_no','=',$reqst->po_no)->where('ICODE','=',$reqst->ICODE)->first();
                    $reqst->extended_no=$extended_no->total_ext;
            }
        }       

        return view('vendor.alldateext-request-filter',$data); 
    }

    public function sampleRequest()
    {      
        return view('vendor.vendor-sample-request');
    }
	
	
	public function filterSampleRequest(Request $request)
    {
		
		$po_number=$request->po_number;
		$category1=$request->category1;
		$category2=$request->category2;
		$category4=$request->category4;

        $user = Auth::user();
		
		$query_str=" purchase_order.status = 'OPEN' AND V_PURORDDET.status='OPEN' AND purchase_order.PCODE = '".$user->SLCODE."' AND  (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) > 0";

    	if(!empty($po_number))
    		$query_str .=" AND purchase_order.PONO = '".$po_number."' ";
    	if(!empty($category1))
    		$query_str .=" AND products.cat1 = '".$category1."' ";
		if(!empty($category2))
    		$query_str .=" AND products.cat2 = '".$category2."' ";
		if(!empty($category4))
    		$query_str .=" AND products.CCODE4 = '".$category4."' ";
			
		
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.PONO', 'purchase_order.ORDDT','purchase_order.TIME','products.ICODE','products.cat1','products.cat2','products.cat3','products.cat4','V_PURORDDET.RATE','purchase_order_delivery.DUEDATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY',DB::raw('sum(git.git_qty) as git_qty'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('purchase_order_delivery', 'purchase_order_delivery.PURORDDET_CODE', '=', 'V_PURORDDET.CODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE') 
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')       
		->whereRaw($query_str)
        //->orderBy('purchase_order.ORDDT', 'desc')
        ->orderBy('purchase_order_delivery.DUEDATE', 'asc')
		->groupBy('V_PURORDDET.id')
        ->paginate(30);
		#->toSql();	
		#dd( $purchaseOrderList);
		if(!empty($purchaseOrderList) && count($purchaseOrderList)>0)
        {
            foreach($purchaseOrderList as $reqst)
            {
                $actiondtls =DB::table('po_action')->select('action_type','status','ICODE')->where('po_no', '=',$reqst->PONO)->where('status', '=','new')->orderBy('created_at','desc')->get();
                        if(!empty($actiondtls) && count($actiondtls)>0)
                        $reqst->actiondtls=$actiondtls;
                }
            }	
		//dd($purchaseOrderList);
        return view('vendor.vendor-sample-request-filter')->with('purchaseOrderList', $purchaseOrderList);
    }
	
	
	
    public function viewRateChange()
    {
    	return view('vendor.vendor-request-rate-change');
    }
	
	
	
	
	public function filterRateChange(Request $request)
    {
		
		$po_number=$request->po_number;
        $category1=$request->category1;
        $category2=$request->category2;
        $category4=$request->category4;

        $user = Auth::user();
        
        $query_str=" purchase_order.status = 'OPEN' AND V_PURORDDET.status='OPEN' AND purchase_order.PCODE = '".$user->SLCODE."' AND  (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) > 0";

        if(!empty($po_number))
            $query_str .=" AND purchase_order.PONO = '".$po_number."' ";
        if(!empty($category1))
            $query_str .=" AND products.cat1 = '".$category1."' ";
        if(!empty($category2))
            $query_str .=" AND products.cat2 = '".$category2."' ";
        if(!empty($category4))
            $query_str .=" AND products.CCODE4 = '".$category4."' ";
            
            
       
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.PONO', 'purchase_order.ORDDT','purchase_order.TIME','products.ICODE','products.cat1','products.cat2','products.cat3','products.cat4','V_PURORDDET.RATE','purchase_order_delivery.DUEDATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY',DB::raw('sum(git.git_qty) as git_qty'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')  
        ->leftJoin('purchase_order_delivery', 'purchase_order_delivery.PURORDDET_CODE', '=', 'V_PURORDDET.CODE')   
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')   
        ->whereRaw($query_str)
        ->orderBy('purchase_order_delivery.DUEDATE', 'asc')
        ->groupBy('V_PURORDDET.id')
        ->paginate(30); 		
		
		if(!empty($purchaseOrderList) && count($purchaseOrderList)>0)
        {
            foreach($purchaseOrderList as $reqst)
            {
                $actiondtls =DB::table('po_action')->select('action_type','status','ICODE')->where('po_no', '=',$reqst->PONO)->whereIn('status',array('new', 'inprocess'))->orderBy('created_at','desc')->get();
                        if(!empty($actiondtls) && count($actiondtls)>0)
                        $reqst->actiondtls=$actiondtls;
                }
            }
		
        return view('vendor.vendor-request-rate-change-filter')->with('purchaseOrderList', $purchaseOrderList);
    }
	
    public function viewRequestCutting()
    {
    	return view('vendor.vendor-cutting-request');
    }
	
	public function filterRequestCutting(Request $request)
    {
		
		$po_number=$request->po_number;
        $category1=$request->category1;
        $category2=$request->category2;
        $category4=$request->category4;

        $user = Auth::user();
        
        $query_str=" purchase_order.status = 'OPEN' AND V_PURORDDET.status='OPEN' AND purchase_order.PCODE = '".$user->SLCODE."' AND  (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) > 0";

        if(!empty($po_number))
            $query_str .=" AND purchase_order.PONO = '".$po_number."' ";
        if(!empty($category1))
            $query_str .=" AND products.cat1 = '".$category1."' ";
        if(!empty($category2))
            $query_str .=" AND products.cat2 = '".$category2."' ";
        if(!empty($category4))
            $query_str .=" AND products.CCODE4 = '".$category4."' ";
            
            
        $user = Auth::user();
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.PONO', 'purchase_order.ORDDT','purchase_order.TIME','products.ICODE','products.cat1','products.cat2','products.cat3','products.cat4','V_PURORDDET.RATE','purchase_order_delivery.DUEDATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY',DB::raw('sum(git.git_qty) as git_qty'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE') 
        ->leftJoin('purchase_order_delivery', 'purchase_order_delivery.PURORDDET_CODE', '=', 'V_PURORDDET.CODE')   
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')     
        ->whereRaw($query_str)
        ->orderBy('purchase_order_delivery.DUEDATE', 'asc')
        ->groupBy('V_PURORDDET.id')
        ->paginate(30); 	
		
		if(!empty($purchaseOrderList) && count($purchaseOrderList)>0)
        {
            foreach($purchaseOrderList as $reqst)
            {
                $actiondtls =DB::table('po_action')->select('action_type','status','ICODE')->where('po_no', '=',$reqst->PONO)->where('status', '=','new')->orderBy('created_at','desc')->get();
                        if(!empty($actiondtls) && count($actiondtls)>0)
                        $reqst->actiondtls=$actiondtls;
                }
            }
		
        return view('vendor.vendor-cutting-request-filter')->with('purchaseOrderList', $purchaseOrderList);
    }
	
    public function viewWidthChanges()
    {
         
		
    	return view('vendor.vendor-width-change');
    }
	
	public function filterWidthChanges(Request $request)
    {
		
		$po_number=$request->po_number;
        $category1=$request->category1;
        $category2=$request->category2;
        $category4=$request->category4;

        $user = Auth::user();
        
        $query_str=" purchase_order.status = 'OPEN' AND V_PURORDDET.status='OPEN' AND purchase_order.PCODE = '".$user->SLCODE."' AND  (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) > 0";

        if(!empty($po_number))
            $query_str .=" AND purchase_order.PONO = '".$po_number."' ";
        if(!empty($category1))
            $query_str .=" AND products.cat1 = '".$category1."' ";
        if(!empty($category2))
            $query_str .=" AND products.cat2 = '".$category2."' ";
        if(!empty($category4))
            $query_str .=" AND products.CCODE4 = '".$category4."' ";
            
            
        $user = Auth::user();
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.PONO', 'purchase_order.ORDDT','purchase_order.TIME','products.ICODE','products.is_fabric','products.cat1','products.cat2','products.cat3','products.cat4','V_PURORDDET.RATE','purchase_order_delivery.DUEDATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY',DB::raw('sum(git.git_qty) as git_qty'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('purchase_order_delivery', 'purchase_order_delivery.PURORDDET_CODE', '=', 'V_PURORDDET.CODE')  
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')       
        ->whereRaw($query_str)
        ->orderBy('purchase_order_delivery.DUEDATE', 'asc')
        ->groupBy('V_PURORDDET.id')
        ->paginate(30); 	
		
		if(!empty($purchaseOrderList) && count($purchaseOrderList)>0)
        {
            foreach($purchaseOrderList as $reqst)
            {
                $actiondtls =DB::table('po_action')->select('action_type','status','ICODE')->where('po_no', '=',$reqst->PONO)->whereIn('status',array('new', 'inprocess'))->orderBy('created_at','desc')->get();
                        if(!empty($actiondtls) && count($actiondtls)>0)
                        $reqst->actiondtls=$actiondtls;
                }
            }	
		
     
        return view('vendor.vendor-width-change-filter')->with('purchaseOrderList', $purchaseOrderList);
    }
	
    public function viewVendorDateExt()
    {
        return view('vendor.vendor-date-extention');
    }
	
	public function filterVendorDateExt(Request $request)
    {
		
		$po_number=$request->po_number;
        $category1=$request->category1;
        $category2=$request->category2;
        $category4=$request->category4;

        $user = Auth::user();
        
        $query_str=" purchase_order.status = 'OPEN' AND V_PURORDDET.status='OPEN' AND purchase_order.PCODE = '".$user->SLCODE."' AND  (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) > 0";

        if(!empty($po_number))
            $query_str .=" AND purchase_order.PONO = '".$po_number."' ";
        if(!empty($category1))
            $query_str .=" AND products.cat1 = '".$category1."' ";
        if(!empty($category2))
            $query_str .=" AND products.cat2 = '".$category2."' ";
        if(!empty($category4))
            $query_str .=" AND products.CCODE4 = '".$category4."' ";
            
            
        $user = Auth::user();
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.PONO', 'purchase_order.ORDDT','purchase_order.TIME','products.ICODE','products.cat1','products.cat2','products.cat3','products.cat4','V_PURORDDET.RATE','purchase_order_delivery.DUEDATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY',DB::raw('sum(git.git_qty) as git_qty'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE') 
        ->leftJoin('purchase_order_delivery', 'purchase_order_delivery.PURORDDET_CODE', '=', 'V_PURORDDET.CODE')   
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')     
        ->whereRaw($query_str)
        ->orderBy('purchase_order_delivery.DUEDATE', 'asc')
        ->groupBy('V_PURORDDET.id')
        ->paginate(30); 
		//dd($purchaseOrderList);
		if(!empty($purchaseOrderList) && count($purchaseOrderList)>0)
        {
            foreach($purchaseOrderList as $reqst)
            {
                $actiondtls =DB::table('po_action')->select('action_type','status','ICODE')->where('po_no', '=',$reqst->PONO)->where('status', '=','new')->orderBy('created_at','desc')->get();
                        if(!empty($actiondtls) && count($actiondtls)>0)
                        $reqst->actiondtls=$actiondtls;
                }
            }	
		
        return view('vendor.vendor-date-extention-filter')->with('purchaseOrderList', $purchaseOrderList);
    }
	
	
    public function viewVendorCalcellation()
    {
        

        return view('vendor.vendor-calcellation');
    }
	
	public function filterVendorCalcellation(Request $request)
    {
		
		$po_number=$request->po_number;
        $category1=$request->category1;
        $category2=$request->category2;
        $category4=$request->category4;

        $user = Auth::user();
        
        $query_str=" purchase_order.status = 'OPEN' AND V_PURORDDET.status='OPEN' AND purchase_order.PCODE = '".$user->SLCODE."' AND  (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) > 0";

        if(!empty($po_number))
            $query_str .=" AND purchase_order.PONO = '".$po_number."' ";
        if(!empty($category1))
            $query_str .=" AND products.cat1 = '".$category1."' ";
        if(!empty($category2))
            $query_str .=" AND products.cat2 = '".$category2."' ";
        if(!empty($category4))
            $query_str .=" AND products.CCODE4 = '".$category4."' ";
            
            
        $user = Auth::user();
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.PONO', 'purchase_order.ORDDT','purchase_order.TIME','products.ICODE','products.cat1','products.cat2','products.cat3','products.cat4','V_PURORDDET.RATE','purchase_order_delivery.DUEDATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY',DB::raw('sum(git.git_qty) as git_qty'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE') 
        ->leftJoin('purchase_order_delivery', 'purchase_order_delivery.PURORDDET_CODE', '=', 'V_PURORDDET.CODE')    
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')    
        ->whereRaw($query_str)
        ->orderBy('purchase_order_delivery.DUEDATE', 'asc')
        ->groupBy('V_PURORDDET.id')
        ->paginate(30); 	
		
		if(!empty($purchaseOrderList) && count($purchaseOrderList)>0)
        {
            foreach($purchaseOrderList as $reqst)
            {
                $actiondtls =DB::table('po_action')->select('action_type','status','ICODE')->where('po_no', '=',$reqst->PONO)->where('status', '=','new')->orderBy('created_at','desc')->get();
                        if(!empty($actiondtls) && count($actiondtls)>0)
                        $reqst->actiondtls=$actiondtls;
                }
            }	
		
        return view('vendor.vendor-calcellation-filter')->with('purchaseOrderList', $purchaseOrderList);
    }
    public function viewVendorcreateSupportTicket()
    {
    //     $user = Auth::user();
    //     $po_list = DB::table('purchase_order')
    //     ->select('id','ORDNO','PONO')
    //     ->where('purchase_order.status', '=','OPEN')    
    //     ->where('purchase_order.PCODE', '=',$user->SLCODE)
    //     ->get();
        
    //     if(!empty($po_list) && count($po_list)>0)
    //     {
    //         foreach($po_list as $reqst)
    //         {
    //             $actiondtls =DB::table('po_action')->select('action_type','status')->where('po_no', '=',$reqst->PONO)->where('status', '=','new')->orderBy('created_at','desc')->get();
    //                     if(!empty($actiondtls) && count($actiondtls)>0)
    //                     $reqst->actiondtls=$actiondtls;
    //             }
    //         }   
    //     //dd($po_list);
    //     return view('vendor.vendor-create-support-ticket')->with('po_list',$po_list);
        

        return view('vendor.vendor-create-support-ticket');
     }

     public function filterCreateSupportTicket(Request $request)
    {
        
        $po_number=$request->po_number;
        $category1=$request->category1;
        $category2=$request->category2;
        $category4=$request->category4;

        $user = Auth::user();
        
        $query_str=" purchase_order.status = 'OPEN' AND V_PURORDDET.status='OPEN' AND purchase_order.PCODE = '".$user->SLCODE."' AND  (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) > 0";

        if(!empty($po_number))
            $query_str .=" AND purchase_order.PONO = '".$po_number."' ";
        if(!empty($category1))
            $query_str .=" AND products.cat1 = '".$category1."' ";
        if(!empty($category2))
            $query_str .=" AND products.cat2 = '".$category2."' ";
        if(!empty($category4))
            $query_str .=" AND products.CCODE4 = '".$category4."' ";
            
            
        $user = Auth::user();
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.PONO', 'purchase_order.ORDDT','purchase_order.TIME','products.ICODE','products.cat1','products.cat2','products.cat3','products.cat4','V_PURORDDET.RATE','purchase_order_delivery.DUEDATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY',DB::raw('sum(git.git_qty) as git_qty'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE') 
        ->leftJoin('purchase_order_delivery', 'purchase_order_delivery.PURORDDET_CODE', '=', 'V_PURORDDET.CODE')   
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')     
        ->whereRaw($query_str)
        ->orderBy('purchase_order_delivery.DUEDATE', 'asc')
        ->groupBy('V_PURORDDET.id')
        ->paginate(30);   
        
        if(!empty($purchaseOrderList) && count($purchaseOrderList)>0)
        {
            foreach($purchaseOrderList as $reqst)
            {
                $actiondtls =DB::table('po_action')->select('action_type','status','ICODE')->where('po_no', '=',$reqst->PONO)->where('status', '=','new')->orderBy('created_at','desc')->get();
                        if(!empty($actiondtls) && count($actiondtls)>0)
                        $reqst->actiondtls=$actiondtls;
                }
            }   
        //dd($purchaseOrderList);
        return view('vendor.vendor-create-support-ticket-filter')->with('purchaseOrderList', $purchaseOrderList);
    }
    public function addSupportRequest(Request $request)
    {
        $return=0;
        
        
        $support['user_id'] = Auth::user()->id;
        $support['po_no'] = $request->order_id;   
        $support['action_type'] = 'others';
        $support['remarks'] = $request->remks;   
        $support['created_at'] = date('Y-m-d H:i:s');
        $support['updated_at'] = date('Y-m-d H:i:s');
                
        if(DB::table('po_action')->insert($support))
        {
            $return=1;
        }
        echo $return;
       
    }

  //    public function filterAction(Request $request)
  //   {
  //       /*--Filter By Type*/
  //     $user_type=Auth::user()->type;      
  //     $status=$request->status;
  //       $type=$request->type;
  //       $po_no=$request->po_no;
  //       $query="action_type != ''";
  //       if(!empty($type))
  //           $query .=" AND action_type = '".$type."' ";
  //       if(!empty($po_no))
  //           $query .=" AND po_no = '".$po_no."' ";
  //     if(!empty($status))
  //       $query .=" AND status = '".$status."' ";
  //        if($user_type=='vendor'){
  //               $data['request_list'] = DB::table('po_action')  
  //               ->select('po_action.*')
  //               ->where('user_id', '=',Auth::user()->id) 
  //               ->whereRaw($query)
  //               ->orderBy('priority','asc') 
  //               ->orderBy('created_at','desc')              
  //               ->paginate(20);
  //       }
  //       else{
  //         $data['request_list'] = DB::table('po_action')  
  //           ->select('po_action.*')           
  //           ->whereRaw($query)
  //           ->orderBy('priority','asc') 
  //           ->orderBy('created_at','desc')              
  //           ->paginate(20);
  //       }
  //        if(!empty($data['request_list']) && count($data['request_list'])>0)
  //       {
  //           foreach($data['request_list'] as $reqst)
  //           {
  //               $req_comments =DB::table('request_comments')->where('action_id', '=',$reqst->id)->orderBy('created_at','desc')->get();
  //                       if(!empty($req_comments) && count($req_comments)>0)
  //                       $reqst->req_comments=$req_comments;
  //               }
  //           }
  //       //dd($request_list);
  // //       $html='';
  // //       $inc=0;
  // //       if(!empty($request_list) && count($request_list)){
  // //        foreach ($request_list as $req) {
  // //                                        $type='';
  // //                                        if($req->action_type=='cutting')
  // //                                         $type='Request For Cutting';
  // //                                         elseif($req->action_type=='rate_change') 
  // //                                          $type='Request For Rate Change';
  // //                                         elseif($req->action_type=='width_change') 
  // //                                          $type='Request For Width Change';
  // //                                         elseif($req->action_type=='date_ext') 
  // //                                          $type='Request For Date Extention';
  // //                                         elseif($req->action_type=='cancel') 
  // //                                          $type='Request For Cancellation';
  // //                                         elseif($req->action_type=='others') 
  // //                                          $type='Request For Others';
  // //                       $inc++;

  // //                                    $html .=' <tr>
  // //                                     <td class="text-center">'.$inc.'</td>
  // //                                    <td class="text-center">'.$req->po_no.'</td>
  // //                                    <td class="text-center">26/09/2016</td>
  // //                                       <td class="text-center">'.$type.'</td>
  // //                                       <td class="text-center">--</td>
  // //                                       <td class="text-center">--</td>
  // //                                       <td class="text-center">--</td>
  // //                                       <td class="text-center"><span class="sm-btn btn-primary btn-xs">New</span></td>
  // //                                       <td class="text-center">Lorem Ipsum... <a href="vendor-comments-2.php" class="btn btn-warning btn-xs">Details</a></td>
  // //                                       <td class="text-center"><a href="javascript:void(0)" class="btn btn-success">Accpet</a></td>
  // //                                   </tr>';

  // //                               }
  // //                           }
  // //                           else{
  // //                            $html .=' <tr>
  // //                                    <td class="text-center" colspan="10">No Result Found</td>
  // //                                   </tr>';


  // //       }

  // // echo $html;
        
  // //   }

  // return view('vendor.vendor-all-request-filter',$data); 
  //   }
     public function requestWiseComments($id)
    {
        //dd($id);
        $poactiondetails = DB::table('po_action')
                  ->select('*')
                  ->where('id', '=', $id)->first();
                  
        $Comments = DB::table('request_comments')
                  ->select('*')
                  ->where('action_id', '=', $id)->orderBy('created_at')->get();

        $extended_no =DB::table('po_action')->select(DB::raw('count(id) as total_ext'))->where('status', '=','accept')->where('po_no','=',$poactiondetails->po_no)->where('ICODE','=',$poactiondetails->ICODE)->first();
                                  
        //dd($extended_no);
        return view('vendor.requestwise-comments')->with('comments_list',$Comments)->with('action_id', $id)->with('poactiondetails',$poactiondetails)->with('extended_no',$extended_no);
       
    }
     public function addRequestComments(Request $request)
    {
        $return=0;
        
        $action['action_id'] = $request->action_id;
        $action['user_id'] = Auth::user()->id;   
        $action['comments'] = $request->comments;
        $action['created_at'] = date('Y-m-d H:i:s');
        $action['updated_at'] = date('Y-m-d H:i:s');
                
        if(DB::table('request_comments')->insert($action))
        {
            $return=1;
        }
        echo $return;
    }
    public function acceptRequest(Request $request)
    {
        // $reqs = DB::table('po_action')->where('id','=', $id)->update(['status' =>'close']);

        // return Redirect::route('all-requst-vendor');
        $id=$request->req_id;
        $return=0;
       
        $quotation['status'] = 'accept';
        $quotation['priority'] = '2'; 
        if(!empty($request->acpt_val)){
        $quotation['accepted_rate'] = $request->acpt_val;    
        }       
        if(DB::table('po_action')->where('id','=', $id)->update($quotation))
        {
			$acDtls = DB::table('po_action')->where('id','=', $id)->first();				
		
			$notify_array['message'] = ucfirst($acDtls->action_type).' Request for PO:'.$acDtls->po_no.' has been Closed';
			$notify_array['type'] = ucfirst($acDtls->action_type).'RequestAccepted';
            if($acDtls->action_type=='date_ext')
            {
            $notify_array['red_url'] = 'alldateext-request';
            }
            if($acDtls->action_type=='rate_change' || $acDtls->action_type=='width_change')
            {
            $notify_array['red_url'] = 'allwidthratechange-request';
            }
            if($acDtls->action_type=='cutting' || $acDtls->action_type=='cancel' || $acDtls->action_type=='others' || $acDtls->action_type=='sample')
            {
            $notify_array['red_url'] = 'allcuttingsampleothers-request';
            }
			$notify_array['sender'] = $acDtls->user_id;
			$notify_array['receiver'] =1;
			$notify_array['status'] = 'NEW';
			$notify_array['table_name'] = 'po_action';
			$notify_array['primary_key'] = 'id';
			$notify_array['primary_key_val'] =$id;
			$notify_array['created_at'] =date('Y-m-d H:i:s');	
			DB::table('notification')->insert($notify_array);
			
            return Redirect::back();
        }
        echo $return;
    }

   public function rejectRequest($id)
    {
       if(DB::table('po_action')->where('id','=', $id)->update(['status' =>'reject','priority' =>'4']))
        {
            $acDtls = DB::table('po_action')->where('id','=', $id)->first();                
        
            $notify_array['message'] = ucfirst($acDtls->action_type).' Request for PO:'.$acDtls->po_no.' has been Rejected';
            $notify_array['type'] = ucfirst($acDtls->action_type).'RequestRejected';
            if($acDtls->action_type=='date_ext')
            {
            $notify_array['red_url'] = 'alldateext-request';
            }
            if($acDtls->action_type=='rate_change' || $acDtls->action_type=='width_change')
            {
            $notify_array['red_url'] = 'allwidthratechange-request';
            }
            if($acDtls->action_type=='cutting' || $acDtls->action_type=='cancel' || $acDtls->action_type=='others' || $acDtls->action_type=='sample')
            {
            $notify_array['red_url'] = 'allcuttingsampleothers-request';
            }
            $notify_array['sender'] = $acDtls->user_id;
            $notify_array['receiver'] =1;
            $notify_array['status'] = 'NEW';
            $notify_array['table_name'] = 'po_action';
            $notify_array['primary_key'] = 'id';
            $notify_array['primary_key_val'] =$id;
            $notify_array['created_at'] =date('Y-m-d H:i:s');   
            DB::table('notification')->insert($notify_array);

        return Redirect::back();
    }
    }
   

    public function notifyToAdmin($id)
    {
        if(DB::table('po_action')->where('id','=', $id)->update(['notify' =>1]))
        {
            $acDtls = DB::table('po_action')->where('id','=', $id)->first();                
        
            $notify_array['message'] = ucfirst($acDtls->action_type).' Request for PO:'.$acDtls->po_no.' has been Notified';
            $notify_array['type'] = ucfirst($acDtls->action_type).'RequestNotified';
            if($acDtls->action_type=='date_ext')
            {
            $notify_array['red_url'] = 'alldateext-request';
            }
            if($acDtls->action_type=='rate_change' || $acDtls->action_type=='width_change')
            {
            $notify_array['red_url'] = 'allwidthratechange-request';
            }
            if($acDtls->action_type=='cutting' || $acDtls->action_type=='cancel' || $acDtls->action_type=='others' || $acDtls->action_type=='sample')
            {
            $notify_array['red_url'] = 'allcuttingsampleothers-request';
            }
            $notify_array['sender'] = $acDtls->user_id;
            $notify_array['receiver'] =1;
            $notify_array['status'] = 'NEW';
            $notify_array['table_name'] = 'po_action';
            $notify_array['primary_key'] = 'id';
            $notify_array['primary_key_val'] =$id;
            $notify_array['created_at'] =date('Y-m-d H:i:s');   
            DB::table('notification')->insert($notify_array);

        return Redirect::back();
    }
    }
}
