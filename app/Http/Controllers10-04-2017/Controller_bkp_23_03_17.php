<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	
	public function __construct(){//For Check Before Every Class Call 22-02-2017 Khalilur
			 $this->middleware(function ($request, $next) {
					$user = Auth::user();
					if($user->status== 0){
						Auth::logout();
						return redirect('/')->withErrors(['loginmsg' => 'Your account has been inactivated Recently!']);
					}
					return $next($request);
			 });
	}
		
	public function getInvItem()//Fetch Data From V_INVITEM table
    {
		$query_str="SELECT * from V_INVITEM where ROWNUM <= 1000";
		return Model::oracleQuery($query_str);
    }
	
	
	public function getPriceList()//Fetch Data From V_PUR_PRICE_CHART table
    {
		$query_str="SELECT count(*) from V_PUR_PRICE_CHART";
		return Model::oracleQuery($query_str);
    }
	
	public function getFinsl()//Fetch Data From V_FINSL table
    {
		$query_str="SELECT count(*) from V_FINSL";
		return Model::oracleQuery($query_str);
    }
	
	public function getPurOrdMain()//Fetch Data From V_PURORDMAIN table
    {
		$query_str="SELECT count(*) from V_PURORDMAIN";
		return Model::oracleQuery($query_str);
    }
	
	public function getPurOrdChg()//Fetch Data From V_PURORDCHG table
    {
		$query_str="SELECT count(*) from V_PURORDCHG";
		return Model::oracleQuery($query_str);
    }
	
	public function getPurOrdDetDelivery()//Fetch Data From V_PURORDDET_DELIVERY table
    {
		$query_str="SELECT count(*) from V_PURORDDET_DELIVERY";
		return Model::oracleQuery($query_str);
    }
	
	
	public function getPurRtMain()//Fetch Data From V_PURRTMAIN table
    {
		$query_str="SELECT count(*) from V_PURRTMAIN";
		return Model::oracleQuery($query_str);
    }
	
	public function getPurRtChg()//Fetch Data From V_PURRTCHG table
    {
		$query_str="SELECT count(*) from V_PURRTCHG";
		return Model::oracleQuery($query_str);
    }
	
	public function getPurRtChgItem ()//Fetch Data From V_PURRTCHG_ITEM  table
    {
		$query_str="SELECT count(*) from V_PURRTCHG_ITEM ";
		return Model::oracleQuery($query_str);
    }



}
