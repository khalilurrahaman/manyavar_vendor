<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Carbon\Carbon;

class VendorDashboardController extends Controller
{
    public function vendorDashboard()
    {
    	$user = Auth::user();
    	/*$date1 = Carbon::now()->addWeek(2)->format('Y-m-d');
    	dd($date1);*/
        $query_str="purchase_order.PCODE = '".$user->SLCODE."' AND  (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) > 0 ";

    	$newPOList = DB::table('purchase_order')->select('purchase_order.id as poId','purchase_order.ORDCODE as ordCode', 'purchase_order.ORDNO as odrNo', 'purchase_order.PONO as PONO', 'purchase_order.ORDDT as l_time', 'purchase_order.GRSAMT', 'purchase_order.CHGAMT', 'purchase_order.NETAMT','v_purordchg.ISTAX','v_purordchg.RATE as disc_rate','V_Fincharge.CHGNAME', DB::raw('sum(V_PURORDDET.RATE * V_PURORDDET.ORDQTY) as total'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')						
		->leftJoin('v_purordchg', 'purchase_order.ORDCODE', '=', 'v_purordchg.ORDCODE')
		->leftJoin('V_Fincharge', 'v_purordchg.CHGCODE', '=', 'V_Fincharge.CHGCODE')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
        ->where('purchase_order.status', '=','NEW')
        ->groupBy('purchase_order.ORDCODE')
        ->orderBy('purchase_order.TIME', 'desc')->get();		
		#dd($newPOList );

        $totnewPo = DB::table('purchase_order')->select('purchase_order.id as poId','purchase_order.ORDCODE as ordCode', 'purchase_order.ORDNO as odrNo', 'purchase_order.PONO as PONO', 'purchase_order.TIME as l_time', DB::raw('sum(V_PURORDDET.RATE * V_PURORDDET.ORDQTY) as total'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
        ->where('purchase_order.status', '=','NEW')
        ->groupBy('purchase_order.ORDCODE')
        ->orderBy('purchase_order.TIME', 'desc')->get();

        $query_str_due_po=" purchase_order.status = 'OPEN' AND V_PURORDDET.STATUS = 'OPEN' AND purchase_order.PCODE = '".Auth::user()->SLCODE."' AND  (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) > 0 ";
        $bet2="'".Carbon::now()->format('Y-m-d')."' AND '".Carbon::now()->addDay(15)->format('Y-m-d')."'";
            $query_str_due_po .=" AND purchase_order_delivery.DUEDATE BETWEEN ". $bet2;

        $totalpotobeexpired = DB::table('purchase_order')->select(['purchase_order.ORDNO', 'purchase_order.PONO as PONO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.ICODE','purchase_order_delivery.DUEDATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY'])
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('purchase_order_delivery', 'purchase_order_delivery.PURORDDET_CODE', '=', 'V_PURORDDET.CODE')  
        ->where('purchase_order.status', '=','OPEN')
        ->where('V_PURORDDET.status', '=','OPEN')
        ->whereRaw($query_str_due_po)
        ->orderBy('purchase_order_delivery.DUEDATE', 'asc')->get();    

        $acceptedPOList = DB::table('purchase_order')->select(['purchase_order.ORDNO', 'purchase_order.PONO as PONO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.ICODE','purchase_order_delivery.DUEDATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY'])
    	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('purchase_order_delivery', 'purchase_order_delivery.PURORDDET_CODE', '=', 'V_PURORDDET.CODE')  
    	->where('purchase_order.status', '=','OPEN')
        ->where('V_PURORDDET.status', '=','OPEN')
    	->whereRaw($query_str_due_po)
    	->orderBy('purchase_order_delivery.DUEDATE', 'asc')->get();
    	//dd($totalpotobeexpired);

         if(!empty($acceptedPOList) && count($acceptedPOList)>0)
        {
            foreach($acceptedPOList as $reqst)
            {
                $actiondtls =DB::table('po_action')->select('action_type','status','ICODE')->where('po_no', '=',$reqst->PONO)->where('action_type', '=','date_ext')->whereIn('status',array('new', 'inprocess'))->orderBy('created_at','desc')->get();
                if(!empty($actiondtls) && count($actiondtls)>0)
                    $reqst->actiondtls=$actiondtls;
            }
        }

        $data['rfq_list'] = DB::table('rfqs')   
        ->join('products','rfqs.product_id', '=', 'products.id')
        ->join('rfq_details', 'rfqs.id', '=', 'rfq_details.rfq_id')
        ->selectRaw('rfqs.id,rfq_details.id as details_id,rfqs.req_id,rfq_details.shipment_provider,rfq_details.docket_no,rfqs.product_id,products.width_size_required,products.cat1,products.cat2,products.cat3,products.cat4,products.image')        
        ->whereRaw('rfqs.status=1 AND rfq_details.vendor_id = '.$user->id.' AND rfq_details.id NOT IN (select rfq_details_id from rfq_quetes group by rfq_details_id)')
        ->orderBy('rfqs.created_at','desc')
        ->take(5)->get();

		$where='notification.status="NEW" AND notification.receiver="'.Auth::user()->id.'" AND DATE(notification.created_at) > (NOW() - INTERVAL 7 DAY)';
		$notificationList = DB::table('notification')->select('notification.*')
        ->whereRaw($where)
        ->orderBy('notification.created_at', 'desc')->take(5)->get();
        $total = DB::table('notification')->selectRaw('count(*) as num')
        ->whereRaw($where)
        ->first();        
        if(count($total)>0)
            $notificationList->tot=$total->num;
        //dd($notificationList);


        $data['POList']=$newPOList;
        $data['acceptedPOList']=$acceptedPOList;
        $data['totnewPo']=$totnewPo;
        $data['notificationList']=$notificationList;
        $data['totalpotobeexpired']=$totalpotobeexpired;
		//dd($data);
        return view('vendor.dashboard',$data);
    }

    public function updatePoStatus(Request $request)
    {
        //dd($request->id);
       if(DB::table('purchase_order')->where('purchase_order.id', '=',$request->id)->update(['status'=>'OPEN', 'updated_at'=>date('Y-m-d H:i:s')]))
	   {
		   		/*$notify_array['message'] = 'New PO has been Accepted';
				$notify_array['type'] = 'NewPOAccept';
                $notify_array['red_url'] = 'purchase-order-list';
				$notify_array['sender'] = Auth::user()->id;
				$notify_array['receiver'] = 1;
				$notify_array['status'] = 'NEW';
				$notify_array['table_name'] = 'purchase_order';
				$notify_array['primary_key'] = 'id';
				$notify_array['primary_key_val'] =$request->id;
				$notify_array['created_at'] =date('Y-m-d H:i:s');	
				DB::table('notification')->insert($notify_array);*/
	   }

        echo 1; exit;
    }

    public function rejectPoStatus(Request $request)
    {
 	  if(DB::table('purchase_order')->where('purchase_order.id', '=',$request->id)->update(['status'=>'REJECT','remarks'=>$request->remarks,'updated_at'=>date('Y-m-d H:i:s')]))
	   {
            $ORDCODE = DB::table('purchase_order')->select('ORDCODE')->where('purchase_order.id', '=',$request->id)->first();
            DB::table('V_PURORDDET')->where('V_PURORDDET.ORDCODE','=',$ORDCODE->ORDCODE)->update(['STATUS'=>'REJECT','updated_at'=>date('Y-m-d H:i:s')]);
            
	   		/*$notify_array['message'] = 'New PO has been Rejected';
			$notify_array['type'] = 'NewPOReject';
            $notify_array['red_url'] = 'reject-purchase-order-list';
			$notify_array['sender'] = Auth::user()->id;
			$notify_array['receiver'] = 1;
			$notify_array['status'] = 'NEW';
			$notify_array['table_name'] = 'purchase_order';
			$notify_array['primary_key'] = 'id';
			$notify_array['primary_key_val'] =$request->id;
			$notify_array['created_at'] =date('Y-m-d H:i:s');	
			DB::table('notification')->insert($notify_array);*/
	   }
        echo 1; exit;
    }

    public function viewPo(Request $request)
    {   
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.id', 'purchase_order.ORDNO', 'purchase_order.PONO as PONO', 'purchase_order.ORDDT','purchase_order.TIME', 'products.cat1', 'products.cat2', 'products.cat3', 'products.cat4',  'products.CNAME6','V_PURORDDET.RATE', 'V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY','v_purordchg.ISTAX','v_purordchg.RATE as disc_rate','V_Fincharge.CHGNAME', 'purchase_order.GRSAMT', 'purchase_order.CHGAMT', 'purchase_order.NETAMT', 'purchase_order_delivery.DUEDATE')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')								
		->leftJoin('v_purordchg', 'purchase_order.ORDCODE', '=', 'v_purordchg.ORDCODE')
		->leftJoin('V_Fincharge', 'v_purordchg.CHGCODE', '=', 'V_Fincharge.CHGCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->leftJoin('purchase_order_delivery', 'V_PURORDDET.CODE', '=', 'purchase_order_delivery.PURORDDET_CODE')
        ->where('purchase_order.id', '=',$request->id)
        ->orderBy('purchase_order.ORDDT', 'desc')
        ->get();
        return $purchaseOrderList;
		
    }

   public function allnewPO()
    {
        
        $user = Auth::user();
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.id as poId','purchase_order.ORDNO', 'purchase_order.PONO as PONO', 'purchase_order.ORDDT','purchase_order.TIME','purchase_order_delivery.DUEDATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_Fincharge.CHGNAME', 'purchase_order.GRSAMT', 'purchase_order.CHGAMT', 'purchase_order.NETAMT','V_PURORDDET.CNLQTY', DB::raw('sum(V_PURORDDET.RATE * V_PURORDDET.ORDQTY) as total'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('v_purordchg', 'purchase_order.ORDCODE', '=', 'v_purordchg.ORDCODE')
        ->leftJoin('V_Fincharge', 'v_purordchg.CHGCODE', '=', 'V_Fincharge.CHGCODE')
        ->leftJoin('purchase_order_delivery', 'purchase_order_delivery.PURORDDET_CODE', '=', 'V_PURORDDET.CODE')
        ->where('purchase_order.status', '=','NEW')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
        ->groupBy('purchase_order.ORDCODE')
        ->orderBy('purchase_order_delivery.DUEDATE', 'asc')
        ->get();

        //dd($purchaseOrderList);
        return view('vendor.vendor-new-po')->with('purchaseOrderList', $purchaseOrderList);
    }
 
 
 public function selectallPoStatus(Request $request)
    {

        $var=$request->selected;
        //dd($var);
        foreach($var as $key=>$val){

        if(DB::table('purchase_order')->where('purchase_order.id', '=',$val)->update(['status'=>'OPEN','updated_at'=>date('Y-m-d H:i:s')]))
		{
				/*$notify_array['message'] = 'New PO has been Accepted';
				$notify_array['type'] = 'NewPOAccept';
				$notify_array['sender'] = Auth::user()->id;
				$notify_array['receiver'] = 1;
				$notify_array['status'] = 'NEW';
				$notify_array['table_name'] = 'purchase_order';
				$notify_array['primary_key'] = 'id';
				$notify_array['primary_key_val'] =$val;
				$notify_array['created_at'] =date('Y-m-d H:i:s');	
				DB::table('notification')->insert($notify_array);*/
		}
		else echo 0;

        
        }
        echo 1; exit;
    }
	
	public function allNotification()
    {
        $where='notification.receiver="'.Auth::user()->id.'"';
		$notificationList = DB::table('notification')->select('notification.*')
        ->whereRaw($where)       
        ->orderBy('notification.created_at', 'desc')
        ->get();

        $unread = DB::table('notification')->selectRaw('count(*) as num')
        ->whereRaw($where)
        ->where('notification.status', '=', 'NEW')
        ->first();

        if(count($unread)>0)
            $notificationList->unread= $unread->num;
		//dd($notificationList);
		
        return view('vendor.all-notification')->with('notificationList',$notificationList);
    }
	
	public function readNotification(Request $request)
    {
		$return_val=0;
		if(DB::table('notification')->where('notification.id', '=',$request->id)->update(['status'=>'READ']))
		{
			$return_val=1;
		}
		echo $return_val;

    }
}

