<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Product;
use App\Models\GoodsReturns;
use DB;
use Auth;
use Redirect;


class GoodsReturnController extends Controller
{
    public function viewList()
    {   
		if($this->userAccessibilityCheck('goods-return'))
			return view('admin.googs-return-list');
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);		              
        
    }
    public function closedReturn()
    {  
	
		if($this->userAccessibilityCheck('goods-return-close'))
			return view('admin.goods-return-closed');
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);		           
        
    }

    public function viewAddform()
    {
		if($this->userAccessibilityCheck('add_return'))
		{
			$vendor_list = User::select('id','name')->where('type','=','vendor')->where('status', '=', 1)->get();
			$product_list = Product::select('id','image','cat1','cat2','cat3','cat4')->where('cat1', '<>','')->inRandomOrder()->limit(10)->get();
			$cat1_list = Product::select('id','cat1')->where('cat1', '<>','')->groupBy('cat1')->get();
			$cat2_list = Product::select('id','cat2')->where('cat2', '<>','')->groupBy('cat2')->get();
			$cat3_list = Product::select('id','cat3')->where('cat3', '<>','')->groupBy('cat3')->get();
			$cat4_list = Product::select('id','cat4')->where('cat4', '<>','')->groupBy('cat4')->get();
			
			
			$cat1_list1 = Product::where('cat1', '<>','')->groupBy('cat1')->pluck('cat1');
			$cat2_list2 = Product::where('cat2', '<>','')->groupBy('cat2')->pluck('cat2');
			return view('admin.googs-return-add')->with('allVendor', $vendor_list)->with('allProduct', $product_list)->with('cat1_list', $cat1_list)->with('cat2_list', $cat2_list)->with('cat3_list', $cat3_list)->with('cat4_list', $cat4_list)->with('cat1_list1', $cat1_list1)->with('cat2_list2', $cat2_list2);
		}
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);		 
       
    }

    public function postAddreturn(Request $request)
    {
        $user = Auth::user();

        $rfq_return = new GoodsReturns;
        //$rfq_return->product_id = trim($request->product_id);
        //echo $request->product_id;exit;
        $rfq_return->vendor_id = trim($request->vendor_id);
        $rfq_return->docket_no = trim($request->docket_no);
        $rfq_return->prn_no = trim($request->prn_no);
        if(empty($request->prn_date))
        {
            $rfq_return->prn_date='0000-00-00';
        }
        else
        {
          $rfq_return->prn_date =date('Y-m-d', strtotime(str_replace('/', '-', $request->prn_date)));
        }
        $rfq_return->courier_name = trim($request->courier_name);
        $rfq_return->comments = trim($request->comment);
        $rfq_return->image = trim($request->image);
        $rfq_return->category1 = trim($request->cat_1);
        $rfq_return->category2 = trim($request->cat_2);
        $rfq_return->reason = trim($request->remarks);
        $rfq_return->return_quantity = trim($request->quantity);
        
        $rfq_return->createdby = $user->id;
        $rfq_return->modifiedby = $user->id;
        $rfq_return->status = 1;
        $rfq_return->created_at =date('Y-m-d H:i:s');
        $rfq_return->updated_at =date('Y-m-d H:i:s');
        if($rfq_return->save())
        {
                    
            $notify_array['message'] = 'Defective Fabric:'.$rfq_return->category1.'-'.$rfq_return->category2.' has been Returned';
            $notify_array['type'] = 'DefectiveFabric';
            $notify_array['red_url'] = 'goods-receive';
            $notify_array['sender'] = Auth::user()->id;
            $notify_array['receiver'] = $rfq_return->vendor_id;
            $notify_array['status'] = 'NEW';
            $notify_array['table_name'] = 'goods_returns';
            $notify_array['primary_key'] = 'id';
            $notify_array['primary_key_val'] =$rfq_return->id;
            $notify_array['created_at'] =date('Y-m-d H:i:s');   
            DB::table('notification')->insert($notify_array);
                    
            echo "s";
        }
        else
        {
            echo "ds";
        }



        return Redirect::route('goods-returns-list')->withErrors(['Goods has been added successfully for return']);
    }

    public function goodsReceiveList()
    {
        
        return view('vendor.goods-received-list');
    }

     public function goodsVendorClosedList()
    {
       
        return view('vendor.goods-closed-list');
    }

    

    public function goodsReceiveAccept(Request $request)
    {
      if(GoodsReturns::where('id','=', $request->returntId)->update(['updated_at' =>date('Y-m-d H:i:s'),'status' =>0,'vendor_comments'=>trim($request->vendor_comment)]))
        {
            $notify_array['message'] = 'Defective Fabric:'.$request->design.' has been accepted';
            $notify_array['type'] = 'DefectiveFabric';
            $notify_array['red_url'] = 'closed-return';
            $notify_array['sender'] = Auth::user()->id;
            $notify_array['receiver'] = 1;
            $notify_array['status'] = 'NEW';
            $notify_array['table_name'] = 'goods_returns';
            $notify_array['primary_key'] = 'id';
            $notify_array['primary_key_val'] =$request->returntId;
            $notify_array['created_at'] =date('Y-m-d H:i:s');   
            DB::table('notification')->insert($notify_array);
        }
        return Redirect::route('goods-receive-list')->withErrors(['Return request has been accepted successfully']);
    }
    public function addGoodsFile(Request $request)
    {
         $file = $request->file('file');

        $imageName = time().'_'.$request->file('file')->getClientOriginalName();
        if($file->move(public_path('upload/temp'), $imageName)){
            $data['file_name']=$imageName;
            return $data;
        }
    }
    public function filterGoodsReturn(Request $request)
    {
        //dd(11);
        $cat1=$request->cat1;
        $cat2=$request->cat2;
        $name=$request->name;
        
        $query_str=" goods_returns.status = '1' ";
        
        if(!empty($cat1))
            $query_str .=" AND goods_returns.category1 = '".$cat1."' ";
        if(!empty($cat2))
            $query_str .=" AND goods_returns.category2 = '".$cat2."' ";
        if(!empty($name))
            $query_str .=" AND goods_returns.vendor_id = '".$name."' ";
        
     
                
                
        $return_list = DB::table('goods_returns')
            
            ->join('users', 'users.id', '=', 'goods_returns.vendor_id')->whereRaw($query_str)
            ->orderBy('goods_returns.updated_at','desc')
            ->select('goods_returns.id','goods_returns.category1','goods_returns.category2','goods_returns.comments','goods_returns.return_quantity','goods_returns.product_id','goods_returns.docket_no','goods_returns.prn_no','goods_returns.prn_date','goods_returns.courier_name','goods_returns.vendor_comments','goods_returns.status','users.name','goods_returns.image','goods_returns.reason')->get();

        if(!empty($return_list) && count($return_list)>0)
        {
            foreach($return_list as $recv)
            {
                $good_comments =DB::table('goods_comments')->where('goods_id', '=',$recv->id)->orderBy('created_at','desc')->get();
                        if(!empty($good_comments) && count($good_comments)>0)
                        $recv->good_comments=$good_comments;
            }
        }    

        //dd($return_list);
        return view('admin.goods-return-filter')->with('returnList',$return_list);
    }
    public function filterGoodsReturnClosed(Request $request)
    {
        //dd(11);
        $cat1=$request->cat1;
        $cat2=$request->cat2;
        $name=$request->name;
        
        $query_str=" goods_returns.status = '0' ";
        
        if(!empty($cat1))
            $query_str .=" AND goods_returns.category1 = '".$cat1."' ";
        if(!empty($cat2))
            $query_str .=" AND goods_returns.category2 = '".$cat2."' ";
        if(!empty($name))
            $query_str .=" AND goods_returns.vendor_id = '".$name."' ";
        
     
                
                
        $return_list = DB::table('goods_returns')
            
            ->join('users', 'users.id', '=', 'goods_returns.vendor_id')->whereRaw($query_str)
            ->orderBy('goods_returns.updated_at','desc')
            ->select('goods_returns.id','goods_returns.category1','goods_returns.category2','goods_returns.comments','goods_returns.return_quantity','goods_returns.product_id','goods_returns.docket_no','goods_returns.prn_no','goods_returns.prn_date','goods_returns.courier_name','goods_returns.vendor_comments','goods_returns.status','users.name','goods_returns.image','goods_returns.reason')->get();

        if(!empty($return_list) && count($return_list)>0)
        {
            foreach($return_list as $recv)
            {
                $good_comments =DB::table('goods_comments')->where('goods_id', '=',$recv->id)->orderBy('created_at','desc')->get();
                        if(!empty($good_comments) && count($good_comments)>0)
                        $recv->good_comments=$good_comments;
                }
            }    


        return view('admin.goods-return-closed-filter')->with('returnList',$return_list);
    }
    public function filterVendorGoodsReturn(Request $request)
    {
        //dd(11);
        $user = Auth::user();
        $cat1=$request->cat1;
        $cat2=$request->cat2;
        $name=$user->id;
        
        $query_str=" goods_returns.vendor_id = '".$name."' ";
        
        if(!empty($cat1))
            $query_str .=" AND goods_returns.category1 = '".$cat1."' ";
        if(!empty($cat2))
            $query_str .=" AND goods_returns.category2 = '".$cat2."' ";
        
        
     
                
                
        $return_list = DB::table('goods_returns')
            
            ->join('users', 'users.id', '=', 'goods_returns.vendor_id')->whereRaw($query_str)
            ->orderBy('goods_returns.updated_at','desc')->where('goods_returns.status', '=', 1)
            ->select('goods_returns.id','goods_returns.category1','goods_returns.category2','goods_returns.comments','goods_returns.return_quantity','goods_returns.product_id','goods_returns.docket_no','goods_returns.prn_no','goods_returns.prn_date','goods_returns.courier_name','goods_returns.vendor_comments','goods_returns.status','users.name','goods_returns.image','goods_returns.reason')->paginate(20);

        if(!empty($return_list) && count($return_list)>0)
        {
            foreach($return_list as $recv)
            {
                $good_comments =DB::table('goods_comments')->where('goods_id', '=',$recv->id)->orderBy('created_at','desc')->get();
                        if(!empty($good_comments) && count($good_comments)>0)
                        $recv->good_comments=$good_comments;
                }
            }
                
    return view('vendor.goods-return-filter')->with('receiveList',$return_list);
    }
    public function filterVendorclosedReturn(Request $request)
    {
        //dd(11);
        $user = Auth::user();
        $cat1=$request->cat1;
        $cat2=$request->cat2;
        $name=$user->id;
        
        $query_str=" goods_returns.vendor_id = '".$name."' ";
        
        if(!empty($cat1))
            $query_str .=" AND goods_returns.category1 = '".$cat1."' ";
        if(!empty($cat2))
            $query_str .=" AND goods_returns.category2 = '".$cat2."' ";
        
        
     
                
                
        $return_list = DB::table('goods_returns')
            
            ->join('users', 'users.id', '=', 'goods_returns.vendor_id')->whereRaw($query_str)
            ->orderBy('goods_returns.updated_at','desc')->where('goods_returns.status', '=', 0)
            ->select('goods_returns.id','goods_returns.category1','goods_returns.category2','goods_returns.comments','goods_returns.return_quantity','goods_returns.product_id','goods_returns.docket_no','goods_returns.prn_no','goods_returns.prn_date','goods_returns.courier_name','goods_returns.vendor_comments','goods_returns.status','users.name','goods_returns.image','goods_returns.reason')->paginate(20);

        if(!empty($return_list) && count($return_list)>0)
        {
            foreach($return_list as $recv)
            {
                $good_comments =DB::table('goods_comments')->where('goods_id', '=',$recv->id)->orderBy('created_at','desc')->get();
                        if(!empty($good_comments) && count($good_comments)>0)
                        $recv->good_comments=$good_comments;
                }
            }
                
    return view('vendor.goods-return-closed-filter')->with('receiveList',$return_list);
    }
    public function goodsComments($id)
    {
       $goods= DB::table('goods_returns')
            
            ->join('users', 'users.id', '=', 'goods_returns.vendor_id')->where('goods_returns.status','=',1)->where('goods_returns.id','=',$id)
            ->orderBy('goods_returns.updated_at','desc')
            ->select('goods_returns.id','goods_returns.category1','goods_returns.category2','goods_returns.comments','goods_returns.return_quantity','goods_returns.product_id','goods_returns.docket_no','goods_returns.prn_no','goods_returns.prn_date','goods_returns.courier_name','goods_returns.vendor_comments','goods_returns.status','users.name','goods_returns.image','goods_returns.reason')->first();
        $Comments = DB::table('goods_comments')
                  ->select('*')
                  ->where('goods_id', '=', $id)->orderBy('created_at')->get();
        $Comments = DB::table('goods_comments')
                  ->select('*')
                  ->where('goods_id', '=', $id)->orderBy('created_at')->get();
        //dd($Comments);
        
        return view('vendor.goods-comments')->with('comments_list',$Comments)->with('action_id', $id)->with('goods_id',$id)->with('goods',$goods);
       
    }
    public function addGoodsComments(Request $request)
    {
        $return=0;
        
        $action['goods_id'] = $request->goods_id;
        $action['user_id'] = Auth::user()->id;   
        $action['comments'] = $request->comments;
        $action['created_at'] = date('Y-m-d H:i:s');
        $action['updated_at'] = date('Y-m-d H:i:s');
                
        if(DB::table('goods_comments')->insert($action))
        {
            $return=1;
        }
        echo $return;
    }

    public function adminGoodsComments($id)
    {
       $goods= DB::table('goods_returns')
            
            ->join('users', 'users.id', '=', 'goods_returns.vendor_id')->where('goods_returns.status','=',1)->where('goods_returns.id','=',$id)
            ->orderBy('goods_returns.updated_at','desc')
            ->select('goods_returns.id','goods_returns.category1','goods_returns.category2','goods_returns.comments','goods_returns.return_quantity','goods_returns.product_id','goods_returns.docket_no','goods_returns.prn_no','goods_returns.prn_date','goods_returns.courier_name','goods_returns.vendor_comments','goods_returns.status','users.name','goods_returns.image','goods_returns.reason')->first();
        $Comments = DB::table('goods_comments')
                  ->select('*')
                  ->where('goods_id', '=', $id)->orderBy('created_at')->get();
        //dd($Comments);
        
        return view('admin.admingoods-comments')->with('comments_list',$Comments)->with('action_id', $id)->with('goods_id',$id)->with('goods',$goods);
       
    }
    public function addAdminGoodsComments(Request $request)
    {
        $return=0;
        
        $action['goods_id'] = $request->goods_id;
        $action['user_id'] = Auth::user()->id;   
        $action['comments'] = $request->comments;
        $action['created_at'] = date('Y-m-d H:i:s');
        $action['updated_at'] = date('Y-m-d H:i:s');
                
        if(DB::table('goods_comments')->insert($action))
        {
            $return=1;
        }
        echo $return;
    }
    public function getVendor(Request $request)
    {
        $status=$request->value;

        $vendor_name_list= DB::table('goods_returns')   
        
        ->join('users', 'goods_returns.vendor_id', '=', 'users.id') 
        ->select('users.id','users.name')        
        ->where('goods_returns.status','=',$status)
        ->groupBy('users.id') 
        ->orderBy('users.id','desc')
        ->get();
        $vendor_html="<option value=''>Select Vendor</option>";
        foreach ($vendor_name_list as $vendor_html_value)
        {
            $vendor_html=$vendor_html.'<option value="'.$vendor_html_value->id.'">'.$vendor_html_value->name.'</option>';
        }
        $data['vendor']=$vendor_html;
        return $data;
    }
    public function getCat1(Request $request)
    {
        $status=$request->value;

        $cat1_list = DB::table('goods_returns')   
        ->select('goods_returns.category1')        
        ->where('goods_returns.status','=',$status)
        ->where('goods_returns.category1','!=','')
        ->groupBy('goods_returns.category1') 
        ->orderBy('goods_returns.category1','desc')
        ->get();

        $cat1_html="<option value=''>Select Design</option>";
        foreach ($cat1_list as $cat1_html_value)
        {
            $cat1_html=$cat1_html.'<option value="'.$cat1_html_value->category1.'">'.$cat1_html_value->category1.'</option>';
        }
        $data['cat1']=$cat1_html;
        return $data;
    }
    public function getCat2(Request $request)
    {
        $status=$request->value;
        $cat2_list = DB::table('goods_returns')   
        ->select('goods_returns.category2')        
        ->where('goods_returns.status','=',$status)
        ->where('goods_returns.category2','!=','')
        ->groupBy('goods_returns.category2') 
        ->orderBy('goods_returns.category2','desc')
        ->get();

        $cat2_html='<option value="">Select Colour</option>';
        foreach ($cat2_list as $cat2_html_value)
        {
            $cat2_html=$cat2_html.'<option value="'.$cat2_html_value->category2.'">'.$cat2_html_value->category2.'</option>';
        }
        $data['cat2']=$cat2_html;
        return $data;
    }
    public function getVendorCat1(Request $request)
    {
        $user = Auth::user();
        $status=$request->value;

        $cat1_list = DB::table('goods_returns')   
        ->select('goods_returns.category1')        
        ->where('goods_returns.status','=',$status)
        ->where('goods_returns.category1','!=','')
        ->where('goods_returns.vendor_id','=',$user->id)
        ->groupBy('goods_returns.category1') 
        ->orderBy('goods_returns.category1','desc')
        ->get();

        $cat1_html="<option value=''>Select Design</option>";
        foreach ($cat1_list as $cat1_html_value)
        {
            $cat1_html=$cat1_html.'<option value="'.$cat1_html_value->category1.'">'.$cat1_html_value->category1.'</option>';
        }
        $data['cat1']=$cat1_html;
        return $data;
    }
    public function getVendorCat2(Request $request)
    {
        $user = Auth::user();
        $status=$request->value;
        $cat2_list = DB::table('goods_returns')   
        ->select('goods_returns.category2')        
        ->where('goods_returns.status','=',$status)
        ->where('goods_returns.category2','!=','')
        ->where('goods_returns.vendor_id','=',$user->id)
        ->groupBy('goods_returns.category2') 
        ->orderBy('goods_returns.category2','desc')
        ->get();

        $cat2_html='<option value="">Select Colour</option>';
        foreach ($cat2_list as $cat2_html_value)
        {
            $cat2_html=$cat2_html.'<option value="'.$cat2_html_value->category2.'">'.$cat2_html_value->category2.'</option>';
        }
        $data['cat2']=$cat2_html;
        return $data;
    }
}