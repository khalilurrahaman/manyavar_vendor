<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Models\Product;
use DB;
use Auth;
use Redirect;
use Image;
use URL;


class ItemMasterController extends Controller
{
    public function index()
    {
		if($this->userAccessibilityCheck('item-master-list'))
		{
			$data=Product::orderBy('id','DESC')->where('product_type','=','Live')->get();
            $cat1_list = Product::where('cat1', '<>','')->where('product_type','=','Live')->groupBy('cat1')->pluck('cat1');
        	return view('admin.item-master-list')->with('data',$data)->with('cat1_list', $cat1_list);
		}
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);
        
    }
	
	
	public function sample()
    {
		if($this->userAccessibilityCheck('sample-item-list'))
		{
            $live_product ='';
			//$live_product = Product::select('id','image','width_size_required','cat1','cat2','cat3','cat4')->where('product_type','=','Live')->inRandomOrder()->get();
			$data=Product::orderBy('id','DESC')->where('product_type','=','Sample')->paginate(30);
            $cat1_list = Product::where('cat1', '<>','')->where('product_type','=','Sample')->groupBy('cat1')->pluck('cat1');
			return view('admin.item-master-sample-list')->with('data',$data)->with('live_product',$live_product)->with('cat1_list', $cat1_list);
		}
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);
		
    }
	
	public function searchItemSample(Request $request)
    {
		
		$query_str=" product_type = 'Sample' ";
    	if(!empty($request->selectcat1))
    		$query_str .=" AND cat1 = '".$request->selectcat1."' ";
    	if(!empty($request->selectcat2))
    		$query_str .=" AND cat2 = '".$request->selectcat2."' ";
		if(!empty($request->selectcat3))
    		$query_str .=" AND cat3 = '".$request->selectcat3."' ";
		if(!empty($request->selectcat4))
    		$query_str .=" AND cat4 = '".$request->selectcat4."' ";
			
        $data=Product::orderBy('id','DESC')->whereRaw($query_str)->get();
        return view('admin.item-master-sample-list-filter')->with('data',$data);
    }
    
	
	public function searchItemLive(Request $request)
    {
		
		$query_str=" product_type = 'Live' ";
    	if(!empty($request->selectcat1))
    		$query_str .=" AND cat1 = '".$request->selectcat1."' ";
    	if(!empty($request->selectcat2))
    		$query_str .=" AND cat2 = '".$request->selectcat2."' ";
		if(!empty($request->selectcat3))
    		$query_str .=" AND cat3 = '".$request->selectcat3."' ";
		if(!empty($request->selectcat4))
    		$query_str .=" AND cat4 = '".$request->selectcat4."' ";
			
        $data=Product::orderBy('id','DESC')->whereRaw($query_str)->get();
        return view('admin.item-master-live-list-filter')->with('data',$data);
    }
	
	public function getOptionFilterSample(Request $request)
    {
		
		$data['cat2_list'] = Product::select('cat2')->distinct('cat2')->where('product_type','=','Sample')->where('cat1', '=', $request->search_str)->get();
		$data['cat3_list'] = Product::select('cat3')->distinct('cat3')->where('product_type','=','Sample')->where('cat1', '=', $request->search_str)->get();
		$data['cat4_list'] = Product::select('cat4')->distinct('cat4')->where('product_type','=','Sample')->where('cat1', '=', $request->search_str)->get();
		
        $html = view('admin.get-option-filter-sample',$data);  
		return  $html;
    }
	
	public function getOptionFilter(Request $request)
    {
		
		$data['cat2_list'] = Product::select('cat2')->distinct('cat2')->where('product_type','=','Live')->where('cat1', '=', $request->search_str)->get();
		$data['cat3_list'] = Product::select('cat3')->distinct('cat3')->where('product_type','=','Live')->where('cat1', '=', $request->search_str)->get();
		$data['cat4_list'] = Product::select('cat4')->distinct('cat4')->where('product_type','=','Live')->where('cat1', '=', $request->search_str)->get();
		
        $html = view('admin.get-option-filter-live',$data);  
		return  $html;
    }
	public function getOptionFilterList(Request $request)
    {
		
		$data['cat2_list'] = Product::select('cat2')->distinct('cat2')->where('product_type','=','Live')->where('cat1', '=', $request->search_str)->get();
		$data['cat3_list'] = Product::select('cat3')->distinct('cat3')->where('product_type','=','Live')->where('cat1', '=', $request->search_str)->get();
		$data['cat4_list'] = Product::select('cat4')->distinct('cat4')->where('product_type','=','Live')->where('cat1', '=', $request->search_str)->get();
		
        $html = view('admin.get-option-filter-live-list',$data);  
		return  $html;
    }
    public function autocompleteVendor()
    {
        $term = $_REQUEST['term'];
        
        $results = array();
        
        $queries = \DB::table('users')
          ->where('name', 'LIKE', '%'.$term.'%')
          ->where('status','=','1')
          ->get();
        
        foreach ($queries as $query)
        {
            $results[] = [ 'id' => $query->id, 'value' => ucfirst($query->name) ];
        }
        return \Response::json($results);
    }
    public function autocompleteDesign()
    {
        $term = $_REQUEST['term'];
        
        $results = array();
        
        $queries = \DB::table('products')
          ->where('cat1', 'LIKE', '%'.$term.'%')->groupBy('cat1')->get();
        
        foreach ($queries as $query)
        {
            $results[] = [ 'id' => $query->id, 'value' => $query->cat1 ];
        }
        return \Response::json($results);
    }
	public function searchItemByKeyword(Request $request)
    {
		if(!empty($request->selectcat1) || !empty($request->selectcat2) || !empty($request->selectcat3) || !empty($request->selectcat4))
		{
					$conds="";
					$i=0;
					if(!empty($request->selectcat1) && $request->selectcat1!='Select Cat1')
					{
						$conds .=" `cat1` LIKE '%".$request->selectcat1."%' ";
						$i++;	
					}
					if(!empty($request->selectcat2) && $request->selectcat2!='Select Cat2')
					{
						if(empty($i))
							$conds .=" `cat2` LIKE '%".$request->selectcat2."%' ";
						else
							$conds .=" AND `cat2` LIKE '%".$request->selectcat2."%' ";
						$i++;	
					}
					if(!empty($request->selectcat3) && $request->selectcat3!='Select Cat3')
					{
						if(empty($i))
							$conds .=" `cat3` LIKE '%".$request->selectcat3."%' ";
						else
							$conds .=" AND `cat3` LIKE '%".$request->selectcat3."%' ";
						$i++;	
					}
					if(!empty($request->selectcat4) && $request->selectcat4!='Select Cat4')
					{
						if(empty($i))
							$conds .=" `cat4` LIKE '%".$request->selectcat4."%' ";
						else
							$conds .=" AND `cat4` LIKE '%".$request->selectcat4."%' ";
						$i++;	
					}
					
					$product_list=Product::whereRaw($conds)->where('product_type','=','Live')->groupBy('cat1')->get();
		}elseif(!empty($request->search_str))
		{
			$product_list = Product::where('cat1', 'like', '%' . $request->search_str . '%')->where('product_type','=','Live')
			->orWhere('cat2', 'like', '%' . $request->search_str . '%')
			->orWhere('cat3', 'like', '%' . $request->search_str . '%')
			->orWhere('cat4', 'like', '%' . $request->search_str . '%')
			->groupBy('cat1')
			->get();
		}
		else
		{
			$product_list = Product::where('product_type','=','Live')->groupBy('cat1')->inRandomOrder()->get();
		}
		
		
		$html='';
		 if(!empty($product_list) && count($product_list)>0){
                                   foreach($product_list as $ext){
                                    $html .='<tr>';
                                    	$html .='<td class="text-center col-xs-2">';
 										
                                            $e_image=URL::to('/')."/img/pre-order-img.jpg";
                                            if(!empty($ext->image) && file_exists(public_path().'/upload/product/'.$ext->image))
                                            {
                                               $e_image= URL::to('/') .'/upload/product/'.$ext->image;
                                            }
                                       
                                        $html .='<img src="'.$e_image.'" title="small image"  width="55" height="55" class="selectProductExistClassImage'.$ext->id.'">';
                                        $html .='</td>';
                                        $html .='<td class="text-center col-xs-2 selectProductExistClassCat1'.$ext->id.'">'.$ext->cat1.'</td>';
                                        $html .='<td class="text-center col-xs-2 selectProductExistClassCat2'.$ext->id.'">'.$ext->cat2.'</td>';
                                        $html .='<td class="text-center col-xs-2 selectProductExistClassCat3'.$ext->id.'">'.$ext->cat3.'</td>';
                                        $html .='<td class="text-center col-xs-2 selectProductExistClassCat4'.$ext->id.'">'.$ext->cat4.'</td>';
                                        $html .='<td class="text-center col-xs-2">';
                                        $html .='<a href="javascript:void(0);" product_id="'.$ext->id.'" class="btn btn-success selectProductExist" style="width:56px; padding:3px 0; font-size:12px;">Select</a>';
                                        $html .='</td>';
                                    $html .='</tr>';
								   }
		 }
                                    else
									{
                                     $html .='<tr>';
                                    	$html .='<td class="text-center col-xs-12" colspan="6">Product Not Available!</td>';
                                    $html .='</tr>';
									}
                                    
    	return $html;
    }
	
	
	public function mergeItem(Request $request)
    {
		$retrn=0;
		$product_id=$request->product_id;
		$merge_with=$request->merge_with;
		
		
		if(!empty($product_id))
		{
				$fetchProduct=Product::where('id','=',$merge_with)->where('product_type','=','Sample')->first();
				if(!empty($fetchProduct) && count($fetchProduct)>0)
				{		
						$update_records['image'] = $fetchProduct->image; 
						if(DB::table('products')->where('id','=', $product_id)->update($update_records))
						{	
                            						
							$update_gal['product_id'] = $product_id; 
                            DB::table('product_gallery')->where('product_id','=', $merge_with)->update($update_gal);
                            // $pGalList=DB::table('product_gallery')->select('*')->where('product_id','=',$merge_with)->get();
                            //  if(!empty($pGalList) && count($pGalList)>0)
                            //  {
                            //   foreach ($pGalList as $pGal) 
                            //   {
                            //     DB::table('product_gallery')->where('id','=', $pGal->id)->update($update_gal);//Product Gallery   
                            //   }

                            //  }
                            
                            $update_rfqs['product_id'] = $product_id; 
							DB::table('rfqs')->where('product_id','=', $merge_with)->update($update_rfqs);												
							if(DB::table('products')->where('id','=', $merge_with)->delete())
							{
								$retrn=1;
							}
								
						}
				}
			
		}
		
		
		return  $retrn;
    }
	
	
    public function addItem(Request $request)
    {
        $user = Auth::user();
        $category1=$request->category1;
        $category2=$request->category2;
        $category3=$request->category3;  
        $category4=$request->category4;
        $this->validate($request,[
            'category1'=>'required',
            'category2'=>'required',
            'image'=>'image'
            ]);
        $itemMaster=new Product;
        $width_size_required=0;
        if(isset($request->width_size_required))
        {
        	 $width_size_required=1;
        }
        $itemMaster->width_size_required=$width_size_required;
        $itemMaster->cat1=$category1;
        $itemMaster->cat2=$category2;
        $itemMaster->cat3=$category3;
        $itemMaster->cat4=$category4;
        $itemMaster->createdby=$user->id;
        $itemMaster->modifiedby=$user->id;
		$itemMaster->product_type='Sample';
        if($request->hasFile('each_image.0'))
        {
            $file_name=time().$request->file('each_image.0')->getClientOriginalName();
            /*Image::make(Input::file('image')->getRealPath())->save(base_path().'/public/upload/product/'.$file_name);*/

            $request->file('each_image.0')->move(base_path() . '/public/upload/product/', $file_name );
            $itemMaster->image=$file_name;

        }
        else
        {
            $itemMaster->image='';
        }
        if($itemMaster->save())
        {
            $last_id=$itemMaster->id;
            foreach ($request->each_image as $key => $value) 
            {
                if($request->hasFile('each_image.'.$key))
                {
                    if($key!=0)
                    {
                        $each_image = time().rand().$request->file('each_image.'.$key)->getClientOriginalName();
                        $request->file('each_image.'.$key)->move(base_path() . '/public/upload/product/', $each_image);
                        //$each_image = $image_name;
                        $p_g['product_image'] = $each_image;
                        $p_g['product_id'] =$last_id;
                        $p_g['created_at'] =date('Y-m-d H:i:s'); 
                        $p_g['updated_at']= date('Y-m-d H:i:s');  
                        DB::table('product_gallery')->insert($p_g);
                    }
                } 
            }
            return Redirect('admin/item-master-sample')->withErrors(["Item Added Successfully"]);
        }
        else
        {
            return Redirect('admin/item-master');
        }
    }
    public function editItem($id)
    {
		if($this->userAccessibilityCheck('sample-item-edit'))
		{
			$data=Product::where('id','=',$id)->first();
			$outreachtypelist=DB::table('product_gallery')->select('*')->where('product_id','=',$id)->orderBy('id','DESC')->get();
			return view('admin.edit-item')->with('data',$data)->with('outreachgal',$outreachtypelist);
		}
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);
        
    }
    public function itemMaster()
    {
		if($this->userAccessibilityCheck('sample-item-add'))
			return view('admin.add-item');
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);
        
    }
    public function updateItem(Request $request)
    {
        $user = Auth::user();

        
        $id=$request->id;
        $category1=$request->category1;
        $category2=$request->category2;
        $category3=$request->category3;
        $category4=$request->category4;

        $this->validate($request,[
            'category1'=>'required',
            'category2'=>'required',
            'image'=>'image'
            ]);
        $itemMaster=Product::where('id','=',$id)->first();
        $width_size_required=0;
        if(isset($request->width_size_required))
        {
        	 $width_size_required=1;
        }
        $itemMaster->width_size_required=$width_size_required;
        $itemMaster->cat1=$category1;
        $itemMaster->cat2=$category2;
        $itemMaster->cat3=$category3;
        $itemMaster->cat4=$category4;
        $itemMaster->createdby=$user->id;
        $itemMaster->modifiedby=$user->id;
        if($request->hasFile('each_image.0'))
        {
            $file_name=time().$request->file('each_image.0')->getClientOriginalName();
            $request->file('each_image.0')->move(base_path() . '/public/upload/product/', $file_name );
            $itemMaster->image=$file_name;

        }
        if($itemMaster->save())
        {
            if(!empty($request->each_image))
            {
            foreach ($request->each_image as $key => $value) 
            {
                if($request->hasFile('each_image.'.$key))
                {
                    if($key!=0)
                    {
                        $each_image = time().rand().$request->file('each_image.'.$key)->getClientOriginalName();
                        $request->file('each_image.'.$key)->move(base_path() . '/public/upload/product/', $each_image);
                        //$each_image = $image_name;
                        $p_g['product_image'] = $each_image;
                        $p_g['product_id'] =$id;
                        $p_g['created_at'] =date('Y-m-d H:i:s'); 
                        $p_g['updated_at']= date('Y-m-d H:i:s');  
                        DB::table('product_gallery')->insert($p_g);
                    }
                } 
            }
        }
            return Redirect('admin/item-master-sample')->withErrors(["Item Updated Successfully"]);
        }
        else
        {
            return Redirect('admin/item-master');
        }
    }

    
    public function deleteProductImage(Request $request)
    {   $tr=0;
        if(DB::table('product_gallery')->where('id','=',$request->p_id)->delete())
        {
            $tr=1;
        }
        echo $tr;

    }
    public function deleteItem($id)
    {
        $data=Product::where('id','=',$id);
        if($data->delete())
        {
            return redirect()->back()->withErrors(["Item Deleted Successfully"]);
        }

    }
  
}
