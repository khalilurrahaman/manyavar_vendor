<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Models\Product;
use App\Models\price;

use DB;
use Auth;
use Redirect;
use Image;


class PriceController extends Controller
{
    //----Price List For Admin----// 
    public function index()
    {
        if($this->userAccessibilityCheck('price-list'))
    	{
            //$data=price::orderBy('request_status','DESC')->paginate(25);
            $data = DB::table('prices')->select('prices.*','products.image','products.cat1','products.cat2','products.cat3','products.cat4','products.CNAME6','users.name')
            ->leftJoin('products', 'prices.ICODE', '=', 'products.ICODE')
            ->leftJoin('users', 'prices.PCODE', '=', 'users.SLCODE')
            ->orderBy('prices.priority', 'DESC')
            ->get();
            // ->paginate(25);
            //dd($data);
            return view('admin.price-list')->with('data',$data);
        }
       else
	       return Redirect::route('dashboard')->withErrors(['You have no access!']);
		    
       
    }
    
	public function editPrice($id)
	    {
	        $data=price::where('id','=',$id)->first();
	        //dd($data);
	        return view('admin.edit-price')->with('data',$data);
	    }
	public function updatePrice(Request $request)
    {
    	//dd($request);
        $id=$request->id;
        $old_rate=$request->old_rate;
        $price=$request->price;


        $priceMaster=price::where('id','=',$id)->first();
        if($price==$request->req_price){
            $priceMaster->RATE=$price;
            $priceMaster->updated_at= date('Y-m-d H:i:s');
            $priceMaster->old_rate=$old_rate;
            $priceMaster->request_status= 2;
            $priceMaster->priority= 3;
        }
        else{
            $priceMaster->negotiate_rate=$price;
            $priceMaster->request_status= 3;
            $priceMaster->priority= 5;

        }
        
        if($priceMaster->save())
        {
            return Redirect::route('price-list')->withErrors(["Price Updated Successfully"]);
        }
        else
        {
            return Redirect::route('price-list');
        }
    } 
   //------For Vendor Price Listing----------// 
    public function vendorPriceList()
    { 
        $user = Auth::user();
        // $data=price::orderBy('request_status','DESC')
        // ->where('PCODE','=',$user->SLCODE)
        // ->paginate(25);
        $data = DB::table('prices')->select('prices.*','products.image','products.cat1','products.cat2','products.cat3','products.cat4','products.CNAME6')
            ->leftJoin('products', 'prices.ICODE', '=', 'products.ICODE')
            ->where('prices.PCODE','=',$user->SLCODE)
            ->orderBy('prices.priority', 'DESC')
            ->paginate(25);

      // dd($data);
        return view('vendor.price-list')->with('data',$data);
    }
    public function searchByKeywordAdmin(Request $request)
    { //dd($request->selectcat1);

        $query_str =" ";
        if(!empty($request->selectcat1))
            $query_str .=" users.name = '".trim($request->selectcat1)."' ";
        if(!empty($request->selectcat2))
        {
            if(!empty($request->selectcat1))
                $query_str .=" AND products.cat1 = '".trim($request->selectcat2)."' ";
            else
                $query_str .=" products.cat1 = '".trim($request->selectcat2)."' ";

        }
            

        

         $data = DB::table('prices')->select('prices.*','products.image','products.cat1','products.cat2','products.cat3','products.cat4','products.CNAME6','users.name')
            ->leftJoin('products', 'prices.ICODE', '=', 'products.ICODE')
            ->leftJoin('users', 'prices.PCODE', '=', 'users.SLCODE')
            ->whereRaw($query_str)
            ->orderBy('prices.priority', 'DESC')
            ->paginate(25);
        //dd($data);    
        return view('admin.price-list-filter')->with('data',$data);
    }

    public function searchByKeywordVendor(Request $request)
    { //dd($request->selectcat);

        $query_str =" ";
        if(!empty($request->selectcat))
            $query_str .=" products.cat1 = '".trim($request->selectcat)."' ";
        

         $data = DB::table('prices')->select('prices.*','products.image','products.cat1','products.cat2','products.cat3','products.cat4','products.CNAME6')
            ->leftJoin('products', 'prices.ICODE', '=', 'products.ICODE')
            ->whereRaw($query_str)
            ->orderBy('prices.priority', 'DESC')
            ->paginate(25);

      // dd($data);
        return view('vendor.price-list-filter')->with('data',$data);
    }

    public function autocompleteDesignVendor()
    {
        $term = $_REQUEST['term'];
        
        $results = array();
        
        $queries = \DB::table('products')
          ->where('cat1', 'LIKE', '%'.$term.'%')->groupBy('cat1')->get();
        
        foreach ($queries as $query)
        {
            $results[] = [ 'id' => $query->id, 'value' => $query->cat1 ];
        }
        return \Response::json($results);
    }
     

    // public function requestforPricechangeToAdmin($id)
    // {
    //     if(DB::table('prices')->where('id','=', $id)->update(['request_status' =>1]))
    //     {
    //     return Redirect::back();
    //     }
    // }


    public function vendorRequestforPricechange($id)
    {
            $data=price::where('id','=',$id)->first();
	        //dd($data);
	        return view('vendor.pricechange-request')->with('data',$data);
    }
    
    public function postRequestforPricechange(Request $request)
    {
       //dd($request);
        $id=$request->id;
        $old_rate=$request->old_rate;
        $reqprice=$request->price;


        $priceMaster=price::where('id','=',$id)->first();
        //$priceMaster->RATE=$price;
        //$priceMaster->updated_at= date('Y-m-d H:i:s');
        $priceMaster->old_rate=$old_rate;
        $priceMaster->requested_rate=$reqprice;
        $priceMaster->request_status= 1;    
        $priceMaster->priority= 4;
        
        if($priceMaster->save())
        {
            return Redirect::route('vendor-price-list')->withErrors(["Request Sent Successfully"]);
        }
        else
        {
            return Redirect::route('vendor-price-list');
        }
    }

    public function acceptNegotiatePrice(Request $request)
    {
        
        $id=$request->p_id;
        $return=0;
       
        
        $quotation['request_status'] = '2';
        $quotation['priority'] = '3';
        $quotation['updated_at']= date('Y-m-d H:i:s'); 
        if(!empty($request->act_val)){
        $quotation['RATE'] = $request->act_val;    
        }       
        if(DB::table('prices')->where('id','=', $id)->update($quotation))
        {
           
            
            return Redirect::back();
        }
        echo $return;
    }


     public function rejectNegotiatePrice($id)
    {
        if(DB::table('prices')->where('id','=', $id)->update(['request_status' =>4,'priority' =>2]))
        {
        return Redirect::back();
        }
    }

     public function rejectPricechangeReq($id)
    {
        if(DB::table('prices')->where('id','=', $id)->update(['request_status' =>4,'priority' =>2]))
        {
        return Redirect::back();
        }
    }

}

