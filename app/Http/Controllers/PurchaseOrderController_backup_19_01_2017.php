<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\purchesOrder;
use Auth;
use Redirect;
use DB;

class PurchaseOrderController extends Controller
{
    public function index()
    {
        
        //$last_three_month = date('Y-m-d',strtotime("-3 month"));
        //dd($last_three_month);
        $purchaseOrderList = DB::table('purchase_order')->select(['purchase_order.ORDNO', 'purchase_order.PONO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE', 'products.ICODE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY','users.name',DB::raw('sum(git.git_qty) as git_qty')])
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
        ->orderBy('purchase_order.ORDDT', 'desc')
        ->groupBy('V_PURORDDET.id')
        ->paginate(30);
        
        //Filter Data
        
        $data['vendore_list']  = DB::table('purchase_order')->select('users.name','users.id')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
        ->orderBy('users.name', 'asc')
        ->groupBy('users.name')
        ->get();
        
        
        $data['po_number_list'] = DB::table('purchase_order')->select('purchase_order.ORDNO','purchase_order.PONO')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->groupBy('purchase_order.ORDNO')
        ->orderBy('purchase_order.ORDDT', 'desc')
        ->get();
        
        $data['cat1_list'] = DB::table('purchase_order')->select('products.cat1')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->groupBy('products.cat1')
        ->orderBy('products.cat1', 'desc')
        ->get();
        
        
        $data['cat2_list'] = DB::table('purchase_order')->select('products.cat2')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->groupBy('products.cat2')
        ->orderBy('products.cat2', 'desc')
        ->get();
        
        //dd($purchaseOrderList);
        return view('admin.purchase-order-list',$data)->with('purchaseOrderList', $purchaseOrderList);
    }

    
     public function filterPurchaseOrder(Request $request)
    {
        
        $po_number=$request->po_number;
        $category1=$request->category1;
        $category2=$request->category2;
        $due_from=$request->due_from;
        $due_to=$request->due_to;
        $vendore_id=$request->vendore_name;
        
        
        $query_str=" purchase_order.status = 'OPEN' ";
        if(!empty($po_number))
            $query_str .=" AND purchase_order.PONO = '".$po_number."' ";
        if(!empty($category1))
            $query_str .=" AND products.cat1 = '".$category1."' ";
        if(!empty($category2))
            $query_str .=" AND products.cat2 = '".$category2."' ";
            
        if(!empty($due_from))
            $query_str .=" AND DATE(purchase_order.ORDDT) >= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_from)))."' ";
        if(!empty($due_to))
            $query_str .=" AND DATE(purchase_order.ORDDT) <= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_to)))."' ";
        
        if(!empty($vendore_id))
            $query_str .=" AND users.id = '".$vendore_id."' ";
            
        $purchaseOrderList = DB::table('purchase_order')->select(['purchase_order.ORDNO', 'purchase_order.PONO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE', 'products.ICODE', 'V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY','users.name',DB::raw('sum(git.git_qty) as git_qty')])
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
        ->where('purchase_order.status', '=','OPEN')
        ->whereRaw($query_str)
        ->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
        ->orderBy('purchase_order.ORDDT', 'desc')
         ->groupBy('V_PURORDDET.id')
        ->paginate(30);
        
    
        return view('admin.purchase-order-list-filter')->with('purchaseOrderList', $purchaseOrderList);
    }

    public function archievePOview()
    {
        
        $query_str=" ((purchase_order.status = 'OPEN') AND (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY<=0)) OR (V_PURORDDET.STATUS ='CLOSED')";
        
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.PONO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE', 'products.ICODE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY', 'users.name',DB::raw('sum(git.git_qty) as git_qty'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
        
        ->whereRaw($query_str)
        ->orderBy('V_PURORDDET.updated_at', 'desc')
        ->groupBy('V_PURORDDET.id')
        ->paginate(30);

        $data['po_number_list'] = DB::table('purchase_order')->select('purchase_order.ORDNO','purchase_order.PONO')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->whereRaw($query_str)
        ->groupBy('purchase_order.ORDNO')
        ->orderBy('purchase_order.ORDDT', 'desc')
        ->get();
        
        $data['vendore_list']  = DB::table('purchase_order')->select('users.name','users.id')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
        ->whereRaw($query_str)
        ->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
        ->orderBy('users.name', 'asc')
        ->groupBy('users.name')
        ->get();
        
        $data['cat1_list'] = DB::table('purchase_order')->select('products.cat1')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->whereRaw($query_str)
        ->groupBy('products.cat1')
        ->orderBy('products.cat1', 'desc')
        ->get();
        
        
        $data['cat2_list'] = DB::table('purchase_order')->select('products.cat2')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->whereRaw($query_str)
        ->groupBy('products.cat2')
        ->orderBy('products.cat2', 'desc')
        ->get();
        
        #dd($purchaseOrderList);
        return view('admin.purchase-order-archieve-list',$data)->with('purchaseOrderList', $purchaseOrderList);
    }

    public function filterArchievePurchaseOrder(Request $request)
    {
        $po_number=$request->po_number;
        $category1=$request->category1;
        $category2=$request->category2;
        $due_from=$request->due_from;
        $due_to=$request->due_to;
        $vendore_id=$request->vendore_name;
        
        
        $query_str=" (((purchase_order.status = 'OPEN') AND (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY<=0)) OR (V_PURORDDET.STATUS ='CLOSED'))";
        if(!empty($po_number))
            $query_str .=" AND purchase_order.PONO = '".$po_number."' ";
        if(!empty($category1))
            $query_str .=" AND products.cat1 = '".$category1."' ";
        if(!empty($category2))
            $query_str .=" AND products.cat2 = '".$category2."' ";
            
        if(!empty($due_from))
            $query_str .=" AND DATE(purchase_order.ORDDT) >= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_from)))."' ";
        if(!empty($due_to))
            $query_str .=" AND DATE(purchase_order.ORDDT) <= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_to)))."' ";
        
        if(!empty($vendore_id))
            $query_str .=" AND users.id = '".$vendore_id."' ";
        //dd($query_str);
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.PONO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE', 'products.ICODE', 'V_PURORDDET.id', 'V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY', 'users.name',DB::raw('sum(git.git_qty) as git_qty'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
        
        ->whereRaw($query_str)
        ->orderBy('V_PURORDDET.updated_at', 'desc')
        ->groupBy('V_PURORDDET.id')
        ->paginate(30);
        //->toSql();
        
        //dd($purchaseOrderList);
        return view('admin.purchase-order-archieve-filter')->with('purchaseOrderList', $purchaseOrderList);
    }

    public function rejectPOview()
    {
        
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.PONO', 'purchase_order.ORDDT','purchase_order.TIME', 'purchase_order.remarks', 'products.cat1','products.cat2','products.cat3','products.cat4','products.RATE', 'products.ICODE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY','users.name', DB::raw('sum(git.git_qty) as git_qty'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
        ->where('purchase_order.status', '=','REJECT')
        ->orderBy('purchase_order.updated_at', 'desc')
        ->groupBy('purchase_order.ORDNO')
        ->paginate(30);

        $data['po_number_list'] = DB::table('purchase_order')->select('purchase_order.ORDNO','purchase_order.PONO')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','REJECT')
        ->groupBy('purchase_order.ORDNO')
        ->orderBy('purchase_order.updated_at', 'desc')
        ->groupBy('purchase_order.ORDNO')
        ->get();
        
        $data['vendore_list']  = DB::table('purchase_order')->select('users.name','users.id')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
        ->where('purchase_order.status', '=','REJECT')
        ->orderBy('users.name', 'asc')
        ->groupBy('users.name')
        ->get();
        
        $data['cat1_list'] = DB::table('purchase_order')->select('products.cat1')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','REJECT')
        ->groupBy('products.cat1')
        ->orderBy('products.cat1', 'desc')
        ->get();
        
        
        $data['cat2_list'] = DB::table('purchase_order')->select('products.cat2')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','REJECT')
        ->groupBy('products.cat2')
        ->orderBy('products.cat2', 'desc')
        ->get();
        
        
        //dd($purchaseOrderList);
        return view('admin.purchase-order-reject-list',$data)->with('purchaseOrderList', $purchaseOrderList);
    }

     public function filterRejectPurchaseOrder(Request $request)
    {
       
        $po_number=$request->po_number;
        $category1=$request->category1;
        $category2=$request->category2;
        $due_from=$request->due_from;
        $due_to=$request->due_to;
        $vendore_id=$request->vendore_name;
        
        
        $query_str=" purchase_order.status = 'REJECT' ";
        if(!empty($po_number))
            $query_str .=" AND purchase_order.PONO = '".$po_number."' ";
        if(!empty($category1))
            $query_str .=" AND products.cat1 = '".$category1."' ";
        if(!empty($category2))
            $query_str .=" AND products.cat2 = '".$category2."' ";
            
        if(!empty($due_from))
            $query_str .=" AND DATE(purchase_order.ORDDT) >= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_from)))."' ";
        if(!empty($due_to))
            $query_str .=" AND DATE(purchase_order.ORDDT) <= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_to)))."' ";
        
        if(!empty($vendore_id))
            $query_str .=" AND users.id = '".$vendore_id."' ";
        
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.PONO', 'purchase_order.ORDDT','purchase_order.TIME', 'purchase_order.remarks', 'products.cat1','products.cat2','products.cat3','products.cat4','products.RATE', 'products.ICODE','V_PURORDDET.ORDQTY', 'V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY', 'users.name', DB::raw('sum(git.git_qty) as git_qty'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
        ->where('purchase_order.status', '=','REJECT')
        ->whereRaw($query_str)
        ->orderBy('purchase_order.updated_at', 'desc')
        ->groupBy('purchase_order.ORDNO')
        ->paginate(30);
        
        //dd($purchaseOrderList);
        return view('admin.purchase-order-reject-filter')->with('purchaseOrderList', $purchaseOrderList);
    }

}
