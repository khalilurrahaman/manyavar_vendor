<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\purchesOrder;
use Auth;
use Redirect;
use DB;
use Carbon\Carbon;

class PurchaseOrderController extends Controller
{


    //========Start Po Functionality=============//
    public function index()
    {  
		if($this->userAccessibilityCheck('purchase-order-list'))
        {
            $query_str=" purchase_order.status = 'OPEN' AND V_PURORDDET.STATUS = 'OPEN' AND 
            ((V_PURORDDET.ORDQTY - (V_PURORDDET.RCQTY + V_PURORDDET.CNLQTY)) > 0) ";
                
            $vendorList = DB::table('purchase_order')
            ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
            ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
            ->whereRaw($query_str)
            ->orderBy('users.name', 'asc')
            //->groupBy('users.id')
            ->distinct('users.id')
            ->pluck('users.name');

             $designlist = DB::table('purchase_order')
            ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
            ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
            ->whereRaw($query_str)
            ->where('products.cat1','!=','NULL')
            ->distinct('products.cat1')
            ->orderBy('products.cat1', 'desc')
            ->pluck('products.cat1');
            
             $colorList = DB::table('purchase_order')
            ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
            ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
            ->whereRaw($query_str)
            ->where('products.cat2','!=','NULL')
            ->distinct('products.cat2')
            ->orderBy('products.cat2', 'desc')
            ->pluck('products.cat2');
            #dd( $vendorList);
			return view('admin.purchase-order-list')->with('vendorList',$vendorList)->with('designlist',$designlist)->with('colorList',$colorList);
        }
		else
        {
			return Redirect::route('dashboard')->withErrors(['You have no access!']);		          
        }
    	
    }


    public function filterPurchaseOrderAutoSearch(Request $request)
    {       
        
        $due_from=trim($request->due_from);
        $due_to=trim($request->due_to);
        $vendore_name=trim($request->vendore_name);
        
        
        $query_str=" purchase_order.status = 'OPEN' AND V_PURORDDET.STATUS = 'OPEN' AND 
        ((V_PURORDDET.ORDQTY - (V_PURORDDET.RCQTY + V_PURORDDET.CNLQTY)) > 0) ";

        if(!empty($due_from))
            $query_str .=" AND DATE(purchase_order.ORDDT) >= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_from)))."' ";
        if(!empty($due_to))
            $query_str .=" AND DATE(purchase_order.ORDDT) <= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_to)))."' ";        
        if(!empty($vendore_name))
            $query_str .=" AND users.name like '%".$vendore_name."%' ";

            
        $poList = DB::table('purchase_order')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
        ->whereRaw($query_str)
        ->orderBy('purchase_order.PONO', 'asc')
        ->distinct('purchase_order.PONO')
        ->pluck('purchase_order.PONO');
        //dd($purchaseOrderList );
    
        return $poList;
    }
    
    public function getPoList()
    {       
		
		$query_str=" purchase_order.status = 'OPEN' AND V_PURORDDET.STATUS = 'OPEN' AND 
        (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) > 0 ";
		
    	$po_number_list = DB::table('purchase_order')->select('purchase_order.ORDNO','purchase_order.PONO as PONO')
    	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
		->whereRaw($query_str)
    	->orderBy('purchase_order.PONO', 'asc')
         ->groupBy('purchase_order.PONO')
        ->get();
		
		
       

        $po_list_html='<option value="">PO Number</option>';
        foreach($po_number_list as $row)
        {
            $po_list_html .='<option value="'.$row->PONO.'">'.$row->PONO.'</option>';
        }

        $return_data=array();
        $return_data['po']=$po_list_html;

        return $return_data;exit;
    }

    public function getVendorList()
    {               
		
		$query_str=" purchase_order.status = 'OPEN' AND V_PURORDDET.STATUS = 'OPEN' AND 
        (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) > 0 ";
		
    	$vendore_list = DB::table('purchase_order')->select('users.name','users.id')
    	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
		->whereRaw($query_str)
    	->orderBy('users.name', 'asc')
         ->groupBy('users.name')
        ->get();
		
		
        $vendor_list_html='<option value="">Vendor Number</option>';
        foreach($vendore_list as $row)
        {
            $vendor_list_html .='<option value="'.$row->id.'">'.$row->name.'</option>';
        }

        $return_data=array();
        $return_data['vendor']=$vendor_list_html;

        return $return_data;exit;
    }


    public function getDesignList()
    {
		
		$query_str=" purchase_order.status = 'OPEN' AND V_PURORDDET.STATUS = 'OPEN' AND 
        (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) > 0 ";
		
    	$design_list = DB::table('purchase_order')->select('products.cat1')
    	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
		->whereRaw($query_str)
    	->orderBy('products.cat1', 'asc')
         ->groupBy('products.cat1')
        ->get();

        $design_list_html='<option value="">Design</option>';
        foreach($design_list as $row)
        {
            $design_list_html .='<option value="'.$row->cat1.'">'.$row->cat1.'</option>';
        }

        $return_data=array();
        $return_data['design']=$design_list_html;

        return $return_data;exit;
    }

    public function getColorList()
    {        		
		$query_str=" purchase_order.status = 'OPEN' AND V_PURORDDET.STATUS = 'OPEN' AND 
        (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) > 0 ";
		
    	$cat2_list = DB::table('purchase_order')->select('products.cat2')
    	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
		->whereRaw($query_str)
    	->orderBy('products.cat2', 'asc')
         ->groupBy('products.cat2')
        ->get();

        $color_list_html='<option value="">Colour</option>';
        foreach($cat2_list as $row)
        {
            $color_list_html .='<option value="'.$row->cat2.'">'.$row->cat2.'</option>';
        }

        $return_data=array();
        $return_data['color']=$color_list_html;

        return $return_data;exit;
    }
	
	public function getCategory3List()
    {      
		
		$query_str=" purchase_order.status = 'OPEN' AND V_PURORDDET.STATUS = 'OPEN' AND 
        (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) > 0 ";
		
    	$cat3_list = DB::table('purchase_order')->select('products.cat3','products.CCODE3')
    	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
		->whereRaw($query_str)
    	->orderBy('products.cat3', 'asc')
         ->groupBy('products.cat3')
        ->get();

        $cat3_list_html='<option value="">Cat 3</option>';
        foreach($cat3_list as $row)
        {
            $cat3_list_html .='<option value="'.$row->CCODE3.'">'.$row->cat3.'</option>';
        }

        $return_data=array();
        $return_data['category3']=$cat3_list_html;

        return $return_data;exit;
    }
	
	public function getWidthSizeList()
    {           
		
		$query_str=" purchase_order.status = 'OPEN' AND V_PURORDDET.STATUS = 'OPEN' AND 
        (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY) > 0 ";
		
    	$cat4_list = DB::table('purchase_order')->select('products.cat4','products.CCODE4')
    	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
		->whereRaw($query_str)
    	->orderBy('products.cat4', 'asc')
         ->groupBy('products.cat4')
        ->get();


        $cat4_list_html="<option value=''>Width / Size</option>";
        foreach($cat4_list as $row)
        {
            $cat4_list_html .="<option value='".$row->CCODE4."'>".$row->cat4."</option>";
        }

        $return_data=array();
        $return_data['width_size']=$cat4_list_html;

        return $return_data;exit;
    }
	
	
	
       
    
	public function filterPurchaseOrder(Request $request)
    {    	
		$po_number=trim($request->po_number);
		$category1=trim($request->category1);
		$category2=trim($request->category2);
		$category3=trim($request->category3);
		$category4=trim($request->category4);
        $category6=trim($request->category6);
		$due_from=trim($request->due_from);
		$due_to=trim($request->due_to);

        $next15=$request->next15;
        $prev15=$request->prev15;

		$vendore_name=trim($request->vendore_name);
		
		
		$query_str=" purchase_order.status = 'OPEN' AND V_PURORDDET.STATUS = 'OPEN' AND 
        ((V_PURORDDET.ORDQTY - (V_PURORDDET.RCQTY + V_PURORDDET.CNLQTY)) > 0) ";

    	if(!empty($po_number))
    		$query_str .=" AND purchase_order.PONO like '%".$po_number."%' ";			
    	if(!empty($category1))
    		$query_str .=" AND products.cat1 like '%".$category1."%' ";
		if(!empty($category2))
    		$query_str .=" AND products.cat2 like '%".$category2."%' ";
		if(!empty($category3))
    		$query_str .=" AND products.cat3 like '%".$category3."%' ";
		if(!empty($category4))
    		$query_str .=" AND products.cat4 like '%".$category4."%' ";
        if(!empty($category6))
            $query_str .=" AND products.CNAME6 like '%".$category6."%' ";
		
			
		if(!empty($due_from))
    		$query_str .=" AND DATE(purchase_order.ORDDT) >= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_from)))."' ";
    	if(!empty($due_to))
    		$query_str .=" AND DATE(purchase_order.ORDDT) <= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_to)))."' ";

        if(!empty($prev15)){
            //$bet1="'".Carbon::now()->subDay(15)->format('Y-m-d')."' AND '".Carbon::now()->format('Y-m-d')."'";
            $bet1="'".Carbon::now()->format('Y-m-d')."'";
            $query_str .=" AND purchase_order_delivery.DUEDATE <= ". $bet1;
        }

        if(!empty($next15)){
            $bet2="'".Carbon::now()->format('Y-m-d')."' AND '".Carbon::now()->addDay(15)->format('Y-m-d')."'";
            $query_str .=" AND purchase_order_delivery.DUEDATE BETWEEN ". $bet2;
        }

		
		if(!empty($vendore_name))
    		$query_str .=" AND users.name like '%".$vendore_name."%' ";

			
    	$purchaseOrderList = DB::table('purchase_order')->select(['purchase_order.ORDNO', 'purchase_order.PONO as PONO', 'purchase_order.ORDDT','purchase_order.TIME','products.id as product_id','products.image','products.cat1','products.cat2','products.cat3','products.cat4','products.CNAME5','git.pd_id as pd_id','products.CNAME6','products.CNAME5','products.CNAME6','V_PURORDDET.RATE', 'products.ICODE','purchase_order_delivery.DUEDATE', 'V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY','users.name',DB::raw('sum(git.git_qty) as git_qty')])
    	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
        ->leftJoin('purchase_order_delivery', 'purchase_order_delivery.PURORDDET_CODE', '=', 'V_PURORDDET.CODE')
		->whereRaw($query_str)
    	->orderBy('purchase_order_delivery.DUEDATE', 'asc')
        ->groupBy('V_PURORDDET.id')
        ->get();
       // dd($purchaseOrderList );

        //$perpageTotalrate =  $purchaseOrderList->sum('RATE');
        $perpageTotalorderQty =  $purchaseOrderList->sum('ORDQTY');
        $perpageTotalrcvQty =  $purchaseOrderList->sum('RCQTY');
        $perpageTotalcanQty =  $purchaseOrderList->sum('CNLQTY');
        $perpageTotalgitQty =  $purchaseOrderList->sum('git_qty');
        //dd($perpageTotalrate);
        if(!empty($purchaseOrderList) && count($purchaseOrderList)>0)
        {
            foreach($purchaseOrderList as $reqst)
            {
                 $reqst->p_git_qty=0;
                if(!empty($reqst->pd_id))
                {
                $pqty = DB::table('git')->select(DB::raw('sum(git.git_qty) as p_git_qty'))    
                ->where('git_rcv','=','0.00')
                ->where('pd_id','=',$reqst->pd_id)
                ->groupBy('pd_id')
                ->first();
                if(!empty($pqty) && count($pqty)>0)
                    $reqst->p_git_qty=$pqty->p_git_qty;
                else
                    $reqst->p_git_qty=0;
            }


            }
        }
		
	
    	return view('admin.purchase-order-list-filter')->with('purchaseOrderList', $purchaseOrderList)->with('perpageTotalorderQty',$perpageTotalorderQty)->with('perpageTotalrcvQty',$perpageTotalrcvQty)->with('perpageTotalcanQty',$perpageTotalcanQty)->with('perpageTotalgitQty',$perpageTotalgitQty);
    }
     //=========End Po Functionality=============//

    //==========Start Archive Po Functionality=============//
    public function archievePOview()
    {
		if($this->userAccessibilityCheck('archieve-purchase-order-list'))
        {
            $query_str=" (((purchase_order.status = 'OPEN') AND ((V_PURORDDET.ORDQTY - (V_PURORDDET.RCQTY + V_PURORDDET.CNLQTY<=0)))) OR (V_PURORDDET.STATUS ='CLOSED'))";
            //$vendorList ='';
            $vendorList = DB::table('purchase_order')
            ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
            ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
            ->whereRaw($query_str)
            ->orderBy('users.name', 'asc')
            ->distinct('users.id')
            ->pluck('users.name');
            //$poList ='';
            $poList = DB::table('purchase_order')
            ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
            ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
            ->whereRaw($query_str)
            ->orderBy('purchase_order.PONO', 'asc')
            ->distinct('purchase_order.PONO')
            ->pluck('purchase_order.PONO');
            
            //$designlist ='';
            //  $designlist = DB::table('purchase_order')
            // ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
            // ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
            // ->whereRaw($query_str)
            // ->where('products.cat1','!=','NULL')
            // ->distinct('products.cat1')
            // ->orderBy('products.cat1', 'desc')
            // ->pluck('products.cat1');
            //$colorList ='';
            // $colorList = DB::table('purchase_order')
            // ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
            // ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
            // ->whereRaw($query_str)
            // ->where('products.cat2','!=','NULL')
            // ->distinct('products.cat2')
            // ->orderBy('products.cat2', 'desc')
            // ->pluck('products.cat2');

			// return view('admin.purchase-order-archieve-list')->with('vendorList',$vendorList)->with('poList',$poList)->with('designlist',$designlist)->with('colorList',$colorList);
            return view('admin.purchase-order-archieve-list')->with('vendorList',$vendorList)->with('poList',$poList);
        }
		else{
			return Redirect::route('dashboard')->withErrors(['You have no access!']);			
        }
        
    }

    public function getArchivePoList()
    {
        $query_str=" (((purchase_order.status = 'OPEN') AND (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY<=0)) OR (V_PURORDDET.STATUS ='CLOSED'))";

        $po_number_list = DB::table('purchase_order')->select('purchase_order.ORDNO','purchase_order.PONO as PONO')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->whereRaw($query_str)
        ->orderBy('purchase_order.ORDDT', 'desc')
        ->groupBy('purchase_order.ORDNO')
        ->get();

        $po_list_html='<option value="">PO Number</option>';
        foreach($po_number_list as $row)
        {
            $po_list_html .='<option value="'.$row->PONO.'">'.$row->PONO.'</option>';
        }

        $return_data=array();
        $return_data['po']=$po_list_html;

        return $return_data;exit;
    }

    public function getArchiveVendorList()
    {        
        $query_str=" (((purchase_order.status = 'OPEN') AND (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY<=0)) OR (V_PURORDDET.STATUS ='CLOSED'))";

        $vendore_list  = DB::table('purchase_order')->select('users.name','users.id')  
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
        ->whereRaw($query_str)
        ->orderBy('users.name', 'asc')
        ->groupBy('users.name')
        ->get();

        $vendor_list_html='<option value="">Vendor Number</option>';
        foreach($vendore_list as $row)
        {
            $vendor_list_html .='<option value="'.$row->id.'">'.$row->name.'</option>';
        }

        $return_data=array();
        $return_data['vendor']=$vendor_list_html;

        return $return_data;exit;
    }


    public function getArchiveDesignList()
    {
        $query_str=" (((purchase_order.status = 'OPEN') AND (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY<=0)) OR (V_PURORDDET.STATUS ='CLOSED'))";
        
       $design_list = DB::table('purchase_order')->select('products.cat1')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->whereRaw($query_str)
        ->groupBy('products.cat1')
        ->orderBy('products.cat1', 'desc')
        ->get();

        $design_list_html='<option value="">Design</option>';
        foreach($design_list as $row)
        {
            $design_list_html .='<option value="'.$row->cat1.'">'.$row->cat1.'</option>';
        }

        $return_data=array();
        $return_data['design']=$design_list_html;

        return $return_data;exit;
    }

    public function getArchiveColorList()
    { 
        $query_str=" (((purchase_order.status = 'OPEN') AND (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY<=0)) OR (V_PURORDDET.STATUS ='CLOSED'))";

        $cat2_list = DB::table('purchase_order')->select('products.cat2')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->whereRaw($query_str)
        ->groupBy('products.cat2')
        ->orderBy('products.cat2', 'desc')
        ->get();

        $color_list_html='<option value="">Color</option>';
        foreach($cat2_list as $row)
        {
            $color_list_html .='<option value="'.$row->cat2.'">'.$row->cat2.'</option>';
        }

        $return_data=array();
        $return_data['color']=$color_list_html;

        return $return_data;exit;
    }
     public function getArchiveCategory3List()
    {               
        /*$cat3_list = DB::table('V_PURORDDET')->select('products.cat3')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('V_PURORDDET.STATUS', '=','OPEN')
        ->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
        ->groupBy('products.cat3')
        ->orderBy('products.cat3', 'desc')
        ->get();*/
		
		 $query_str=" (((purchase_order.status = 'OPEN') AND (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY<=0)) OR (V_PURORDDET.STATUS ='CLOSED'))";

        $cat3_list = DB::table('purchase_order')->select('products.cat3')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
		->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->whereRaw($query_str)
        ->orderBy('products.cat3', 'desc')
        ->groupBy('products.cat3')
        ->get();



        $category3_list_html='<option value="">Cat 3</option>';
        foreach($cat3_list as $row)
        {
            $category3_list_html .='<option value="'.$row->cat3.'">'.$row->cat3.'</option>';
        }

        $return_data=array();
        $return_data['category3']=$category3_list_html;

        return $return_data;exit;
    }
	
	
	 public function getArchiveWidthSizeList()
    {               
        /*$cat4_list = DB::table('V_PURORDDET')->select('products.cat4')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('V_PURORDDET.STATUS', '=','OPEN')
        ->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
        ->groupBy('products.cat4')
        ->orderBy('products.cat4', 'desc')
        ->get();*/
		
		 $query_str=" (((purchase_order.status = 'OPEN') AND (V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY<=0)) OR (V_PURORDDET.STATUS ='CLOSED'))";

        $cat4_list = DB::table('purchase_order')->select('products.cat4')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
		->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->whereRaw($query_str)
        ->orderBy('products.cat4', 'desc')
        ->groupBy('products.cat4')
        ->get();
		

        $width_size_list_html="<option value=''>Width / Size</option>";
        foreach($cat4_list as $row)
        {
            $width_size_list_html .="<option value='".$row->cat4."'>".$row->cat4."</option>";
        }

        $return_data=array();
        $return_data['width_size']=$width_size_list_html;

        return $return_data;exit;
    }  

    public function filterArchievePurchaseOrder(Request $request)
    {
        $po_number=trim($request->po_number);
        $category1=trim($request->category1);
        $category2=trim($request->category2);
        $category3=trim($request->category3);
        $category4=trim($request->category4);
        $category6=trim($request->category6);
        $due_from=trim($request->due_from);
        $due_to=trim($request->due_to);
        $vendore_name=trim($request->vendore_name);
        
        
        $query_str=" (((purchase_order.status = 'OPEN') AND ((V_PURORDDET.ORDQTY - (V_PURORDDET.RCQTY + V_PURORDDET.CNLQTY<=0)))) OR (V_PURORDDET.STATUS ='CLOSED'))";
        
        if(!empty($po_number))
            $query_str .=" AND purchase_order.PONO like '%".$po_number."%' ";           
        if(!empty($category1))
            $query_str .=" AND products.cat1 like '%".$category1."%' ";
        if(!empty($category2))
            $query_str .=" AND products.cat2 like '%".$category2."%' ";
        if(!empty($category3))
            $query_str .=" AND products.cat3 like '%".$category3."%' ";
        if(!empty($category4))
            $query_str .=" AND products.cat4 like '%".$category4."%' ";
        if(!empty($category6))
            $query_str .=" AND products.CNAME6 like '%".$category6."%' ";
        
            
        if(!empty($due_from))
            $query_str .=" AND DATE(purchase_order.ORDDT) >= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_from)))."' ";
        if(!empty($due_to))
            $query_str .=" AND DATE(purchase_order.ORDDT) <= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_to)))."' ";
        
        if(!empty($vendore_name))
            $query_str .=" AND users.name like '%".$vendore_name."%' ";

        //dd($query_str);
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.PONO as PONO', 'purchase_order.ORDDT','purchase_order.TIME','products.id as product_id','products.image','products.cat1','products.cat2','products.cat3','products.cat4','products.CNAME5','products.CNAME6','V_PURORDDET.RATE', 'products.ICODE','purchase_order_delivery.DUEDATE', 'V_PURORDDET.id', 'V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY', 'users.name',DB::raw('sum(git.git_qty) as git_qty'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
        ->leftJoin('purchase_order_delivery', 'purchase_order_delivery.PURORDDET_CODE', '=', 'V_PURORDDET.CODE')
        ->whereRaw($query_str)
        ->orderBy('V_PURORDDET.updated_at', 'desc')
        ->groupBy('V_PURORDDET.id')
        ->get();
        //->toSql();
        
        //dd($purchaseOrderList);
        return view('admin.purchase-order-archieve-filter')->with('purchaseOrderList', $purchaseOrderList);
    }

    //==========End Archive Po Functionality=============//

    //==========Start Rejected Po Functionality=============//

    public function rejectPOview()
    {
		if($this->userAccessibilityCheck('all-git-list'))
        {
            $query_str=" purchase_order.status = 'REJECT' AND V_PURORDDET.STATUS ='REJECT' ";
            $vendorList = DB::table('purchase_order')
            ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
            ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
            ->whereRaw($query_str)
            ->orderBy('users.name', 'asc')
            ->distinct('users.id')
            ->pluck('users.name');
            
            $poList = DB::table('purchase_order')
            ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
            ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
            ->whereRaw($query_str)
            ->orderBy('purchase_order.PONO', 'asc')
            ->distinct('purchase_order.PONO')
            ->pluck('purchase_order.PONO');

            $designlist = DB::table('purchase_order')
            ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
            ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
            ->whereRaw($query_str)
            ->where('products.cat1','!=','NULL')
            ->distinct('products.cat1')
            ->orderBy('products.cat1', 'desc')
            ->pluck('products.cat1');

            $colorList = DB::table('purchase_order')
            ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
            ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
            ->whereRaw($query_str)
            ->where('products.cat2','!=','NULL')
            ->distinct('products.cat2')
            ->orderBy('products.cat2', 'desc')
            ->pluck('products.cat2');
            
           //dd($colorList);
			return view('admin.purchase-order-reject-list')->with('vendorList',$vendorList)->with('poList',$poList)->with('designlist',$designlist)->with('colorList',$colorList);
        }
		else{
			return Redirect::route('dashboard')->withErrors(['You have no access!']);
        }
        
    }

    public function getRejectPoList()
    {
       /* $po_number_list = DB::table('purchase_order')->select('purchase_order.ORDNO','purchase_order.PONO')
        ->where('purchase_order.status', '=','REJECT')
		->groupBy('purchase_order.PONO')
        ->orderBy('purchase_order.ORDDT', 'desc')
        ->get();*/
		
		$po_number_list = DB::table('purchase_order')->select('purchase_order.PONO as PONO','purchase_order.ORDNO')
		->leftJoin('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
		->where('purchase_order.status', '=','REJECT')
        ->where('V_PURORDDET.STATUS', '=','REJECT')
        //->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
        ->groupBy('purchase_order.PONO')
        ->orderBy('purchase_order.PONO', 'desc')
        ->get();
		
		
        $po_list_html='<option value="">PO Number</option>';
        foreach($po_number_list as $row)
        {
            $po_list_html .='<option value="'.$row->PONO.'">'.$row->PONO.'</option>';
        }

        $return_data=array();
        $return_data['po']=$po_list_html;

        return $return_data;exit;
    }

    public function getRejectVendorList()
    {        
        $vendore_list  = DB::table('purchase_order')->select('users.name','users.id')  
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
        ->where('purchase_order.status', '=','REJECT')
        ->orderBy('users.name', 'asc')
        ->groupBy('users.name')
        ->get();

        $vendor_list_html='<option value="">Vendor Number</option>';
        foreach($vendore_list as $row)
        {
            $vendor_list_html .='<option value="'.$row->id.'">'.$row->name.'</option>';
        }

        $return_data=array();
        $return_data['vendor']=$vendor_list_html;

        return $return_data;exit;
    }


    public function getRejectDesignList()
    {
        
       $design_list = DB::table('V_PURORDDET')->select('products.cat1')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('V_PURORDDET.STATUS', '=','REJECT')
        //->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
        ->groupBy('products.cat1')
        ->orderBy('products.cat1', 'desc')
        ->get();

        $design_list_html='<option value="">Design</option>';
        foreach($design_list as $row)
        {
            $design_list_html .='<option value="'.$row->cat1.'">'.$row->cat1.'</option>';
        }

        $return_data=array();
        $return_data['design']=$design_list_html;

        return $return_data;exit;
    }

    public function getRejectColorList()
    {               
        $cat2_list = DB::table('V_PURORDDET')->select('products.cat2')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('V_PURORDDET.STATUS', '=','REJECT')
        //->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
        ->groupBy('products.cat2')
        ->orderBy('products.cat2', 'desc')
        ->get();

        $color_list_html='<option value="">Color</option>';
        foreach($cat2_list as $row)
        {
            $color_list_html .='<option value="'.$row->cat2.'">'.$row->cat2.'</option>';
        }

        $return_data=array();
        $return_data['color']=$color_list_html;

        return $return_data;exit;
    }
	
	 public function getRejectCategory3List()
    {               
        $cat3_list = DB::table('V_PURORDDET')->select('products.cat3','products.CCODE3')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('V_PURORDDET.STATUS', '=','REJECT')
        //->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
        ->groupBy('products.cat3')
        ->orderBy('products.cat3', 'desc')
        ->get();

        $cat3_list_html='<option value="">Cat 3</option>';
        foreach($cat3_list as $row)
        {
            $cat3_list_html .='<option value="'.$row->CCODE3.'">'.$row->cat3.'</option>';
        }

        $return_data=array();
        $return_data['category3']=$cat3_list_html;
		

        return $return_data;exit;
    }
	
	
	 public function getRejectWidthSizeList()
    {               
        $cat4_list = DB::table('V_PURORDDET')->select('products.cat4','products.CCODE4')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('V_PURORDDET.STATUS', '=','REJECT')
        //->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
        ->groupBy('products.cat4')
        ->orderBy('products.cat4', 'desc')
        ->get();
        $cat4_list_html="<option value=''>Width / Size</option>";
        foreach($cat4_list as $row)
        {
            $cat4_list_html .="<option value='".$row->CCODE4."'>".$row->cat4."</option>";
        }

        $return_data=array();
        $return_data['width_size']=$cat4_list_html;

        return $return_data;exit;
    }

    public function filterRejectPurchaseOrder(Request $request)
    {
       
        $po_number=trim($request->po_number);
        $category1=trim($request->category1);
        $category2=trim($request->category2);
        $category3=trim($request->category3);
        $category4=trim($request->category4);
        $category6=trim($request->category6);
        $due_from=trim($request->due_from);
        $due_to=trim($request->due_to);
        $vendore_name=trim($request->vendore_name);
        
        
        $query_str=" purchase_order.status = 'REJECT' AND V_PURORDDET.STATUS ='REJECT' ";
        
        if(!empty($po_number))
            $query_str .=" AND purchase_order.PONO like '%".$po_number."%' ";           
        if(!empty($category1))
            $query_str .=" AND products.cat1 like '%".$category1."%' ";
        if(!empty($category2))
            $query_str .=" AND products.cat2 like '%".$category2."%' ";
        if(!empty($category3))
            $query_str .=" AND products.cat3 like '%".$category3."%' ";
        if(!empty($category4))
            $query_str .=" AND products.cat4 like '%".$category4."%' ";
        if(!empty($category6))
            $query_str .=" AND products.CNAME6 like '%".$category6."%' ";
        
            
        if(!empty($due_from))
            $query_str .=" AND DATE(purchase_order.ORDDT) >= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_from)))."' ";
        if(!empty($due_to))
            $query_str .=" AND DATE(purchase_order.ORDDT) <= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_to)))."' ";
        
        if(!empty($vendore_name))
            $query_str .=" AND users.name like '%".$vendore_name."%' ";
        
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.PONO as PONO', 'purchase_order.ORDDT','purchase_order.TIME', 'purchase_order.remarks','products.id as product_id','products.image', 'products.cat1','products.cat2','products.cat3','products.cat4','products.CNAME5','products.CNAME6','V_PURORDDET.RATE', 'products.ICODE','purchase_order_delivery.DUEDATE','V_PURORDDET.ORDQTY', 'V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY', 'users.name', DB::raw('sum(git.git_qty) as git_qty'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->leftJoin('users', 'users.SLCODE', '=', 'purchase_order.PCODE')
        ->leftJoin('purchase_order_delivery', 'purchase_order_delivery.PURORDDET_CODE', '=', 'V_PURORDDET.CODE')
        ->whereRaw($query_str)
        ->orderBy('purchase_order.updated_at', 'desc')
        ->groupBy('V_PURORDDET.id')
        ->get();
        
        //dd($purchaseOrderList);
        return view('admin.purchase-order-reject-filter')->with('purchaseOrderList', $purchaseOrderList);
    }

    //==========End Rejected Po Functionality=============//

}
