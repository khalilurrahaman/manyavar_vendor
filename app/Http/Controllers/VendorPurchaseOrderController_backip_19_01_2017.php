<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

class VendorPurchaseOrderController extends Controller
{
    public function index()
    {
    	return view('admin.purchase-order-list');
    }
	
	
    public function purchaseOrderlistview()//Vendor Purchase Order Listing Section
    {   
        $user = Auth::user();
		$purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.PONO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE', 'products.ICODE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY',DB::raw('sum(git.git_qty) as git_qty'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
    	->where('purchase_order.status', '=','OPEN')
		->where('V_PURORDDET.status', '=','OPEN')
		->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY'),'>',0)
		//->where('git_qty','>',0)
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
    	->orderBy('purchase_order.ORDDT', 'desc')
		->groupBy('V_PURORDDET.id')
		->paginate(30);
		
		
		//Filter Data
		$data['po_number_list'] = DB::table('purchase_order')->select('ORDNO','PONO')->where('status','=','OPEN')->where('PCODE','=',$user->SLCODE)->groupBy('ORDNO')->orderBy('ORDNO','desc')->get();

        $data['cat1_list']=DB::table('purchase_order')->select('products.cat1')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
        ->groupBy('products.cat1')
        ->orderBy('products.cat1', 'desc')->get();

        $data['cat2_list']=DB::table('purchase_order')->select('products.cat2')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->where('purchase_order.status', '=','OPEN')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
        ->groupBy('products.cat2')
        ->orderBy('products.cat2', 'desc')->get();


		//$data['cat1_list'] = DB::table('products')->select('cat1')->groupBy('cat1')->orderBy('cat1','desc')->get();


		//$data['cat2_list'] = DB::table('products')->select('cat2')->groupBy('cat2')->orderBy('cat2','desc')->get();
		if(!empty($purchaseOrderList) && count($purchaseOrderList)>0)
        {
            foreach($purchaseOrderList as $reqst)
            {
                $actiondtls =DB::table('po_action')->select('action_type','status')->where('po_no', '=',$reqst->PONO)->where('status', '=','new')->orderBy('created_at','desc')->get();
                        if(!empty($actiondtls) && count($actiondtls)>0)
                        $reqst->actiondtls=$actiondtls;
                }
            }
         //dd($purchaseOrderList);
    	return view('vendor.vendor-purchase-order-list',$data)->with('purchaseOrderList', $purchaseOrderList);
    }
	
	public function filterPurchaseOrder(Request $request)
    {
    	$user = Auth::user();
		$po_number=$request->po_number;
		$category1=$request->category1;
		$category2=$request->category2;
		$due_from=$request->due_from;
		$due_to=$request->due_to;
		
		$query_str=" purchase_order.status = 'OPEN' ";
    	if(!empty($po_number))
    		$query_str .=" AND purchase_order.PONO = '".$po_number."' ";
    	if(!empty($category1))
    		$query_str .=" AND products.cat1 = '".$category1."' ";
		if(!empty($category2))
    		$query_str .=" AND products.cat2 = '".$category2."' ";
		if(!empty($due_from))
    		$query_str .=" AND DATE(purchase_order.ORDDT) >= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_from)))."' ";
    	if(!empty($due_to))
    		$query_str .=" AND DATE(purchase_order.ORDDT) <= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_to)))."' ";
			
    	$purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.PONO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE', 'products.ICODE', 'V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY',DB::raw('sum(git.git_qty) as git_qty'))
		
		->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
    	->where('purchase_order.status', '=','OPEN')
		->where('V_PURORDDET.status', '=','OPEN')
		->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY - V_PURORDDET.CNLQTY'),'>',0)
		//->where('git_qty','>',0)
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
		->whereRaw($query_str)
    	->orderBy('purchase_order.ORDDT', 'desc')
		->groupBy('V_PURORDDET.id')
		->paginate(30);
		
        if(!empty($purchaseOrderList) && count($purchaseOrderList)>0)
        {
            foreach($purchaseOrderList as $reqst)
            {
                $actiondtls =DB::table('po_action')->select('action_type','status')->where('po_no', '=',$reqst->PONO)->where('status', '=','new')->orderBy('created_at','desc')->get();
                if(!empty($actiondtls) && count($actiondtls)>0)
                    $reqst->actiondtls=$actiondtls;
            }
        }
	
    	return view('vendor.vendor-purchase-order-list-filter')->with('purchaseOrderList', $purchaseOrderList);
    }
    public function purchaseOrderRequest(Request $request)
    {
        //dd($request);
        $return=0;
        
        $porequest['user_id']=Auth::user()->id;  
        $porequest['po_no']= $request->po_no;
        $porequest['created_at']= date('Y-m-d H:i:s');
        $porequest['updated_at']= date('Y-m-d H:i:s');
        $porequest['status']= 'new';
		
        if($request->type=='cutting'){        
        $porequest['action_type'] = $request->type;
        //$porequest['new_po'] = $request->new_po;
        $porequest['remarks'] = $request->remarks;
        }

        if($request->type=='rate_change'){
        $porequest['old_value'] = $request->old_rate;        
        $porequest['action_type'] = $request->type;        
        $porequest['Reason'] = $request->reason;
        $porequest['new_rate'] = $request->new_rate;
        }

        if($request->type=='width_change'){ 
        $porequest['old_width'] = $request->old_width;    
        $porequest['old_value'] = $request->old_rate;       
        $porequest['action_type'] = $request->type;
        $porequest['Reason'] = $request->reason;        
        $porequest['new_rate'] = $request->new_rate;
        $porequest['new_width'] = $request->new_width;
        }

        if($request->type=='date_ext'){        
        $porequest['action_type'] = $request->type;
        $porequest['Reason'] = $request->reason;        
        $porequest['ext_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $request->ext_date)));        
        }

        if($request->type=='cancel'){        
        $porequest['action_type'] = $request->type;
        $porequest['Reason'] = $request->reason;
        $porequest['cancel_qty'] = $request->can_qty;       
        }

        if($request->type=='others'){        
        $porequest['action_type'] = $request->type;
        $porequest['remarks'] = $request->reason;  
                   
        }

        if($request->type=='sample'){        
        $porequest['action_type'] = $request->type;
        $porequest['remarks'] = $request->reason;        
        $porequest['docket_no'] = $request->docket_no;
        //$porequest['prn_no'] = $request->prn_no;
        //$porequest['prn_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $request->prn_date)));
        $porequest['courier_no'] = $request->courier_no;                
        }


        if(DB::table('po_action')->insert($porequest))
        {
            $return=1;
        }
        echo $return;
    }

     public function archievePOlistview()//Vendor Archieve Purchase Order Listing Section
    {   
        $user = Auth::user();
        //dd($user->SLCODE);
        $query_str= "((purchase_order.status = 'CLOSED') OR (purchase_order.status = 'REJECT')) AND (purchase_order.PCODE = '".Auth::user()->SLCODE."')";

        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.PONO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE', 'products.ICODE', 'V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY',DB::raw('sum(git.git_qty) as git_qty'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
       ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->whereRaw($query_str)
        ->orderBy('purchase_order.ORDDT', 'desc')
        ->groupBy('V_PURORDDET.id')
        ->paginate(10);
        
        $data['po_number_list'] = DB::table('purchase_order')->select('ORDNO','PONO')->whereRaw($query_str)->groupBy('PONO')->orderBy('PONO','desc')->get();

        $data['cat1_list']=DB::table('purchase_order')->select('products.cat1')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->whereRaw($query_str)
        ->groupBy('products.cat1')
        ->orderBy('products.cat1', 'desc')->get();

        $data['cat2_list']=DB::table('purchase_order')->select('products.cat2')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->whereRaw($query_str)
        ->groupBy('products.cat2')
        ->orderBy('products.cat2', 'desc')->get();
        
        //dd($purchaseOrderList);
        return view('vendor.vendor-purchase-order-archieve',$data)->with('purchaseOrderList', $purchaseOrderList);
    }

    public function filterArchievePurchaseOrder(Request $request)
    {
        $user = Auth::user();
        $po_number=$request->po_number;
        $category1=$request->category1;
        $category2=$request->category2;
        $due_from=$request->due_from;
        $due_to=$request->due_to;
        
        $query_str= "((purchase_order.status = 'CLOSED') OR (purchase_order.status = 'REJECT')) AND (purchase_order.PCODE = '".Auth::user()->SLCODE."')";
        if(!empty($po_number))
            $query_str .=" AND purchase_order.PONO = '".$po_number."' ";
        if(!empty($category1))
            $query_str .=" AND products.cat1 = '".$category1."' ";
        if(!empty($category2))
            $query_str .=" AND products.cat2 = '".$category2."' ";
        if(!empty($due_from))
            $query_str .=" AND DATE(purchase_order.ORDDT) >= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_from)))."' ";
        if(!empty($due_to))
            $query_str .=" AND DATE(purchase_order.ORDDT) <= '".date('Y-m-d', strtotime(str_replace('/', '-', $due_to)))."' ";
            
        $user = Auth::user();
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.ORDNO', 'purchase_order.PONO', 'purchase_order.ORDDT','purchase_order.TIME','products.cat1','products.cat2','products.cat3','products.cat4','products.RATE', 'products.ICODE', 'V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY',DB::raw('sum(git.git_qty) as git_qty'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
       ->leftJoin('git', 'git.pd_id', '=', 'V_PURORDDET.id')
        ->whereRaw($query_str)
        ->orderBy('purchase_order.ORDDT', 'desc')
        ->groupBy('V_PURORDDET.id')
        ->paginate(10);
    
        return view('vendor.vendor-purchase-order-archieve-filter')->with('purchaseOrderList', $purchaseOrderList);
    }
}
