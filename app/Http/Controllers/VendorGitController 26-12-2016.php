<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Barryvdh\DomPDF\Facade as PDF;
class VendorGitController extends Controller
{
    public function viewGit()
    {
    	return view('admin.vendor-all-git-list');
    }

    public function viewGrt()
    {
    	return view('admin.vendor-all-grt-list');
    }
	
	
    public function viewAllVendorGit()
    {
		
    	return view('vendor.vendor-all-git');
    }
	
    public function viewAddVendorGit()
    {
		
		#echo Auth::user()->SLCODE;exit;
		$data['cat1_list'] = DB::table('products')->select('cat1')->groupBy('cat1')->orderBy('cat1','desc')->get();
		$data['cat2_list'] = DB::table('products')->select('cat2')->groupBy('cat2')->orderBy('cat2','desc')->get();
		$data['cat3_list'] = DB::table('products')->select('cat3')->groupBy('cat3')->orderBy('cat3','desc')->get();
		$data['cat4_list'] = DB::table('products')->select('cat4')->groupBy('cat4')->orderBy('cat4','desc')->get();
		
		$data['pending_qty_list'] =DB::table('purchase_order')->select(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY as p_qty'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->where('purchase_order.status', '=','OPEN')
        ->orderBy('p_qty')
		->distinct('p_qty')->get();
		
		
		
		$data['purchaseOrderList'] = DB::table('purchase_order')->select('purchase_order.ORDNO','products.image','products.cat1','products.cat2','products.cat3','products.cat4',DB::raw('sum(V_PURORDDET.ORDQTY) as ORDQTY'),DB::raw('sum(V_PURORDDET.RCQTY) as RCQTY'),'V_PURORDDET.ORDCODE','V_PURORDDET.ICODE',DB::raw('sum(V_PURORDDET.CNLQTY) as CNLQTY'),DB::raw('sum(V_PURORDDET.OQTY) as OQTY'),'git.git_qty')
    	->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
    	->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
		
		->leftJoin('git','git.ICODE', '=', 'V_PURORDDET.ICODE')
		->where('git.status', '!=','Close')
    	->where('purchase_order.status', '=','OPEN')
		->where(DB::raw('V_PURORDDET.ORDQTY - V_PURORDDET.RCQTY'),'>',0)
		->where('purchase_order.PCODE','=',Auth::user()->SLCODE)
    	->orderBy('purchase_order.ORDDT', 'desc')
		->groupBy('V_PURORDDET.ICODE')
		->get();
		
    	return view('vendor.vendor-add-git',$data);
    }
	
	public function submitGitRecords(Request $request)
    {
		
		for($i=0;$i<count($request->git_qty);$i++)
		{
			if(!empty($request->git_qty[$i]))
			{
				
				$exist_check = DB::table('git')
					->where('SLCODE', '=', Auth::user()->SLCODE)
					->where('ICODE', '=', $request->ICODE[$i])
					->first();
				
				
						
				if (is_null($exist_check)) 
				{
						
						$git['ICODE'] = $request->ICODE[$i];//
						$git['SLCODE'] = Auth::user()->SLCODE;
						$git['git_qty'] = $request->git_qty[$i];//
						$git['challan_no'] = $request->challan_no;
						$git['lorry_no'] = $request->lorry_no;
						$git['transport_no'] = $request->transport_no;
						$git['git_date'] = $request->date;
						$git['created_at'] = date('Y-m-d H:i:s');
						$git['updated_at'] = date('Y-m-d H:i:s');
						$git['status'] = 'Pending';
						DB::table('git')->insert($git);
				} 
				else 
				{
						
						$git['git_qty'] = $request->git_qty[$i];//
						$git['challan_no'] = $request->challan_no;
						$git['lorry_no'] = $request->lorry_no;
						$git['transport_no'] = $request->transport_no;
						$git['git_date'] = $request->date;
						$git['created_at'] = date('Y-m-d H:i:s');
						$git['updated_at'] = date('Y-m-d H:i:s');
						$git['status'] = 'Pending';
						DB::table('git')->insert($git);
				}

			
				
				
			}
		}
		
		
		return view('vendor.success-git',$git);
		
    }	
	
	public function generateInvoice($data='')
    {
		
		 $content = 'hhh';
		// $admission_letter_css = public_path().'/css/letter/letter.css';
		 $mypdf = new mPDF();
		 //$mypdf->WriteHTML(file_get_contents($admission_letter_css), 1);
		 $mypdf->WriteHTML($content, 2);
		 $mypdf->Output();
 
    	/*$pdf = app('dompdf.wrapper');
		$pdf = $pdf->loadView('vendor.git-invoice', $data);
		dd($pdf);
		return $pdf->stream();*/
	}
	
	public function downloadInvoice($data='')
    {
		$pdf = \PDF::loadView('vendor.git-invoice', $data);
		return $pdf->download('invoice.pdf');
	
	}
	
	
}
