<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Models\Section;
use DB;
use Redirect;

class AdminSectionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		if($this->userAccessibilityCheck('section-list'))
		{
			$list=Section::orderBy('id')->paginate(10);
        	return view('admin.section-list')->with('list',$list);
		}
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);
		    
        
    }
    public function getEditData(Request $request)
    {
        $id=$request->id;
        $data=Section::where('id','=', $id)->first();
        return $data;
    }
    
    public function addSection(Request $request)
    {
        $user=\Auth::user();
        $this->validate($request,[
        'name'=>'required|unique:sections',        
        'description'=>'required'
        ]);

        $name=trim($request->name);        
        $description=$request->description;        
        /*$createdby=$user->id;
        $modifiedby=$user->id;*/
        $createdby=3;
        $modifiedby=3;


        $section=new Section;
        $section->name=$name; 
        $section->description=$description;      
        $section->createdby=$createdby;
        $section->modifiedby=$modifiedby;
        if($section->save())
        {
            return redirect::route('section-listing')->withErrors(["New Section Added Successfully"]);
                  
        }
        else
        {
            return redirect::route('section-listing')->withErrors(["Error !!"]);
        }

    }

    public function updateSection(Request $request)
    {

        $user=\Auth::user();
        $this->validate($request,[
        'name'=>'required|unique:sections,name,'.$request->id,        
        'description'=>'required'
        ]);
        $id=$request->id;
        $name=trim($request->name);        
        $description=$request->description;        
/*        $modifiedby=$user->id;
*/        $modifiedby=3;

        $data=Section::where('id','=',$id)->first();
        $data->name=$name;
        $data->description=$description;
        $data->modifiedby=$modifiedby;
        if($data->save())
        {
            return redirect::route('section-listing')->withErrors(["Section Updated Successfully"]);
        }
        else
        {
            return redirect()->back()->withErrors(["Error !!"]);
        }

        /*$data=\DB::table('departments')->where('id','=',$id)->update(
            array(
                'name' => $name,                
                'description' => $description,                
                'modifiedby' => $modifiedby          
               
                ));*/
        
        
    }
    public function deleteSection(Request $request)
    {
		if($this->userAccessibilityCheck('section-delete'))
	 	{
		    $id=$request->id;
			$data=Section::where('id','=',$id);
			if($data->delete())
			{
				return redirect()->back()->withErrors(["Section Deleted Successfully"]);
			}
	 	}
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);
       
        
    }

    public function addSectionPage()
    {
        if($this->userAccessibilityCheck('section-add'))
		 	return view('admin.add-section');
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);
       
        
    }
    public function editSectionPage($id)
    {
		if($this->userAccessibilityCheck('section-edit'))
	 	{
		  $section=Section::where('id','=',$id)->first();
          return view('admin.edit-section')->with('section',$section);
	 	}
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);
       
        
    }
}
