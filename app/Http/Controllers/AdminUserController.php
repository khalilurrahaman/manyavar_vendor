<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\UserDetails;
use App\Models\Department;
use App\Models\Devision;
use App\Models\Role;
use Illuminate\Http\Request;
use Redirect;
use Auth;
use DB;
use Validator;

class AdminUserController extends Controller
{
    /**
     * Show the User Listing.
     */

    public function usetList()
    {
		
		if($this->userAccessibilityCheck('user-list'))
		{
			$users = User::selectRaw('id, name, department_id, email,mobile,address, createdby, createdby, status,devision_id')->where('type','=','admin')->where('id', '!=', 1)->orderBy('id', 'desc')->get();

			foreach ($users as $key=>$eachUser) {
				$users[$key]['dept_name']= Department::Select('name')->where('id','=',$eachUser->department_id)->first();
			}
			foreach ($users as $key=>$eachUser) {
				$users[$key]['user_details']= UserDetails::selectRaw('mobile, address')->where('userid','=',$eachUser->id)->first();
			}
			#dd($users);
			return view('admin.user-list')->with('userList',$users);
		}
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);
		    
    	
    }

    /**
     * View User Add page with Data.
     */

    public function userAdd(Request $request)
    {
		if($this->userAccessibilityCheck('user_add'))
		{
            $roleList = Role::where('status',1)->get();
			$devisionList = Devision::where('status','=',1)->get();
        	return view('admin.user')->with('roleList', $roleList)->with('devisionList', $devisionList);
		}
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);
        
    }

    public function postAddUser(Request $request)
    {
        $userdetails = User::all();
        foreach ($userdetails as $eachUser) {
            if($eachUser->email == $request->email){
                return \Redirect::back()->withErrors(['Email id already exist']);
            }
            if($eachUser->username == $request->user_name){
                return \Redirect::back()->withErrors(['User Name already exist']);
            }
        }
        //dd(implode(',',$request->division));
        $user = Auth::user();
        $users = new User;
        $users->type = 'admin';
        $users->role_id = $request->role_id;
        $users->username = $request->user_name;
        $users->name = $request->name;
        $users->department_id = 0;
        $users->section_id = 0;
        if(!empty($request->division))
        {
            $users->devision_id = implode(',',$request->division);
        }
        $users->email = $request->email;
        $users->password = md5($request->password);
        $users->createdby = $user->id;
        $users->modifiedby = $user->id;
        $users->mobile = $request->mobile_no;
        $users->address = $request->address;
        $users->status = 1;
        
        if($users->save()){
            /*$userdetails = new UserDetails;
            $userdetails->userid = $users->id;
            $userdetails->mobile = $request->mobile_no;
            $userdetails->address = $request->address;
            $userdetails->save();*/
        }

        return Redirect::route('user-listing')->withErrors(["New User Added Successfully"]);
    }

    public function getEditUser($user_id)
    {
		if($this->userAccessibilityCheck('edit_user'))
		{
			$users = DB::table('users')
            
            ->leftJoin('roles', 'users.role_id', '=', 'roles.id')
            ->where('users.id', '=',$user_id)
            ->first(['users.id', 'users.name', 'users.username','users.email','users.password','users.role_id','users.mobile','users.address','users.devision_id','roles.name as role_name']);

			$roleList = Role::where('status',1)->get();
			$devisionList = Devision::where('status','=',1)->get();
			return view('admin.edit-user')->with('users', $users)->with('roleList', $roleList)->with('devisionList', $devisionList);
		}
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);
        


    }

    public function updateUser(Request $request)
    {
        //dd($request);
        $authUser = Auth::user();
        if($request->email != $request->new_email ){
            $email = User::where('email','=',$request->new_email)->first();
            if(!empty($email)){
                return \Redirect::back()->withErrors(['Email id already exist']);
            }
        }
        if($request->username != $request->new_username ){
            $username = User::where('username','=',$request->new_username)->first();
            if(!empty($username)){
                return \Redirect::back()->withErrors(['Email id already exist']);
            }
        }

        $user = User::find($request->id);
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->role_id = $request->role_id;
        $user->modifiedby = $authUser->id;
        $user->mobile = $request->mobile;
        $user->address = $request->address;
        if(!empty($request->division))
        {
            $user->devision_id = implode(',',$request->division);
        }
        
        if($user->save()){
           /* $userdetails = UserDetails:: where('userid', '=', $user->id)->first();
            $userdetails->userid = $user->id;
            $userdetails->mobile = $request->mobile;
            $userdetails->address = $request->address;
            $userdetails->save();*/
        }
        return Redirect::route('user-listing')->withErrors(["User Updated Successfully"]);
    }

    public function deleteUser($id)
    {
		if($this->userAccessibilityCheck('delete_user'))
		{
			$user = User::where('id','=',$id)->delete();
        	return Redirect::route('user-listing')->withErrors(["User Deleted Successfully"]);
		}
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);
       
    }

    public function archiveUser($user_id)
    {
		if($this->userAccessibilityCheck('status_user'))
		{
			 $user = User::where('id', '=', $user_id)->update(['status'=>0]);
        	 return Redirect::route('user-listing')->withErrors(["User Inactivated Successfully"]);
		}
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);
			
       
    }

    public function activateUser($user_id)
    {
        if($this->userAccessibilityCheck('status_user'))
		{
			$user = User::where('id', '=', $user_id)->update(['status'=>1]);
        	return Redirect::route('user-listing')->withErrors(["User Activated Successfully"]);
		}
		else
			return Redirect::route('dashboard')->withErrors(['You have no access!']);
			
    }


    
    public function editProfile()
        {
            $user_id=Auth::User()->id;
            
            $data['userdata']=User::where('id','=',$user_id)->first();
            $data['user_data']=UserDetails::where('userid','=',$user_id)->first();
            //dd($userdata);
            return view('admin.edit-profile',$data);
        }
    public function updateProfile(Request $request)
    {
        //dd($request);
        Validator::extend('alpha_spaces', function($attribute, $value)
        {
            return preg_match('/^[\pL\s]+$/u',$value);
        });
        $messages = [
        'name.alpha_spaces' => 'The Name may only contain letters and spaces.',     
        ];

        $this->validate($request,[
        'name'=>'required|alpha_spaces',
        'username'=>'required',
        'email'=>'required|email|unique:users,email,'.$request->id,
        'mobile'=>'required',
        'address'=>'required',              
        ],$messages);

        $id=$request->id;
        $name=trim($request->name);
        $username=trim($request->username);
        $email=trim($request->email);        
        $mobile=$request->mobile;
        $address=$request->address;

        $profile_edit=User::where('id','=',$id)->first();
        $profile_edit->name=$name;
        $profile_edit->username=$username;
        $profile_edit->email=$email;
        $profile_edit->mobile=$mobile;
        $profile_edit->address=$address;
        
        if($profile_edit->save())
        {
            /*$vendor_details=UserDetails::where('userid','=',$id)->first();
            $vendor_details->mobile=$mobile;
            $vendor_details->address=$address;
            if($vendor_details->save())
            {*/

                return redirect::route('admin-profileedit-req')->withErrors('Profile Updated Successfully');

            /*}*/
        }
    
    }
    public function editPassword()
    {
         $user=Auth::User()->id;        
         return view('admin.edit-password')->with('userid',$user);
    }
    public function updatePassword(Request $request)
    {
        //dd($request);
        $this->validate($request,[
        'old_password'=>'required',        
        'new_password'=>'required|min:6',        
        'confirm_new_password'=>'required|min:6|same:new_password'
        ]);

        $id=$request->id;     
        $pass=Auth::User()->password;        
        $new_pass=md5($request->new_password); 
        $old_password=md5($request->old_password); 
		
		if(empty($request->new_password) || empty($request->confirm_new_password))
              return redirect::back()->withErrors(['Please Provide your desire password']);  
			  
	   if($request->new_password!=$request->confirm_new_password)
	  		 return redirect::back()->withErrors(['New password and Confirm New Password Mismatch']);  
			         
        if($old_password==$pass){
          $pass_edit=User::findOrFail($id);
          $pass_edit->password=$new_pass;
          
        try{
            $pass_edit->save();
            return redirect::route('admin-change-pass')->withErrors('User Password Changed Successfully');;
          
            }
        catch(Exception $e){}
          }
          else{
               return redirect::back()->withErrors(['Old password does not matched']);
;
          }      

    }
	
	public function userAvailability(Request $request)
    {
        $user=$request->user;
		$return=1;
        $user_availability =DB::table('users')->select('users.id')
		->where('users.username','=',$user)
		->get();
		if(!empty($user_availability) && count($user_availability)>0)
			$return=0;
		
        return $return;
    }

}
