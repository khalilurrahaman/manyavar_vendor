<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use DB;
use Carbon\Carbon;  

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Connection with Oracel Server.
     *
     * @var array
     */

    public static function oracleQuery($sql) 
    {
        ini_set('memory_limit', '20000M');
        ini_set('max_execution_time', 600);
        $conn = oci_connect('ers$web', 'gmpl', '223.30.114.68:1521/SANSAR');
        if (!$conn) {   
            return false; 
        }

        $Oracle_query = oci_parse($conn, $sql);
        oci_execute($Oracle_query);
        $result=array();
        while ($row = oci_fetch_array($Oracle_query, OCI_ASSOC+OCI_RETURN_NULLS)) {                       
            $result[]=$row;
        }

        oci_close($conn);

        return $result;
    }

    
    public static function user_authentication($credentials)
    {
        //dd($credentials['email']);
        $user = new User; 
        $user = $user->where('username','=',$credentials['email'])->where('password','=',md5($credentials['password']))->first(); 
        //dd($user);
        if(!empty($user))
        {
            if(Auth::attempt($credentials))
            {
                return true;
            } 
            else
            {
                return false;
            }
        }
        else
        {
             return false;

        }
    }

    //Get User Id from email id
    public static function get_user_id($useremail){
        $user = new User;
        $user = $user->where('username','=',$useremail)->where('status','=',1)->first();  
        if($user){
            return $user->id;
        } 
        else{
          return 0;
        }
    }

    //Update user by access token
    public static function update_userPass($accessTocken,$password){
       
        $user = User::where('remember_token', '=', $accessTocken)->where('status','=',1)->first();
        if(count($user)>0){
            $user->password = md5($password);
            $user->save();
            return true;
        } 
        else{
          return 0;
        }
    }

    //Get User Data (email)
    public static function get_user_data($useremail){
      
        $user = new User;
        $user = $user->where('username','=',$useremail)->where('status','=', 1)->first();  
        
        return User::get_user_info($user);
    }

    //Check Access Token Valide or not
    public static function check_access_token($accessTocken){
        $user = new User;
        $user = $user->where('remember_token','=',$accessTocken)->where('status','=',1)->first();  
        if($user){
            return true;
        } 
        else{
            return false;
        }
    }
    //Get User Data (Accesstoken)
    public static function get_user_info_from_accesstoken($accesstoken){
        $user = User::where('remember_token','=',$accesstoken)->where('status','=',1)->first();  
        return User::get_user_info($user);
    }

    //Get User Data
    public static function get_user_info($user){
        $userData=[];
        if($user){
          $userData['userid'] = $user->id;
          $userData['fullName'] = $user->name;
          $userData['access_token'] = $user->remember_token;
        }
        return $userData;   
    }

    //Update User Info
    public static function update_user($userId){
        
        $user = User::where('id','=',$userId)->first();
        if(count($user)>0){
            $user->updated_at = date('Y-m-d H:i:s');
            $user->save();
            return true;
        } 
        else{
            return false;
        }                          
    }

    //Get User Id from Access token
    public static function get_user_id_from_accesstoken($accesstoken){
        $user = User::where('remember_token','=',$accesstoken)->where('status','=',1)->first();  
        if($user){
            return $user->id;
        } 
        else{
          return 0;
        }
    }

    public static function new_poList($id,$currentPage)
    {
        //dd($id);
        $num_rec_per_page=10;

        if(!empty($currentPage)) { 

            $page  = intval($currentPage);

        } 
        else { 

            $page=1; 
        }
        if($page<1)
        {
            $page=1;
        }

        $start_from = ($page-1) * $num_rec_per_page;

        $user = User::where('id','=',$id)->first();
        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.id as poId','purchase_order.ORDNO', 'purchase_order.PONO as PONO', 'purchase_order.ORDDT','purchase_order.TIME','purchase_order_delivery.DUEDATE','V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_Fincharge.CHGNAME', 'purchase_order.GRSAMT', 'purchase_order.CHGAMT', 'purchase_order.NETAMT','V_PURORDDET.CNLQTY', DB::raw('sum(V_PURORDDET.RATE * V_PURORDDET.ORDQTY) as total'))
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('v_purordchg', 'purchase_order.ORDCODE', '=', 'v_purordchg.ORDCODE')
        ->leftJoin('V_Fincharge', 'v_purordchg.CHGCODE', '=', 'V_Fincharge.CHGCODE')
        ->leftJoin('purchase_order_delivery', 'purchase_order_delivery.PURORDDET_CODE', '=', 'V_PURORDDET.CODE')
        ->where('purchase_order.status', '=','NEW')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
        ->groupBy('purchase_order.ORDCODE')
        ->orderBy('purchase_order_delivery.DUEDATE', 'asc')
        ->skip($start_from)->take($num_rec_per_page)
        ->get();
        $poData = array();
        foreach ($purchaseOrderList as $key=>$eachPo) {
            $poData[$key]['poId']=$eachPo->poId;
            $poData[$key]['ORDNO']=$eachPo->ORDNO;
            $poData[$key]['PONO']=$eachPo->PONO;
            $poData[$key]['ORDDT']= Carbon::parse($eachPo->ORDDT)->format('d/m/Y');
            $poData[$key]['NETAMT']=$eachPo->NETAMT;
        }
        return $poData;
    }


    public static function getPoDetails($id,$poId)
    {
        //dd($id);
        $user = User::where('id','=',$id)->first();

        $purchaseOrderList = DB::table('purchase_order')->select('purchase_order.id', 'purchase_order.ORDNO', 'purchase_order.PONO as PONO','purchase_order.DOCNO','purchase_order.ORDDT','purchase_order.TIME', 'products.cat1', 'products.cat2', 'products.cat3', 'products.cat4',  'products.CNAME6','V_PURORDDET.RATE', 'V_PURORDDET.ORDQTY','V_PURORDDET.RCQTY','V_PURORDDET.CNLQTY','v_purordchg.ISTAX','v_purordchg.RATE as disc_rate','V_Fincharge.CHGNAME', 'purchase_order.GRSAMT', 'purchase_order.CHGAMT', 'purchase_order.NETAMT', 'purchase_order_delivery.DUEDATE')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')                              
        ->leftJoin('v_purordchg', 'purchase_order.ORDCODE', '=', 'v_purordchg.ORDCODE')
        ->leftJoin('V_Fincharge', 'v_purordchg.CHGCODE', '=', 'V_Fincharge.CHGCODE')
        ->leftJoin('products', 'V_PURORDDET.ICODE', '=', 'products.ICODE')
        ->leftJoin('purchase_order_delivery', 'V_PURORDDET.CODE', '=', 'purchase_order_delivery.PURORDDET_CODE')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
        ->where('purchase_order.id', '=',$poId)
        ->orderBy('purchase_order.ORDDT', 'desc')
        ->get();

        $poDetailsMsg = array();
        $poDetailsMsg=false;
        if(!empty($purchaseOrderList) && count($purchaseOrderList)>0)
        $poDetailsMsg=true;

        $poDetailsData = array();
        if(!empty( $purchaseOrderList[0]))
        {
            $poDetailsData['document_no']=!empty($purchaseOrderList[0]->DOCNO)? $purchaseOrderList[0]->DOCNO :"";
            $poDetailsData['ValidItyPeriod']=Carbon::parse($purchaseOrderList[0]->DUEDATE)->format('d/m/Y');
            $poDetailsData['valid_to']='';
            $poDetailsData['credit_days']='30';
            $poDetailsData['currency']='INR';
            $poDetailsData['exchange_rate']='';
        }
        else
        {
            $poDetailsData=(object)[];
        }
        $poListData = array();
        foreach ($purchaseOrderList as $key=>$eachPo) {
            $poListData[$key]['design']=$eachPo->cat1;
            $poListData[$key]['colour']=$eachPo->cat2;
            $poListData[$key]['cat_3']=$eachPo->cat3;
            $poListData[$key]['width_size']= $eachPo->cat4;
            $poListData[$key]['cat_6']=!empty($eachPo->CNAME6)? $eachPo->CNAME6 :"";
            $poListData[$key]['due_date']=Carbon::parse($eachPo->DUEDATE)->format('d/m/Y');
            $poListData[$key]['qty']=$eachPo->ORDQTY;
            $poListData[$key]['rate']=$eachPo->RATE;
            $poListData[$key]['amount']=$eachPo->ORDQTY*$eachPo->RATE;
        }
        $poDetailsCalData = array();
        if(!empty( $purchaseOrderList[0]))
        {
            
            $poDetailsCalData['total']=$purchaseOrderList[0]->GRSAMT;
            $poDetailsCalData['discount']=$purchaseOrderList[0]->CHGAMT;
            $poDetailsCalData['net_amount']=$purchaseOrderList[0]->NETAMT;
        }
        else
        {
            $poDetailsCalData=(object)[];
        }
        

        $poDetailsFinalDataArray = array();
        $poDetailsFinalDataArray['success'] = $poDetailsMsg;
        $poDetailsFinalDataArray['details'] = $poDetailsData;
        $poDetailsFinalDataArray['details_list'] = $poListData;
        $poDetailsFinalDataArray['calculations'] = $poDetailsCalData;
        #dd($poDetailsFinalDataArray);
        return $poDetailsFinalDataArray;
    }

    public static function totalPo_count($id,$currentPage){
        $user = User::where('id','=',$id)->first();
        $orderCount = DB::table('purchase_order')
        ->join('V_PURORDDET','purchase_order.ORDCODE', '=', 'V_PURORDDET.ORDCODE')
        ->leftJoin('v_purordchg', 'purchase_order.ORDCODE', '=', 'v_purordchg.ORDCODE')
        ->leftJoin('V_Fincharge', 'v_purordchg.CHGCODE', '=', 'V_Fincharge.CHGCODE')
        ->leftJoin('purchase_order_delivery', 'purchase_order_delivery.PURORDDET_CODE', '=', 'V_PURORDDET.CODE')
        ->where('purchase_order.status', '=','NEW')
        ->where('purchase_order.PCODE', '=',$user->SLCODE)
        ->groupBy('purchase_order.ORDCODE')->get();
        //dd(ceil(count($orderCount)/10));
        return ceil(count($orderCount)/10);
    }


 public static function poStatus($poId,$accepted,$remarks='')
    {
       $return=false;
       $status='REJECT';
       if($accepted)
       $status='OPEN';

       if(DB::table('purchase_order')->where('purchase_order.id', '=',$poId)->update(['status'=>$status,'remarks'=>$remarks,'updated_at'=>date('Y-m-d H:i:s')]))
       {
            $ORDCODE = DB::table('purchase_order')->select('ORDCODE')->where('purchase_order.id', '=',$poId)->first();
            DB::table('V_PURORDDET')->where('V_PURORDDET.ORDCODE','=',$ORDCODE->ORDCODE)->update(['STATUS'=>$status,'updated_at'=>date('Y-m-d H:i:s')]);
            $return=true;
       }
        return $return;
    }

    public static function getMailConfig() 
    {
        $config=array();

        $config['Password'] = "wF7B2rAP";
        $config['Username'] = "sansar@manyavar.com";         
        $config['reply_to'] = "sansar@manyavar.com";
        $config['smtp']     = "mail.manyavar.com";//"smtp.gmail.com";
        
        return $config;
    }  

    public static function sendPush($device_token,$device_type,$device_id,$msg,$title,$token_id/*$msg='', $badge=0*/) {
        $push= new User;
        //dd($device_type);
        //$device_type = 'android';
        if ($msg != null && $device_token!=null && $device_token!='(null)' && $device_type!=null) 
        {

            if ($device_type == 'android') 
            { 
                //$push->pushNotice($device_id,$msg,$title,$token_id);

            }
            else if($device_type == 'ios')
            {
                $push->iosPush($device_id,$msg,$title,$token_id);
            }
        }     
    }


    static function pushNotice($android,$message,$title,$token_id,$noMessage=false)
    {
        //dd($token_id);
        $data = array('title'=>$title,'message'=>$message);
        //dd('swapan');
        $target = array($token_id); 
        //FCM api URL
        $url = 'https://fcm.googleapis.com/fcm/send';
        //api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
        $server_key = 'AAAAo0YvqtA:APA91bE84IpY9szL-zRMIvMhkh4yJxH87_7Qh80liAFYTi9nuC5mBz0ECFh5Rd8pBhNez-14ix5bOXgvcZPyCx2PrNeGReR3bW-Km-FV7Pi0ApETwo4vc0PedMNM48HLufVfJ-KNrOdJ';
                    
        $fields = array();
        $fields['data'] = $data;
        if(is_array($target)){
            $fields['registration_ids'] = $target;
        }else{
            $fields['to'] = $target;
        }
        //header with content_type api key
        $headers = array(
            'Content-Type:application/json',
          'Authorization:key='.$server_key
        );
                    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        //echo $result;
        if ($result === FALSE) {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
        //echo $result;
        
    }

    static function iosPush($device_id,$message,$title,$token_id,$noMessage=false)
    {   
        //dd($token_id);
        $ch = curl_init("https://fcm.googleapis.com/fcm/send");
        $key = "AAAA_x8txMw:APA91bG_f-QnrRKGRl0p6Oq6rhmsSznQULoI7DbcaNJzyJ0dnJf4cQCSsKqFysO7P83j1995Yzw_cxuvoEC1lbWCmw5ghicnsKGPftchSjn42pGQx4hHXcP-2eCd2dUDX6Tb7Oh8d28N";
        //print_r($ios);
        //The device token.
        $token = array($token_id); //token here

        //Title of the Notification.
        $title = $title;

        //Body of the Notification.
        $body = $message;

        //Creating the notification array.
        $notification = array('title' =>$title , 'text' => $body);

        //This array contains, the token and the notification. The 'to' attribute stores the token.
        $arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high');

        //Generating JSON encoded string form the above array.
        $json = json_encode($arrayToSend);
        //dd($json);
        //Setup headers:
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key= $key'; // key here

        //Setup curl, add headers and post parameters.
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);       

        //Send the request
        $response = curl_exec($ch);
        echo $response;
        //Close request
        curl_close($ch);
        return $response;
    }

    /*static function iosPush($deviceToken,$msg="Hello Testing !!",$badge=0,$showMsg=false)
    {   
        //print_r("swapan");exit;
        // Put your device token here (without spaces):
        //$ios = array("229adf5e4586ded157cb0e3bef480e16f0c72d67fcbadbbb7651a1be2d7d7374","d6d16e9ae9124e1f24a31849e892cd986ecd6b4a40cca59c7bcb480da509f6d1");
        
        //foreach ($ios as $deviceToken) {
            
        // Put your private key's passphrase here:
            if($deviceToken!='(null)' && !empty($deviceToken))
            {
                $passphrase = 'count';

                // Put your alert message here:
                $message = $msg;

                ////////////////////////////////////////////////////////////////////////////////
                //$dir = dirname(__FILE__);
                $ctx = stream_context_create();
                stream_context_set_option($ctx, 'ssl', 'local_cert', '/var/www/html/count_new/public/countDevPushCertificates.pem');
                stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

                // Open a connection to the APNS server
                $fp = stream_socket_client(
                    'ssl://gateway.sandbox.push.apple.com:2195', $err,
                    //'ssl://gateway.push.apple.com:2195', $err,
                    $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

                if (!$fp)
                    exit("Failed to connect: $err $errstr" . PHP_EOL);

                // Create the payload body
                $body['aps'] = array(
                    'alert' => $message,
                    'sound' => 'default'
                    );

                // Encode the payload as JSON
                $payload = json_encode($body);

                // Build the binary notification
                $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

                // Send it to the server
                $result = fwrite($fp, $msg, strlen($msg));
        
                //$lineForLog = ':: iOS :: Message successfully delivered to token '.$deviceToken . PHP_EOL;
                
                // Close the connection to the server
                fclose($fp);
               
            }
        //}
    }*/



}
