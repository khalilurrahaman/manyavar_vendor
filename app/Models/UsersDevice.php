<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UsersDevice extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'usersdevice';
 
	//Add new device
	public static function addDevice($userId,$deviceId,$deviceType)
	{
		$device = New UsersDevice;
		$device->user_id = $userId;
		$device->device_id = $deviceId;
		$device->status = 1;
		$device->device_type = $deviceType;
		$device->device_last_active_time = date('Y-m-d H:i:s');
		if($device->save()){
           return true;
		}
		else{
           return false;
		}
	}
	//Find Device Id
	public static function findDeviceId($userId){
		 $device = New UsersDevice;
	     $device = $device->where('user_id','=',$userId)->first();
	     if($device){
            return true;
	     }  
	     else{
            return false;
	     }               		
	}
	//Update Login info
	public static function updateLoginInfo($userId,$deviceId,$deviceType,$token_id){
	 	$device = New UsersDevice;
     	$device = $device->where('user_id','=',$userId)->first();
     	//dd($device);
	    if(count($device)>0){
    		//$device->status =1;
    		$device->device_id = $deviceId;
    		$device->device_type = $deviceType;
    		$device->token_id = $token_id;
          	$device->device_last_active_time = date('Y-m-d H:i:s');
	    }                
		if($device->save()){
           return true;
		}
		else{
           return false;
		}                      
	}   
}
