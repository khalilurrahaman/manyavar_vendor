<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
	protected $table='userdetails';
    protected $fillable=['id','userid','mobile','address','created_at','updated_at'];
}
