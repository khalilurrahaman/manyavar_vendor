<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Devision extends Model
{
    protected $table='devision';
    protected $fillable=['id','name','description','status','createdby','modifiedby','created_at','updated_at'];
}
