<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
	protected $table='document';
    protected $fillable=['id','doc_type','vendor_id','month','remarks','created_by','updated_by','created_at','updated_at','status'];


  public function getDocType()
  {
    return $this->belongsTo('App\Models\DocumentType','doc_type','id');
  }
  public function getDocVendor()
  {
    return $this->belongsTo('App\User','vendor_id','id');
  }
  public function getDocFile()
 	{
    	return $this->belongsTo('App\Models\DocumentFiles','id','document_id');
  	}
}
