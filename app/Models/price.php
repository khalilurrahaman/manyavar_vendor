<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class price extends Model
{
    protected $table='prices';
    public function getVendorByPrice()
  	{
    
    	return $this->belongsTo('App\User','PCODE','SLCODE');
  	}
    public function getItemByPrice()
  	{
    
    	return $this->belongsTo('App\Models\Product','ICODE','ICODE');
  	}
}

 
