<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
	protected $table='sections';
    protected $fillable=['id','name','description','status','createdby','modifiedby','created_at','updated_at'];
}
