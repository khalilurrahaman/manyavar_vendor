<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentFiles extends Model
{
	protected $table='document_files';
    protected $fillable=['id','document_id','document_file','created_by','updated_by','created_at','updated_at'];

    
}
