<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use URL;
use File;

class Api extends Model
{		
	//Check Device Type
	public static function is_device_type_valide($deviceType){
		
		if($deviceType =='android' || $deviceType =='ios'){
           return true;
		}
		else{
		   return false;
		}
	}
	//Check Device Type
	public static function error_message($deviceType){
		
		/*if(){
           return true;
		}
		else{
		   return false;
		}*/
	}
	//Check name format valide or not
	public static function is_name_valide($name=''){
	     if(preg_match("/^[a-zA-Z ]*$/",$name)){
	       return true;
	     }
	     else{
	     	return false;
	     }
	}	
	//Check Email format valide or not
	public static function is_email_valide($email=''){
		
         if(filter_var($email, FILTER_VALIDATE_EMAIL)){
           return true;
         }
         else{
         	return false;
         }
	}
	//Check Mobile no format valide or not
	public static function is_mob_no_valide($mobileNo=''){

       if($mobileNo){
         if(preg_match('/^\d{10}$/',$mobileNo)){
           return true;
         }
         else{
         	return false;
         }
       }
       else{
       	 return false;
       }
	}
	//Check required fields of an api request
	public static function is_request_json_valide(array $requiredParams,$postRequest){

		
		if($postRequest){
          $postRequest = json_decode(json_encode($postRequest), true);  //Object to array
		}
		else{
          $postRequest = [];
		}

		$allKeysOfPostRequest = Api::array_keys_multi($postRequest,false);

		foreach ($requiredParams as $key => $requiredParam) {

			if(!in_array($requiredParam, $allKeysOfPostRequest)){
				return false;
			}
		}
        return true;
	}
	//Check empty fields of an array
	public static function check_empty(array $notEmptyParams,$postRequest){

		if($postRequest){
          $postRequest = json_decode(json_encode($postRequest), true);  //Object to array
		}
		else{
          $postRequest = [];
		}
		$response=[];
		$allKeysOfPostRequest = Api::array_keys_multi($postRequest,true);
		foreach ($notEmptyParams as $key => $notEmptyParam) {

			if(in_array($notEmptyParam, $allKeysOfPostRequest)){
				$response['status'] = false;
				$response['msg'] = $notEmptyParam.' field can not be empty.';
				return $response;
			}
		}
		$response['status'] = true;
		$response['msg'] = 'no error';
        return $response;		
	}
	//Get All Keys From a multidimantion array
    public static function array_keys_multi(array $array,$checkEmpty = false){
        
        $keys = array();
        foreach ($array as $key =>$value) {

        	if($checkEmpty){
               if(empty($value)){
                 $keys[] = $key;
               }
        	}
        	else{
        		$keys[] = $key;
        	}
            
            if (is_array($value)) {
                $keys = array_merge($keys,Api::array_keys_multi($value,$checkEmpty));

            }
        }
        
        return $keys;
    }
}
