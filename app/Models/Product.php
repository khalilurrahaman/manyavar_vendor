<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table='products';
    protected $fillable=['id','cat1','cat2','cat3','cat4','image','createdby','modifiedby','created_at','updated_at'];
	
   
   public static function oracleQuery($sql)/*Connection for database shrink By Khalilur.*/
   {
       ini_set('memory_limit', '20000M');
       ini_set('max_execution_time', 600);
       $conn = oci_connect('ers$web', 'gmpl', '223.30.114.68:1521/SANSAR');
       if (!$conn) {  
           return false;
       }

       $Oracle_query = oci_parse($conn, $sql);
       oci_execute($Oracle_query);
       $result=array();
       while ($row = oci_fetch_array($Oracle_query, OCI_ASSOC+OCI_RETURN_NULLS)) {                      
           $result[]=$row;
       }

       oci_close($conn);

       return $result;
   }  
}
