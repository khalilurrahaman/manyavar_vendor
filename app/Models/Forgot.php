<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Forgot extends Model
{
    protected $table='forgot_password';
    protected $fillable=['id','email','old_password','token','created_at','updated_at'];
}
