<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Quetes extends Model
{
	 protected $table = 'rfq_quetes';

	 public function quotationCommend()
	 {
	 	return $this->hasMany('App\Comments', 'quete_id', 'id')->orderBy('id', 'desc');
	 }
}


