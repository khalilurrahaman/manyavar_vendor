<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentType extends Model
{
	protected $table='document_type_master';
    protected $fillable=['id','type_name','descricription','status','created_by','updated_by','created_at','updated_at'];
}
