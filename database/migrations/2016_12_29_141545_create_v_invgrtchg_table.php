<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVInvgrtchgTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('V_INVGRTCHG', function (Blueprint $table) {
            $table->increments('id');
            $table->string('GRTCODE')->nullable();
            $table->string('SEQ')->nullable();
            $table->string('CHGCODE')->nullable();
            $table->double('RATE')->nullable();
            $table->string('SIGN')->nullable();
            $table->double('CHGAMT')->nullable();
            $table->string('WITHOUT_TERM_FORMULA')->nullable();
            $table->string('BASIS')->nullable();
            $table->string('APPAMT')->nullable();
            $table->string('FORMULAE')->nullable();
            $table->string('OPERATION_LEVEL')->nullable();
            $table->string('ISTAX')->nullable();
            $table->string('SOURCE')->nullable();
            $table->string('CODE')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('V_INVGRTCHG');
    }
}
