<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatrVPURORDDETTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('V_PURORDDET', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ORDCODE')->nullable();
            $table->string('ICODE')->nullable();
            $table->string('ORDQTY')->nullable();
            $table->double('RATE')->nullable();
            $table->string('RCQTY')->nullable();
            $table->string('OQTY')->nullable();
            $table->longText('REM')->nullable();
            $table->string('CODE')->nullable();
            $table->double('DISCOUNT')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('V_PURORDDET');
    }
}
