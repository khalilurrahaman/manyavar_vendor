<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('CODE');
            $table->string('PCODE');
            $table->string('ICODE');
            $table->date('EFFECTIVE_DATE');
            $table->double('RATE');
            $table->integer('DELIVERY_DAYS');
            $table->string('EXTINCT');
            $table->integer('ECODE');
            $table->date('TIME');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prices');
    }
}
