<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVINVITEMTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('V_INVITEM', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ICODE')->nullable();
            $table->string('GRPCODE')->nullable();
            $table->string('DIVISION')->nullable();
            $table->string('SECTION')->nullable();
            $table->string('DEPARTMENT')->nullable();
            $table->string('CCODE1')->nullable();
            $table->string('CCODE2')->nullable();
            $table->string('CCODE3')->nullable();
            $table->string('CCODE4')->nullable();
            $table->string('CCODE5')->nullable();
            $table->string('CCODE6')->nullable();
            $table->string('CNAME1')->nullable();
            $table->string('CNAME2')->nullable();
            $table->string('CNAME3')->nullable();
            $table->string('CNAME4')->nullable();
            $table->string('CNAME5')->nullable();
            $table->string('CNAME6')->nullable();
            $table->double('RATE')->nullable();
            $table->double('MRP')->nullable();
            $table->string('BARRQ')->nullable();
            $table->string('BARUNIT')->nullable();
            $table->longText('REM')->nullable();
            $table->string('PARTYCODE')->nullable();
            $table->string('PARTYNAME')->nullable();
            $table->date('GENERATED')->nullable();
            $table->date('LAST_CHANGED')->nullable();
            $table->date('EXPIRY_DATE')->nullable();
            $table->string('GRCCODE')->nullable();
            $table->double('LISTED_MRP')->nullable();
            $table->string('ITEM_NAME')->nullable();
            $table->string('DESC1')->nullable();
            $table->string('DESC2')->nullable();
            $table->string('DESC3')->nullable();
            $table->string('DESC4')->nullable();
            $table->string('DESC5')->nullable();
            $table->string('DESC6')->nullable();
            $table->string('NUM1')->nullable();
            $table->string('NUM2')->nullable();
            $table->string('NUM3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('V_INVITEM');
    }
}
