<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVPurordchgTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('v_purordchg', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ORDCODE')->nullable();
            $table->string('CHGCODE')->nullable();
            $table->double('RATE')->nullable();
            $table->string('CODE')->nullable();
            $table->string('SOURCE')->nullable();
            $table->string('ISTAX')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('v_purordchg');
    }
}
