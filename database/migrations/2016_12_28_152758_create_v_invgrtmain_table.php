<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVInvgrtmainTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('V_INVGRTMAIN', function (Blueprint $table) {
            $table->increments('id');
            $table->string('GRTCODE')->nullable();
            $table->string('GRTNO')->nullable();
            $table->date('GRTDT')->nullable();
            $table->string('PCODE')->nullable();
            $table->string('GRCCODE')->nullable();
            $table->string('LOCCODE')->nullable();
            $table->string('RTCODE')->nullable();
            $table->string('GENO')->nullable();
            $table->longText('REM')->nullable();
            $table->string('YCODE')->nullable();
            $table->string('ECODE')->nullable();
            $table->date('TIME')->nullable();
            $table->date('LAST_ACCESS_TIME')->nullable();
            $table->string('LAST_ACCESS_ECODE')->nullable();
            $table->string('TAXCHGPER')->nullable();
            $table->string('DISCHGPER')->nullable();
            $table->string('AGCODE')->nullable();
            $table->string('ADMOU_CODE')->nullable();
            $table->string('FINTRADEGRP_CODE')->nullable();
            $table->string('DOCCODE')->nullable();
            $table->string('SCHEME_DOCNO')->nullable();
            $table->string('GRSAMT')->nullable();
            $table->double('CHGAMT')->nullable();
            $table->double('NETAMT')->nullable();
            $table->string('ADMCURRENCY_CODE')->nullable();
            $table->string('EXRATE')->nullable();
            $table->string('ADMSITE_CODE')->nullable();
            $table->string('AUTH_ECODE')->nullable();
            $table->date('AUTH_TIME')->nullable();
            $table->string('PSITE_STOCKPOINT_CODE')->nullable();
            $table->string('FORMCODE')->nullable();
            $table->string('INTGCODE')->nullable();
            $table->string('PURTERMCODE')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('V_INVGRTMAIN');
    }
}
