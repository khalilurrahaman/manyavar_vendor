<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatrVPURRTDETTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('V_PURRTDET', function (Blueprint $table) {
            $table->increments('id');
            $table->string('RTCODE')->nullable();
            $table->string('ICODE')->nullable();
            $table->double('QTY')->nullable();
            $table->double('RATE')->nullable();
            $table->double('NETAMT')->nullable();
            $table->longText('REM')->nullable();
            $table->string('GRTCODE')->nullable();
            $table->string('CODE')->nullable();
            $table->string('EX_BASIS')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('V_PURRTDET');
    }
}
