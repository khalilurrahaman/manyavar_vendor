<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrderReturnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order_return', function (Blueprint $table) {
            $table->increments('id');
            $table->string('RTCODE')->nullable();
            $table->string('RTNO')->nullable();
            $table->date('RTDT');
            $table->string('PCODE')->nullable();
            $table->string('DOCNO')->nullable();
            $table->string('AGCODE')->nullable();
            $table->double('AGRATE')->nullable();
            $table->date('TIME')->nullable();
            $table->string('REM')->nullable();
            $table->date('LAST_ACCESS_TIME');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_order_return');
    }
}
