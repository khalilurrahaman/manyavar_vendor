<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsReturnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goods_returns', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->nullable();

            $table->integer('createdby')->unsigned()->index();
            $table->foreign('createdby')->references('id')->on('users')->onDelete('cascade');
            $table->integer('modifiedby')->unsigned()->index();
            $table->foreign('modifiedby')->references('id')->on('users')->onDelete('cascade');
            $table->string('prn_no')->nullable();
            $table->date('pra_date');
            $table->string('courier_name')->nullable();
            $table->integer('status')->default(1);
            $table->longText('comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goods_returns');
    }
}
