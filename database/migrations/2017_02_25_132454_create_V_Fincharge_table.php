<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVFinchargeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('V_Fincharge', function (Blueprint $table) {
            $table->increments('id');
            $table->string('CHGCODE')->nullable();
            $table->string('CHGNAME')->nullable();
            $table->string('GLCODE')->nullable();
            $table->double('RATE')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('V_Fincharge');
    }
}
