<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRfqQuetesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rfq_quetes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rfq_id')->unsigned()->index();
            $table->foreign('rfq_id')->references('id')->on('rfqs')->onDelete('cascade');                       
            $table->integer('vendor_id');
            $table->double('price')->default(0);
            $table->integer('width')->default(0);
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rfq_quetes');
    }
}
