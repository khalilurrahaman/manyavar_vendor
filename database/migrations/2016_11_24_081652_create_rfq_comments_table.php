<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRfqCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rfq_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quete_id')->unsigned()->index();
            $table->foreign('quete_id')->references('id')->on('rfq_quetes');                       
            $table->integer('user_id');
            $table->longText('comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rfq_comments');
    }
}
