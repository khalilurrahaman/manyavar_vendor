<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseVFinshTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('v_finsh', function (Blueprint $table) {
            $table->increments('id');
            $table->string('SLCODE')->nullable();
            $table->longText('SLNAME')->nullable();
            $table->longText('ABBRE')->nullable();
            $table->longText('BADDR')->nullable();
            $table->longText('BDIST')->nullable();
            $table->longText('BCTNAME')->nullable();
            $table->string('BPIN')->nullable();
            $table->string('BPH1')->nullable();
            $table->string('BPH2')->nullable();
            $table->string('BFX1')->nullable();
            $table->string('BEMAIL')->nullable();
            $table->string('BWEBSITE')->nullable();
            $table->string('LSTNO')->nullable();
            $table->string('CSTNO')->nullable();
            $table->string('PAN')->nullable();
            $table->longText('REM')->nullable();
            $table->string('CLSCODE')->nullable();
            $table->longText('SDIST')->nullable();
            $table->longText('SCTNAME')->nullable();
            $table->string('SPIN')->nullable();
            $table->bigInteger('SPH1')->nullable();
            $table->bigInteger('SPH2')->nullable();
            $table->bigInteger('SPH3')->nullable();
            $table->bigInteger('SPH4')->nullable();
            $table->bigInteger('SFX1')->nullable();
            $table->bigInteger('SFX2')->nullable();
            $table->string('SEMAIL')->nullable();
            $table->longText('SWEBSITE')->nullable();
            $table->longText('BRAND')->nullable();
            $table->longText('PRODUCT')->nullable();
            $table->longText('COMPANYTYPE')->nullable();
            $table->longText('INDUSTRYTYPE')->nullable();
            $table->date('CSTDATE')->nullable();
            $table->date('LSTDATE')->nullable();
            $table->longText('BANKNAME')->nullable();
            $table->longText('BANKACCOUNTNO')->nullable();
            $table->longText('BANKIFCCODE')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('v_finsh');
    }
}
