<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRfqDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rfq_details', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('rfq_id')->unsigned()->index();
            $table->foreign('rfq_id')->references('id')->on('rfqs')->onDelete('cascade');                       
            $table->integer('vendor_id');
            $table->string('shipment_provider')->nullable();
            $table->string('docket_no')->nullable();
            $table->integer('createdby')->unsigned()->index();
            $table->foreign('createdby')->references('id')->on('users');
            $table->integer('modifiedby')->unsigned()->index();
            $table->foreign('modifiedby')->references('id')->on('users');
            $table->integer('status')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rfq_details');
    }
}
