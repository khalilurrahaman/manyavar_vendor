<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseReturnChangeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_return_change', function (Blueprint $table) {
            $table->increments('id');
            $table->string('RTCODE')->nullable();
            $table->string('RTNO')->nullable();
            $table->date('RTDT');
            $table->string('CHGCODE')->nullable();
            $table->double('RATE')->nullable();
            $table->string('FORMCODE')->nullable();
            $table->string('FORMNO')->nullable();
            $table->date('FORMDT');
            $table->string('BASIS')->nullable();
            $table->string('SOURCE')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_return_change');
    }
}
