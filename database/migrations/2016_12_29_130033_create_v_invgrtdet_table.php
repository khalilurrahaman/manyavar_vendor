<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVInvgrtdetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('V_INVGRTDET', function (Blueprint $table) {
            $table->increments('id');
            $table->string('GRTCODE')->nullable();
            $table->string('GRTNO')->nullable();
            $table->date('GRTDT')->nullable();
            $table->string('GRCCODE')->nullable();
            $table->string('LOCCODE')->nullable();
            $table->string('ICODE')->nullable();
            $table->double('QTY')->nullable();
            $table->double('RATE')->nullable();
            $table->longText('REM')->nullable();
            $table->string('YCODE')->nullable();
            $table->double('RTINVQTY')->nullable();
            $table->double('GRSAMT')->nullable();
            $table->double('DISCOUNT')->nullable();
            $table->double('TAXAMT')->nullable();
            $table->double('NETAMT')->nullable();
            $table->string('TAXCHGPER')->nullable();
            $table->string('DISCHGPER')->nullable();
            $table->double('XGRSAMT')->nullable();
            $table->double('XDISCOUNT')->nullable();
            $table->double('XTAXAMT')->nullable();
            $table->double('XNETAMT')->nullable();
            $table->string('CODE')->nullable();
            $table->double('CHGAMT')->nullable();
            $table->double('EFFAMT')->nullable();
            $table->string('INVGRCDET_CODE')->nullable();
            $table->string('EXCISEMAIN_CODE')->nullable();
            $table->string('EX_BASIS')->nullable();
            $table->string('EX_EFFRATE')->nullable();
            $table->string('EX_ABTFACTOR')->nullable();
            $table->string('EX_DUTYFACTOR')->nullable();
            $table->string('EX_CESSFACTOR')->nullable();
            $table->string('EX_ROUNDOFF')->nullable();
            $table->string('EX_APPAMT')->nullable();
            $table->string('EX_DUTYAMT')->nullable();
            $table->string('EX_CESSAMT')->nullable();
            $table->string('RSP')->nullable();
            $table->string('EX_APPLICABLE_FROM')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('V_INVGRTDET');
    }
}
