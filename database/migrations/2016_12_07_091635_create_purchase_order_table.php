<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ORDCODE');
            $table->string('ORDNO');
            $table->date('ORDDT');
            $table->string('PCODE');
            $table->string('DOCNO');
            $table->string('STAT');
            $table->string('REM')->nullable();
            $table->date('TIME');
            $table->integer('YCODE');
            $table->date('LAST_ACCESS_TIME');
            $table->integer('SHIPSTATUS')->default(0);
            $table->integer('DESCCMPCODE')->nullable();
            $table->integer('ORDCMPCODE')->nullable();
            $table->integer('CREATORCMPCODE')->nullable();
            $table->integer('EXRATE')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_order');
    }
}
