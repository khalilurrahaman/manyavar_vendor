<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSearchStringToVINVITEMTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('V_INVITEM', function (Blueprint $table) {
            $table->longText('SEARCH_STRING')->nullable()->after('NUM3');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('V_INVITEM', function (Blueprint $table) {
            $table->dropColumn('SEARCH_STRING');
        });
    }
}
