<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_role', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userid')->unsigned()->index();
            $table->foreign('userid')->references('id')->on('users')->onDelete('cascade');
            $table->integer('roleid')->unsigned()->index();
            $table->foreign('roleid')->references('id')->on('roles')->onDelete('cascade');
            $table->integer('assignedby')->unsigned()->index();
            $table->foreign('assignedby')->references('id')->on('users')->onDelete('cascade');
            $table->integer('modifiedby')->unsigned()->index();
            $table->foreign('modifiedby')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_role');
    }
}
