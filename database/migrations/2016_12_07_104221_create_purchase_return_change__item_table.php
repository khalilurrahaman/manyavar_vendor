<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseReturnChangeItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_return_change_item', function (Blueprint $table) {
            $table->increments('id');
            $table->string('RTCODE')->nullable();
            $table->string('PURRTDET_CODE')->nullable();
            $table->string('CHGCODE')->nullable();
            $table->double('RATE')->nullable();
            $table->string('BASIS')->nullable();
            $table->string('SOURCE')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_return_change_item');
    }
}
